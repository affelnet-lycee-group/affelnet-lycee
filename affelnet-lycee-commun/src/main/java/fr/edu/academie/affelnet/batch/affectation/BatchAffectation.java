/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.batch.affectation;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import fr.edu.academie.affelnet.batch.Batch;
import fr.edu.academie.affelnet.batch.EtatBatch;
import fr.edu.academie.affelnet.batch.audit.AuditLancementOPA;
import fr.edu.academie.affelnet.batch.audit.AuditPam;
import fr.edu.academie.affelnet.batch.audit.AuditPreReclassement;
import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.dao.interfaces.BatchDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.GroupeOrigineDao;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.operationProgrammeeAffectation.EtatLancementOPA;
import fr.edu.academie.affelnet.domain.parametrage.TypeTour;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/** Classe des traitements batch PAM. */
public class BatchAffectation extends Batch {

    /** Nom du traitement batch - Attribution des groupes origine. */
    public static final String NOM_BATCH_ATTRIBUTION_GROUPES = "attributionGroupes";

    /** Nom du traitement batch - Lancement d'une OPA. */
    public static final String NOM_BATCH_OPA_LANCEMENT = "opa_lancement";

    /** Nom du traitement batch PAM - Recalcul des piles. */
    public static final String NOM_BATCH_PAM_RECALCUL_PILES = "calculEtatsStats";

    /** Nom du traitement batch - Reclassement final. */
    public static final String NOM_BATCH_RECLASSEMENT = "opa_reclassementFinal";

    /** Nom du parametre pour l'identifiant d'OPA. */
    public static final String PARAMETRE_ID_OPA = "idOpa";

    /** Nom du parametre pour le nom du batch affectation ex�cut�. */
    public static final String PARAMETRE_NOM_BATCH = "nomBatch";

    /**
     * Nom du param�tre donnant le flag indiquant si on doit conserver la s�curisation des d�cisions prises lors
     * d'une OPA pr�c�dente.
     */
    public static final String PARAMETRE_CONSERVER_SECURISATION = "conserverSecurisation";

    /**
     * Nom du param�tre donnant le flag indiquant si on doit r�initialiser les r�sultats finaux du tour.
     */
    public static final String PARAMETRE_REINITIALISER_RESULTATS_FINAUX = "reinitialiserResultatsFinaux";

    /**
     * Nom du param�tre donnant le flag indiquant si ddes �l�ves sont impact�s par l'OPA.
     */
    public static final String PARAMETRE_IMPACT_ELEVE = "impactEleve";

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(BatchAffectation.class);

    /** L'audit utilis� par le batch. */
    private AuditPam auditPam;

    /** Liste des �tapes du traitement. */
    private List<EtapeTraitementAffectation> etapesTraitement;

    /**
     * L'op�ration planifi�e d'affectation sur laquelle travailler (peut �tre null si on n'est pas dans le cadre
     * d'une OPA).
     */
    private Long idOpa;

    /** La table des param�tres du traitement. */
    private Map<String, String> parametres;

    /** Le gestionnaire d'op�rations programm�es d'affectation. */
    private OperationProgrammeeAffectationManager opaManager;

    /** Le gestionnaire de campagne d'affectation. */
    private CampagneAffectationManager campagneAffectationManager;

    /** Le DAO des groupes origine. */
    private BaseDao<GroupeOrigine, String> groupeOrigineDao;

    /**
     * Cr�e un batch d'affectation pour une list d'�tapes de traitement.
     * 
     * @param parametres
     *            les param�tres de lancement du batch
     * 
     * @param etapesTraitement
     *            les �tapes du traitement � effectuer
     */
    public BatchAffectation(Map<String, String> parametres, List<EtapeTraitementAffectation> etapesTraitement) {

        this.etapesTraitement = etapesTraitement;
        this.parametres = parametres;

        // Pr�paration des beans DAO et managers
        this.opaManager = SpringUtils.getBean(OperationProgrammeeAffectationManager.class);
        this.campagneAffectationManager = SpringUtils.getBean(CampagneAffectationManager.class);
        this.groupeOrigineDao = SpringUtils.getBean(GroupeOrigineDao.class);

        // Si le batch concerne une OPA
        String idOpaStr = parametres.get(PARAMETRE_ID_OPA);
        if (idOpaStr == null) {
            this.idOpa = null;
        } else {
            this.idOpa = Long.parseLong(idOpaStr);
        }

    }

    @Override
    protected final void lancerTraitement() throws Exception {
        // Inutilis� car on surcharge la m�thode run()
    }

    /**
     * Lance l'audit pr�-affectation.
     * 
     * @return vrai si l'�tape d'audit a r�ussi, sinon faux
     * @throws Exception
     *             en cas de probl�me
     */
    private boolean lancerEtapeAudit() throws Exception {
        // Lance l'�tape
        messages.start("Audit pr�-affectation");
        auditPam.lancerTraitement();
        messages.end();

        return auditPam.isTraitementOK();
    }

    /**
     * 
     * @param etapeTraitement
     *            l'�tape de traitement � lancer
     * @throws Exception
     *             en cas d'erreur
     */
    protected void lancerEtapeTraitement(EtapeTraitementAffectation etapeTraitement) throws Exception {

        // R�cup�re dynamiquement un DAO permettant d'effectuer l'�tape du traitement.
        BatchDao batchDao = SpringUtils.getBean(etapeTraitement.getDaoClass());

        // Transmet les param�tres au DAO g�rant l'�tape
        batchDao.setParameters(parametres);

        // Lance l'�tape
        lancerTraitement(batchDao);
    }

    @Override
    public String getDescription() {

        StringBuffer description = new StringBuffer();
        description.append("Affectation (");

        if (idOpa != null) {

            OperationProgrammeeAffectation opa = opaManager.charger(idOpa);
            opa.ajouterDansDescriptionTraitement(description);

        } else {
            String libelleTour = TypeTour.getLibellePourTour(campagneAffectationManager.getNumeroTourCourant());
            description.append(libelleTour);
        }
        description.append(") : ");

        boolean dejaUneEtape = false;

        for (EtapeTraitementAffectation etape : etapesTraitement) {

            if (dejaUneEtape) {
                description.append(", ");
            } else {
                dejaUneEtape = true;
            }
            description.append(etape.getDescription());
        }

        return description.toString();
    }

    @Override
    protected String buildResultat() {

        StringBuffer resultatToutesEtapes = new StringBuffer();

        for (EtapeTraitementAffectation etape : etapesTraitement) {

            String messageResultatEtape;

            switch (etape) {

                case ATTRIBUTION_GROUPES:
                    messageResultatEtape = etape.getDescription() + decrireResultatsGroupesOrigineConstitues();
                    break;

                case CLASSEMENT_PROVISOIRE:
                case RECLASSEMENT_FINAL:

                    messageResultatEtape = getResultatsAuditPrealable(etape);
                    if (messageResultatEtape.isEmpty()) {
                        messageResultatEtape = etape.getDescription();
                    }

                    break;

                default:
                    messageResultatEtape = etape.getDescription();
            }

            resultatToutesEtapes.append("\n-");
            resultatToutesEtapes.append(messageResultatEtape);
        }
        return resultatToutesEtapes.toString();
    }

    /**
     * R�cup�re les messages de r�sultats d'un audit pr�-affectation pr�alable � une �tape.
     * 
     * Le r�sultat de l'audit retravaill� pour �tre silencieux sur un audit r�ussi.
     * 
     * @param etape
     *            les informations d'�tape (pour contextualiser le message)
     * 
     * @return le message bloquant l'�tape, issu de l'audit pr�-affectation si celui-ci echoue ou la chaine vide
     *         s'il n'y a pas de probl�me
     */
    private String getResultatsAuditPrealable(EtapeTraitementAffectation etape) {

        if (auditPam.isTraitementOK()) {
            return StringUtils.EMPTY;
        }

        // l'audit pr�-affectation ne s'est pas bien pass�
        // sinon l'utilisateur a �t� inform� par informerTraitementErreur()
        return "L'audit pr�-affectation a d�cel� des incoh�rences," + " l'�tape '" + etape.getDescription()
                + "' ne peut pas �tre r�alis�e.\n" + auditPam.buildResultat();

    }

    /**
     * @return texte de synth�se des r�sultats de l'Attribution des groupes
     */
    private String decrireResultatsGroupesOrigineConstitues() {

        List<GroupeOrigine> lister = groupeOrigineDao.lister();
        return GroupeOrigine.genererTexteTableauGroupes(lister);
    }

    /**
     * On va utiliser plusieurs sessions afin de minimiser l'utilisation m�moire
     * et la taille des fichiers de log.
     * 
     * TODO : le batch d'affectation est sp�cifique car on encha�ne un ensemble d'�tapes g�r�es par diff�rentes
     * DAO.
     * Les autres batchs ont un DAO associ�.
     */
    @Override
    public void run() {
        enregistrerLancement();

        // Etat de lancement actuel optimiste (dans le cas d'une OPA)
        EtatLancementOPA etatOpa = EtatLancementOPA.SUCCES;

        // Buffer pour collecter les r�sultats
        StringBuffer resultatsCollectes = new StringBuffer();

        try {
            LOG.debug("D�but du traitement batch.");

            changementEtat(EtatBatch.EN_COURS);

            // informer les ecouteurs
            informerDebutTraitement();

            // Le cas �ch�ant, information sur l'OPA concern�e pour les r�sultats
            if (idOpa != null) {
                resultats.put("idOpa", idOpa);
            }

            // si on continue
            boolean continuer = true;

            // On effectue l'audit pralable requis s'il y en a un
            determineAuditAdapte();

            if (auditPam != null) {
                try {
                    // ouverture de la session
                    HibernateUtil.openSession();
                    // d�but des transaction
                    HibernateUtil.beginTransaction();

                    continuer = lancerEtapeAudit();

                    // si l'audit pr�-affectation ne s'est pas bien pass� on s'arr�te
                    if (!continuer) {
                        // l'audit pr�-affectation ne s'est pas bien pass�
                        String msgErr = "Le traitement a �t� interrompu"
                                + " car l'audit pr�-affectation a d�cel� des incoh�rences";

                        String causeErreur = auditPam.buildResultat();
                        // afficher en html pour plus de lisibilit�
                        String causeErreurHtml = causeErreur.replaceAll("\\n", "<br />");
                        messages.start(msgErr);
                        messages.end();

                        changementEtat(EtatBatch.EN_ERREUR);

                        informerTraitementEnErreur(msgErr + "<br/><br/>" + causeErreurHtml);
                        executionBatch.setErreur(msgErr + "\n\n" + causeErreur);

                        if (idOpa != null) {
                            etatOpa = EtatLancementOPA.ERREUR;
                        }

                    }

                    // validation de la transaction
                    HibernateUtil.commit();
                } finally {
                    // Fermeture de la session
                    HibernateUtil.closeSession();
                }
            }

            // si l'audit pr�-affectation s'est bien pass� ou s'il n'y a pas eu d'audit
            if (continuer) {

                for (EtapeTraitementAffectation etape : etapesTraitement) {

                    LOG.info("Etape du traitement : " + etape);
                    try {
                        // ouverture de la session
                        HibernateUtil.openSession();

                        // d�but de transaction
                        HibernateUtil.beginTransaction();

                        resultatsCollectes.append("\n - ");
                        resultatsCollectes.append(etape.getDescription());

                        lancerEtapeTraitement(etape);

                        if (etape == EtapeTraitementAffectation.ATTRIBUTION_GROUPES) {
                            resultatsCollectes.append(decrireResultatsGroupesOrigineConstitues());
                        }

                        // validation de la transaction
                        HibernateUtil.commit();
                    } finally {
                        // Fermeture de la session
                        HibernateUtil.closeSession();
                    }
                }

            }

            LOG.debug("Le traitement batch s'est termin� avec succ�s.");

            try {
                // ouverture de la session
                HibernateUtil.openSession();
                // d�but des transaction
                HibernateUtil.beginTransaction();

                // enregistrement des r�sultats (r�sultat uniquement si l'auditOK)
                if (continuer) {
                    executionBatch.setResultat(resultatsCollectes.toString());
                }

                // validation de la transaction
                HibernateUtil.commit();
            } finally {
                // Fermeture de la session
                HibernateUtil.closeSession();
            }

            changementEtat(EtatBatch.FIN_NORMALE);

            // informer les ecouteurs
            informerTraitementTermine();

        } catch (Exception e1) {

            String msg = (StringUtils.isNotEmpty(e1.getMessage()) ? e1.getMessage() : e1.getClass().toString());

            LOG.fatal("Une erreur s'est produite lors de l'ex�cution du batch :" + msg, e1);

            LOG.debug("Annulation de la transaction");

            // Rollback de la transaction
            try {
                HibernateUtil.rollback();
                LOG.info("La transaction a �t� annul�e avec succ�s");
            } catch (DataAccessException e2) {
                LOG.fatal("L'annulation de la transaction par rollback a �choue", e2);
            }

            // Enregistrement dans l'historique des traitements
            StringWriter sw = new StringWriter();
            e1.printStackTrace(new PrintWriter(sw));
            // TODO ajouter � quelle �tape
            executionBatch.setErreur(sw.toString());

            changementEtat(EtatBatch.EN_ERREUR);

            // informer les ecouteurs
            informerTraitementEnErreur(msg);

            if (idOpa != null) {
                etatOpa = EtatLancementOPA.ERREUR;
            }

        } finally {

            if (!estTerminaisonGeree()) {
                gererFinAnormale();
                etatOpa = EtatLancementOPA.ERREUR;
            }

            // fermeture de la session dao
            Session currentSession = HibernateUtil.currentSession();
            if (currentSession != null && currentSession.isOpen()) {
                HibernateUtil.closeSession();
            }

            if (idOpa != null) {
                enregistrerStatusOpa(etatOpa);
            }

            enregistrerFin();
        }
    }

    /**
     * Enregistre l'�tat de lancement de l'OPA.
     * 
     * @param etatOpa
     *            etat de l'OPA
     */
    private void enregistrerStatusOpa(EtatLancementOPA etatOpa) {
        LOG.info("Enregistrement du statut de lancement l'OPA : ");
        try {
            // ouverture de la session
            HibernateUtil.openSession();
            // d�but des transaction
            HibernateUtil.beginTransaction();

            OperationProgrammeeAffectation opa = opaManager.charger(idOpa);
            opa.setDateDernierLancement(new Date());
            opa.setEtatLancement(etatOpa);
            opaManager.modifier(opa);

            // validation de la transaction
            HibernateUtil.commit();
        } finally {
            // Fermeture de la session
            HibernateUtil.closeSession();
        }
    }

    /** M�thode de cr�ation du batch de l'audit a effectuer selon les �tapes d'ex�cution pr�vues pour le batch. */
    private void determineAuditAdapte() {

        // TODO : voir si on passe � un fonctionnement o� l'on interroge chaque �tape du plan pour d�terminer si
        // elle requiert un audit et de quel type et voir comment r�cup�rer une instance compatible

        if (etapesTraitement.contains(EtapeTraitementAffectation.RECLASSEMENT_FINAL)) {

            LOG.info("Le plan de batch affectation contient un reclassement final "
                    + "qui n�cessite un audit pr�-reclassement");
            auditPam = new AuditPreReclassement();
        }

        if (etapesTraitement.contains(EtapeTraitementAffectation.CLASSEMENT_PROVISOIRE)) {

            LOG.info("Le plan de batch contient une �tape qui n�cessite un audit pr�-affectation");
            auditPam = new AuditLancementOPA(idOpa.toString());
        }

        // Il faut transmettre au batch appel� le gestionnaire de messages et la table des r�sultats
        if (auditPam != null) {
            auditPam.setMessages(messages);
            auditPam.setResultats(resultats);
        }
    }
}
