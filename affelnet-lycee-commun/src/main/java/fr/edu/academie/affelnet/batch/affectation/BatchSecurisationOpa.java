/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.batch.affectation;

import org.hibernate.Query;

import fr.edu.academie.affelnet.batch.Batch;
import fr.edu.academie.affelnet.batch.planificationCampagneAffectation.CompteurOpa;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.operationProgrammeeAffectation.EtatLancementOPA;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/**
 * Traitement de lancement de la s�curisation des r�sultats provisoires d'une OPA.
 */
public class BatchSecurisationOpa extends Batch {

    /** Nom du traitement batch. */
    public static final String NOM_BATCH = "opa_securisation";

    /**
     * Nom du r�sultat correspondant au d�roulement de la s�curisation.
     */
    public static final String SECURISATION_OK = "securisationOK";

    /**
     * Nom du r�sultat correspondant � l'OPA.
     */
    public static final String OPA = "opa";

    /**
     * Nom du r�sultat correspondant au nombre de voeux.
     */
    public static final String NOMBRE_ELEVES = "nombreEleves";

    /**
     * Nom du r�sultat correspondant au nombre d'�l�ves.
     */
    public static final String NOMBRE_VOEUX = "nombreVoeux";

    /**
     * Nom du r�sultat correspondant au nombre de voeux s�curis�s.
     */
    public static final String NOMBRE_VOEUX_SECURISES = "nombreVoeuxSecurises";

    /**
     * Le dao pour les r�sultats provisoires.
     */
    private OperationProgrammeeAffectationManager operationProgrammeeAffectationManager;

    /**
     * L'OPA concern�e par le batch de s�curisation.
     */
    private OperationProgrammeeAffectation opa;

    /**
     * Les informations sur des comptes pour l'OPA.
     */
    private CompteurOpa compteur;

    /**
     * Constructeur.
     * 
     * @param idOpa
     *            identifiant de l'OPA s�curis�e
     */
    public BatchSecurisationOpa(String idOpa) {
        this.operationProgrammeeAffectationManager = SpringUtils
                .getBean(OperationProgrammeeAffectationManager.class);
        opa = operationProgrammeeAffectationManager.charger(Long.valueOf(idOpa));
    }

    /**
     * @param operationProgrammeeAffectationManager
     *            the operationProgrammeeAffectationManager to set
     */
    public void setOperationProgrammeeAffectationManager(
            OperationProgrammeeAffectationManager operationProgrammeeAffectationManager) {
        this.operationProgrammeeAffectationManager = operationProgrammeeAffectationManager;
    }

    @Override
    public String getDescription() {

        StringBuffer description = new StringBuffer();
        description.append("S�curisation des r�sultats provisoires (");
        opa.ajouterDansDescriptionTraitement(description);
        description.append(")");
        return description.toString();
    }

    @Override
    public void lancerTraitement() {
        // V�rification de l'�tat de l'OPA
        if (EtatLancementOPA.SUCCES.equals(opa.getEtatLancement())) {
            securiserResultats();
            resultats.put(SECURISATION_OK, true);
        } else {
            resultats.put(SECURISATION_OK, false);
            throw new ValidationException("L'OPA indiqu�e n'est pas dans l'�tat \"succ�s\"");
        }
    }

    @Override
    public String buildResultat() {
        String resultats = "OPA : " + opa.getNom() + "\n";
        resultats += "Nombre d'�l�ves : " + compteur.getNombreEleves() + "\n";
        resultats += "Nombre de voeux : " + compteur.getNombreVoeuxEleve() + "\n";
        resultats += "Nombre de voeux s�curis�s : " + compteur.getNombreVoeuxSecurises();

        return resultats;
    }

    /**
     * M�thode de s�curisation des voeux.
     */
    private void securiserResultats() {

        messages.start("S�curisation des r�sultats provisoires");
        Query query = HibernateUtil.currentSession().getNamedQuery("securisationResultatsProvisoires.update.sql");
        query.setParameter("idOpa", opa.getId());
        query.setParameter("codeDecision", ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_AFFECTE);
        query.executeUpdate();
        messages.end();

        compteur = operationProgrammeeAffectationManager.calculerCompteurs(opa.getId());

        resultats.put(OPA, opa);
        resultats.put(NOMBRE_ELEVES, compteur.getNombreEleves());
        resultats.put(NOMBRE_VOEUX, compteur.getNombreVoeuxEleve());
        resultats.put(NOMBRE_VOEUX_SECURISES, compteur.getNombreVoeuxSecurises());
    }
}
