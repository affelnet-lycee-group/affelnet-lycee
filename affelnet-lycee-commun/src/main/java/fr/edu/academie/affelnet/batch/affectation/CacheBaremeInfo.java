/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.batch.affectation;

import java.util.HashMap;
import java.util.Map;

/**
 * Objet utilis� dans le calcul du bar�me contenant des informations diverses pour une offre information.
 */
public class CacheBaremeInfo {
    /** Le code de l'offre de formation. */
    private String codeVoeu;

    /** Le mode de calcul du bar�me. */
    private String modeCalculBareme;

    /** Le bonus de doublement. */
    private Integer bonusDoublement;

    /** La map liant les codes des champs disciplinaires et les coefficients associ�s. */
    private Map<String, Integer> coeffParCodeChampDisciplinaire;

    /** La map liant les id des �valuations compl�mentaires et les coefficients associ�s. */
    private Map<Long, Integer> coeffParIdEvalComp;

    /** La map liant les id des rangs des mati�res et les coefficients associ�s. */
    private Map<Long, Integer> coeffParIdRang;

    /**
     * Constructeur par d�faut.
     */
    public CacheBaremeInfo() {
        super();
        coeffParCodeChampDisciplinaire = new HashMap<String, Integer>();
        coeffParIdEvalComp = new HashMap<Long, Integer>();
        coeffParIdRang = new HashMap<Long, Integer>();
    }

    /**
     * @param codeVoeu
     *            code de l'offre formation associ�e.
     */
    public CacheBaremeInfo(String codeVoeu) {
        this();
        this.codeVoeu = codeVoeu;
    }

    /**
     * @return the codeVoeu
     */
    public String getCodeVoeu() {
        return codeVoeu;
    }

    /**
     * @param codeVoeu
     *            the codeVoeu to set
     */
    public void setCodeVoeu(String codeVoeu) {
        this.codeVoeu = codeVoeu;
    }

    /**
     * @return the modeCalculBareme
     */
    public String getModeCalculBareme() {
        return modeCalculBareme;
    }

    /**
     * @param modeCalculBareme
     *            the modeCalculBareme to set
     */
    public void setModeCalculBareme(String modeCalculBareme) {
        this.modeCalculBareme = modeCalculBareme;
    }

    /**
     * @return the bonusDoublement
     */
    public Integer getBonusDoublement() {
        return bonusDoublement;
    }

    /**
     * @param bonusDoublement
     *            the bonusDoublement to set
     */
    public void setBonusDoublement(Integer bonusDoublement) {
        this.bonusDoublement = bonusDoublement;
    }

    /**
     * @return the coeffParCodeChampDisciplinaire
     */
    public Map<String, Integer> getCoeffParCodeChampDisciplinaire() {
        return coeffParCodeChampDisciplinaire;
    }

    /**
     * @return the coeffParIdEvalComp
     */
    public Map<Long, Integer> getCoeffParIdEvalComp() {
        return coeffParIdEvalComp;
    }

    /**
     * @param coeffParIdEvalComp
     *            the coeffParIdEvalComp to set
     */
    public void setCoeffParIdEvalComp(Map<Long, Integer> coeffParIdEvalComp) {
        this.coeffParIdEvalComp = coeffParIdEvalComp;
    }

    /**
     * @return the coeffParIdRang
     */
    public Map<Long, Integer> getCoeffParIdRang() {
        return coeffParIdRang;
    }

    /**
     * @param coeffParIdRang
     *            the coeffParIdRang to set
     */
    public void setCoeffParIdRang(Map<Long, Integer> coeffParIdRang) {
        this.coeffParIdRang = coeffParIdRang;
    }

    /**
     * @param coeffParCodeChampDisciplinaire
     *            the coeffParCodeChampDisciplinaire to set
     */
    public void setCoeffParCodeChampDisciplinaire(Map<String, Integer> coeffParCodeChampDisciplinaire) {
        this.coeffParCodeChampDisciplinaire = coeffParCodeChampDisciplinaire;
    }

    /**
     * D�finit une valeur dans la map des coefficient des champs disciplinaires.
     * 
     * @param codeChamp
     *            code du champs disciplinaire
     * @param valCoeff
     *            valeur du coefficient
     */
    public void putCoeffParCodeChampDisciplinaire(String codeChamp, Integer valCoeff) {
        this.coeffParCodeChampDisciplinaire.put(codeChamp, valCoeff);
    }

    /**
     * Fournit le coefficient associ� au champ disciplinaire.
     * 
     * @param codeChamp
     *            le code du champ disciplinaire
     * @return le coefficient du champ disciplinaire
     */
    public Integer getCoeffCodeChampDisciplinaire(String codeChamp) {
        return this.coeffParCodeChampDisciplinaire.get(codeChamp);
    }

    /**
     * D�finit une valeur dans la map des coefficient des �valuations compl�mentaires.
     * 
     * @param idEvalComp
     *            id de l'�valuation compl�mentaire
     * @param valCoeff
     *            valeur du coefficient
     */
    public void putCoeffIdEvalComp(Long idEvalComp, Integer valCoeff) {
        this.coeffParIdEvalComp.put(idEvalComp, valCoeff);
    }

    /**
     * Fournit le coefficient associ� � l'�valuation compl�mentaire.
     * 
     * @param idEvalComp
     *            id de l'�valuation compl�mentaire
     * @return le coefficient de l'�valuation compl�mentaire
     */
    public Integer getCoeffIdEvalComp(Long idEvalComp) {
        return this.coeffParIdEvalComp.get(idEvalComp);
    }

    /**
     * D�finit une valeur dans la map des coefficient des rangs.
     * 
     * @param idRang
     *            l'id du rang
     * @param valCoeff
     *            valeur du coefficient
     */
    public void putCoeffIdRang(Long idRang, Integer valCoeff) {
        this.coeffParIdRang.put(idRang, valCoeff);
    }

    /**
     * Fournit le coefficient associ� au rang.
     * 
     * @param idRang
     *            l'id du rang
     * @return le coefficient du rang
     */
    public Integer getCoeffIdRang(Long idRang) {
        return this.coeffParIdRang.get(idRang);
    }
}
