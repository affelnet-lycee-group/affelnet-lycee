/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.batch.affectation;

import fr.edu.academie.affelnet.dao.hibernate.affectation.ReinitialisationResultatsFinauxDaoImpl;
import fr.edu.academie.affelnet.dao.interfaces.BatchDao;
import fr.edu.academie.affelnet.dao.interfaces.affectation.CalculMoyenneDao;
import fr.edu.academie.affelnet.dao.interfaces.affectation.ClassementProvisoireDao;
import fr.edu.academie.affelnet.dao.interfaces.affectation.HarmonisationDao;
import fr.edu.academie.affelnet.dao.interfaces.affectation.ReclassementFinalDao;
import fr.edu.academie.affelnet.dao.interfaces.pam.AttributionGroupeDao;
import fr.edu.academie.affelnet.dao.interfaces.pam.CalculBaremeDao;
import fr.edu.academie.affelnet.dao.interfaces.pam.RecalculePilesDao;

/** Les �tapes du traitement d'affectation. */
public enum EtapeTraitementAffectation {

    /** Attribution des groupes origine aux �l�ves. */
    ATTRIBUTION_GROUPES("Attribution des groupes", AttributionGroupeDao.class),

    /** Calcul des moyennes. */
    CALCUL_MOYENNE("Calcul moyenne", CalculMoyenneDao.class),

    /** Harmonisation des �valuations/notes. */
    HARMONISATION("Harmonisation", HarmonisationDao.class),

    /** Initialisation des r�sultats finaux et des statistiques. */
    REINITIALISATION_RESULTATS_FINAUX("R�initialisation des r�sultats finaux et des statistiques",
            ReinitialisationResultatsFinauxDaoImpl.class),

    /** Calcul du bar�me. */
    CALCUL_BAREME("Calcul du bar�me", CalculBaremeDao.class),

    /**
     * Classement provisoire des voeux des �l�ves pour une op�ration programm�e d'affectation et attribution d'un
     * r�sultat provisoire.
     */
    CLASSEMENT_PROVISOIRE("Classement provisoire", ClassementProvisoireDao.class),

    /** Reclassement final des voeux et des �l�ves. */
    RECLASSEMENT_FINAL("Reclassement final", ReclassementFinalDao.class),

    /** Calcul des �tats statistiques. */
    CALCUL_ETATS_STATISTIQUES("Calcul des �tats statistiques", RecalculePilesDao.class);

    /** Description de l'�tape. */
    private String description;

    /** La classe de DAO pour ce traitement. */
    private Class<? extends BatchDao> classe;

    /**
     * @param description
     *            la description de l'�tape
     * @param classe
     *            la classe de DAO
     */
    private EtapeTraitementAffectation(String description, Class<? extends BatchDao> classe) {
        this.description = description;
        this.classe = classe;
    }

    /** @return description de l'�tape */
    public String getDescription() {
        return this.description;
    }

    /** @return la classe de DAO pour cette �tape. */
    public Class<? extends BatchDao> getDaoClass() {
        return classe;
    }

}
