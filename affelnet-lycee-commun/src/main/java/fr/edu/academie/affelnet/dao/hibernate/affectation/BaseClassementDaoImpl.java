/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.springframework.beans.factory.annotation.Autowired;

import fr.edu.academie.affelnet.dao.hibernate.BatchDaoImpl;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.domain.affectation.Classement;
import fr.edu.academie.affelnet.domain.affectation.EntreeClassement;
import fr.edu.academie.affelnet.domain.affectation.EntreeClassementBareme;
import fr.edu.academie.affelnet.domain.affectation.EntreeClassementCommission;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.service.ParametreManager;

/** Base des traitements de classement de voeux des �l�ves. */
public abstract class BaseClassementDaoImpl extends BatchDaoImpl {

    /** Nombre d'it�rations avant le nettoyage d'une session Hibernate. */
    protected static final int INTERVALLE_NETTOYAGE_SESSION = 1000;

    /** Comparateur d'entrees de classement par rang. */
    protected static final Comparator<EntreeClassement> COMPARATEUR_ENTREES_PAR_RANG = new Comparator<EntreeClassement>() {

        @Override
        public int compare(EntreeClassement o1, EntreeClassement o2) {

            int rang1 = o1.getRang();
            int rang2 = o2.getRang();

            return Integer.compare(rang1, rang2);
        }

    };

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(BaseClassementDaoImpl.class);

    /** Le gestionnaire de param�tres. */
    protected ParametreManager parametreManager;

    /** Le classement des voeux d'�l�ves par offre de formation. **/
    protected Map<String, Classement<EntreeClassement>> classementParOffre = new HashMap<String, Classement<EntreeClassement>>();

    /** Ensemble des codes d'offres de formation restant � traiter. */
    protected Set<String> codesOffresRestantATraiter = new HashSet<String>();

    /**
     * Table des entr�es de classement correspondant aux r�sultats de voeux � classer par �l�ve (ine), dans l'ordre
     * des rangs croissants.
     */
    protected Map<String, TreeSet<EntreeClassement>> entreesVoeuxIntegrerParRangParEleve;

    /**
     * @param parametreManager
     *            Le gestionnaire de param�tres.
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * Classement it�ratif par troncature des listes et insertion des voeux suivants.
     * 
     * @throws SQLException
     *             en cas de probl�me sur les donn�es
     */
    protected void classementVoeuEleve() throws SQLException {

        // Indice de l'it�ration pour l'avancement.
        int iteration = 1;

        // Tant qu'il y a des offres de formation restant � traiter
        while (!codesOffresRestantATraiter.isEmpty()) {

            messages.start("Etape " + iteration++ + " (" + codesOffresRestantATraiter.size()
                    + " offres de formation � traiter) ");

            tronquerListesPrincipales();
            tronquerListesSupplementaires();

            messages.end();
        }
    }

    /**
     * Ajoute une liste r�sultats de voeux d'�l�ve dans les classements.
     * 
     * @param resultatsOpa
     *            la liste des r�sultats � ajouter.
     */
    protected void ajouterNouvellesEntreesClassement(List<ResultatsProvisoiresOpa> resultatsOpa) {

        for (ResultatsProvisoiresOpa resultatOpa : resultatsOpa) {
            ajouterNouvelleEntreeClassement(resultatOpa);
        }
    }

    /**
     * Ajoute une nouvelle entr�e dans les classements pour le r�sultat d'OPA fourni.
     * 
     * @param resultatProvisoire
     *            le r�sultat provisoire correspondant au voeu de l'�l�ve � ajouter ajouter dans le classement
     */
    protected void ajouterNouvelleEntreeClassement(ResultatsProvisoiresOpa resultatProvisoire) {

        EntreeClassement entreeClassement = buildEntreeClassement(resultatProvisoire);
        ajouterNouvelleEntreeClassement(entreeClassement);
    }

    /**
     * Ajoute une nouvelle entr�e dans les classements.
     * 
     * @param entreeClassement
     *            l'entr�e de classement pour le r�sultat provisoire correspondant au voeu de l'�l�ve
     *            � ajouter ajouter dans le classement
     */
    protected void ajouterNouvelleEntreeClassement(EntreeClassement entreeClassement) {

        String codeOffre = entreeClassement.getCodeOffreFormation();

        // L'offre de formation correspondante doit exister par son code dans les classements
        if (classementParOffre.containsKey(codeOffre)) {
            classementParOffre.get(codeOffre).ajouterListePrincipale(entreeClassement);

            // Met � jour la la liste des codes des offres de formation de fa�on � ce que
            // l'offre de formation soit (� nouveau) trait�e par la suite pour prendre en compte
            // le voeu d'�l�ve que l'on vient d'ajouter
            codesOffresRestantATraiter.add(codeOffre);
        }
    }

    /**
     * Construit un �l�ment de classement poue le r�sultat d'un voeu d'�l�ve fourni.
     * 
     * @param resultatsProvisoiresOpa
     *            le r�sultat du voeu de l'�l�ve � traiter
     * @return l'entr�e de classement � traiter
     */
    protected EntreeClassement buildEntreeClassement(ResultatsProvisoiresOpa resultatsProvisoiresOpa) {

        boolean utiliseBareme = resultatsProvisoiresOpa.getVoeuEleve().getVoeu().utiliseBareme();

        if (utiliseBareme) {
            return new EntreeClassementBareme(resultatsProvisoiresOpa);
        } else {
            return new EntreeClassementCommission(resultatsProvisoiresOpa);
        }
    }

    /**
     * Tronque l'ensemble ordonn� des r�sultats des voeux d'�l�ves pris en liste principale
     * pour chaque offre de formation pour respecter la capacit�.
     * 
     * <p>
     * Les �l�ments retir�s sont mis en liste suppl�mentaire.
     * </p>
     * <p>
     * Les voeux de rang suivant (s'il y en a) seront ajout�s � la liste des pris.
     * </p>
     * 
     * <ol>
     * <li>Pour les offres sans bar�me, on retire des voeux �l�ves class�s ceux qui ont des d�cisions provisoires
     * les "excluent" (annuler, non trait� ...)</li>
     * <li>On exclut les voeux des �levs class�s forc� � "refus�"</li>
     * <li>On r�duit la liste des voeux d'�l�ves pris pour qu'elle respecte la capacit� en mettant les voeux
     * restants dans la liste suppl�mentaire</li>
     * </ol>
     * 
     * @throws SQLException
     *             en cas de probl�me sur les donn�es
     */
    private void tronquerListesPrincipales() throws SQLException {

        // le nombre d'offres trait�es (pour l'avancement)
        int nombreOffresTraitees = 0;

        // le nombre d'offres restant � consid�rer
        int nombreOffresATaiter = codesOffresRestantATraiter.size();

        // Parcourt l'association des classements pour chaque offre de formation
        for (Map.Entry<String, Classement<EntreeClassement>> entreeClassement : classementParOffre.entrySet()) {

            // R�cup�re le code de l'offre de formation
            String codeVoeu = entreeClassement.getKey();

            // Si l'offre de formation n'a pas encore �t� consid�r�e
            // ou si de nouveaux voeux d'�l�ves ont �t� ajout�s sur cette offre de formation.
            if (codesOffresRestantATraiter.contains(codeVoeu)) {

                LOG.debug("Troncature liste principale de l'offre : " + codeVoeu);

                // R�cup�re le classement associ�
                Classement<EntreeClassement> classement = entreeClassement.getValue();

                // Tronque la liste principale et r�cup�re la liste des voeux d'�l�ves retires du classement
                // pour cette offre de formation pour lesquels il faut consid�rer le voeu suivant
                List<EntreeClassement> listeVoeuEleveRetiresClassement = tronquerListePrincipale(classement);

                // On consid�re cette offre de formation comme trait�e pour l'instant.
                // On reviendra dessus �ventuellement si un VoeuEleve portant sur cette offre de formation est
                // r�introduit par la suite
                codesOffresRestantATraiter.remove(codeVoeu);

                // Introduire les nouveaux VoeuEleve
                // et mettre � jour la liste des voeux � consid�rer
                introduireVoeuEleveSuivants(listeVoeuEleveRetiresClassement);

                LOG.debug("Troncature liste principale effectu�e");

                messages.setAvancement(++nombreOffresTraitees, nombreOffresATaiter);

            } // if -- offre de formation � traiter

        } // for -- Parcours de l'association des voeux �l�ves class�s par offre

    }

    /**
     * Tronque une liste principale de voeux class�s.
     * 
     * <ul>
     * <li>On retire tous les voeux qui ne sont pas classables.</li>
     * <li>On ram�ne la liste principale � sa capacit� et les voeux classables en trop sont revers�s sur la liste
     * suppl�mentaire.</li>
     * <li>On retourne l'ensemble des voeux retir�s (cela permettra de collecter les voeux de rangs suivants les
     * �l�ves correspondants).</li>
     * </ul>
     * 
     * @param classement
     *            le classement � traiter
     * @return la liste des voeux d'�l�ves retires du classement en liste principale
     */
    private List<EntreeClassement> tronquerListePrincipale(Classement<EntreeClassement> classement) {

        List<EntreeClassement> listeVoeuEleveRetiresClassement = new ArrayList<EntreeClassement>();

        // la liste des �l�ves pris pour cette offre de formation
        TreeSet<EntreeClassement> listePris = classement.getListePrincipale();

        // Retire les voeux des �l�ves exclus du classement (refus�s, absents ...)
        LOG.debug(" - debut de suppression des voeux exclus du classement");
        for (Iterator<EntreeClassement> iter = listePris.iterator(); iter.hasNext();) {

            EntreeClassement entreeClassementTestee = iter.next();

            if (entreeClassementTestee.estExclusClassement()) {

                iter.remove();

                // On m�morise le voeu retir�
                listeVoeuEleveRetiresClassement.add(entreeClassementTestee);
            }
        }
        LOG.debug(" - fin suppression des voeux exclus du classement");

        classement.tronquerListePrincipale(listeVoeuEleveRetiresClassement);

        return listeVoeuEleveRetiresClassement;
    }

    /**
     * Tronque les listes suppl�mentaires de chaque offre de formation pour se limiter � la capacit�.
     */
    private void tronquerListesSupplementaires() {

        // Pour tous les classements de voeux �l�ve
        for (Classement<EntreeClassement> classement : classementParOffre.values()) {
            classement.tronquerListeSupplementaire();
        }
    }

    /** Collecte l'ensemble des �l�ments correspondant aux r�sultats de voeux � classer. */
    protected void collecterVoeuxAClasser() {

        messages.start("Collecte des voeux d'�l�ves � classer");

        int nbResultatsCollectes = 0;
        entreesVoeuxIntegrerParRangParEleve = new HashMap<>();

        Criteria criteriaResultatsVoeux = getCriteriaResultatsVoeux();
        ScrollableResults scrollResult = criteriaResultatsVoeux.scroll();

        while (scrollResult.next()) {

            ResultatsProvisoiresOpa resultatsProvisoires = (ResultatsProvisoiresOpa) scrollResult.get(0);

            EntreeClassement entreeClassement = buildEntreeClassement(resultatsProvisoires);

            String ine = entreeClassement.getIne();
            TreeSet<EntreeClassement> voeuxEleve = entreesVoeuxIntegrerParRangParEleve.get(ine);

            if (voeuxEleve == null) {
                // Ajoute le classement pour l'�l�ve s'il n'�tait pas pr�sent
                voeuxEleve = new TreeSet<EntreeClassement>(COMPARATEUR_ENTREES_PAR_RANG);
                entreesVoeuxIntegrerParRangParEleve.put(ine, voeuxEleve);
            }

            voeuxEleve.add(entreeClassement);

            // Nettoyage r�gulier de la session Hibernate
            if ((nbResultatsCollectes % INTERVALLE_NETTOYAGE_SESSION == 0) && (nbResultatsCollectes > 0)) {

                LOG.debug("Nettoyage des caches et de la session Hibernate");
                HibernateUtil.cleanupSession();
            }

            // maj de l'avancement
            messages.setAvancement(++nbResultatsCollectes);

        }
        scrollResult.close();

        // Nettoyage de la session en cours
        HibernateUtil.cleanupSession();

        messages.end();
    }

    /**
     * Cr�e un crit�re Hibernate permettant de r�cup�rer les r�sultats correspondant aux voeux des �l�ves � classer
     * avec les informations utiles associ�es en fetch join.
     * 
     * @return Crit�ria Hibernate
     */
    protected abstract Criteria getCriteriaResultatsVoeux();

    /**
     * Ajoute aux classements toutes les entr�es correspondant aux premiers voeux dans l'ordre des rangs.
     * (except�s ceux des �l�ves forc�s globalement).
     */
    protected void integrerPremiersVoeux() {

        LOG.debug("Int�gration de tous les premiers voeux des �l�ves");

        // Parcours des voeux des �l�ves restant � classer
        Iterator<Map.Entry<String, TreeSet<EntreeClassement>>> itEntreesVoeuxEleve = entreesVoeuxIntegrerParRangParEleve
                .entrySet().iterator();

        while (itEntreesVoeuxEleve.hasNext()) {

            Map.Entry<String, TreeSet<EntreeClassement>> entreeVoeuxAIntegrerPourEleve = itEntreesVoeuxEleve
                    .next();

            TreeSet<EntreeClassement> voeuxAIntegrer = entreeVoeuxAIntegrerPourEleve.getValue();

            // Prend (et supprime) le r�sultat correspondant au prochain voeu (dans l'ordre du rang)
            EntreeClassement premiereEntreeVoeuAClasserPourEleve = voeuxAIntegrer.pollFirst();
            if (premiereEntreeVoeuAClasserPourEleve != null) {

                // Classe l'entr�e de classement pour le premier voeu que l'on vient de prendre
                ajouterNouvelleEntreeClassement(premiereEntreeVoeuAClasserPourEleve);
            }

            // S'il ne reste plus d'autre voeux, on purge
            if (voeuxAIntegrer.isEmpty()) {
                // Supprime l'entr�e correspondant � l'�l�ve
                itEntreesVoeuxEleve.remove();
            }

        }

    }

    /**
     * Introduire les entr�es de classement correspondant aux r�sultats pour les voeux des �l�ves de rang suivant
     * pour les entr�es de classement de la liste pass�e en param�tre.
     * 
     * @param listEntreesPourAjoutVoeuSuivant
     *            entr�es de classement correspondant aux VoeuEleve exclus de la liste des VoeuEleve pris
     */
    protected void introduireVoeuEleveSuivants(List<EntreeClassement> listEntreesPourAjoutVoeuSuivant) {

        int nbVoeux = listEntreesPourAjoutVoeuSuivant.size();

        if (nbVoeux == 0) {
            // Aucun voeu � ajouter
            return;
        }

        LOG.debug("Introduction des voeux suivants pour " + nbVoeux + " entr�es");

        // Pour chaque entr�e de classement dont on va rechercher le voeu suivant
        for (EntreeClassement entreeClassement : listEntreesPourAjoutVoeuSuivant) {

            // On r�cup�re l'INE
            String ine = entreeClassement.getIne();

            // On r�cup�re les entr�es restantes, on retire la premi�re et on l'ajoute au classement
            TreeSet<EntreeClassement> entreesVoeuxEleveSuivantes = entreesVoeuxIntegrerParRangParEleve.get(ine);
            if (entreesVoeuxEleveSuivantes != null) {

                EntreeClassement entreeSuivante = entreesVoeuxEleveSuivantes.pollFirst();
                if (entreeSuivante != null) {
                    ajouterNouvelleEntreeClassement(entreeSuivante);
                }

                // S'il ne reste plus de voeux pour l'�l�ve, on ne repassera plus dessus
                // donc on retire l'association de la table pour cet �l�ve
                if (entreesVoeuxEleveSuivantes.isEmpty()) {
                    entreesVoeuxIntegrerParRangParEleve.remove(ine);
                }
            }

        } // for -- entr�es de voeux dont on cherche les suivantes

    }

    /**
     * Enregistre en base les r�sultats du classement.
     * 
     * @throws SQLException
     *             en cas de probl�me sur les donn�es
     */
    protected abstract void archiverResultats() throws SQLException;
}
