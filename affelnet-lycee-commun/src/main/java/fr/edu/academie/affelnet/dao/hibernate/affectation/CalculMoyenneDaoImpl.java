/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.pam.BatchOpaDaoImpl;
import fr.edu.academie.affelnet.dao.interfaces.affectation.CalculMoyenneDao;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.EleveManager;
import fr.edu.academie.affelnet.service.EvalNoteManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationDisciplineManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/**
 * Traitement de calcul de la moyenne lors de l'affectation.
 */
@Repository("CalculMoyenneDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CalculMoyenneDaoImpl extends BatchOpaDaoImpl implements CalculMoyenneDao {

    /** Loggeur de la classe. */
    private static final Log LOG = LogFactory.getLog(CalculMoyenneDaoImpl.class);

    /**
     * Le manager utilis� pour les �valuations/notes.
     */
    private EvalNoteManager evalNoteManager;

    /**
     * Le manager utilis� pour les disciplines et champs disciplinaires.
     */
    private EvaluationDisciplineManager evaluationDisciplineManager;

    /**
     * Le manager utilis� pour les �l�ves.
     */
    private EleveManager eleveManager;

    /**
     * Le manager utilis� pour les rangs.
     */
    private OperationProgrammeeAffectationManager operationProgrammeeAffectationManager;

    /**
     * @param evalNoteManager
     *            the evalNoteManager to set
     */
    @Autowired
    public void setEvalNoteManager(EvalNoteManager evalNoteManager) {
        this.evalNoteManager = evalNoteManager;
    }

    /**
     * @param evaluationDisciplineManager
     *            the evaluationDisciplineManager to set
     */
    @Autowired
    public void setEvaluationDisciplineManager(EvaluationDisciplineManager evaluationDisciplineManager) {
        this.evaluationDisciplineManager = evaluationDisciplineManager;
    }

    /**
     * @param eleveManager
     *            the eleveManager to set
     */
    @Autowired
    public void setEleveManager(EleveManager eleveManager) {
        this.eleveManager = eleveManager;
    }

    /**
     * @param operationProgrammeeAffectationManager
     *            the operationProgrammeeAffectationManager to set
     */
    @Autowired
    public void setOperationProgrammeeAffectationManager(
            OperationProgrammeeAffectationManager operationProgrammeeAffectationManager) {
        this.operationProgrammeeAffectationManager = operationProgrammeeAffectationManager;
    }

    @Override
    public void lancerTraitement() throws Exception {
        if (!evalNoteManager.estSansEvalNote()) {

            // Calcul des moyennes
            messages.start("Recalcul des moyennes des �l�ves");

            Long idOpa = getIdentifiantOperationProgrammeeAffectation();
            List<String> modes = operationProgrammeeAffectationManager.modeBaremeParOpa(idOpa);

            // Calcul des moyennes des notes
            if (modes.contains(ParametresFormationAccueil.MODE_BAREME_NOTE)) {
                LOG.info("Calcul des moyennes de notes");
                messages.start("Recalcul des moyennes des des notes");
                eleveManager.miseAJourMoyennesEleves();
                messages.end();
            }

            // Calcul des moyennes des champs disciplinaires
            if (modes.contains(ParametresFormationAccueil.MODE_BAREME_EVALUATION)) {
                LOG.info("Calcul des moyennes des champs disciplinaires");
                evalNoteManager.purgerChampDisciplinaireOpa(idOpa);
                calculMoyenneChampsDiciplinaireSQL();
                // calculMoyenneChampsDiciplinairePalier2nde();
            }
            messages.end();

        }
    }

    /**
     * Calcule et enregistre les moyennes des champs disciplinaire pour un �l�ve de palier 3�me via un traitement
     * HQL.
     */
    private void calculMoyenneChampsDiciplinaireSQL() {
        messages.start("Calcul des moyennes des champs disciplinaires");

        long idOpa = getIdentifiantOperationProgrammeeAffectation();
        Session session = HibernateUtil.currentSession();

        Query calculDesMoyennesAvecEvalDisc3eme = session
                .getNamedQuery("calculDesMoyennesChampDisciplinaireAvecEvalDisciplinePalier3eme");
        calculDesMoyennesAvecEvalDisc3eme.setParameter("idOpa", idOpa);
        calculDesMoyennesAvecEvalDisc3eme.executeUpdate();

        Query calculDesMoyennesAvecEvalDisc2nde = session
                .getNamedQuery("calculDesMoyennesChampDisciplinaireAvecEvalDisciplinePalier2nde");
        calculDesMoyennesAvecEvalDisc2nde.setParameter("idOpa", idOpa);
        calculDesMoyennesAvecEvalDisc2nde.executeUpdate();

        Query calculDesMoyennesManquantes = session.getNamedQuery("calculDesMoyennesChampDisciplinaireManquants");
        calculDesMoyennesManquantes.setParameter("idOpa", idOpa);
        calculDesMoyennesManquantes.executeUpdate();

        messages.end();
    }

    /**
     * Associe les champs disciplinaires et les points correspondants aux notes de l'�l�ve.
     * 
     * @param eleve
     *            L'�l�ve not�
     * @param rangParValeur
     *            La map liant les rangs et leur valeur
     * @param groupeNiveauMap
     *            Map des groupes de niveau
     * @return une map liant les champs disciplinaire et la liste des points correspondants.
     */
    protected Map<String, List<Double>> trierNotesParChamp(Eleve eleve, Map<Integer, Rang> rangParValeur,
            Map<Integer, GroupeNiveau> groupeNiveauMap) {
        Map<String, List<Double>> listPointsParChamp = new HashMap<String, List<Double>>();
        Map<Integer, Double> notes = eleve.getNotes();
        String codeChamp;
        Integer valeurPoints;

        for (Integer indiceRang : notes.keySet()) {
            if (notes.get(indiceRang) != null && rangParValeur.get(indiceRang).getChampDisciplinaire() != null) {
                codeChamp = rangParValeur.get(indiceRang).getChampDisciplinaire().getCode();
                valeurPoints = evaluationDisciplineManager.conversionEvalNumEnPoint(notes.get(indiceRang),
                        groupeNiveauMap);

                if (!listPointsParChamp.containsKey(codeChamp)) {
                    listPointsParChamp.put(codeChamp, new ArrayList<Double>());
                }
                listPointsParChamp.get(codeChamp).add(valeurPoints.doubleValue());
            }
        }
        return listPointsParChamp;
    }

    /**
     * Calcule la moyenne d'une liste de nombre en arrondissant � deux chiffres apr�s la virgule.
     * 
     * @param moyennes
     *            la liste de nombre
     * @return la moyenne (ou null si elle n'existe pas)
     */
    protected Double moyenneChamp(List<Double> moyennes) {
        if (moyennes == null || moyennes.size() == 0) {
            return null;
        }

        Double somme = 0d;
        for (Double valMoyenne : moyennes) {
            somme += valMoyenne;
        }
        return (double) (Math.round(somme * 100.0 / moyennes.size()) / 100.0);
    }
}
