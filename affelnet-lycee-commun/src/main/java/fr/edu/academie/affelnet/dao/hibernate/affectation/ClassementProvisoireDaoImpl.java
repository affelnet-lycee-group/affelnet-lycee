/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.sql.SQLException;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.batch.affectation.BatchAffectation;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.affectation.ClassementProvisoireDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.affectation.Classement;
import fr.edu.academie.affelnet.domain.affectation.DecisionCommission;
import fr.edu.academie.affelnet.domain.affectation.EntreeClassement;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.StatutEleve;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/**
 * Impl�mentation du classement provisoire des �l�ves pour une OPA.
 */
@Repository("ClassementProvisoireDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ClassementProvisoireDaoImpl extends BaseClassementDaoImpl implements ClassementProvisoireDao {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(ClassementProvisoireDaoImpl.class);

    /** Le gestionnaire d'OPA. */
    private OperationProgrammeeAffectationManager opaManager;

    /** L'OPA pour laquelle le reclassement est effectu�e. */
    private OperationProgrammeeAffectation opa;

    /** @return identifiant programm�e d'affectation contexte. */
    protected long getIdentifiantOperationProgrammeeAffectation() {
        String idOpaStr = params.get(BatchAffectation.PARAMETRE_ID_OPA);
        return Long.parseLong(idOpaStr);
    }

    /**
     * @param operationProgrammeeAffectationManager
     *            opaManager
     */
    @Autowired
    public void setOperationProgrammeeAffectationManager(
            OperationProgrammeeAffectationManager operationProgrammeeAffectationManager) {
        this.opaManager = operationProgrammeeAffectationManager;
    }

    @Override
    public void lancerTraitement() throws SQLException {

        messages.start("Classement provisoire");

        // D�termine l'OPA
        long idOpa = getIdentifiantOperationProgrammeeAffectation();
        LOG.info("Classement provisoire des �l�ves pour l'OPA " + idOpa);
        opa = opaManager.charger(idOpa);

        // Initialisation (capacites, liste des offres de formation n'utilisant pas de bar�me)
        messages.start("Initialisation des classements pour les offres de formation de l'OPA");
        initialisationClassementsParOffre();
        messages.end();

        // Initialisation de l'ensemble des offres de formation � analyser
        for (String codeVoeu : classementParOffre.keySet()) {
            codesOffresRestantATraiter.add(codeVoeu);
        }

        collecterVoeuxAClasser();

        // Int�gration de tous premiers voeux des �l�ves (de rang minimal pour l'OPA)
        messages.start("Int�gration des premiers voeux des �l�ves de l'OPA");
        integrerPremiersVoeux();
        messages.end();

        // classement des VoeuEleve
        messages.start("Classement des voeux des �l�ves");
        classementVoeuEleve();
        messages.end();

        // Enregistrement des resultats en base de donn�es
        messages.start("Enregistrement des r�sultats du classement");
        archiverResultats();
        messages.end();

        // Nettoyage de la session en cours
        messages.start("Nettoyage");
        HibernateUtil.cleanupSession();
        messages.end();

        messages.end();
        LOG.info("Classement termin�");
    }

    /**
     * Initialise les classements par offre de formation pour l'OPA.
     * 
     * Pr�pare une table associant un Classement � chaque code d'offre de formation.
     * 
     * Remarque, les offres de formation de recensement sont exclues du traitement.
     */
    private void initialisationClassementsParOffre() {
        LOG.debug("D�but d'initialisation de la table des classements par offre");
        Session session = HibernateUtil.currentSession();

        // requ�te permettant d'obtenir le nombre d'�l�ves qui sont forc�s globalement ET qui sont admis sur ce
        // voeu
        Query queryNombreElevesForcesAdmis = session
                .getNamedQuery("compterElevesForcesAdmisPourOffreFormation.select.hql");
        queryNombreElevesForcesAdmis.setParameter("numeroTour", opa.getNumeroTourSuivant().shortValue());

        // ---------------------- Offres trait�es par bar�me ----------------------------------------------
        // Collecte des informations sur les offres de formation avec prise en compte de bar�me
        // - Code
        // - Capacit� d'affectation
        // - Capacit� de liste suppl�mentaire
        Query queryCapacitesOffresAvecBaremePourOpa;

        if (opa.getType().isTourSuivant()) {
            queryCapacitesOffresAvecBaremePourOpa = session
                    .getNamedQuery("codesEtCapacitesOffresFormationAvecBaremePourOpaTourSuivant.select.hql");

        } else {
            queryCapacitesOffresAvecBaremePourOpa = session
                    .getNamedQuery("codesEtCapacitesOffresFormationAvecBaremePourOpaTourPrincipal.select.hql");
            queryCapacitesOffresAvecBaremePourOpa.setParameter("idOpa", opa.getId());
        }

        ScrollableResults scrollPam = queryCapacitesOffresAvecBaremePourOpa.scroll();
        while (scrollPam.next()) {
            Object[] infosOffre = scrollPam.get();
            // le code du voeu
            String codeVoeu = (String) infosOffre[0];

            int capacite = 0;
            int capaciteListeSupplementaire = 0;

            // Capacit� liste principale
            if (infosOffre[1] != null) {

                // R�cup�re le nombre de voeux forc�s admis sur cette offre
                queryNombreElevesForcesAdmis.setString("codeVoeu", codeVoeu);
                int nbForcesAdmis = ((Number) queryNombreElevesForcesAdmis.uniqueResult()).intValue();

                // On retire de la capacit� le nombre d'�l�ve qui sont forc�s et admis sur cette offre de
                // formation
                capacite = ((Number) infosOffre[1]).intValue() - nbForcesAdmis;
            }

            // Capacit� de la liste suppl�mentaire
            if (infosOffre[2] != null) {
                capaciteListeSupplementaire = ((Number) infosOffre[2]).intValue();
            }

            LOG.debug("Offre trait�e bar�me : " + codeVoeu + ", capa : " + capacite + ", ls : "
                    + capaciteListeSupplementaire);

            // Pr�pare un classement pour l'offre de formation
            classementParOffre.put(codeVoeu,
                    new Classement<EntreeClassement>(capacite, capaciteListeSupplementaire));
        }
        scrollPam.close();

        // ------------------- Offres trait�es en commission --------------------
        // Collecte les informations sur les offres de formation trait�es en commission
        // - Code
        // - Nombre de voeux �l�ves pris en commission
        // - Nombre de voeux �l�ves en liste suppl�mentaire en commission
        Query queryCodeCapaciteOffreCommissionPourOpa;

        if (opa.getType().isTourSuivant()) {
            queryCodeCapaciteOffreCommissionPourOpa = session
                    .getNamedQuery("codesEtCapacitesOffresFormationCommissionPourOpaTourSuivant.select.hql");
            queryCodeCapaciteOffreCommissionPourOpa.setParameter("numeroTour",
                    opa.getNumeroTourSuivant().shortValue());
        } else {
            queryCodeCapaciteOffreCommissionPourOpa = session
                    .getNamedQuery("codesEtCapacitesOffresFormationCommissionPourOpaTourPrincipal.select.hql");
            queryCodeCapaciteOffreCommissionPourOpa.setParameter("idOpa", opa.getId());
        }

        ScrollableResults scrollNonPam = queryCodeCapaciteOffreCommissionPourOpa.scroll();
        while (scrollNonPam.next()) {
            Object[] infosOffre = scrollNonPam.get();
            // code voeu
            String codeVoeu = (String) infosOffre[0];

            int capacite = 0;
            int capaciteListeSupplementaire = 0;

            // Capacit� liste principale = nombre d'�l�ve pris apr�s les commissions
            if (infosOffre[1] != null) {
                capacite = ((Number) infosOffre[1]).intValue();
            }

            // capacit� liste suppl�mentaire
            if (infosOffre[2] != null) {
                capaciteListeSupplementaire = ((Number) infosOffre[2]).intValue();
            }

            LOG.debug("Offre trait�e en commission : " + codeVoeu + ", capa : " + capacite + ", ls : "
                    + capaciteListeSupplementaire);

            // Pr�pare un classement pour l'offre de formation
            classementParOffre.put(codeVoeu,
                    new Classement<EntreeClassement>(capacite, capaciteListeSupplementaire));

        }
        scrollNonPam.close();

        LOG.debug("Fin d'initialisation de la table des classements par offre");
    }

    /**
     * Cr�e un crit�re Hibernate permettant de r�cup�rer les r�sultats correspondant aux voeux des �l�ves pour
     * l'OPA courante avec les informations utiles associ�es en fetch join.
     * 
     * @return Crit�ria Hibernate
     */
    @Override
    protected Criteria getCriteriaResultatsVoeux() {
        Session session = HibernateUtil.currentSession();

        // Collecte toutes les entr�es de r�sultat provisoires correspondant � des voeux �l�ves � classer
        // On filtre
        // * pour l'OPA concern�e
        // * pour les �l�ves non forc�s globalement
        // * pour les offres de formation non appentissage

        Criteria criteria = session.createCriteria(ResultatsProvisoiresOpa.class, "resultat");
        criteria.createAlias("voeuEleve", "voeuEleve");
        criteria.createAlias("voeuEleve.candidature", "candidature");
        criteria.createAlias("voeuEleve.voeu", "voeu");
        criteria.createAlias("voeu.statut", "statut");

        criteria.add(Restrictions.eq("resultat.opa", opa));
        criteria.add(Restrictions.eq("candidature.flagEleveForce", Flag.NON));

        criteria.add(Restrictions
                .not(Restrictions.and(Restrictions.eq("voeu.indicateurPam", TypeOffre.COMMISSION.getIndicateur()),
                        Restrictions.eq("statut.code", StatutEleve.CODE_APPRENTISSAGE))));

        return criteria;
    }

    @Override
    protected void archiverResultats() {
        LOG.debug("Archivage des r�sultats du classement de l'OPA");

        // Pour pr�parer les r�sultats, on met tout � refus� dans le doute.
        initialisationResultatsRefuses();

        for (Classement<EntreeClassement> classement : classementParOffre.values()) {

            enregistrerElevesPris(classement.getListePrincipale());
            enregistrerListesSupplementaires(classement.getListeSupplementaire());

        }

        reporterDecisionsCommission();
        reporterForcagesGlobaux();

    }

    /** Reporte les d�cisions sp�cifiques de commission dans les r�sultats provisoires (absent ou non trait�). */
    private void reporterDecisionsCommission() {
        messages.start("Traitement des d�cisions sp�cifiques prises en commissions");

        // Reporte les d�cisions prises en commission sur les voeux d'�l�ves vers les d�cisions des
        // r�sultats provisoires
        Session session = HibernateUtil.currentSession();
        Query q = session.getNamedQuery("reporterDecisionCommissionResultatsProvisoires.update.hql");
        q.setParameter("idOpa", opa.getId());

        // Non-trait�
        q.setParameter("codeDecisionProvisoire", ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_NON_TRAITE);
        q.setParameter("codeDecisionCommission", DecisionCommission.NON_TRAITE.getCode());
        q.executeUpdate();

        // Dossier-absent
        q.setParameter("codeDecisionProvisoire", ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_DOSSIER_ABSENT);
        q.setParameter("codeDecisionCommission", DecisionCommission.DOSSIER_ABSENT.getCode());
        q.executeUpdate();

        // En attente de signature du contrat
        q.setParameter("codeDecisionProvisoire",
                ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_EN_ATTENTE_SIGNATURE_CONTRAT);
        q.setParameter("codeDecisionCommission", DecisionCommission.EN_ATTENTE_SIGNATURE_CONTRAT.getCode());
        q.executeUpdate();

        // Admis, contrat sign�
        q.setParameter("codeDecisionProvisoire",
                ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_ADMIS_CONTRAT_SIGNE);
        q.setParameter("codeDecisionCommission", DecisionCommission.ADMIS_CONTRAT_SIGNE.getCode());
        q.executeUpdate();

        messages.end();
    }

    /** Pour pr�parer les r�sultats, par s�curit�, on met tout � refus� tous les r�sultats provisoires de l'OPA. */
    private void initialisationResultatsRefuses() {

        Session session = HibernateUtil.currentSession();
        Query q = session.getNamedQuery("archiverResultatsProvisoiresInitialisationRefuse.update.hql");
        q.setParameter("idOpa", opa.getId());
        q.executeUpdate();
    }

    /** Reporte les d�cisions des voeux sur les r�sultats provisoires pour les �l�ves forc�s globalement. */
    private void reporterForcagesGlobaux() {

        Session session = HibernateUtil.currentSession();
        Query q = session.getNamedQuery("archiverResultatsProvisoiresReporterForcagesGlobaux.update.hql");
        q.setParameter("idOpa", opa.getId());
        q.executeUpdate();
    }

    /**
     * Enregistre les r�sultats pour les �l�ves affect�s provisoirement.
     * 
     * @param entrees
     *            entr�es de classement affect�es provisoirement
     */
    private void enregistrerElevesPris(Set<EntreeClassement> entrees) {

        Session session = HibernateUtil.currentSession();
        Query q = session.getNamedQuery("archiverResultatsProvisoiresAffectesOuLS.update.hql");
        q.setParameter("idOpa", opa.getId());

        for (EntreeClassement entree : entrees) {

            q.setParameter("ine", entree.getIne());
            q.setParameter("rang", entree.getRang());

            q.setParameter("codeDecisionProvisoire", ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_AFFECTE);
            q.setParameter("numeroListeSupp", null);

            q.executeUpdate();
        }
    }

    /**
     * Enregistre les r�sultats pour les �l�ves en liste suppl�mentaire.
     * 
     * @param entrees
     *            entr�es de classement en liste compl�mentaires
     */
    private void enregistrerListesSupplementaires(Set<EntreeClassement> entrees) {

        Session session = HibernateUtil.currentSession();
        Query q = session.getNamedQuery("archiverResultatsProvisoiresAffectesOuLS.update.hql");
        q.setParameter("idOpa", opa.getId());

        int numeroListeSupp = 1;

        for (EntreeClassement entree : entrees) {

            q.setParameter("ine", entree.getIne());
            q.setParameter("rang", entree.getRang());

            q.setParameter("codeDecisionProvisoire",
                    ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_LISTE_SUPPLEMENTAIRE);
            q.setParameter("numeroListeSupp", (short) numeroListeSupp++);

            q.executeUpdate();
        }
    }

}
