/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.affectation.EvaluationChampDisciplinaireOpaDao;
import fr.edu.academie.affelnet.domain.affectation.EvaluationChampDisciplinaireOpa;
import fr.edu.academie.affelnet.domain.affectation.EvaluationChampDisciplinaireOpaPK;

/**
 * Dao pour les moyennes des �valuations liss�s ou non des champs disciplinaire.
 */
@Repository("EvaluationChampDisciplinaireOpaDaoImpl")
public class EvaluationChampDisciplinaireOpaDaoImpl
        extends BaseDaoHibernate<EvaluationChampDisciplinaireOpa, EvaluationChampDisciplinaireOpaPK>
        implements EvaluationChampDisciplinaireOpaDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        List<Tri> tri = new ArrayList<Tri>();
        tri.add(Tri.asc("opa"));
        tri.add(Tri.asc("eleve"));
        tri.add(Tri.asc("champDisciplinaire"));
        return tri;
    }

    @Override
    protected String getMessageObjectNotFound(EvaluationChampDisciplinaireOpaPK key) {
        return "La moyenne des champs dsiciplinaire pour l'�l�ve " + key.getIne()
                + " n'a pas �t� trouv�e pour le champs disciplinaire " + key.getCodeChampDisciplinaire()
                + " pour l'OPA d'id " + key.getIdOpa();
    }

    @Override
    protected void setKey(EvaluationChampDisciplinaireOpa object, EvaluationChampDisciplinaireOpaPK key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(EvaluationChampDisciplinaireOpa object,
            EvaluationChampDisciplinaireOpaPK oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "L'objet existe d�j�.";
    }

    @Override
    protected Class<EvaluationChampDisciplinaireOpa> getObjectClass() {
        return EvaluationChampDisciplinaireOpa.class;
    }

    @Override
    public void purgerChampDisciplinaireOpa(Long idOpa) {
        try {
            Session session = getSession();
            String hql = "delete from EvaluationChampDisciplinaireOpa e " + "where e.id.idOpa = :idOpa";
            session.createQuery(hql).setParameter("idOpa", idOpa).executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void purgerChampDisciplinaireOpaEleve(String ine) {
        try {
            Session session = getSession();
            String hql = "delete from EvaluationChampDisciplinaireOpa e where e.id.ine = :ine";
            session.createQuery(hql).setParameter("ine", ine).executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
