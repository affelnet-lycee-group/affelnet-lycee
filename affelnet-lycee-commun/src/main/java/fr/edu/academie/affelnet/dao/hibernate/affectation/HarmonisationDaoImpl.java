/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.pam.BatchOpaDaoImpl;
import fr.edu.academie.affelnet.dao.interfaces.affectation.HarmonisationDao;
import fr.edu.academie.affelnet.domain.affectation.EvaluationComplementaireOpa;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.EvalNoteManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationComplementaireManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationDisciplineManager;

/** Implementation du traitement de lissage des notes. */
@Repository("HarmonisationgeDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HarmonisationDaoImpl extends BatchOpaDaoImpl implements HarmonisationDao {

    /** Valeur minimale possible pour les notes liss�es. */
    private static final int NOTE_LISSEE_MINIMALE = 0;

    /** Valeur maximale possible pour les notes liss�es. */
    private static final double NOTE_LISSEE_MAXIMALE = 999.999;

    /** Nombre d'it�rations avant un flush de session Hibernate. */
    private static final int NB_ITERATIONS_FLUSH = 20;

    /** Loggeur de la classe. */
    private static final Log LOG = LogFactory.getLog(HarmonisationDaoImpl.class);

    /**
     * Le manager utilis� pour les �valuations/notes.
     */
    private EvalNoteManager evalNoteManager;

    /** Le manager pour les �valuations compl�mentaires. */
    private EvaluationComplementaireManager evaluationComplementaireManager;

    /**
     * @param evalNoteManager
     *            the evalNoteManager to set
     */
    @Autowired
    public void setEvalNoteManager(EvalNoteManager evalNoteManager) {
        this.evalNoteManager = evalNoteManager;
    }

    /**
     * @param evaluationDisciplineManager
     *            the evaluationDisciplineManager to set
     */
    @Autowired
    public void setEvaluationDisciplineManager(EvaluationDisciplineManager evaluationDisciplineManager) {
    }

    /**
     * @param campagneAffectationManager
     *            Le gestionnaire de campagne d'affectation
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
    }

    /**
     * @param evaluationComplementaireManager
     *            the evaluationComplementaireManager to set
     */
    @Autowired
    public void setEvaluationComplementaireManager(
            EvaluationComplementaireManager evaluationComplementaireManager) {
        this.evaluationComplementaireManager = evaluationComplementaireManager;
    }

    @Override
    public void lancerTraitement() {
        // Le lissage n'est pas n�cessaire
        // s'il n'existe aucune offre de formation de type 'bar�me avec �valuations/notes'
        if (!evalNoteManager.estSansEvalNote()) {
            long idOpa = getIdentifiantOperationProgrammeeAffectation();
            // lissage par groupe
            messages.start("Harmonisation des �valuations et des notes par groupe");
            List<String> modes = operationProgrammeeAffectationManager.modeBaremeParOpa(idOpa);

            // Harmonisation des �valuations
            if (modes.contains(ParametresFormationAccueil.MODE_BAREME_EVALUATION)) {
                lisserEvalParGroupe();
            }

            // Harmonisation des notes
            if (modes.contains(ParametresFormationAccueil.MODE_BAREME_NOTE)) {
                lisserNotesSql();
            }
            messages.end();

            // Nettoyage de la session en cours
            HibernateUtil.cleanupSession();
        }
    }

    /**
     * Harmonise les notes via une requ�te SQL.
     */
    private void lisserNotesSql() {
        messages.start("Harmonisation des notes");

        long idOpa = getIdentifiantOperationProgrammeeAffectation();
        Session session = HibernateUtil.currentSession();

        Query harmonisationNote = session.getNamedQuery("harmonisationNote.merge.sql");
        harmonisationNote.setParameter("idOpa", idOpa);
        harmonisationNote.executeUpdate();
        messages.end();
    }

    /**
     * Harmonise les pints des �l�ves pour les �valuations compl�mentaire et des champs disciplinaires.
     */
    private void lisserEvalParGroupe() {
        lisserChampDisciplinaireParGroupeSQL();
        lisserEvaluationComplementaireParGroupe();
    }

    /**
     * Harmonise les �valuations des champs disciplinaires via une requ�te SQL.
     */
    private void lisserChampDisciplinaireParGroupeSQL() {
        messages.start("Harmonisation des �valuations des champs disciplinaires");

        long idOpa = getIdentifiantOperationProgrammeeAffectation();
        Session session = HibernateUtil.currentSession();

        Query armonisationEvaluationChampDisciplinaire = session
                .getNamedQuery("harmonisationEvaluationChampDisciplinaire.merge.sql");
        armonisationEvaluationChampDisciplinaire.setParameter("idOpa", idOpa);
        armonisationEvaluationChampDisciplinaire.executeUpdate();
        messages.end();
    }

    /**
     * G�n�re les statistiques et enregistre en base de donn�es les �valuations de disciplines liss�es.
     */
    private void lisserEvaluationComplementaireParGroupe() {
        Map<Long, Map<String, StatistiquesHarmonisationGroupe>> mapStatParEvalComp = calculStatParEvalCompHQL();
        harmonisationEvaluationComplementaire(mapStatParEvalComp);
    }

    /**
     * Calcule les statistiques par �valuation compl�mentaire et par groupe origine via une requ�te SQL.
     * 
     * @return Les �tats statistiques en fonction de l'�valuation compl�mentaire et du groupe origine
     */
    @SuppressWarnings("unchecked")
    private Map<Long, Map<String, StatistiquesHarmonisationGroupe>> calculStatParEvalCompHQL() {
        String hql = "select eval.evaluationComplementaire.id,  "
                + " eval.eleve.groupeOrigine.code, avg(cast(eval.echelon.valeurPoint as double)),"
                + " STDDEV(cast(eval.echelon.valeurPoint as double))  "
                + "from fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve as eval "
                + " where eval.eleve.groupeOrigine is not null "
                + " group by eval.evaluationComplementaire.id, eval.eleve.groupeOrigine.code"
                + " order by eval.evaluationComplementaire.id, eval.eleve.groupeOrigine.code";

        List<Object[]> listStats = HibernateUtil.currentSession().createQuery(hql).list();

        Map<Long, Map<String, StatistiquesHarmonisationGroupe>> statParChampEtPargroupe = new HashMap<Long, Map<String, StatistiquesHarmonisationGroupe>>();

        for (Object[] statsBrut : listStats) {
            Long idEvalComp = ((Number) statsBrut[0]).longValue();
            StatistiquesHarmonisationGroupe stat = new StatistiquesHarmonisationGroupe((String) statsBrut[1],
                    (Double) statsBrut[2], (Double) statsBrut[3]);
            if (!statParChampEtPargroupe.containsKey(idEvalComp)) {
                statParChampEtPargroupe.put(idEvalComp, new HashMap<String, StatistiquesHarmonisationGroupe>());
            }

            statParChampEtPargroupe.get(idEvalComp).put(stat.getCodeGroupe(), stat);
        }

        return statParChampEtPargroupe;
    }

    /**
     * Harmonise les �valuations compl�mentaires des �l�ves.
     * A l'aide les statistiques des �valuations des �valuations compl�mentaires par groupe origin, calcule et
     * enregistre les points harmonis�es des �l�ves.
     * 
     * @param mapStatParEvalComp
     *            Map contenant les statistiques des �valuations en fonction des �valuations compl�mentaires et des
     *            groupes origine
     */
    private void harmonisationEvaluationComplementaire(
            Map<Long, Map<String, StatistiquesHarmonisationGroupe>> mapStatParEvalComp) {
        messages.start("Harmonisation des points des �l�ves pour les �valuations compl�mentaires");
        int nbAtraiter = evaluationComplementaireManager.compteEvaluationComplementaireAvecGroupeOrigine();
        ScrollableResults evalCompScroll = evaluationComplementaireManager
                .scrollEvaluationComplementaireAvecGroupeOrigine();

        long idOpa = getIdentifiantOperationProgrammeeAffectation();
        evalNoteManager.purgerEvaluationCompementaireOpa(idOpa);

        EvaluationComplementaireEleve evalCompEleve;
        EvaluationComplementaireOpa evalCompOpa;
        EvaluationComplementaire evalComp;
        Eleve eleve;
        Double points;
        GroupeOrigine groupe;
        Double moyenne;
        Double ecartType;
        int nbTraite = 0;

        while (evalCompScroll.next()) {
            evalCompEleve = (EvaluationComplementaireEleve) evalCompScroll.get(0);
            eleve = evalCompEleve.getEleve();
            evalComp = evalCompEleve.getEvaluationComplementaire();
            points = evalCompEleve.getPoints();
            groupe = evalCompEleve.getEleve().getGroupeOrigine();
            if (groupe != null) {
                moyenne = mapStatParEvalComp.get(evalComp.getId()).get(groupe.getCode()).getMoyenne();
                ecartType = mapStatParEvalComp.get(evalComp.getId()).get(groupe.getCode()).getEcartType();

                evalCompOpa = new EvaluationComplementaireOpa(eleve, idOpa, evalComp);

                evalCompOpa.setValeurPointsHarmo(getValeurHarmo(points, moyenne, ecartType));
                evalNoteManager.creerEvaluationComplementaireOpa(evalCompOpa);
            } else {
                LOG.info("L'�l�ve " + eleve + " n'a pas de groupe origine");
            }

            if (nbTraite % NB_ITERATIONS_FLUSH == 0) {
                HibernateUtil.cleanupSession();
            }

            messages.setAvancement(++nbTraite, nbAtraiter);
        }
        messages.end();
    }

    /**
     * Harmonise les points ou les notes des �l�ves en fonction de la moyenne et de l'�cart type du groupe.
     * 
     * @param points
     *            les points ou la note
     * @param moyenne
     *            la moyenne
     * @param ecartType
     *            l'�cart type
     * @return la note ou les points harmonis�s par 10
     */
    public static double getValeurHarmo(double points, double moyenne, double ecartType) {
        double result = 10.0;
        if (ecartType != 0.0) {
            result += ((points - moyenne) / ecartType);
            result = Math.max(result, NOTE_LISSEE_MINIMALE);
            result = Math.min(result, NOTE_LISSEE_MAXIMALE);
        }
        return result * 10;
    }

}
