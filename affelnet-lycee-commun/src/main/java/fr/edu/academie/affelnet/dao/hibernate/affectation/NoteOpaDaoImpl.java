/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.affectation.NoteOpaDao;
import fr.edu.academie.affelnet.domain.affectation.NoteOpa;
import fr.edu.academie.affelnet.domain.affectation.NoteOpaPK;

/**
 * Dao pour les moyennes des �valuations liss�s ou non des champs disciplinaire.
 */
@Repository("noteOpaDaoImpl")
public class NoteOpaDaoImpl extends BaseDaoHibernate<NoteOpa, NoteOpaPK> implements NoteOpaDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        List<Tri> tri = new ArrayList<Tri>();
        tri.add(Tri.asc("opa"));
        tri.add(Tri.asc("eleve"));
        tri.add(Tri.asc("rang"));
        return tri;
    }

    @Override
    protected String getMessageObjectNotFound(NoteOpaPK key) {
        return "La note liss�e l'�l�ve " + key.getIne() + " n'a pas �t� trouv�e pour le rang " + key.getIdRang()
                + " pour l'OPA d'id " + key.getIdOpa();
    }

    @Override
    protected void setKey(NoteOpa object, NoteOpaPK key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(NoteOpa object, NoteOpaPK oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "L'objet existe d�j�.";
    }

    @Override
    protected Class<NoteOpa> getObjectClass() {
        return NoteOpa.class;
    }

    @Override
    public void purger(Long idOpa) {
        try {
            Session session = getSession();
            String hql = "delete from NoteOpa n where n.id.idOpa = :idOpa";
            session.createQuery(hql).setParameter("idOpa", idOpa).executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void purgerEleve(String ine) {
        try {
            Session session = getSession();
            String hql = "delete from NoteOpa n where n.id.ine = :ine";
            session.createQuery(hql).setParameter("ine", ine).executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
