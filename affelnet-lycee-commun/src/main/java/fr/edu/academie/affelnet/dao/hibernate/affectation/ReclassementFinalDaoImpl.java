/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.affectation.ReclassementFinalDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.affectation.Classement;
import fr.edu.academie.affelnet.domain.affectation.DecisionCommission;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;
import fr.edu.academie.affelnet.domain.affectation.EntreeClassement;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.StatutEleve;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.statistiques.EtatStatistiqueManager;

/**
 * Impl�mentation du reclassement final des voeux des �l�ves sur les offres de formation.
 */
@Repository("ReclassementFinalDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ReclassementFinalDaoImpl extends BaseClassementDaoImpl implements ReclassementFinalDao {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(ReclassementFinalDaoImpl.class);

    /** Le gestionnaire de la campagne d'affectation. */
    private CampagneAffectationManager campagneAffectationManager;

    /** Le gestionnaire des �tats statistiques. */
    private EtatStatistiqueManager etatStatistiqueManager;

    /** Le num�ro du tour pour lequel s'effectue le reclassement final. */
    private short numeroTour;

    /**
     * @param campagneAffectationManager
     *            Le gestionnaire de la campagne d'affectation
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneAffectationManager = campagneAffectationManager;
    }

    /**
     * @param etatStatistiqueManager
     *            the etatStatistiqueManager to set
     */
    @Autowired
    public void setEtatStatistiqueManager(EtatStatistiqueManager etatStatistiqueManager) {
        this.etatStatistiqueManager = etatStatistiqueManager;
    }

    @Override
    public void lancerTraitement() {
        try {

            messages.start("Reclassement final");

            // D�termine le tour concern�
            numeroTour = campagneAffectationManager.getNumeroTourCourant();
            LOG.info("D�but du reclassement final pour le tour : " + numeroTour);

            // Initialisation (capacites, liste des offres de formation n'utilisant pas de bar�me)
            messages.start("Initialisation des classements pour les offres de formation du tour");
            initialisationClassementsParOffre();
            messages.end();

            // Initialisation de l'ensemble des offres de formation � analyser
            for (String codeVoeu : classementParOffre.keySet()) {
                codesOffresRestantATraiter.add(codeVoeu);
            }

            collecterVoeuxAClasser();

            // Int�gration de tous premiers voeux des �l�ves (de rang minimal pour le tour)
            messages.start("Int�gration des premiers voeux des �l�ves du tour");
            integrerPremiersVoeux();
            messages.end();

            // classement des VoeuEleve
            messages.start("Classement des voeux des �l�ves");
            classementVoeuEleve();
            messages.end();

            // Enregistrement des resultats en base de donn�es
            messages.start("Enregistrement des r�sultats du classement");
            archiverResultats();
            messages.end();

            // Actualiser etat applicatif suite au reclassement
            messages.start("Actualisation de l'�tat applicatif suite au reclassement");
            campagneAffectationManager.actualiserEtatSuiteReclassementFinal();
            messages.end();

            // Nettoyage de la session et r�initialisation des infos sur les �tats statistiques
            messages.start("Nettoyage");
            etatStatistiqueManager.reinitialiserEtatsStatistiques();
            HibernateUtil.cleanupSession();
            messages.end();

        } catch (SQLException e) {
            throw new DaoException(e);
        }

        messages.end();
        LOG.info("Reclassement termin�");
    }

    /**
     * Initialise les classements par offre de formation.
     * 
     * Pr�pare une table associant un Classement � chaque code d'offre de formation.
     */
    @SuppressWarnings("unchecked")
    private void initialisationClassementsParOffre() {
        LOG.debug("D�but d'initialisation de la table des classements par offre");
        Session session = HibernateUtil.currentSession();

        // requ�te permettant d'obtenir le nombre d'�l�ve qui sont forc�s globalement ET qui sont admis sur ce voeu
        Query queryNombreElevesForcesAdmis = session
                .getNamedQuery("compterElevesForcesAdmisPourOffreFormation.select.hql");
        queryNombreElevesForcesAdmis.setParameter("numeroTour", numeroTour);

        // On liste les offres de formation de recensement afin de mettre
        // artificiellement la capacit� � 0 (pour ne pas passer dans le reclassement)
        Query queryCodesOffresFormationRecensement = session
                .getNamedQuery("codesOffresFormationRecensement.select.hql");
        List<String> codesVoeuRecensement = queryCodesOffresFormationRecensement.list();

        // ---------------------- Offres trait�es par bar�me ----------------------------------------------
        // Collecte des informations sur les offres de formation avec prise en compte de bar�me
        // - Code
        // - Capacit� d'affectation
        // - Capacit� de liste suppl�mentaire
        Query queryCapacitesOffresAvecBareme = session
                .getNamedQuery("codesEtCapacitesOffresFormationAvecBareme.select.hql");
        ScrollableResults scrollPam = queryCapacitesOffresAvecBareme.scroll();
        while (scrollPam.next()) {
            Object[] infosOffre = scrollPam.get();
            // le code du voeu
            String codeVoeu = (String) infosOffre[0];

            int capacite = 0;
            int capaciteListeSupplementaire = 0;

            // Les capacit�s ne sont pas prises en compte et restent � 0 pour les voeux de recensement
            if (!codesVoeuRecensement.contains(codeVoeu)) {

                // Capacit� liste principale
                if (infosOffre[1] != null) {

                    // R�cup�re le nombre de voeux forc�s admis sur cette offre
                    queryNombreElevesForcesAdmis.setString("codeVoeu", codeVoeu);
                    int nbForcesAdmis = ((Number) queryNombreElevesForcesAdmis.uniqueResult()).intValue();

                    // On retire de la capacit� le nombre d'�l�ve qui sont forc�s et admis sur cette offre de
                    // formation
                    capacite = ((Number) infosOffre[1]).intValue() - nbForcesAdmis;
                }

                // Capacit� de la liste suppl�mentaire
                if (infosOffre[2] != null) {
                    capaciteListeSupplementaire = ((Number) infosOffre[2]).intValue();
                }
            }

            LOG.debug("Offre trait�e bar�me : " + codeVoeu + ", capa : " + capacite + ", ls : "
                    + capaciteListeSupplementaire);

            // Pr�pare un classement pour l'offre de formation
            classementParOffre.put(codeVoeu,
                    new Classement<EntreeClassement>(capacite, capaciteListeSupplementaire));
        }
        scrollPam.close();

        // ------------------- Offres trait�es en commission --------------------
        // Collecte les informations sur les offres de formation trait�es en commission
        // - Code
        // - Nombre de voeux �l�ves pris en commission
        // - Nombre de voeux �l�ves en liste suppl�mentaire en commission
        Query queryCodeCapaciteOffreCommission = session
                .getNamedQuery("codesEtCapacitesOffresFormationCommissionScolaire.select.hql");
        queryCodeCapaciteOffreCommission.setParameter("numeroTour", numeroTour);

        ScrollableResults scrollNonPam = queryCodeCapaciteOffreCommission.scroll();
        while (scrollNonPam.next()) {
            Object[] infosOffre = scrollNonPam.get();
            // code voeu
            String codeVoeu = (String) infosOffre[0];

            int capacite = 0;
            int capaciteListeSupplementaire = 0;

            // Les capacit�s ne sont pas prises en compte et restent � 0 pour les voeux de recensement
            if (!codesVoeuRecensement.contains(codeVoeu)) {

                // Capacit� liste principale = nombre d'�l�ve pris apr�s les commissions
                if (infosOffre[1] != null) {
                    capacite = ((Number) infosOffre[1]).intValue();
                }

                // capacit� liste suppl�mentaire
                if (infosOffre[2] != null) {
                    capaciteListeSupplementaire = ((Number) infosOffre[2]).intValue();
                }

            }

            LOG.debug("Offre trait�e en commission : " + codeVoeu + ", capa : " + capacite + ", ls : "
                    + capaciteListeSupplementaire);

            // Pr�pare un classement pour l'offre de formation
            classementParOffre.put(codeVoeu,
                    new Classement<EntreeClassement>(capacite, capaciteListeSupplementaire));

        }
        scrollNonPam.close();

        LOG.debug("Fin d'initialisation de la table des classements par offre");
    }

    /**
     * Cr�e un crit�re Hibernate permettant de r�cup�rer les r�sultats provisoires
     * correspondant aux voeux des �l�ves.
     * <ul>
     * <li>pour le num�ro du tour courant</li>
     * <li>avec les informations utiles associ�es en fetch join</li>
     * </ul>
     * 
     * @return Crit�ria Hibernate
     */
    @Override
    protected Criteria getCriteriaResultatsVoeux() {
        Session session = HibernateUtil.currentSession();

        // Collecte toutes les entr�es de r�sultat provisoires correspondant � des voeux �l�ves � classer
        // On filtre
        // * pour le tour concern�
        // * pour les �l�ves non forc�s globalement.
        // * pour les offres de formation non appentissage
        
        Criteria criteria = session.createCriteria(ResultatsProvisoiresOpa.class, "resultat");
        criteria.createAlias("voeuEleve", "voeuEleve");
        criteria.createAlias("voeuEleve.candidature", "candidature");
        criteria.createAlias("opa", "opa");
        criteria.createAlias("voeuEleve.voeu", "voeu");
        criteria.createAlias("voeu.statut", "statut");
        
        criteria.add(Restrictions.eq("id.numeroTour", numeroTour));
        criteria.add(Restrictions.eq("candidature.flagEleveForce", Flag.NON));
        
        criteria.add(Restrictions
                .not(Restrictions.and(Restrictions.eq("voeu.indicateurPam", TypeOffre.COMMISSION.getIndicateur()),
                        Restrictions.eq("statut.code", StatutEleve.CODE_APPRENTISSAGE))));
        
        return criteria;
    }

    @Override
    protected void archiverResultats() throws SQLException {
        // On initialise les r�sultats sans d�cision finale pour les voeux/candidatures non forc�s
        initialisationResultatsNull();

        // Traitement des d�cisions de recensement
        enregistrerDecisionsRecensement();

        // Traitement des voeux apprentissage.
        miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission.ADMIS_CONTRAT_SIGNE);
        miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission.EN_ATTENTE_SIGNATURE_CONTRAT);
        miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission.REFUSE);
        // Les d�cisions "non trait�s" et "dossier absent" aboutissent sur une candidature refus�e si il n'y a pas
        // eu de meilleure d�cision d'apprentissage
        miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission.NON_TRAITE, DecisionFinale.REFUSE);
        miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission.DOSSIER_ABSENT, DecisionFinale.REFUSE);

        // Traitement g�n�ral des voeux ---
        enregistrerListesSupplementaires();
        enregistrerElevesPris();
        enregistrerElevesRefuses();

        // Positionne les d�cisions finales sp�cifiques aux offres g�r�es en commissions (absent ou non trait�).
        messages.start("Traitement des d�cisions sp�cifiques aux commissions (dossiers absents, non trait�s");
        miseAjourDecisionFinaleVoeuSpecifiqueCommission(DecisionCommission.DOSSIER_ABSENT);
        miseAjourDecisionFinaleVoeuSpecifiqueCommission(DecisionCommission.NON_TRAITE);
    }

    /**
     * Initialise les r�sultats finaux � null sur les voeux et les candidatures du tour.
     */
    private void initialisationResultatsNull() {

        LOG.debug("Initialisation des r�sultats � refus�");

        Session session = HibernateUtil.currentSession();

        Query queryInitialisationVoeuxRefuses = session
                .getNamedQuery("archiverResultatsFinauxInitialisationVoeuxNull.update.sql");
        queryInitialisationVoeuxRefuses.setParameter("numeroTour", numeroTour);
        queryInitialisationVoeuxRefuses.executeUpdate();

        Query queryInitialisationElevesRefuses = session
                .getNamedQuery("archiverResultatsFinauxInitialisationElevesNull.update.sql");
        queryInitialisationElevesRefuses.setParameter("numeroTour", numeroTour);
        queryInitialisationElevesRefuses.executeUpdate();
    }


    /**
     * Attribue � les d�cisions finales sp�cifiques aux voeux en apprentissage g�r�s en commission.
     * 
     * @param decisionCommission
     *            la d�cision prise en commission
     * @param codeDecisionFinale
     *            le code de la d�cision finale � attribuer
     */
    private void miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission decisionCommission) {
        miseAjourDecisionFinaleCommissionApprentissage(decisionCommission,
                decisionCommission.getDecisionFinaleCorrespondante());
    }

    /**
     * Attribue � les d�cisions finales sp�cifiques aux voeux en apprentissage g�r�s en commission.
     * 
     * @param decisionCommission
     *            la d�cision prise en commission
     * @param decisionCandidature
     *            la d�cision � mettre sur la candidature
     * @param codeDecisionFinale
     *            le code de la d�cision finale � attribuer
     */
    private void miseAjourDecisionFinaleCommissionApprentissage(DecisionCommission decisionCommission,
            DecisionFinale decisionCandidature) {
        Session session = HibernateUtil.currentSession();

        Query queryCandidatureCommission = session
                .getNamedQuery("archiverResultatsCandidaturesApprentissage.update.sql");
        queryCandidatureCommission.setParameter("numeroTour", numeroTour);
        queryCandidatureCommission.setParameter("codeDecisionFinale", decisionCandidature.getCode());
        queryCandidatureCommission.setParameter("codeDecisionCommission", decisionCommission.getCode());
        queryCandidatureCommission.executeUpdate();

        miseAjourDecisionFinaleVoeuSpecifiqueCommission(decisionCommission);
    }

    /**
     * Attribue � les d�cisions finales sp�cifiques aux voeux g�r�s en commission.
     * 
     * @param decisionCommission
     *            la d�cision prise en commission
     * @param codeDecisionFinale
     *            le code de la d�cision finale � attribuer
     */
    private void miseAjourDecisionFinaleVoeuSpecifiqueCommission(DecisionCommission decisionCommission) {
        int codeDecisionFinale = decisionCommission.getDecisionFinaleCorrespondante().getCode();
        LOG.debug("Attribution des d�cisions sp�cifiques de commission " + decisionCommission + " -> "
                + codeDecisionFinale);

        Session session = HibernateUtil.currentSession();

        Query queryDecisionsSpecifiques = session
                .getNamedQuery("archiverResultatsFinauxDecisionSpecifiqueCommission.update.sql");
        queryDecisionsSpecifiques.setParameter("numeroTour", numeroTour);
        queryDecisionsSpecifiques.setParameter("codeDecisionFinale", codeDecisionFinale);
        queryDecisionsSpecifiques.setParameter("codeDecisionCommission", decisionCommission.getCode());
        queryDecisionsSpecifiques.executeUpdate();
    }

    /**
     * Positionne les d�cisions finales concernant les offres de recensement.
     * met � jour tous les voeux concern�s et les �l�ves s'ils n'ont fait que des voeux de recensement.
     */
    private void enregistrerDecisionsRecensement() {
        messages.start("Traitement des voeux sur les offres de formation de recensement");

        Session session = HibernateUtil.currentSession();

        // On met � les d�cisions finales et de reclassement � 'recensement' pour
        // tous les voeux sur une offre de formation de recensement, pour les �l�ves non forc�s
        Query queryUpdateDecisionsRecensementVoeux = session
                .getNamedQuery("archiverResultatsFinauxDecisionRecensementVoeux.update.sql");
        queryUpdateDecisionsRecensementVoeux.setParameter("numeroTour", numeroTour);
        queryUpdateDecisionsRecensementVoeux.executeUpdate();

        // On attribue la d�cision finale 'recensement'
        // aux les �l�ves qui n'ont fait que des voeux de recensement
        Query updateDecisionsRecensementEleves = session
                .getNamedQuery("archiverResultatsFinauxDecisionRecensementEleves.update.sql");
        updateDecisionsRecensementEleves.setParameter("numeroTour", numeroTour);
        updateDecisionsRecensementEleves.executeUpdate();

        messages.end();
    }

    /**
     * Enregistre les r�sultats pour les �l�ves en liste suppl�mentaire.
     * 
     * @throws SQLException
     *             en cas d'erreur SQL
     */
    private void enregistrerListesSupplementaires() {

        messages.start("Traitement des �l�ves en listes suppl�mentaires");

        Session session = HibernateUtil.currentSession();

        Query queryVoeuEleveEnLs = session.getNamedQuery("archiverResultatsFinauxVoeuxListeSup.update.sql");
        Query queryEleveEnLs = session.getNamedQuery("archiverResultatsFinauxElevesListeSup.update.sql");

        for (Map.Entry<String, Classement<EntreeClassement>> classementPourVoeu : classementParOffre.entrySet()) {

            TreeSet<EntreeClassement> entreesClassementListeSup = classementPourVoeu.getValue()
                    .getListeSupplementaire();
            int classement = 0;

            for (EntreeClassement entreeClassementListeSup : entreesClassementListeSup) {

                String ine = entreeClassementListeSup.getIne();
                int rang = entreeClassementListeSup.getRang();

                // VoeuEleve
                classement++;
                queryVoeuEleveEnLs.setShort("numeroTour", numeroTour);
                queryVoeuEleveEnLs.setInteger("numListeSup", classement);
                queryVoeuEleveEnLs.setString("ine", ine);
                queryVoeuEleveEnLs.setInteger("rang", rang);
                queryVoeuEleveEnLs.executeUpdate();

                // Eleve
                queryEleveEnLs.setString("ine", ine);
                queryEleveEnLs.setShort("numeroTour", numeroTour);
                queryEleveEnLs.executeUpdate();
            }
        }

        messages.end();
    }

    /**
     * Enregistre les r�sultats pour les �l�ves pris.
     * 
     * @throws SQLException
     *             en cas d'erreur SQL
     */
    private void enregistrerElevesPris() {

        messages.start("Traitement des �l�ves pris");

        Session session = HibernateUtil.currentSession();

        Query queryVoeuElevePris = session.getNamedQuery("archiverResultatsFinauxVoeuElevePris.update.sql");
        Query queryElevePris = session.getNamedQuery("archiverResultatsFinauxElevePris.update.sql");

        for (Map.Entry<String, Classement<EntreeClassement>> classementPourOffre : classementParOffre.entrySet()) {

            TreeSet<EntreeClassement> entreesClassementPris = classementPourOffre.getValue().getListePrincipale();

            for (EntreeClassement entreeClassementPris : entreesClassementPris) {

                String ine = entreeClassementPris.getIne();
                int rang = entreeClassementPris.getRang();

                // VoeuEleve
                queryVoeuElevePris.setShort("numeroTour", numeroTour);
                queryVoeuElevePris.setString("ine", ine);
                queryVoeuElevePris.setInteger("rang", rang);
                queryVoeuElevePris.executeUpdate();

                // Eleve
                queryElevePris.setInteger("rang", rang);
                queryElevePris.setString("ine", ine);
                queryElevePris.setShort("numeroTour", numeroTour);
                queryElevePris.executeUpdate();
            }
        }

        messages.end();
    }

    /**
     * Enregistre les r�sultats finaux des voeux et des candidatures refus�s suite au classement des voeux.
     */
    private void enregistrerElevesRefuses() {
        Session session = HibernateUtil.currentSession();

        Query queryVoeuxRefuses = session
                .getNamedQuery("archiverResultatsVoeuxRefusesRestants.update.sql");
        queryVoeuxRefuses.setParameter("numeroTour", numeroTour);
        queryVoeuxRefuses.executeUpdate();

        Query queryCandidaturesRefuses = session
                .getNamedQuery("archiverResultatsCandidaturesRefusesRestants.update.sql");
        queryCandidaturesRefuses.setParameter("numeroTour", numeroTour);
        queryCandidaturesRefuses.executeUpdate();
    }
}
