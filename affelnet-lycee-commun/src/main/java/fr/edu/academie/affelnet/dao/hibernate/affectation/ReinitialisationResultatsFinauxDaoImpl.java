/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.pam.BatchOpaDaoImpl;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.affectation.ReinitialisationResultatsFinauxDao;
import fr.edu.academie.affelnet.domain.statistiques.Statistique;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;

/**
 * Implementation du dao de l'�tape de traitement d'affectation r�alisant la r�initialisation des r�sultats finaux
 * pour le tour actuel.
 */
@Repository("ReinitialisationResultatsFinauxDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ReinitialisationResultatsFinauxDaoImpl extends BatchOpaDaoImpl
        implements ReinitialisationResultatsFinauxDao {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(ReinitialisationResultatsFinauxDaoImpl.class);

    /** Le gestionnaire de la campagne d'affectation. */
    private CampagneAffectationManager campagneAffectationManager;

    /** Le num�ro du tour courant (pour lequel s'effectue la r�initisation). */
    private short numeroTourCourant;

    /**
     * @param campagneAffectationManager
     *            Le gestionnaire de la campagne d'affectation
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneAffectationManager = campagneAffectationManager;
    }

    @Override
    public void lancerTraitement() throws Exception {

        numeroTourCourant = campagneAffectationManager.getNumeroTourCourant();

        LOG.info("R�initialisation des r�sultats finaux du tour " + numeroTourCourant);

        messages.start("Reinitialisation des resultats finaux");
        suppressionStatistiques();
        reinitialisationPiles();
        suppressionDecisionsFinales();
        actualiserEtatApplicatif();
        messages.end();

        LOG.info("R�initialisation des r�sultats finaux du tour " + numeroTourCourant + " effectu�e");
    }

    /**
     * Suppression des d�cisions finales sur les �l�ves et les voeux du tour courant.
     */
    private void suppressionDecisionsFinales() {

        Session session = HibernateUtil.currentSession();

        messages.start("Mise � jour des d�cisions sur les �l�ves");
        Query queryReinitEleves = session
                .getNamedQuery("reinitialiserResultatsFinauxElevesNonForceTour.update.sql");
        queryReinitEleves.setShort("tour", numeroTourCourant);
        queryReinitEleves.executeUpdate();
        messages.end();

        messages.start("Mise � jour des d�cisions sur les voeux");
        Query queryReinitVoeux = session
                .getNamedQuery("reinitialiserResultatsFinauxVoeuxElevesNonForceTour.update.sql");
        queryReinitVoeux.setShort("tour", numeroTourCourant);
        queryReinitVoeux.executeUpdate();
        messages.end();

    }

    /**
     * Supprime le contenu des tables de statistiques pour le tour courant.
     */
    private void suppressionStatistiques() {

        Session session = HibernateUtil.currentSession();

        for (Statistique statistique : Statistique.values()) {

            messages.start("Suppression des donn�es de la statistique " + statistique.getLibelle());
            String nomTable = statistique.getNomTable();

            LOG.info("Suppression du contenu de la table '" + nomTable + "' pour le tour " + numeroTourCourant
                    + ".");
            String deleteTableStatPourTourQuerySql = "delete from " + nomTable + " where VAL_TOUR = :numeroTour";

            Query deleteTableStatPourTourQuery = session.createSQLQuery(deleteTableStatPourTourQuerySql);

            deleteTableStatPourTourQuery.setParameter("numeroTour", numeroTourCourant);

            DaoUtils.executeHibernateQuery(deleteTableStatPourTourQuery);
            LOG.info("Suppression du contenu de la table '" + nomTable + "' pour le tour " + numeroTourCourant
                    + " effectu�e.");
            messages.end();
        }
    }

    /** R�initialisation des piles. */
    private void reinitialisationPiles() {

        Session session = HibernateUtil.currentSession();
        messages.start("Remise � z�ro des compteurs de piles");
        Query queryReinitPiles = session.getNamedQuery("reinitialiserPilesTour.update.sql");
        queryReinitPiles.executeUpdate();
        messages.end();
    }

    /**
     * Actualise l'�tat applicatif suite � la r�initialisation.
     */
    private void actualiserEtatApplicatif() {
        messages.start("Mise � jour de l'�tat applicatif");
        campagneAffectationManager.actualiserEtatSuiteReinitialisationResultatsFinaux();
        messages.end();
    }

}
