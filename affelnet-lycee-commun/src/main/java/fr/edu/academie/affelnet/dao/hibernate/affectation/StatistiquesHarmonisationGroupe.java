/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.affectation;

/** Statistiques des notes par groupe origine et notes pour le lissage. */
public class StatistiquesHarmonisationGroupe {

    /** Code du groupe origine. */
    private String codeGroupe;

    /** Moyenne des notes. */
    private double moyenne;

    /** Ecart-type des notes. */
    private double ecartType;

    /**
     * @param codeGroupe
     *            le code du groupe origine
     * @param moyenne
     *            Moyenne des notes
     * @param ecartType
     *            Ecart-type des notes
     */
    public StatistiquesHarmonisationGroupe(String codeGroupe, double moyenne, double ecartType) {
        super();
        this.codeGroupe = codeGroupe;
        this.moyenne = moyenne;
        this.ecartType = ecartType;
    }

    /**
     * @return le code du groupe origine
     */
    public String getCodeGroupe() {
        return codeGroupe;
    }

    /**
     * @param codeGroupe
     *            le code du groupe origine
     */
    public void setCodeGroupe(String codeGroupe) {
        this.codeGroupe = codeGroupe;
    }

    /**
     * @return Moyenne des notes
     */
    public double getMoyenne() {
        return moyenne;
    }

    /**
     * @param moyenne
     *            Moyenne des notes
     */
    public void setMoyenne(double moyenne) {
        this.moyenne = moyenne;
    }

    /**
     * @return Ecart-type des notes
     */
    public double getEcartType() {
        return ecartType;
    }

    /**
     * @param ecartType
     *            Ecart-type des notes
     */
    public void setEcartType(double ecartType) {
        this.ecartType = ecartType;
    }

    /** @return repr�sentation synth�tique de la statistique */
    public String toPrettyString() {
        return " groupe : " + codeGroupe + ", " + "moyenne : " + moyenne + " , ecart-type : " + ecartType;
    }

}
