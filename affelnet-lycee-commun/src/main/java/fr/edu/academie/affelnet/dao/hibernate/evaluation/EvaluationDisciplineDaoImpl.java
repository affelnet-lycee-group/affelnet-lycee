/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.EvaluationDisciplineDao;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationDiscipline;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationDisciplinePK;

/**
 * Impl�mentation du Dao pour la gestion des �valuations des disciplines.
 */
@Repository("EvaluationDisciplineDaoImpl")
public class EvaluationDisciplineDaoImpl extends BaseDaoHibernate<EvaluationDiscipline, EvaluationDisciplinePK>
        implements EvaluationDisciplineDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        List<Tri> tri = new ArrayList<Tri>();
        tri.add(Tri.asc("etablissement"));
        return tri;
    }

    @Override
    protected String getMessageObjectNotFound(EvaluationDisciplinePK key) {
        return "La demande ou l'�tablissement n'a pas �t� trouv�";
    }

    @Override
    protected void setKey(EvaluationDiscipline object, EvaluationDisciplinePK key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(EvaluationDiscipline object, EvaluationDisciplinePK oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "La demande d'�valuation existe d�j� pour cet �tablissement";
    }

    @Override
    protected Class<EvaluationDiscipline> getObjectClass() {
        return EvaluationDiscipline.class;
    }

    @Override
    public Filtre filtreElevesSansEvalDiscipline() {
        return Filtre.sizeEgale("evaluationsDiscipline", 0);
    }
}
