/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.EvaluationSocleDao;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSoclePK;

/**
 * Impl�mentation du Dao pour la gestion des demandes d'�valuation LSU.
 */
@Repository("EvaluationSocleDaoImpl")
public class EvaluationSocleDaoImpl extends BaseDaoHibernate<EvaluationSocle, EvaluationSoclePK>
        implements EvaluationSocleDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        List<Tri> tri = new ArrayList<Tri>();
        tri.add(Tri.asc("etablissement"));
        return tri;
    }

    @Override
    protected String getMessageObjectNotFound(EvaluationSoclePK key) {
        return "La demande ou l'�tablissement n'a pas �t� trouv�";
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "La demande d'�valuation existe d�j� pour cet �tablissement";
    }

    @Override
    protected void setKey(EvaluationSocle object, EvaluationSoclePK key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(EvaluationSocle object, EvaluationSoclePK oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected Class<EvaluationSocle> getObjectClass() {
        return EvaluationSocle.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EvaluationSocle> listerParEleve(String ine) {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(EvaluationSocle.class)
                    .add(Restrictions.eq("id.eleve.ine", ine));
            return criteria.list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void purger(String ine) {
        try {
            Session session = getSession();
            String hql = "delete from EvaluationSocle e where e.id.eleve.ine = :ine";
            session.createQuery(hql).setParameter("ine", ine).executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public Filtre filtreElevesSansEvalSocle() {
        return Filtre.sizeEgale("evaluationsSocle", 0);
    }

}
