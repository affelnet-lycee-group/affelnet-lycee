/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.evaluation.lsu;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.lsu.CorrespondanceEvaluationDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CorrespondanceEvaluation;

/**
 * Implementation du DAO pour la gestion Hibernate des correspondances entre les �valuations des champs
 * disciplinaires
 * et les groupes de niveau.
 */
@Repository("CorrespondanceEvaluationDao")
public class CorrespondanceEvaluationDaoImpl extends BaseDaoHibernate<CorrespondanceEvaluation, Long>
        implements CorrespondanceEvaluationDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        List<Tri> tri = new ArrayList<Tri>();
        tri.add(Tri.asc("idEtablissement"));
        tri.add(Tri.asc("valEval"));
        return tri;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "La correspondance demand�e n'a pas �t� trouv�e.";
    }

    @Override
    protected void setKey(CorrespondanceEvaluation object, Long key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(CorrespondanceEvaluation object, Long oldKey) {
        return !(object.getId() == oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "cette correspondance existe d�j�.";
    }

    @Override
    protected Class<CorrespondanceEvaluation> getObjectClass() {
        return CorrespondanceEvaluation.class;
    }

    @Override
    public CorrespondanceEvaluation charger(String idEtablissement, String valEval) {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(CorrespondanceEvaluation.class)
                    .add(Restrictions.eq("idEtablissement", idEtablissement))
                    .add(Restrictions.eq("valEval", valEval).ignoreCase());
            return (CorrespondanceEvaluation) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> chargerEvalParEtab(String idEtablissement) {
        // r�cup�ration de la session
        Session session = getSession();
        Query requete = session.getNamedQuery("hql.select.valeurEvalPourEtab");
        requete.setString("idEtablissement", idEtablissement);

        return requete.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CorrespondanceEvaluation> chargerCorrespondanceParEtab(String idEtablissement) {
        // r�cup�ration de la session
        Session session = getSession();
        Query requete = session.getNamedQuery("hql.select.correspondancePourEtab");
        requete.setString("idEtablissement", idEtablissement);

        return requete.list();
    }

    @Override
    public boolean existe(String idEtablissement, String valEval) {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(CorrespondanceEvaluation.class)
                    .setProjection(Projections.rowCount()).add(Restrictions.eq("idEtablissement", idEtablissement))
                    .add(Restrictions.ilike("valEval", valEval));
            return ((Number) criteria.uniqueResult()).intValue() > 0;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public int compteCorresNonResolue(String idEtablissement) {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(CorrespondanceEvaluation.class)
                    .setProjection(Projections.rowCount()).add(Restrictions.eq("idEtablissement", idEtablissement))
                    .add(Restrictions.isNull("groupeNiveau")).add(Restrictions.eq("flagIgnore", Flag.NON));
            return ((Number) criteria.uniqueResult()).intValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
