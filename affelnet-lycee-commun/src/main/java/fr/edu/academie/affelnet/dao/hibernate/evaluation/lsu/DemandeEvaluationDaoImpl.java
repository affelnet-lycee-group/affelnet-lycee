/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.evaluation.lsu;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.lsu.DemandeEvaluationDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.lsu.DemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EtatDemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationEleveLSU;

/**
 * Impl�mentation du Dao pour la gestion des demandes d'�valuation LSU.
 */
@Repository("DemandeEvaluationDaoImpl")
public class DemandeEvaluationDaoImpl extends BaseDaoHibernate<DemandeEvaluation, String>
        implements DemandeEvaluationDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        List<Tri> tri = new ArrayList<Tri>();
        tri.add(Tri.asc("etablissement"));
        return tri;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        return "La demande ou l'�tablissement n'a pas �t� trouv�";
    }

    /**
     * Non impl�ment�.
     */
    @Override
    protected void setKey(DemandeEvaluation object, String key) {
        // object.setEtablissement(key);
    }

    @Override
    protected boolean hasKeyChange(DemandeEvaluation object, String oldKey) {
        return !object.getEtablissement().getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "La demande d'�valuation existe d�j� pour cet �tablissement";
    }

    @Override
    protected Class<DemandeEvaluation> getObjectClass() {
        return DemandeEvaluation.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EvaluationEleveLSU> listerEleveNonIntegre(String idEtablissement) {
        try {
            Session session = getSession();

            Criteria criteria = session.createCriteria(EvaluationEleveLSU.class, "eleve")
                    .createAlias("eleve.demandeEvaluation", "demande")
                    .add(Restrictions.eq("demande.idEtablissement", idEtablissement))
                    .add(Restrictions.eq("demande.flagIntegration", Flag.OUI))
                    .add(Restrictions.eq("demande.etat", EtatDemandeEvaluation.OK))
                    .add(Restrictions.eq("eleve.flagIntegre", Flag.NON)).addOrder(Order.asc("eleve.nom"))
                    .addOrder(Order.asc("eleve.prenom"));
            return (List<EvaluationEleveLSU>) criteria.list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public int compterEleveIntegre(String idEtablissement) {
        try {
            Session session = getSession();

            Criteria criteria = session.createCriteria(EvaluationEleveLSU.class, "eleve")
                    .createAlias("eleve.demandeEvaluation", "demande")
                    .add(Restrictions.eq("demande.idEtablissement", idEtablissement))
                    .add(Restrictions.eq("demande.flagIntegration", Flag.OUI))
                    .add(Restrictions.eq("eleve.flagIntegre", Flag.OUI)).setProjection(Projections.rowCount());
            return ((Number) criteria.uniqueResult()).intValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EvaluationEleveLSU> chargerEleveLsu(String ine, String idEtablissement) {
        try {
            Session session = getSession();

            Criteria criteria = session.createCriteria(EvaluationEleveLSU.class, "eleve")
                    .createAlias("eleve.demandeEvaluation", "demande").add(Restrictions.eq("ine", ine))
                    .add(Restrictions.eq("demande.idEtablissement", idEtablissement));
            return criteria.list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
