/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.AvisDao;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.AvisPK;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.service.nomenclature.TypeAvisManager;

/**
 * Implementation de la gestion des avis.
 */
@Repository("AvisDao")
public class AvisDaoImpl extends BaseDaoHibernate<Avis, AvisPK> implements AvisDao {

    /** Liste des ordres par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("id.type.code"));
        DEFAULT_ORDERS.add(Tri.asc("id.code"));
    }

    private TypeAvisManager typeAvisManager;

    /**
     * @param typeAvisManager
     *            the typeAvisManager to set
     */
    @Autowired
    public void setTypeAvisManager(TypeAvisManager typeAvisManager) {
        this.typeAvisManager = typeAvisManager;
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected void setKey(Avis avis, AvisPK key) {
        avis.setId(key);
    }

    @Override
    protected boolean hasKeyChange(Avis avis, AvisPK oldKey) {
        return !avis.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un avis avec ce type et ce code";
    }

    @Override
    protected Class<Avis> getObjectClass() {
        return Avis.class;
    }

    @Override
    protected String getMessageObjectNotFound(AvisPK key) {

        if (key != null && key.getType() != null && key.getType().getCode() != null
                && key.getType().getCode().equals(CodeTypeAvis.AVIS_DE_GESTION.getCode())) {
            return "L'" + typeAvisManager.charger(CodeTypeAvis.AVIS_DE_GESTION).getLibelle() + " " + key.getCode()
                    + " n'existe pas";

        } else if (key != null && key.getType() != null && key.getType().getCode() != null
                && key.getType().getCode().equals(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT.getCode())) {
            return "L'" + typeAvisManager.charger(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT).getLibelle() + " "
                    + key.getCode() + " n'existe pas";

        } else {
            return "Avis inexistant";
        }
    }
}
