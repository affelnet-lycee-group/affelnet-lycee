/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.AvisParFormationOrigineDao;
import fr.edu.academie.affelnet.domain.nomenclature.AvisParFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;

/**
 * Implementation de la gestion de la saisie de l'avis du chef d'�tablissement d'origine en
 * fonction de l'origine et de l'accueil de l'�l�ve.
 */
@Repository("AvisParFormationOrigineDao")
public class AvisParFormationOrigineDaoImpl extends BaseDaoHibernate<AvisParFormationOrigine, Long>
        implements AvisParFormationOrigineDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("formation.id.mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("formation.id.codeSpecialite"));
    }

    @Override
    public boolean isUnique(Long pfaId, String mnemonique, String codeSpecialite, Matiere option1,
            Matiere option2) {
        Session s = getSession();

        // critere de recherche
        Criteria c = s.createCriteria(AvisParFormationOrigine.class);

        // Param�tre par formation d'accueil conrcern�
        if (pfaId != null) {
            c.add(Restrictions.eq("parametresFormationAccueil.id", pfaId));
        }

        // Formation d'origine et options
        c.add(Restrictions.eq("mnemonique", mnemonique));

        if (codeSpecialite != null) {
            c.add(Restrictions.eq("codeSpecialite", codeSpecialite));
        } else {
            c.add(Restrictions.isNull("codeSpecialite"));
        }

        if (option1 != null) {
            c.add(Restrictions.eq("option1.cleGestion", option1.getCleGestion()));
        } else {
            c.add(Restrictions.isNull("option1.cleGestion"));
        }

        if (option2 != null) {
            c.add(Restrictions.eq("option2.cleGestion", option2.getCleGestion()));
        } else {
            c.add(Restrictions.isNull("option2.cleGestion"));
        }

        return c.list().isEmpty();
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un lien entre une formation d'origine "
                + "et un param�tre par formation d'accueil avec le m�me identifiant";
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Il n'existe pas de lien entre une formation d'origine "
                + "et un param�tre par formation d'accueil avec l'identifiant " + key;
    }

    @Override
    protected Class<AvisParFormationOrigine> getObjectClass() {
        return AvisParFormationOrigine.class;
    }

    @Override
    protected boolean hasKeyChange(AvisParFormationOrigine object, Long oldKey) {
        return false;
    }

    @Override
    protected void setKey(AvisParFormationOrigine object, Long key) {
    }
}
