/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.BonusDao;
import fr.edu.academie.affelnet.domain.nomenclature.Bonus;
import fr.edu.academie.affelnet.domain.nomenclature.TypeBonus;

/** Implementation de la gestion des bonus "Pr�paration du bar�me". */
@Repository("BonusDao")
public class BonusDaoImpl extends BaseDaoHibernate<Bonus, TypeBonus> implements BonusDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("ordre"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(TypeBonus key) {
        return "Le bonus doublement " + key + " n'existe pas";
    }

    @Override
    protected void setKey(Bonus bonus, TypeBonus key) {
        bonus.setType(key);
    }

    @Override
    protected boolean hasKeyChange(Bonus bonus, TypeBonus oldKey) {
        return !bonus.getType().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce code";
    }

    @Override
    protected Class<Bonus> getObjectClass() {
        return Bonus.class;
    }

    @Override
    public Map<TypeBonus, Bonus> mapBonus() {
        /*
         * R�cup�re la liste des bonus (en lecture et en �criture)
         * Cr�e un stream d'Object
         * Le transforme en Map avec pour cl� le type et en valeur le Bonus
         * Si deux Bonus identiques sont r�cup�r�s, le nouveau remplace l'ancien
         * L'impl�mentation de Map est TreeMap ce qui signifie que les Bonus sont tri�s
         * via {@link Bonus#compareTo(Bonus)}
         */
        return lister(false).stream()
                .collect(Collectors.toMap(Bonus::getType, Function.identity(), (a, b) -> b, TreeMap::new));
    }
}
