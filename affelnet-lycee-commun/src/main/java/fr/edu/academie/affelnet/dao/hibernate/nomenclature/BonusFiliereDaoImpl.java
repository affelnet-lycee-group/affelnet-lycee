/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.BonusFiliereDao;
import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;

/** Implementation de la gestion des bonus entre fili�res. */
@Repository("BonusFiliereDao")
public class BonusFiliereDaoImpl extends BaseDaoHibernate<BonusFiliere, Long> implements BonusFiliereDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("mnemoniqueOrigine"));
        DEFAULT_ORDERS.add(Tri.asc("codeSpecialiteOrigine"));
        DEFAULT_ORDERS.add(Tri.asc("optionOrigine1.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("optionOrigine2.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("mnemoniqueAccueil"));
        DEFAULT_ORDERS.add(Tri.asc("codeSpecialiteAccueil"));
        DEFAULT_ORDERS.add(Tri.asc("matiereEnseigOptionnel.cleGestion"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected void setKey(BonusFiliere bonusFiliere, Long key) {
        bonusFiliere.setId(key);
    }

    @Override
    protected boolean hasKeyChange(BonusFiliere bonusFiliere, Long oldKey) {
        return !bonusFiliere.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un bonus fili�re avec cet identifiant";
    }

    @Override
    protected Class<BonusFiliere> getObjectClass() {
        return BonusFiliere.class;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Le bonus fili�re " + key + " n'existe pas";
    }

    @Override
    public boolean isUnique(BonusFiliere bonusFiliere) {
        List<Filtre> filters = new ArrayList<Filtre>();

        // cas de la mise � jour, on enl�ve l'�l�ment lui m�me
        Long bonusFiliereId = bonusFiliere.getId();
        if (bonusFiliereId != null) {
            filters.add(Filtre.notEqual("id", bonusFiliereId));
        }

        filters.add(Filtre.equal("mnemoniqueOrigine", bonusFiliere.getMnemoniqueOrigine()));
        filters.add(Filtre.equalOrIsNull("codeSpecialiteOrigine", bonusFiliere.getCodeSpecialiteOrigine()));

        Matiere optionOrigine1 = bonusFiliere.getOptionOrigine1();
        Matiere optionOrigine2 = bonusFiliere.getOptionOrigine2();
        filters.add(Filtre.or(
                Filtre.and(Filtre.equalOrIsNull("optionOrigine1", optionOrigine1),
                        Filtre.equalOrIsNull("optionOrigine2", optionOrigine2)),
                Filtre.and(Filtre.equalOrIsNull("optionOrigine1", optionOrigine2),
                        Filtre.equalOrIsNull("optionOrigine2", optionOrigine1))));

        filters.add(Filtre.equal("mnemoniqueAccueil", bonusFiliere.getMnemoniqueAccueil()));
        filters.add(Filtre.equalOrIsNull("codeSpecialiteAccueil", bonusFiliere.getCodeSpecialiteAccueil()));

        Matiere matiereEnseigOptionnel = bonusFiliere.getMatiereEnseigOptionnel();

        filters.add(Filtre.equalOrIsNull("matiereEnseigOptionnel", matiereEnseigOptionnel));

        return getNombreElements(filters) <= 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<BonusFiliere> listerBonusFilieresPourMefStat4(String mefStat4) {
        Session session = getSession();
        Query bonusFilerePourMefStat4 = session.getNamedQuery("hql.select.bonusFilierePourMefStat4");
        bonusFilerePourMefStat4.setString("mefStat4", mefStat4);

        return bonusFilerePourMefStat4.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BonusFiliere> listerBonusFiliere(Formation formation) {
        Criteria c = getSession().createCriteria(BonusFiliere.class);
        c.add(Restrictions.eq("mnemoniqueOrigine", formation.getId().getMnemonique()));
        return c.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<BonusFiliere> listerBonusFiliereFetch() {
        Criteria c = getSession().createCriteria(BonusFiliere.class);
        c.setFetchMode("formationOrigine", FetchMode.JOIN);
        c.setFetchMode("optionOrigine1", FetchMode.JOIN);
        c.setFetchMode("optionOrigine2", FetchMode.JOIN);
        c.setFetchMode("formationAccueil", FetchMode.JOIN);
        c.setFetchMode("matiereEnseigOptionnel", FetchMode.JOIN);
        return c.list();
    }

}
