/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.CommissionDao;
import fr.edu.academie.affelnet.domain.nomenclature.Commission;

/** Implementation de la gestion des commissions. */
@Repository("CommissionDao")
public class CommissionDaoImpl extends BaseDaoHibernate<Commission, String> implements CommissionDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("code"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected void setKey(Commission commission, String key) {
        commission.setCode((String) key);
    }

    @Override
    protected boolean hasKeyChange(Commission commission, String oldKey) {
        return !commission.getCode().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� une commission avec le m�me code";
    }

    @Override
    protected Class<Commission> getObjectClass() {
        return Commission.class;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        return "La commission " + key + " n'existe pas";
    }

    @Override
    public boolean existeCommission(String codeCommission) {
        try {
            // r�cup�ration de la session
            Session session = getSession();
            // recherche des �l�ments voulus
            StringBuffer requete = new StringBuffer();
            requete.append("select count(*) from Commission c ");
            requete.append("where c.code = '" + codeCommission + "' ");
            long i = ((Number) session.createQuery(requete.toString()).iterate().next()).longValue();
            return (i > 0);
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
