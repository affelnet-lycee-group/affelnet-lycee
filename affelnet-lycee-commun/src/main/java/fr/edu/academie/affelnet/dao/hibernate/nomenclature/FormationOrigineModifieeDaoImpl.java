/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.utils.MatiereUtils;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.FormationOrigineModifieeDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigineModifiee;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;

/**
 * Implementation de la gestion des formations origine modifi�es.
 */
@Repository("FormationOrigineModifieeDao")
public class FormationOrigineModifieeDaoImpl extends BaseDaoHibernate<FormationOrigineModifiee, Long>
        implements FormationOrigineModifieeDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("formationInitiale.id.mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("formationInitiale.id.codeSpecialite"));
        DEFAULT_ORDERS.add(Tri.asc("optionInitiale1.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("optionInitiale2.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("formationFinale.id.mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("formationFinale.id.codeSpecialite"));
        DEFAULT_ORDERS.add(Tri.asc("optionFinale1.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("optionFinale2.cleGestion"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "La formation d'origine " + key + " n'existe pas";
    }

    @Override
    protected void setKey(FormationOrigineModifiee formationOrigineModifiee, Long key) {
        formationOrigineModifiee.setId(key);
    }

    @Override
    protected boolean hasKeyChange(FormationOrigineModifiee formationOrigineModifiee, Long oldKey) {
        return !formationOrigineModifiee.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� une formation origine modifi�e avec cet identifiant";
    }

    @Override
    protected Class<FormationOrigineModifiee> getObjectClass() {
        return FormationOrigineModifiee.class;
    }

    @Override
    public boolean isUnique(Long currentKey, Formation formationInitiale, Matiere optionInitiale1,
            Matiere optionInitiale2) {
        FormationOrigineModifiee fom = findExact(formationInitiale,
                MatiereUtils.generateCouple(optionInitiale1, optionInitiale2));
        if (fom == null) {
            return true;
        } else {
            return fom.getId().equals(currentKey);
        }
    }

    /**
     * Recherche la formation origine modifi� qui correspond exactement � la formation et aux options initiales
     * indiqu�es.
     * 
     * @param formationI
     *            la formation initale
     * @param lOptI
     *            la liste des option initiales
     * @return la formation origine modifi�e (ou null si rien aucune ne correspond)
     */
    @SuppressWarnings("unchecked")
    private FormationOrigineModifiee findExact(Formation formationI, List<Matiere> lOptI) {
        Session s = getSession();

        // variables
        String mnemoI = formationI.getId().getMnemonique();
        String coSpeI = formationI.getId().getCodeSpecialite();

        // critere de recherche
        Criteria c = s.createCriteria(FormationOrigineModifiee.class);

        // formationI
        c.add(Restrictions.eq("formationInitiale.id.mnemonique", mnemoI));
        c.add(Restrictions.eq("formationInitiale.id.codeSpecialite", coSpeI));

        // options
        if (lOptI.size() == 2) {
            Matiere optI1 = lOptI.get(0);
            Matiere optI2 = lOptI.get(1);
            Criterion crit1 = Restrictions.and(Restrictions.eq("optionInitiale1", optI1),
                    Restrictions.eq("optionInitiale2", optI2));
            Criterion crit2 = Restrictions.and(Restrictions.eq("optionInitiale1", optI2),
                    Restrictions.eq("optionInitiale2", optI1));
            c.add(Restrictions.or(crit1, crit2));
        } else if (lOptI.size() == 1) {
            Matiere optI = lOptI.get(0);
            c.add(Restrictions.eq("optionInitiale1", optI));
            c.add(Restrictions.isNull("optionInitiale2"));
        } else if (lOptI.isEmpty()) {
            c.add(Restrictions.isNull("optionInitiale1"));
            c.add(Restrictions.isNull("optionInitiale2"));
        }

        // plus de 2 : pas g�rable ici
        else {
            throw new IllegalArgumentException("Pas plus de 2 options dans cette fonction");
        }

        // recherche du resultat
        List<FormationOrigineModifiee> result = c.list();

        if (!result.isEmpty()) {
            return result.get(0);
        }

        // rien n'est trouv�
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FormationOrigineModifiee> listerFormationOrigineModifieePourFormationInitiale(
            Formation formation) {
        Criteria c = getSession().createCriteria(FormationOrigineModifiee.class);
        c.add(Restrictions.eq("formationInitiale.id.mnemonique", formation.getId().getMnemonique()));
        c.add(Restrictions.eq("formationInitiale.id.codeSpecialite", formation.getId().getCodeSpecialite()));
        return c.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FormationOrigineModifiee> listerFormationOrigineModifieeAvecJointuresFetch() {
        Criteria c = getSession().createCriteria(FormationOrigineModifiee.class);
        c.setFetchMode("formationInitiale", FetchMode.JOIN);
        c.setFetchMode("optionInitiale1", FetchMode.JOIN);
        c.setFetchMode("optionInitiale2", FetchMode.JOIN);
        c.setFetchMode("formationFinale", FetchMode.JOIN);
        c.setFetchMode("optionFinale1", FetchMode.JOIN);
        c.setFetchMode("optionFinale2", FetchMode.JOIN);

        return c.list();
    }

}
