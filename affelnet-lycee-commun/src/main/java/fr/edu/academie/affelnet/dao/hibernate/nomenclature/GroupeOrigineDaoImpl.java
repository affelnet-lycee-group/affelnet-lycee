/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.GroupeOrigineDao;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigineFormation;

/**
 * Implementation de la gestion des groupes origine pour les �l�ves.
 */
@Repository("GroupeOrigineDao")
public class GroupeOrigineDaoImpl extends BaseDaoHibernate<GroupeOrigine, String> implements GroupeOrigineDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("code"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    public GroupeOrigine creer(GroupeOrigine groupeOrigine) {
        majEleves();
        groupeOrigine.setNombreEleves(0);
        GroupeOrigine result = (GroupeOrigine) super.creer(groupeOrigine);
        creerFormations(groupeOrigine.getFormations());
        result.setFormations(groupeOrigine.getFormations());
        return result;
    }

    @Override
    public GroupeOrigine charger(String code) {
        GroupeOrigine groupeOrigine = (GroupeOrigine) super.charger(code);
        groupeOrigine.setFormations(listerFormations(code));
        return groupeOrigine;
    }

    @Override
    public GroupeOrigine maj(GroupeOrigine groupeOrigine, String oldKey) {
        supprimerFormations(oldKey);
        groupeOrigine.setNombreEleves(0);
        majEleves();
        GroupeOrigine result = (GroupeOrigine) super.maj(groupeOrigine, oldKey);
        result.setFormations(groupeOrigine.getFormations());
        creerFormations(groupeOrigine.getFormations());
        return result;
    }

    @Override
    public void supprimer(GroupeOrigine groupeOrigine) {
        // mise � z�ro des compteurs et � null des groupes
        majEleves();
        // suppression des formations associ�es � ce groupe
        supprimerFormations(groupeOrigine.getCode());
        // suppression du groupe
        super.supprimer(groupeOrigine);
    }

    /**
     * Ajouter une liste de formations au groupe.
     * 
     * @param formations
     *            la liste des formations (gof)
     */
    private void creerFormations(List<GroupeOrigineFormation> formations) {
        GroupeOrigineFormationDaoImpl daoGroupeOrigineFormation = new GroupeOrigineFormationDaoImpl();
        for (int i = 0; i < formations.size(); i++) {
            GroupeOrigineFormation gof = formations.get(i);
            daoGroupeOrigineFormation.creer(gof);
        }
    }

    /**
     * Supprimer toutes les formations (gof) associ�es � un groupe.
     * 
     * @param codeGroupe
     *            le code du groupe origine
     */
    private void supprimerFormations(String codeGroupe) {
        GroupeOrigineFormationDaoImpl daoGroupeOrigineFormation = new GroupeOrigineFormationDaoImpl();
        // r�cup�ration de la liste des formations associ�es au groupe
        List<GroupeOrigineFormation> formations = listerFormations(codeGroupe);
        for (int i = 0; i < formations.size(); i++) {
            GroupeOrigineFormation gof = formations.get(i);
            daoGroupeOrigineFormation.supprimer(gof);
        }
    }

    /**
     * @param codeGroupe
     *            le code du groupe origine
     * @return la liste des formations (gof) associ�es � un groupe.
     */
    public List<GroupeOrigineFormation> listerFormations(String codeGroupe) {
        GroupeOrigineFormationDaoImpl daoGroupeOrigineFormation = new GroupeOrigineFormationDaoImpl();
        List<Filtre> filtres = new ArrayList<Filtre>();
        filtres.add(Filtre.equal("groupeOrigine.code", codeGroupe));
        return daoGroupeOrigineFormation.lister(filtres);
    }

    @Override
    public void majEleves() {
        // mise � 'null' du groupe origine pour tous les �l�ves
        DaoUtils.executeSQL("update GV_ELEVE set CO_GRP = NULL");

        // mise � z�ro des compteurs �l�ves pour les groupes
        DaoUtils.executeSQL("update GN_GRPO set NB_ELV = 0");
    }

    @Override
    protected void setKey(GroupeOrigine groupeOrigine, String key) {
        groupeOrigine.setCode(key);
    }

    @Override
    protected boolean hasKeyChange(GroupeOrigine groupeOrigine, String oldKey) {
        return !groupeOrigine.getCode().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce code";
    }

    @Override
    protected Class<GroupeOrigine> getObjectClass() {
        return GroupeOrigine.class;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        String code = (String) key;
        return "Le groupe d'origine " + code + " n'existe pas";
    }
}
