/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.GroupeOrigineFormationDao;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigineFormation;

/**
 * Implementation de la gestion des formations pour les groupes origine.
 */
@Repository("GroupeOrigineFormationDao")
public class GroupeOrigineFormationDaoImpl extends BaseDaoHibernate<GroupeOrigineFormation, Long>
        implements GroupeOrigineFormationDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("groupeOrigine"));
        DEFAULT_ORDERS.add(Tri.asc("mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("codeSpecialite"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Le groupe de formation origine " + key + " n'existe pas";
    }

    @Override
    protected void setKey(GroupeOrigineFormation groupeOrigineFormation, Long key) {
        groupeOrigineFormation.setId(key);
    }

    @Override
    protected boolean hasKeyChange(GroupeOrigineFormation groupeOrigineFormation, Long oldKey) {
        return !groupeOrigineFormation.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un lien avec cet identifiant";
    }

    @Override
    protected Class<GroupeOrigineFormation> getObjectClass() {
        return GroupeOrigineFormation.class;
    }
}
