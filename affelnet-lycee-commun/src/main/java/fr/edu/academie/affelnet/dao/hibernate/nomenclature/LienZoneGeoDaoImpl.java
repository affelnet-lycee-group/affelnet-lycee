/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.LienZoneGeoComparator;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.LienZoneGeoDao;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;

/**
 * Implementation de la gestion des liens entre zones g�ographiques.
 */
@Repository("LienZoneGeoDao")
public class LienZoneGeoDaoImpl extends BaseDaoHibernate<LienZoneGeo, Long> implements LienZoneGeoDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("codeZoneGeoOrigine"));
        DEFAULT_ORDERS.add(Tri.asc("voeu.etablissement.zoneGeo.code"));
        DEFAULT_ORDERS.add(Tri.asc("voeu.formationAccueil.id.mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("voeu.formationAccueil.id.codeSpecialite"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Le lien de zone g�ographique " + key + " n'existe pas";
    }

    @Override
    protected void setKey(LienZoneGeo lienZoneGeo, Long key) {
        lienZoneGeo.setId(key);
    }

    @Override
    protected boolean hasKeyChange(LienZoneGeo lienZoneGeo, Long oldKey) {
        return !lienZoneGeo.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un lien avec ces zones g�ographiques et ce voeu";
    }

    @Override
    protected Class<LienZoneGeo> getObjectClass() {
        return LienZoneGeo.class;
    }

    /**
     * @param zgO
     *            la zone g�ographique d'origine
     * @param voeu
     *            le voeu
     * @param cacheMap
     *            le cache
     * @return le lien zone g�o. � appliquer
     */
    @SuppressWarnings("unchecked")
    public static LienZoneGeo find(ZoneGeo zgO, Voeu voeu, Map<String, Object> cacheMap) {
        Session s = HibernateUtil.currentSession();

        // r�cup�ration de la liste des LienZoneGeo
        final String cacheName = "listeLienZoneGeo";
        List<LienZoneGeo> lLzg;
        if (cacheMap == null) {
            // si pas de cache, on r�duit les possibilit�s en mettant les
            // crit�res obligatoires
            Criteria c = s.createCriteria(LienZoneGeo.class);
            c.add(Restrictions.eq("voeu", voeu));
            lLzg = c.list();
        } else if (!cacheMap.containsKey(cacheName)) {
            // lLzg = s.createCriteria(LienZoneGeo.class).list();
            Criteria c = s.createCriteria(LienZoneGeo.class);
            c.setFetchMode("zoneGeoOrigine", FetchMode.JOIN);
            c.setFetchMode("voeu", FetchMode.JOIN);
            lLzg = c.list();

            cacheMap.put(cacheName, lLzg);

            // new pour ne pas alterer la liste en cache
            lLzg = new ArrayList<LienZoneGeo>(lLzg);
        } else {

            // new pour ne pas alterer la liste en cache
            lLzg = new ArrayList<LienZoneGeo>((List<LienZoneGeo>) cacheMap.get(cacheName));
        }

        // parcours de la liste en �liminant tous les mauvais
        for (Iterator<LienZoneGeo> itLzg = lLzg.iterator(); itLzg.hasNext();) {
            LienZoneGeo lzg = itLzg.next();
            ZoneGeo zg = lzg.getZoneGeoOrigine();
            Voeu v = lzg.getVoeu();

            // zone geo
            if (zg != null && !zg.equals(zgO)) {
                itLzg.remove();
                continue;
            }

            // voeu
            if (!v.equals(voeu)) {
                itLzg.remove();
                continue;
            }
        }

        // pas de bonus corrects
        if (lLzg.size() == 0) {
            return null;
        }

        // il reste les �l�ments qui sont corrects, on les trie et on restitue le premier
        TreeSet<LienZoneGeo> listeTriee = new TreeSet<LienZoneGeo>(new LienZoneGeoComparator());
        listeTriee.addAll(lLzg);
        return listeTriee.first();
    }
}
