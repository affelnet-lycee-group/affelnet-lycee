/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.MatiereRangDao;
import fr.edu.academie.affelnet.domain.nomenclature.MatiereRang;

/**
 * Implementation de la gestion des rangs (vue).
 */
@Repository("MatiereRangDao")
public class MatiereRangDaoImpl extends BaseDaoHibernate<MatiereRang, Integer> implements MatiereRangDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("valeur"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    public MatiereRang creer(final MatiereRang matiereRang) {
        throw new DataAccessException("Cr�ation interdite");
    }

    @Override
    public MatiereRang maj(final MatiereRang rang, final Integer oldKey) {
        throw new DataAccessException("Mise � jour interdite");
    }

    @Override
    public void supprimer(final MatiereRang r) {
        throw new DataAccessException("Suppression interdite");
    }

    @Override
    protected void setKey(MatiereRang rang, Integer key) {
        throw new DataAccessException("Mise � jour de la clef interdite");
    }

    @Override
    protected boolean hasKeyChange(MatiereRang rang, Integer oldKey) {
        return !rang.getValeur().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce rang";
    }

    @Override
    protected Class<MatiereRang> getObjectClass() {
        return MatiereRang.class;
    }

    @Override
    protected String getMessageObjectNotFound(Integer key) {
        return "La correspondance matiere pour le rang " + key + " n'existe pas";
    }
}
