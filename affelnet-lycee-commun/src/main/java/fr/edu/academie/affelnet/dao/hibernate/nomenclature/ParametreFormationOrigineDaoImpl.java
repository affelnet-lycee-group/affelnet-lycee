/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.ParametreFormationOrigineDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.ParametreFormationOrigine;

/** Implementation de la gestion des param�tres par formation origine. */
@Repository("ParametreFormationOrigineDao")
public class ParametreFormationOrigineDaoImpl extends BaseDaoHibernate<ParametreFormationOrigine, Long>
        implements ParametreFormationOrigineDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("codeSpecialite"));
        DEFAULT_ORDERS.add(Tri.asc("option1.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("option2.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("id"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected void setKey(ParametreFormationOrigine parametreFormationOrigine, Long key) {
        parametreFormationOrigine.setId(key);
    }

    @Override
    protected boolean hasKeyChange(ParametreFormationOrigine parametreFormationOrigine, Long oldKey) {
        return !parametreFormationOrigine.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec cette formation et ces options";
    }

    @Override
    protected Class<ParametreFormationOrigine> getObjectClass() {
        return ParametreFormationOrigine.class;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Le param�tre pour la formation d'origine " + key + " n'existe pas";
    }

    @Override
    public boolean isUnique(Long currentKey, Formation formation, String mnemonique, Matiere option1,
            Matiere option2) {
        ParametreFormationOrigine pfo = new ParametreFormationOrigine();

        pfo.setOption1(option1);
        pfo.setOption2(option2);
        pfo.setFormationOrigine(formation);
        pfo.setMnemonique(mnemonique);
        if (formation != null) {
            pfo.setCodeSpecialite(formation.getId().getCodeSpecialite());
        }

        // recherche de l'element
        pfo = findExact(pfo);
        if (pfo == null) {
            return true;
        } else {
            return pfo.getId().equals(currentKey);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ParametreFormationOrigine> listerParametreFormationOriginePourFormation(Formation formation) {
        Criteria c = getSession().createCriteria(ParametreFormationOrigine.class);
        c.add(Restrictions.eq("formationOrigine.id.mnemonique", formation.getId().getMnemonique()));
        c.add(Restrictions.eq("formationOrigine.id.codeSpecialite", formation.getId().getCodeSpecialite()));
        return c.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ParametreFormationOrigine> listerParametreFormationOrigineAvecJointureFetch() {
        Criteria c = getSession().createCriteria(ParametreFormationOrigine.class);
        c.setFetchMode("formationOrigine", FetchMode.JOIN);
        c.setFetchMode("option1", FetchMode.JOIN);
        c.setFetchMode("option2", FetchMode.JOIN);
        return c.list();
    }

    /**
     * Recherche s'il existe un param�tre par formation origine correspondant au mod�le fourni.
     * La recherche se base la mise en correspondance du quadruplet cl�
     * (mn�monique, sp�cialit�, option1, option2).
     * 
     * @param pfo
     *            paramefo servant de mod�le pour la recherche
     * @return le paramefo s'il existe ou null sinon
     */
    @SuppressWarnings("unchecked")
    private ParametreFormationOrigine findExact(ParametreFormationOrigine pfo) {
        Session s = getSession();

        // element � rechercher
        String mnemo = pfo.getMnemonique();
        String coSpe = pfo.getCodeSpecialite();
        Matiere option1 = pfo.getOption1();
        Matiere option2 = pfo.getOption2();

        // critere de recherche
        Criteria c = s.createCriteria(ParametreFormationOrigine.class);

        // formation origine
        if (coSpe != null) {
            c.add(Restrictions.eq("mnemonique", mnemo));
            c.add(Restrictions.eq("codeSpecialite", coSpe));
        } else if (mnemo != null) {
            c.add(Restrictions.eq("mnemonique", mnemo));
            c.add(Restrictions.isNull("codeSpecialite"));
        } else {
            c.add(Restrictions.isNull("mnemonique"));
            c.add(Restrictions.isNull("codeSpecialite"));
        }

        // options
        if (option1 != null && option2 != null) {
            Criterion crit1 = Restrictions.and(Restrictions.eq("option1", option1),
                    Restrictions.eq("option2", option2));
            Criterion crit2 = Restrictions.and(Restrictions.eq("option1", option2),
                    Restrictions.eq("option2", option1));
            c.add(Restrictions.or(crit1, crit2));
        } else if (option1 != null) {
            c.add(Restrictions.eq("option1", option1));
            c.add(Restrictions.isNull("option2"));
        } else {
            c.add(Restrictions.isNull("option1"));
            c.add(Restrictions.isNull("option2"));
        }

        // recherche du resultat
        List<ParametreFormationOrigine> result = c.list();

        if (result.size() > 0) {
            return result.get(0);
        }

        // rien n'est trouv�
        return null;

    }

    @Override
    public Long nombreParamefoGenerique(String mnemonique) {

        Session session = getSession();
        Query query = session.getNamedQuery("hql.count.paramfoPourMnemoniqueEtCodeSpecialiteNul");
        query.setString("mnemonique", mnemonique);

        return ((Number) query.uniqueResult()).longValue();
    }
}
