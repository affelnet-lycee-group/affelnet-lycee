/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.ParametresFormationAccueilComparator;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.ParametresFormationAccueilDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.Pile;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;

/** Implementation de la gestion des param�tres par formation d'accueil. */
@Repository("ParametresFormationAccueilDao")
public class ParametresFormationAccueilDaoImpl extends BaseDaoHibernate<ParametresFormationAccueil, Long>
        implements ParametresFormationAccueilDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("formationAccueil.id.mnemonique"));
        DEFAULT_ORDERS.add(Tri.asc("formationAccueil.id.codeSpecialite"));
        DEFAULT_ORDERS.add(Tri.asc("matiereEnseigOptionnel.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("id"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected void setKey(ParametresFormationAccueil parametresFormationAccueil, Long key) {
        parametresFormationAccueil.setId(key);
    }

    @Override
    protected boolean hasKeyChange(ParametresFormationAccueil parametresFormationAccueil, Long oldKey) {
        return !parametresFormationAccueil.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un param�tre par formation d'accueil avec le m�me identifiant";
    }

    @Override
    protected Class<ParametresFormationAccueil> getObjectClass() {
        return ParametresFormationAccueil.class;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Il n'existe pas de param�tre par formation d'accueil avec l'identifiant " + key;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ParametresFormationAccueil> recuprererParamefa(Formation formation, Map<String, Object> cacheMap) {
        Session s = getSession();

        // r�cup�ration de la liste des ParametresFormationAccueil
        final String cacheName = "listeParametresFormationAccueil";
        List<ParametresFormationAccueil> lPfa;
        if (cacheMap == null) {
            // si pas de cache, on r�duit les possibilit�s en mettant les
            // crit�res obligatoires
            Criteria c = s.createCriteria(ParametresFormationAccueil.class);
            c.add(Restrictions.eq("formationAccueil.id.mnemonique", formation.getId().getMnemonique()));
            c.add(Restrictions.eq("formationAccueil.id.codeSpecialite", formation.getId().getCodeSpecialite()));
            lPfa = new ArrayList<>(c.list());
        } else if (!cacheMap.containsKey(cacheName)) {
            Criteria c = s.createCriteria(ParametresFormationAccueil.class);
            c.setFetchMode("formationAccueil", FetchMode.JOIN);
            c.setFetchMode("matiereEnseigOptionnel", FetchMode.JOIN);
            lPfa = c.list();

            cacheMap.put(cacheName, lPfa);

            // new pour ne pas alterer la liste en cache
            lPfa = new ArrayList<>(lPfa);
        } else {

            // new pour ne pas alterer la liste en cache
            lPfa = new ArrayList<>((List<ParametresFormationAccueil>) cacheMap.get(cacheName));
        }
        return lPfa;
    }

    @Override
    public boolean isUnique(ParametresFormationAccueil parametresFormationAccueil) {
        List<Filtre> filters = new ArrayList<>();

        // cas de la mise � jour, on enl�ve l'�l�ment lui m�me
        Long parametresFormationAccueilId = parametresFormationAccueil.getId();
        if (parametresFormationAccueilId != null) {
            filters.add(Filtre.notEqual("id", parametresFormationAccueilId));
        }

        filters.add(Filtre.equal("formationAccueil", parametresFormationAccueil.getFormationAccueil()));
        filters.add(Filtre.equalOrIsNull("matiereEnseigOptionnel",
                parametresFormationAccueil.getMatiereEnseigOptionnel()));

        return getNombreElements(filters) == 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ParametresFormationAccueil[] listerPfaFormation(Formation formation) {

        // r�cup�ration de la liste de tous les PARAMEFA qui utilisent la
        // formation d'accueil tri�e dans l'ordre de leur application
        Session s = getSession();
        List<ParametresFormationAccueil> lPfa;
        Criteria c = s.createCriteria(ParametresFormationAccueil.class);
        c.add(Restrictions.eq("formationAccueil.id.mnemonique", formation.getId().getMnemonique()));
        c.add(Restrictions.eq("formationAccueil.id.codeSpecialite", formation.getId().getCodeSpecialite()));
        lPfa = new ArrayList<>(c.list());

        TreeSet<ParametresFormationAccueil> listeTriee = new TreeSet<>(new ParametresFormationAccueilComparator());
        listeTriee.addAll(lPfa);

        // ATTENTION le ParametresFormationAccueilComparator donne les �l�ments dans l'ordre inverse
        Object[] pfas = listeTriee.toArray();

        int nbPfas = pfas.length;
        ParametresFormationAccueil[] result = new ParametresFormationAccueil[nbPfas];

        // inversion du tableau
        for (int i = 0; i < nbPfas; i++) {
            result[i] = (ParametresFormationAccueil) pfas[nbPfas - 1 - i];
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> modeBaremeParOpa(Long id, short numeroTour) {
        Session s = getSession();
        Criteria criteriaPfa = s.createCriteria(ParametresFormationAccueil.class);
        DetachedCriteria criteriaPile = DetachedCriteria.forClass(Pile.class);

        // on consid�re les piles du tour demand�
        criteriaPile.add(Restrictions.eq("id.numeroTour", numeroTour));

        if (numeroTour == 0) {
            // On prend les offres de formation li�es � l'OPA en tour principal
            criteriaPile.createAlias("voeu", "voeu");
            criteriaPile.createAlias("voeu.opaTourPrincipal", "opa");
            criteriaPile.add(Restrictions.eq("opa.id", id));
        }

        criteriaPile.setProjection(Projections.property("parametreFormationAccueil.id"));

        criteriaPfa.add(Property.forName("id").in(criteriaPile));
        criteriaPfa.setProjection(Projections.distinct(Projections.property("modeBareme")));

        return criteriaPfa.list();
    }

    @Override
    public Filtre filtreEleveAvecVoeuEval(short tourCourant) {
        // Filtre des voeux avec un mode de calcul du bar�me de type �valuation
        String selectionVoeux = "select distinct v.id.ine from VoeuEleve v where v.voeu.code is not null "
                + "and (v.voeu.indicateurPam = '" + TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur()
                + "' and v.voeu.pile.parametreFormationAccueil.modeBareme = '"
                + ParametresFormationAccueil.MODE_BAREME_EVALUATION + "' and v.id.numeroTour = '" + tourCourant
                + "')";

        String filtreString = " o.ine in (" + selectionVoeux + ")";

        return Filtre.hql(filtreString);
    }

    @Override
    public ParametresFormationAccueil recuprererParamefaExactAvecFormationEtEO(Formation formation,
            String cleGestionEO) {
        return recuprererParamefaExactAvecFormationEtEO(formation.getId().getMnemonique(),
                formation.getId().getCodeSpecialite(), cleGestionEO);
    }

    @Override
    public ParametresFormationAccueil recuprererParamefaExactAvecFormationEtEO(String codeSpecialite,
            String mnemonique, String cleGestionEO) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("formationAccueil.id.mnemonique", mnemonique));
        filtres.add(Filtre.equal("formationAccueil.id.codeSpecialite", codeSpecialite));
        filtres.add(Filtre.equalOrIsNull("matiereEnseigOptionnel.cleGestion", cleGestionEO));

        List<ParametresFormationAccueil> pfaList = lister(filtres);

        if (pfaList.isEmpty()) {
            return null;
        }
        return pfaList.get(0);
    }
}
