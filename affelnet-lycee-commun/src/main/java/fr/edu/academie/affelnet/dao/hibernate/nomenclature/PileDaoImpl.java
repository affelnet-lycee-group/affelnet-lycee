/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.PileDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Pile;
import fr.edu.academie.affelnet.domain.nomenclature.PilePK;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;

/**
 * Implementation de la gestion des piles pour les offres de formation.
 */
@Repository("PileDao")
public class PileDaoImpl extends BaseDaoHibernate<Pile, PilePK> implements PileDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("codeVoeu"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected void setKey(Pile pile, PilePK key) {
        pile.setId(key);
    }

    @Override
    protected boolean hasKeyChange(Pile pile, PilePK oldKey) {
        return !pile.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� une pile avec cette offre de formation et pour ce tour";
    }

    @Override
    protected Class<Pile> getObjectClass() {
        return Pile.class;
    }

    @Override
    protected String getMessageObjectNotFound(PilePK key) {
        return "La pile pour l'offre et le tour indiqu� (" + key + ") n'existe pas ";
    }

    @Override
    public List<Object[]> listeVoeuxParametresFormationAccueilPourOpa(Long idOpa) {
        return listeVoeuxParametresFormationAccueilPourTour(null, idOpa, null);
    }

    @Override
    public List<Object[]> listeVoeuxParametresFormationAccueilPourOpa(Long idOpa, String modeBareme) {
        return listeVoeuxParametresFormationAccueilPourTour(null, idOpa, modeBareme);
    }

    @Override
    public List<Object[]> listeVoeuxParametresFormationAccueilPourTour(Short numTour, String modeBareme) {
        return listeVoeuxParametresFormationAccueilPourTour(numTour, null, modeBareme);
    }

    /**
     * M�thode permettant de r�cup�rer la liste des voeux et des param�tres par formation accueil pour un tour
     * donn� en fonction du mode de bar�me.
     * Il est n�cessaire que soit le num�ro du tour, soit l'id de l'OPA soit null.
     * 
     * @param numTour
     *            le num�ro du tour concern�
     * @param modeBareme
     *            le mode de bar�me cherch�
     * @param idOpa
     *            l'identifiant des op�rations programm�es
     * @return une liste d'objet contenant en premi�re position l'id du paramefa et en seconde position le code de
     *         l'offre de formation
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> listeVoeuxParametresFormationAccueilPourTour(Short numTour, Long idOpa,
            String modeBareme) {
        Criteria criteria = criteriaPileParOpaPourTour(numTour, idOpa, modeBareme);

        ProjectionList projList = Projections.projectionList();
        projList.add(Projections.property("parametreFormationAccueil"));
        projList.add(Projections.property("voeu"));
        criteria.setProjection(projList);

        List<Object[]> listePFAVoeuTourCourant = criteria.list();

        return listePFAVoeuTourCourant;
    }

    /**
     * G�n�re un criteria centr� sur la pile liant les param�tres par formation d'accueil et les OPA.
     * Il est n�cessaire que soit le num�ro de tour, soit l'ID de l'OPA soit null.
     * 
     * @param numTour
     *            le tour actuel (nullable si idOpa fourni)
     * @param idOpa
     *            l'id de l'OPA (nullable si numTour fourni)
     * @param modeBareme
     *            le mode de calcul du bar�me (nullable)
     * @return un criteria de Pile.class, li� aux paramefa (alias : pfa), aux offres de formations (alias : voeu)
     *         et aux OPA (alias : opa)
     */
    private Criteria criteriaPileParOpaPourTour(Short numTour, Long idOpa, String modeBareme) {
        Session session = getSession();
        short valTour;
        if (numTour == null) {
            Criteria criteriaOpa = session.createCriteria(OperationProgrammeeAffectation.class);
            criteriaOpa.add(Restrictions.eq("id", idOpa));
            criteriaOpa.setProjection(Projections.property("numeroTourSuivant"));
            valTour = ((Number) criteriaOpa.uniqueResult()).shortValue();
        } else {
            valTour = numTour;
        }

        // R�cup�ration des param�tres par formation du tour courant dans la pile pour les offres de formation
        Criteria criteria = session.createCriteria(Pile.class);
        criteria.add(Restrictions.eq("id.numeroTour", valTour));
        criteria.createAlias("parametreFormationAccueil", "pfa");
        if (StringUtils.isNotBlank(modeBareme)) {
            criteria.add(Restrictions.eq("pfa.modeBareme", modeBareme));
        }
        criteria.addOrder(Order.asc("pfa.id"));
        criteria.createAlias("voeu", "voeu");
        criteria.add(Restrictions.eqProperty("voeu.code", "codeVoeu"));
        if (valTour <= 0 && idOpa != null) {
            criteria.createCriteria("voeu.opaTourPrincipal", "opa");
            criteria.add(Restrictions.eq("opa.id", idOpa));
        }
        return criteria;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Integer, Integer> compteOffreFormationPourTourSuivant() {
        Session session = getSession();

        Criteria criteria = session.createCriteria(Pile.class);
        criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("id.numeroTour"))
                .add(Projections.count("id.codeOffreFormation")));
        criteria.add(Restrictions.gt("capaciteAffectation", 0));
        criteria.createAlias("voeu", "voeu");
        criteria.add(Restrictions.eq("voeu.flagRecensement", Flag.NON));
        List<Object[]> arrayList = criteria.list();

        Map<Integer, Integer> nbOffreFormationParTour = new HashMap<Integer, Integer>();
        for (Object[] couple : arrayList) {
            if (couple.length == 2) {
                nbOffreFormationParTour.put(((Number) couple[0]).intValue(), ((Number) couple[1]).intValue());
            }
        }
        return nbOffreFormationParTour;
    }
}
