/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.RapprochEtabDao;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.RapprochEtab;

/**
 * Implementation de la gestion des rapprochements entre �tablissements.
 */
@Repository("RapprochEtabDao")
public class RapprochEtabDaoImpl extends BaseDaoHibernate<RapprochEtab, Long> implements RapprochEtabDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("idEtablissementOrigine"));
        DEFAULT_ORDERS.add(Tri.asc("etablissementAccueil.id"));
        DEFAULT_ORDERS.add(Tri.asc("mnemoniqueAccueil"));
        DEFAULT_ORDERS.add(Tri.asc("codeSpecialiteAccueil"));
        DEFAULT_ORDERS.add(Tri.asc("matiereEnseigOptionnel.cleGestion"));
        DEFAULT_ORDERS.add(Tri.asc("id"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "Le rapprochement �tablissement " + key + " n'existe pas";
    }

    @Override
    protected void setKey(RapprochEtab rapprochEtab, Long key) {
        rapprochEtab.setId(key);
    }

    @Override
    protected boolean hasKeyChange(RapprochEtab rapprochEtab, Long oldKey) {
        return !rapprochEtab.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un rapprochement avec ces �tablissements et cette formation";
    }

    @Override
    protected Class<RapprochEtab> getObjectClass() {
        return RapprochEtab.class;
    }

    @Override
    public boolean isUnique(RapprochEtab rapprochEtab) {
        List<Filtre> filters = new ArrayList<>();

        // cas de la mise � jour, on enl�ve l'�l�ment lui m�me
        Long rapprochEtabId = rapprochEtab.getId();
        if (rapprochEtabId != null) {
            filters.add(Filtre.notEqual("id", rapprochEtabId));
        }

        filters.add(Filtre.equalOrIsNull("idEtablissementOrigine", rapprochEtab.getIdEtablissementOrigine()));
        filters.add(Filtre.equal("etablissementAccueil", rapprochEtab.getEtablissementAccueil()));

        filters.add(Filtre.equal("mnemoniqueAccueil", rapprochEtab.getMnemoniqueAccueil()));
        filters.add(Filtre.equalOrIsNull("codeSpecialiteAccueil", rapprochEtab.getCodeSpecialiteAccueil()));

        filters.add(Filtre.equal("mnemoniqueOrigine", rapprochEtab.getMnemoniqueOrigine()));
        filters.add(Filtre.equalOrIsNull("codeSpecialiteOrigine", rapprochEtab.getCodeSpecialiteOrigine()));

        Matiere matiereEnseigOptionnel = rapprochEtab.getMatiereEnseigOptionnel();
        filters.add(Filtre.equalOrIsNull("matiereEnseigOptionnel", matiereEnseigOptionnel));


        return getNombreElements(filters) <= 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Matiere> getOptions() {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des rapprochements
            List<Matiere> listeOptions = session
                    .createQuery("select distinct m from Matiere m where m.cleGestion in "
                            + " (select distinct r.matiereEnseigOptionnel.cleGestion from RapprochEtab r) "
                            + " order by 1")
                    .list();
            return listeOptions;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RapprochEtab> listerRapprochEtabPourEtabAccueil(Etablissement etabAccueil) {
        Criteria c = getSession().createCriteria(RapprochEtab.class);
        c.add(Restrictions.eq("etablissementAccueil", etabAccueil));
        return c.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<RapprochEtab> listerRapprochEtabAvecJointureFetch() {
        Criteria c = getSession().createCriteria(RapprochEtab.class);
        c.setFetchMode("formationAccueil", FetchMode.JOIN);
        c.setFetchMode("matiereEnseigOptionnel", FetchMode.JOIN);
        c.setFetchMode("etablissementAccueil", FetchMode.JOIN);
        c.setFetchMode("etablissementOrigine", FetchMode.JOIN);
        return c.list();
    }
}
