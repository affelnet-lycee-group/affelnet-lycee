/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.VoeuDao;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;

/**
 * Implementation de la gestion de l'offre de formation.
 */
@Repository("VoeuDao")
public class VoeuDaoImpl extends BaseDaoHibernate<Voeu, String> implements VoeuDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("code"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        return "L'offre de formation " + key + " n'existe pas";
    }

    @Override
    protected void setKey(Voeu voeu, String key) {
        voeu.setCode(key);
    }

    @Override
    protected boolean hasKeyChange(Voeu voeu, String oldKey) {
        return !voeu.getCode().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce code";
    }

    @Override
    protected Class<Voeu> getObjectClass() {
        return Voeu.class;
    }

    @Override
    public synchronized String getCode(String prefixeVoeu) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            long count = ((Number) session.createQuery("select count(*) from Voeu v where v like '" + prefixeVoeu
                    + "%' and substring(v.code,1,4) <> '" + prefixeVoeu + "9'").iterate().next()).longValue();
            if (count > 0) {
                String codeMax = (String) session.createQuery("select max(v.code) from Voeu v where v like '"
                        + prefixeVoeu + "%' and substring(v.code,1,4) <> '" + prefixeVoeu + "9'").uniqueResult();
                int i;
                try {
                    i = Integer.parseInt(codeMax.substring(3));
                    return prefixeVoeu + (i + 1);
                } catch (Exception e) {
                    i = Integer.parseInt(codeMax.substring(4));
                    return prefixeVoeu + 'N' + (i + 1);
                }
            } else {
                return prefixeVoeu + "11111";
            }
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public boolean existeVoeu(String codeVoeu) {
        try {
            // r�cup�ration de la session
            Session session = getSession();
            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select count(*) from Voeu v ");
            requete.append("where v.code = '" + codeVoeu + "' ");
            long i = ((Number) session.createQuery(requete.toString()).iterate().next()).longValue();
            return (i > 0);
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public boolean isUnique(Voeu offreFormation) {
        List<Filtre> filters = new ArrayList<>();

        // cas de la mise � jour, on enl�ve l'�l�ment lui m�me
        String codeOffre = offreFormation.getCode();
        if (codeOffre != null) {
            filters.add(Filtre.notEqual("code", codeOffre));
        }

        filters.add(Filtre.equal("etablissement", offreFormation.getEtablissement()));

        filters.add(Filtre.equal("formationAccueil", offreFormation.getFormationAccueil()));

        filters.add(Filtre.equal("statut", offreFormation.getStatut()));

        Matiere matiereEnseigOptionnel = offreFormation.getMatiereEnseigOptionnel();

        filters.add(Filtre.equalOrIsNull("matiereEnseigOptionnel", matiereEnseigOptionnel));

        return getNombreElements(filters) == 0;
    }

    @Override
    public List<Voeu> listerOffresAvecPriseEnCompteNoteEvalSansPFA(Long idOpa) {
        List<Filtre> filtres = new ArrayList<>();
        // Bar�me avec prise en compte des notes/eval
        filtres.add(Filtre.equal("indicateurPam", TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur()));
        // Pas de param�tre par formation d'accueil existant
        filtres.add(Filtre.isNull("pile.parametreFormationAccueil"));

        // Si l'OPA est indiqu�e
        if (idOpa != null) {
            // Restriction sur les offres li�es � l'OPA pour le tour principal
            Filtre filtreLienOpa = Filtre.equal("opaTourPrincipal.id", idOpa);
            // Ou OPA de tour suivant
            Filtre filtreOpaTourSuivant = Filtre.exists(OperationProgrammeeAffectation.class,
                    Filtre.and(Filtre.superieur("numeroTourSuivant", 0), Filtre.equal("id", idOpa)));
            filtres.add(Filtre.or(filtreLienOpa, filtreOpaTourSuivant));
        }

        List<String> fetchList = new ArrayList<>();
        fetchList.add("etablissement");
        fetchList.add("etablissement.typeEtablissement");

        return lister(filtres, null, null, null, fetchList, true);
    }

    @Override
    public List<Voeu> listerOffresFormationIdentiques() {

        List<Filtre> sousRequeteFiltres = new ArrayList<>();
        // On regarde si il existe une offre de formation diff�rente
        sousRequeteFiltres.add(Filtre.not(Filtre.eqPropertiesPere("code", "voeu.code")));
        // Pour le m�me etablissement
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("etablissement", "voeu.etablissement"));
        // La m�me formation
        sousRequeteFiltres.add(
                Filtre.eqPropertiesPere("formationAccueil.id.mnemonique", "voeu.formationAccueil.id.mnemonique"));
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("formationAccueil.id.codeSpecialite",
                "voeu.formationAccueil.id.codeSpecialite"));

        // Le m�me statut
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("statut", "voeu.statut"));

        // Le m�me enseignement optionnel
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("matiereEnseigOptionnel", "voeu.matiereEnseigOptionnel"));

        // cr�ation du filtre exists
        Filtre filtreExists = Filtre.exists(Voeu.class, Filtre.and(sousRequeteFiltres));
        // L'enseignement optionnel doit �tre non null
        Filtre filtreAvecEO = Filtre.and(Filtre.isNotNull("matiereEnseigOptionnel"), filtreExists);

        // Deuxi�me requ�te pour un EO null
        sousRequeteFiltres = new ArrayList<>();
        sousRequeteFiltres.add(Filtre.not(Filtre.eqPropertiesPere("code", "voeu.code")));
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("etablissement", "voeu.etablissement"));
        sousRequeteFiltres.add(
                Filtre.eqPropertiesPere("formationAccueil.id.mnemonique", "voeu.formationAccueil.id.mnemonique"));
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("formationAccueil.id.codeSpecialite",
                "voeu.formationAccueil.id.codeSpecialite"));

        // Le m�me statut
        sousRequeteFiltres.add(Filtre.eqPropertiesPere("statut", "voeu.statut"));

        // Enseignement optionnel null
        sousRequeteFiltres.add(Filtre.isNull("matiereEnseigOptionnel"));

        // cr�ation du filtre exists
        filtreExists = Filtre.exists(Voeu.class, Filtre.and(sousRequeteFiltres));
        // Enseignement optionnel null
        Filtre filtreSansEO = Filtre.and(Filtre.isNull("matiereEnseigOptionnel"), filtreExists);

        return lister(Filtre.or(filtreAvecEO, filtreSansEO));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<Voeu, Boolean> offresFormationsSansOpa() {
        Map<Voeu, Boolean> offres = new LinkedHashMap<>();
        Query q = HibernateUtil.currentSession()
                .getNamedQuery("hql.planificationCampagneAffectation.offresFormationSansOpa");

        List<Object[]> results = q.list();
        for (Object[] ligne : results) {
            if (ligne.length == 2) {
                Voeu offre = (Voeu) ligne[0];
                Boolean presenceVoeu = ligne[1] != null;

                offres.put(offre, presenceVoeu);
            }
        }

        return offres;
    }
}
