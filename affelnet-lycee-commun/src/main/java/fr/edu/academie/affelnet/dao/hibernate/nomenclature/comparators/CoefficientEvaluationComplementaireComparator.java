/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.comparators;

import java.io.Serializable;
import java.util.Comparator;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementaire;

/**
 * Le comparateur permettant de trier les coefficients de param�tres par formation d'acceuil.
 *
 */
public class CoefficientEvaluationComplementaireComparator
        implements Serializable, Comparator<CoefficientEvaluationComplementaire> {

    /**
     * Taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(CoefficientEvaluationComplementaire o1, CoefficientEvaluationComplementaire o2) {
        Integer ordre1 = o1.getEvalComp().getOrdreAffichage();
        Integer ordre2 = o2.getEvalComp().getOrdreAffichage();
        return ordre1.compareTo(ordre2);
    }

}
