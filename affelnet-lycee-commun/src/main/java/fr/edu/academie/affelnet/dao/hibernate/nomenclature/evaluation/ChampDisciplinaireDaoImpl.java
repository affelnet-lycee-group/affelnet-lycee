/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.ChampDisciplinaireDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;

/**
 * Impl�mentation de la gestion des champs disciplinaire.
 *
 */
@Repository("ChampDisciplinaireDao")
public class ChampDisciplinaireDaoImpl extends BaseDaoHibernate<ChampDisciplinaire, String>
        implements ChampDisciplinaireDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("valeurOrdre"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        return "Le champ disciplinaire " + key + " n'existe pas";
    }

    @Override
    protected void setKey(ChampDisciplinaire cd, String key) {
        cd.setCode(key);

    }

    @Override
    protected boolean hasKeyChange(ChampDisciplinaire cd, String oldKey) {
        return !cd.getCode().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce code de champ disciplinaire";
    }

    @Override
    protected Class<ChampDisciplinaire> getObjectClass() {
        return ChampDisciplinaire.class;
    }

}
