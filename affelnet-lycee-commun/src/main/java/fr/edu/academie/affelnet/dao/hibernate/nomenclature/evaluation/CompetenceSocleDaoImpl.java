/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.CompetenceSocleDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;

/**
 * Impl�mentation du Dao pour les comp�tences du socle.
 */
@Repository("CompetenceSocleDaoImpl")
public class CompetenceSocleDaoImpl extends BaseDaoHibernate<CompetenceSocle, String>
        implements CompetenceSocleDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("ordre"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        return "La comp�tence de code " + key + " n'a pas �t� trouv�e";
    }

    @Override
    protected void setKey(CompetenceSocle object, String key) {
        object.setCode(key);
    }

    @Override
    protected boolean hasKeyChange(CompetenceSocle object, String oldKey) {
        return !object.getCode().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "la comp�tence existe d�j�";
    }

    @Override
    protected Class<CompetenceSocle> getObjectClass() {
        return CompetenceSocle.class;
    }
}
