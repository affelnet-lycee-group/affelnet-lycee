/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.DegreMaitriseDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DegreMaitrise;

/**
 * Impl�mentation du Dao pour les groupes de niveau.
 */
@Repository("DegreMaitriseDaoImpl")
public class DegreMaitriseDaoImpl extends BaseDaoHibernate<DegreMaitrise, Integer> implements DegreMaitriseDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        return new ArrayList<Tri>();
    }

    @Override
    protected String getMessageObjectNotFound(Integer key) {
        return "Le groupe de niveau n'a pas �t� trouv�";
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Le groupe de niveau existe d�j�.";
    }

    @Override
    protected void setKey(DegreMaitrise object, Integer key) {
        object.setValeur(key);
    }

    @Override
    protected boolean hasKeyChange(DegreMaitrise object, Integer oldKey) {
        return !(object.getValeur().equals(oldKey));
    }

    @Override
    protected Class<DegreMaitrise> getObjectClass() {
        return DegreMaitrise.class;
    }
}
