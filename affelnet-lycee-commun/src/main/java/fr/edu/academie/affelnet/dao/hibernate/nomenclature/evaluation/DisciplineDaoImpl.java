/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.DisciplineDao;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;

/**
 * Impl�mentation du Dao pour les disciplines.
 */
@Repository("DisciplineDaoImpl")
public class DisciplineDaoImpl extends BaseDaoHibernate<Discipline, Long> implements DisciplineDao {

    /** Liste des ordres par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("ordre"));
        DEFAULT_ORDERS.add(Tri.asc("champDisciplinaire.valeurOrdre"));
        DEFAULT_ORDERS.add(Tri.asc("id"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "La discipline " + key + " n'a pas �t� trouv�e";
    }

    @Override
    protected void setKey(Discipline object, Long key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(Discipline object, Long oldKey) {
        return object.getId() != oldKey;
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "la discipline existe d�j�";
    }

    @Override
    protected Class<Discipline> getObjectClass() {
        return Discipline.class;
    }

    @Override
    public Discipline charger(MefStat mefStat, String codeMat) {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(Discipline.class)
                    .add(Restrictions.eq("codeMatiere", codeMat)).add(Restrictions.eq("mefStat", mefStat));
            return (Discipline) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
