/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.DomaineSpecialiteDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DomaineSpecialite;

/**
 * Impl�mentation de la gestion du domaine de sp�cialit�.
 *
 */
@Repository("DomaineSpecialiteDao")
public class DomaineSpecialiteDaoImpl extends BaseDaoHibernate<DomaineSpecialite, String>
        implements DomaineSpecialiteDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("code"));
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> getListeCode() {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche de tous les codes
            List<String> listeCode = session.createQuery("select code from DomaineSpecialite").list();
            return listeCode;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getListeLibelle() {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche de tous les codes
            List<String> listLibelle = session.createQuery("select libelle from DomaineSpecialite").list();
            return listLibelle;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public boolean containsCodeFormation(Formation formation) {
        MefStat mefStat = formation.getMefStat();
        String codeMefStat = mefStat.getCode();
        // r�cup�ration du 7i�me et 8i�me caract�res du MEFSTAT11
        String domSpeCodeMefStat = codeMefStat.substring(7, 9);
        // r�cup�ration de la liste des codes
        List<String> listeCode = getListeCode();
        // recherche si le code existe dans la liste
        return listeCode.contains(domSpeCodeMefStat);
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(String key) {
        return "Le domaine de sp�cialit� du code " + key + " n'existe pas";
    }

    @Override
    protected void setKey(DomaineSpecialite ds, String key) {
        ds.setCode(key);

    }

    @Override
    protected boolean hasKeyChange(DomaineSpecialite ds, String oldcode) {
        return !ds.getCode().equals(oldcode);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un domaine de sp�cialit� avec ce code";
    }

    @Override
    protected Class<DomaineSpecialite> getObjectClass() {
        return DomaineSpecialite.class;
    }

    @Override
    public boolean existeCodeFormation(String code) {
        try {
            // r�cup�ration de la session
            Session session = getSession();
            // recherche de tous les codes
            String requete = "select count(*) from DomaineSpecialite" + " where code='" + code + "'";
            return ((Number) session.createQuery(requete).iterate().next()).longValue() > 0;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

}
