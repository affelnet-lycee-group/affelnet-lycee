/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.EchelonEvaluationDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EchelonEvaluation;

/**
 * Impl�mentation de la gestion des �chelons des �valuations.
 *
 */
@Repository("EchelonEvaluationDao")
public class EchelonEvaluationDaoImpl extends BaseDaoHibernate<EchelonEvaluation, Integer>
        implements EchelonEvaluationDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("valeurEchelon"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec cet valeur d'�chelon ";
    }

    @Override
    protected Class<EchelonEvaluation> getObjectClass() {
        return EchelonEvaluation.class;
    }

    @Override
    protected String getMessageObjectNotFound(Integer key) {
        return "L'�chelon d'�valuation de la valeur" + key + " n'existe pas";
    }

    @Override
    protected void setKey(EchelonEvaluation object, Integer key) {
        object.setValeurEchelon(key);

    }

    @Override
    protected boolean hasKeyChange(EchelonEvaluation object, Integer oldKey) {
        return !object.getValeurEchelon().equals(oldKey);
    }

}
