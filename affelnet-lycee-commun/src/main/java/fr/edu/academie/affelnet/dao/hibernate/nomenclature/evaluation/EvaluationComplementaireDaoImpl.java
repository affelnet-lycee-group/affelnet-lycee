/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.dialect.DB2Dialect;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.EvaluationComplementaireDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;

/**
 * Impl�mentation de la gestion des �valuations compl�mentaires.
 *
 */
@Repository("EvaluationComplementaireDao")
public class EvaluationComplementaireDaoImpl extends BaseDaoHibernate<EvaluationComplementaire, Long>
        implements EvaluationComplementaireDao {
    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    /** Logger de la classe. */
    private static final Log LOGGER = LogFactory.getLog(EvaluationComplementaireDaoImpl.class);

    static {
        DEFAULT_ORDERS.add(Tri.asc("ordreAffichage"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(Long key) {
        return "L'�valuation compl�mentaire " + key + " n'existe pas";
    }

    @Override
    protected void setKey(EvaluationComplementaire object, Long key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(EvaluationComplementaire object, Long oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec cet identifiantpour cette �valuation compl�mentaire";
    }

    @Override
    protected Class<EvaluationComplementaire> getObjectClass() {
        return EvaluationComplementaire.class;
    }

    @Override
    public EvaluationComplementaire maj(EvaluationComplementaire object, Long oldKey) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            if (!hasKeyChange(object, oldKey)) {
                // chargement de l'ancien composant
                EvaluationComplementaire oldObject = charger(oldKey);

                // Copie des propri�t�s

                oldObject.majProperties(object);
                session.flush();

                return oldObject;
            } else {
                EvaluationComplementaire newObject = new EvaluationComplementaire();

                try {
                    BeanUtils.copyProperties(newObject, object);
                } catch (IllegalAccessException e) {
                    throw new DataAccessException(e);
                } catch (InvocationTargetException e) {
                    throw new DataAccessException(e);
                }
                // suppression de l'ancien objet
                supprimer(charger(oldKey));

                return creer(newObject);
            }

        } catch (NonUniqueObjectException e) {
            throw new DaoException(getMessageDuplicateKey(), e);
        } catch (JDBCException e) {
            if (StringUtils.equals(DB2Dialect.class.getName(), getDialect())
                    && (e.getSQLState().equals("23505"))) {
                // sous MySQL -> SQLSTATE = 23000
                // sous DB2 -> SQLSTATE = 23505
                LOGGER.error("La mise � jour a �chou� suite � une v�rification de contrainte d'unicit�.", e);
                throw new DaoException(getMessageDuplicateKey(), e);
            }
            throw new DataAccessException(e);
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void updateOrdre(int ancienOrdre, int nouvelOrdre) {
        Query q = DaoUtils.getNamedQuery("hql.update.EvalCompOrdre");
        q.setParameter("ancienOrdre", ancienOrdre);
        q.setParameter("nouvelOrdre", nouvelOrdre);
        q.executeUpdate();
    }

    @Override
    public void updateOrdreAutreEC(int ancienOrdre, int nouvelOrdre, Long id) {
        Query q = DaoUtils.getNamedQuery("hql.update.EvalCompIdOrdre");
        q.setParameter("ancienOrdre", ancienOrdre);
        q.setParameter("nouvelOrdre", nouvelOrdre);
        q.setParameter("id", id);
        q.executeUpdate();

    }
}
