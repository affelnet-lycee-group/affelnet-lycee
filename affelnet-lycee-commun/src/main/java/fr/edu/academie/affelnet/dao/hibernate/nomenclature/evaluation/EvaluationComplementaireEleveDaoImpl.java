/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.EvaluationComplementaireEleveDao;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireElevePK;

/**
 * Impl�mentation de la gestion des �valuations compl�mentaires.
 *
 */
@Repository("EvaluationComplementaireEleveDao")
public class EvaluationComplementaireEleveDaoImpl
        extends BaseDaoHibernate<EvaluationComplementaireEleve, EvaluationComplementaireElevePK>
        implements EvaluationComplementaireEleveDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();
    static {
        DEFAULT_ORDERS.add(Tri.asc("evaluationComplementaireElevePK.id"));
        DEFAULT_ORDERS.add(Tri.asc("evaluationComplementaireElevePK.ine"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(EvaluationComplementaireElevePK key) {
        return "L'�valuation compl�mentaire du couple " + key + " n'existe pas";
    }

    @Override
    protected void setKey(EvaluationComplementaireEleve object, EvaluationComplementaireElevePK key) {
        object.setEvaluationComplementaireElevePK(key);
    }

    @Override
    protected boolean hasKeyChange(EvaluationComplementaireEleve object, EvaluationComplementaireElevePK oldKey) {
        return !object.getEvaluationComplementaireElevePK().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec cet identifiantpour cette �valuation compl�mentaire et cet �l�ve";
    }

    @Override
    protected Class<EvaluationComplementaireEleve> getObjectClass() {
        return EvaluationComplementaireEleve.class;
    }

    @Override
    public boolean isEvaluationSaisies() {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(EvaluationComplementaireEleve.class)
                    .setProjection(Projections.rowCount());
            return ((Number) criteria.uniqueResult()).intValue() > 0;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EvaluationComplementaireEleve> listerParEleve(String ine) {
        try {
            Session session = getSession();
            Criteria criteria = session.createCriteria(EvaluationComplementaireEleve.class)
                    .add(Restrictions.eq("id.eleve", ine));
            return criteria.list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void purger(String ine) {
        try {
            Session session = getSession();
            String hql = "delete from EvaluationComplementaireEleve e where e.id.eleve.ine = :ine";
            session.createQuery(hql).setParameter("ine", ine).executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public Filtre filtreEvalComAvecGroupeOrigine() {
        return Filtre.isNotNull("eleve.groupeOrigine");
    }
}
