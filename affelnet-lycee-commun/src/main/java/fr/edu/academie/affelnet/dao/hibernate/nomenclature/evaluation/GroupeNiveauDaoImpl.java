/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.nomenclature.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.GroupeNiveauDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;

/**
 * Impl�mentation du Dao pour les groupes de niveau.
 */
@Repository("GroupeNiveauDaoImpl")
public class GroupeNiveauDaoImpl extends BaseDaoHibernate<GroupeNiveau, Integer> implements GroupeNiveauDao {

    @Override
    protected List<Tri> getDefaultOrders() {
        return new ArrayList<Tri>();
    }

    @Override
    protected String getMessageObjectNotFound(Integer key) {
        return "Le groupe de niveau n'a pas �t� trouv�";
    }

    @Override
    protected void setKey(GroupeNiveau object, Integer key) {
        object.setNiveau(key);
    }

    @Override
    protected boolean hasKeyChange(GroupeNiveau object, Integer oldKey) {
        return object.getNiveau() != oldKey;
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Le groupe de niveau existe d�j�.";
    }

    @Override
    protected Class<GroupeNiveau> getObjectClass() {
        return GroupeNiveau.class;
    }
}
