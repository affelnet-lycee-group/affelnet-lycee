/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.hibernate.BatchDaoImpl;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.nomenclature.GroupeOrigineDaoImpl;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.pam.AttributionGroupeDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigineFormation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.helper.MatiereHelper;

/** Traitement d'attribution des groupes origine aux �l�ves candidats pour toute la campagne d'affectation. */
@Repository("AttributionGroupeDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AttributionGroupeDaoImpl extends BatchDaoImpl implements AttributionGroupeDao {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(AttributionGroupeDaoImpl.class);

    @Override
    public void lancerTraitement() {

        // Attention l'audit pr�-affectation doit �tre lanc� avant l'attribution des groupes
        messages.start("Attribution des groupes");
        reinitialiserGroupes();
        attribuerGroupes();

        // Nettoyage de la session en cours
        HibernateUtil.cleanupSession();

        messages.end();
    }

    /**
     * R�initialise les groupes origine de tous les �l�ves ainsi que les compteurs de groupe.
     * 
     * <ul>
     * <li>Remet � 'null' le groupe de l'�l�ve</li>
     * <li>Remet � z�ro les compteurs du groupe.</li>
     * </ul>
     * 
     * @see GroupeOrigineDaoImpl.majEleves
     */
    private void reinitialiserGroupes() {

        Session session = HibernateUtil.currentSession();

        // mise � blanc des compteurs des groupes
        LOG.debug("R�initialisation des compteurs de groupe");
        DaoUtils.executeHibernateQuery(session.createQuery("update GroupeOrigine set nombreEleves=0"));

        // initialisation du groupe des �l�ves
        LOG.debug("R�initialisation des groupe origine des �l�ves");
        DaoUtils.executeHibernateQuery(session.createQuery("update Eleve set groupeOrigine=null"));
    }

    /**
     * Attribuer un groupe origine � chaque �l�ve candidat � l'affectation (ayant formul� au moins un voeu).
     */
    private void attribuerGroupes() {
        Session session = HibernateUtil.currentSession();

        List<GroupeOrigine> groupesOrigine = listerGroupesOrigine(session);

        // Constitue les groupes un par un
        for (int i = 0; i < groupesOrigine.size(); i++) {
            GroupeOrigine groupe = groupesOrigine.get(i);

            // messages sur le traitement en cours
            messages.start("constitution du groupe " + groupe.getCode());

            // Constitution du groupe
            attribuerGroupe(session, groupe);

            // message pour la fin
            messages.end();

            // mise � jour de l'avancement
            messages.setAvancement(i + 1, groupesOrigine.size());
        }
    }

    /**
     * Liste les groupes origine.
     * 
     * @param session
     *            session Hibernate
     * @return liste des groupes origine
     */
    @SuppressWarnings("unchecked")
    private List<GroupeOrigine> listerGroupesOrigine(Session session) {
        // R�cuperation de la listeAttributionGroupeDaoIm des groupes d'origine possibles
        // pour les �l�ves tri�s par code
        Criteria critereGroupeOrigine = session.createCriteria(GroupeOrigine.class);
        critereGroupeOrigine.addOrder(Order.asc("code"));
        return critereGroupeOrigine.list();
    }

    /**
     * Attribue le groupe aux �l�ves concern�s et met � jour les compteurs du nombre d'�l�ves dans le groupe.
     * 
     * @param session
     *            la session Hibernate
     * @param groupe
     *            le groupe � attribuer
     */
    private void attribuerGroupe(Session session, GroupeOrigine groupe) {

        LOG.debug("Constitution du groupe " + groupe.getCode());

        List<String> ines = listerINEPourGroupe(session, groupe);
        int nbEleve = modifierGroupeEleves(session, groupe, ines);
        modifierCompteurGroupe(session, groupe, nbEleve);
    }

    /**
     * Collecte les INE des �l�ves � placer dans le groupe.
     * 
     * @param session
     *            la session Hibernate
     * @param groupe
     *            le groupe origine � constituer
     * @return liste des INE � placer dans le groupe
     */
    @SuppressWarnings("unchecked")
    private List<String> listerINEPourGroupe(Session session, GroupeOrigine groupe) {

        // On s�lectionne tous les voeux d'�l�ves qui r�pondent aux crit�res du groupe
        Criteria c = session.createCriteria(VoeuEleve.class);
        c.createAlias("eleve", "eleve");
        c.createAlias("candidature", "candidature");
        c.createAlias("eleve.etablissementNational", "etab", JoinType.LEFT_OUTER_JOIN);
        c.createAlias("eleve.optionOrigine2", "optO2", JoinType.LEFT_OUTER_JOIN);
        c.createAlias("eleve.optionOrigine3", "optO3", JoinType.LEFT_OUTER_JOIN);

        // L'�l�ve qui a formul� le voeu ne doit pas encore avoir de groupe
        c.add(Restrictions.isNull("eleve.groupeOrigine"));

        // On exclut les �l�ves forc�s globalement
        c.add(Restrictions.ne("candidature.flagEleveForce", Flag.OUI));

        // Crit�res sur le type de saisie de l'�l�ve (acad�mie / hors-acad�mie)
        if (groupe.getIndicateurAcademie().equals(GroupeOrigine.IND_ACA_HORSACA)) {
            c.add(Restrictions.eq("eleve.flagHorsAcademie", Flag.OUI));
        } else if (groupe.getIndicateurAcademie().equals(GroupeOrigine.IND_ACA)) {
            c.add(Restrictions.eq("eleve.flagHorsAcademie", Flag.NON));
        }

        // Crit�res sur le secteur de l'�tablissement d'origine
        if (groupe.getCodePublicPrive().equals(GroupeOrigine.SECTEUR_PUBLIC)) {
            c.add(Restrictions.eq("etab.codeSecteur", Etablissement.SECTEUR_PUBLIC));
        } else if (groupe.getCodePublicPrive().equals(GroupeOrigine.SECTEUR_PRIVE)) {
            c.add(Restrictions.eq("etab.codeSecteur", Etablissement.SECTEUR_PRIVE));

            // gestion contrat
            if (groupe.getCodeContrat().equals(GroupeOrigine.TYPE_CONTRAT_SOUS_CONTRAT)) {
                c.add(Restrictions.eq("etab.typeContrat", Etablissement.TYPE_CONTRAT_SOUS_CONTRAT));
            } else if (groupe.getCodeContrat().equals(GroupeOrigine.TYPE_CONTRAT_HORS_CONTRAT)) {
                c.add(Restrictions.eq("etab.typeContrat", Etablissement.TYPE_CONTRAT_HORS_CONTRAT));
            }
        }

        // Crit�res sur le type d'etablissement d'origine
        if (groupe.getTypeEtablissement1() != null) {
            Criterion critTE = Restrictions.eq("etab.typeEtablissement", groupe.getTypeEtablissement1());
            if (groupe.getTypeEtablissement2() != null) {
                critTE = Restrictions.or(critTE,
                        Restrictions.eq("etab.typeEtablissement", groupe.getTypeEtablissement2()));
                if (groupe.getTypeEtablissement3() != null) {
                    critTE = Restrictions.or(critTE,
                            Restrictions.eq("etab.typeEtablissement", groupe.getTypeEtablissement3()));
                }
            }
            c.add(critTE);
        }

        // Crit�res sur la nature de l'�tablissement d'origine
        if (groupe.getNature1() != null) {
            Criterion critN = Restrictions.eq("etab.nature", groupe.getNature1());
            if (groupe.getNature2() != null) {
                critN = Restrictions.or(critN, Restrictions.eq("etab.nature", groupe.getNature2()));
                if (groupe.getNature3() != null) {
                    critN = Restrictions.or(critN, Restrictions.eq("etab.nature", groupe.getNature3()));
                }
            }
            c.add(critN);
        }

        // Crit�res sur les formations origine associ�es au groupe
        List<Criterion> critGofs = new ArrayList<Criterion>();
        Criteria cGof = session.createCriteria(GroupeOrigineFormation.class);
        cGof.add(Restrictions.eq("groupeOrigine", groupe));
        List<GroupeOrigineFormation> gofs = cGof.list();
        for (Iterator<GroupeOrigineFormation> it = gofs.iterator(); it.hasNext();) {
            GroupeOrigineFormation gof = it.next();
            String mnemo = gof.getMnemonique();
            String coSpe = gof.getCodeSpecialite();
            Matiere opt1 = gof.getOptionOrigine1();
            Matiere opt2 = gof.getOptionOrigine2();

            // cr�ation du crit�re
            Criterion critGof = Restrictions.eq("eleve.formation.id.mnemonique", mnemo);

            // coSpe
            if (coSpe != null) {
                critGof = Restrictions.and(critGof, Restrictions.eq("eleve.formation.id.codeSpecialite", coSpe));
            }

            // opt1
            if (opt1 != null) {
                if (MatiereHelper.isSurMatiereLV(opt1)) {
                    String pattern = "03%" + opt1.getCodeInterne().charAt(5);
                    Criterion c1 = Restrictions.like("optO2.codeInterne", pattern);
                    Criterion c2 = Restrictions.like("optO3.codeInterne", pattern);
                    critGof = Restrictions.and(critGof, Restrictions.or(c1, c2));
                }

                // pas LV
                else {
                    Criterion c1 = Restrictions.eq("optO2.cleGestion", opt1.getCleGestion());
                    Criterion c2 = Restrictions.eq("optO3.cleGestion", opt1.getCleGestion());
                    critGof = Restrictions.and(critGof, Restrictions.or(c1, c2));
                }
            }

            // opt2
            if (opt2 != null) {
                if (MatiereHelper.isSurMatiereLV(opt2)) {
                    String pattern = "03%" + opt2.getCodeInterne().charAt(5);
                    Criterion c1 = Restrictions.like("optO2.codeInterne", pattern);
                    Criterion c2 = Restrictions.like("optO3.codeInterne", pattern);
                    critGof = Restrictions.and(critGof, Restrictions.or(c1, c2));
                }

                // pas LV
                else {
                    Criterion c1 = Restrictions.eq("optO2.cleGestion", opt2.getCleGestion());
                    Criterion c2 = Restrictions.eq("optO3.cleGestion", opt2.getCleGestion());
                    critGof = Restrictions.and(critGof, Restrictions.or(c1, c2));
                }
            }

            // ajout
            critGofs.add(critGof);
        }
        if (critGofs.size() > 0) {
            c.add(HibernateUtil.or(critGofs));
        }

        // On collecte les INE des �l�ves qui constitueront le groupe
        Projection p = Projections.distinct(Projections.property("eleve.ine"));
        c.setProjection(p);
        return c.list();
    }

    /**
     * Modifie le groupe des �l�ves dont l'INE est indiqu�.
     * 
     * @param session
     *            la session Hibernate
     * @param groupe
     *            le groupe � attribuer
     * @param ines
     *            liste des INE des �l�ves � modifier
     * @return le nombre d'�l�ves modifi�s
     */
    private int modifierGroupeEleves(Session session, GroupeOrigine groupe, List<String> ines) {

        int nbElevesTraites = 0;
        int nbElevesATraiter = ines.size();

        try {
            SQLQuery query = session.createSQLQuery("update GV_ELEVE set CO_GRP=:codeGroupe where INE_ELV=:ine");
            query.setString("codeGroupe", groupe.getCode());

            for (String ine : ines) {

                query.setString("ine", ine);
                query.executeUpdate();

                // increment des �l�ves trait�s
                nbElevesTraites++;

                if (nbElevesTraites % 100 == 0) {
                    LOG.debug("�l�ves trait�s : " + nbElevesTraites + " / " + nbElevesATraiter);
                    messages.setAvancement(nbElevesTraites, nbElevesATraiter);
                }

            }
        } catch (HibernateException e) {
            throw new DaoException(e);
        }
        return nbElevesTraites;
    }

    /**
     * Met � jour la valeur du compteur du groupe origine avec le nombre d'�l�ves.
     * 
     * @param session
     *            la session Hibernate
     * @param groupe
     *            le groupe origine concern�
     * @param nbEleves
     *            le nombre d'�l�ves
     */
    private void modifierCompteurGroupe(Session session, GroupeOrigine groupe, int nbEleves) {
        // mise � jour du groupe
        Query updateG = session
                .createQuery("update GroupeOrigine set nombreEleves=:nombreEleves where code=:code");
        updateG.setString("code", groupe.getCode());
        updateG.setInteger("nombreEleves", nbEleves);
        updateG.executeUpdate();
    }
}
