/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam;

import org.springframework.beans.factory.annotation.Autowired;

import fr.edu.academie.affelnet.batch.affectation.BatchAffectation;
import fr.edu.academie.affelnet.dao.hibernate.BatchDaoImpl;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/** Les �tapes de traitement batch d'une OPA. */
public abstract class BatchOpaDaoImpl extends BatchDaoImpl {

    /** Le gestionnaire d'op�rations progamm�es d'affectation. */
    protected OperationProgrammeeAffectationManager operationProgrammeeAffectationManager;

    /**
     * @param operationProgrammeeAffectationManager
     *            Le gestionnaire d'op�rations progamm�es d'affectation
     */
    @Autowired
    public void setOperationProgrammeeAffectationManager(
            OperationProgrammeeAffectationManager operationProgrammeeAffectationManager) {
        this.operationProgrammeeAffectationManager = operationProgrammeeAffectationManager;
    }

    /** @return identifiant programm�e d'affectation contexte. */
    protected long getIdentifiantOperationProgrammeeAffectation() {
        String idOpaStr = params.get(BatchAffectation.PARAMETRE_ID_OPA);
        return Long.parseLong(idOpaStr);
    }
}
