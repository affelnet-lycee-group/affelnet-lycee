/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam;

import java.util.List;

import fr.edu.academie.affelnet.dao.hibernate.utils.MatiereUtils;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/** Correspond � l'aggregat d'une formation et de matieres (options ou enseignement optionnel). */
public final class BigFormation {

    /** La formation. */
    private Formation formation;

    /** Liste des options. */
    private List<Matiere> options;

    /** Enseignement optionnel. */
    private Matiere matiereEnseigOptionnel;

    /**
     * Construit l'aggregat BigFormation pour une formation origine.
     * 
     * @param formation
     *            formation concern�es
     * @param matieres
     *            liste des options concern�es
     */
    public BigFormation(Formation formation, List<Matiere> options) {
        this.formation = formation;
        this.options = options;
    }

    /**
     * Construit l'aggregat BigFormation pour une formation origine.
     * 
     * @param formation
     *            formation concern�es
     * @param ensOpt
     *            Enseignement optionnel
     */
    public BigFormation(Formation formation, Matiere ensOpt) {
        this.formation = formation;
        this.matiereEnseigOptionnel = ensOpt;
    }

    /**
     * Construit un aggregat BigFormation 'affectation' correspondant � partir d'un voeu �l�ve.
     * 
     * @param v
     *            Voeu de l'�l�ve pour lequel construire l'aggregat BigFormation 'affectation'
     * @return l'aggregat BigFormation
     */
    public static BigFormation build(Voeu v) {
        Formation formationA = v.getFormationAccueil();
        Matiere ensOpt = v.getMatiereEnseigOptionnel();
        return new BigFormation(formationA, ensOpt);
    }

    /**
     * Construit un aggregat BigFormation 'origine' correspondant � partir d'un �l�ve.
     * 
     * @param e
     *            �l�ve pour lequel construire l'aggregat BigFormation 'origine'
     * @return l'aggregat BigFormation 'origine'
     */
    public static BigFormation build(Eleve e) {
        Formation formationO = e.getFormation();
        Matiere opt1 = e.getOptionOrigine2();
        Matiere opt2 = e.getOptionOrigine3();
        List<Matiere> lOpt = MatiereUtils.generateCouple(opt1, opt2);
        return new BigFormation(formationO, lOpt);
    }

    /** @return formation concern�e */
    public Formation getFormation() {
        return formation;
    }

    /**
     * @return the options
     */
    public List<Matiere> getOptions() {
        return options;
    }

    /**
     * @return the matiereEnseigOptionnel
     */
    public Matiere getMatiereEnseigOptionnel() {
        return matiereEnseigOptionnel;
    }
}
