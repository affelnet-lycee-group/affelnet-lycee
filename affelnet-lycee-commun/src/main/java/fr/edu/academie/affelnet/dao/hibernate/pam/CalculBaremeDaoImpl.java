/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam;

import fr.edu.academie.affelnet.batch.affectation.BatchAffectation;
import fr.edu.academie.affelnet.batch.affectation.CacheBaremeInfo;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.nomenclature.LienZoneGeoDaoImpl;
import fr.edu.academie.affelnet.dao.hibernate.utils.MatiereUtils;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.BonusDao;
import fr.edu.academie.affelnet.dao.interfaces.pam.CalculBaremeDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.ResultatsProvisoiresOpaDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.affectation.EvaluationChampDisciplinaireOpa;
import fr.edu.academie.affelnet.domain.affectation.EvaluationComplementaireOpa;
import fr.edu.academie.affelnet.domain.affectation.NoteOpa;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.Bonus;
import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.Derogation;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigineModifiee;
import fr.edu.academie.affelnet.domain.nomenclature.LienRangParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.ParametreFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.RapprochEtab;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.nomenclature.TypeBonus;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.VoeuDeFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DegreMaitrise;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.DerogationVoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.service.EvalNoteManager;
import fr.edu.academie.affelnet.service.VoeuEleveManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationSocleManager;
import fr.edu.academie.affelnet.service.nomenclature.BonusFiliereManager;
import fr.edu.academie.affelnet.service.nomenclature.FormationOrigineModifieeManager;
import fr.edu.academie.affelnet.service.nomenclature.ParametreFormationOrigineManager;
import fr.edu.academie.affelnet.service.nomenclature.ParametresFormationAccueilManager;
import fr.edu.academie.affelnet.service.nomenclature.RapprochEtabManager;
import fr.edu.academie.affelnet.service.nomenclature.VoeuDeFiliereManager;
import fr.edu.academie.affelnet.utils.Collecteur;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Impl�mentation de l'�tape de traitement r�alisant le calcul du bar�me pour une op�ration de programm�e
 * d'affectation donn�e.
 */
@Repository("CalculBaremeDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CalculBaremeDaoImpl extends BatchOpaDaoImpl implements CalculBaremeDao {

    /** Coefficient � appliquer sur la somme des points du socle. */
    public static final int COEFF_SOCLE = 12;

    /** Le bar�me total maximal. */
    private static final double BAREME_TOTAL_MAXIMUM = 9999999.999D;

    /** Bonus maximal pour les notes. */
    private static final double BONUS_MAX_NOTES = 99999.999D;

    /** Bonus maximal pour les evaluations. */
    private static final double BONUS_MAX_EVAL = 999999.999D;

    /**
     * Informations associ�es � l'OPA trait�e.
     * A conserver ind�pendamment des sessions Hibernate.
     */
    public class InformationsOPA {

        /** Le bonus acad�mique associ� au premier voeu. */
        private int bonusAcademique;

        /** Le bonus attribu� au premier voeu. */
        private int bonusPremierVoeu;

        /** Le bonus attribu� aux �l�ves boursiers. */
        private int bonusBoursier;

        /** La conservation des r�sultats s�curis�s. */
        private boolean conserverResultatsSecurises;

        /**
         * Cr�e une information d'OPA � partir d'un bean OPA source.
         * 
         * @param operationProgrammeeAffectation
         *            l'OPA source
         */
        public InformationsOPA(OperationProgrammeeAffectation operationProgrammeeAffectation) {

            bonusAcademique = operationProgrammeeAffectation.getBonusAcademique();
            bonusPremierVoeu = operationProgrammeeAffectation.getBonusPremierVoeu();
            bonusBoursier = operationProgrammeeAffectation.getBonusBoursier();
        }

        /**
         * @return la valeur du bonus acad�mique correspondant � l'OPA
         */
        public int getBonusAcademique() {
            return bonusAcademique;
        }

        /**
         * @return la valeur du bonus attribu� au premier voeu correspondant � l'OPA
         */
        public int getBonusPremierVoeu() {
            return bonusPremierVoeu;
        }

        /**
         * @return la valeur du bonus attribu� pour les �l�ves boursiers correspondant � l'OPA.
         */
        public int getBonusBoursier() {
            return bonusBoursier;
        }

        /**
         * @return le boolean indiquant si les r�sultats s�curis�s sont conserv�s.
         */
        public boolean isConserverResultatsSecurises() {
            return conserverResultatsSecurises;
        }

    }

    /** Nombre d'it�rations au bout duquel on nettoie la session. */
    private static final int INTERVALLE_NETTOYAGE_SESSION = 500;

    /** Loggeur de la classe. */
    private static final Log LOG = LogFactory.getLog(CalculBaremeDaoImpl.class);

    /** Table de mise en cache des donn�es globales. */
    private Map<String, Object> cacheMap;

    /**
     * Le Dao utilis� pour les bonus de pr�paration du bar�me.
     */
    private BonusDao bonusDao;

    /**
     * Le DAO des formations origine modifi�es.
     */
    private FormationOrigineModifieeManager formationOrigineModifieeManager;

    /** Le manager des param�tres par formation d'accueil. */
    private ParametresFormationAccueilManager parametresFormationAccueilManager;

    /**
     * Le manager utilis� pour les voeux de fili�re.
     */
    private VoeuDeFiliereManager voeuDeFiliereManager;

    /** Gestionnaire d'�valuations et de notes. */
    private EvalNoteManager evalNoteManager;

    /** Le gestionnaire d'�valuations du socle. */
    private EvaluationSocleManager evaluationSocleManager;

    /** Le gestionnaire des voeux �l�ve. */
    private VoeuEleveManager voeuEleveManager;

    /** Le gestionnaire des bonus fili�re. */
    private BonusFiliereManager bonusFiliereManager;

    /** Le gestionnaire des param�tres par formation d'origine. */
    private ParametreFormationOrigineManager parametreFormationOrigineManager;

    /** Le gestionnaire des rapprochements etablissement. */
    private RapprochEtabManager rapprochEtabManager;

    /** Le DAO des r�sultats provisoires. */
    private ResultatsProvisoiresOpaDao resultatsProvisoiresOpaDao;

    /** Les informations sur l'OPA conserv�es au long du traitement. */
    private InformationsOPA informationsOPA;

    /** Nombre de comp�tence du socle. */
    private int nbCompetences;

    /** Doit-on conserver d'�ventuels r�sultats s�curis�s lorsque l'on recycle un r�sultat d'OPA. */
    private boolean conserverResultatsSecurises;

    /**
     * Map contenant les valeurs des coefficients li�s aux code des champs disciplines correspondants en fonction
     * de l'id du param�tre par formation d'accueil associ�.
     */
    private Map<String, CacheBaremeInfo> mapCacheBaremeInfoParCodeVoeu;

    /** Constructeur du DAO. */
    public CalculBaremeDaoImpl() {
        // initialisation des donn�es globales
        cacheMap = new HashMap<>();
    }

    /**
     * @param bonusDao
     *            the bonusDao to set
     */
    @Autowired
    public void setBonusDao(BonusDao bonusDao) {
        this.bonusDao = bonusDao;
    }

    /**
     * @param voeuDeFiliereManager
     *            the voeuDeFiliereManager to set
     */
    @Autowired
    public void setVoeuDeFiliereManager(VoeuDeFiliereManager voeuDeFiliereManager) {
        this.voeuDeFiliereManager = voeuDeFiliereManager;
    }

    /**
     * @param formationOrigineModifieeManager
     *            the formationOrigineModifieeManager to set
     */
    @Autowired
    public void setFormationOrigineModifieeManager(
            FormationOrigineModifieeManager formationOrigineModifieeManager) {
        this.formationOrigineModifieeManager = formationOrigineModifieeManager;
    }

    /**
     * @param evalNoteManager
     *            le gestionnaire d'�valuations et de notes
     */
    @Autowired
    public void setEvalNoteManager(EvalNoteManager evalNoteManager) {
        this.evalNoteManager = evalNoteManager;
    }

    /**
     * @param evaluationSocleManager
     *            Le gestionnaire d'�valuations du socle.
     */
    @Autowired
    public void setEvaluationSocleManager(EvaluationSocleManager evaluationSocleManager) {
        this.evaluationSocleManager = evaluationSocleManager;
    }

    /**
     * @param parametresFormationAccueilManager
     *            the parametresFormationAccueilManager to set
     */
    @Autowired
    public void setParametresFormationAccueilManager(
            ParametresFormationAccueilManager parametresFormationAccueilManager) {
        this.parametresFormationAccueilManager = parametresFormationAccueilManager;
    }

    /**
     * @param voeuEleveManager
     *            the voeuEleveManager to set
     */
    @Autowired
    public void setVoeuEleveManager(VoeuEleveManager voeuEleveManager) {
        this.voeuEleveManager = voeuEleveManager;
    }

    /**
     * @param resultatsProvisoiresOpaDao
     *            the resultatsProvisoiresOpaDao to set
     */
    @Autowired
    public void setResultatsProvisoiresOpaDao(ResultatsProvisoiresOpaDao resultatsProvisoiresOpaDao) {
        this.resultatsProvisoiresOpaDao = resultatsProvisoiresOpaDao;
    }

    /**
     * @param bonusFiliereManager
     *            the bonusFiliereManager to set
     */
    @Autowired
    public void setBonusFiliereManager(BonusFiliereManager bonusFiliereManager) {
        this.bonusFiliereManager = bonusFiliereManager;
    }

    /**
     * @param parametreFormationOrigineManager
     *            the parametreFormationOrigineManager to set
     */
    @Autowired
    public void setParametreFormationOrigineManager(
            ParametreFormationOrigineManager parametreFormationOrigineManager) {
        this.parametreFormationOrigineManager = parametreFormationOrigineManager;
    }

    /**
     * @param rapprochEtabManager
     *            the rapprochEtabManager to set
     */
    @Autowired
    public void setRapprochEtabManager(RapprochEtabManager rapprochEtabManager) {
        this.rapprochEtabManager = rapprochEtabManager;
    }

    /**
     * Indique si l'on doit conserver les r�sultats s�curis�s.
     * 
     * @param conserverResultatsSecurises
     *            vrai si on conserve le bonus de s�curisation, sinon faux
     */
    void setConserverResultatsSecurises(boolean conserverResultatsSecurises) {
        if (conserverResultatsSecurises) {
            messages.start("Conservation des r�sultats s�curis�s");
            LOG.info("Conservation des r�sultats s�curis�s");
            messages.end();
        }
        this.conserverResultatsSecurises = conserverResultatsSecurises;
    }

    @Override
    public void lancerTraitement() {
        // Informations sur l'OPA trait�e
        long idOpa = getIdentifiantOperationProgrammeeAffectation();
        LOG.debug("Calcul du bar�me pour l'OPA " + idOpa);

        messages.start("Suppression des r�sultats provisoires obsol�tes");
        suppressionResultatsProvisoiresObsoletes(idOpa);
        messages.end();

        // On effectue le calcul du bar�me si il y a des �l�ves
        String flagImpactEleve = params.get(BatchAffectation.PARAMETRE_IMPACT_ELEVE);
        if (Flag.OUI.equals(flagImpactEleve)) {
            messages.start("Calcul du bar�me");
            calculBareme(idOpa);
            messages.end();
        }
    }

    /**
     * Suppression des r�sultats provisoires de l'OPA li�s � une offre de formation qui ne l'est plus.
     * 
     * @param idOpa
     *            id de l'OPA trait�e
     */
    private void suppressionResultatsProvisoiresObsoletes(long idOpa) {
        resultatsProvisoiresOpaDao.supprimerResultatsProvisoiresOffresNonSelectionnees(idOpa);
    }

    /**
     * R�cup�ration des voeux �l�ves, initialisation et alimentation des r�sultats provisoires en calculant les
     * bar�mes.
     * 
     * @param idOpa
     *            id de l'OPA concern�e
     */
    private void calculBareme(long idOpa) {
        OperationProgrammeeAffectation operationProgrammeeAffectation = operationProgrammeeAffectationManager
                .charger(idOpa);
        String flagConservationResultatsSecurises = params.get(BatchAffectation.PARAMETRE_CONSERVER_SECURISATION);

        setConserverResultatsSecurises(Flag.OUI.equals(flagConservationResultatsSecurises));
        setInformationsOPA(new InformationsOPA(operationProgrammeeAffectation));

        // R�cup�ration des r�sultats provisoires de l'OPA � traiter et des voeux �l�ves associ�s :
        // �l�ves non-forc�s ayant fait des voeux sur des offres avec calcul de bar�me.
        LOG.debug("Collecte pour des r�sultats provisoires et des voeux");

        // calcul du nombre r�sultats � traiter pour l'avancement
        Filtre filtreVoeuEleveOpa = voeuEleveManager.filtreVoeuxElevesPourOPASansRecensement(idOpa);

        int nombreResultatsATraiter = voeuEleveManager.compterVoeuEleve(filtreVoeuEleveOpa);
        LOG.debug("Nombre de r�sultats provisoires OPA  � traiter : " + nombreResultatsATraiter);

        initCoeffMap(idOpa);

        if (nombreResultatsATraiter > 0) {
            try {
                Collecteur<VoeuEleve> collecteur = voeuEleveManager.collecterVoeuxEleves(filtreVoeuEleveOpa,
                        listeFetchJoin());
                int nbResultatsTraites = 0;

                while (collecteur.collecter()) {
                    VoeuEleve voeuEleve = collecteur.get();
                    ResultatsProvisoiresOpa resultatProvisoire = initialiserResultatProvisoire(voeuEleve,
                            operationProgrammeeAffectation);

                    if (voeuEleveManager.isAvecCalculBareme(voeuEleve)) {
                        resultatProvisoire = calculerBareme(resultatProvisoire);
                    } else {
                        resultatProvisoire.setCodeDecisionProvisoire(
                                ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_REFUSE);
                    }
                    voeuEleve.setResultatsProvisoiresOpa(resultatProvisoire);
                    HibernateUtil.currentSession().saveOrUpdate(voeuEleve);

                    // Nettoyage r�gulier de la session Hibernate
                    if ((nbResultatsTraites % INTERVALLE_NETTOYAGE_SESSION == 0) && (nbResultatsTraites > 0)) {

                        LOG.debug("Nettoyage des caches et de la session Hibernate");

                        // On va nettoyer la session Hibernate mais il faut au pr�alable
                        // purger les tables de mise en cache des objets utiles
                        // sinon on peut obtenir des "lazy exception"
                        cacheMap = new HashMap<>();
                        HibernateUtil.cleanupSession();
                    }

                    // maj de l'avancement
                    messages.setAvancement(++nbResultatsTraites, nombreResultatsATraiter);
                }

                LOG.debug("Calcul du bar�me - lib�ration des ressources");

                collecteur.close();
            } catch (IOException e) {
                LOG.error("Erreur lors du calcul du bar�me", e);
            }
        }
        // Nettoyage de la session en cours
        HibernateUtil.cleanupSession();

        LOG.debug("Calcul du bar�me - FIN - lib�ration m�moire");
    }

    /**
     * Initialise le param�tre mapCacheBaremeInfoParCodeVoeu pour le traitement.
     * 
     * @param idOpa
     *            l'ID de l'OPA
     */
    private void initCoeffMap(Long idOpa) {
        mapCacheBaremeInfoParCodeVoeu = new HashMap<>();
        // R�cup�rer les pfa utilis�s dans l'OPA
        List<Object[]> listePfaOffreFormation = parametresFormationAccueilManager.listePfaOffreFormation(idOpa);

        for (Object[] pfaOffreFormation : listePfaOffreFormation) {
            ParametresFormationAccueil pfa = (ParametresFormationAccueil) pfaOffreFormation[0];
            Voeu offreFormation = (Voeu) pfaOffreFormation[1];
            CacheBaremeInfo baremeInfo = new CacheBaremeInfo(offreFormation.getCode());

            // Mode de calcul du bar�me
            String modeBareme = pfa.getModeBareme();
            baremeInfo.setModeCalculBareme(modeBareme);

            // Bonus doublement
            baremeInfo.setBonusDoublement(pfa.getBonusDoublement());

            // Alimentation des coefficients pour les notes/�valuations
            if (ParametresFormationAccueil.MODE_BAREME_EVALUATION.equals(modeBareme)) {
                for (Entry<ChampDisciplinaire, Integer> coeff : parametresFormationAccueilManager
                        .coefficientChampDisciplinaireMap(pfa).entrySet()) {
                    baremeInfo.putCoeffParCodeChampDisciplinaire(coeff.getKey().getCode(), coeff.getValue());
                }

                for (CoefficientEvaluationComplementaire coeff : pfa.getCoefficientsEvalCompl()) {
                    baremeInfo.putCoeffIdEvalComp(coeff.getEvalComp().getId(), coeff.getValCoef());
                }
            } else if (ParametresFormationAccueil.MODE_BAREME_NOTE.equals(modeBareme)) {
                for (LienRangParametresFormationAccueil coeff : pfa.getCoefficients()) {
                    baremeInfo.putCoeffIdRang(coeff.getId().getIdRang(), coeff.getValCoef());
                }
            }

            mapCacheBaremeInfoParCodeVoeu.put(offreFormation.getCode(), baremeInfo);
        }
    }

    /**
     * Positionne les informations li�es � l'OPA n�cessaires pour <strong>ce</strong> calcul du bar�me.
     * 
     * @param informationsOPA
     *            les informations li�es � l'OPA
     */
    protected void setInformationsOPA(InformationsOPA informationsOPA) {
        this.informationsOPA = informationsOPA;
    }

    /**
     * Liste des �l�ments n�cessitant une jointure fetch pour les performances du traitement.
     * 
     * @return la liste des qualifiers des �l�ments n�cessitant une jointure fecth
     */
    private List<String> listeFetchJoin() {
        List<String> fectJoinList = new ArrayList<>();

        fectJoinList.add("resultatsProvisoiresOpa");
        fectJoinList.add("eleve");
        fectJoinList.add("eleve.etablissement");
        fectJoinList.add("eleve.zoneGeo");
        fectJoinList.add("eleve.formation");
        fectJoinList.add("eleve.groupeOrigine");
        fectJoinList.add("eleve.evaluationsSocle");
        fectJoinList.add("eleve.evaluationsSocle.degre");
        fectJoinList.add("avisEntretien");
        fectJoinList.add("lv1");
        fectJoinList.add("candidature");
        fectJoinList.add("voeu");
        fectJoinList.add("voeu.etablissement");
        fectJoinList.add("voeu.formationAccueil");
        fectJoinList.add("voeu.matiereEnseigOptionnel");
        fectJoinList.add("voeu.voie");
        fectJoinList.add("voeu.commission");
        fectJoinList.add("voeu.pile");

        return fectJoinList;
    }

    /**
     * Initialise les r�sultats provisoires pour le calcul du bar�me.
     * 
     * @param voeuEleve
     *            Le voeu de l'�l�ve
     * @param operationProgrammeeAffectation
     *            l'OPA concern�e
     * @return le r�sultat provisoire cr�� ou r�initialis�
     */
    ResultatsProvisoiresOpa initialiserResultatProvisoire(VoeuEleve voeuEleve,
            OperationProgrammeeAffectation operationProgrammeeAffectation) {
        ResultatsProvisoiresOpa resultatsOpa = voeuEleve.getResultatsProvisoiresOpa();

        if (resultatsOpa != null) {
            if (!operationProgrammeeAffectation.equals(resultatsOpa.getOpa())) {
                resultatsOpa.setOpa(operationProgrammeeAffectation);
            }
            resultatsOpa.reinitialiserResultats(conserverResultatsSecurises);
        } else {
            resultatsOpa = new ResultatsProvisoiresOpa();
            resultatsOpa.setVoeuEleve(voeuEleve);
            resultatsOpa.setId(voeuEleve.getId());
            resultatsOpa.setOpa(operationProgrammeeAffectation);
        }
        voeuEleve.setResultatsProvisoiresOpa(resultatsOpa);

        return resultatsOpa;
    }

    /**
     * Calcul du bar�me pour le r�sultat provisoire d'un voeu d'�l�ve.
     * 
     * @param resultat
     *            R�sultat provisoire pour le voeu d'un �l�ve � traiter
     */
    protected ResultatsProvisoiresOpa calculerBareme(ResultatsProvisoiresOpa resultat) {

        // Le voeu de l'�l�ve
        VoeuEleve voeuEleve = resultat.getVoeuEleve();

        // L'opa associ� au r�sultat
        OperationProgrammeeAffectation opa = resultat.getOpa();

        // La formation origine de l'eleve
        Eleve eleve = voeuEleve.getEleve();
        BigFormation bfO = BigFormation.build(eleve);
        BigFormation bfOMod = bigFormationOrigineModifiee(eleve, cacheMap);

        // La formation d'acceuil li�e au voeu
        Voeu voeu = voeuEleve.getVoeu();
        BigFormation bfA = BigFormation.build(voeu);

        CacheBaremeInfo baremeInfo = mapCacheBaremeInfoParCodeVoeu.get(voeu.getCode());

        // Calcul des diff�rents bonus
        double baremeNotes = 0;
        double baremeEvaluations = 0;
        double baremeEvalSocle = 0;
        double baremeEvalCompl = 0;
        double baremeDiscipline = 0;
        double coefficientGroupe = eleve.getGroupeOrigine().getCoefficient();

        // Si l'offre de formation est de type "bar�me avec �valuations/notes"
        // On va consid�rer le param�tre par formation d'accueil associ� (pr�sence garantie par l'audit)
        // On calcule le bar�me selon le mode indiqu�
        if (voeu.utiliseBaremeNotesEvaluations() && (baremeInfo != null)) {
            String modeBareme = baremeInfo.getModeCalculBareme();
            if (ParametresFormationAccueil.MODE_BAREME_NOTE.equals(modeBareme)) {
                baremeNotes = calculerBaremeNotes(eleve, baremeInfo, opa);
            } else if (ParametresFormationAccueil.MODE_BAREME_EVALUATION.equals(modeBareme)) {

                baremeEvalSocle = calculerBaremePourEvaluationSocle(eleve);
                baremeDiscipline = calculerBaremePourEvaluationDiscipline(eleve, opa, voeu.getCode());
                baremeEvalCompl = calculerBaremePourEvaluationComplementaire(eleve, opa, baremeInfo);
                baremeEvaluations = arrondir(baremeEvalSocle + baremeDiscipline + baremeEvalCompl);
            }
        }

        int bonusRetardScolaire = calculerBonusRetardScolaire(eleve);
        int bonusFiliere = calculerBonusFiliere(bfO, bfA);

        int bonusAvisChefEtab = calculerBonusAvis(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT, voeuEleve);
        int bonusAvisDSDEN = calculerBonusAvis(CodeTypeAvis.AVIS_DSDEN, voeuEleve);
        int bonusAvisPasserelle = calculerBonusAvis(CodeTypeAvis.AVIS_PASSERELLE, voeuEleve);
        int bonusAvisGestion = calculerBonusAvis(CodeTypeAvis.AVIS_DE_GESTION, voeuEleve);

        int bonusDoublement = calculerBonusDoublement(eleve, bfOMod, voeu, bfA);
        int bonusBoursier = calculerBonusBoursier(eleve);

        int bonusRapprochement = calculerBonusRapprochement(eleve, voeu, bfA);
        int bonusVoeu1 = calculerBonusVoeu1(voeuEleve);
        int bonusAcademique = calculerBonusAcademique(eleve);

        int bonusLienZoneGeo = calculerBonusLienZoneGeo(eleve, voeu);
        int bonusDerogation = calculerBonusDerogation(voeuEleve);

        int bonusVoeuDeFiliere = calculerBonusVoeuDeFiliere(voeuEleve);

        // Calcul du bar�me : somme du bar�me des notes et des diff�rents bonus
        double bareme = arrondir(baremeNotes + baremeEvaluations + bonusRetardScolaire + bonusFiliere
                + bonusAvisChefEtab + bonusAvisDSDEN + bonusAvisPasserelle + bonusAvisGestion + bonusDoublement
                + bonusBoursier + bonusRapprochement + bonusVoeu1 + bonusAcademique + bonusLienZoneGeo
                + bonusDerogation + bonusVoeuDeFiliere);

        // Garde fou sur la taille du bareme
        bareme = bornerBaremeArrondi(bareme, BAREME_TOTAL_MAXIMUM);

        // Mise a jour du r�sultat
        resultat.setBaremeNotes(baremeNotes);
        resultat.setBaremeEvaluations(baremeEvaluations);
        resultat.setCoefficientGroupeOrigine(coefficientGroupe);

        // Mise � jour des bareme qui ont �t� utilis� pour les �valuations
        resultat.setBaremeEvalCompl(baremeEvalCompl);
        resultat.setBaremeEvalDiscipline(baremeDiscipline);
        resultat.setBaremeEvalSocle(baremeEvalSocle);

        resultat.setBonusRetardScolaire(bonusRetardScolaire);
        resultat.setBonusFiliere(bonusFiliere);

        resultat.setBonusAvisChefEtablissement(bonusAvisChefEtab);
        resultat.setBonusAvisDsden(bonusAvisDSDEN);
        resultat.setBonusAvisPasserelle(bonusAvisPasserelle);
        resultat.setBonusAvisDeGestion(bonusAvisGestion);

        resultat.setBonusDoublement(bonusDoublement);
        resultat.setBonusBoursier(bonusBoursier);
        resultat.setBonusRapprochement(bonusRapprochement);
        resultat.setBonusVoeu1(bonusVoeu1);
        resultat.setBonusAcademique(bonusAcademique);
        resultat.setBonusLienZoneGeo(bonusLienZoneGeo);
        resultat.setBonusDerogation(bonusDerogation);
        resultat.setBonusVoeuDeFiliere(bonusVoeuDeFiliere);

        resultat.setBareme(bareme);

        return resultat;
    }

    /**
     * Construit un aggregat BigFormation 'origine' correspondant � partir d'un �l�ve.
     * Cette version prend en compte les formations modifi�es (mefomod).
     * 
     * @param e
     *            �l�ve pour lequel construire l'aggregat BigFormation 'origine'
     * @param cacheMap
     *            table pour la mise en cache
     * @return la formation et les options d'origine en prenant en compte
     */
    private BigFormation bigFormationOrigineModifiee(Eleve e, Map<String, Object> cacheMap) {
        Formation formationO = e.getFormation();
        Matiere opt1 = e.getOptionOrigine2();
        Matiere opt2 = e.getOptionOrigine3();
        List<Matiere> lOpt = MatiereUtils.generateCouple(opt1, opt2);

        FormationOrigineModifiee fom = formationOrigineModifieeManager.find(formationO, lOpt, cacheMap);

        if (fom != null) {
            formationO = fom.getFormationFinale();
        }

        return new BigFormation(formationO, new ArrayList<>(0));
    }

    /**
     * Calcule le bonus de d�rogation pour le voeu �l�ve fourni.
     * <ul>
     * <li>* Ne concerne que les voeux d�rogatoires.</li>
     * </ul>
     * 
     * @param ve
     *            Voeu �l�ve concern�
     * @return valeur du bonus d�rogation
     */
    public int calculerBonusDerogation(VoeuEleve ve) {
        int somme = 0;

        if (ve.getFlagVoeuDerogation().equals(Flag.OUI)) {
            Map<Integer, DerogationVoeuEleve> derogationsVoeuEleve = ve.getDerogationsVoeuEleve();

            Iterator<DerogationVoeuEleve> derogationsEleveIterator = derogationsVoeuEleve.values().iterator();

            while (derogationsEleveIterator.hasNext()) {
                DerogationVoeuEleve derogationVoeuEleve = derogationsEleveIterator.next();

                Derogation derogation = derogationVoeuEleve.getId().getDerogation();

                // bonus parcours scolaire pris en compte uniquement
                // si flag �l�ve parcourt scolaire particuliers � O
                if (derogation.getId().equals(Derogation.CODE_DEROGATION_PARCOURS_SCOLAIRE_PARTICULIER)) {
                    if (ve.getFlagValidationParcoursScolaireParticulier() != null
                            && ve.getFlagValidationParcoursScolaireParticulier().equals(Flag.OUI)) {
                        somme += derogation.getBonus();
                    }
                } else {
                    // bonus dans les autres cas
                    somme += derogation.getBonus();
                }

            } // while -- d�rogations associ�es au voeu �l�ve
        } // if -- post-3�me
        return somme;
    }

    /**
     * Attribue le bonus acad�mique si l'�l�ve est concern�.
     * 
     * @param eleve
     *            l'�l�ve � traiter
     * @return valeur du bonus acad�mique si le flag est positionn� sur l'�l�ve
     */
    private int calculerBonusAcademique(Eleve eleve) {
        if (eleve.getFlagBonusAcademique().equals(Flag.OUI)) {
            return informationsOPA.getBonusAcademique();
        }
        return 0;
    }

    /**
     * Calcul du bonus pour un avis.
     * 
     * @param codeTypeAvis
     *            le type de l'avis
     * @param ve
     *            le voeu de l'�l�ve
     * @return la valeur du bonus pour l'avis
     */
    private int calculerBonusAvis(CodeTypeAvis codeTypeAvis, VoeuEleve ve) {
        Avis avis = ve.getAvis(codeTypeAvis);
        if (avis == null) {
            return 0;
        } else {
            return avis.getBonus();
        }
    }

    /**
     * Calcul du bonus doublement pour l'�l�ve.
     * 
     * @param eleve
     *            �l�ve concern�
     * @param voeu
     *            voeu �l�ve concern�
     * @param bfO
     *            l'aggregat BigFormation origine
     * @param bfA
     *            l'aggregat BigFormation accueil (correspondant au voeu)
     * @return valeur du bonus doublement
     */
    private int calculerBonusDoublement(Eleve eleve, BigFormation bfO, Voeu voeu, BigFormation bfA) {

        // infos origine
        Etablissement etabO = eleve.getEtablissement();
        Formation formationO = bfO.getFormation();

        // infos accueil
        Etablissement etabA = voeu.getEtablissement();
        Formation formationA = bfA.getFormation();

        // il faut le m�me �tablissement
        if (etabO == null || !etabO.equals(etabA)) {
            return 0;
        }

        // il faut la m�me formation
        if (!formationO.equals(formationA)) {
            return 0;
        }

        // r�cup�ration du bon paramefA (si celui donn�e en param�tre est null)

        CacheBaremeInfo baremeInfo = mapCacheBaremeInfoParCodeVoeu.get(voeu.getCode());

        if (baremeInfo != null) {
            Integer bonusDoublement = baremeInfo.getBonusDoublement();
            if (bonusDoublement != null) {
                return bonusDoublement;
            }
        }

        // utilisation du bonus par defaut
        return getBonus(TypeBonus.DOUBLEMENT).getValeur();
    }

    /**
     * Calcule le bonus pour les �l�ves boursiers.
     * 
     * @param eleve
     *            �l�ve concern�
     * @return la valeur du bonus boursier � attribuer
     */
    protected int calculerBonusBoursier(Eleve eleve) {

        // Si l'�l�ve n'est pas boursier => non applicable
        if (!Flag.toBoolean(eleve.getFlagBoursier())) {
            return 0;
        }

        // L'�l�ve est boursier, on examine son palier d'origine.
        Palier palierEleve = eleve.getPalierOrigine();
        if (palierEleve == null) {
            // Palier inconnu (normalement, ce cas ne devrait pas se produire
            LOG.warn("Un palier manquant a �t� d�tect� pour l'�l�ve " + eleve.getIne()
                    + " lors du calcule du crit�re boursier.");
            return 0;
        }

        // On n'attribue le bonus boursier qu'aux �l�ves du palier 3�me.
        if (Palier.CODE_PALIER_3EME.equals(palierEleve.getCode())) {
            return informationsOPA.getBonusBoursier();
        }

        // A d�faut, pas de bonus
        return 0;
    }

    /**
     * Calcule le bonus pour le suivi d'une fili�re entre la formation origine et accueil.
     * 
     * @param bfO
     *            l'aggregat BigFormation origine
     * @param bfA
     *            l'aggregat BigFormation accueil
     * @return bonus accord� au suivi de la fili�re entre bfO et bfA
     */
    private int calculerBonusFiliere(BigFormation bfO, BigFormation bfA) {
        // infos origine
        Formation formationO = bfO.getFormation();
        List<Matiere> lOpt = bfO.getOptions();

        // infos accueil
        Formation formationA = bfA.getFormation();
        Matiere ensOpt = bfA.getMatiereEnseigOptionnel();

        // r�cup�ration du bonus fili�re ad�quat
        BonusFiliere bf = bonusFiliereManager.trouverBonusFiliereLePlusProche(formationO, lOpt, formationA, ensOpt,
                cacheMap);
        if (bf != null) {
            return bf.getBonus();
        }

        // rien ne va
        return 0;
    }

    /**
     * Calcule si l'�l�ve peut b�n�ficier d'un bonus pour un lien zone g�ographique.
     * 
     * @param eleve
     *            �l�ve concern�
     * @param voeu
     *            voeu concern�
     * @return bonus lien zone g�o accord�
     */
    private int calculerBonusLienZoneGeo(Eleve eleve, Voeu voeu) {
        // v�rification de l'origine (adresse origine de l'�l�ve)
        ZoneGeo zgO = eleve.getZoneGeo();
        if (zgO != null) {
            // r�cup�ration du bon lien zone g�o.
            LienZoneGeo lzg = LienZoneGeoDaoImpl.find(zgO, voeu, cacheMap);
            if (lzg != null) {
                return lzg.getBonus();
            }
        }

        // rien ne va
        return 0;
    }

    /**
     * Attribue le bonus pr�vu pour le premier voeu de l'�l�ve (rang 1).
     * 
     * @param ve
     *            voeu de l'�l�ve
     * @return bonus pour le voeu de rang 1
     */
    private int calculerBonusVoeu1(VoeuEleve ve) {
        if (ve.getId().getRang() > 1) {
            return 0;
        }

        // r�cup�ration du bonus
        return informationsOPA.getBonusPremierVoeu();
    }

    /**
     * Calcul du bonus rapprochement accord� au voeu �l�ve selon son �tablissement d'origine.
     * 
     * @param eleve
     *            �l�ve concern�
     * @param voeu
     *            voeu formul� par l'�l�ve
     * @param bfA
     *            l'aggregat BigFormation accueil (correspondant au voeu)
     * @return valeur du bonus rapprochement pour ce voeu
     */
    private int calculerBonusRapprochement(Eleve eleve, Voeu voeu, BigFormation bfA) {

        // Code �tablissement / d�partement origine
        String codeEtablissementOuDepartementOrigine = eleve.numeroUaiOuDepartementMenOrigine();

        // infos accueil
        Etablissement etabA = voeu.getEtablissement();
        Formation formationA = bfA.getFormation();
        Matiere ensOpt = bfA.getMatiereEnseigOptionnel();
        Formation formationO = eleve.getFormation();
        RapprochEtab re = rapprochEtabManager.trouverPlusProche(codeEtablissementOuDepartementOrigine, etabA,
                formationA, formationO, ensOpt, cacheMap);

        if (re != null) {
            return re.getBonus();
        }

        // rien ne va
        return 0;
    }

    /**
     * Calcule le bonus de retard scolaire dont l'�l�ve peut b�n�ficier d'apr�s les param�tres origine.
     * 
     * @param eleve
     *            �l�ve concern�
     * @return valeur du bonus accord� � l'�l�ve pour le retard scolaire
     */
    private int calculerBonusRetardScolaire(Eleve eleve) {

        // On commence par regarder si le flag de retard scolaire est positionn� � OUI pour l'�l�ve
        String flRetardScolaire = eleve.getFlagRetardScolaire();
        if (flRetardScolaire == null || flRetardScolaire.equals(Flag.NON)) {
            return 0;
        }

        // On peut attribuer le bonus, donc il faut rechercher le paramefo qui convient.
        // On se base pour cela sur la formation et les options origine de l'�l�ve
        Formation formation = eleve.getFormation();
        Matiere option2 = eleve.getOptionOrigine2();
        Matiere option3 = eleve.getOptionOrigine3();
        List<Matiere> lOptions = MatiereUtils.generateCouple(option2, option3);

        ParametreFormationOrigine pfo = parametreFormationOrigineManager.find(formation, lOptions, cacheMap);

        if (pfo == null) {
            return 0;
        } else {
            // Attribution du bonus du paramefo retrouv�
            return pfo.getBonusRetardScolaire();
        }
    }

    /**
     * Calcule le bar�me attribu� � un �l�ve pour un voeu en fonction de ses notes.
     * 
     * @param eleve
     *            �l�ve concern�
     * @param baremeInfo
     *            l'objet contenant les coefficients � appliquer
     * @param opa
     *            l'operation programm�e affectation
     * @return valeur du bar�me li� aux notes pour ce voeu
     */
    protected double calculerBaremeNotes(Eleve eleve, CacheBaremeInfo baremeInfo,
            OperationProgrammeeAffectation opa) {
        String ine = eleve.getIne();
        Long idOpa = opa.getId();

        Map<Rang, NoteOpa> mapNote = evalNoteManager.mapNoteHarmo(ine, idOpa);

        // calcul du r�sultat
        double baremeNote = 0;
        for (Entry<Rang, NoteOpa> rangNote : mapNote.entrySet()) {
            // r�cuperation de la note harmonis�e
            Double noteHarmonisee = rangNote.getValue().getValeurNoteHarmo();

            // r�cup�ration du coefficient � appliquer
            Integer coef = baremeInfo.getCoeffIdRang(rangNote.getKey().getId());

            // ajout au bar�me
            if (noteHarmonisee != null && coef != null) {
                baremeNote += coef.doubleValue() * noteHarmonisee.doubleValue();
            }
        }

        // pond�ration par le coefficient du groupe
        baremeNote = ponderationResultatCoefficientGroupeOrigine(eleve, baremeNote);

        // V�rification sur l'intervalle maximum
        baremeNote = bornerBaremeArrondi(baremeNote, BONUS_MAX_NOTES);
        return baremeNote;
    }

    /**
     * Borne et arrondit le bar�me dans l'intervalle autoris� : entre 0 et la valeur maximale fournie.
     * 
     * @param bareme
     *            le bar�me � borner
     * @param valeurMaximale
     *            la valeur maximale
     * @return la valeur du bonus born�e arrondie
     */
    public double bornerBaremeArrondi(double bareme, double valeurMaximale) {
        double baremeBorne = 0;
        // intervalle possible
        if (bareme < 0) {
            baremeBorne = 0;
        } else if (bareme > valeurMaximale) {
            baremeBorne = valeurMaximale;
        } else {
            baremeBorne = arrondir(bareme);
        }
        return baremeBorne;
    }

    /**
     * Pond�re un bar�me selon le groupe origine..
     * 
     * @param eleve
     *            l'�l�ve associ� � un groupe d'origine
     * @param resultat
     *            le r�sultat � pond�rer
     * @return le r�sultat pond�r�
     */
    public double ponderationResultatCoefficientGroupeOrigine(Eleve eleve, double resultat) {
        return resultat * eleve.getGroupeOrigine().getCoefficient().doubleValue();
    }

    /**
     * Calcul du bar�me li� aux �valuations du socle commun de comp�tences.
     * 
     * @param eleve
     *            l'�l�ve concern�
     * @return le bar�me du socle
     */
    protected double calculerBaremePourEvaluationSocle(Eleve eleve) {

        double baremeSocle = 0.0;
        initNbCompetence();

        switch (eleve.modeEvalNote()) {

            case NOTE:
                baremeSocle = getNombrePointsSoclePalier2nde();
                break;
            case EVALUATION:

                Map<CompetenceSocle, EvaluationSocle> evalSocleBase = eleve.getEvaluationsSocle();

                int nbEvalSocle = 0;
                for (EvaluationSocle eval : evalSocleBase.values()) {
                    baremeSocle += eval.getPoints();
                    nbEvalSocle++;
                }

                if (nbEvalSocle != 0 && nbEvalSocle != nbCompetences) {
                    baremeSocle = baremeSocle * nbCompetences / nbEvalSocle;
                }

                break;

            default:
                LOG.debug("L'�l�ve " + eleve.getIne() + "n'a pas de palier => bar�me socle � z�ro.");

        }

        // Application du coefficient de socle
        baremeSocle *= COEFF_SOCLE;

        // Pond�ration par le coefficient du groupe origine
        baremeSocle = ponderationResultatCoefficientGroupeOrigine(eleve, baremeSocle);

        // Limitation � l'intervalle attendu
        baremeSocle = bornerBaremeArrondi(baremeSocle, BONUS_MAX_EVAL);

        return baremeSocle;
    }

    /**
     * Initialise le nombre de comp�tence du socle.
     */
    private void initNbCompetence() {
        if (nbCompetences == 0) {
            nbCompetences = evaluationSocleManager.listerCompetencesSocle().size();
        }
    }

    /**
     * @return le nombre de points de socle pour un �l�ve du palier 2nde.
     */
    private Double getNombrePointsSoclePalier2nde() {
        return (double) (nbCompetences * DegreMaitrise.POINTS_DEFAUT_PALIER_2NDE);
    }

    /**
     * Calcul du bar�me li� aux �valuations compl�mentaires acad�miques.
     * 
     * @param eleve
     *            l'�l�ve concern�
     * @param opa
     *            Operation programm�e d'affectation
     * @param baremeInfo
     *            l'objet contenant les coefficients
     * @return le bar�me des �valuations compl�mentaires
     */
    protected double calculerBaremePourEvaluationComplementaire(Eleve eleve, OperationProgrammeeAffectation opa,
            CacheBaremeInfo baremeInfo) {
        // calcul du r�sultat
        double valeurPoint = 0;
        String ine = eleve.getIne();
        Long idOpa = opa.getId();

        // r�cup�ration de la liste des �valuations compl�mentaire
        Map<EvaluationComplementaire, EvaluationComplementaireOpa> mapEvaluationComplementaireOpa = evalNoteManager
                .mapPointLisseEvalCompl(ine, idOpa);

        if (!mapEvaluationComplementaireOpa.isEmpty()) {

            // Parcours de la liste
            for (EvaluationComplementaireOpa evaluationComplementaireOpa : mapEvaluationComplementaireOpa
                    .values()) {
                // r�cup�ration de la valeur de point harmonis�
                double valeurPointsHarmo = evaluationComplementaireOpa.getValeurPointsHarmo();

                // r�cup�ration du coefficient associ� au paramefa et
                EvaluationComplementaire evalCompl = evaluationComplementaireOpa.getEvaluationComplementaire();
                int coef = 0;
                if ((evalCompl != null && baremeInfo != null)) {
                    coef = baremeInfo.getCoeffIdEvalComp(evalCompl.getId());
                }
                // Somme pond�r�e selon les coefficients des �valuations des �valuations compl�mentaires de la
                // formation d'accueil et en particulier du param�tre par formation d'accueil qui s'applique
                valeurPoint += coef * valeurPointsHarmo;
            }

            // pond�ration par le coefficient du groupe
            valeurPoint = ponderationResultatCoefficientGroupeOrigine(eleve, valeurPoint);

            // V�rification sur l'intervalle maximum
            valeurPoint = bornerBaremeArrondi(valeurPoint, BONUS_MAX_EVAL);
        }

        return valeurPoint;
    }

    /**
     * Calcule le bar�me pour les �valuations disciplinaires.
     * 
     * @param eleve
     *            l'�l�ve concern�
     * @param opa
     *            l'op�ration programm� d'affectation
     * @param codeVoeu
     *            le code du voeu
     * @return le bar�me li� aux �valuations disciplinaires
     */
    protected double calculerBaremePourEvaluationDiscipline(Eleve eleve, OperationProgrammeeAffectation opa,
            String codeVoeu) {
        // calcul du r�sultat
        double valeurPoint = 0;
        String ine = eleve.getIne();
        Long idOpa = opa.getId();

        // r�cup�ration de la liste des �valuations des champs disciplinaire

        Map<ChampDisciplinaire, EvaluationChampDisciplinaireOpa> mapChampOpa = evalNoteManager
                .mapPointHarmoChampDisp(ine, idOpa);
        CacheBaremeInfo baremeInfo = mapCacheBaremeInfoParCodeVoeu.get(codeVoeu);

        if (!mapChampOpa.isEmpty()) {

            // Parcours de la liste
            for (Entry<ChampDisciplinaire, EvaluationChampDisciplinaireOpa> entry : mapChampOpa.entrySet()) {
                // r�cup�ration de la valeur de point harmonis�
                double moyennePointsHarmo = entry.getValue().getValeurMoyennePointsHarmo();

                // r�cup�ration du coefficient associ�
                Integer coef = baremeInfo.getCoeffCodeChampDisciplinaire(entry.getKey().getCode());

                // Somme pond�r�e selon les coefficients des �valuations des �valuations compl�mentaires de la
                // formation d'accueil et en particulier du param�tre par formation d'accueil qui s'applique
                if (coef != null) {
                    valeurPoint += coef * moyennePointsHarmo;
                }
            }

            // pond�ration par le coefficient du groupe
            valeurPoint = ponderationResultatCoefficientGroupeOrigine(eleve, valeurPoint);

            // V�rification sur l'intervalle maximum
            valeurPoint = bornerBaremeArrondi(valeurPoint, BONUS_MAX_EVAL);
        }

        return valeurPoint;
    }

    /**
     * M�thode de calcul du bonus pour le voeu de fili�re / mont�e p�dagogique.
     * 
     * On attribue le bonus de "mont�e p�dagogique" aux �l�ves passant de 2nde pro en 1�re pro
     * avec une formation d'accueil
     * <ul>
     * <li>soit ayant la m�me sp�cialit� dans le m�me �tablissement</li>
     * <li>soit �tant une �ventuelle adaptation de fili�re d�finie dans la nomenclature "Voeux de fili�re".</li>
     * </ul>
     * 
     * @param ve
     *            le voeu formul� par l'�l�ve
     * @return la valeur du bonus attribu� � ce voeu, en ce qui concerne les voeux de fili�re
     */
    private int calculerBonusVoeuDeFiliere(VoeuEleve ve) {
        int bonusVoeuDeFiliere = 0;

        if (ve.getVoeu().getEtablissement().equals(ve.getEleve().getEtablissement())) {

            Formation formationOrigine = ve.getEleve().getFormation();
            Formation formationAccueil = ve.getVoeu().getFormationAccueil();

            // Chargement d'uen �ventuelle adatation de la fili�re d�finie pour la formation d'origine
            VoeuDeFiliere voeuDeFiliere = voeuDeFiliereManager.chargerNullable(formationOrigine.getId());

            if (formationOrigine.getMefStat().getCodeMefStat4().equals(MefStat.CODE_MEFSTAT4_2NDPRO)
                    && formationAccueil.getMefStat().getCodeMefStat4().equals(MefStat.CODE_MEFSTAT4_1PRO)
                    && (voeuDeFiliere != null && formationAccueil.equals(voeuDeFiliere.getFormationDAccueil())
                            || voeuDeFiliere == null && formationAccueil.getId().getCodeSpecialite()
                                    .equals(formationOrigine.getId().getCodeSpecialite()))) {

                bonusVoeuDeFiliere = getBonus(TypeBonus.MONTEE_PEDAGOGIQUE).getValeur();
            }
        }

        return bonusVoeuDeFiliere;
    }

    /** @return objet contenant les bonus pour PAM */
    private Bonus getBonus(TypeBonus typeBonus) {
        Bonus bonus;
        String cacheKey = typeBonus.toString();
        if (cacheMap.containsKey(cacheKey)) {
            bonus = (Bonus) cacheMap.get(cacheKey);
        } else {
            bonus = bonusDao.charger(typeBonus);
            cacheMap.put(cacheKey, bonus);
        }
        return bonus;
    }

    /**
     * Calcul d'arrondi � 3 chiffres d�cimaux utilis� pour les bar�mes.
     * L'arrondi s'effectue � la valeur la plus proche ou � la valeur
     * sup�rieure en cas d'�gale distance.
     * 
     * @param d
     *            valeur � arrondir
     * @return valeur arrondie
     */
    private static double arrondir(double d) {
        return new BigDecimal(String.valueOf(d)).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

}
