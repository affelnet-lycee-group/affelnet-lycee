/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BatchDaoImpl;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.pam.RecalculePilesDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.statistiques.EtatStatistique;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.statistiques.EtatStatistiqueManager;
import fr.edu.academie.affelnet.service.statistiques.OffreDemandeManager;

/** Implementation de la gestion du calcul des piles (traitement PAM). */
@Repository("RecalculePilesDao")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RecalculePilesDaoImpl extends BatchDaoImpl implements RecalculePilesDao {

    /** Le nom du param�tre de s�lection du calcul des piles. */
    public static final String SELECTION_PILES = "selectionPiles";

    /** Le nom du param�tre de s�lection du calcul de l'offre et la demande sur le premier voeu. */
    public static final String SELECTION_OFFRE_ET_DEMANDE_SUR_PREMIER_VOEU = "selectionOffreEtDemandeSurPremierVoeu";

    /** Le nom du param�tre de s�lection du calcul de l'offre et la demande par district d'origine. */
    public static final String SELECTION_OFFRE_ET_DEMANDE_PAR_DISTRICT_ORIGINE = "selectionOffreEtDemandeParDistrictOrigine";

    /** Le nom du param�tre de s�lection du calcul du mouvement entre districts. */
    public static final String SELECTION_MOUVEMENT_ENTRE_DISTRICTS = "selectionMouvementEntreDistricts";

    /** Le nom du param�tre de s�lection du calcul du mouvement entre d�partements. */
    public static final String SELECTION_MOUVEMENT_ENTRE_DEPARTEMENTS = "selectionMouvementEntreDepartements";

    /** Le nom du param�tre de s�lection du calcul de l'offre et la demande par �tablissement d'origine. */
    public static final String SELECTION_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ORIGINE = "selectionOffreEtDemandeParEtablissementOrigine";

    /** Le nom du param�tre de s�lection du calcul de l'offre et la demande par �tablissement d'accueil. */
    public static final String SELECTION_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ACCUEIL = "selectionOffreEtDemandeParEtablissementAccueil";

    /** Le nom du param�tre de s�lection du calcul de l'�ge moyen par formation d'origine. */
    public static final String SELECTION_AGE_MOYEN_PAR_FORMATION_ORIGINE = "selectionAgeMoyenParFormationOrigine";

    /** Le nom du param�tre de s�lection du calcul du point par pile. */
    public static final String SELECTION_POINT_PAR_PILE = "selectionPointParPile";

    /** Le nom du param�tre de s�lection du calcul du bilan par formation. */
    public static final String SELECTION_BILAN_PAR_FORMATION = "selectionBilanParFormation";

    /** Le nom du param�tre de s�lection du calcul des affect�s par formation d'origine. */
    public static final String SELECTION_AFFECTES_PAR_FORMATION_ORIGINE = "selectionAffectesParFormationOrigine";

    /** Le nom du param�tre de s�lection du calcul de l'analyse par MEF origine. */
    public static final String SELECTION_ANALYSE_PAR_MEF_ORIGINE = "selectionAnalyseParMefOrigine";

    /** Le nom du param�tre de manager utilis� pour les offres et demandes. */
    private OffreDemandeManager offreDemandeManager;

    /** Le gestionnaire de campagne d'affectation. */
    private CampagneAffectationManager campagneAffectationManager;

    /** Le gestionnaire des �tats statistiques. */
    private EtatStatistiqueManager etatStatistiqueManager;

    /** Num�ro du tour courant pour lequel effectuer le traitement. */
    private short numeroTour;

    /**
     * @param offreDemandeManager
     *            the offreDemandeManager to set
     */
    @Autowired
    public void setOffreDemandeManager(OffreDemandeManager offreDemandeManager) {
        this.offreDemandeManager = offreDemandeManager;
    }

    /**
     * @param campagneAffectationManager
     *            Le gestionnaire de campagne d'affectation
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneAffectationManager = campagneAffectationManager;
    }

    /**
     * @param etatStatistiqueManager
     *            the etatStatistiqueManager to set
     */
    @Autowired
    public void setEtatStatistiqueManager(EtatStatistiqueManager etatStatistiqueManager) {
        this.etatStatistiqueManager = etatStatistiqueManager;
    }

    @Override
    public void lancerTraitement() {
        messages.start("Recalcul des piles et des statistiques");

        numeroTour = campagneAffectationManager.getNumeroTourCourant();

        // maj des piles
        if (Flag.OUI.equals(params.get(SELECTION_PILES))) {
            messages.start("Mise � jour des piles");
            majPiles();
            EtatStatistique etat = etatStatistiqueManager.mettreAJourEtatStatistique(EtatStatistique.ID_PILES);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        // maj des stats
        if (Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_SUR_PREMIER_VOEU))
                || Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_PAR_DISTRICT_ORIGINE))
                || Flag.OUI.equals(params.get(SELECTION_MOUVEMENT_ENTRE_DISTRICTS))
                || Flag.OUI.equals(params.get(SELECTION_MOUVEMENT_ENTRE_DEPARTEMENTS))
                || Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ORIGINE))
                || Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ACCUEIL))
                || Flag.OUI.equals(params.get(SELECTION_AGE_MOYEN_PAR_FORMATION_ORIGINE))
                || Flag.OUI.equals(params.get(SELECTION_POINT_PAR_PILE))
                || Flag.OUI.equals(params.get(SELECTION_BILAN_PAR_FORMATION))
                || Flag.OUI.equals(params.get(SELECTION_AFFECTES_PAR_FORMATION_ORIGINE))
                || Flag.OUI.equals(params.get(SELECTION_ANALYSE_PAR_MEF_ORIGINE))) {
            messages.start("Mise � jour des statistiques");
            majStats();
            messages.end();
        }

        // Nettoyage de la session en cours
        HibernateUtil.cleanupSession();

        // fin
        messages.end();
    }

    /**
     * Cette m�thode met � jour les piles (*V_PILE) des offres de formation du tour courant.
     */
    private void majPiles() {
        Session session = HibernateUtil.currentSession();

        // mise � jour du bar�me mini
        DaoUtils.executeHibernateQuery(session.getNamedQuery("sql.update.pam.recalculPiles.majBaremeMini"));

        // mise � jour du bar�me max
        DaoUtils.executeHibernateQuery(session.getNamedQuery("sql.update.pam.recalculPiles.majBaremeMaxi"));

        // mise � jour du bar�me dernier admis
        DaoUtils.executeHibernateQuery(session.getNamedQuery("sql.update.pam.recalculPiles.majDernierAdmis"));

        // mise � jour du nombre �l�ves demandeurs
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.majNbElevesDemandeurs"));

        // mise � jour du nombre �l�ves admis
        DaoUtils.executeHibernateQuery(session.getNamedQuery("sql.update.pam.recalculPiles.majNbElevesAdmis"));

        // Mise � jour de la date de derni�re modification des piles
        DaoUtils.executeHibernateQuery(session.getNamedQuery("sql.update.pam.recalculPiles.majDateModification"));

    }

    /**
     * Cette m�thode met � jour les tables de statistiques (GZ_*).
     */
    private void majStats() {

        if (Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_SUR_PREMIER_VOEU))) {
            messages.start("Offre et demande sur le premier voeu");
            majStatsOffreDemandeParVoeuDAccueilSurLePremierVoeu();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_OFFRE_ET_DEMANDE_SUR_PREMIER_VOEU);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_PAR_DISTRICT_ORIGINE))) {
            messages.start("Offre et demande par district d'origine");
            majStatsOffreEtDemandeParDistrictOrigine();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_OFFRE_ET_DEMANDE_PAR_DISTRICT_ORIGINE);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_MOUVEMENT_ENTRE_DISTRICTS))) {
            messages.start("Mouvement entre districts");
            majStatsMouvementEntreDistrict();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_MOUVEMENT_ENTRE_DISTRICTS);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_MOUVEMENT_ENTRE_DEPARTEMENTS))) {
            messages.start("Mouvement entre d�partements");
            majStatsMouvementEntreDepartement();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_MOUVEMENT_ENTRE_DEPARTEMENTS);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ORIGINE))) {
            messages.start("Offre et demande par �tablissement d'origine");
            majStatsOffreEtDemandeParEtablissementOrigine();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ORIGINE);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ACCUEIL))) {
            messages.start("Offre et demande par �tablissement d'accueil");
            majStatsOffreEtDemandeParEtablissementAccueil();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_OFFRE_ET_DEMANDE_PAR_ETABLISSEMENT_ACCUEIL);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_AGE_MOYEN_PAR_FORMATION_ORIGINE))) {
            messages.start("Age moyen par formation d'origine");
            majStatsAgeMoyenParFormationOrigine();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_AGE_MOYEN_PAR_FORMATION_ORIGINE);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_POINT_PAR_PILE))) {
            messages.start("Point par pile");
            majStatsPointParPileParEtablissementAccueil();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_POINT_PAR_PILE);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_BILAN_PAR_FORMATION))) {
            messages.start("Bilan par formation");
            majStatsBilanParFormation();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_BILAN_PAR_FORMATION);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_AFFECTES_PAR_FORMATION_ORIGINE))) {
            messages.start("�l�ves affect�s par formation d'origine");
            majStatsListeParFormationOrigine();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_AFFECTES_PAR_FORMATION_ORIGINE);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }

        if (Flag.OUI.equals(params.get(SELECTION_ANALYSE_PAR_MEF_ORIGINE))) {
            messages.start("Analyse par MEF origine");
            majStatsAnalyseFormationOrigine();
            EtatStatistique etat = etatStatistiqueManager
                    .mettreAJourEtatStatistique(EtatStatistique.ID_ANALYSE_PAR_MEF_ORIGINE);
            resultats.put(String.valueOf(etat.getId()), etat);
            messages.end();
        }
    }

    /**
     * Cette m�thode met � jour la statistique "Offre et demande sur le premier voeu".
     */
    private void majStatsOffreDemandeParVoeuDAccueilSurLePremierVoeu() {
        Session session = HibernateUtil.currentSession();
        // offre et demande par voeu d'accueil sur le premier voeu
        Query requeteEffacement = session.createSQLQuery("delete from GZ_O1V where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        // code niveau = autres
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.insert.pam.recalculPiles.stats.offreEtDemandeParVoeuAccueilSurPremierVoeu.niveauAutres")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.update.pam.recalculPiles.stats.offreEtDemandeParVoeuAccueilSurPremierVoeu.niveauAutres")
                .setParameter("numeroTour", numeroTour));
        // par sexe et code niveau
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.insert.pam.recalculPiles.stats.offreEtDemandeParVoeuAccueilSurPremierVoeu.parSexeEtParNiveau")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.update.pam.recalculPiles.stats.offreEtDemandeParVoeuAccueilSurPremierVoeu.parSexeEtParNiveau")
                .setParameter("numeroTour", numeroTour));
        // tous code sexe
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.insert.pam.recalculPiles.stats.offreEtDemandeParVoeuAccueilSurPremierVoeu.tousSexeParNiveau")
                .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Liste par formation d'origine".
     */
    private void majStatsListeParFormationOrigine() {
        Session session = HibernateUtil.currentSession();
        // liste par formation origine
        // code niveau = autres
        Query requeteEffacement = session.createSQLQuery("delete from GZ_LPFO where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.listeParFormationOrigine")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.listeParFormationOrigine.nombreAdmis")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.listeParFormationOrigine.nombreListeSup")
                        .setParameter("numeroTour", numeroTour));

        // par sexe et code niveau
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.insert.pam.recalculPiles.stats.listeParFormationOrigine.parSexeEtParNiveau")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.update.pam.recalculPiles.stats.listeParFormationOrigine.parSexeEtCodeNiveau.nombreAdmis")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.update.pam.recalculPiles.stats.listeParFormationOrigine.parSexeEtCodeNiveau.nombreListeSup")
                .setParameter("numeroTour", numeroTour));
        // tous code sexe
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.insert.pam.recalculPiles.stats.listeParFormationOrigine.tousSexeParNiveau")
                .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Offre et demande par district d'origine".
     */
    private void majStatsOffreEtDemandeParDistrictOrigine() {
        Session session = HibernateUtil.currentSession();
        // r�cup�ration du nombre de voeux
        int nbVoeux = offreDemandeManager.getMaxVoeux();

        // Suppresion des anciennes donn�es
        Query requeteEffacement = session.createSQLQuery("delete from GZ_ODIS where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.offreEtDemandeParDistrictOrigine")
                        .setParameter("numeroTour", numeroTour));
        // autres (�l�ves qui n'ont pas de district origine)
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.insert.pam.recalculPiles.stats.offreEtDemandeParDistrictOrigine.elevesSansDistrict")
                .setParameter("numeroTour", numeroTour));

        // mise � jour du nombre de voeux
        String sql = ((SQLQuery) session.getNamedQuery(
                "sql.update.pam.recalculPiles.stats.offreEtDemandeParDistrictOrigine.nbVoeuxSatisfaitsV1"))
                        .getQueryString();
        String sqlSansDistrict = ((SQLQuery) session.getNamedQuery("sql.update.pam.recalculPiles.stats."
                + "offreEtDemandeParDistrictOrigine.nbVoeuxSatisfaitsV1.elevesSansDistrict")).getQueryString();
        for (int i = 1; i <= nbVoeux; i++) {
            String sqlToRun = StringUtils.replace(sql, "Z.NB_SATIS_V1", "Z.NB_SATIS_V" + i);
            DaoUtils.executeHibernateQuery(
                    session.createSQLQuery(sqlToRun).setInteger("rang", i).setParameter("numeroTour", numeroTour));
            // autres (�l�ves qui n'ont pas de district origine)
            sqlToRun = StringUtils.replace(sqlSansDistrict, "Z.NB_SATIS_V1", "Z.NB_SATIS_V" + i);
            DaoUtils.executeHibernateQuery(
                    session.createSQLQuery(sqlToRun).setInteger("rang", i).setParameter("numeroTour", numeroTour));
        }

        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.insert.pam.recalculPiles.stats.offreEtDemandeParDistrictOrigine.sommeNbVoeuxSatisfaits")
                .setParameter("numeroTour", numeroTour));

        StringBuffer totalODIS = new StringBuffer("0");
        for (int i = 1; i <= nbVoeux; i++) {

            Query requeteMaj = session.createSQLQuery("update GZ_ODIS set PO_V" + i + "=(100.0*NB_SATIS_V" + i
                    + "/NB_DEM) where NB_DEM>0 and VAL_TOUR = :numeroTour");
            requeteMaj.setParameter("numeroTour", numeroTour);
            DaoUtils.executeHibernateQuery(requeteMaj);

            totalODIS.append(" + NB_SATIS_V");
            totalODIS.append(i);
        }

        Query requeteTotal = session
                .createSQLQuery("update GZ_ODIS set TO_SATIS=" + totalODIS + " where VAL_TOUR = :numeroTour");
        requeteTotal.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteTotal);

        Query requetePourcentage = session.createSQLQuery("update GZ_ODIS set "
                + "PO_SATIS=(100.0*TO_SATIS/NB_DEM) where NB_DEM>0 and VAL_TOUR = :numeroTour");
        requetePourcentage.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requetePourcentage);
    }

    /**
     * Cette m�thode met � jour la statistique "Mouvement entre districts".
     */
    private void majStatsMouvementEntreDistrict() {
        Session session = HibernateUtil.currentSession();

        // Suppression des anciennes donn�es
        Query requeteEffacement = session.createSQLQuery("delete from GZ_MDIS where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.mouvementEntreDistrict")
                        .setParameter("numeroTour", numeroTour));
        // hors aca. ou pas �tab. origine
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.insert.pam.recalculPiles.stats.mouvementEntreDistrict.parDistrictAcceuilEtSexe")
                .setParameter("numeroTour", numeroTour));

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.mouvementEntreDistrict.somme")
                        .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Mouvement entre d�partements".
     */
    private void majStatsMouvementEntreDepartement() {
        Session session = HibernateUtil.currentSession();

        // Suppression des anciennes donn�es
        Query requeteEffacement = session.createSQLQuery("delete from GZ_MDPT where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.mouvementEntreDepartement")
                        .setParameter("numeroTour", numeroTour));

        // hors aca.
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.mouvementEntreDepartement.horsAca")
                        .setParameter("numeroTour", numeroTour));

        // somme par d�partement
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.mouvementEntreDepartement.somme")
                        .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Offre et demande par �tablissement d'accueil".
     */
    private void majStatsOffreEtDemandeParEtablissementAccueil() {

        // r�cup�ration du nombre de voeux
        int nbVoeux = offreDemandeManager.getMaxVoeux();

        // r�cup�ration de la session
        Session session = HibernateUtil.currentSession();

        // effacement des donn�es pr�c�dentes
        Query requeteEffacement = session.createSQLQuery("delete from GZ_OETAC where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.offreDemandeParEtabAccueil")
                        .setParameter("numeroTour", numeroTour));

        String sqlNbVoeux = ((SQLQuery) session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.offreDemandeParEtabAccueil.nbVoeux"))
                        .getQueryString();
        String sqlNbVoeuxSatisfaits = ((SQLQuery) session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.offreDemandeParEtabAccueil.nbVoeuxSatisfaits"))
                        .getQueryString();
        for (int i = 1; i <= nbVoeux; i++) {
            String sqlToRun = StringUtils.replace(sqlNbVoeux, "Z.NB_V1", "Z.NB_V" + i);
            DaoUtils.executeHibernateQuery(
                    session.createSQLQuery(sqlToRun).setInteger("rang", i).setParameter("numeroTour", numeroTour));

            sqlToRun = StringUtils.replace(sqlNbVoeuxSatisfaits, "Z.NB_SATIS_V1", "Z.NB_SATIS_V" + i);
            DaoUtils.executeHibernateQuery(
                    session.createSQLQuery(sqlToRun).setInteger("rang", i).setParameter("numeroTour", numeroTour));
        }

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.offreDemandeParEtabAccueil.somme")
                        .setParameter("numeroTour", numeroTour));

        StringBuffer totalNbOETAC = new StringBuffer("0");
        StringBuffer totalSatisOETAC = new StringBuffer("0");
        for (int i = 1; i <= nbVoeux; i++) {
            Query requete = session.createSQLQuery("update GZ_OETAC set PO_SATIS_V" + i + "=(100.0*NB_SATIS_V" + i
                    + "/NB_V" + i + ") where NB_V" + i + ">0 " + "and VAL_TOUR = :numeroTour");
            requete.setParameter("numeroTour", numeroTour);
            DaoUtils.executeHibernateQuery(requete);

            totalNbOETAC.append(" + NB_V");
            totalNbOETAC.append(i);
            totalSatisOETAC.append(" + NB_SATIS_V");
            totalSatisOETAC.append(i);
        }

        Query requeteTotal = session
                .createSQLQuery("update GZ_OETAC set NB_TOT=" + totalNbOETAC + " where VAL_TOUR = :numeroTour");
        requeteTotal.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteTotal);

        Query requeteSatisfaits = session.createSQLQuery(
                "update GZ_OETAC set NB_SATIS_TOT=" + totalSatisOETAC + " where VAL_TOUR = :numeroTour");
        requeteSatisfaits.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteSatisfaits);

        Query requetePourcentageSatisfaits = session.createSQLQuery("update GZ_OETAC set PO_SATIS_TOT="
                + "(100.0*NB_SATIS_TOT/NB_TOT) where NB_TOT>0 and VAL_TOUR = :numeroTour");
        requetePourcentageSatisfaits.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requetePourcentageSatisfaits);
    }

    /**
     * Cette m�thode met � jour la statistique "Offre et demande par �tablissement d'origine".
     */
    private void majStatsOffreEtDemandeParEtablissementOrigine() {
        // r�cup�ration du nombre de voeux
        int nbVoeux = offreDemandeManager.getMaxVoeux();
        Session session = HibernateUtil.currentSession();

        // Suppression des anciennes donn�es
        Query requeteEffacement = session.createSQLQuery("delete from GZ_OETAO where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        // �l�ves de l'acad�mie
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.insert.pam.recalculPiles.stats.offreDemandeParEtabOrigine.elevesDeAcademie")
                .setParameter("numeroTour", numeroTour));
        // �l�ves hors acad�mie
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.insert.pam.recalculPiles.stats.offreDemandeParEtabOrigine.elevesHorsAcademie")
                .setParameter("numeroTour", numeroTour));

        String sqlElevesDeAcademieAvecIdEtab = ((SQLQuery) session
                .getNamedQuery("sql.update.pam.recalculPiles.stats."
                        + "offreDemandeParEtabOrigine.nombreVoeux.elevesDeAcademieAvecIdEtab")).getQueryString();
        String sqlElevesDeAcademieSansIdEtab = ((SQLQuery) session
                .getNamedQuery("sql.update.pam.recalculPiles.stats."
                        + "offreDemandeParEtabOrigine.nombreVoeux.elevesDeAcademieSansIdEtab")).getQueryString();
        String sqlElevesHorsAcademie = ((SQLQuery) session.getNamedQuery("sql.update.pam.recalculPiles.stats."
                + "offreDemandeParEtabOrigine.nombreVoeux.elevesHorsAcademie")).getQueryString();
        String sqlDemandesSatifAvecIdEtab = ((SQLQuery) session.getNamedQuery("sql.update.pam.recalculPiles.stats."
                + "offreDemandeParEtabOrigine.nombreVoeuxSatisfaits.elevesDeAcademieAvecIdEtab")).getQueryString();
        String sqlDemandesSatifSansIdEtab = ((SQLQuery) session.getNamedQuery("sql.update.pam.recalculPiles.stats."
                + "offreDemandeParEtabOrigine.nombreVoeuxSatisfaits.elevesDeAcademieSansIdEtab")).getQueryString();
        String sqlDemandesSatifHorsAcademie = ((SQLQuery) session
                .getNamedQuery("sql.update.pam.recalculPiles.stats."
                        + "offreDemandeParEtabOrigine.nombreVoeuxSatisfaits.elevesHorsAcademie")).getQueryString();

        for (int i = 1; i <= nbVoeux; i++) {
            // nb demandes des �l�ves de l'acad�mie avec un id �tab.
            String sqlElevesDeAcademieAvecIdEtabVi = StringUtils.replace(sqlElevesDeAcademieAvecIdEtab, "Z.NB_V1",
                    "Z.NB_V" + i);
            // nb demandes des �l�ves de l'acad�mie sans id �tab.
            String sqlElevesDeAcademieSansIdEtabVi = StringUtils.replace(sqlElevesDeAcademieSansIdEtab, "Z.NB_V1",
                    "Z.NB_V" + i);
            // nb demandes des �l�ves hors acad�mie
            String sqlElevesHorsAcademieVi = StringUtils.replace(sqlElevesHorsAcademie, "Z.NB_V1", "Z.NB_V" + i);
            // nb demandes satisfaites des �l�ves de l'acad�mie avec un id �tab.
            String sqlDemandesSatifAvecIdEtabVi = StringUtils.replace(sqlDemandesSatifAvecIdEtab, "Z.NB_SATIS_V1",
                    "Z.NB_SATIS_V" + i);
            // nb demandes satisfaites des �l�ves de l'acad�mie sans id �tab.
            String sqlDemandesSatifSansIdEtabVi = StringUtils.replace(sqlDemandesSatifSansIdEtab, "Z.NB_SATIS_V1",
                    "Z.NB_SATIS_V" + i);
            // nb demandes satisfaites des �l�ves hors acad�mie
            String sqlDemandesSatifHorsAcademieVi = StringUtils.replace(sqlDemandesSatifHorsAcademie,
                    "Z.NB_SATIS_V1", "Z.NB_SATIS_V" + i);

            // execution des requetes
            DaoUtils.executeHibernateQuery(session.createSQLQuery(sqlElevesDeAcademieAvecIdEtabVi)
                    .setInteger("rang", i).setParameter("numeroTour", numeroTour));
            DaoUtils.executeHibernateQuery(session.createSQLQuery(sqlElevesDeAcademieSansIdEtabVi)
                    .setInteger("rang", i).setParameter("numeroTour", numeroTour));
            DaoUtils.executeHibernateQuery(session.createSQLQuery(sqlElevesHorsAcademieVi).setInteger("rang", i)
                    .setParameter("numeroTour", numeroTour));
            DaoUtils.executeHibernateQuery(session.createSQLQuery(sqlDemandesSatifAvecIdEtabVi)
                    .setInteger("rang", i).setParameter("numeroTour", numeroTour));
            DaoUtils.executeHibernateQuery(session.createSQLQuery(sqlDemandesSatifSansIdEtabVi)
                    .setInteger("rang", i).setParameter("numeroTour", numeroTour));
            DaoUtils.executeHibernateQuery(session.createSQLQuery(sqlDemandesSatifHorsAcademieVi)
                    .setInteger("rang", i).setParameter("numeroTour", numeroTour));
        }
        // calcul des sommes
        DaoUtils.executeHibernateQuery(session.getNamedQuery(
                "sql.insert.pam.recalculPiles.stats.offreDemandeParEtabOrigine.nombreVoeuxSatisfaits.somme")
                .setParameter("numeroTour", numeroTour));

        StringBuffer totalOETAO = new StringBuffer("0");
        StringBuffer totalSatisOETAO = new StringBuffer("0");
        for (int i = 1; i <= nbVoeux; i++) {
            Query requeteMaj = session.createSQLQuery("update GZ_OETAO set PO_SATIS_V" + i + "=(100.0*NB_SATIS_V"
                    + i + "/NB_V" + i + ") where NB_V" + i + ">0 and VAL_TOUR = :numeroTour");
            requeteMaj.setParameter("numeroTour", numeroTour);
            DaoUtils.executeHibernateQuery(requeteMaj);

            totalOETAO.append(" + NB_V");
            totalOETAO.append(i);
            totalSatisOETAO.append(" + NB_SATIS_V");
            totalSatisOETAO.append(i);
        }

        Query requeteTotal = session
                .createSQLQuery("update GZ_OETAO set NB_TOT=" + totalOETAO + " where VAL_TOUR = :numeroTour");
        requeteTotal.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteTotal);

        Query requeteTotalSatisfaits = session.createSQLQuery(
                "update GZ_OETAO set NB_SATIS_TOT=" + totalSatisOETAO + " where VAL_TOUR = :numeroTour");
        requeteTotalSatisfaits.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteTotalSatisfaits);

        Query requetePourcentage = session.createSQLQuery("update GZ_OETAO set "
                + "PO_SATIS_TOT=(100.0*NB_SATIS_TOT/NB_TOT) where NB_TOT>0 and VAL_TOUR = :numeroTour");
        requetePourcentage.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requetePourcentage);
    }

    /**
     * Cette m�thode met � jour la statistique "Age moyen par formation d'origine".
     */
    private void majStatsAgeMoyenParFormationOrigine() {
        Session session = HibernateUtil.currentSession();

        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.delete.pam.recalculPiles.stats.ageMoyenParFormationOrigine.initTour")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.ageMoyenParFormationOrigine.parSexe")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.ageMoyenParFormationOrigine")
                        .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Point par pile".
     */
    private void majStatsPointParPileParEtablissementAccueil() {
        Session session = HibernateUtil.currentSession();

        // Suppression des anciennes donn�es
        Query requeteEffacement = session.createSQLQuery("delete from GZ_PIL where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        // avec sexe
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.pointParPile.avecSexe")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.pointParPile.avecSexe.nbAt")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.pointParPile.avecSexe.minAt")
                        .setParameter("numeroTour", numeroTour));

        // sans sexe
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.pointParPile.sansSexe")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.pointParPile.sansSexe.nbAt")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.pointParPile.sansSexe.minAt")
                        .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Bilan par formation".
     */
    private void majStatsBilanParFormation() {
        Session session = HibernateUtil.currentSession();

        // Vide la table
        Query requeteEffacement = session.createSQLQuery("delete from GZ_BPF where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        // Insertion des voeux
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.bilanParFormation")
                        .setParameter("numeroTour", numeroTour));

        // pour le voeu 1
        // mise � jour du nombre eleves demandeurs
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesDemandeursV1.sexe1")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesDemandeursV1.sexe2")
                .setParameter("numeroTour", numeroTour));
        // mise � jour du nombre eleves admis
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesAdmisV1.sexe1")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesAdmisV1.sexe2")
                .setParameter("numeroTour", numeroTour));
        // tous voeux
        // mise � jour du nombre eleves demandeurs
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesDemandeurs.sexe1")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesDemandeurs.sexe2")
                .setParameter("numeroTour", numeroTour));
        // mise � jour du nombre eleves admis
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesAdmis.sexe1")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.bilanParFormation.nombreElevesAdmis.sexe2")
                .setParameter("numeroTour", numeroTour));
    }

    /**
     * Cette m�thode met � jour la statistique "Analyse par MEF origine".
     */
    private void majStatsAnalyseFormationOrigine() {
        // r�cup�ration de la session
        Session session = HibernateUtil.currentSession();

        // effacement des donn�es pr�c�dentes
        Query requeteEffacement = session.createSQLQuery("delete from GZ_AFO where VAL_TOUR = :numeroTour");
        requeteEffacement.setParameter("numeroTour", numeroTour);
        DaoUtils.executeHibernateQuery(requeteEffacement);

        // insertion des donn�es : d�partement, formation d'origine, formation d'accueil, nb affect�s
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.analyseFormationOrigine.academie")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.analyseFormationOrigine.horsAcademie")
                        .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.insert.pam.recalculPiles.stats.analyseFormationOrigine.tous")
                        .setParameter("numeroTour", numeroTour));

        // nombre d'�l�ves demandeurs en voeu 1
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbDemandesV1.academie")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbDemandesV1.horsAcademie")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbDemandesV1.tous")
                .setParameter("numeroTour", numeroTour));

        // nombre d'�l�ves affect�s
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbAffectes.academie")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbAffectes.horsAcademie")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(
                session.getNamedQuery("sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbAffectes.tous")
                        .setParameter("numeroTour", numeroTour));

        // nombre d'�l�ves affect�s en voeu 1
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbAffectesV1.academie")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery(
                        "sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbAffectesV1.horsAcademie")
                .setParameter("numeroTour", numeroTour));
        DaoUtils.executeHibernateQuery(session
                .getNamedQuery("sql.update.pam.recalculPiles.stats.analyseFormationOrigine.nbAffectesV1.tous")
                .setParameter("numeroTour", numeroTour));
    }
}
