/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam.comparators;

import java.io.Serializable;
import java.util.Comparator;

import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.helper.MatiereHelper;

/**
 * Comparateur de bonus entre fili�res selon l'ordre croissant de g�n�ralisation.
 */
public class BonusFiliereComparator implements Comparator<BonusFiliere>, Serializable {

    /** Le param�tre 1 est moins g�n�ral que le param�tre 2. */
    private static final int BF1_SUPP_BF2 = -1;

    /** Le param�tre 1 est aussi g�n�ral que le param�tre 2. */
    private static final int BF1_EGAL_BF2 = 1;

    /** Le param�tre 1 est plus g�n�ral que le param�tre 2. */
    private static final int BF1_INF_BF2 = 2;

    /** Nombre maximal d'options pour un bonus fili�re. */
    private static final int NB_OPT_MAX = 3;

    /**
     * Num�ro de version de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(BonusFiliere bf1, BonusFiliere bf2) {

        // comparaison des formations
        int cFormation = compareFormations(bf1, bf2);
        if (cFormation != BF1_EGAL_BF2) {
            return cFormation;
        }

        // comparaison des options
        int cOptions = compareOptions(bf1, bf2);
        if (cOptions != BF1_EGAL_BF2) {
            return cOptions;
        }

        // meme niveau
        return BF1_EGAL_BF2;
    }

    /**
     * Compare les deux formations de deux bonus fili�res. On attribue un ordre
     * de valuation � chacune des formation puis on les compare.
     * 
     * @param bf1
     *            premier bonus fili�re
     * @param bf2
     *            second bonus fili�re
     * @return valeur de comparaison
     */
    private int compareFormations(BonusFiliere bf1, BonusFiliere bf2) {
        // valeurs des formations saisies
        int nbF1 = genValueFormation(bf1);
        int nbF2 = genValueFormation(bf2);

        // resultat
        if (nbF1 > nbF2) {
            return BF1_INF_BF2;
        } else if (nbF1 == nbF2) {
            return BF1_EGAL_BF2;
        } else {
            return BF1_SUPP_BF2;
        }
    }

    /**
     * Attribue une valeur de g�n�ralisation de la formation au bonus fili�re.
     * Un nombre plus grand indique une g�n�ralisation plus grande.
     * 
     * @param bf
     *            le bonus fili�re � classer
     * @return valeur de g�n�ralisation
     */
    private int genValueFormation(BonusFiliere bf) {
        // origine
        Formation fO = bf.getFormationOrigine();
        String mnemoO = bf.getMnemoniqueOrigine();

        // accueil
        Formation fA = bf.getFormationAccueil();
        String mnemoA = bf.getMnemoniqueAccueil();

        // tout renseign�
        if (fO != null && fA != null) {
            return 1;
        }

        // coSpeO � $
        if (mnemoO != null && fA != null) {
            return 2;
        }

        // coSpeA � $
        if (fO != null && mnemoA != null) {
            return 3;
        }

        // coSpeO et coSpeA � $
        if (mnemoO != null && mnemoA != null) {
            return 4;
        }

        // mnemoA et coSpeA � $
        if (fO != null) {
            return 5;
        }

        // coSpeO et mnemoA et coSpeA � $
        return 6;
    }

    /**
     * @param bf1
     *            le bonus fili�re 1
     * @param bf2
     *            le bonus fili�re 2
     * @return valeur de comparaison
     */
    private int compareOptions(BonusFiliere bf1, BonusFiliere bf2) {
        // valeurs des formations saisies
        int nbF1 = genValueOptions(bf1);
        int nbF2 = genValueOptions(bf2);

        // resultat
        if (nbF1 > nbF2) {
            return BF1_INF_BF2;
        } else if (nbF1 == nbF2) {
            return BF1_EGAL_BF2;
        } else {
            return BF1_SUPP_BF2;
        }
    }

    /**
     * Attribue une valeur de g�n�ralisation aux options du param�tre par
     * formation d'origine. Un nombre plus grand indique une g�n�ralisation plus
     * grande.
     * 
     * @param bf
     *            le bonus fili�re
     * @return valeur de g�n�ralisation
     */
    private int genValueOptions(BonusFiliere bf) {
        // options origines
        Matiere opt1 = bf.getOptionOrigine1();
        boolean isOpt1LV = MatiereHelper.isSurMatiereLV(opt1);
        Matiere opt2 = bf.getOptionOrigine2();
        boolean isOpt2LV = MatiereHelper.isSurMatiereLV(opt2);

        // ens det
        Matiere ensOpt = bf.getMatiereEnseigOptionnel();
        boolean isEnsOptLV = MatiereHelper.isSurMatiereLV(ensOpt);

        // nb option
        int nbOpt = (opt1 != null ? 1 : 0) + (opt2 != null ? 1 : 0) + (ensOpt != null ? 1 : 0);

        // nb de LV
        int nbLV = (isOpt1LV ? 1 : 0) + (isOpt2LV ? 1 : 0) + (isEnsOptLV ? 1 : 0);

        // calcul du resultat
        return (NB_OPT_MAX - nbOpt) * (NB_OPT_MAX + 1) + nbLV;
    }
}
