/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam.comparators;

import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigineModifiee;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.helper.MatiereHelper;

import java.io.Serializable;
import java.util.Comparator;

/** Comparateur de formations origine modif�es (MEFOMOD). */
public class FormationOrigineModifieeComparator implements Comparator<FormationOrigineModifiee>, Serializable {

    /** Le premier mef origine modifi� a un motif d'options origine moins g�n�rique que le second. */
    protected static final int BF1_SUPP_BF2 = -1;

    /** Egalit� des mef origine modifi� du point de vue de la g�n�ricit� des motifs d'options origine. */
    protected static final int BF1_EGAL_BF2 = 1;

    /** Le premier mef origine modifi� a un motif d'options origine plus g�n�rique que le second. */
    protected static final int BF1_INF_BF2 = 2;

    /** Nombre maximal d'options origine pour un mefomod. */
    private static final int NB_OPT_MAX = 2;

    /**
     * Num�ro de version de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    // FIXME : la valeur de retour du comparateur Mefomod n'est pas celle attendue dans l'interface Comparator.
    @Override
    public int compare(FormationOrigineModifiee fom1, FormationOrigineModifiee fom2) {

        // comparaison des options
        int cOptions = compareOptionsOrigine(fom1, fom2);
        if (cOptions != BF1_EGAL_BF2) {
            return cOptions;
        }

        // meme niveau
        return BF1_EGAL_BF2;
    }

    /**
     * Compare les options initiales des formations origine modifi�es.
     * 
     * @param fom1
     *            param�tre par formation d'origine modifi�e 1
     * @param fom2
     *            param�tre par formation d'origine modifi�e 1
     * @return valeur de comparaison
     */
    private int compareOptionsOrigine(FormationOrigineModifiee fom1, FormationOrigineModifiee fom2) {
        // valeurs des formations saisies
        int nbF1 = genValueOptions(fom1);
        int nbF2 = genValueOptions(fom2);

        // resultat
        if (nbF1 > nbF2) {
            return BF1_INF_BF2;
        } else if (nbF1 == nbF2) {
            return BF1_EGAL_BF2;
        } else {
            return BF1_SUPP_BF2;
        }
    }

    /**
     * Calcule le rang de g�n�ricit� des options d'une formation origine modifi�e.
     * 
     * <p>
     * Cette valeur permet d'ordonner les paramefo selon leur "masque" sur les les options origine :
     * plus le "masque" est g�n�ral, plus la valeur est �l�v�e.
     * </p>
     * <code> rang = (NB_OPT_MAX - nbOptionsOrigine) * (NB_OPT_MAX + 1) + nbLanguesVivantesOrigine</code>
     * 
     * <p>
     * O� :
     * </p>
     * <ul>
     * <li>NB_OPT_MAX=2 : nombre d'options origine possibles</li>
     * <li>nbOptionsOrigine : nombre d'options origine valoris�es</li>
     * <li>nbLanguesVivantesOrigine : nombre de langues vivantes g�n�riques origine valoris�es</li>
     * </ul>
     * 
     * @param fom
     *            la formation origine modifi�e
     * @return rang de g�n�ricit� du paramefo
     */
    protected int genValueOptions(FormationOrigineModifiee fom) {
        // options origines
        Matiere opt1 = fom.getOptionInitiale1();
        boolean isOpt1LV = MatiereHelper.isSurMatiereLV(opt1);

        Matiere opt2 = fom.getOptionInitiale2();
        boolean isOpt2LV = MatiereHelper.isSurMatiereLV(opt2);

        // Nombre d'options origine
        int nbOptionsOrigine = (opt1 != null ? 1 : 0) + (opt2 != null ? 1 : 0);

        // Nb de langues vivantes origine
        int nbLanguesVivantesOrigine = (isOpt1LV ? 1 : 0) + (isOpt2LV ? 1 : 0);

        // calcul du resultat
        return (NB_OPT_MAX - nbOptionsOrigine) * (NB_OPT_MAX + 1) + nbLanguesVivantesOrigine;
    }
}
