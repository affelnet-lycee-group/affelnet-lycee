/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam.comparators;

import java.io.Serializable;
import java.util.Comparator;

import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;

public class LienZoneGeoComparator implements Comparator<LienZoneGeo>, Serializable {

    private static final int LZG1_SUPP_LZG2 = -1;
    private static final int LZG1_EGAL_LZG2 = 1;
    private static final int LZG1_INF_LZG2 = 2;

    /**
     * Num�ro de version de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(LienZoneGeo lzg1, LienZoneGeo lzg2) {

        // comparaison des zgO
        ZoneGeo zg1 = lzg1.getZoneGeoOrigine();
        ZoneGeo zg2 = lzg2.getZoneGeoOrigine();
        if (zg1 == null && zg2 != null) {
            return LZG1_INF_LZG2;
        } else if (zg1 != null && zg2 == null) {
            return LZG1_SUPP_LZG2;
        }

        // meme niveau
        return LZG1_EGAL_LZG2;
    }
}
