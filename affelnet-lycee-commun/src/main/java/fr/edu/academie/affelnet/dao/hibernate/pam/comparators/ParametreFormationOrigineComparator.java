/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam.comparators;

import java.io.Serializable;
import java.util.Comparator;

import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.ParametreFormationOrigine;
import fr.edu.academie.affelnet.helper.MatiereHelper;

/**
 * Comparateur de param�tres par formation d'origine selon l'ordre croissant de g�n�ralisation.
 * 
 * <p>
 * Un param�tre par formation d'origine (paramefo) est compos� d'une formation (MEF) origine
 * accompagn� de 2 options.
 * </p>
 * 
 * <p>
 * Un paramefo sera not� ici sous la forme (lic_mef, co_spemef) [cg_opt1,cgopt2]
 * </p>
 * 
 * <p>
 * Il est possible d'avoir un caract�re '$' pour g�n�raliser la formation
 * ou les options ce qui donne, par exemple ('3DANSE', '') ['DANSE','$'].
 * En pratique, on utilise des valeurs nulles.
 * </p>
 *
 * <p>
 * Le paramefo le plus g�n�ral (grand) serait ($,$) [$,$] toutefois le mn�monique
 * est obligatoire, ce qui fait qu'il ne sera pas rencontr� en pratique.
 * </p>
 */
public class ParametreFormationOrigineComparator implements Comparator<ParametreFormationOrigine>, Serializable {

    /** Le param�tre 1 est moins g�n�ral que le param�tre 2. */
    private static final int PFO1_MOINSGEN_PFO2 = -1;

    /** Le param�tre 1 est aussi g�n�ral que le param�tre 2. */
    private static final int PFO1_AUSSIGEN_PFO2 = 0;

    /** Le param�tre 1 est plus g�n�ral que le param�tre 2. */
    private static final int PFO1_PLUSGEN_PFO2 = 1;

    /** Nombre maximal d'options pour un paramefo. */
    private static final int NB_OPT_MAX = 2;

    /**
     * Num�ro de version de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(ParametreFormationOrigine pfo1, ParametreFormationOrigine pfo2) {

        // comparaison des formations
        int cFormation = compareFormations(pfo1, pfo2);
        if (cFormation != PFO1_AUSSIGEN_PFO2) {
            return cFormation;
        }

        // comparaison des options
        int cOptions = compareOptions(pfo1, pfo2);
        if (cOptions != PFO1_AUSSIGEN_PFO2) {
            return cOptions;
        }

        // meme niveau
        return PFO1_AUSSIGEN_PFO2;
    }

    /**
     * Compare les deux formations de deux param�tres par formation d'origine.
     * On attribue un ordre de valuation � chacune des formation puis on les compare.
     * 
     * @param pfo1
     *            premier param�tre par formation d'origine
     * @param pfo2
     *            second param�tre par formation d'origine
     * @return valeur de comparaison
     */
    private int compareFormations(ParametreFormationOrigine pfo1, ParametreFormationOrigine pfo2) {
        // valeurs des formations saisies
        int nbF1 = genValueFormation(pfo1);
        int nbF2 = genValueFormation(pfo2);

        // resultat
        if (nbF1 > nbF2) {
            return PFO1_PLUSGEN_PFO2;
        } else if (nbF1 == nbF2) {
            return PFO1_AUSSIGEN_PFO2;
        } else {
            return PFO1_MOINSGEN_PFO2;
        }
    }

    /**
     * Attribue une valeur de g�n�ralisation de la formation au param�tre par formation d'origine.
     * Un nombre plus grand indique une g�n�ralisation plus grande.
     * 
     * @param pfo
     *            param�tre par formation d'origine � classer
     * @return valeur de g�n�ralisation
     */
    private int genValueFormation(ParametreFormationOrigine pfo) {
        Formation formation = pfo.getFormationOrigine();

        if (formation != null) {
            // formation renseign�e
            return 1;
        } else {
            // m�tier � $
            return 2;
        }
    }

    /**
     * Compare les options de deux param�tres par formation origine.
     * 
     * @param pfo1
     *            premier param�tre par formation d'origine
     * @param pfo2
     *            second param�tre par formation d'origine
     * @return valeur de comparaison
     */
    private int compareOptions(ParametreFormationOrigine pfo1, ParametreFormationOrigine pfo2) {
        // valeurs des formations saisies
        int nbO1 = genValueOptions(pfo1);
        int nbO2 = genValueOptions(pfo2);

        // resultat
        if (nbO1 > nbO2) {
            return PFO1_PLUSGEN_PFO2;
        } else if (nbO1 == nbO2) {
            return PFO1_AUSSIGEN_PFO2;
        } else {
            return PFO1_MOINSGEN_PFO2;
        }
    }

    /**
     * Attribue une valeur de g�n�ralisation aux options du param�tre par formation d'origine.
     * Un nombre plus grand indique une g�n�ralisation plus grande.
     * 
     * @param pfo
     *            param�tre par formation d'origine
     * @return valeur de g�n�ralisation
     */
    private int genValueOptions(ParametreFormationOrigine pfo) {

        /*
         * On calcule le resultat en partant du nombre maximal d'options
         * NB_OPT_MAX (qui est la borne sup), duquel on retire le nombre d'options
         * Donc, plus on a d'options, moins le paramefo est g�n�ral, ce qui donne
         * (NB_OPT_MAX - nbOpt).
         * 
         * Il faut �galement prendre en compte qu'une option peut �tre une langue vivante
         * et qu'il est permis de saisir des LV banalis�s, par exemple
         * 'LV 3' est plus g�n�rale 'ITA 3' d'ou l'ajout d'un test sur-mati�re.
         * 
         * L'aspect pr�sence/absence est plus important que la banalisation des LV
         * donc on multiplie par (NB_OPT_MAX + 1) la valeur de g�n�ralisation des options
         * comme un 'poids fort' avant d'ajouter le nombre de LV banalis�es pr�sentes.
         */

        // options origine
        Matiere option1 = pfo.getOption1();
        boolean isOption1LV = MatiereHelper.isSurMatiereLV(option1);
        Matiere option2 = pfo.getOption2();
        boolean isOption2LV = MatiereHelper.isSurMatiereLV(option2);

        // Nombre d'options
        int nbOpt = (option1 != null ? 1 : 0) + (option2 != null ? 1 : 0);

        // Nombre de LV g�n�ralis�es
        int nbLV = (isOption1LV ? 1 : 0) + (isOption2LV ? 1 : 0);

        // calcul du resultat
        return (NB_OPT_MAX - nbOpt) * (NB_OPT_MAX + 1) + nbLV;
    }
}
