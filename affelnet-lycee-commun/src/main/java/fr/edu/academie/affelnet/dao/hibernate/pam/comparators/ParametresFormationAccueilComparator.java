/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam.comparators;

import java.io.Serializable;
import java.util.Comparator;

import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;

/**
 * Comparateur de param�tres par formation d'accueil selon l'ordre croissant de g�n�ralisation.
 */
public class ParametresFormationAccueilComparator implements Comparator<ParametresFormationAccueil>, Serializable {

    private static final int PFA1_SUPP_PFA2 = -1;
    private static final int PFA1_EGAL_PFA2 = 1;
    private static final int PFA1_INF_PFA2 = 2;

    /**
     * Num�ro de version de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(ParametresFormationAccueil pfa1, ParametresFormationAccueil pfa2) {

        Matiere enseignPfa1 = pfa1.getMatiereEnseigOptionnel();
        Matiere enseignPfa2 = pfa2.getMatiereEnseigOptionnel();

        if (enseignPfa1 != null && enseignPfa2 == null) {
            return PFA1_SUPP_PFA2;
        } else if (enseignPfa1 == null && enseignPfa2 != null) {
            return PFA1_INF_PFA2;
        }

        return PFA1_EGAL_PFA2;
    }
}
