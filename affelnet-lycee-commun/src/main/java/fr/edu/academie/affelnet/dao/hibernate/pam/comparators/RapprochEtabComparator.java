/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.pam.comparators;

import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.RapprochEtab;
import fr.edu.academie.affelnet.helper.MatiereHelper;

import java.io.Serializable;
import java.util.Comparator;

public class RapprochEtabComparator implements Comparator<RapprochEtab>, Serializable {

    private static final int RE1_SUPP_RE2 = -1;
    private static final int RE1_EGAL_RE2 = 1;
    private static final int RE1_INF_RE2 = 2;

    /** Nombre maximal d'options pour un rapprochement entre �tablissements. */
    private static final int NB_OPT_MAX = 1;

    /**
     * Num�ro de version de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(RapprochEtab re1, RapprochEtab re2) {

        // comparaison des formations
        int cEF = compareEtabsEtFormations(re1, re2);
        if (cEF != RE1_EGAL_RE2) {
            return cEF;
        }

        // comparaison des options
        int cOptions = compareOptions(re1, re2);
        if (cOptions != RE1_EGAL_RE2) {
            return cOptions;
        }

        // meme niveau
        return RE1_EGAL_RE2;
    }

    private int compareEtabsEtFormations(RapprochEtab re1, RapprochEtab re2) {
        // valeurs des formations saisies
        int nbF1 = genValueEtabsEtFormations(re1);
        int nbF2 = genValueEtabsEtFormations(re2);

        // resultat
        if (nbF1 > nbF2) {
            return RE1_INF_RE2;
        } else if (nbF1 == nbF2) {
            return RE1_EGAL_RE2;
        } else {
            return RE1_SUPP_RE2;
        }
    }

    private final static String XX = "xx";
    private final static String X$ = "x$";
    private final static String $$ = "$$";
    private final static String DEP = "DEP";

    private int genValueEtabsEtFormations(RapprochEtab re) {
        // origine
        Etablissement etabO = re.getEtablissementOrigine();
        String mnemoO = re.getMnemoniqueOrigine();
        String specialiteO = re.getCodeSpecialiteOrigine();
        String idEtabO = re.getIdEtablissementOrigine();
        // accueil
        Formation fA = re.getFormationAccueil();
        String mnemoA = re.getMnemoniqueAccueil();
        String specialiteA = re.getCodeSpecialiteAccueil();

        boolean mnemoASaisi = !(mnemoA == null);
        boolean speASaisi = !(specialiteA == null);
        boolean mnemoOSaisi = !(mnemoO == null);
        boolean speOSaisi = !(specialiteO == null);
        boolean etabOSaisi = !(etabO == null);
        boolean isDepO = (idEtabO == null) ? false : (idEtabO.length() == 3);

        String formationAccueil = null;
        String formationOrigine = null;
        String etablissementOrigine = null;

        if( mnemoASaisi && speASaisi){
            formationAccueil = XX;
        } else if( mnemoASaisi && !speASaisi){
            formationAccueil = X$;
        } else{
            formationAccueil = $$;
        }
        if( mnemoOSaisi && speOSaisi){
            formationOrigine = XX;
        } else if( mnemoOSaisi && !speOSaisi){
            formationOrigine = X$;
        } else{
            formationOrigine = $$;
        }
        if(etabOSaisi){
            etablissementOrigine = XX;
            if(isDepO){
                etablissementOrigine = DEP;
            }
        } else {
            etablissementOrigine = $$;
        }
        // �tablissement
        //  XX = UAI saisi
        //  DEP = D�partement saisi
        //  $$ = n'importe quel �tablissement

        //Formation
        // XX = mn�mo saisi + sp�cialit� saisie
        // X$= mn�mo saisi + sp�cialit� $
        // $$ = mn�mo $+ sp�cialit� $ (n'importe quelle formation)

        if( formationAccueil.equals(XX) && etablissementOrigine.equals(XX) && formationOrigine.equals(XX)){
            return 1;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals(XX) && formationOrigine.equals(X$)){
            return 2;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals(XX) && formationOrigine.equals($$)){
            return 3;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals(DEP) && formationOrigine.equals(XX)){
            return 4;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals($$) && formationOrigine.equals(XX)){
            return 5;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals(XX) && formationOrigine.equals(XX)){
            return 6;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals(XX) && formationOrigine.equals(XX)){
            return 7;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals(XX) && formationOrigine.equals(X$)){
            return 8;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals(XX) && formationOrigine.equals(X$)){
            return 9;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals(XX) && formationOrigine.equals($$)){
            return 10;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals(XX) && formationOrigine.equals($$)){
            return 11;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals(DEP) && formationOrigine.equals(XX)){
            return 12;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals(DEP) && formationOrigine.equals(XX)){
            return 13;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals($$) && formationOrigine.equals(XX)){
            return 14;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals($$) && formationOrigine.equals(XX)){
            return 15;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals(DEP) && formationOrigine.equals(X$)){
            return 16;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals($$) && formationOrigine.equals(X$)){
            return 17;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals(DEP) && formationOrigine.equals($$)){
            return 18;
        }
        if( formationAccueil.equals(XX) && etablissementOrigine.equals($$) && formationOrigine.equals($$)){
            return 19;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals(DEP) && formationOrigine.equals(X$)){
            return 20;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals(DEP) && formationOrigine.equals(X$)){
            return 21;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals($$) && formationOrigine.equals(X$)){
            return 22;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals($$) && formationOrigine.equals(X$)){
            return 23;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals(DEP) && formationOrigine.equals($$)){
            return 24;
        }
        if( formationAccueil.equals($$) && etablissementOrigine.equals(DEP) && formationOrigine.equals($$)){
            return 25;
        }
        if( formationAccueil.equals(X$) && etablissementOrigine.equals($$) && formationOrigine.equals($$)){
            return 26;
        }
        return 27;

    }

    private int compareOptions(RapprochEtab re1, RapprochEtab re2) {
        // valeurs des formations saisies
        int nbF1 = genValueOptions(re1);
        int nbF2 = genValueOptions(re2);

        // resultat
        if (nbF1 > nbF2) {
            return RE1_INF_RE2;
        } else if (nbF1 == nbF2) {
            return RE1_EGAL_RE2;
        } else {
            return RE1_SUPP_RE2;
        }
    }

    private int genValueOptions(RapprochEtab re) {
        // ens det
        Matiere ensOpt = re.getMatiereEnseigOptionnel();
        boolean isEnsOptLV = MatiereHelper.isSurMatiereLV(ensOpt);

        // nb option
        int nbOpt = ensOpt != null ? 1 : 0;

        // nb de LV
        int nbLV = isEnsOptLV ? 1 : 0;

        // calcul du resultat
        return (NB_OPT_MAX - nbOpt) * (NB_OPT_MAX + 1) + nbLV;
    }
}
