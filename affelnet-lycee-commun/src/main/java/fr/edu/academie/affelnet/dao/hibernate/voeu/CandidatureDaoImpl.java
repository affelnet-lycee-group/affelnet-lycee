/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.voeu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.voeu.CandidatureDao;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.CandidaturePK;

/**
 * Implementation de la gestion de la candidature d'un �l�ve pour un tour.
 */
@Repository("CandidatureDao")
public class CandidatureDaoImpl extends BaseDaoHibernate<Candidature, CandidaturePK> implements CandidatureDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("id.ine"));
        DEFAULT_ORDERS.add(Tri.asc("id.numeroTour"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(CandidaturePK key) {
        return "La candidature de l'�l�ve " + key.getIne() + " n'existe pas pour le tour " + key.getNumeroTour();
    }

    @Override
    protected void setKey(Candidature object, CandidaturePK key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(Candidature object, CandidaturePK oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� une candidature pour l'�l�ve dans le tour";
    }

    @Override
    protected Class<Candidature> getObjectClass() {
        return Candidature.class;
    }

    @Override
    public Candidature getCandidatureEleve(String ine, short numeroTour) {
        List<Filtre> filtres = new ArrayList<>();

        filtres.add(Filtre.equal("id.ine", ine));
        filtres.add(Filtre.equal("id.numeroTour", numeroTour));
        filtres.add(Filtre.isNotNull("rangAdmission"));

        Filtre andFiltre = Filtre.and(filtres);

        List<Candidature> result = this.lister(andFiltre);

        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public int majFlagsCandidature(Candidature candidature) {
        try {
            Session session = getSession();
            Query q = session.getNamedQuery("hql.candidature.update.flags");

            q.setString("flagValidationAvis", candidature.getFlagValidationAvis());
            q.setString("flagSaisieValide", candidature.getFlagSaisieValide());
            q.setString("flagConformeDo", candidature.getFlagConformeDo());
            q.setDate("dateMAJ", new Date());
            q.setString("ine", candidature.getId().getIne());
            q.setShort("numeroTour", candidature.getId().getNumeroTour());

            return q.executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public List<Candidature> getCandidaturesZoneGeoUnique(short numeroTour) {
        try {
            Session session = getSession();
            Query q = session.getNamedQuery("candidatures.zoneGeoUnique.select.hql");

            q.setParameter("numeroTour", numeroTour);

            return q.list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }
}
