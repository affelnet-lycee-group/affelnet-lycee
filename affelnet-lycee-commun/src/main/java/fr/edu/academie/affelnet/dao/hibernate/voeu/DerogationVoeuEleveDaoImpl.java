/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.voeu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.voeu.DerogationVoeuEleveDao;
import fr.edu.academie.affelnet.domain.voeu.DerogationVoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.DerogationVoeuElevePK;

/**
 * Impl�mentation de la gestion d�rogations pour les voeux des �l�ves.
 */
@Repository("DerogationVoeuEleveDao")
public class DerogationVoeuEleveDaoImpl extends BaseDaoHibernate<DerogationVoeuEleve, DerogationVoeuElevePK>
        implements DerogationVoeuEleveDao {

    /** Liste des ordres par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("id.voeuEleve.eleve.nom"));
        DEFAULT_ORDERS.add(Tri.asc("id.voeuEleve.eleve.prenom"));
        DEFAULT_ORDERS.add(Tri.asc("id.voeuEleve.eleve.prenom2"));
        DEFAULT_ORDERS.add(Tri.asc("id.voeuEleve.eleve.prenom3"));
        DEFAULT_ORDERS.add(Tri.asc("id.voeuEleve.id.ine"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce code";
    }

    @Override
    protected String getMessageObjectNotFound(DerogationVoeuElevePK key) {
        return "La d�rogation �l�ve " + key + " n'existe pas";
    }

    @Override
    protected Class<DerogationVoeuEleve> getObjectClass() {
        return DerogationVoeuEleve.class;
    }

    @Override
    protected boolean hasKeyChange(DerogationVoeuEleve derogationVoeuEleve, DerogationVoeuElevePK oldKey) {
        return !derogationVoeuEleve.getId().equals(oldKey);
    }

    @Override
    protected void setKey(DerogationVoeuEleve derogationVoeuEleve, DerogationVoeuElevePK key) {
        derogationVoeuEleve.setId(key);
    }

    @Override
    public DerogationVoeuEleve creer(DerogationVoeuEleve derogationVoeuEleve) {
        derogationVoeuEleve.setDateCreation(new Date());
        derogationVoeuEleve.getId().getVoeuEleve().getDerogationsVoeuEleve()
                .put(derogationVoeuEleve.getId().getDerogation().getId(), derogationVoeuEleve);
        return derogationVoeuEleve;
    }

    @Override
    public DerogationVoeuEleve maj(DerogationVoeuEleve derogationVoeuEleve, DerogationVoeuElevePK id) {
        derogationVoeuEleve.getId().getVoeuEleve().getDerogationsVoeuEleve()
                .put(derogationVoeuEleve.getId().getDerogation().getId(), derogationVoeuEleve);
        return derogationVoeuEleve;
    }

}
