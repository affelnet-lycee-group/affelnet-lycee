/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.voeu;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.utils.CollecteurScrollableResultSet;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.carteScolaire.TronconCarteScolaire;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationPK;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.EtablissementNational;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.enumeration.StatutAdresseEnum;
import fr.edu.academie.affelnet.enumeration.StatutZoneGeoEnum;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.EleveManager;
import fr.edu.academie.affelnet.utils.ChaineUtils;
import fr.edu.academie.affelnet.utils.Collecteur;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/** Implementation de l'objet d'acc�s aux donn�es �l�ve. */
@Repository("EleveDao")
public class EleveDaoImpl extends BaseDaoHibernate<Eleve, String> implements EleveDao {

    /** Loggeur de la classe. */
    private static final Log LOG = LogFactory.getLog(EleveDaoImpl.class);

    /** Liste des ordres par defaut. */
    private static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();
    static {
        DEFAULT_ORDERS.add(Tri.asc("nom"));
        DEFAULT_ORDERS.add(Tri.asc("prenom"));
        DEFAULT_ORDERS.add(Tri.asc("prenom2"));
        DEFAULT_ORDERS.add(Tri.asc("prenom3"));
        DEFAULT_ORDERS.add(Tri.asc("ine"));
    }

    /**
     * Nombre d'it�rations avant rafra�chissement des scrolls.
     */
    private static final int NB_ITERATON_RAFRAICHISSEMENT = 500;

    /**
     * Le manager pour les campagnes.
     */
    private CampagneAffectationManager campagneAffectationManager;

    /**
     * Le manager pour les �l�ves.
     */
    private EleveManager eleveManager;

    /**
     * @param campagneAffectationManager
     *            the campagneAffectationManager to set
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneAffectationManager = campagneAffectationManager;
    }

    /**
     * @param eleveManager
     *            the eleveManager to set
     */
    @Autowired
    public void setEleveManager(EleveManager eleveManager) {
        this.eleveManager = eleveManager;
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(String ine) {
        return "L'�l�ve " + ine + " n'existe pas";
    }

    @Override
    protected void setKey(Eleve eleve, String ine) {
        eleve.setIne(ine);
    }

    @Override
    protected boolean hasKeyChange(Eleve eleve, String oldIne) {
        return !eleve.getIne().equals(oldIne);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec cet INE";
    }

    @Override
    protected Class<Eleve> getObjectClass() {
        return Eleve.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean isNotesSaisies() {
        try {
            // r�cup�ration de la session
            Session session = getSession();
            Iterator<Number> it = session
                    .createQuery("select count(*) from Eleve e where e.note1 is not null "
                            + "or e.note2 is not null or e.note3 is not null "
                            + "or e.note4 is not null or e.note5 is not null "
                            + "or e.note6 is not null or e.note7 is not null "
                            + "or e.note8 is not null or e.note9 is not null "
                            + "or e.note10 is not null or e.note11 is not null "
                            + "or e.note12 is not null or e.note13 is not null "
                            + "or e.note14 is not null or e.note15 is not null "
                            + "or e.note16 is not null or e.note17 is not null "
                            + "or e.note18 is not null or e.note19 is not null " + "or e.note20 is not null ")
                    .iterate();
            long i = it.next().longValue();
            return (i > 0);
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public List<Eleve> listEleveAvecAgeInvalide(List<Filtre> filtresSupplementaire) {
        // filtre age invalide
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }
        filtres.add(Filtre.or(Filtre.inferieur("age", Eleve.AGE_MINI), Filtre.superieur("age", Eleve.AGE_MAXI)));
        filtres.add(Filtre.hql("(select count(*) from VoeuEleve voeu where voeu.id.ine = o.ine "
                + "and voeu.id.numeroTour = '" + campagneAffectationManager.getNumeroTourCourant() + "') > 0"));
        filtres.add(eleveManager.filtreElevesNonForces());

        return lister(filtres);
    }

    @Override
    public List<Eleve> elevesSansZoneGeographique(List<Filtre> filtresSupplementaire) {
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }
        filtres.add(Filtre.isNull("zoneGeo"));
        filtres.add(Filtre.hql("(select count(*) from VoeuEleve voeu where voeu.id.ine = o.ine "
                + "and voeu.voeu.flagRecensement='N' " + "and voeu.id.numeroTour = '"
                + campagneAffectationManager.getNumeroTourCourant() + "') > 0"));
        filtres.add(eleveManager.filtreElevesNonForces());

        return lister(filtres);
    }

    @Override
    public List<Eleve> listerElevesSansResponsableNotifiable(List<Filtre> filtresSupplementaire) {
        List<Eleve> elevesInvalides = new ArrayList<>();
        Eleve eleve;
        int nbTraite = 0;
        ScrollableResults scroll = prepareScrollElevesResponsablesVoeuxSansRecensement(filtresSupplementaire);
        try {
            while (scroll.next()) {
                eleve = (Eleve) scroll.get(0);

                if (!eleve.hasResponsableNotifiable()) {
                    elevesInvalides.add(eleve);
                }

                if (++nbTraite % NB_ITERATON_RAFRAICHISSEMENT == 0) {
                    HibernateUtil.cleanupSession();
                }
            }

        } finally {
            scroll.close();
        }
        return elevesInvalides;
    }

    @Override
    public List<Eleve> listerElevesAvecResponsableNonNotifiable(List<Filtre> filtresSupplementaire) {
        List<Eleve> elevesInvalides = new ArrayList<>();
        Eleve eleve;
        int nbTraite = 0;
        ScrollableResults scroll = prepareScrollElevesResponsablesVoeuxSansRecensement(filtresSupplementaire);

        try {
            while (scroll.next()) {
                eleve = (Eleve) scroll.get(0);

                if (eleve.hasResponsableLegalNonNotifiable()) {
                    elevesInvalides.add(eleve);
                }

                if (++nbTraite % NB_ITERATON_RAFRAICHISSEMENT == 0) {
                    HibernateUtil.cleanupSession();
                }
            }

        } finally {
            scroll.close();
        }
        return elevesInvalides;
    }

    private ScrollableResults prepareScrollElevesResponsablesVoeuxSansRecensement(List<Filtre> filtresSupplementaire) {
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }

        filtres.add(eleveManager.filtreElevesNonForces());

        // Pr�sence de voeux (hors offre de formation de recensement)
        filtres.add(Filtre.hql("(select count(*) from VoeuEleve voeu where voeu.id.ine = o.ine "
                + "and voeu.voeu.flagRecensement='N' " + "and voeu.id.numeroTour = '"
                + campagneAffectationManager.getNumeroTourCourant() + "') > 0"));
        List<String> jointuresFetch = new ArrayList<>();
        jointuresFetch.add("responsables");
        jointuresFetch.add("responsables.adresseResidence");
        jointuresFetch.add("responsables.adresseResidence.codePostal");
        jointuresFetch.add("responsables.adresseResidence.pays");
        jointuresFetch.add("responsables.lienEleveResponsable");
        jointuresFetch.add("responsables.niveauResponsabilite");
        jointuresFetch.add("responsables.categorieSocioProfessionnelle");

        return scroll(filtres, null, null, null, jointuresFetch, true);
    }

    @Override
    public Filtre filtreElevesSansNote(List<Rang> rangObligatoires) {
        // Parcours des rangs
        StringBuffer clauseNotes = new StringBuffer();

        for (Rang rang : rangObligatoires) {
            if (clauseNotes.length() > 0) {
                clauseNotes.append(" or ");
            }
            clauseNotes.append("o.note" + rang.getValeur() + " is null");
        }
        return Filtre.hql("(" + clauseNotes.toString() + ")");
    }

    @Override
    public Filtre filtreElevesPalierOrigine(short codePalier) {
        return Filtre.equal("palierOrigine.code", codePalier);
    }

    @Override
    public Filtre filtreElevesAvecVoeuAvecBareme(short tourCourant) {
        String selectionVoeuxEleve = "select distinct v.id.ine from VoeuEleve v where v.voeu.code is not null "
                + "and (v.voeu.indicateurPam = '" + TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur()
                + "' and v.flagRefusVoeu = '" + Flag.NON + "' and v.id.numeroTour = '" + tourCourant + "')";

        String hql = " o.ine in (" + selectionVoeuxEleve + ") ";
        return Filtre.hql(hql);
    }

    @Override
    public boolean estMnemoniqueFormationOrigineEleve(String mnemo) {
        try {
            // r�cup�ration de la session
            Session session = getSession();
            Query requeteNombreElevesMnemoniqueMefOrigine = session
                    .getNamedQuery("nombreElevesMnemoniqueFormationOrigine.select.hql");
            requeteNombreElevesMnemoniqueMefOrigine.setParameter("mnemonique", mnemo);

            long nombreEleves = ((Number) requeteNombreElevesMnemoniqueMefOrigine.uniqueResult()).longValue();

            return (nombreEleves > 0);
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Eleve> getListeEleves(Etablissement etablissement) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ves appartenant � l'�tablissement
            return session
                    .createQuery(
                            "select e from Eleve e where e.etablissement.id = '" + etablissement.getId() + "'")
                    .list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public long getNbElevesValides(Etablissement etablissement) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            String requete = "select count(*) from Candidature candidature where"
                    + " candidature.eleve.etablissement.id = '" + etablissement.getId() + "'"
                    + " and candidature.flagSaisieValide = 'O'" + " and candidature.id.numeroTour = '"
                    + campagneAffectationManager.getNumeroTourCourant() + "'";
            return ((Number) session.createQuery(requete).iterate().next()).longValue();

        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> getClasses(Etablissement etablissement) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ves appartenant � l'�tablissement
            return session
                    .createQuery("select distinct e.classe from Eleve e where e.etablissement.id = '"
                            + etablissement.getId() + "' order by 1")
                    .list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Matiere> getOptions(Etablissement etablissement) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ves appartenant � l'�tablissement
            return session
                    .createQuery("select distinct m from Matiere m where m.cleGestion in"
                            + " (select distinct e.optionOrigine2.cleGestion from Eleve e"
                            + " where e.etablissement.id = '" + etablissement.getId()
                            + "') or m.cleGestion in (select distinct e.optionOrigine3.cleGestion"
                            + " from Eleve e where e.etablissement.id = '" + etablissement.getId()
                            + "') order by 1")
                    .list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Formation> getFormations(Etablissement etablissement) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ves appartenant � l'�tablissement
            return session
                    .createQuery("select distinct e.formation from Eleve e where e.etablissement.id = '"
                            + etablissement.getId() + "' order by 1")
                    .list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    /**
     * Inserer les etablissements appartenant � la table AV_ELEVE dont les
     * eleves ont ete selectionnes qui ne sont pas pr�sents dans la table
     * GN_ETABACA.
     */
    @Override
    public void insertionEtablissementsManquants() {
        String reqInsertionEtabManquant = "insert into GN_ETABACA (ID_ETAB,CO_ETAB,CO_ZGEO,CO_MIN,LIL_ETAB,ADR_ETAB,"
                + "LIL_VIL,VAL_CPETAB,CO_PUPRETAB,TYP_CNT,CO_DIS,NU_TELETAB,CO_ZBAS,CO_TYPETAB,CO_NAT,ID_CIO,"
                + "VAL_MELETAB,FL_INT,FL_RAR,FL_VALSAI,DAD_ETABACA,DAF_ETABACA,DAC_ETABACA,DAM_ETABACA)"
                + " select DISTINCT ID_ETAB,CO_ETAB,CO_ZGEO,CO_MIN,LIL_ETAB,ADR_ETAB,LIL_VIL,VAL_CPETAB,CO_PUPRETAB,"
                + "TYP_CNT,CO_DIS,NU_TELETAB,CO_ZBAS,CO_TYPETAB,CO_NAT,ID_CIO,VAL_MELETAB,FL_INT,'N','N',"
                + "DAD_ETABACA,DAF_ETABACA,DAC_ETABACA,DAM_ETABACA ";
        reqInsertionEtabManquant += " FROM AN_ETABACA "
                + " WHERE AN_ETABACA.ID_ETAB in ( SELECT AV_ELEVE.ID_ETAB FROM AV_ELEVE,AN_MEFSTAT,CN_INITELVMEFSTAT"
                + " WHERE AV_ELEVE.ID_ETAB NOT IN ( ";
        reqInsertionEtabManquant += " SELECT ID_ETAB FROM GN_ETABACA)"
                + " and AV_ELEVE.CO_MEFSTAT_ELV = AN_MEFSTAT.CO_MEFSTAT"
                + " and AN_MEFSTAT.CO_MEFSTAT4 =CN_INITELVMEFSTAT.CO_MEFSTAT4 )";

        DaoUtils.executeSQL(reqInsertionEtabManquant);
    }

    /**
     * Inserer les identifiants manquants dans la table CN_INITETABID.
     */
    @Override
    public void insertionIDEtabManquants() {
        String reqInsertIDEtabManquant = "INSERT INTO CN_INITETABID "
                + "SELECT DISTINCT AV_ELEVE.ID_ETAB, CURRENT DATE, CURRENT DATE "
                + "FROM AV_ELEVE,AN_MEFSTAT,CN_INITELVMEFSTAT WHERE AV_ELEVE.ID_ETAB NOT IN "
                + "( SELECT ID_ETAB FROM GN_ETABACA) and AV_ELEVE.ID_ETAB not in ( select ID_ETAB from CN_INITETABID)"
                + " and AV_ELEVE.CO_MEFSTAT_ELV = AN_MEFSTAT.CO_MEFSTAT"
                + " and AN_MEFSTAT.CO_MEFSTAT4=CN_INITELVMEFSTAT.CO_MEFSTAT4";
        DaoUtils.executeSQL(reqInsertIDEtabManquant);
    }

    @Override
    public List<Eleve> getDoublons(String nom, String prenom, Date dateNaissance, String idEtablissement) {
        // existe-il un �l�ve avec les m�mes nom, pr�nom et date de naissance ?
        List<Filtre> listFiltersDbl = new ArrayList<>();
        listFiltersDbl.add(Filtre.like("nom", nom));
        listFiltersDbl.add(Filtre.like("prenom", prenom + "%"));
        listFiltersDbl.add(Filtre.equal("dateNaissance", dateNaissance));
        if (StringUtils.isNotEmpty(idEtablissement)) {
            listFiltersDbl.add(Filtre.equal("etablissementNational.id", idEtablissement));
        }

        List<Tri> tris = new ArrayList<>();
        tris.add(Tri.asc("nom"));
        tris.add(Tri.asc("prenom"));
        tris.add(Tri.asc("prenom2"));
        tris.add(Tri.asc("prenom3"));

        return lister(listFiltersDbl, tris);
    }

    @Override
    public void tagguerDoublons() {

        // remise � Flag.NON du flag doublon
        DaoUtils.executeSQL("update GV_ELEVE set FL_DOUBLON = 'N'");
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // pr�paration de la requ�te de mise � jour des doublons
            Query qUpdateDoublon = session.createQuery(
                    "update Eleve e set e.flagDoublon = 'O', e.flagDoublonTraite = 'N' where e.ine = :ine");

            // on r�cup�re la liste de tous les �l�ves
            // qui ont des voeux
            Query qSelectEleves = session
                    .createQuery("select e.ine, e.nom, e.prenom, e.dateNaissance, e.flagDoublonTraite"
                            + " from Eleve e where exists (from VoeuEleve ve   where e.ine=ve.eleve.ine)");
            ScrollableResults srEleves = qSelectEleves.scroll(ScrollMode.FORWARD_ONLY);

            Map<TripletEleve, List<Object[]>> mEleves = new HashMap<>();

            while (srEleves.next()) {
                Object[] oEleve = srEleves.get();
                String nom = (String) oEleve[1];
                String prenom = (String) oEleve[2];
                Date dateNaissance = (Date) oEleve[3];

                // traitement des chaines de caract�res
                nom = ChaineUtils.suppressionAccent(nom);
                prenom = ChaineUtils.suppressionAccent(prenom);

                TripletEleve te = new TripletEleve(nom, prenom, dateNaissance);

                // on regarde si le triplet existe d�j�
                if (!mEleves.containsKey(te)) {
                    mEleves.put(te, new ArrayList<Object[]>(1));
                }

                // ajout de l'�l�ve en cours au triplet nom-prenom-date
                (mEleves.get(te)).add(oEleve);
            }
            srEleves.close();

            // traitement de la map (recherche des �l�ves en probabilit� de doublons)
            for (Iterator<List<Object[]>> itMap = mEleves.values().iterator(); itMap.hasNext();) {
                List<Object[]> lEleves = itMap.next();
                if (lEleves.size() == 1) {
                    // rien � faire
                    continue;
                }

                // on regarde si la liste des �l�ves en probabilit�s de doublons
                // pour ce triplet ont d�j� �t� trait�
                boolean tousTraites = true;
                for (Iterator<Object[]> itList = lEleves.iterator(); itList.hasNext();) {
                    Object[] oEleve = itList.next();
                    String flagDoublonTraite = (String) oEleve[4];
                    if (flagDoublonTraite.equals(Flag.NON)) {
                        tousTraites = false;
                    }
                }
                if (tousTraites) {
                    // rien � faire
                    continue;
                }

                // traitement des doublons
                for (Iterator<Object[]> itList = lEleves.iterator(); itList.hasNext();) {
                    Object[] oEleve = itList.next();
                    String ine = (String) oEleve[0];
                    qUpdateDoublon.setString("ine", ine);
                    DaoUtils.executeHibernateQuery(qUpdateDoublon);
                }
            }
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    /**
     * Cette classe sert pour la gestion des doublons. Elle contient le triplet
     * : nom - prenom - date de naissance.
     */
    private static class TripletEleve {

        /** Nom de l'�l�ve. */
        private String nom;

        /** Pr�nom de l'�l�ve. */
        private String prenom;

        /** Date de naissance. */
        private Date dateNaissance;

        /**
         * Cr�e un triplet pour la gestion de doublons.
         * 
         * @param nom
         *            nom de l'�l�ve
         * @param prenom
         *            pr�nom de l'�l�ve
         * @param dateNaissance
         *            date de naissance
         */
        public TripletEleve(String nom, String prenom, Date dateNaissance) {
            this.nom = nom;
            this.prenom = prenom;
            this.dateNaissance = dateNaissance;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(this.nom).append(this.prenom).append(this.dateNaissance)
                    .toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof TripletEleve)) {
                return false;
            }
            TripletEleve castOther = (TripletEleve) other;
            return new EqualsBuilder().append(castOther.nom, this.nom).append(castOther.prenom, this.prenom)
                    .append(castOther.dateNaissance, this.dateNaissance).isEquals();
        }
    }

    @Override
    public void traiterDoublon(Eleve eleve) {
        eleve.setFlagDoublon(Flag.NON);
        eleve.setFlagDoublonTraite(Flag.OUI);
        maj(eleve, eleve.getIne());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String[]> listerFormationOrigineSansParametres() {
        return (List<String[]>) DaoUtils
                .listNamedQuery("sql.select.audit.pam.controleFormationOrigine");
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Etablissement> listerEtablissementSansDistrictDesElevesPresentsEntite() {

        // r�cup�ration de la session
        Session session = getSession();

        Criteria critEtablissementSansDistrict = session.createCriteria(Etablissement.class, "etab");

        // Alias
        critEtablissementSansDistrict.createAlias("typeEtablissement", "typeEtablissement");

        // Ajout des restrictions
        // - Sans district
        critEtablissementSansDistrict.add(Restrictions.isNull("district"));

        // - Seulement pour ceux qui ont des �l�ves
        critEtablissementSansDistrict.add(Restrictions.sizeGt("eleves", 0));

        // trier la liste r�sultat par num�ro UAI �tablissement
        critEtablissementSansDistrict.addOrder(Order.asc("id"));

        return critEtablissementSansDistrict.list();
    }

    @Override
    public boolean existeEleveSQL(String ine) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            SQLQuery query = session.createSQLQuery("SELECT * FROM GV_ELEVE WHERE INE_ELV = :ine");
            query.setString("ine", ine);
            return query.uniqueResult() != null;
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public int suppressionMultipleParFormation(List<FormationPK> listFormationPKASupprimer) {

        int nbDel = 0;

        if (!listFormationPKASupprimer.isEmpty()) {

            StringBuffer requete = new StringBuffer();

            // Suppression dans la table GV_ELEVE
            requete.append("(select INE_ELV from GV_ELEVE where ");

            // Cr�ation de la partie de requ�te contenant la liste des formations � traiter
            boolean dejaUn = false;
            for (int i = 0; i < listFormationPKASupprimer.size(); i++) {

                if (dejaUn) {
                    requete.append(" or ");
                } else {
                    dejaUn = true;
                }

                FormationPK fPk = listFormationPKASupprimer.get(i);
                requete.append("(LIC_MEF='");
                requete.append(fPk.getMnemonique());
                requete.append("' and CO_SPEMEF='");
                requete.append(fPk.getCodeSpecialite());
                requete.append("')");

            }

            requete.append(")");

            // On ne supprime pas les �l�ves s�lectionn�s par mefStat4 et mn�monique
            requete.append("except (" + requeteIneElevesSelectionnesMefStatMnemonique() + ")");

            // On ne supprime pas les �l�ves s�lectionn�s par mefstat4
            requete.append("except (" + requeteIneElevesSelectionnesParMefstat4() + ")");

            // Execution de la requ�te
            String subSelectIneSupprimes = requete.toString();
            nbDel = suppressionEleveDeselectionnes(subSelectIneSupprimes);
        }
        return nbDel;
    }

    @Override
    public int suppressionMultipleParMefStat4(List<String> listMefStat4ASupprimer) {

        int nbDel = 0;

        if (!listMefStat4ASupprimer.isEmpty()) {

            StringBuffer requete = new StringBuffer();

            // Suppression dans la table GV_ELEVE des �l�ves originaires des MeSftats donn�s
            requete.append("(select GV_ELEVE.INE_ELV from GN_MEF, GV_ELEVE, AN_MEFSTAT "
                    + "where GN_MEF.LIC_MEF = GV_ELEVE.LIC_MEF and GN_MEF.CO_SPEMEF = GV_ELEVE.CO_SPEMEF "
                    + "and AN_MEFSTAT.CO_MEFSTAT = GN_MEF.CO_MEFSTAT and AN_MEFSTAT.CO_MEFSTAT4 in (");

            // Cr�ation de la liste pour la requ�te
            boolean dejaUn = false;
            for (int i = 0; i < listMefStat4ASupprimer.size(); i++) {
                if (dejaUn) {
                    requete.append(", ");
                } else {
                    dejaUn = true;
                }
                requete.append("'");
                requete.append(listMefStat4ASupprimer.get(i));
                requete.append("'");
            }

            requete.append(")) ");

            // On ne supprime pas les �l�ves s�lectionn�s par mefStat4 et mn�monique
            requete.append("except (" + requeteIneElevesSelectionnesMefStatMnemonique() + ")");

            // On ne supprime pas les �l�ves s�lectionn�s par formation
            requete.append("except (" + requeteIneElevesSelectionnesParFormation() + ")");

            // Execution de la requ�te
            String subSelectIneSupprimes = requete.toString();
            nbDel = suppressionEleveDeselectionnes(subSelectIneSupprimes);
        }

        return nbDel;
    }

    @Override
    public int suppressionMultipleParMnemonique(Map<String, List<String>> mnemoniquesASupprimer) {

        int nbDel = 0;

        if (mnemoniquesASupprimer.size() > 0) {

            StringBuffer requete = new StringBuffer();

            // Suppression dans la table GV_ELEVE des �l�ves originaires des Mnemoniques donn�s
            requete.append("(select GV_ELEVE.INE_ELV from GN_MEF, GV_ELEVE, AN_MEFSTAT "
                    + "where GN_MEF.LIC_MEF = GV_ELEVE.LIC_MEF and AN_MEFSTAT.CO_MEFSTAT = GN_MEF.CO_MEFSTAT "
                    + "and (");

            // Cr�ation de la liste pour la requ�te
            StringBuffer listeMnmoniques = new StringBuffer();
            for (Entry<String, List<String>> listePourCodeMefStat4 : mnemoniquesASupprimer.entrySet()) {
                for (String mnemonique : listePourCodeMefStat4.getValue()) {
                    if (listeMnmoniques.length() > 0) {
                        listeMnmoniques.append("or ");
                    }
                    listeMnmoniques
                            .append("AN_MEFSTAT.CO_MEFSTAT4 = '" + listePourCodeMefStat4.getKey() + "' and ");
                    listeMnmoniques.append("GV_ELEVE.LIC_MEF = '" + mnemonique + "' ");
                }
            }

            requete.append(listeMnmoniques);
            requete.append(")) ");

            // On ne supprime pas les �l�ves s�lectionn�s par mefstat4
            requete.append("except (" + requeteIneElevesSelectionnesParMefstat4() + ")");

            // On ne supprime pas les �l�ves s�lectionn�s par formation
            requete.append("except (" + requeteIneElevesSelectionnesParFormation() + ")");

            // Execution de la requ�te
            String subSelectIneSupprimes = requete.toString();
            nbDel = suppressionEleveDeselectionnes(subSelectIneSupprimes);
        }

        return nbDel;
    }

    /**
     * Supprime les �l�ves dont les INE sont donn�s par sous-requ�te.
     * Attention, cette m�thode est sp�cifique � la "s�lection des �l�ves".
     * 
     * @param subSelectIneSupprimes
     *            sous-requ�te donnant les INE des �l�ves � retirer.
     * @return nombre d'�l�ves supprim�s
     */
    private int suppressionEleveDeselectionnes(String subSelectIneSupprimes) {

        // Informations d'�valuation (utilis�es pour l'harmonisation)
        DaoUtils.executeSQL(
                "delete from GV_EVAL_CHAMP_DISCIPLINAIRE_OPA where INE_ELV in (" + subSelectIneSupprimes + ")");
        DaoUtils.executeSQL(
                "delete from GV_EVAL_COMPLEMENTAIRE_OPA where INE_ELV in (" + subSelectIneSupprimes + ")");

        DaoUtils.executeSQL("delete from GV_EVAL_SOCLE where INE_ELV in (" + subSelectIneSupprimes + ")");
        DaoUtils.executeSQL("delete from GV_EVAL_DISCIPLINE where INE_ELV in (" + subSelectIneSupprimes + ")");
        DaoUtils.executeSQL("delete from GV_EVAL_COMPLEMENTAIRE where INE_ELV in (" + subSelectIneSupprimes + ")");

        DaoUtils.executeSQL("delete from GV_NOTE_OPA where INE_ELV in (" + subSelectIneSupprimes + ")");

        DaoUtils.executeSQL(
                "delete from GV_DECISION_ORIENTATION_ELEVE where INE_ELV in (" + subSelectIneSupprimes + ")");

        DaoUtils.executeSQL("delete from GV_RESPONSABLE where INE_ELV in (" + subSelectIneSupprimes + ")");

        return DaoUtils.executeSQL("delete from GV_ELEVE where INE_ELV in (" + subSelectIneSupprimes + ")");
    }

    /** @return Requ�te listant les INE des �l�ves s�lectionn�s par mefStat4 et mn�monique. */
    private String requeteIneElevesSelectionnesMefStatMnemonique() {
        return "select GV_ELEVE.INE_ELV from GV_ELEVE, GN_MEF, AN_MEFSTAT "
                + "where GN_MEF.LIC_MEF = GV_ELEVE.LIC_MEF and AN_MEFSTAT.CO_MEFSTAT = GN_MEF.CO_MEFSTAT "
                + "and (AN_MEFSTAT.CO_MEFSTAT4, GV_ELEVE.LIC_MEF) in "
                + "(select CO_MEFSTAT4, LIC_MEF from CN_INIT_ELV_MNEMO)";
    }

    /** @return Requ�te listant les INE des �l�ves s�lectionn�s par MefStat4. */
    private String requeteIneElevesSelectionnesParMefstat4() {
        return "select GV_ELEVE.INE_ELV from GN_MEF, GV_ELEVE, AN_MEFSTAT "
                + "where GN_MEF.LIC_MEF = GV_ELEVE.LIC_MEF and GN_MEF.CO_SPEMEF = GV_ELEVE.CO_SPEMEF "
                + "and AN_MEFSTAT.CO_MEFSTAT = GN_MEF.CO_MEFSTAT "
                + "and AN_MEFSTAT.CO_MEFSTAT4 in (select CO_MEFSTAT4 from CN_INITELVMEFSTAT)";
    }

    /** @return Requ�te listant les INE des �l�ves s�lectionn�s par formation (mn�monique + sp�cialit�). */
    private String requeteIneElevesSelectionnesParFormation() {
        return "select GV_ELEVE.INE_ELV from GV_ELEVE, GN_MEF "
                + "where GN_MEF.LIC_MEF = GV_ELEVE.LIC_MEF and GN_MEF.CO_SPEMEF = GV_ELEVE.CO_SPEMEF "
                + "and (GV_ELEVE.LIC_MEF, GV_ELEVE.CO_SPEMEF) in "
                + "(select LIC_MEF, CO_SPEMEF from CN_INITELVMEF)";
    }

    /**
     * Filtre sur les �l�ves ayant formul� des voeux pour le tour courant.
     * 
     * @return filtre sur la pr�sence de voeux
     */
    @Override
    public Filtre getFiltreEleveAvecVoeux() {
        return Filtre.hql("(select count(*) from VoeuEleve voeu where voeu.id.ine = o.ine "
                + "and voeu.id.numeroTour = '" + campagneAffectationManager.getNumeroTourCourant() + "') > 0");
    }

    /**
     * Cr�e un filtre donnant les �l�ves qui n'ont pas que des voeux de recencement.
     * 
     * @return filtre sur les �l�ves
     */
    @Override
    public Filtre getFiltreExistenceVoeuAutreQueRecencement() {

        StringBuffer sbFiltreHQL = new StringBuffer();
        sbFiltreHQL.append("exists (select 1 from Voeu v");
        sbFiltreHQL.append(" where v.code in ");
        sbFiltreHQL.append("(select ve3.voeu.code from VoeuEleve ve3 ");
        sbFiltreHQL.append("where ve3.id.ine = o.ine)");
        sbFiltreHQL.append(" and v.flagRecensement='N')");
        return Filtre.hql(sbFiltreHQL.toString());
    }

    /**
     * Cr�e un filtre donnant les �l�ves sans proposition d'accueil.
     * 
     * @return filtre sur l'absence de voeux de propositions d'accueil
     */
    @Override
    public Filtre getFiltreElevesSansPropositionAccueil() {

        StringBuffer sbFiltreHQL = new StringBuffer();
        sbFiltreHQL.append("o.ine not in (select distinct ve2.id.ine");
        sbFiltreHQL.append(" from VoeuEleve ve2");
        sbFiltreHQL.append(" where ve2.flagPropositionAccueil='O')");
        return Filtre.hql(sbFiltreHQL.toString());
    }

    @Override
    public VoeuEleve getPropositionAccueil(Eleve eleve) {

        // liste de ses voeux
        List<VoeuEleve> listeVoeux = eleve.voeuxDuTour(campagneAffectationManager.getNumeroTourCourant());

        for (int j = 0; j < listeVoeux.size(); j++) {
            VoeuEleve ve = listeVoeux.get(j);
            if (ve.getFlagPropositionAccueil().equals(Flag.OUI)) {
                return ve;
            }
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> getClasses(EtablissementNational etablissementNational) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ves appartenant � l'�tablissement
            // TODO v�rifier si on doit encore ajouter le flag �l�ve hors aca
            return session
                    .createQuery("select distinct e.classe from Eleve e where e.etablissementNational.id = '"
                            + etablissementNational.getId() + "' order by 1")
                    .list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Formation> getFormations(EtablissementNational etablissementNational) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ves appartenant � l'�tablissement
            return session
                    .createQuery("select distinct e.formation from Eleve e where e.etablissementNational.id = '"
                            + etablissementNational.getId() + "' order by 1")
                    .list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public ScrollableResults scrollElevePalier3emePourEtab(String idEtablissement) {
        // r�cup�ration de la session
        Session session = getSession();

        Criteria c = session.createCriteria(Eleve.class, "elv");
        c.createAlias("elv.palierOrigine", "palier");
        c.createAlias("elv.etablissement", "etablissement");
        c.add(Restrictions.eq("etablissement.id", idEtablissement));
        c.add(Restrictions.eq("palier.code", Palier.CODE_PALIER_3EME));

        return c.scroll();
    }

    @Override
    public int majNoteMoyenne(Eleve eleve) {
        try {
            Session session = getSession();
            Query q = session.getNamedQuery("hql.eleve.update.notemoyenne");

            q.setDouble("noteMoyenne", eleve.getNoteMoyenne());
            q.setDate("dateMAJ", new Date());
            q.setString("ine", eleve.getIne());

            return q.executeUpdate();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void reinitialiserStatutZoneGeoPalier3eme() {
        try {
            getSession().getNamedQuery("eleve.statutZoneGeo.reinitialiser.update.hql").executeUpdate();
        } catch (HibernateException e) {
            LOG.error("Une erreur s'est produite lors de l'execution de la requete", e);
            throw new DataAccessException(e);
        }
    }

    @Override
    public void specifierZoneGeoElevesMemeCommuneTroncon(TronconCarteScolaire tronconCarteScolaire) {
        try {
            getSession()
                    .getNamedQuery("zoneGeo.determination.zoneGeoUnique.update.hql")
                    .setParameter("zoneGeo", tronconCarteScolaire.getZoneGeo())
                    .setParameter("commune", tronconCarteScolaire.getCommune()).executeUpdate();
        } catch (HibernateException e) {
            LOG.error("Une erreur s'est produite lors de l'execution de la requete nommee", e);
            throw new DataAccessException(e);
        }
    }

    @Override
    public Collecteur<Eleve> scrollEleves3emeAcaAdresseRedressee() {
        try {
            Filtre filtreDejaTraites = Filtre.in("statutZoneGeo",
                    Arrays.asList(StatutZoneGeoEnum.NON_TRAITE, StatutZoneGeoEnum.MANUEL));
            Filtre filtrePalierOrigine = Filtre.equal("palierOrigine.code", Palier.CODE_PALIER_3EME);
            Filtre filtreAca = Filtre.equal("flagHorsAcademie", Flag.NON);
            Filtre filtreStatutAdresse = Filtre.in("adresseResidence.statutAdresse", Arrays.asList(
                    StatutAdresseEnum.AF, StatutAdresseEnum.VM, StatutAdresseEnum.VA, StatutAdresseEnum.VR));

            Set<String> fetchJoin = new HashSet<>();
            fetchJoin.add("zoneGeo");
            fetchJoin.add("zoneGeo.liensZoneGeo.voeu.opaTourPrincipal");

            return new CollecteurScrollableResultSet<>(scroll(
                    Arrays.asList(Filtre
                            .and(new Filtre[] { filtreDejaTraites, filtrePalierOrigine, filtreAca,
                                    filtreStatutAdresse })),
                    null, null, fetchJoin, null, false));
        } catch (HibernateException e) {
            LOG.error("Une erreur s'est produite lors de l'execution de la requete nommee", e);
            throw new DataAccessException(e);
        }
    }

    @Override
    public int nombreElevesPalier3emeParStatutZoneGeo(StatutZoneGeoEnum statutZoneGeo) {
        Filtre filtrePalierOrigine = Filtre.equal("palierOrigine.code", Palier.CODE_PALIER_3EME);
        Filtre filtreStatutZoneGeo = Filtre.equal("statutZoneGeo", statutZoneGeo);
        Filtre filtreAca = Filtre.equal("flagHorsAcademie", Flag.NON);

        return getNombreElements(Filtre.and(Arrays.asList(filtrePalierOrigine, filtreStatutZoneGeo, filtreAca)));
    }

    @Override
    public int nombreElevesAdresseInvalide() {
        Filtre filtrePalierOrigine = Filtre.equal("palierOrigine.code", Palier.CODE_PALIER_3EME);
        Filtre filtreAca = Filtre.equal("flagHorsAcademie", Flag.NON);
        Filtre filtreStatutAdresse = Filtre.not(Filtre.in("adresseResidence.statutAdresse", Arrays
                .asList(StatutAdresseEnum.AF, StatutAdresseEnum.VM, StatutAdresseEnum.VA, StatutAdresseEnum.VR)));

        return getNombreElements(Filtre.and(Arrays.asList(filtrePalierOrigine, filtreStatutAdresse, filtreAca)));
    }

    @Override
    public List<Eleve> listerElevesPalier3eme(Filtre filtre, List<Tri> tris) {
        Filtre filtrePalierOrigine = Filtre.equal("palierOrigine.code", Palier.CODE_PALIER_3EME);

        Filtre filtres = Filtre.and(Arrays.asList(filtrePalierOrigine, filtre));

        return lister(filtres, tris);
    }

    @Override
    public List<Eleve> listerElevesSansZoneGeoDetermineeAuto() {
        List<Tri> tris = new ArrayList<>();

        tris.add(Tri.asc("etablissement.id"));
        tris.add(Tri.asc("nom"));
        tris.add(Tri.asc("adresseResidence.codePostal.commune.code"));

        Filtre statutZoneGeoFiltre = Filtre.in("statutZoneGeo",
                Arrays.asList(StatutZoneGeoEnum.MANUEL, StatutZoneGeoEnum.NON_ABOUTI));
        Filtre filtreAca = Filtre.equal("flagHorsAcademie", Flag.NON);

        return listerElevesPalier3eme(Filtre.and(statutZoneGeoFiltre, filtreAca), tris);
    }
}
