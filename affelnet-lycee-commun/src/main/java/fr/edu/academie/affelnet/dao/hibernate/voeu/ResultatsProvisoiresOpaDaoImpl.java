/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.voeu;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.batch.planificationCampagneAffectation.CompteurOpa;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.voeu.ResultatsProvisoiresOpaDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;

/**
 * Implementation de la gestion des r�sultats provisoires d'OPA.
 */
@Repository("ResultatsProvisoiresOpaDao")
public class ResultatsProvisoiresOpaDaoImpl extends BaseDaoHibernate<ResultatsProvisoiresOpa, VoeuElevePK>
        implements ResultatsProvisoiresOpaDao {

    /** Liste des tris par defaut. */
    protected static final List<Tri> DEFAULT_ORDERS = new ArrayList<Tri>();

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(ResultatsProvisoiresOpaDaoImpl.class);

    static {
        DEFAULT_ORDERS.add(Tri.asc("id.numeroTour"));
        DEFAULT_ORDERS.add(Tri.asc("voeuEleve.eleve.nom"));
        DEFAULT_ORDERS.add(Tri.asc("voeuEleve.eleve.prenom"));
        DEFAULT_ORDERS.add(Tri.asc("id.ine"));
        DEFAULT_ORDERS.add(Tri.asc("opa.id"));
        DEFAULT_ORDERS.add(Tri.asc("id.rang"));
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(VoeuElevePK key) {
        return "Le r�sultat provisoire d'OPA " + key.toPrettyString() + " n'existe pas";
    }

    @Override
    protected void setKey(ResultatsProvisoiresOpa object, VoeuElevePK key) {
        object.setId(key);
    }

    @Override
    protected boolean hasKeyChange(ResultatsProvisoiresOpa object, VoeuElevePK oldKey) {
        return !object.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec ce code";
    }

    @Override
    protected Class<ResultatsProvisoiresOpa> getObjectClass() {
        return ResultatsProvisoiresOpa.class;
    }

    @Override
    public CompteurOpa calculerCompteurs(long idOpa) {

        Session session = getSession();
        CompteurOpa compteur = new CompteurOpa();

        // Nombre d'�l�ves (nombre d'INE distincts)
        Criteria critereNbEleves = session.createCriteria(ResultatsProvisoiresOpa.class);
        critereNbEleves.add(Restrictions.eq("opa.id", idOpa));
        critereNbEleves.setProjection(Projections.countDistinct("id.ine"));
        Integer nbEleves = ((Number) critereNbEleves.uniqueResult()).intValue();
        compteur.setNombreEleves(nbEleves.intValue());

        // Nombre de voeux �l�ves
        Criteria critereNbVoeux = session.createCriteria(ResultatsProvisoiresOpa.class);
        critereNbVoeux.add(Restrictions.eq("opa.id", idOpa));
        critereNbVoeux.setProjection(Projections.rowCount());
        Integer nbVoeux = ((Number) critereNbVoeux.uniqueResult()).intValue();
        compteur.setNombreVoeuxEleve(nbVoeux.intValue());

        // Nombre de voeux d'�l�ves s�curis�s (sur la base du bonus s�curisation)
        Criteria critereNbVoeuxSecurises = session.createCriteria(ResultatsProvisoiresOpa.class);
        critereNbVoeuxSecurises.add(Restrictions.eq("opa.id", idOpa));
        critereNbVoeuxSecurises.add(Restrictions.eq("flagSecurisation", Flag.OUI));
        critereNbVoeuxSecurises.setProjection(Projections.rowCount());
        Integer nbVoeuxSecurises = ((Number) critereNbVoeuxSecurises.uniqueResult()).intValue();
        compteur.setNombreVoeuxSecurises(nbVoeuxSecurises.intValue());

        return compteur;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OperationProgrammeeAffectation> listerOpaSecuriseesExecutees(String idEtablissement,
            short numeroTour) {

        Session session = getSession();

        Query q = session.getNamedQuery("listerOpaResultatsProvisoiresNonAffectesSecurisation.select.hql");
        q.setShort("numeroTour", numeroTour);
        q.setParameter("idEtablissement", idEtablissement);

        return q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ResultatsProvisoiresOpa> listerResultatsProvisoiresNonAffectesSecurisation(String idEtablissement,
            short numeroTour) {

        Session session = getSession();

        Query q = session.getNamedQuery("listerResultatsProvisoiresNonAffectesSecurisation.select.hql");
        q.setShort("numeroTour", numeroTour);
        q.setParameter("idEtablissement", idEtablissement);

        return q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Eleve> listerElevesNonAffectesProvisoireSecu(Long idOpa, String idEtablissement) {

        Session session = getSession();

        Query q = session.getNamedQuery("listerElevesNonAffectesProvisoireSecu.select.hql");
        q.setParameter("idOpa", idOpa);
        q.setParameter("idEtablissement", idEtablissement);

        return q.list();
    }

    @Override
    public void supprimerResultatsProvisoiresOffresNonSelectionnees(Long idOpa) {

        Session session = getSession();
        Query q = session.getNamedQuery("nettoyerResultatsProvisoiresOffresDelectionnees.delete.hql");
        q.setParameter("idOpa", idOpa);
        int nbResultatsSupprimes = q.executeUpdate();

        LOG.debug("R�sultats supprim�s : " + nbResultatsSupprimes);

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> listerIdOpaPourBonusAvis(short numeroTour, Avis avis) {

        // On examine les r�sultats provisoires du tour non forc�s
        Criteria criteriaResultatPourTypeAvis = getCriteriaResultatsTourDuNonForcesAvecBareme(numeroTour);

        // On s'appuie sur les m�ta-informations des avis pour constituer les filtre
        CodeTypeAvis codeTypeAvis = avis.getId().getType().decode();
        String nomAttributAvisVoeu = codeTypeAvis.getNomAttributAvisVoeu();

        // On filtre sur les avis positionn�s sur les voeux qui correspondent � celui indiqu�
        criteriaResultatPourTypeAvis.add(Restrictions.eq("ve." + nomAttributAvisVoeu, avis));

        // On s'int�ressera � l'identifiant d'OPA distinct
        criteriaResultatPourTypeAvis.setProjection(Projections.distinct(Projections.property("opa.id")));

        return criteriaResultatPourTypeAvis.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> listerIdOpaPourLienZoneGeo(short numeroTour, LienZoneGeo lienZoneGeo) {

        Session session = getSession();
        Criteria criteria = session.createCriteria(ResultatsProvisoiresOpa.class);

        // Pour les voeux et candidatures non forc�s
        criteria.createAlias("voeuEleve", "ve");
        criteria.createAlias("ve.candidature", "cand");
        criteria.add(Restrictions.eq("ve.flagRefusVoeu", Flag.NON));
        criteria.add(Restrictions.eq("cand.flagEleveForce", Flag.NON));

        // Pour le tour donn�
        criteria.add(Restrictions.eq("id.numeroTour", numeroTour));

        // Filtre sur la zone g�ographique (si elle est valu�e), sinon tous les r�sultats sont concern�s
        ZoneGeo zoneGeo = lienZoneGeo.getZoneGeoOrigine();
        if (zoneGeo != null) {

            // La zone g�ographique est pr�cis�e, on se restreint aux r�sultats des �l�ves concern�s
            criteria.createAlias("ve.eleve", "elv");
            criteria.createAlias("elv.zoneGeo", "zone");
            criteria.add(Restrictions.eq("zone.code", zoneGeo.getCode()));
        }

        // L'offre de formation est la cible du lien zone g�o et trait�e par bar�me
        Voeu offreFormationCible = lienZoneGeo.getVoeu();
        criteria.createAlias("ve.voeu", "offre");
        criteria.add(Restrictions.eq("offre.code", offreFormationCible.getCode()));
        criteria.add(Restrictions.ne("offre.indicateurPam", TypeOffre.COMMISSION.getIndicateur()));

        // On s'int�ressera � l'identifiant d'OPA distinct
        criteria.setProjection(Projections.distinct(Projections.property("opa.id")));

        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> listerIdOpaPourParamefa(short numeroTour, ParametresFormationAccueil paramefa) {

        Session session = getSession();

        // On liste les id par query SQL (limites de l'ORM sur les jointures sur les piles / tour)
        Query queryIdOpaPourParamefa = session.getNamedQuery("listerIdentifiantsOpaPourParamefa.select.sql");
        queryIdOpaPourParamefa.setParameter("numeroTour", numeroTour);
        queryIdOpaPourParamefa.setParameter("idParamefa", paramefa.getId());

        return queryIdOpaPourParamefa.list();
    }

    /**
     * Constitue un criteria Hibernate pour obtenir les r�sultats du tour sur des voeux non forc�s, sur des offres
     * avec bar�me.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @return le crit�ria Hibernate
     */
    private Criteria getCriteriaResultatsTourDuNonForcesAvecBareme(short numeroTour) {
        Session session = getSession();
        Criteria critereResultat = session.createCriteria(ResultatsProvisoiresOpa.class);

        // Pour les voeux et candidatures non forc�s
        critereResultat.createAlias("voeuEleve", "ve");
        critereResultat.createAlias("ve.candidature", "cand");
        critereResultat.add(Restrictions.eq("ve.flagRefusVoeu", Flag.NON));
        critereResultat.add(Restrictions.eq("cand.flagEleveForce", Flag.NON));

        // L'offre de formation est trait�e par bar�me
        critereResultat.createAlias("ve.voeu", "offre");
        critereResultat.add(Restrictions.ne("offre.indicateurPam", TypeOffre.COMMISSION.getIndicateur()));

        // Pour le tour donn�
        critereResultat.add(Restrictions.eq("id.numeroTour", numeroTour));
        return critereResultat;
    }
}
