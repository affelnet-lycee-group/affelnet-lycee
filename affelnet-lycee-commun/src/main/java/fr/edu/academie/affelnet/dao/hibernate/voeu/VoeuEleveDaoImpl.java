/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.hibernate.voeu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.BaseDaoHibernate;
import fr.edu.academie.affelnet.dao.hibernate.utils.CollecteurScrollableResultSet;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.utils.Collecteur;


/**
 * Implementation de la gestion des voeux des �l�ves.
 */
@Repository("VoeuEleveDao")
public class VoeuEleveDaoImpl extends BaseDaoHibernate<VoeuEleve, VoeuElevePK> implements VoeuEleveDao {

    /** liste des ordres par defaut. */
    private static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();

    /**
     * Le manager pour les campagnes.
     */
    private CampagneAffectationManager campagneManager;

    static {
        DEFAULT_ORDERS.add(Tri.asc("eleve.nom"));
        DEFAULT_ORDERS.add(Tri.asc("eleve.prenom"));
        DEFAULT_ORDERS.add(Tri.asc("eleve.prenom2"));
        DEFAULT_ORDERS.add(Tri.asc("eleve.prenom3"));
        DEFAULT_ORDERS.add(Tri.asc("id.ine"));
        DEFAULT_ORDERS.add(Tri.asc("id.rang"));
    }

    /**
     * @param campagneManager
     *            the campagneManager to set
     */
    @Autowired
    public void setCampagneManager(CampagneAffectationManager campagneManager) {
        this.campagneManager = campagneManager;
    }

    @Override
    protected List<Tri> getDefaultOrders() {
        return DEFAULT_ORDERS;
    }

    @Override
    protected String getMessageObjectNotFound(VoeuElevePK id) {
        return "Le voeuEleve " + id.getIne() + ":" + id.getRang() + " n'existe pas";
    }

    @Override
    protected void setKey(VoeuEleve voeuEleve, VoeuElevePK key) {
        voeuEleve.setId(key);
    }

    @Override
    protected boolean hasKeyChange(VoeuEleve voeuEleve, VoeuElevePK oldKey) {
        return !voeuEleve.getId().equals(oldKey);
    }

    @Override
    protected String getMessageDuplicateKey() {
        return "Il existe d�j� un enregistrement avec cette cl�";
    }

    @Override
    protected Class<VoeuEleve> getObjectClass() {
        return VoeuEleve.class;
    }

    @Override
    public VoeuEleve creer(VoeuEleve voeuEleve) {
        voeuEleve.getCandidature().getVoeux().add(voeuEleve);
        return voeuEleve;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Double> getListLSupp(String codeVoeu, Set<VoeuElevePK> ensembleEleveAIgnorer) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select ve.numeroListeSuppProvisoire from VoeuEleve ve ");
            requete.append("where not(ve.numeroListeSuppProvisoire is null) ");
            Iterator<VoeuElevePK> it = ensembleEleveAIgnorer.iterator();
            while (it.hasNext()) {
                requete.append("and ve.id.ine!='");
                requete.append((it.next()).getIne());
                requete.append("' ");
            }
            requete.append("and ve.voeu.code='");
            requete.append(codeVoeu);
            requete.append("' and ve.codeDecisionProvisoire='C' ");

            return session.createQuery(requete.toString()).list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Voeu> getListeVoeux(Etablissement etablissement) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select distinct ve.voeu from VoeuEleve ve ");
            requete.append("where ve.candidature.flagSaisieValide = 'O' ");
            requete.append("and ve.eleve.etablissement.id='" + etablissement.getId() + "' ");
            requete.append("and ve.id.numeroTour = ");
            requete.append(campagneManager.getNumeroTourCourant());
            requete.append("order by ve.voeu.etablissement.id");
            return session.createQuery(requete.toString()).list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Etablissement> getListeEtablissementAccueil(Etablissement etablissementOrigine) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("from Etablissement e where e.id in ");
            requete.append("(select distinct ve.voeu.etablissement.id from VoeuEleve ve ");
            requete.append("where ve.eleve.etablissement.id='");
            requete.append(etablissementOrigine.getId());
            requete.append("') ");
            requete.append("order by e.id");
            return session.createQuery(requete.toString()).list();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public long getNbSelections(Voeu voeu, Etablissement etablissementOrigine) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select count(*) from VoeuEleve ve ");
            requete.append("where ve.candidature.flagSaisieValide = 'O' ");
            requete.append("and ve.eleve.etablissement.id = '" + etablissementOrigine.getId() + "' ");
            requete.append("and ve.voeu.code='" + voeu.getCode() + "'");
            return ((Number) session.createQuery(requete.toString()).iterate().next()).longValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public long getNbSelectionsRang1(Voeu voeu, Etablissement etablissementOrigine) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select count(*) from VoeuEleve ve ");
            requete.append("where ve.candidature.flagSaisieValide = 'O' ");
            requete.append("and ve.eleve.etablissement.id = '" + etablissementOrigine.getId() + "' ");
            requete.append("and ve.id.rang = 1 ");
            requete.append("and ve.voeu.code='" + voeu.getCode() + "'");
            return ((Number) session.createQuery(requete.toString()).iterate().next()).longValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public long getNbVoeux(String ine) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select count(*) from VoeuEleve ve ");
            requete.append("where ve.id.ine = '" + ine + "' ");
            return ((Number) session.createQuery(requete.toString()).iterate().next()).longValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public short nombreVoeuxTourCourant(String ine) {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select count(*) from VoeuEleve ve ");
            requete.append("where ve.id.ine = '" + ine + "' and ve.id.numeroTour = ");
            requete.append(campagneManager.getNumeroTourCourant());
            return ((Number) session.createQuery(requete.toString()).iterate().next()).shortValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean isAvisSaisi(Avis avis) {
        try {
            // r�cup�ration de la session
            Session session = getSession();
            Iterator<Number> it = session.createQuery("select count(*) from VoeuEleve ve "
                    + "where (ve.avisEntretien.id.code = '" + avis.getId().getCode()
                    + "' and ve.avisEntretien.id.type.code = '" + avis.getId().getType().getCode()
                    + "') or (ve.avisConseilClasse.id.code = '" + avis.getId().getCode()
                    + "' and ve.avisConseilClasse.id.type.code = '" + avis.getId().getType().getCode() + "')")
                    .iterate();
            long i = it.next().longValue();
            return (i > 0);
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public List<Voeu> listerVoeuxCapaciteNulle(List<Filtre> filtresSupplementaire) {
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }

        filtres.add(Filtre.equal("voeu.pile.capaciteAffectation", 0));
        filtres.add(Filtre.equal("voeu.flagRecensement", Flag.NON));
        filtres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));
        filtres.add(Filtre.equal("flagRefusVoeu", Flag.NON));
        filtres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));

        List<String> fetchList = new ArrayList<>();

        // Permet d'�viter une LazyInitialisationException dans les audits
        fetchList.add("voeu.etablissement.typeEtablissement.libelleCourt");

        List<VoeuEleve> voeuxEleve = lister(filtres, null, null, null, fetchList, true);

        List<Voeu> voeux = new ArrayList<>();
        for (VoeuEleve voeu : voeuxEleve) {

            if (!voeux.contains(voeu.getVoeu())) {
                voeux.add(voeu.getVoeu());
            }
        }

        return voeux;
    }

    @Override
    public long getNbVoeuxValides() {
        try {
            // r�cup�ration de la session
            Session session = getSession();

            // recherche des �l�ments voulus
            StringBuilder requete = new StringBuilder();
            requete.append("select count(*) from VoeuEleve ve where ve.candidature.flagSaisieValide = 'O' ");
            return ((Number) session.createQuery(requete.toString()).iterate().next()).longValue();
        } catch (HibernateException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> listerIdEtablissementsAffectationUtilises() {

        Session sess = getSession();

        String requeteHql = "select distinct ve.voeu.etablissement.id "
                + " from fr.edu.academie.affelnet.domain.voeu.VoeuEleve ve " + " where ve.voeu.flagRecensement = '"
                + Flag.NON + "' and ve.voeu.etablissement.codeInterne = :codeInterne ";
        Query hqlQuery = sess.createQuery(requeteHql);
        hqlQuery.setParameter("codeInterne", Etablissement.CODE_INTERNE_AFFECTATION);
        return hqlQuery.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Eleve> listerElevesIncoherenceNbVoeuxRangMax() {
        Session session = getSession();
        Query requete = session.getNamedQuery("sql.select.audit.pam.listerElevesIncoherenceNbVoeuxRangMax");
        requete.setParameter("numTourCourant", campagneManager.getNumeroTourCourant());

        return requete.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Eleve> listerElevesIncoherenceNbVoeuxRangMax(String idOpa) {
        Session session = getSession();
        Query requete = session.getNamedQuery("sql.select.audit.pam.listerElevesIncoherenceNbVoeuxRangMaxPourOPA");
        requete.setParameter("idOpa", idOpa);
        requete.setParameter("numTourCourant", campagneManager.getNumeroTourCourant());

        return requete.list();
    }

    @Override
    public Collecteur<VoeuEleve> collecterVoeuxEleves(Filtre filtre, List<String> listeFectJoin) {
        ScrollableResults scroll = scroll(Arrays.asList(filtre), null, null, null, listeFectJoin, false);

        return new CollecteurScrollableResultSet<>(scroll);
    }

    @Override
    public Filtre filtreElevePossedeVoeuAvecEvalNoteTourCourantPourOpa(String qualifierIne, long idOpa) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.isNotNull("voeu.code"));
        // V�rification que l'ordre de formation prend en compte les notes/evaluations
        filtres.add(Filtre.equal("voeu.indicateurPam", TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur()));
        // V�rification que le voeu n'est pas forc� � non
        filtres.add(Filtre.equal("flagRefusVoeu", Flag.NON));
        // On se restreit au tour courant
        filtres.add(campagneManager.filtreTourCourant("id.numeroTour"));
        // Lien avec l'INE de la requ�te p�re
        filtres.add(Filtre.eqPropertiesPere("id.ine", qualifierIne));
        // V�rification que l'offre de formation appartient � l'OPA
        Filtre filtreOFdansOpa = Filtre.equal("voeu.operationProgrammeeAffectations.id", idOpa);
        // Ou que c'est un tour suivant
        Filtre filtreEstToursuivant = Filtre.notEqual("id.numeroTour",
                CampagneAffectationManager.NUMERO_TOUR_PRINCIPAL);
        filtres.add(Filtre.or(filtreOFdansOpa, filtreEstToursuivant));
        return Filtre.exists(VoeuEleve.class, Filtre.and(filtres));
    }

    @Override
    public Integer getNombreDecisionsFinalesManquantes(short numeroTour) {

        Session session = getSession();
        Criteria criteria = session.createCriteria(VoeuEleve.class);
        criteria.add(Restrictions.eq("id.numeroTour", numeroTour));
        criteria.add(Restrictions.isNull("codeDecisionFinal"));
        criteria.setProjection(Projections.rowCount());

        return ((Number) criteria.uniqueResult()).intValue();
    }

    @Override
    public void supprimerVoeuxEleves(int tour, int palier) {

        Session session = getSession();
        Query q = session.getNamedQuery("supprimerVoeuxEleves.delete.hql");
        q.setParameter("tour", tour);
        q.setParameter("palier", palier);
        q.executeUpdate();
    }

    @Override
    public void supprimerCandidaturesEleves(int tour, int palier) {

        Session session = getSession();
        Query q = session.getNamedQuery("supprimerCandidaturesEleves.delete.hql");
        q.setParameter("tour", tour);
        q.setParameter("palier", palier);
        q.executeUpdate();
    }

    @Override
    public void supprimerDerogationsEleves(int tour, int palier) {

        Session session = getSession();
        Query q = session.getNamedQuery("supprimerDerogationsEleves.delete.hql");
        q.setParameter("tour", tour);
        q.setParameter("palier", palier);
        q.executeUpdate();
    }

    @Override
    public void supprimerResultatsProvisoiresOpaEleves(int tour, int palier) {

        Session session = getSession();
        Query q = session.getNamedQuery("supprimerResultatsProvisoiresOpaEleves.delete.hql");
        q.setParameter("tour", tour);
        q.setParameter("palier", palier);
        q.executeUpdate();
    }

    @Override
    public List<String> getCodesVoeuxAffectesDansEtablisementHorsAcademie(){
        Session session = getSession();
        Query queryVoeux = session.getNamedQuery("voeux.etablissement.hors.aca.select.sql");
        return queryVoeux.list();
    }

    @Override
    public List<String> getElevesAvecVoeuxAffectesEtablisementPublicHorsAcademie(){
        Session session = getSession();
        Query queryVoeux = session.getNamedQuery("eleves.avec.voeux.etablissement.public.hors.aca.select.sql");
        return queryVoeux.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isSaisieVoeuApprentissageDemarre() {
        Session session = getSession();

        Query selectVoeuAP = session.getNamedQuery("voeu.select.StatutApprentissage.sql");
        List<VoeuEleve> result = selectVoeuAP.list();
        return !result.isEmpty();
    }
}
