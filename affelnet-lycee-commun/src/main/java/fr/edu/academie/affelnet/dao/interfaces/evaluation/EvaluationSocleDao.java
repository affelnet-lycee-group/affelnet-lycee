/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.evaluation;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSoclePK;

/**
 * Dao pour les �valuations des comp�tences du socle.
 */
public interface EvaluationSocleDao extends BaseDao<EvaluationSocle, EvaluationSoclePK> {

    /**
     * Liste les �valuations d'un �l�ves.
     * 
     * @param ine
     *            l'identifiant de l'�l�ve
     * @return la liste des �valuations du socle
     */
    List<EvaluationSocle> listerParEleve(String ine);

    /**
     * Supprime l'ensemble des �valuations du socle de l'�l�ve.
     * 
     * @param ine
     *            id de l'�l�ve
     */
    void purger(String ine);

    /**
     * Fournit le filtre pour r�cup�rer la liste des �l�ves sans �valuation du socle.
     * 
     * @return le filtre pour les �l�ves sans �valuation du socle
     */
    Filtre filtreElevesSansEvalSocle();
}
