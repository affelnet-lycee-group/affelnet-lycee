/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.evaluation.lsu;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CorrespondanceEvaluation;

/**
 * Interface du DAO pour les correspondances entre les �valuations des champs disciplinaires
 * et les groupes de niveau.
 */
public interface CorrespondanceEvaluationDao extends BaseDao<CorrespondanceEvaluation, Long> {
    /**
     * Charge depuis la base de donn�e la correspondance.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @param valEval
     *            evaluation
     * @return la correspondance
     */
    CorrespondanceEvaluation charger(String idEtablissement, String valEval);

    /**
     * R�cup�re l'ensemble des �valuations ne correspondant pas � une note sur 20 d'un �tablissement.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return une liste contenant toutes les �valuations des correspondances de l'�tablissement
     */
    List<String> chargerEvalParEtab(String idEtablissement);

    /**
     * R�cup�re l'ensemble des correspondances d'un �tablissement.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return la liste des correspondances de l'�tablissement
     */
    List<CorrespondanceEvaluation> chargerCorrespondanceParEtab(String idEtablissement);

    /**
     * D�termine si une �valuation similaire � la casse pr�s existe d�j� en base de donn�es.
     * 
     * @param valEval
     *            valeur de l'�valuation
     * @param idEtablissement
     *            id de l'�tablissement
     * @return vrai si une telle �valuation existe
     */
    boolean existe(String idEtablissement, String valEval);

    /**
     * Renvoie le nombre de correspondance non r�solue pour un �tablissement.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return le nombre de correspondance non r�solu�
     */
    int compteCorresNonResolue(String idEtablissement);
}
