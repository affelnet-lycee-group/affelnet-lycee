/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.evaluation.lsu;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.evaluation.lsu.DemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationEleveLSU;

/**
 * Dao pour la gestion des demandes d'�valuation LSU.
 */
public interface DemandeEvaluationDao extends BaseDao<DemandeEvaluation, String> {

    /**
     * Fournit la liste des �l�ves non int�gr�s pour l'�tablissement donn�.
     * 
     * @param idEtablissement
     *            id de l'�tablissement concern�
     * @return les �l�ves non int�gr�s (vide si int�gration n'a pas eu lieu)
     */
    List<EvaluationEleveLSU> listerEleveNonIntegre(String idEtablissement);

    /**
     * Fournit le nombre d'�l�ve dont les �valuations ont �t� int�gr�s via la demande aupr�s de LSU.
     * 
     * @param idEtablissement
     *            id de l'�tablissement concern�
     * @return le nombre d'�l�ve dont les �valuations ont �t� int�gr�es.
     */
    int compterEleveIntegre(String idEtablissement);

    /**
     * R�cup�re depuis la table d'accueil l'�l�ve LSU correspondant � l'INE.
     * 
     * @param ine
     *            INE de l'�l�ve
     * @param idEtablissement
     *            Id de '�tablissement de l'�l�ve
     * @return la liste des �l�ves LSU pr�sents dans la table d'accueil
     */
    List<EvaluationEleveLSU> chargerEleveLsu(String ine, String idEtablissement);

}
