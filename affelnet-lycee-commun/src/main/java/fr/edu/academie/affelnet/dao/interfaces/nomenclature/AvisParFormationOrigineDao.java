/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.AvisParFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;

/**
 * Objet d'acc�s aux donn�es concernant les formations origine pour lesquelles
 * un avis 'PARAMEFA' est demand� dans le cadre de l'application d'un param�tre par formation d'accueil.
 */
public interface AvisParFormationOrigineDao extends BaseDao<AvisParFormationOrigine, Long> {

    /**
     * V�rifie si le lien entre une formation d'origine et un param�tre par formation d'accueil existe d�j�.
     * 
     * @param pfaId
     *            L'id du param�tre par formation d'accueil
     * @param mnemonique
     *            mnemonique de la formation d'origine pour laquelle l'avis sera demand�
     * @param codeSpecialite
     *            code specialite de la formation d'origine pour laquelle l'avis sera demand�
     * @param option1
     *            L'option 1 de la formation d'origine
     * @param option2
     *            L'option 2 de la formation d'origine
     * @return vrai si la formation d'origine et ses options ne sont pas encore li�es au param�tre par
     *         formation d'accueil, faux sinon
     */
    boolean isUnique(Long pfaId, String mnemonique, String codeSpecialite, Matiere option1, Matiere option2);
}
