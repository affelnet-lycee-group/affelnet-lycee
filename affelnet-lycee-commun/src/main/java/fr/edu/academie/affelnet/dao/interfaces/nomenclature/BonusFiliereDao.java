/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;

/** Interface pour le DAO des bonus entre fili�res. */
public interface BonusFiliereDao extends BaseDao<BonusFiliere, Long> {

    /**
     * @param bonusFiliere
     *            le bonusFiliere
     * @return <code>true</code> si c'est un doublon
     */
    boolean isUnique(BonusFiliere bonusFiliere);

    /**
     * Liste les <code>BonusFiliere</code> dont la formation d'origine correspond au mefstat4 pass� en param�tre.
     * 
     * @param mefStat4
     *            le mefstat4 sur lequel filtrer
     * @return la liste des <code>BonusFiliere</code> correspondant
     */
    List<BonusFiliere> listerBonusFilieresPourMefStat4(String mefStat4);

    /**
     * Founis les bonus fili�re pour une formation.
     * 
     * @param formation
     *            la formation
     * @return la liste des bonus fili�re
     */
    List<BonusFiliere> listerBonusFiliere(Formation formation);

    /**
     * Founis l'ensemble des bonus fili�re avec des donn�es compl�mentaires via jointue fetch.
     * 
     * @return la liste des bonus fili�re
     */
    List<BonusFiliere> listerBonusFiliereFetch();
}
