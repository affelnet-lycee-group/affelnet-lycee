/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigineModifiee;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;

/**
 * Interface pour le DAO des formations origine modifi�es.
 */
public interface FormationOrigineModifieeDao extends BaseDao<FormationOrigineModifiee, Long> {
    /**
     * @param currentKey
     *            la cl� courante
     * @param formationInitiale
     *            la <code>Formation</code> initiale
     * @param optionInitiale1
     *            l'option initiale 1
     * @param optionInitiale2
     *            l'option initiale 2
     * @return si la formation + option initiales est unique
     */
    boolean isUnique(Long currentKey, Formation formationInitiale, Matiere optionInitiale1,
            Matiere optionInitiale2);

    /**
     * Fournis la liste des formations origine modifi�es ayant cette formation initiale.
     * 
     * @param formation
     *            la formation initiale
     * @return la liste des formation origine modifi�e
     */
    List<FormationOrigineModifiee> listerFormationOrigineModifieePourFormationInitiale(Formation formation);

    /**
     * Fournis l'ensemble des formations origine modifi�es avec des objets li�es via jointure fetch.
     * 
     * @return la liste des ofmrations origine modifi�es
     */
    List<FormationOrigineModifiee> listerFormationOrigineModifieeAvecJointuresFetch();
}
