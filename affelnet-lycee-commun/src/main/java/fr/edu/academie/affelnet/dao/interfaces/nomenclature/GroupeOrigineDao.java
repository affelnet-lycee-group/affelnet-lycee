/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigineFormation;

/** Interface pour le DAO des groupes origine. */
public interface GroupeOrigineDao extends BaseDao<GroupeOrigine, String> {

    /**
     * 
     * @param groupeOrigine
     *            le nouveau <code>GroupeOrigine</code> � ins�rer dans la base
     * @return le <code>GroupeOrigine</code> cr��
     */
    GroupeOrigine creer(final GroupeOrigine groupeOrigine);

    /**
     * @param code
     *            le code du <code>GroupeOrigine</code> � charger
     * @return le <code>GroupeOrigine</code> correspondant au code
     */
    GroupeOrigine charger(final String code);

    /**
     * @param groupeOrigine
     *            le <code>GroupeOrigine</code> � mettre � jour
     * @param oldKey
     *            la cl� de l'ancien <code>GroupeOrigine</code>
     * @return le <code>GroupeOrigine</code> mis � jour
     */
    GroupeOrigine maj(final GroupeOrigine groupeOrigine, final String oldKey);

    /**
     * @param groupeOrigine
     *            le <code>GroupeOrigine</code> � supprimer
     */
    void supprimer(final GroupeOrigine groupeOrigine);

    /**
     * @param codeGroupe
     *            le <code>GroupeOrigine</code> dont on veut lister les
     *            formations associ�es
     * @return la <code>List</code> des formations
     */
    List<GroupeOrigineFormation> listerFormations(String codeGroupe);

    /**
     * Cette m�thode met � 'null' le groupe origine pour tous les �l�ves et
     * remet les compteurs �l�ves dans les groupes � z�ro.
     */
    void majEleves();

}
