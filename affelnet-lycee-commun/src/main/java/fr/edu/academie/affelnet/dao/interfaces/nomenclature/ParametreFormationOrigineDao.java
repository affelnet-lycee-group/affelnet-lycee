/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.ParametreFormationOrigine;

/**
 * Interface pour le DAO des param�tres par formation d'origine.
 */
public interface ParametreFormationOrigineDao extends BaseDao<ParametreFormationOrigine, Long> {
    /**
     * @param currentKey
     *            la cl� primaire
     * @param formation
     *            la formation test�e
     * @param mnemonique
     *            le mn�monique de la formation test�e
     * @param option1
     *            l'option 1 de la formation test�e
     * @param option2
     *            l'option 2 de la formation test�e
     * 
     * @return vrai si la formation existe d�j� et est unique, faut sinon
     */
    boolean isUnique(Long currentKey, Formation formation, String mnemonique, Matiere option1, Matiere option2);

    /**
     * M�thode permettant de compter les parametres par formation d'origine pour une mn�monique et
     * un code de sp�cialit� nul.
     * 
     * @param mnemonique
     *            une mn�monique de formation
     * @return le nombre de parametres par formation d'origine associ�s � le mn�monique et le code de sp�cialit�.
     */
    Long nombreParamefoGenerique(String mnemonique);

    /**
     * Liste les param�tres par formation d'origine correspondant � une formation d'origine.
     * 
     * @param formation
     *            la formation d'origine
     * @return la liste des param�tres par formation d'origine
     */
    List<ParametreFormationOrigine> listerParametreFormationOriginePourFormation(Formation formation);

    /**
     * Liste les param�tres par formation d'origine en chargeant les objets li�s via jointure fetch.
     * 
     * @return l'ensemble des param�tres par formation d'origine
     */
    List<ParametreFormationOrigine> listerParametreFormationOrigineAvecJointureFetch();
}
