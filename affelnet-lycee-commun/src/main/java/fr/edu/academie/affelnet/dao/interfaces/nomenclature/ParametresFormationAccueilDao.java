/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;
import java.util.Map;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;

/**
 * Interface pour le DAO des param�tres par formation d'accueil.
 */
public interface ParametresFormationAccueilDao extends BaseDao<ParametresFormationAccueil, Long> {

    /**
     * @param parametresFormationAccueil
     *            la parametresFormationAccueil � tester
     * @return vrai si la parametresFormationAccueil est unique.
     */
    boolean isUnique(ParametresFormationAccueil parametresFormationAccueil);

    /**
     * @param formation
     *            la <code>Formation</code> d'accueil
     * @return un tableau de param�tres par formation d'accueil qui utilisent la
     *         formation pass�e en param�tre tri� par ordre de leur application
     *         (du + g�n�rique au - g�n�rique)
     */
    ParametresFormationAccueil[] listerPfaFormation(Formation formation);

    /**
     * Fournit les modes de bar�me utilis�s par l'OPA.
     * 
     * @param idOpa
     *            Id de l'OPA
     * @param numeroTour
     *            Le tour actuel
     * @return les modes de bar�me utilis�s
     */
    List<String> modeBaremeParOpa(Long idOpa, short numeroTour);

    /**
     * G�n�re un filtre pour r�cup�rer les �l�ves avec un voeu dont le mode de calcul du bar�me est �valuation.
     * 
     * @param tourCourant
     *            Le tour courant
     * @return un filtre avec les �l�ves avec un voeu eval
     */
    Filtre filtreEleveAvecVoeuEval(short tourCourant);

    /**
     * R�cup�re les param�tres par formation d'accueil correspondants � la formation sans prendre en compte
     * l'enseignement optionnel lorsqu'il n'y a pas de cache. Sinon, renvoie l'ensemble des paramefa tout en
     * alimentant le cache si n�cessaire.
     * 
     * @param formation
     *            la formation dont on cherche les paramefa
     * @param cacheMap
     *            la map contenant en cache les donn�es d�j� extraites
     * @return la liste des paramefa correspondant � la formation donn�e ou l'ensemble des paramefa
     */
    List<ParametresFormationAccueil> recuprererParamefa(Formation formation, Map<String, Object> cacheMap);

    /**
     * R�cup�re le param�tre par formation d'accueil correspondant strictement � la formation et � l'enseignement
     * optionnel ou null si celui-ci n'existe pas.
     * 
     * @param formation
     *            la formation du paramefa
     * @param cleGestionEO
     *            la cl� de gestion de l'enseignement optionnel
     * @return la paramefa correspondant strictement � la formation et l'EO ou null
     */
    ParametresFormationAccueil recuprererParamefaExactAvecFormationEtEO(Formation formation,
            String cleGestionEO);

    /**
     * R�cup�re le param�tre par formation d'accueil correspondant strictement � la formation et � l'enseignement
     * optionnel ou null si celui-ci n'existe pas.
     * 
     * @param codeSpecialite
     *            le code sp�cialit� de la formation
     * @param mnemonique
     *            le mn�monique de la formation
     * @param cleGestionEO
     *            la cl� de gestion de l'enseignement optionnel
     * @return la paramefa correspondant strictement � la formation et l'EO ou null
     */
    ParametresFormationAccueil recuprererParamefaExactAvecFormationEtEO(String codeSpecialite, String mnemonique,
            String cleGestionEO);
}
