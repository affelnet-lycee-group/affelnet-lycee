/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;
import java.util.Map;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Pile;
import fr.edu.academie.affelnet.domain.nomenclature.PilePK;

/** Interface pour le DAO de la pile du voeu. */
public interface PileDao extends BaseDao<Pile, PilePK> {

    /**
     * M�thode permettant de r�cup�rer la liste des voeux et des param�tres par formation accueil pour un tour
     * donn� en fonction du mode de bar�me.
     * 
     * @param numTour
     *            le num�ro du tour concern�
     * @param modeBareme
     *            le mode de bar�me cherch�
     * @return une liste d'objet contenant en premi�re position l'id du paramefa et en seconde position le code de
     *         l'offre de formation
     */
    List<Object[]> listeVoeuxParametresFormationAccueilPourTour(Short numTour, String modeBareme);

    /**
     * M�thode permettant de r�cup�rer la liste des voeux et des param�tres par formation accueil pour une OPA
     * donn�e.
     * 
     * @param idOpa
     *            l'identifiant des op�rations programm�es
     * @return une liste d'objet contenant en premi�re position l'id du paramefa et en seconde position le code de
     *         l'offre de formation
     */
    List<Object[]> listeVoeuxParametresFormationAccueilPourOpa(Long idOpa);

    /**
     * M�thode permettant de r�cup�rer la liste des voeux et des param�tres par formation accueil pour une OPA
     * donn�e.
     * 
     * @param idOpa
     *            l'identifiant des op�rations programm�es
     * @param modeBareme
     *            le mode de bar�me cherch�
     * @return une liste d'objet contenant en premi�re position l'id du paramefa et en seconde position le code de
     *         l'offre de formation
     */
    List<Object[]> listeVoeuxParametresFormationAccueilPourOpa(Long idOpa, String modeBareme);

    /**
     * M�thode comptant les offres de formations disponibles en fonction du tour, pour les tours suivants.
     * Prend en compte le tour suivant actuel et ceux ant�rieurs.
     * 
     * @return les offres de formations disponibles pour chaque tour suivant
     */
    Map<Integer, Integer> compteOffreFormationPourTourSuivant();
}
