/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.RapprochEtab;

/**
 * Interface pour le DAO des rapprochements entre �tablissements.
 */
public interface RapprochEtabDao extends BaseDao<RapprochEtab, Long> {
    /**
     * @param rapprochEtab
     *            le rapprochEtab � tester
     * @return <code>true</code> s'il n'y a pas de doublon
     */
    boolean isUnique(RapprochEtab rapprochEtab);

    /**
     * @return la liste des options pour un rapprochement
     */
    List<Matiere> getOptions();

    /**
     * Fournis les rapprochements �tablissement correspondante � l'�tablissement d'accueil donn�.
     * 
     * @param etabAccueil
     *            l'�tablissement d'accueil
     * @return les rapprochements �tablissement
     */
    List<RapprochEtab> listerRapprochEtabPourEtabAccueil(Etablissement etabAccueil);

    /**
     * Fournis la liste des rapprochements �tablissement avec les objets li�s charg�s via jointure fetch.
     * 
     * @return la liste des rapprochements �tablissement
     */
    List<RapprochEtab> listerRapprochEtabAvecJointureFetch();
}
