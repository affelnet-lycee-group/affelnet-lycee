/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature;

import java.util.List;
import java.util.Map;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;

/**
 * Interface pour le DAO de l'offre de formation.
 */
public interface VoeuDao extends BaseDao<Voeu, String> {
    /**
     * @param prefixeVoeu
     *            le pr�fixe de l'offre de formation (3 premiers caract�res)
     * @return le code g�n�r� (pr�fixe + incr�ment sur 5 caract�res commen�ant par 11111)
     */
    String getCode(String prefixeVoeu);

    /**
     * @param codeInterne
     *            le code interne
     * @return <code>true</code> si l'offre de formation existe
     */
    boolean existeVoeu(String codeInterne);

    /** Contr�le de l'unicit� de l'offre de formation transmise.
     * On se base sur les �l�ments suivants :
     * <ul>
     *     <li>la formation d'accueil (Mef : mn�monique + sp�cialit�)</li>
     *     <li>les enseignements optionnels contingent�s �ventuels (cl� de gestion)</li>
     *     <li>le statut de la scolarit� d'accueil (code statut)</li>
     *     <li>l'�tablissement d'accueil (identifiant �tablissement UAI/RNE)</li>
     * </ul>
     * 
     * @param offreFormation
     *            l'offre de formation dont l'unicit� est � tester
     * @return vrai si l'offre de formation est unique, sinon faux.
     */
    boolean isUnique(Voeu offreFormation);

    /**
     * Liste les offres de formation avec prise en compte des notes non li�es � un param�tre par formation
     * d'accueil.
     * Si l'ID de l'OPA est fourni, on se restrait � celle-ci. Sinon, on regarde pour l'ensemble des offres.
     * 
     * @param idOpa
     *            l'identifiant de l'opa en cours de traitement
     * @return la liste des offres de formation Bar�me avec �valuations/notes qui n'ont pas de coefs sp�cifi�s
     */
    List<Voeu> listerOffresAvecPriseEnCompteNoteEvalSansPFA(Long idOpa);

    /** Constitue la liste des offres de formation identiques (ne respectant pas le crit�re d'unicit�).
     * On se base sur les �l�ments suivants :
     * <ul>
     *     <li>la formation d'accueil (Mef : mn�monique + sp�cialit�)</li>
     *     <li>les enseignements optionnels contingent�s �ventuels (cl� de gestion)</li>
     *     <li>le statut de la scolarit� d'accueil (code statut)</li>
     *     <li>l'�tablissement d'accueil (identifiant �tablissement UAI/RNE)</li>
     * </ul>
     * 
     * @return liste des offres de formation identiques
     */
    List<Voeu> listerOffresFormationIdentiques();

    /**
     * Fournis la liste des offres de formation hors recensement, � capacit� non nulle
     * et li�es � aucune OPA en indiquant si un voeu a �t� formul� dessus au tour principal.
     * 
     * @return une map avec les offres de formation sans OPA en index et si elles sont li�es � un voeu du tour
     *         principal dans la valeur.
     */
    Map<Voeu, Boolean> offresFormationsSansOpa();
}
