/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DomaineSpecialite;

/**
 * Interface de DAO pour les domaines sp�cialit�s.
 *
 */
public interface DomaineSpecialiteDao extends BaseDao<DomaineSpecialite, String> {

    /**
     * M�thode listant tous les codes de la table domaine de sp�cialit�.
     * 
     * @return une liste de code sur 2 caract�res
     */
    List<String> getListeCode();

    /**
     * M�thode listant tous les libell�s de la table domaine de sp�cialit�.
     * 
     * @return une liste de libell�
     */
    List<String> getListeLibelle();

    /**
     * M�thode permettant de v�rifier si une formation est
     * contenu dans un domaine de sp�cialit�.
     * 
     * @param formation
     *            la formation avec son mefstat11
     * @return vrai si le code mefstat11 contient le domaine
     */
    boolean containsCodeFormation(Formation formation);

    /**
     * M�thode permettant de chercher si le code existe dans la table.
     * 
     * @param code
     *            est les deux caract�re r�cup�r�
     * @return vrai si le code existe
     */
    boolean existeCodeFormation(String code);

}
