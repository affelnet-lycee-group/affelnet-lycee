/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;

/** Interface pour le DAO des �valuations compl�mentaires. */
public interface EvaluationComplementaireDao extends BaseDao<EvaluationComplementaire, Long> {

    /**
     * Modifie l'ordre de l'�valuation compl�mentaire concern�e.
     * 
     * @param ancienOrdre
     *            ancien ordre
     * @param nouvelOrdre
     *            nouvel ordre
     */
    void updateOrdre(int ancienOrdre, int nouvelOrdre);

    /**
     * Modifie l'ordre des autres �valuation compl�mentaire concern�e.
     * 
     * @param ancienOrdre
     *            ancien ordre
     * @param nouvelOrdre
     *            nouvel ordre
     * @param idCourantEC
     *            identifiant de l'�valuation compl�mentaire � ne pas modifier
     */
    void updateOrdreAutreEC(int ancienOrdre, int nouvelOrdre, Long idCourantEC);
}
