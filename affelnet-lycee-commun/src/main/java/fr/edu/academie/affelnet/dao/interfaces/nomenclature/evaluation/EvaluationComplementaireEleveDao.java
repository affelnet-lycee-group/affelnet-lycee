/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireElevePK;

/**
 * Classe permettant de faire le lien entre les �valuation compl�mentaire, les �l�ves et les �chelons.
 *
 */
public interface EvaluationComplementaireEleveDao
        extends BaseDao<EvaluationComplementaireEleve, EvaluationComplementaireElevePK> {

    /**
     * M�thode permettant de v�rifier si les �valuations n'ont pas encore �t� saisie.
     * 
     * @return vrai si il y a aucun r�sultat
     */
    boolean isEvaluationSaisies();

    /**
     * M�thode permettant de lister les �valuations compl�mentaires par �l�ve.
     * 
     * @param ine
     *            l'identifiant national de l'�l�ve
     * @return une liste des �valuation de l'�l�ve
     */
    List<EvaluationComplementaireEleve> listerParEleve(String ine);

    /**
     * M�thode permettant de nettoyer la table si on supprime un �l�ve.
     * 
     * @param ine
     *            l'identifiant national de l'�l�ve
     */
    void purger(String ine);

    /**
     * Cr�� un filtre pour les �valuations compl�mentaires concernant les �l�ves avec un groupe origine.
     * 
     * @return un filtre pour les �valuations compl�mentaires concernant les �l�ves avec un groupe origine
     */
    Filtre filtreEvalComAvecGroupeOrigine();
}
