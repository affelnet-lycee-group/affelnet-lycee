/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.voeu;

import java.util.List;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.CandidaturePK;

/**
 * Interface pour le DAO des candidatures d'un �l�ve.
 */
public interface CandidatureDao extends BaseDao<Candidature, CandidaturePK> {

    /**
     * Recherche la quandidature de l'�l�ve en fonction de son ide et du tour
     *
     * @param ine
     *            Ine de l'�l�ve
     * @param numeroTour
     *            Num�ro du tour
     * @return La candidature de l'�l�ve en fonction de son ide et du tour
     */
    Candidature getCandidatureEleve(String ine, short numeroTour);

    /**
     * Mise � jour des flags flagValidationAvis, flagSaisieValide, flagConformeDo sur la candidature de l'�l�ve.
     * Mse � jour �galement de la date de mise � jour de la candidature.
     * Utilis� dans les traitements de masse (batch d'int�gration finale).
     *
     * @param candidature
     *            Candidature
     * @return nombre d'enregistrements mis � jour
     */
    int majFlagsCandidature(Candidature candidature);

    /**
     * R�cup�re les candidatures du tour des �l�ves en zone g�ographique unique en fonction du tour.
     * 
     * @param numeroTour
     *            Num�ro du tour.
     * @return Les candidatures du tour des �l�ves en zone g�ographique unique en fonction du tour.
     */
    List<Candidature> getCandidaturesZoneGeoUnique(short numeroTour);
}
