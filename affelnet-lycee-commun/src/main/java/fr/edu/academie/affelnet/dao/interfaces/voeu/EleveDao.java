/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.voeu;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.domain.carteScolaire.TronconCarteScolaire;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationPK;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.EtablissementNational;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.enumeration.StatutZoneGeoEnum;
import fr.edu.academie.affelnet.utils.Collecteur;
import org.hibernate.ScrollableResults;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Interface pour le DAO des �l�ves.
 */
public interface EleveDao extends BaseDao<Eleve, String> {

    /**
     * @return vrai si des notes ont d�j� �t� saisies
     */
    boolean isNotesSaisies();

    /**
     * @param etablissement
     *            l'<code>Etablissement</code> dont on veut la liste �l�ves
     * @return la liste des �l�ves pr�sents dans l'�tablissement
     */
    List<Eleve> getListeEleves(Etablissement etablissement);

    /**
     * @param etablissement
     *            l'<code>Etablissement</code> dont on veut la liste �l�ves
     * @return le nombre d'�l�ves dont la saisie est valide dans l'�tablissement
     */
    long getNbElevesValides(Etablissement etablissement);

    /**
     * Liste les �l�ves ayant au moins 1 voeu et n'ayant pas l'�ge entre 10 et 30.
     * Ils doivent sortir en anomalie lors de l'audit.
     * 
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return liste des �l�ves n'ayant pas l'�ge entre 10 et 30
     */
    List<Eleve> listEleveAvecAgeInvalide(List<Filtre> filtresSupplementaire);

    /**
     * Liste les �l�ves sans zone g�ographique et ayant formul� des voeux.
     * 
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return liste des �l�ves sans zone g�ographique et ayant formul� des voeux
     */
    List<Eleve> elevesSansZoneGeographique(List<Filtre> filtresSupplementaire);

    /**
     * Liste les �l�ves sans responsable notifiable et ayant au moins un voeu qui n'est pas de recensement.
     *
     * @param filtresSupplementaire les filtres suppl�mentaires � appliquer
     * @return liste des �l�ves sans responsable notifiable
     */
    List<Eleve> listerElevesSansResponsableNotifiable(List<Filtre> filtresSupplementaire);

    /**
     * Liste les �l�ves qui ont au moins un responsable non notifiable et ayant au moins un voeu qui n'est pas de recensement.
     *
     * @param filtresSupplementaire les filtres suppl�mentaires � appliquer
     * @return liste des �l�ves avec au moins un responsable non notifiable
     */
    List<Eleve> listerElevesAvecResponsableNonNotifiable(List<Filtre> filtresSupplementaire);

    /**
     * Lister les �tablissements des �l�ves pr�sents n'ayant pas de district.
     *
     * @return la <code>List</code> des �tablissements des �l�ves pr�sents
     * n'ayant pas de district (les entit�s)
     */
    List<Etablissement> listerEtablissementSansDistrictDesElevesPresentsEntite();

    /**
     * Recherche si le mn�monique indiqu� correspond � la formation d'origine d'au moins un �l�ve.
     * 
     * @param mnemo
     *            le mn�monique du MEF
     * @return si des �l�ves sont rattach� � ce mnemo
     */
    boolean estMnemoniqueFormationOrigineEleve(String mnemo);

    /**
     * @param nom
     *            le nom de l'�l�ve
     * @param prenom
     *            le pr�nom de l'�l�ve nom de l'�l�ve
     * @param dateNaissance
     *            la date de naissance de l'�l�ve
     * @param idEtablissement
     *            Le code uai de l'�tablissement de l'�l�ve.
     * @return la liste des �l�ves en probalit� de doublons.
     */
    List<Eleve> getDoublons(String nom, String prenom, Date dateNaissance, String idEtablissement);

    /**
     * @param etablissement
     *            l'<code>Etablissement</code>
     * @return la liste des classes pour un �tablissement
     */
    List<String> getClasses(Etablissement etablissement);

    /**
     * @param etablissement
     *            l'<code>Etablissement</code>
     * @return la liste des options pour un �tablissement
     */
    List<Matiere> getOptions(Etablissement etablissement);

    /**
     * @param etablissement
     *            l'<code>Etablissement</code>
     * @return la liste des formations pr�sentes dans un �tablissement
     */
    List<Formation> getFormations(Etablissement etablissement);

    /**
     * Avant d'inserer les eleves de AV_ELEVE vers GV_ELEVE on ajoute les
     * etablissements manquants dans GN_ETABACA si il y en a.
     */
    void insertionEtablissementsManquants();

    /**
     * Avant d'inserer les eleves de AV_ELEVE vers GV_ELEVE on ajoute les
     * identifiants etablissements manquants dans CN_INITETABID.
     */
    void insertionIDEtabManquants();

    /**
     * permet d'indiquer sur les �l�ves ceux consid�r� comme doublons.
     */
    void tagguerDoublons();

    /**
     * @param eleve
     *            l'<code>Eleve</code> � traiter comme non doublon
     */
    void traiterDoublon(Eleve eleve);

    /**
     * Cette m�thode liste les formations origine pr�sentes dans la table
     * GV_ELEVE qui n'ont pas de correspondance dans la table GN_PARAMEFO. Cette
     * m�thode est utilis�e dans le traitement de fin de saisie des voeux.
     * 
     * @return une liste des mn�moniques, codes de sp�cialit� et options des formation origine.
     */
    List<String[]> listerFormationOrigineSansParametres();

    /**
     * Equivalent du exist(), mais s'assure que l'on est pas perturb� par un
     * objet mis en cache hibernate.
     * 
     * @param ine
     *            l'INE de l'�l�ve
     * @return vrai si l'�l�ve existe dans la table GV_ELEVE
     */
    boolean existeEleveSQL(String ine);

    /**
     * Suppression des �l�ves appartenant � la liste des formations pass�e en
     * param�tre.
     * 
     * @param listFormationPKASupprimer
     *            liste des identifiants des formations concern�es
     * @return nombre d'entr�es modifi�es
     */
    int suppressionMultipleParFormation(List<FormationPK> listFormationPKASupprimer);

    /**
     * Suppression des �l�ves appartenant � la liste des mefStat4 pass�e en
     * param�tre.
     * 
     * @param listMefStat4ASupprimer
     *            liste des codes MefStat4 concern�s
     * @return nombre d'entr�es modifi�es
     */
    int suppressionMultipleParMefStat4(List<String> listMefStat4ASupprimer);

    /**
     * M�thode de suppression des �l�ves associ�s � une collection de mn�moniques sp�cifi�e.
     * 
     * @param mnemoniquesASupprimer
     *            la table des mn�moniques, index�e par leur MefStat4
     * @return le nombre d'�l�ves supprim�s
     */
    int suppressionMultipleParMnemonique(Map<String, List<String>> mnemoniquesASupprimer);

    /**
     * @param eleve
     *            l'<code>Eleve</code>
     * @return la proposition d'accueil pour l'�l�ve
     */
    VoeuEleve getPropositionAccueil(Eleve eleve);

    /**
     * Filtre sur les �l�ves ayant formul� des voeux.
     * 
     * @return filtre sur la pr�sence de voeux
     */
    Filtre getFiltreEleveAvecVoeux();

    /**
     * Cr�e un filtre donnant les �l�ves sans proposition d'accueil.
     * 
     * @return filtre sur l'absence de voeux de propositions d'accueil
     */
    Filtre getFiltreElevesSansPropositionAccueil();

    /**
     * Cr�e un filtre donnant les �l�ves qui n'ont pas que des voeux de recencement.
     * 
     * @return filtre sur les �l�ves
     */
    Filtre getFiltreExistenceVoeuAutreQueRecencement();

    /**
     * Cette m�thode r�cup�re les classes des �l�ves dont l'�tablissement
     * d'origine est l'�tablissement utilisant le service de saisie simplifi�e..
     * 
     * @param etablissementNational
     *            l'�tablissement national
     * @return la liste des classes
     */

    List<String> getClasses(EtablissementNational etablissementNational);

    /**
     * Cette m�thode r�cup�re les formations des �l�ves dont l'�tablissement
     * d'origine est l'�tablissement utilisant le service de saisie simplifi�e..
     * 
     * @param etablissementNational
     *            l'�tablissement national
     * @return la liste des formations
     */

    List<Formation> getFormations(EtablissementNational etablissementNational);

    /**
     * M�thode de construction d'un filtre de s�lection des �l�ves dont des notes sont manquantes.
     * 
     * @param rangObligatoires
     *            les rangs des notes obligatoires
     * @return le filtre des �l�ves auxquels il manque au moins une note obligatoire
     */
    Filtre filtreElevesSansNote(List<Rang> rangObligatoires);

    /**
     * Fournit un filtre sur le palier origine de l'�l�ve.
     * 
     * @param codePalier
     *            le code du palier origine de l'�l�ve
     * @return un filtre sur le palier origine de l'�l�ve
     */
    Filtre filtreElevesPalierOrigine(short codePalier);

    /**
     * Fournit un filtre des �l�ves poss�dant un voeu avec calcul du bar�me.
     * 
     * @param tourCourant
     *            le tour actuel
     * @return un filtre des �l�ves avec voeux et calcul du bar�me
     */
    Filtre filtreElevesAvecVoeuAvecBareme(short tourCourant);

    /**
     * M�thode de r�cup�ration de l'ensemble des �l�ves de palier 3�me d'un �tablissement.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return un r�sultat scrollable contenant tous les �l�ves de palier 3�me de l'�tablissement
     */
    ScrollableResults scrollElevePalier3emePourEtab(String idEtablissement);

    /**
     * Mise � jour de la note moyenne et de la date de mise � jour de l'�l�ve.
     * Utilis� dans les traitements de masse (batch d'int�gration finale).
     *
     * @param eleve
     *            Eleve
     * @return nombre d'enregistrements mis � jour
     */
    int majNoteMoyenne(Eleve eleve);

    /**
     * Initialisation du statut de la zone g�ographique � 'NON_TRAITE' pour l'ensemble des �l�ves du palier 3�me
     * pour lesquels le statut de la zone g�ographique est diff�rent de 'MANUEL'.
     */
    void reinitialiserStatutZoneGeoPalier3eme();

    /**
     * Sp�cifier la zone g�ographique des �l�ves du palier 3�me faisant partie de la commune sp�cifi�e dans le
     * tron�on de la carte scolaire fournit.
     *
     * @param tronconCarteScolaire
     *            Tron�on de la carte scolaire.
     */
    void specifierZoneGeoElevesMemeCommuneTroncon(TronconCarteScolaire tronconCarteScolaire);

    /**
     * Retourne un collecteur listant les �l�ves pour lesquels les adresses ont �t� redress�es et dont le statut de
     * la zone g�ographique est NON_TRAITE ou MANUEL.
     *
     * @return Un collecteur listant les �l�ves pour lesquels les adresses ont �t� redress�es et dont le statut de
     *         la zone g�ographique est NON_TRAITE ou MANUEL.
     */
    Collecteur<Eleve> scrollEleves3emeAcaAdresseRedressee();

    /**
     * Retourne le nombre d'�l�ve ayant pour statut de d�termination automatique de la zone g�ographique celui
     * fournit en entr�e.
     *
     * @param statutZoneGeo
     *            Le statut de la zone g�ographique.
     * @return Le nombre d'�l�ve du ayant pour statut de d�termination automatique de la zone g�ographique celui
     *         fournit en entr�e.
     */
    int nombreElevesPalier3emeParStatutZoneGeo(StatutZoneGeoEnum statutZoneGeo);

    /**
     * Retourne le nombre d'�l�ve ayant une adresse pour laquelle aucune correspondance n'a �t� trouv�e dans la
     * carte scolaire.
     * 
     * @return Le nombre d'�l�ve ayant une adresse pour laquelle aucune correspondance n'a �t� trouv�e dans la
     *         carte scolaire.
     */
    int nombreElevesAdresseInvalide();

    /**
     * Retourne les �l�ves de palier 3�me respectant le filtre sp�cifi� et tri� dans l'ordre sp�cifi�.
     *
     * @param filtre
     *            Le filtre
     * @param tris
     *            Tris.
     * @return �l�ves de palier 3�me respectant le filtre sp�cifi� et tri� dans l'ordre sp�cifi�.
     */
    List<Eleve> listerElevesPalier3eme(Filtre filtre, List<Tri> tris);

    /**
     * Retourne les �l�ves dont la zone g�ographique n'a pu �tre d�termin�e automatiquement.
     * 
     * @return Ls �l�ves dont la zone g�ographique n'a pu �tre d�termin�e automatiquement.
     */
    List<Eleve> listerElevesSansZoneGeoDetermineeAuto();
}
