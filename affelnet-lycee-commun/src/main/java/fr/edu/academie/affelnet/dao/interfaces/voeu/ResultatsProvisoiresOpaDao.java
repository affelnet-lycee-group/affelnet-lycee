/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.voeu;

import java.util.List;

import fr.edu.academie.affelnet.batch.planificationCampagneAffectation.CompteurOpa;
import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;

/**
 * Interface pour le DAO des r�sultats provisoires d'une OPA.
 */
public interface ResultatsProvisoiresOpaDao extends BaseDao<ResultatsProvisoiresOpa, VoeuElevePK> {

    /**
     * Constitue les compteurs de r�sultats de l'OPA.
     *
     * @param idOpa
     *            identifiant de l'OPA
     * @return compteurs de r�sultats de l'OPA
     */
    CompteurOpa calculerCompteurs(long idOpa);

    /**
     * Liste les OPA s�curisables d�j� ex�cut�es o� l'�tablissement a des �l�ves non affect�s
     * provisoirement.
     * 
     * @param idEtablissement
     *            l'identifiant de l'�tablissement d'origine des �l�ves (code RNE)
     * @param numeroTour
     *            le num�ro du tour
     * @return liste des OPA
     */
    List<OperationProgrammeeAffectation> listerOpaSecuriseesExecutees(String idEtablissement, short numeroTour);

    /**
     * Liste les r�sultats provisoires "non affect�s" correspondant aux voeux des �l�ves d'un �tablissement donn�.
     * 
     * On consid�re toutes les OPA s�curis�es du tour courant
     * 
     * @param idEtablissement
     *            l'identifiant de l'�tablissement d'origine des �l�ves (code RNE)
     * @param numeroTour
     *            le num�ro du tour
     * 
     * @return les r�sultats provisoires
     */
    List<ResultatsProvisoiresOpa> listerResultatsProvisoiresNonAffectesSecurisation(String idEtablissement,
            short numeroTour);

    /**
     * Liste les r�sultats provisoires "non affect�s" correspondant aux voeux des �l�ves d'un �tablissement donn�.
     * 
     * @param idOpa
     *            l'OPA pour laquelle retrouver les r�sultats
     * @param idEtablissement
     *            l'identifiant de l'�tablissement d'origine des �l�ves (code RNE)
     * @return les r�sultats provisoires
     */
    List<Eleve> listerElevesNonAffectesProvisoireSecu(Long idOpa, String idEtablissement);

    /**
     * Supprime les r�sultats provisoires corrsondant � des offres non s�lectionn�es pour l'OPA (principale).
     * 
     * @param idOpa
     *            l'identifiant de l'OPA
     */
    void supprimerResultatsProvisoiresOffresNonSelectionnees(Long idOpa);

    /**
     * Liste les identifiants d'OPA pour lesquels un bonus li� � l'avis indiqu� est attribu�.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param avis
     *            l'avis
     * @return liste des identifiants d'OPA
     */
    List<Long> listerIdOpaPourBonusAvis(short numeroTour, Avis avis);

    /**
     * Liste les identifiants d'OPA pour lesquels un bonus lien zone g�ographique peut s'appliquer.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param lienZoneGeo
     *            le lien zone g�ographique
     * @return liste des identifiants d'OPA
     */
    List<Long> listerIdOpaPourLienZoneGeo(short numeroTour, LienZoneGeo lienZoneGeo);

    /**
     * Liste les identifiants d'OPA pour lesquels un param�tre par formation d'accueil peut s'appliquer.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param paramefa
     *            param�tre par formation d'accueil
     * 
     * @return liste des identifiants d'OPA
     */
    List<Long> listerIdOpaPourParamefa(short numeroTour, ParametresFormationAccueil paramefa);
}
