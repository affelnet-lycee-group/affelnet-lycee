/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.dao.interfaces.voeu;

import java.util.List;
import java.util.Set;

import fr.edu.academie.affelnet.dao.interfaces.BaseDao;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;
import fr.edu.academie.affelnet.utils.Collecteur;

/**
 * Interface pour le DAO des voeux des �l�ves.
 */
public interface VoeuEleveDao extends BaseDao<VoeuEleve, VoeuElevePK> {

    /**
     * @param codeVoeu
     *            le code du voeu
     * @param ensembleEleveAIgnorer
     *            l'ensemble des eleves � ne pas prendre en compte
     * @return la liste des lsupp deja saisies
     */
    List<Double> getListLSupp(String codeVoeu, Set<VoeuElevePK> ensembleEleveAIgnorer);

    /**
     * Cette m�thode donne la liste des voeux pour un �tabissement d'origine.
     * Quels sont les voeux que les �l�ves (dont la saisie est valid�e) d'un �tablissement ont demand� ?
     *
     * @param etablissement
     *            l'<code>Etablissement</code> d'origine
     * @return la liste des voeux pour un �tabissement d'origine
     */
    List<Voeu> getListeVoeux(Etablissement etablissement);

    /**
     * Cette m�thode retourne la liste des �tablissements d'accueil pour un
     * �tablissement d'origine. Elle est utilis�e dans la saisie des voeux - bordereau par �tab. d'accueil.
     *
     * @param etablissementOrigine
     *            l'<code>Etablissement</code> d'origine
     * @return la liste des �tablissements d'accueil pour un �tablissement
     *         d'origine
     */
    List<Etablissement> getListeEtablissementAccueil(Etablissement etablissementOrigine);

    /**
     * Cette m�thode donne le nb de fois qu'un voeu est s�lectionn� pour un �tablissement d'origine.
     *
     * @param voeu
     *            le <code>Voeu</code>
     * @param etablissementOrigine
     *            l'<code>Etablissement</code> d'origine
     * @return le nb de fois que le voeu est s�lectionn�
     */
    long getNbSelections(Voeu voeu, Etablissement etablissementOrigine);

    /**
     * Cette m�thode donne le nb de fois qu'un voeu est s�lectionn�.
     *
     * @param voeu
     *            le <code>Voeu</code>
     * @param etablissementOrigine
     *            l'<code>Etablissement</code> d'origine
     * @return le nb de fois que le voeu est s�lectionn� au 1er rang pour un
     *         �tablissement d'origine
     */
    long getNbSelectionsRang1(Voeu voeu, Etablissement etablissementOrigine);

    /**
     * @param ine
     *            l'INE de l'�l�ve
     * @return le nombre de voeux pour un �l�ve
     */
    long getNbVoeux(String ine);

    /**
     * @param ine
     *            l'INE de l'�l�ve
     * @return le nombre de voeux pour un �l�ve pour le tour en cours
     */
    short nombreVoeuxTourCourant(String ine);

    /**
     * Cette m�thode permet de savoir si des �l�ves sont rattach�s � cet avis.
     *
     * @param avis
     *            l'<code>Avis</code> � v�rifier
     * @return <code>true</code> si des �l�ves sont rattach�s � cet avis
     */
    boolean isAvisSaisi(Avis avis);

    /**
     * Liste les codes des offres de formation choisies ayant une capacit� d'affectation nulle.
     * (on ne controle pas la capacit� pour les voeux de recensement)
     *
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return les offres de formation choisies ayant une capacit� d'affectation nulle.
     */
    List<Voeu> listerVoeuxCapaciteNulle(List<Filtre> filtresSupplementaire);

    /**
     * compte les voeux de la table GV_VOEU dont l'�l�ve a un flag de saisie valide � oui.
     *
     * @return le nombre de voeu
     */
    long getNbVoeuxValides();

    /**
     * Liste les identifiants des �tablissement AFFECTATION utilis�s dans les voeux d'�l�ves.
     *
     * @return liste des identifiants UAIs concern�s
     */
    List<String> listerIdEtablissementsAffectationUtilises();

    /**
     * @return liste des �l�ves dont le nombre de voeux est incoh�rent avec le rang maximal
     */
    List<Eleve> listerElevesIncoherenceNbVoeuxRangMax();

    /**
     * @param idOpa
     *            l'id d'OPA
     * @return liste des �l�ves dont le nombre de voeux est incoh�rent avec le rang maximal
     */
    List<Eleve> listerElevesIncoherenceNbVoeuxRangMax(String idOpa);

    /**
     * Collecte les voeux des �l�ves concern�s pour le filtre fourni.
     *
     * @param filtre
     *            Filtre Hql
     * @param listeFectJoin
     *            Liste des �l�ments n�cessitant une jointure fecth
     * @return collecteur des voeux des �l�ves concern�s
     */
    Collecteur<VoeuEleve> collecterVoeuxEleves(Filtre filtre, List<String> listeFectJoin);

    /**
     * Fournit un filtre gardant les �l�ves avec un voeu n�cessitant la pr�sence d'�valuation ou de note pour le
     * tour courant et l'OPA fournie.
     *
     * @param qualifierIne
     *            qualifieur de l'INE � utiliser dans la requ�te
     * @param idOpa
     *            id de l'OPA
     * @return un filtre pour les �l�ves avec un voeu avec eval/note pour le tour courant et l'OPA
     */
    Filtre filtreElevePossedeVoeuAvecEvalNoteTourCourantPourOpa(String qualifierIne, long idOpa);

    /**
     * Donne le nombre de voeux pour lesquels il manque une d�cision finale.
     *
     * @param numeroTour
     *            le num�ro du tour
     * @return nombre de voeux concern�s
     */
    Integer getNombreDecisionsFinalesManquantes(short numeroTour);

    /**
     * Supprime tous les voeux Affelnet des �l�ves ayant des voeux T�l�service.
     *
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    void supprimerVoeuxEleves(int tour, int palier);

    /**
     * Supprime toutes les candidatures des �l�ves ayant des voeux T�l�service.
     *
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    void supprimerCandidaturesEleves(int tour, int palier);

    /**
     * Supprime toutes les d�rogations des �l�ves ayant des voeux T�l�service.
     *
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    void supprimerDerogationsEleves(int tour, int palier);

    /**
     * Supprime tous les r�sultats provisoires des Opa des �l�ves ayant des voeux T�l�service.
     *
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    void supprimerResultatsProvisoiresOpaEleves(int tour, int palier);

    /**
     * Liste des codes voeux affect�s dans des etablissements qui ne font pas partie de l'acad�mie
     * @return
     */
    List<String> getCodesVoeuxAffectesDansEtablisementHorsAcademie();


    /**
     * Liste des el�ves (INE) dont les voeux affect�s dans des etablissements qui ne font pas partie de l'acad�mie
     * @return
     */
    List<String> getElevesAvecVoeuxAffectesEtablisementPublicHorsAcademie();

    /** Indique s'il existe des voeux avec le statut apprentissage */
    boolean isSaisieVoeuApprentissageDemarre();
}
