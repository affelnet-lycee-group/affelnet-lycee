/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.util.List;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classement sur liste principale et liste compl�mentaire.
 * 
 * @param <T>
 *            les �l�ments � classer
 * 
 *            Les listes sont � d�bordement possible.
 */
public class Classement<T extends Comparable<T>> {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(Classement.class);

    /** La capacit� de la liste principale. **/
    private int capaciteListePrincipale;

    /** La capacit� de la liste suppl�mentaire. **/
    private int capaciteListeSupplementaire;

    /** Liste principale. **/
    private TreeSet<T> listePrincipale;

    /** Liste suppl�mentaire. **/
    private TreeSet<T> listeSupplementaire;

    /**
     * Cr�e un classement.
     * 
     * Les capacit�s n�gatives sont remises � z�ro.
     * 
     * @param capaciteListePrincipale
     *            capacit� de la liste principale
     * @param capaciteListeSupplementaire
     *            capacit� de la liste suppl�mentaire
     */
    public Classement(int capaciteListePrincipale, int capaciteListeSupplementaire) {

        this.capaciteListePrincipale = Math.max(capaciteListePrincipale, 0);
        this.capaciteListeSupplementaire = Math.max(capaciteListeSupplementaire, 0);
        this.listePrincipale = new TreeSet<T>();
        this.listeSupplementaire = new TreeSet<T>();
    }

    /** @return la capacit� de la liste principale */
    public int getCapaciteListePrincipale() {
        return this.capaciteListePrincipale;
    }

    /**
     * @return la capacite de la liste supplementaire
     */
    public int getCapaciteListeSupplementaire() {
        return capaciteListeSupplementaire;
    }

    /** @return la liste principale */
    public TreeSet<T> getListePrincipale() {
        return this.listePrincipale;
    }

    /** @return la liste suppl�mentaire */
    public TreeSet<T> getListeSupplementaire() {
        return this.listeSupplementaire;
    }

    /**
     * Ajoute un �l�ment en liste principale.
     * 
     * @param elementClasse
     *            l'�l�ment � classer
     * 
     *            L'ajout se fait sous r�serve d'une capacit� suffisante examin�e lors de la troncature des
     *            listes. Le d�bordement possible avant que la liste principale ne soit tronqu�e.
     */
    public void ajouterListePrincipale(T elementClasse) {
        this.listePrincipale.add(elementClasse);
    }

    /**
     * Ajoute un �l�ment en liste supplementaire.
     * 
     * @param elementClasse
     *            l'�l�ment � classer
     * 
     *            L'ajout se fait sous r�serve d'une capacit� suffisante examin�e lors de la troncature des
     *            listes. Le d�bordement est possible avant que la liste suppl�mentaire ne soit tronqu�e
     */
    public void ajouterListeSupplementaire(T elementClasse) {
        this.listeSupplementaire.add(elementClasse);
    }

    /**
     * Tronque la liste principale pour la mettre en ad�quation avec sa capacit�.
     * 
     * <p>
     * Tant que l'on est au dessus de la capacit�, on retire les �l�ments class�s les plus "faibles" de la liste
     * principale et on les place en liste compl�mentaire. Les �l�ments retir�s sont aussi ajout�s � la liste
     * fournie en param�tre pour traitement ult�rieur.
     * </p>
     * 
     * @param listeElementsRetires
     *            liste des �l�ments retir�s (liste de d�bordement)
     * 
     */
    public void tronquerListePrincipale(List<T> listeElementsRetires) {

        LOG.debug("Troncature au dela de la capacit� de la liste principale");

        LOG.debug("capacit� LP = " + capaciteListePrincipale + " , capacit� LS = " + capaciteListeSupplementaire
                + ", demandes = " + listePrincipale.size());

        while (listePrincipale.size() > capaciteListePrincipale) {
            T dernier = listePrincipale.last();
            listePrincipale.remove(dernier);

            // S'il ya une liste suppl�mentaire on y place l'�l�ment retir�
            if (capaciteListeSupplementaire > 0) {
                ajouterListeSupplementaire(dernier);
            }

            // On m�morise l'�l�ment retir� dans la liste de d�bordement
            listeElementsRetires.add(dernier);
        }

        LOG.debug("Fin de troncature au dela de la capacit� de la liste principale");
    }

    /** Tronque la liste suppl�mentaire pour la mettre en ad�quation avec sa capacit�. */
    public void tronquerListeSupplementaire() {

        // Retire le dernier de la liste suppl�mentaire tant qu'elle d�passe sa capacit�.
        while (listeSupplementaire.size() > capaciteListeSupplementaire) {
            T dernier = listeSupplementaire.last();
            listeSupplementaire.remove(dernier);
        }

    }

}
