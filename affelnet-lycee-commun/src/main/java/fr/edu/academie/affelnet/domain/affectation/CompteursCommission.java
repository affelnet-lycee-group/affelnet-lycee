/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.util.HashMap;
import java.util.Map;

/** Compteurs pour le travail en commission sur une offre de formation donn�e. */
public class CompteursCommission {

    /** Nombre de demandes (hors for�age). Correspond au nombre de voeux non forc�s sur l'offre. */
    private int nombreDemandes;

    /**
     * Nombre de voeux concern�s sur l'offre pour chaque type de d�cision possible en commission (hors
     * for�age).
     */
    private Map<DecisionCommission, Integer> nombreVoeuxParDecision;

    /** Nombre de voeux forc�s affect�s sur l'offre. */
    private int nombreVoeuxForcesAffectes;

    /** Cr�e les compteurs. */
    public CompteursCommission() {
        this.nombreDemandes = 0;
        this.nombreVoeuxParDecision = new HashMap<>();
        this.nombreVoeuxForcesAffectes = 0;
    }

    /**
     * @return nombre de demandes (hors for�age)
     */
    public int getNombreDemandes() {
        return nombreDemandes;
    }

    /**
     * @param nombreDemandes
     *            nombre de demandes (hors for�age)
     */
    public void setNombreDemandes(int nombreDemandes) {
        this.nombreDemandes = nombreDemandes;
    }

    /**
     * @return la table associative du nombre de voeux par type de d�cision prise en commission (hors for�age)
     */
    public Map<DecisionCommission, Integer> getNombreVoeuxParDecision() {
        return nombreVoeuxParDecision;
    }

    /**
     * Positionne un comptage pour un type de d�cision prise en commission.
     * 
     * @param decision
     *            la d�cision � comptabiliser
     * @param valeur
     *            la valeur du compteur (hors for�age)
     */
    public void setComptage(DecisionCommission decision, int valeur) {
        this.nombreVoeuxParDecision.put(decision, valeur);
    }

    /**
     * @param decision
     *            le type de d�cision concern�e
     * @return nombre de voeux pour une d�cision donn�e (hors for�age)
     */
    public int getNombreVoeuxPourDecision(DecisionCommission decision) {

        Integer compteurDecision = this.nombreVoeuxParDecision.get(decision);

        if (compteurDecision == null) {
            throw new IllegalStateException(
                    "Le compteur de d�cision en commission " + decision + " n'a jamais �t� valu�.");
        }

        return compteurDecision;
    }

    /**
     * @param nombreForcesAffectes
     *            nombre de voeux forc�s affect�s
     */
    public void setNombreVoeuxForcesAffectes(int nombreForcesAffectes) {
        this.nombreVoeuxForcesAffectes = nombreForcesAffectes;
    }

    /**
     * @return nombre de voeux forc�s affect�s
     */
    public int getNombreVoeuxForcesAffectes() {
        return nombreVoeuxForcesAffectes;
    }

}
