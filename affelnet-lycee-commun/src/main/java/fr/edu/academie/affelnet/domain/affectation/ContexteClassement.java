/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** Contexte influant sur la fa�on dont sont r�alis�s les classements des voeux des �l�ves. */
public class ContexteClassement {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(ContexteClassement.class);

    /** Date � utiliser par d�faut pour les dates inconnues, c'est la date du classement. */
    private Date dateClassement;

    /**
     * Cr�e un contexte de classement pour un lancement donn�.
     * 
     * @param dateClassement
     *            date du classement � utiliser par d�faut
     */
    public ContexteClassement(Date dateClassement) {

        this.dateClassement = dateClassement != null ? new Date(dateClassement.getTime()) : null;

        LOG.debug("Date du classement � utiliser par d�faut : " + dateClassement);
    }

    /** @return la date courante du classement � utiliser � d�faut d'une autre (horodatage) */
    public Date getDateClassement() {
        return dateClassement != null ? new Date(dateClassement.getTime()) : null;
    }
}
