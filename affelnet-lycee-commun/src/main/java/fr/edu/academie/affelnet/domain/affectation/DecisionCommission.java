/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

/** D�cision saisie pour un voeu d'�l�ve lors du travail en commission. */
public enum DecisionCommission {

    /** Le voeu de l'�l�ve est pris sur l'offre trait�e (pour le statut scolaire uniquement). */
    PRIS("P", "Pris", "Pris", DecisionFinale.AFFECTE),

    /**
     * Le voeu de l'�l�ve est plac� en liste compl�mentaire avec un num�ro associ� (pour le statut scolaire
     * uniquement).
     */
    COMPLEMENTAIRE("C", "liste Comp.", "Liste compl�mentaire", DecisionFinale.LISTE_SUPP),

    /** L'�l�ve est admis et son contrat est sign� (pour le statut apprentissage uniquement). */
    ADMIS_CONTRAT_SIGNE("S", "Admis contrat sign�", "Admis - contrat sign�", DecisionFinale.ADMIS_CONTRAT_SIGNE),

    /** L'�l�ve est admis mais son contrat n'est pas encore sign� (pour le statut apprentissage uniquement). */
    EN_ATTENTE_SIGNATURE_CONTRAT("W", "Attente sign.", "Attente signature contrat",
            DecisionFinale.EN_ATTENTE_SIGNATURE_CONTRAT),

    /** Le voeu de l'�l�ve est refus�. */
    REFUSE("R", "Refus", "Refus", DecisionFinale.REFUSE),

    /**
     * Le voeu de l'�l�ve n'a pas �t� trait� par la commission.
     */
    NON_TRAITE("N", "Non trait�", "Non trait�", DecisionFinale.NON_TRAITE),

    /** Le voeu de l'�l�ve est "de recensement". */
    RECENSEMENT("I", "", "", DecisionFinale.RECENSEMENT),

    /** Le dossier correspondant au voeu de l'�l�ve est d�clar� absent. */
    DOSSIER_ABSENT("A", "Absents", "Dossier absent", DecisionFinale.DOSSIER_ABSENT);

    /** Code associ� � la d�cision. */
    private String code;

    /** Libell� court de la d�cision. */
    private String libelleCourt;

    /** Libell� long de la d�cision. */
    private String libelleLong;

    /** La d�cision finale correspondant � la d�cision prise en commission. */
    private DecisionFinale decisionFinaleCorrespondante;

    /**
     * Constructeur de d�cision de commission.
     * 
     * @param code
     *            le code associ�
     * @param libelleCourt
     *            le libell� court de la d�cision
     * @param libelleLong
     *            le libell� long de la d�cision
     * @param decisionFinaleCorrespondante
     *            la d�cision finale correspondante
     */
    DecisionCommission(String code, String libelleCourt, String libelleLong,
            DecisionFinale decisionFinaleCorrespondante) {
        this.code = code;
        this.libelleCourt = libelleCourt;
        this.libelleLong = libelleLong;
        this.decisionFinaleCorrespondante = decisionFinaleCorrespondante;
    }

    /** @return Code de la d�cision. */
    public String getCode() {
        return this.code;
    }

    /**
     * @return Libell� court de la d�cision.
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @return Libell� long de la d�cision.
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @return la traduction directe en d�cision finale de la d�cision prise en commission
     */
    public DecisionFinale getDecisionFinaleCorrespondante() {
        return decisionFinaleCorrespondante;
    }

    /**
     * @param code
     *            le code de la d�cision
     * @return la d�cision en commission pour le code, ou null si le code est invalide
     */
    public static DecisionCommission getPourCode(String code) {

        if (code == null) {
            return null;
        }

        for (DecisionCommission decisionTestee : values()) {
            if (decisionTestee.getCode().equals(code)) {
                return decisionTestee;
            }
        }

        return null;
    }

    /**
     * @param code
     *            le code de la d�cision � tester
     * @return vrai si la d�cision saisie est valide, sinon false
     */
    public static boolean estCodeValide(String code) {
        return getPourCode(code) != null;
    }

}
