/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.util.HashMap;
import java.util.Map;

/**
 * Repr�sente une d�cision finale d'affectation.
 * 
 * La d�cision est utilisable sur un voeu d'�l�ve, sur sa candidature lors d'un tour et plus g�n�ralement sur la
 * campagne. Il faut toutefois l'accompagner dans ce cas d'une information sur le voeu et le tour concern�s.
 */
public enum DecisionFinale {

    /** L'�l�ve est affect�. */
    AFFECTE(1, "Affect�"),

    /** L'�l�ve est en liste compl�mentaire. */
    LISTE_SUPP(2, "Liste suppl�mentaire"),

    /** L'�l�ve est refus�. */
    REFUSE(3, "Refus�"),

    /**
     * L'�l�ve ne participe plus � l'affectation.
     * 
     * <p>
     * Cette d�cision ne peut �tre attribu�e que globalement sur un �l�ve et par for�age par l'administration.
     * </p>
     */
    ABANDON(4, "Abandon"),

    /**
     * L'�l�ve est Admis mais en attente de signature de contrat.
     * 
     * <p>
     * Cette d�cision n'est attribu�e que pour les voeux de statut apprentissage.
     * </p>
     */
    EN_ATTENTE_SIGNATURE_CONTRAT(5, "Attente signature contrat"),

    /**
     * L'�l�ve est Admis et le contrat est sign�.
     *
     * <p>
     * Cette d�cision n'est attribu�e que pour les voeux de statut apprentissage.
     * </p>
     */
    ADMIS_CONTRAT_SIGNE(6, "Admis - contrat sign�"),

    /**
     * D�cision indicative dite de "recensement" pour l'�l�ve, pour une suite non g�r�e par le processus
     * d'affectation.
     */
    RECENSEMENT(7, "Recensement"),

    /**
     * Le voeu de l'�l�ve n'a pas �t� trait� en commission car son dossier �tait absent (voir
     * {@link DecisionCommission#DOSSIER_ABSENT}).
     * 
     * <p>
     * Remarque : Cette d�cision n'est utilisable que sur un voeu d'�l�ve. Sur un �l�ve, on consid�re lors du
     * reclassement qu'il y a pas de d�cision ce qui est �quivalent � un refus ({@link DecisionFinale#REFUSE}).
     * </p>
     */
    DOSSIER_ABSENT(8, "Dossier absent"),

    /**
     * Le voeu de l'�l�ve est d�clar� explicitement "non trait�" en commission (voir
     * {@link DecisionCommission#NON_TRAITE}).
     * 
     * <p>
     * Remarque : Cette d�cision n'est utilisable que sur un voeu d'�l�ve. Sur un �l�ve, on consid�re lors du
     * reclassement qu'il y a pas de d�cision ce qui est �quivalent � un refus ({@link DecisionFinale#REFUSE}).
     * </p>
     */
    NON_TRAITE(9, "Non trait�");

    /** Le code de la d�cision finale. */
    private int code;

    /** Libell� correspondant � la d�cision finale. */
    private String libelle;

    /**
     * @param code
     *            Le code de la d�cision finale.
     * @param libelle
     *            Libell� correspondant � la d�cision finale
     */
    DecisionFinale(int code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }

    /**
     * @return Le code de la d�cision finale.
     */
    public int getCode() {
        return code;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param code
     *            le code de la d�cision finale � r�soudre (�ventuellement null si non attribu�)
     * @return la d�cision finale correspondant au code, ou null si le code est inconnu
     */
    public static DecisionFinale getPourCode(Integer code) {

        if (code == null) {
            return null;
        }

        for (DecisionFinale decisionTestee : values()) {
            if (decisionTestee.getCode() == code) {
                return decisionTestee;
            }
        }

        return null;
    }

    /**
     * R�cup�re le libell� d'une d�cision finale par son code
     * 
     * @param code
     *            Code de la d�cision finale
     * @return Le libell� de la d�cision finale
     */
    public static String getLibelleDecisionParCode(Integer code) {
        DecisionFinale decisionFinale = getPourCode(code);
        if (decisionFinale != null) {
            return decisionFinale.getLibelle();
        }
        return null;
    }

    /**
     * @return la map liant les codes de d�cision finale avec le libell� associ�.
     */
    public static Map<Integer, String> getLibelleDecisionParCode() {
        Map<Integer, String> libelleParCode = new HashMap<>();
        for (DecisionFinale decision : DecisionFinale.values()) {
            libelleParCode.put(decision.getCode(), decision.getLibelle());
        }
        return libelleParCode;
    }
}
