/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;

/**
 * Classe de base des entr�es pour le classement des r�sultats des voeux des �l�ves.
 * 
 * <p>
 * Les informations sur l'�l�ve et son voeu sont extraites � l'instanciation de l'entr�e. On prend toutes les
 * informations n�cessaires au classement sans garder de r�f�rence aux donn�es. Seule la r�f�rence au contexte de
 * classement est gard�e mais et il ne doit pas lui-m�me contenir des r�f�rences aux donn�es.
 * </p>
 */
public abstract class EntreeClassement implements Comparable<EntreeClassement> {

    /** Valeur indiquant que l'objet courant (receveur, ou this) est � classer en premier. */
    public static final int RECEVEUR_CLASSE_EN_PREMIER = -1;

    /** Valeur indiquant que l'objet pass� en param�tre est � classer en premier. */
    public static final int PARAMETRE_CLASSE_EN_PREMIER = 1;

    /** Identifiant national de l'�l�ve ayant formul� le voeu. */
    private String ine;

    /** Code de l'offre de formation concern�e. */
    private String codeOffreFormation;

    /** Le rang du voeu. */
    private int rang;

    /** Le num�ro du tour dans lequel le voeu a �t� formul�. */
    private short numeroTour;

    /** Horodatage de l'enregistrement du voeu (ou � d�faut la date courante). */
    private Date horodatage;

    /** Flag indiquant si le voeu de l'�l�ve est forc� refus�. */
    private String flagRefusVoeu;

    /** La valeur g�n�r�e pour le voeu et utiliser pour d�partager des voeux avec un bar�me �gal. */
    private int valeurDepartage;

    /**
     * @param resultatsProvisoiresOpa
     *            le r�sultat du voeu de l'�l�ve � classer
     */
    public EntreeClassement(ResultatsProvisoiresOpa resultatsProvisoiresOpa) {

        VoeuElevePK id = resultatsProvisoiresOpa.getId();
        this.ine = id.getIne();
        this.rang = id.getRang();
        this.numeroTour = id.getNumeroTour();

        VoeuEleve voeuEleve = resultatsProvisoiresOpa.getVoeuEleve();

        this.codeOffreFormation = voeuEleve.getVoeu().getCode();

        // On prend l'horodatage de la candidature ou � d�faut la date courante
        this.horodatage = voeuEleve.getCandidature().getHorodatage();
        if (this.horodatage == null) {
            this.horodatage = new Date();
        }

        this.flagRefusVoeu = voeuEleve.getFlagRefusVoeu();

        this.valeurDepartage = voeuEleve.getValeurDepartage();
    }

    /** @return l'identifiant national de l'�l�ve */
    public String getIne() {
        return ine;
    }

    /**
     * @return codeOffreFormation
     */
    public String getCodeOffreFormation() {
        return codeOffreFormation;
    }

    /** @return Le rang du voeu. */
    public int getRang() {
        return rang;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof EntreeClassement) {

            EntreeClassement autre = (EntreeClassement) obj;
            return new EqualsBuilder().append(this.ine, autre.ine).append(this.numeroTour, autre.numeroTour)
                    .append(this.rang, autre.rang).isEquals();

        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ine).append(rang).append(numeroTour).toHashCode();
    }

    @Override
    public String toString() {
        return new StringBuffer().append("tour : ").append(numeroTour).append(", ine : ").append(ine)
                .append(", rang : ").append(rang).toString();
    }

    /** @return vrai si le voeu doit �tre retir� du classement, sinon faux */
    public boolean estExclusClassement() {

        // On exclut du classement les voeux �l�ve refus�s individuellement (FL_REFVOE='O')
        return Flag.OUI.equals(flagRefusVoeu);
    }

    /**
     * Compare l'entr�e courante de classement avec une autre selon le crit�re de leur valeur de d�partage
     * al�atoire.
     * 
     * @param autreEntree
     *            l'autre entr�e de classement
     * @return -1 si l'entr�e courante est la plus petite strictement (class�e en premier), 1 sinon
     */
    protected int compareToByDefault(EntreeClassement autreEntree) {

        // On utilise la valeur de d�partage g�n�r�e al�atoirement pour classer les voeux de bar�mes �gaux
        if (valeurDepartage > autreEntree.valeurDepartage) {
            return RECEVEUR_CLASSE_EN_PREMIER;
        } else if (valeurDepartage < autreEntree.valeurDepartage) {
            return PARAMETRE_CLASSE_EN_PREMIER;
        } else {
            // En cas d'�galit� on regarde l'horodatage
            Date oHorodatage = autreEntree.horodatage;

            // On prend le premier selon l'horodatage (i.e le plus vieux)
            if (oHorodatage.after(horodatage)) {
                return RECEVEUR_CLASSE_EN_PREMIER;
            } else {
                return PARAMETRE_CLASSE_EN_PREMIER;
            }
        }
    }
}
