/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;

/** Entr�e de classement pour un r�sultat de voeu d'�l�ve trait� avec bar�me. */
public class EntreeClassementBareme extends EntreeClassement {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(EntreeClassementBareme.class);

    /** Le bar�me. */
    private double bareme;

    /**
     * L'indicateur de s�curisation.
     */
    private String flagSecurisation;

    /**
     * @param resultatsProvisoiresOpa
     *            le r�sultat correspondant au voeu de l'�l�ve � classer
     */
    public EntreeClassementBareme(ResultatsProvisoiresOpa resultatsProvisoiresOpa) {
        super(resultatsProvisoiresOpa);

        // bareme (0 si null)
        if (resultatsProvisoiresOpa.getBareme() != null) {
            this.bareme = resultatsProvisoiresOpa.getBareme().doubleValue();
        } else {
            this.bareme = 0;
        }

        flagSecurisation = resultatsProvisoiresOpa.getFlagSecurisation();
    }

    @Override
    public String toString() {
        return new StringBuffer().append(super.toString()).append(", bar�me : ").append(bareme).toString();
    }

    @Override
    public int compareTo(EntreeClassement autre) {

        if (!(autre instanceof EntreeClassementBareme)) {

            LOG.error("Entr�e de classement incoh�rente pour une offre avec bar�me : " + autre.toString() + " - "
                    + autre.getClass());
            throw new IllegalArgumentException("Le classement n'est pas coh�rent vis � vis du type d'offre");
        }

        EntreeClassementBareme autreEntreeClassement = (EntreeClassementBareme) autre;

        if (this == autreEntreeClassement) {
            return 0;
        }

        double oBareme = autreEntreeClassement.bareme;

        if (Flag.OUI.equals(flagSecurisation) && Flag.NON.equals(autreEntreeClassement.flagSecurisation)) {
            // Si l'appelant est s�curis� il est prioritaire
            return RECEVEUR_CLASSE_EN_PREMIER;
        } else if (Flag.NON.equals(flagSecurisation) && Flag.OUI.equals(autreEntreeClassement.flagSecurisation)) {
            // Si le param�tre est s�curis� il est prioritaire
            return PARAMETRE_CLASSE_EN_PREMIER;
        } else {
            // Dans le cas o� les deux voeux sont s�curis�s ou ne le sont pas, on compare les bar�mes
            if (oBareme < bareme) {

                // L'autre entr�e � un bar�me plus petit
                return RECEVEUR_CLASSE_EN_PREMIER;

            } else if (oBareme == bareme) {

                // A bar�me �gal, on prend les crit�res par d�faut (valeur al�atoire, horodatage ...)
                return compareToByDefault(autreEntreeClassement);
            }

            // l'autre gagne
            return PARAMETRE_CLASSE_EN_PREMIER;
        }
    }
}
