/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;

/** Entr�e de classement pour un r�sultat provisoire associ� � un voeu d'�l�ve trait� en commission. */
public class EntreeClassementCommission extends EntreeClassement {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(EntreeClassementCommission.class);

    /** La d�cision saisie en commission. */
    private DecisionCommission decisionCommission;

    /** Le num�ro de liste compl�mentaire saisi en commission. */
    private double numeroListeComplementaireCommission;

    /**
     * @param resultatsProvisoiresOpa
     *            le r�sultat correspondant au r�sultat du voeu de l'�l�ve � classer
     */
    public EntreeClassementCommission(ResultatsProvisoiresOpa resultatsProvisoiresOpa) {
        super(resultatsProvisoiresOpa);

        // On examine le voeu de l'�l�ve pour r�cup�rer les d�cisions prises en commission
        VoeuEleve voeuEleve = resultatsProvisoiresOpa.getVoeuEleve();

        // D�cision en commission
        this.decisionCommission = DecisionCommission.getPourCode(voeuEleve.getCodeDecisionProvisoire());

        // Num�ro de lise compl�mentaire (0 si null)
        this.numeroListeComplementaireCommission = 0;
        if (voeuEleve.getNumeroListeSuppProvisoire() != null) {
            this.numeroListeComplementaireCommission = voeuEleve.getNumeroListeSuppProvisoire().doubleValue();
        }

    }

    @Override
    public String toString() {
        return new StringBuffer().append(super.toString()).append(", Code d�cision : ").append(decisionCommission)
                .append(", Num. LS : ").append(numeroListeComplementaireCommission).toString();
    }

    /**
     * @return vrai si la d�cision prise en commission pour le voeu est � exclure du classement (REFUSE,
     *         NON_TRAITE, RECENSEMENT ou ABSENT)
     */
    protected boolean estDecisionCommissionAExclure() {
        return decisionCommission == DecisionCommission.NON_TRAITE
                || decisionCommission == DecisionCommission.REFUSE
                || decisionCommission == DecisionCommission.RECENSEMENT
                || decisionCommission == DecisionCommission.DOSSIER_ABSENT;
    }

    /** @return vrai si le voeu doit �tre retir� du classement, sinon faux */
    @Override
    public boolean estExclusClassement() {

        // On exclut aussi du classement les offres trait�es en commission,
        // les voeux d'�l�ves ayant des d�cisions non classables
        return super.estExclusClassement() || estDecisionCommissionAExclure();
    }

    @Override
    public int compareTo(EntreeClassement vo) {

        if (!(vo instanceof EntreeClassementCommission)) {

            LOG.error("Entr�e de classement incoh�rente pour une offre trait�e commission : " + vo.toString()
                    + " - " + vo.getClass());
            throw new IllegalArgumentException("Le classement n'est pas coh�rent vis � vis du type d'offre");

        }

        EntreeClassementCommission o = (EntreeClassementCommission) vo;

        if (this == o) {
            return 0;
        }

        DecisionCommission oDecisionCommission = o.decisionCommission;
        double oNumeroListeComplementaire = o.numeroListeComplementaireCommission;

        // Classement sans bar�me => les voeux ont �t� trait�s en commission

        // comparaison des codeDecision provisoire
        if (decisionCommission == DecisionCommission.PRIS && oDecisionCommission != DecisionCommission.PRIS) {

            // L'appelant a �t� pris (l'autre ne l'est pas)
            return RECEVEUR_CLASSE_EN_PREMIER;

        } else if (decisionCommission != DecisionCommission.PRIS
                && oDecisionCommission == DecisionCommission.PRIS) {

            // L'autre a �t� pris (l'appelant ne l'est pas)
            return PARAMETRE_CLASSE_EN_PREMIER;

        } else if (decisionCommission == DecisionCommission.COMPLEMENTAIRE
                && oDecisionCommission != DecisionCommission.COMPLEMENTAIRE) {

            // L'appelant est sur liste suppl�mentaire (l'autre ne l'est pas)
            return RECEVEUR_CLASSE_EN_PREMIER;

        } else if (decisionCommission != DecisionCommission.COMPLEMENTAIRE
                && oDecisionCommission == DecisionCommission.COMPLEMENTAIRE) {

            // L'autre est sur liste suppl�mentaire (l'appelant ne l'est pas)
            return PARAMETRE_CLASSE_EN_PREMIER;

        } else {

            // comparaison des num liste supp provisoire
            if (oNumeroListeComplementaire > numeroListeComplementaireCommission) {
                return RECEVEUR_CLASSE_EN_PREMIER;

            } else if (oNumeroListeComplementaire == numeroListeComplementaireCommission) {

                return compareToByDefault(o);

            }
        }

        // l'autre gagne
        return PARAMETRE_CLASSE_EN_PREMIER;
    }
}
