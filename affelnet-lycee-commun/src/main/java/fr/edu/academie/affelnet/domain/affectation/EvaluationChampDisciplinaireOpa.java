/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Contient le montant de points (liss�s ou non) associ�s � un champs disciplinaire pour un �l�ve et une OPA.
 */
public class EvaluationChampDisciplinaireOpa {
    /** Id composite. */
    private EvaluationChampDisciplinaireOpaPK id;

    /**
     * �l�ve auquel appartient les points.
     */
    private Eleve eleve;

    /**
     * Op�ration programm�e � laquelle sont li�s les points.
     */
    private OperationProgrammeeAffectation opa;

    /**
     * Le champs disciplinaire �valu�.
     */
    private ChampDisciplinaire champDisciplinaire;

    /**
     * La moyenne des points pour le champs disciplinaire.
     */
    private Double valeurMoyennePoints;

    /**
     * la moyenne des points pour le champs disciplinaire apr�s l'harmonisation.
     */
    private Double valeurMoyennePointsHarmo;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationChampDisciplinaireOpa() {
        super();
    }

    /**
     * Constructeur avec des valeurs.
     * 
     * @param eleve
     *            �l�ve concern�
     * @param idOpa
     *            id de l'opa
     * @param champDisciplinaire
     *            le champs disciplinaire �valu�
     */
    public EvaluationChampDisciplinaireOpa(Eleve eleve, Long idOpa, ChampDisciplinaire champDisciplinaire) {
        this();
        this.eleve = eleve;
        this.champDisciplinaire = champDisciplinaire;
        this.id = new EvaluationChampDisciplinaireOpaPK(eleve.getIne(), idOpa, champDisciplinaire.getCode());
    }

    /**
     * @return the id
     */
    public EvaluationChampDisciplinaireOpaPK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(EvaluationChampDisciplinaireOpaPK id) {
        this.id = id;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the opa
     */
    public OperationProgrammeeAffectation getOpa() {
        return opa;
    }

    /**
     * @param opa
     *            the opa to set
     */
    public void setOpa(OperationProgrammeeAffectation opa) {
        this.opa = opa;
    }

    /**
     * @return the champDisciplinaire
     */
    public ChampDisciplinaire getChampDisciplinaire() {
        return champDisciplinaire;
    }

    /**
     * @param champDisciplinaire
     *            the champDisciplinaire to set
     */
    public void setChampDisciplinaire(ChampDisciplinaire champDisciplinaire) {
        this.champDisciplinaire = champDisciplinaire;
    }

    /**
     * @return the valeurMoyennePoints
     */
    public Double getValeurMoyennePoints() {
        return valeurMoyennePoints;
    }

    /**
     * @param valeurMoyennePoints
     *            the valeurMoyennePoints to set
     */
    public void setValeurMoyennePoints(Double valeurMoyennePoints) {
        this.valeurMoyennePoints = valeurMoyennePoints;
    }

    /**
     * @return the valeurMoyennePointsHarmo
     */
    public Double getValeurMoyennePointsHarmo() {
        return valeurMoyennePointsHarmo;
    }

    /**
     * @param valeurMoyennePointsHarmo
     *            the valeurMoyennePointsHarmo to set
     */
    public void setValeurMoyennePointsHarmo(Double valeurMoyennePointsHarmo) {
        this.valeurMoyennePointsHarmo = valeurMoyennePointsHarmo;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EvaluationChampDisciplinaireOpa)) {
            return false;
        }
        EvaluationChampDisciplinaireOpa castOther = (EvaluationChampDisciplinaireOpa) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }
}
