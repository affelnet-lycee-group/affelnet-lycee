/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Cl� primaire pour la classe EvaluationChampDisciplinaireOpa.
 */
public class EvaluationChampDisciplinaireOpaPK implements Serializable {
    /** default Id. */
    private static final long serialVersionUID = 1L;

    /** Id de l'�l�ve. */
    private String ine;

    /** Id de l'opa. */
    private Long idOpa;

    /** Id du champ disciplinaire. */
    private String codeChampDisciplinaire;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationChampDisciplinaireOpaPK() {
        super();
    }

    /**
     * Constructeur avec l'ensemble des param�tres.
     * 
     * @param ine
     *            Ine de l'�l�ve
     * @param idOpa
     *            Id de l'opa
     * @param codeChampDisciplinaire
     *            Code du champs disciplinaire
     */
    public EvaluationChampDisciplinaireOpaPK(String ine, Long idOpa, String codeChampDisciplinaire) {
        this();
        this.ine = ine;
        this.idOpa = idOpa;
        this.codeChampDisciplinaire = codeChampDisciplinaire;
    }

    /**
     * @return the ine
     */
    public String getIne() {
        return ine;
    }

    /**
     * @param ine
     *            the ine to set
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    /**
     * @return the idOpa
     */
    public Long getIdOpa() {
        return idOpa;
    }

    /**
     * @param idOpa
     *            the idOpa to set
     */
    public void setIdOpa(Long idOpa) {
        this.idOpa = idOpa;
    }

    /**
     * @return the codeChampDisciplinaire
     */
    public String getCodeChampDisciplinaire() {
        return codeChampDisciplinaire;
    }

    /**
     * @param codeChampDisciplinaire
     *            the codeChampDisciplinaire to set
     */
    public void setCodeChampDisciplinaire(String codeChampDisciplinaire) {
        this.codeChampDisciplinaire = codeChampDisciplinaire;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIne()).append(getIdOpa()).append(getCodeChampDisciplinaire())
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EvaluationChampDisciplinaireOpaPK)) {
            return false;
        }
        EvaluationChampDisciplinaireOpaPK castOther = (EvaluationChampDisciplinaireOpaPK) other;
        return new EqualsBuilder().append(this.getIne(), castOther.getIne())
                .append(this.getIdOpa(), castOther.getIdOpa())
                .append(this.getCodeChampDisciplinaire(), castOther.getCodeChampDisciplinaire()).isEquals();
    }
}
