/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Contient la valeur des points harmonis�s d'une �valuation compl�mentaire pour un �l�ve dans le cadre d'une OPA.
 */
public class EvaluationComplementaireOpa {
    /** Id composite. */
    private EvaluationComplementaireOpaPK id;

    /** �l�ve �valu�. */
    private Eleve eleve;

    /** Op�ration programm�e dans laquelle se fait le processus d'effactation. */
    private OperationProgrammeeAffectation opa;

    /** �valuation associ�e. */
    private EvaluationComplementaire evaluationComplementaire;

    /** Valeur des points apr�s le processus d'harmonisation. */
    private double valeurPointsHarmo;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationComplementaireOpa() {
        super();
    }

    /**
     * Constructeur avec les �l�ments pour l'id composite.
     * 
     * @param eleve
     *            �l�ve �valu�
     * @param idOpa
     *            OPA concern�e
     * @param evalComp
     *            �valuation compl�mentaire associ�e
     */
    public EvaluationComplementaireOpa(Eleve eleve, long idOpa, EvaluationComplementaire evalComp) {
        this.id = new EvaluationComplementaireOpaPK(eleve.getIne(), idOpa, evalComp.getId());
        this.eleve = eleve;
        this.evaluationComplementaire = evalComp;
    }

    /**
     * @return the id
     */
    public EvaluationComplementaireOpaPK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(EvaluationComplementaireOpaPK id) {
        this.id = id;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the opa
     */
    public OperationProgrammeeAffectation getOpa() {
        return opa;
    }

    /**
     * @param opa
     *            the opa to set
     */
    public void setOpa(OperationProgrammeeAffectation opa) {
        this.opa = opa;
    }

    /**
     * @return the evaluationComplementaire
     */
    public EvaluationComplementaire getEvaluationComplementaire() {
        return evaluationComplementaire;
    }

    /**
     * @param evaluationComplementaire
     *            the evaluationComplementaire to set
     */
    public void setEvaluationComplementaire(EvaluationComplementaire evaluationComplementaire) {
        this.evaluationComplementaire = evaluationComplementaire;
    }

    /**
     * @return the valeurPointsHarmo
     */
    public double getValeurPointsHarmo() {
        return valeurPointsHarmo;
    }

    /**
     * @param valeurPointsHarmo
     *            the valeurPointsHarmo to set
     */
    public void setValeurPointsHarmo(double valeurPointsHarmo) {
        this.valeurPointsHarmo = valeurPointsHarmo;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EvaluationComplementaireOpa)) {
            return false;
        }
        EvaluationComplementaireOpa castOther = (EvaluationComplementaireOpa) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }
}
