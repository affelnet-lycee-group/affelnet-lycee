/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Valeur de la note harmonis�e d'un rang pour un �l�ve dans el cadre d'une OPA.
 */
public class NoteOpa {
    /** Id composite. */
    private NoteOpaPK id;

    /** �l�ve not�. */
    private Eleve eleve;

    /** Operation programm�e dans laquelle se fait le processus d'affectation. */
    private OperationProgrammeeAffectation opa;

    /** Rang de la note. */
    private Rang rang;

    /** Valeur de la note harmonis�e. */
    private double valeurNoteHarmo;

    /**
     * Constructeur par d�faut.
     */
    public NoteOpa() {
        super();
    }

    /**
     * Constructeur avec les param�tres de l'ID.
     * 
     * @param eleve
     *            �l�ve �valu�
     * @param idOpa
     *            Id de l'OPA
     * @param rang
     *            Rang de la note
     */
    public NoteOpa(Eleve eleve, long idOpa, Rang rang) {
        this();
        this.id = new NoteOpaPK(eleve.getIne(), idOpa, rang.getId());
        this.eleve = eleve;
        this.rang = rang;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @return the id
     */
    public NoteOpaPK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(NoteOpaPK id) {
        this.id = id;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the opa
     */
    public OperationProgrammeeAffectation getOpa() {
        return opa;
    }

    /**
     * @param opa
     *            the opa to set
     */
    public void setOpa(OperationProgrammeeAffectation opa) {
        this.opa = opa;
    }

    /**
     * @return the rang
     */
    public Rang getRang() {
        return rang;
    }

    /**
     * @param rang
     *            the rang to set
     */
    public void setRang(Rang rang) {
        this.rang = rang;
    }

    /**
     * @return the valeurNoteHarmo
     */
    public double getValeurNoteHarmo() {
        return valeurNoteHarmo;
    }

    /**
     * @param valeurNoteHarmo
     *            the valeurNoteHarmo to set
     */
    public void setValeurNoteHarmo(double valeurNoteHarmo) {
        this.valeurNoteHarmo = valeurNoteHarmo;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof NoteOpa)) {
            return false;
        }
        NoteOpa castOther = (NoteOpa) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

}
