/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Cl� primaire pour la classe NoteOpa.
 */
public class NoteOpaPK implements Serializable {
    /** default Id. */
    private static final long serialVersionUID = 1L;

    /** Id de l'�l�ve. */
    private String ine;

    /** Id de l'opa. */
    private Long idOpa;

    /** Id du rang. */
    private Long idRang;

    /**
     * Constructeur par d�faut.
     */
    public NoteOpaPK() {
        super();
    }

    /**
     * Constructeur avec les �l�ments de l'id.
     * 
     * @param ine
     *            Ine de l'�l�ve
     * @param idOpa
     *            Id de l'OPA
     * @param idRang
     *            Id du rang
     */
    public NoteOpaPK(String ine, long idOpa, long idRang) {
        this();
        this.ine = ine;
        this.idOpa = idOpa;
        this.idRang = idRang;
    }

    /**
     * @return the ine
     */
    public String getIne() {
        return ine;
    }

    /**
     * @param ine
     *            the ine to set
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    /**
     * @return the idOpa
     */
    public Long getIdOpa() {
        return idOpa;
    }

    /**
     * @param idOpa
     *            the idOpa to set
     */
    public void setIdOpa(Long idOpa) {
        this.idOpa = idOpa;
    }

    /**
     * @return the idRang
     */
    public Long getIdRang() {
        return idRang;
    }

    /**
     * @param idRang
     *            the idRang to set
     */
    public void setIdRang(Long idRang) {
        this.idRang = idRang;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIne()).append(getIdOpa()).append(getIdRang()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof NoteOpaPK)) {
            return false;
        }
        NoteOpaPK castOther = (NoteOpaPK) other;
        return new EqualsBuilder().append(this.getIne(), castOther.getIne())
                .append(this.getIdOpa(), castOther.getIdOpa()).append(this.getIdRang(), castOther.getIdRang())
                .isEquals();
    }
}
