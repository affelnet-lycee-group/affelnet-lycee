/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.affectation;

import java.io.Serializable;
import java.util.Comparator;
import java.util.EnumMap;

import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;

/**
 * Comparateur de d�cisions finale. Ordonne les d�cisions finale avec en premi�re celle qui impacte la candidature.
 */
public class VoeuEleveDecisionFinaleComparateur implements Comparator<VoeuEleve>, Serializable {
    /** Id par d�faut. */
    private static final long serialVersionUID = 1L;

    /** Si on traite les voeux class�s ou non. */
    private boolean voeuxClasses = true;

    /** Valeurs de comparaison des d�cisions finale lorsqu'on prend en compte les voeux class�s. */
    private EnumMap<DecisionFinale, Integer> valeurDecisionVoeuxClasses = new EnumMap<>(DecisionFinale.class);

    /**
     * Constructeur par d�faut.
     */
    public VoeuEleveDecisionFinaleComparateur() {
        super();
        valeurDecisionVoeuxClasses.put(DecisionFinale.AFFECTE, -3);
        valeurDecisionVoeuxClasses.put(DecisionFinale.LISTE_SUPP, -2);
        valeurDecisionVoeuxClasses.put(DecisionFinale.REFUSE, -1);
        valeurDecisionVoeuxClasses.put(DecisionFinale.ABANDON, 0);
        valeurDecisionVoeuxClasses.put(DecisionFinale.EN_ATTENTE_SIGNATURE_CONTRAT, -2);
        valeurDecisionVoeuxClasses.put(DecisionFinale.ADMIS_CONTRAT_SIGNE, -3);
        valeurDecisionVoeuxClasses.put(DecisionFinale.RECENSEMENT, 0);
        valeurDecisionVoeuxClasses.put(DecisionFinale.DOSSIER_ABSENT, 1);
        valeurDecisionVoeuxClasses.put(DecisionFinale.NON_TRAITE, 1);
    }

    /**
     * Constructeur du comparateur en indiquant si on souhaite prendre en compte les voeux class�s.
     * 
     * @param voeuxClasses
     *            si on veut prendre en compte les voeux class�s
     */
    public VoeuEleveDecisionFinaleComparateur(boolean voeuxClasses) {
        this();
        this.voeuxClasses = voeuxClasses;
    }

    @Override
    public int compare(VoeuEleve o1, VoeuEleve o2) {
        DecisionFinale decisionVoeu1 = DecisionFinale.getPourCode(o1.getCodeDecisionFinal());
        DecisionFinale decisionVoeu2 = DecisionFinale.getPourCode(o2.getCodeDecisionFinal());

        if (decisionVoeu1 == null && decisionVoeu2 != null) {
            return 1;
        } else if (decisionVoeu1 != null && decisionVoeu2 == null) {
            return -1;
        } else if (decisionVoeu1 != null) {
            int valeurVoeu1 = valeurDecisionVoeuxClasses.get(decisionVoeu1)
                    * (o1.getVoeu().estReclassementApprentissage() != voeuxClasses ? 10 : 1);
            int valeurVoeu2 = valeurDecisionVoeuxClasses.get(decisionVoeu2)
                    * (o2.getVoeu().estReclassementApprentissage() != voeuxClasses ? 10 : 1);

            if (valeurVoeu1 - valeurVoeu2 != 0) {
                return valeurVoeu1 - valeurVoeu2;
            }
        }

        return o1.getId().getRang() - o2.getId().getRang();
    }

    /**
     * @return the voeuxClasses
     */
    public boolean isVoeuxClasses() {
        return voeuxClasses;
    }

    /**
     * @param voeuxClasses
     *            the voeuxClasses to set
     */
    public void setVoeuxClasses(boolean voeuxClasses) {
        this.voeuxClasses = voeuxClasses;
    }
}
