/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

import java.io.Serializable;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EchelonEvaluation;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Classe permettant d'acc�der au information des �valuations compl�mentaires
 * que l'on retrouve dans la table GV_EVAL_COMPLEMENTAIRE.
 */
public class EvaluationComplementaireEleve implements Serializable, EvaluationEleve {

    /**
     * Tag par s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Valeur du couple formant la primary key.
     */
    private EvaluationComplementaireElevePK evaluationComplementaireElevePK;

    /**
     * Echelon d'�valuation.
     */
    private EchelonEvaluation echelon;

    /** El�ve auquel appartient l'�valuation. */
    private Eleve eleve;

    /** Code de la comp�tence. */
    private EvaluationComplementaire evaluationComplementaire;

    /**
     * M�thode permettant de r�cup�rer l'�chelon de l'�valuation.
     * 
     * @return l'�chelon d'�valuation
     */
    public EchelonEvaluation getEchelon() {
        return echelon;
    }

    /**
     * M�thode mettant � jour l'�chelon de l'�valuation.
     * 
     * @param echelon
     *            l'�chelon d'�valuation
     */
    public void setEchelon(EchelonEvaluation echelon) {
        this.echelon = echelon;
    }

    /**
     * M�thode redonnant le couple de primaryKey pour l'�valuation compl�mentaires d'un �l�ve.
     * 
     * @return le couple de primaryKey pour l'�valuation compl�mentaire d'un �l�ve
     */
    public EvaluationComplementaireElevePK getEvaluationComplementaireElevePK() {
        return evaluationComplementaireElevePK;
    }

    /**
     * M�thode mettant � jour le couple de la primaryKey de l'�valuation compl�mentaire d'un �l�ve.
     * 
     * @param evaluationComplementaireElevePK
     *            le couple de primarykey pour l'�valuation compl�mentaire de l'�l�ve
     */
    public void setEvaluationComplementaireElevePK(
            EvaluationComplementaireElevePK evaluationComplementaireElevePK) {
        this.evaluationComplementaireElevePK = evaluationComplementaireElevePK;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the evaluationComplementaire
     */
    public EvaluationComplementaire getEvaluationComplementaire() {
        return evaluationComplementaire;
    }

    /**
     * @param evaluationComplementaire
     *            the evaluationComplementaire to set
     */
    public void setEvaluationComplementaire(EvaluationComplementaire evaluationComplementaire) {
        this.evaluationComplementaire = evaluationComplementaire;
    }

    @Override
    public String getLibelle() {
        return evaluationComplementaire.getLibelle();
    }

    @Override
    public Double getPoints() {
        Double points = null;
        if (echelon != null) {
            points = (double) echelon.getValeurPoint();
        }
        return points;
    }

    @Override
    public String affichagePositionnement() {
        if (echelon != null) {
            return echelon.getLibelle() + " (" + echelon.getValeurPoint() + " pts)";
        }
        return LIBELLE_ABSENCE_DISPENSE_NON_EVAL;
    }

}
