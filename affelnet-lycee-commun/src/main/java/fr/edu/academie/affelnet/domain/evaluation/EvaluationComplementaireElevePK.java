/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Classe permettant d'obtenir le couple ine/id �tant le primary key de EvaluationComplementaireEleve.
 */
public class EvaluationComplementaireElevePK implements Serializable {

    /**
     * Tag de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * INE de l'�l�ve.
     */
    private String ine;

    /**
     * ID de l'�valuation compl�mentaire.
     */
    private Long id;

    /** Constructeur vide. */
    public EvaluationComplementaireElevePK() {
    }

    /**
     * Cr�ation d'une cl� identifiante pour l'�valuation compl�mentaire d'un �l�ve.
     * 
     * @param ine
     *            de l'�l�ve
     * @param id
     *            de l'�valuation compl�mentaire
     */
    public EvaluationComplementaireElevePK(String ine, Long id) {
        this.id = id;
        this.ine = ine;
    }

    /**
     * M�thode permettant de r�cup�rer INE de l'�l�ve.
     * 
     * @return INE de l'�l�ve
     */
    public String getIne() {
        return ine;
    }

    /**
     * M�thode mettant � jour le lien entre les �valuations compl�mentaire et un �l�ve.
     * 
     * @param ine
     *            on identifie l'�l�ve
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    /**
     * M�thode permettant de r�cup�rer l'ID de l'�valuation compl�mentaire.
     * 
     * @return l'identifiant de l'�valuation compl�mentaire
     */
    public Long getId() {
        return id;
    }

    /**
     * M�thode mettant � jour l'identifiant de l'�valuation compl�mentaire.
     * 
     * @param id
     *            l'�valuation compl�mentaire
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("ine", getIne()).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (!(other instanceof EvaluationComplementaireElevePK)) {
            return false;
        }
        EvaluationComplementaireElevePK castOther = (EvaluationComplementaireElevePK) other;
        return new EqualsBuilder().append(this.getIne(), castOther.getIne())
                .append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIne()).append(getId()).toHashCode();
    }

}
