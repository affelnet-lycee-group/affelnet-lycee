/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Evaluation d'une discipline pour un �l�ve.
 */
public class EvaluationDiscipline implements EvaluationEleve {

    /** Valeur minimale de points pour l'�valuation d'une discipline. */
    public static final int VALEUR_POINTS_MINIMALE = 3;

    /** Valeur maximale de points pour l'�valuation d'une discipline. */
    public static final int VALEUR_POINTS_MAXIMALE = 16;

    /** Cl� primaire compos�e. */
    private EvaluationDisciplinePK id;

    /** Nombre de point apport� par l'�valuation. */
    private double moyenne;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationDiscipline() {
        super();
    }

    /**
     * Constructeur avec l'ensemble des param�tres.
     * 
     * @param eleve
     *            �l�ve auquel appartient l'�valuation
     * @param discipline
     *            Discipline �valu�e
     * @param moyenne
     *            Moyenne des points
     */
    public EvaluationDiscipline(Eleve eleve, Discipline discipline, double moyenne) {
        this();
        this.moyenne = moyenne;
        this.id = new EvaluationDisciplinePK(eleve, discipline);
    }

    /**
     * @return the id
     */
    public EvaluationDisciplinePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(EvaluationDisciplinePK id) {
        this.id = id;
    }

    /**
     * @return the moyenne
     */
    public double getMoyenne() {
        return moyenne;
    }

    /**
     * @param moyenne
     *            the moyenne to set
     */
    public void setMoyenne(double moyenne) {
        this.moyenne = moyenne;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return id.getEleve();
    }

    /**
     * @return the discipline
     */
    public Discipline getDiscipline() {
        return id.getDiscipline();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EvaluationDiscipline)) {
            return false;
        }
        EvaluationDiscipline castOther = (EvaluationDiscipline) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("MoyennePoints(");
        sb.append(getEleve().getIne());
        sb.append("; ");
        sb.append(getDiscipline());
        sb.append(")=");
        sb.append(moyenne);
        return sb.toString();
    }

    /**
     * Teste qu'une moyenne de discipline est dans l'intervalle de validit�.
     * 
     * @param moyennePoints
     *            la moyenne � tester
     * @return vrai si la moyenne est valide, sinon faux
     */
    public static boolean estMoyenneValide(double moyennePoints) {
        return moyennePoints >= VALEUR_POINTS_MINIMALE && moyennePoints <= VALEUR_POINTS_MAXIMALE;
    }

    /** @return cha�ne d�crivant l'intervalle de validit� d'une �valuation de discipline en points. */
    public static String chaineIntervalleEvaluationPoints() {
        StringBuffer sbIntervalle = new StringBuffer();
        sbIntervalle.append("[");
        sbIntervalle.append(VALEUR_POINTS_MINIMALE);
        sbIntervalle.append("; ");
        sbIntervalle.append(VALEUR_POINTS_MAXIMALE);
        sbIntervalle.append("]");
        return sbIntervalle.toString();
    }

    @Override
    public String getLibelle() {
        return this.getDiscipline().getMatiere().getLibelleCourt();
    }

    @Override
    public Double getPoints() {
        return moyenne;
    }

    @Override
    public String affichagePositionnement() {
        if (moyenne != 0) {
            return moyenne + " points";
        }
        return LIBELLE_ABSENCE_DISPENSE_NON_EVAL;
    }
}
