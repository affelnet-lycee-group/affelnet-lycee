/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Cl� primaire associ�e � l'�valuation d'une discipline.
 */
public class EvaluationDisciplinePK implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** �l�ve auquel appartient l'�valuation. */
    private Eleve eleve;

    /** Discipline �valu�e. */
    private Discipline discipline;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationDisciplinePK() {
        super();
    }

    /**
     * Constructeur avec l'ensemble des param�tres.
     * 
     * @param eleve
     *            �l�ve �valu�.
     * @param discipline
     *            Discipline �valu�e.
     */
    public EvaluationDisciplinePK(Eleve eleve, Discipline discipline) {
        this.eleve = eleve;
        this.discipline = discipline;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the discipline
     */
    public Discipline getDiscipline() {
        return discipline;
    }

    /**
     * @param discipline
     *            the discipline to set
     */
    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (!(other instanceof EvaluationDisciplinePK)) {
            return false;
        }
        EvaluationDisciplinePK castOther = (EvaluationDisciplinePK) other;
        return new EqualsBuilder().append(this.getEleve(), castOther.getEleve())
                .append(this.getDiscipline(), castOther.getDiscipline()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getEleve()).append(getDiscipline()).toHashCode();
    }
}
