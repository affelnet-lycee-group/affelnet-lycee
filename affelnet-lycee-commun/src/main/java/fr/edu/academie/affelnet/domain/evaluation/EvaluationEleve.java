/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

/**
 * Interface regroupant les �valuations.
 */
public interface EvaluationEleve {
    /** Cha�ne de caract�re affich�e si l'�valuation n'est pas �valu�e. */
    String LIBELLE_ABSENCE_DISPENSE_NON_EVAL = "Absence / dispense / non �valu�";

    /**
     * Renvoie le libell� de la comp�tence/mati�re/�valuation compl�mentaire associ�.
     * 
     * @return le libell� de l'�valu�
     */
    String getLibelle();

    /**
     * Renvoie le nombre de points de l'�valuation (null si non �valu�).
     * 
     * @return le nombre de points
     */
    Double getPoints();

    /**
     * Renvoie le degr� de ma�trise, l'�chelon ou le nombre de point pour affichage.
     * 
     * @return l'affichage de l'�valuation
     */
    String affichagePositionnement();
}
