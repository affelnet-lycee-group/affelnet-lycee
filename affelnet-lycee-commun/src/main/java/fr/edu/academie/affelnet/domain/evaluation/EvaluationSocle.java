/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DegreMaitrise;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Evaluation du socle pour un �l�ve.
 */
public class EvaluationSocle implements EvaluationEleve {
    /** id compos� du socle. */
    private EvaluationSoclePK id;

    /** Degr� de note de la comp�tence. */
    private DegreMaitrise degre;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationSocle() {
        super();
    }

    /**
     * @param degre
     *            degr� de note de la comp�tence
     * @param id
     *            id composite
     */
    public EvaluationSocle(EvaluationSoclePK id, DegreMaitrise degre) {
        this();
        this.degre = degre;
        this.id = id;
    }

    /**
     * @param eleve
     *            �l�ve auquel appartient l'�valuation
     * @param competence
     *            code de la comp�tence �valu�e
     * @param degre
     *            degr� de l'�valuation
     */
    public EvaluationSocle(Eleve eleve, CompetenceSocle competence, DegreMaitrise degre) {
        this(new EvaluationSoclePK(eleve, competence), degre);
    }

    /**
     * @return the degre
     */
    public DegreMaitrise getDegre() {
        return degre;
    }

    /**
     * @param degre
     *            the degre to set
     */
    public void setDegre(DegreMaitrise degre) {
        this.degre = degre;
    }

    /**
     * @return the id
     */
    public EvaluationSoclePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(EvaluationSoclePK id) {
        this.id = id;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return id.getEleve();
    }

    /**
     * @return the competence
     */
    public CompetenceSocle getCompetence() {
        return id.getCompetence();
    }

    @Override
    public String getLibelle() {
        return this.getCompetence().getLibelleCourt();
    }

    @Override
    public Double getPoints() {
        Double points = null;
        if (degre != null) {
            points = (double) degre.getPoints();
        }
        return points;
    }

    @Override
    public String affichagePositionnement() {
        if (degre != null) {
            return degre.getLibelle() + " (" + degre.getPoints() + " pts)";
        }
        return LIBELLE_ABSENCE_DISPENSE_NON_EVAL;
    }
}
