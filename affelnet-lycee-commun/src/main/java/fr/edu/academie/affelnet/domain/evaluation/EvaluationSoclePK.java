/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Cl� primaire d'une �valuation du socle.
 */
public class EvaluationSoclePK implements Serializable {
    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** El�ve auquel appartient le socle. */
    private Eleve eleve;

    /** Code de la comp�tence. */
    private CompetenceSocle competence;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationSoclePK() {
    }

    /**
     * Constructeur avec les param�tres pour compl�ter la cl�.
     * 
     * @param eleve
     *            El�ve auquel appartient le socle
     * @param competence
     *            code de la comp�tence
     */
    public EvaluationSoclePK(Eleve eleve, CompetenceSocle competence) {
        super();
        this.eleve = eleve;
        this.competence = competence;
    }

    /**
     * @return the eleve
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            the eleve to set
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the competence
     */
    public CompetenceSocle getCompetence() {
        return competence;
    }

    /**
     * @param competence
     *            the competence to set
     */
    public void setCompetence(CompetenceSocle competence) {
        this.competence = competence;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof EvaluationSoclePK)) {
            return false;
        }
        EvaluationSoclePK castOther = (EvaluationSoclePK) other;
        return new EqualsBuilder().append(this.getEleve(), castOther.getEleve())
                .append(this.getCompetence(), castOther.getCompetence()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getEleve()).append(getCompetence()).toHashCode();
    }
}
