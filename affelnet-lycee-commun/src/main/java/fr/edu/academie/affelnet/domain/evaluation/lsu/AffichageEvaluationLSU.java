/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.accueil.EtablissementNational;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DegreMaitrise;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;
import fr.edu.academie.affelnet.service.evaluation.EvaluationDisciplineManager;

/**
 * Classe pour l'affichage des �valuations et la liaison avec les nomenclatures.
 */
public class AffichageEvaluationLSU {

    /** Ensemble des �valuations du socle. */
    private SortedMap<CompetenceSocle, DegreMaitrise> socle;

    /** Les �valuations de discipline par p�riode. */
    private SortedSet<PeriodeLSU> periodes;

    /** Date d'int�gration des r�sultats. */
    private Date dateIntegration;

    /** Nom et INE de l'�l�ve. */
    private String nomEleve;

    /**
     * Constructeur par d�faut.
     */
    public AffichageEvaluationLSU() {
        super();
        socle = new TreeMap<CompetenceSocle, DegreMaitrise>();
        periodes = new TreeSet<PeriodeLSU>();
    }

    /**
     * G�n�re la map du socle � partir des �valuations et des nomenclatures.
     * 
     * @param socle
     *            les �valuations du socle issus de LSU
     * @param mapCompetence
     *            la liste des comp�tences
     * @param mapDegre
     *            les groupes de niveau
     */
    public void genererMapSocle(Set<EvaluationSocleLSU> socle, Map<String, CompetenceSocle> mapCompetence,
            Map<Integer, DegreMaitrise> mapDegre) {
        DegreMaitrise degre;
        CompetenceSocle competence;
        for (EvaluationSocleLSU evalSocle : socle) {
            competence = mapCompetence.get(evalSocle.getId().getCompetence());
            degre = mapDegre.get(evalSocle.getDegre());
            this.socle.put(competence, degre);
        }
    }

    /**
     * G�n�re les images des �valuations LSU.
     * 
     * @param bilans
     *            les bilans p�riodes issus de LSU
     * @param disciplines
     *            la liste des mati�res
     * @param niveaux
     *            les groupes de niveau
     * @return les �valuations dont le nombre de point n'a pas �t� attribu�.
     */
    public List<AffichageEvaluationDisciplineLSU> genererAffichageDiscipline(Set<EvaluationBilanPeriodeLSU> bilans,
            List<Discipline> disciplines, List<GroupeNiveau> niveaux) {
        Map<String, Discipline> mapDiscipline = new HashMap<String, Discipline>();
        Map<Integer, GroupeNiveau> mapNiveau = new HashMap<Integer, GroupeNiveau>();
        periodes = new TreeSet<PeriodeLSU>();

        for (Discipline discipline : disciplines) {
            mapDiscipline.put(discipline.getCodeMatiere(), discipline);
        }
        for (GroupeNiveau niveau : niveaux) {
            mapNiveau.put(niveau.getNiveau(), niveau);
        }

        PeriodeLSU periode;
        List<AffichageEvaluationDisciplineLSU> evalSansPoint = new ArrayList<AffichageEvaluationDisciplineLSU>();
        AffichageEvaluationDisciplineLSU evaluation;
        Discipline discipline;
        Integer valNiveau;
        long nbEval = 0;
        // On parcourt les bilans p�riodiques
        for (EvaluationBilanPeriodeLSU bilan : bilans) {
            periode = new PeriodeLSU(bilan);

            // On g�n�re par bilan les �valuations
            for (EvaluationDisciplineLSU evaluationLsu : bilan.getDisciplines()) {
                evaluation = new AffichageEvaluationDisciplineLSU(nbEval++);
                discipline = mapDiscipline.get(evaluationLsu.getCodeMat());
                valNiveau = evaluationLsu.getNiveau();

                evaluation.setValEval(evaluationLsu.getValeur());
                evaluation.setModalite(evaluationLsu.getModaliteElection());

                if (discipline != null) {
                    evaluation.setLibelleMatiere(discipline.getMatiere().getLibelleLong());
                    evaluation.setOrdre(discipline.getOrdre());

                    // On v�rifie que la modalit� d'�lection est accept�e
                    if (EvaluationDisciplineManager.MODALITE_ELECTION_ACCEPTEES
                            .contains(evaluationLsu.getModaliteElection())) {
                        if (valNiveau != null) {
                            evaluation.setLibelleGroupe(mapNiveau.get(valNiveau).getLibelle());
                            evaluation.setGroupeNiveau(mapNiveau.get(valNiveau));
                        } else {
                            evalSansPoint.add(evaluation);
                        }
                    } else {
                        evaluation.setModaliteIgnoree(true);
                    }
                }
                // Si il n'y a pas de discipline correspondante, on n'attribue pas de point
                else {
                    evaluation.setLibelleMatiere(evaluationLsu.getCodeMat());
                    evaluation.setOrdre(Integer.MAX_VALUE);
                    evaluation.setMatiereInconnue(true);
                }

                periode.addEvaluationsDiscipline(evaluation);
            }
            periodes.add(periode);
        }
        return evalSansPoint;
    }

    /**
     * @return the socle
     */
    public Map<CompetenceSocle, DegreMaitrise> getSocle() {
        return socle;
    }

    /**
     * @param socle
     *            the socle to set
     */
    public void setSocle(Map<CompetenceSocle, DegreMaitrise> socle) {
        this.socle = new TreeMap<CompetenceSocle, DegreMaitrise>();
        this.socle.putAll(socle);
    }

    /**
     * @return the periodes
     */
    public SortedSet<PeriodeLSU> getPeriodes() {
        return periodes;
    }

    /**
     * @param periodes
     *            the periodes to set
     */
    public void setPeriodes(SortedSet<PeriodeLSU> periodes) {
        this.periodes = periodes;
    }

    /**
     * Ajoute une p�riode.
     * 
     * @param periode
     *            p�riode ajout�e
     */
    public void addPeriode(PeriodeLSU periode) {
        this.periodes.add(periode);
    }

    /**
     * @return the dateIntegration
     */
    public Date getDateIntegration() {
        return dateIntegration;
    }

    /**
     * @param dateIntegration
     *            the dateIntegration to set
     */
    public void setDateIntegration(Date dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    /**
     * @param socle
     *            the socle to set
     */
    public void setSocle(SortedMap<CompetenceSocle, DegreMaitrise> socle) {
        this.socle = socle;
    }

    /**
     * @return the nomEleve
     */
    public String getNomEleve() {
        return nomEleve;
    }

    /**
     * @param nomEleve
     *            the nomEleve to set
     */
    public void setNomEleve(String nomEleve) {
        this.nomEleve = nomEleve;
    }

    /**
     * Classe repr�sentant une p�riode, un bilan p�riodique.
     */
    public static class PeriodeLSU implements Comparable<PeriodeLSU> {

        /** Num�ro de la p�riode. */
        private Integer numPeriode;

        /** Nombre de la p�riode. */
        private Integer nbPeriode;

        /** �tablissement dans lequel a eu lieu la p�riode. */
        private EtablissementNational etablissement;

        /** Id de l'�tablissement. */
        private String idEtablissement;

        /** Division (classe). */
        private String division;

        /** Liste des �valuations. */
        private SortedSet<AffichageEvaluationDisciplineLSU> evaluationsDiscipline;

        /**
         * Constructeur par d�faut.
         */
        public PeriodeLSU() {
            super();
            evaluationsDiscipline = new TreeSet<AffichageEvaluationLSU.AffichageEvaluationDisciplineLSU>();
        }

        /**
         * Constructeur � partir de la p�riode issus des tables d'accueil LSU.
         * 
         * @param bilan
         *            Bilan issu des tables d'accueil
         */
        public PeriodeLSU(EvaluationBilanPeriodeLSU bilan) {
            this();
            this.numPeriode = bilan.getValeurPeriode();
            this.nbPeriode = bilan.getNbPeriode();
            this.idEtablissement = bilan.getIdEtablissement();
            this.division = bilan.getDivision();
        }

        /**
         * @return the numPeriode
         */
        public Integer getNumPeriode() {
            return numPeriode;
        }

        /**
         * @param numPeriode
         *            the numPeriode to set
         */
        public void setNumPeriode(Integer numPeriode) {
            this.numPeriode = numPeriode;
        }

        /**
         * @return the nbPeriode
         */
        public Integer getNbPeriode() {
            return nbPeriode;
        }

        /**
         * @param nbPeriode
         *            the nbPeriode to set
         */
        public void setNbPeriode(Integer nbPeriode) {
            this.nbPeriode = nbPeriode;
        }

        /**
         * @return the etablissement
         */
        public EtablissementNational getEtablissement() {
            return etablissement;
        }

        /**
         * @param etablissement
         *            the etablissement to set
         */
        public void setEtablissement(EtablissementNational etablissement) {
            this.etablissement = etablissement;
        }

        /**
         * @return the idEtablissement
         */
        public String getIdEtablissement() {
            return idEtablissement;
        }

        /**
         * @param idEtablissement
         *            the idEtablissement to set
         */
        public void setIdEtablissement(String idEtablissement) {
            this.idEtablissement = idEtablissement;
        }

        /**
         * @return the division
         */
        public String getDivision() {
            return division;
        }

        /**
         * @param division
         *            the division to set
         */
        public void setDivision(String division) {
            this.division = division;
        }

        /**
         * @return the evaluationsDiscipline
         */
        public SortedSet<AffichageEvaluationDisciplineLSU> getEvaluationsDiscipline() {
            return evaluationsDiscipline;
        }

        /**
         * @param evaluationsDiscipline
         *            the evaluationsDiscipline to set
         */
        public void setEvaluationsDiscipline(SortedSet<AffichageEvaluationDisciplineLSU> evaluationsDiscipline) {
            this.evaluationsDiscipline = evaluationsDiscipline;
        }

        /**
         * Ajoute une �valuation � la p�riode.
         * 
         * @param affichageEvaluationDisciplineLSU
         *            �valuation ajout�e
         */
        public void addEvaluationsDiscipline(AffichageEvaluationDisciplineLSU affichageEvaluationDisciplineLSU) {
            this.evaluationsDiscipline.add(affichageEvaluationDisciplineLSU);
        }

        @Override
        public int compareTo(PeriodeLSU o) {
            int result = this.numPeriode - o.getNumPeriode();
            if (result == 0) {
                result = this.nbPeriode - o.getNbPeriode();
            }
            if (result == 0) {
                result = this.nbPeriode - o.getNbPeriode();
            }
            if (result == 0) {
                result = this.idEtablissement.compareTo(o.idEtablissement);
            }
            if (result == 0) {
                result = this.division.compareTo(o.getDivision());
            }
            if (result == 0) {
                result = this.evaluationsDiscipline.hashCode() - o.getEvaluationsDiscipline().hashCode();
            }
            return result;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(numPeriode).append(nbPeriode).append(etablissement)
                    .append(division).append(evaluationsDiscipline).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof PeriodeLSU)) {
                return false;
            }
            PeriodeLSU castOther = (PeriodeLSU) other;
            return new EqualsBuilder().append(numPeriode, castOther.getNumPeriode())
                    .append(nbPeriode, castOther.getNbPeriode()).append(etablissement, castOther.etablissement)
                    .append(division, castOther.getDivision())
                    .append(evaluationsDiscipline, castOther.getEvaluationsDiscipline()).isEquals();
        }
    }

    /**
     * Image de l'�valuation discipline de LSU en liant avec les nomenclatures d'Affelnet.
     */
    public static class AffichageEvaluationDisciplineLSU implements Comparable<AffichageEvaluationDisciplineLSU> {
        /** Identifiant de l'�valuation. */
        private long id;

        /** Libell� de la mati�re. */
        private String libelleMatiere;

        /** Libell� du positionnement. */
        private String libelleGroupe;

        /** Valeur de l'�valuation. */
        private String valEval;

        /** Le groupe de niveau attribu� par Affelnet. */
        private GroupeNiveau groupeNiveau;

        /** Modalit� d'�lection de la discipline. */
        private char modalite;

        /** Ordre d'affichage. */
        private Integer ordre;

        /** Si la discipline est inconnue dans Affelnet. */
        private boolean matiereInconnue;

        /** Si l'�valuation est � ignorer. */
        private boolean nonNote;

        /** Si la modalit� est refus�e. */
        private boolean modaliteIgnoree;

        /**
         * Constructeur par d�faut.
         */
        public AffichageEvaluationDisciplineLSU() {
            super();
        }

        /**
         * Constructeur avec id.
         * 
         * @param nbEval
         *            id de l'�val
         */
        public AffichageEvaluationDisciplineLSU(long nbEval) {
            this();
            this.id = nbEval;
        }

        /**
         * @return the id
         */
        public long getId() {
            return id;
        }

        /**
         * @param id
         *            the id to set
         */
        public void setId(long id) {
            this.id = id;
        }

        /**
         * @return the libelleMatiere
         */
        public String getLibelleMatiere() {
            return libelleMatiere;
        }

        /**
         * @param libelleMatiere
         *            the libelleMatiere to set
         */
        public void setLibelleMatiere(String libelleMatiere) {
            this.libelleMatiere = libelleMatiere;
        }

        /**
         * @return the libelleGroupe
         */
        public String getLibelleGroupe() {
            return libelleGroupe;
        }

        /**
         * @param libelleGroupe
         *            the libelleGroupe to set
         */
        public void setLibelleGroupe(String libelleGroupe) {
            this.libelleGroupe = libelleGroupe;
        }

        /**
         * @return the valEval
         */
        public String getValEval() {
            return valEval;
        }

        /**
         * @param valEval
         *            the valEval to set
         */
        public void setValEval(String valEval) {
            this.valEval = valEval;
        }

        /**
         * @return the groupeNiveau
         */
        public GroupeNiveau getGroupeNiveau() {
            return groupeNiveau;
        }

        /**
         * @param groupeNiveau
         *            the groupeNiveau to set
         */
        public void setGroupeNiveau(GroupeNiveau groupeNiveau) {
            this.groupeNiveau = groupeNiveau;
        }

        /**
         * @return the modalite
         */
        public char getModalite() {
            return modalite;
        }

        /**
         * @param modalite
         *            the modalite to set
         */
        public void setModalite(char modalite) {
            this.modalite = modalite;
        }

        /**
         * @return the ordre
         */
        public Integer getOrdre() {
            return ordre;
        }

        /**
         * @param ordre
         *            the ordre to set
         */
        public void setOrdre(Integer ordre) {
            this.ordre = ordre;
        }

        /**
         * @return the matiereInconnue
         */
        public boolean isMatiereInconnue() {
            return matiereInconnue;
        }

        /**
         * @param matiereInconnue
         *            the matiereInconnue to set
         */
        public void setMatiereInconnue(boolean matiereInconnue) {
            this.matiereInconnue = matiereInconnue;
        }

        /**
         * @return the nonNote
         */
        public boolean isNonNote() {
            return nonNote;
        }

        /**
         * @param nonNote
         *            the nonNote to set
         */
        public void setNonNote(boolean nonNote) {
            this.nonNote = nonNote;
        }

        /**
         * @return the modaliteIgnoree
         */
        public boolean isModaliteIgnoree() {
            return modaliteIgnoree;
        }

        /**
         * @param modaliteIgnoree
         *            the modaliteIgnoree to set
         */
        public void setModaliteIgnoree(boolean modaliteIgnoree) {
            this.modaliteIgnoree = modaliteIgnoree;
        }

        @Override
        public int compareTo(AffichageEvaluationDisciplineLSU o) {
            int result = this.ordre - o.getOrdre();
            if (result == 0) {
                result = this.modalite - o.getModalite();
            }
            if (result == 0) {
                if (this.groupeNiveau != null) {
                    if (o.groupeNiveau != null) {
                        result = this.groupeNiveau.getPoints() - o.groupeNiveau.getPoints();
                    } else {
                        result = -1;
                    }
                } else {
                    if (o.groupeNiveau != null) {
                        result = 1;
                    }
                }
            }
            if (result == 0) {
                if (this.valEval != null) {
                    if (o.valEval != null) {
                        result = this.valEval.compareTo(o.getValEval());
                    } else {
                        result = -1;
                    }
                } else {
                    if (o.valEval != null) {
                        result = 1;
                    }
                }
            }
            if (result == 0) {
                result = (int) (this.id - o.id);
            }
            return result;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(libelleGroupe).append(libelleMatiere).append(modalite)
                    .append(ordre).append(groupeNiveau).append(valEval).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof AffichageEvaluationDisciplineLSU)) {
                return false;
            }
            AffichageEvaluationDisciplineLSU castOther = (AffichageEvaluationDisciplineLSU) other;
            return this.hashCode() == castOther.hashCode();
        }
    }
}
