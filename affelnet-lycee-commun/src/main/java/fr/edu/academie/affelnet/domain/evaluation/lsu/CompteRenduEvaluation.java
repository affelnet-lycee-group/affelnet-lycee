/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Le compte-rendu li� � la demande d'�valuation.
 */
public class CompteRenduEvaluation {
    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(CompteRenduEvaluation.class);

    /** id de l'�tablissement. */
    private String idEtablissement;

    /** La demande d'�valuation li�e. */
    private DemandeEvaluation demande;

    /** Nombre d'�l�ves pr�sents dans le fichier re�u. */
    private int nbEleve;

    /** Nombre de bilan p�riodique int�gr�s. */
    private int nbBilanPeriodique;

    /** Nombre de socle complets int�gr�s. */
    private int nbSocleComplet;

    /** Ensemble des �l�ves ayant un socle incomplet. */
    private Set<String> elevesSocleIncompletDemande = new TreeSet<>();

    /** Nombre d'�l�ves int�gr�s avec au moins un bilan p�riodique et une �valuation du socle. */
    private int nbEleveIntegreAvecEvaluationEtSocle;

    /** �l�ves de palier 3�me de l'�tablissement absent du fichier. */
    private Set<String> elevesAbsentsLsu = new TreeSet<>();

    /** �l�ves pr�sents dans le fichier et absents de l'�tablissement dans Affelnet. */
    private Set<String> elevesAbsentsAffelnet = new TreeSet<>();

    /** Map contenant les �l�ves d'o� proviennent les correspondances. */
    private Map<String, Set<String>> eleveParCorrespondance = new TreeMap<>();

    /** Map des disciplines inconnues pour ces MefStats. */
    private Map<String, Map<String, Set<String>>> disciplineInconnueFormation = new TreeMap<>();

    /** Ensemble des param�tres � enregistrer en base. */
    private Set<DetailCompteRenduEvaluation> details = new HashSet<>();

    /**
     * @return the idEtablissement
     */
    public String getIdEtablissement() {
        return idEtablissement;
    }

    /**
     * @param idEtablissement
     *            the idEtablissement to set
     */
    public void setIdEtablissement(String idEtablissement) {
        this.idEtablissement = idEtablissement;
    }

    /**
     * @return the demande
     */
    public DemandeEvaluation getDemande() {
        return demande;
    }

    /**
     * @param demande
     *            the demande to set
     */
    public void setDemande(DemandeEvaluation demande) {
        this.demande = demande;
    }

    /**
     * @return the nbEleveDemande
     */
    public int getNbEleve() {
        return nbEleve;
    }

    /**
     * @param nbEleve
     *            the nbEleve to set
     */
    public void setNbEleve(int nbEleve) {
        this.nbEleve = nbEleve;
    }

    /**
     * @return the nbBilanPeriodique
     */
    public int getNbBilanPeriodique() {
        return nbBilanPeriodique;
    }

    /**
     * @param nbBilanPeriodique
     *            the nbBilanPeriodique to set
     */
    public void setNbBilanPeriodique(int nbBilanPeriodique) {
        this.nbBilanPeriodique = nbBilanPeriodique;
    }

    /**
     * @return the nbSocleCompletDemande
     */
    public int getNbSocleComplet() {
        return nbSocleComplet;
    }

    /**
     * @param nbSocleComplet
     *            the nbSocleComplet to set
     */
    public void setNbSocleComplet(int nbSocleComplet) {
        this.nbSocleComplet = nbSocleComplet;
    }

    /**
     * @return the nbEleveIntegreAvecEvaluationEtSocle
     */
    public int getNbEleveIntegreAvecEvaluationEtSocle() {
        return nbEleveIntegreAvecEvaluationEtSocle;
    }

    /**
     * @param nbEleveIntegreAvecEvaluationEtSocle
     *            the nbEleveIntegreAvecEvaluationEtSocle to set
     */
    public void setNbEleveIntegreAvecEvaluationEtSocle(int nbEleveIntegreAvecEvaluationEtSocle) {
        this.nbEleveIntegreAvecEvaluationEtSocle = nbEleveIntegreAvecEvaluationEtSocle;
    }

    /**
     * @return the elevesSocleIncompletDemande
     */
    public Set<String> getElevesSocleIncompletDemande() {
        return elevesSocleIncompletDemande;
    }

    /**
     * @param elevesSocleIncompletDemande
     *            the elevesSocleIncompletDemande to set
     */
    public void setElevesSocleIncompletDemande(Set<String> elevesSocleIncompletDemande) {
        this.elevesSocleIncompletDemande = elevesSocleIncompletDemande;
    }

    /**
     * @return the elevesAbsentsAffelnet
     */
    public Set<String> getElevesAbsentsAffelnet() {
        return elevesAbsentsAffelnet;
    }

    /**
     * @param elevesAbsentsAffelnet
     *            the elevesAbsentsAffelnet to set
     */
    public void setElevesAbsentsAffelnet(Set<String> elevesAbsentsAffelnet) {
        this.elevesAbsentsAffelnet = elevesAbsentsAffelnet;
    }

    /**
     * @return the elevesAbsentsLsu
     */
    public Set<String> getElevesAbsentsLsu() {
        return elevesAbsentsLsu;
    }

    /**
     * @param elevesAbsentsLsu
     *            the elevesAbsentsLsu to set
     */
    public void setElevesAbsentsLsu(Set<String> elevesAbsentsLsu) {
        this.elevesAbsentsLsu = elevesAbsentsLsu;
    }

    /**
     * @return the eleveParCorrespondance
     */
    public Map<String, Set<String>> getEleveParCorrespondance() {
        return eleveParCorrespondance;
    }

    /**
     * @param eleveParCorrespondance
     *            the eleveParCorrespondance to set
     */
    public void setEleveParCorrespondance(Map<String, Set<String>> eleveParCorrespondance) {
        this.eleveParCorrespondance = eleveParCorrespondance;
    }

    /**
     * Ajoute un INE pour la correspondance donn�e.
     * 
     * @param valeurCorrespondance
     *            la valeur de la correspondance
     * @param eleve
     *            une chaine de caract�re contenant les informations de l'�l�ve
     */
    public void addElevePourCorrespondance(String valeurCorrespondance, String eleve) {
        if (eleveParCorrespondance.containsKey(valeurCorrespondance)) {
            eleveParCorrespondance.get(valeurCorrespondance).add(eleve);
        } else {
            Set<String> eleves = new HashSet<>();
            eleves.add(eleve);
            eleveParCorrespondance.put(valeurCorrespondance, eleves);
        }
    }

    /**
     * @return the disciplineInconnueFormation
     */
    public Map<String, Map<String, Set<String>>> getDisciplineInconnueFormation() {
        return disciplineInconnueFormation;
    }

    /**
     * @param disciplineInconnueFormation
     *            the disciplineInconnueFormation to set
     */
    public void setDisciplineInconnueFormation(Map<String, Map<String, Set<String>>> disciplineInconnueFormation) {
        this.disciplineInconnueFormation = disciplineInconnueFormation;
    }

    /**
     * @return the details
     */
    public Set<DetailCompteRenduEvaluation> getDetails() {
        return details;
    }

    /**
     * @param details
     *            the details to set
     */
    public void setDetails(Set<DetailCompteRenduEvaluation> details) {
        this.details = details;
    }

    /**
     * Ajoute un Mefstat pour la discipline donn�e.
     * 
     * @param discilineStr
     *            la valeur de la discipline
     * @param formation
     *            la formation de l'�l�ve
     * @param eleve
     *            une cha�ne de caract�re repr�sentant l'�l�ve
     */
    public void addFormationPourDiscipline(String discilineStr, String formation, String eleve) {
        if (disciplineInconnueFormation.containsKey(discilineStr)) {
            if (disciplineInconnueFormation.get(discilineStr).containsKey(formation)) {
                disciplineInconnueFormation.get(discilineStr).get(formation).add(eleve);
            } else {
                Set<String> eleves = new HashSet<>();
                eleves.add(eleve);
                disciplineInconnueFormation.get(discilineStr).put(formation, eleves);
            }
        } else {
            Map<String, Set<String>> eleveParFormation = new HashMap<>();
            Set<String> eleves = new HashSet<>();
            eleves.add(eleve);
            eleveParFormation.put(formation, eleves);
            disciplineInconnueFormation.put(discilineStr, eleveParFormation);
        }
    }

    /**
     * Incr�mente le nombre de socles complets.
     */
    public void incrementerNbSocleComplet() {
        nbSocleComplet++;
    }

    /**
     * Ajoute l'�l�ve � ceux dont le socle est incomplet.
     * 
     * @param eleve
     *            l'�l�ve dont le socle est incomplet
     */
    public void addEleveSocleIncompletDemande(String eleve) {
        elevesSocleIncompletDemande.add(eleve);
    }

    /**
     * Incr�mente le nombre de bilans p�riodiques.
     */
    public void incrementerNbBilanPeriodique() {
        nbBilanPeriodique++;
    }

    /**
     * Incr�mente le nombre de bilans p�riodiques.
     * 
     * @param nbBilan
     *            le nombre de bilans p�riodiques � ajouter
     */
    public void incrementerNbBilanPeriodique(Integer nbBilan) {
        if (nbBilan != null) {
            nbBilanPeriodique += nbBilan;
        }
    }

    /**
     * Incr�mente le nombre d'�l�ve int�gr� avec au moins une �valuation de discipline et du socle.
     */
    public void incrementerNbEleveIntegreAvecEvaluationEtSocle() {
        nbEleveIntegreAvecEvaluationEtSocle++;
    }

    /**
     * Ajoute l'�l�ve � l'ensemble des �l�ves absents du fichier.
     * 
     * @param eleve
     *            l'�l�ve attendu au format cha�ne de caract�re
     */
    public void addEleveAbsentLsu(String eleve) {
        elevesAbsentsLsu.add(eleve);
    }

    /**
     * Ajoute l'�l�ve � l'ensemble des �l�ves pr�sents dans LSU et absents d'Affelnet.
     * 
     * @param eleve
     *            l'�l�ve attendu au format cha�ne de caract�re
     */
    public void addEleveAbsentAffelnet(String eleve) {
        elevesAbsentsAffelnet.add(eleve);
    }

    /**
     * Cr��e les param�tres (DetailCompteRenduEvaluation) � partir des collections du compte-rendu pour
     * enregistrement en base.
     */
    public void adapterBD() {
        details.clear();
        try {
            details.add(paramEleveParCorrespondance());
            details.add(paramDisciplineInconnueFormation());
            details.add(paramElevesSocleIncomplet());
            details.add(paramElevesAbsentsLsu());
            details.add(paramElevesAbsentsAffelnet());
        } catch (IOException e) {
            LOG.error("Echec de la g�n�ration du JSON pour les anomalies du compte-rendu des �valuations LSU", e);
        }
    }

    /**
     * G�n�re les collections � partir des param�tres enregistr�s en base de donn�es.
     */
    public void genererCompteRendu() {
        ObjectMapper mapper = new ObjectMapper();
        JavaType setType = mapper.getTypeFactory().constructCollectionType(TreeSet.class, String.class);
        JavaType stringType = mapper.getTypeFactory().constructType(String.class);
        JavaType mapSetType = mapper.getTypeFactory().constructMapType(TreeMap.class, stringType, setType);
        JavaType doubleMapSetType = mapper.getTypeFactory().constructMapType(TreeMap.class, stringType,
                mapSetType);

        for (DetailCompteRenduEvaluation det : details) {

            try {
                switch (det.getTypeInformation()) {
                    case ELEVES_PAR_CORRESPONDANCE:
                        Map<String, Set<String>> mapCorrespondance = mapper.readValue(det.getValeurInformation(),
                                mapSetType);
                        eleveParCorrespondance.putAll(mapCorrespondance);
                        break;
                    case DISCIPLINES_INCONNUES_POUR_FORMATIONS:
                        Map<String, Map<String, Set<String>>> mapDiscipline = mapper
                                .readValue(det.getValeurInformation(), doubleMapSetType);
                        disciplineInconnueFormation.putAll(mapDiscipline);
                        break;
                    case ELEVES_SOCLE_INCOMPLET:
                        Set<String> socleIncompletSet = mapper.readValue(det.getValeurInformation(), setType);
                        elevesSocleIncompletDemande.addAll(socleIncompletSet);
                        break;
                    case ELEVES_ABSENTS_AFFELNET:
                        Set<String> absentAffelnetSet = mapper.readValue(det.getValeurInformation(), setType);
                        elevesAbsentsAffelnet.addAll(absentAffelnetSet);
                        break;
                    case ELEVES_ABSENTS_DE_LSU:
                        Set<String> absentLsuSet = mapper.readValue(det.getValeurInformation(), setType);
                        elevesAbsentsLsu.addAll(absentLsuSet);
                        break;
                    default:
                        break;
                }
            } catch (IOException e) {
                LOG.error("Erreur lors de la g�n�ration de la grandeur " + det.getTypeInformation().getCode()
                        + " � partir du JSON enregistr� en base de donn�es", e);
            }
        }
    }

    /**
     * Cr��e les param�tres pour les �l�ves par correspondance pour enregistrement en base de donn�es.
     * 
     * @return les param�tres correspondants aux �l�ves par correspondance
     * @throws IOException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonMappingException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonGenerationException
     *             Erreur lors de la g�n�ration du JSON
     */
    private DetailCompteRenduEvaluation paramEleveParCorrespondance() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = mapper.writeValueAsString(eleveParCorrespondance);
        return new DetailCompteRenduEvaluation(this, TypeInformationCompteRenduLsu.ELEVES_PAR_CORRESPONDANCE,
                jsonValue);

    }

    /**
     * Cr��e les param�tres pour les �l�ves par discipline inconnue pour enregistrement en base de donn�es.
     * 
     * @return les param�tres correspondants aux �l�ves par discipline inconnue
     * @throws IOException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonMappingException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonGenerationException
     *             Erreur lors de la g�n�ration du JSON
     */
    private DetailCompteRenduEvaluation paramDisciplineInconnueFormation() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = mapper.writeValueAsString(disciplineInconnueFormation);

        return new DetailCompteRenduEvaluation(this,
                TypeInformationCompteRenduLsu.DISCIPLINES_INCONNUES_POUR_FORMATIONS, jsonValue);
    }

    /**
     * Cr��e les param�tres pour les �l�ves dont le socle est incomplet.
     * 
     * @return les param�tres pour les �l�ves dont le socle est incomplet
     * @throws IOException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonMappingException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonGenerationException
     *             Erreur lors de la g�n�ration du JSON
     */
    private DetailCompteRenduEvaluation paramElevesSocleIncomplet() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = mapper.writeValueAsString(elevesSocleIncompletDemande);

        return new DetailCompteRenduEvaluation(this, TypeInformationCompteRenduLsu.ELEVES_SOCLE_INCOMPLET,
                jsonValue);
    }

    /**
     * Cr��e les param�tres pour les �l�ves absents du fichier LSU et attendus par Affelnet.
     * 
     * @return les param�tres pour les �l�ves sans �valuation
     * @throws IOException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonMappingException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonGenerationException
     *             Erreur lors de la g�n�ration du JSON
     */
    private DetailCompteRenduEvaluation paramElevesAbsentsLsu() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = mapper.writeValueAsString(elevesAbsentsLsu);

        return new DetailCompteRenduEvaluation(this, TypeInformationCompteRenduLsu.ELEVES_ABSENTS_DE_LSU,
                jsonValue);
    }

    /**
     * Cr��e les param�tres pour les �l�ves pr�sents dans le fichier LSU et absents d'Affelnet.
     * 
     * @return les param�tres pour les �l�ves sans �valuation
     * @throws IOException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonMappingException
     *             Erreur lors de la g�n�ration du JSON
     * @throws JsonGenerationException
     *             Erreur lors de la g�n�ration du JSON
     */
    private DetailCompteRenduEvaluation paramElevesAbsentsAffelnet() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = mapper.writeValueAsString(elevesAbsentsAffelnet);

        return new DetailCompteRenduEvaluation(this, TypeInformationCompteRenduLsu.ELEVES_ABSENTS_AFFELNET,
                jsonValue);
    }

    /**
     * R�initialise le compte-rendu pour un nouveau traitement.
     */
    public void reinitialiser() {
        nbEleve = 0;
        nbBilanPeriodique = 0;
        nbSocleComplet = 0;
        nbEleveIntegreAvecEvaluationEtSocle = 0;
        elevesSocleIncompletDemande.clear();
        elevesAbsentsLsu.clear();
        elevesAbsentsAffelnet.clear();
        eleveParCorrespondance.clear();
        disciplineInconnueFormation.clear();
        details.clear();
    }

    /**
     * M�thode indiquant si le compte-rendu contient les informations relatives � une int�gration..
     * On consid�re que c'est le cas si on trouve au moins un �l�ve avec un socle complet ou incomplet.
     * 
     * @return un boolean indiquant si le compte-rendu poss�de les informations d'une demande d'int�gration
     */
    public boolean isIntegration() {
        return (nbSocleComplet != 0 || !elevesSocleIncompletDemande.isEmpty());
    }
}
