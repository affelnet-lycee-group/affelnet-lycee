/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.util.Date;

import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;

/**
 * Correspondance entre les �valuations re�ues de LSU
 * Et les groupes de niveau pour un �tablissement.
 * Seules les �valuations qui ne sont pas reconnues comme des notes
 * sur 20 sont prises en compte.
 */
public class CorrespondanceEvaluation {
    /** Nombre de caract�re maximal que peut poss�der une �valuation en base de donn�es. */
    public static final int TAILLE_MAX_EVAL = 32;

    /** id technique. */
    private Long id;

    /** Id de l'�tablissement auquel appartient la correspondance. */
    private String idEtablissement;

    /** Evaluation issue de LSU. */
    private String valEval;

    /** Groupe de niveau correspondant � l'�valuation. */
    private GroupeNiveau groupeNiveau;

    /** Date de cr�ation de la correspondance. */
    private Date dateCreation;

    /** Date derni�re modification de la correspondance. */
    private Date dateModification;

    /** Flag pour indiquer si cette �valuation doit �tre prise en compte ou non. **/
    private String flagIgnore;

    /**
     * Constructeur par d�faut.
     */
    public CorrespondanceEvaluation() {
        super();
        this.dateCreation = new Date();
        this.flagIgnore = Flag.NON;
    }

    /**
     * Constructeur avec les arguments essentiels.
     * 
     * @param idEtablissement
     *            id de l'�tablissement de la correspondance.
     * @param valeur
     *            valeur de l'�valuation.
     */
    public CorrespondanceEvaluation(String idEtablissement, String valeur) {
        this();
        this.idEtablissement = idEtablissement;
        this.valEval = troncature32(valeur);
    }

    /**
     * Tronque le String � 32 caract�re si n�cessaire.
     * 
     * @param str
     *            String � tronquer
     * @return la cha�ne de caract�re tronqu�e si n�cessaire
     */
    private String troncature32(String str) {
        if (str.length() > 32) {
            return str.substring(0, 32);
        }
        return str;
    }

    /**
     * Teste si la correspondance est r�solue.
     * 
     * @return true si la correspondance est r�solue
     */
    public boolean isResolue() {
        return this.groupeNiveau != null || this.flagIgnore.equals(Flag.OUI);
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the idEtablissement
     */
    public String getIdEtablissement() {
        return idEtablissement;
    }

    /**
     * @param idEtablissement
     *            the idEtablissement to set
     */
    public void setIdEtablissement(String idEtablissement) {
        this.idEtablissement = idEtablissement;
    }

    /**
     * @return the valEval
     */
    public String getValEval() {
        return valEval;
    }

    /**
     * @param valEval
     *            the valEval to set
     */
    public void setValEval(String valEval) {
        this.valEval = valEval;
    }

    /**
     * @return the groupeNiveau
     */
    public GroupeNiveau getGroupeNiveau() {
        return groupeNiveau;
    }

    /**
     * @param groupeNiveau
     *            the groupeNiveau to set
     */
    public void setGroupeNiveau(GroupeNiveau groupeNiveau) {
        this.groupeNiveau = groupeNiveau;
    }

    /**
     * @return the dateCreation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * @param dateCreation
     *            the dateCreation to set
     */
    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * @return the dateModification
     */
    public Date getDateModification() {
        return dateModification;
    }

    /**
     * @param dateModification
     *            the dateModification to set
     */
    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    /**
     * @return the flagIgnore
     */
    public String getFlagIgnore() {
        return flagIgnore;
    }

    /**
     * @param flagIgnore
     *            the flagIgnore to set
     */
    public void setFlagIgnore(String flagIgnore) {
        this.flagIgnore = flagIgnore;
    }
}
