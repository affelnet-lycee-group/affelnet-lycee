/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.EleveXml;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.LsunAffelnet;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;

/**
 * Classe tra�ant l'�tat de la de derni�re demande
 * d'�valuation LSU d'un �tablissement.
 */
public class DemandeEvaluation {
    /** Taille maximale du message d'erreur. */
    private static final int TAILLE_MSG_ERREUR = 256;

    /** �tablissement � l'origine de la demande. */
    private Etablissement etablissement;

    /** id technique de l'�tablissement. */
    private String idEtablissement;

    /** �tat de la demande. */
    private EtatDemandeEvaluation etat;

    /** Date et heure de cr�ation de la derni�re demande. */
    private Date dateDerDemande;

    /** Date et heure de la r�ception de la derni�re notification de LSU. */
    private Date dateNotification;

    /**
     * Code (�tat) de la r�ception de la derni�re notification de LSU.
     */
    private String codeNotification;

    /** Message d'erreur de la r�ception de la derni�re notification de LSU. */
    private String msgNotification;

    /** Identifiant fourni par LSU. */
    private String uuid;

    /** Date et heure du dernier succ�s de chargement des donn�es. */
    private Date dateSucces;

    /**
     * Flag indiquant si la demande concerne une int�gration (O)
     * Ou un recensement des �valuations (N).
     */
    private String flagIntegration;

    /** Le compte-rendu li� � l'int�gration des �l�ves. */
    private CompteRenduEvaluation compteRenduEvaluation;

    /** Liste des �l�ves de l'�tablissement. */
    private Set<EvaluationEleveLSU> eleves;

    /**
     * Constructeur par d�faut.
     * initialise l'�tat et la date de cr�ation
     */
    public DemandeEvaluation() {
        super();
        this.etat = EtatDemandeEvaluation.ERREUR;
        this.flagIntegration = Flag.NON;
        this.eleves = new HashSet<EvaluationEleveLSU>();
    }

    /**
     * Constructeur avec l'�tablissement associ� � la demande.
     * 
     * @param etablissement
     *            �tablissement � l'origine de la demande.
     */
    public DemandeEvaluation(Etablissement etablissement) {
        this();
        this.etablissement = etablissement;
        this.idEtablissement = etablissement.getId();
    }

    /**
     * @return the etablissement
     */
    public Etablissement getEtablissement() {
        return etablissement;
    }

    /**
     * @param etablissement
     *            the etablissement to set
     */
    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    /**
     * @return the dateDerDemande
     */
    public Date getDateDerDemande() {
        return dateDerDemande;
    }

    /**
     * @param dateDerDemande
     *            the dateDerDemande to set
     */
    public void setDateDerDemande(Date dateDerDemande) {
        this.dateDerDemande = dateDerDemande;
    }

    /**
     * @return the dateNotification
     */
    public Date getDateNotification() {
        return dateNotification;
    }

    /**
     * @param dateNotification
     *            the dateNotification to set
     */
    public void setDateNotification(Date dateNotification) {
        this.dateNotification = dateNotification;
    }

    /**
     * @return the codeNotification
     */
    public String getCodeNotification() {
        return codeNotification;
    }

    /**
     * @param codeNotification
     *            the codeNotification to set
     */
    public void setCodeNotification(String codeNotification) {
        this.codeNotification = codeNotification;
    }

    /**
     * @return the msgNotification
     */
    public String getMsgNotification() {
        return msgNotification;
    }

    /**
     * @param msgNotification
     *            the msgNotification to set
     */
    public void setMsgNotification(String msgNotification) {
        if (msgNotification != null && msgNotification.length() > TAILLE_MSG_ERREUR) {
            this.msgNotification = msgNotification.substring(0, TAILLE_MSG_ERREUR - 1);
        } else {
            this.msgNotification = msgNotification;
        }
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid
     *            the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the dateSucces
     */
    public Date getDateSucces() {
        return dateSucces;
    }

    /**
     * @param dateSucces
     *            the dateSucces to set
     */
    public void setDateSucces(Date dateSucces) {
        this.dateSucces = dateSucces;
    }

    /**
     * @return the eleves
     */
    public Set<EvaluationEleveLSU> getEleves() {
        return eleves;
    }

    /**
     * @param eleves
     *            the eleves to set
     */
    public void setEleves(Set<EvaluationEleveLSU> eleves) {
        this.eleves = eleves;
    }

    /**
     * @return the etat
     */
    public EtatDemandeEvaluation getEtat() {
        return etat;
    }

    /**
     * @param etat
     *            the etat to set
     */
    public void setEtat(EtatDemandeEvaluation etat) {
        this.etat = etat;
        if (etat != EtatDemandeEvaluation.ERREUR) {
            this.msgNotification = StringUtils.EMPTY;
        }
    }

    /**
     * @return the idEtablissement
     */
    public String getIdEtablissement() {
        return this.idEtablissement;
    }

    /**
     * @param idEtablissement
     *            the idEtablissement to set
     */
    public void setIdEtablissement(String idEtablissement) {
        this.idEtablissement = idEtablissement;
    }

    /**
     * @return the flagIntegration
     */
    public String getFlagIntegration() {
        return flagIntegration;
    }

    /**
     * @param flagIntegration
     *            the flagIntegration to set
     */
    public void setFlagIntegration(String flagIntegration) {
        this.flagIntegration = flagIntegration;
    }

    /**
     * @return the compteRenduEvaluation
     */
    public CompteRenduEvaluation getCompteRenduEvaluation() {
        return compteRenduEvaluation;
    }

    /**
     * @param compteRenduEvaluation
     *            the compteRenduEvaluation to set
     */
    public void setCompteRenduEvaluation(CompteRenduEvaluation compteRenduEvaluation) {
        this.compteRenduEvaluation = compteRenduEvaluation;
    }

    /**
     * Alimente la demande avec les �valuations des �l�ves.
     * 
     * @param lsunAffelnet
     *            Classe issus du fichier XML
     */
    public void alimenterEvaluation(LsunAffelnet lsunAffelnet) {
        // On r�cup�re les �l�ves depuis les classes g�n�r�es
        List<EleveXml> eleveList = lsunAffelnet.getDonnees().getEleves().getEleve();
        this.eleves.clear();
        for (EleveXml eleve : eleveList) {
            this.eleves.add(new EvaluationEleveLSU(eleve, this));
        }
    }

    /**
     * Indique si la demande a d�j� �t� int�gr�e ou non.
     * 
     * @return true si l'int�gration a d�j� eu lieu avec succ�s.
     */
    public boolean isIntegree() {
        return this.etat.equals(EtatDemandeEvaluation.OK) && this.flagIntegration.equals(Flag.OUI);
    }

    /**
     * Adapte le compte-rendu pour l'enregistrement en base de donn�es.
     */
    public void adapterBD() {
        if (compteRenduEvaluation != null) {
            compteRenduEvaluation.adapterBD();
        }
    }

    /**
     * G�n�re les valeurs du compte-rendu en fonction des informations en base de donn�es.
     */
    public void genererCompteRendu() {
        if (compteRenduEvaluation != null) {
            compteRenduEvaluation.genererCompteRendu();
        }
    }

    /**
     * R�initialise le compte-rendu et le cr�� si n�cessaire.
     */
    public void reinitialiserCompteRendu() {
        if (compteRenduEvaluation != null) {
            compteRenduEvaluation.reinitialiser();
        } else {
            CompteRenduEvaluation compteRendu = new CompteRenduEvaluation();
            compteRendu.setDemande(this);
            compteRenduEvaluation = compteRendu;
        }
    }
}
