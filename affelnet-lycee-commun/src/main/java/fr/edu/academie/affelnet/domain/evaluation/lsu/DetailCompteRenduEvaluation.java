/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import fr.edu.academie.affelnet.utils.ChaineUtils;

/**
 * Propri�t� complexe du compte-rendu des �valuations.
 */
public class DetailCompteRenduEvaluation {

    /** Id technique. */
    private long id;

    /** Compte-rendu auquel appartiennent les informations. */
    private CompteRenduEvaluation compteRenduEvaluation;

    /** Le type d'information du param�tre. */
    private String typeInformation;

    /** L'objet � enregistrer en cha�ne de caract�re et au format JSON. */
    private String valeurInformation;

    /**
     * Constructeur par d�faut.
     */
    public DetailCompteRenduEvaluation() {
        super();
    }

    /**
     * Le constructeur avec le compte-rendu, la typeInformation du param�tre et une valeur.
     * 
     * @param compteRenduEvaluation
     *            Le compte-rendu associ�
     * @param typeInformation
     *            la typeInformation du param�tre
     * @param valeurInformation
     *            la valeur de l'information
     */
    public DetailCompteRenduEvaluation(CompteRenduEvaluation compteRenduEvaluation,
            TypeInformationCompteRenduLsu typeInformation, String valeurInformation) {
        this();
        this.compteRenduEvaluation = compteRenduEvaluation;
        this.typeInformation = typeInformation.getCode();
        this.valeurInformation = ChaineUtils.tronquerChaineLarge(valeurInformation, null);
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the compteRenduEvaluation
     */
    public CompteRenduEvaluation getCompteRenduEvaluation() {
        return compteRenduEvaluation;
    }

    /**
     * @param compteRenduEvaluation
     *            the compteRenduEvaluation to set
     */
    public void setCompteRenduEvaluation(CompteRenduEvaluation compteRenduEvaluation) {
        this.compteRenduEvaluation = compteRenduEvaluation;
    }

    /**
     * @return the typeInformation
     */
    public TypeInformationCompteRenduLsu getTypeInformation() {
        return TypeInformationCompteRenduLsu.getPourCode(typeInformation);
    }

    /**
     * @param typeInformation
     *            the typeInformation to set
     */
    public void setTypeInformation(TypeInformationCompteRenduLsu typeInformation) {
        this.typeInformation = typeInformation.getCode();
    }

    /**
     * @return the valeurInformation
     */
    public String getValeurInformation() {
        return valeurInformation;
    }

    /**
     * @param valeurInformation
     *            the valeurInformation to set
     */
    public void setValeurInformation(String valeurInformation) {
        this.valeurInformation = valeurInformation;
    }
}
