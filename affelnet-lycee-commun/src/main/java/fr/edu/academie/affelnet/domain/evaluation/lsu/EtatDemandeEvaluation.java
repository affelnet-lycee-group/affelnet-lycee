/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

/**
 * �num�ration des �tats d'une demande d'�valuation LSU.
 */
public enum EtatDemandeEvaluation {
    /** Si une demande peut �tre effectu�e ou si le document a �t� t�l�charg�. */
    OK,
    /** Si la demande est en cours de traitement. */
    EN_COURS,
    /** Si la demande est en erreur. */
    ERREUR;

    /**
     * Transforme de String en �num�ration.
     * 
     * @param v
     *            String � transformer
     * @return l'enum�ration
     */
    public static EtatDemandeEvaluation fromValue(String v) {
        return valueOf(v);
    }

    /**
     * renvoie la valeur au format texte.
     * 
     * @return la valeur en tant que String
     */
    public String toString() {
        return this.name();
    }
}
