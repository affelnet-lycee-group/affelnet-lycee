/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.util.HashSet;
import java.util.Set;

import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.Acquis;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.EleveXml.BilansPeriodiques.BilanPeriodique;
import fr.edu.academie.affelnet.utils.ChaineUtils;

/**
 * Classe pour l'import des donn�es de LSU.
 * Repr�sente une p�riode d'�valuation.
 */
public class EvaluationBilanPeriodeLSU implements Comparable<EvaluationBilanPeriodeLSU> {
    /** id de la p�riode. */
    private Long id;

    /** Eleve auquel appartient la p�riode. */
    private EvaluationEleveLSU evaluationEleveLSU;

    /** identifiant de l'�tablissement de l'�l�ve pendant cette p�riode. */
    private String idEtablissement;

    /** Division (classe) de l'�l�ve pendant cette p�riode. */
    private String division;

    /** Indice de la p�riode. */
    private int valeurPeriode;

    /** Nombre de p�riode. */
    private int nbPeriode;

    /** Liste des �valuations de la p�riode. */
    private Set<EvaluationDisciplineLSU> disciplines;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationBilanPeriodeLSU() {
        super();
        this.disciplines = new HashSet<EvaluationDisciplineLSU>();
    }

    /**
     * Constructeur � partir de la classe XML.
     * 
     * @param bilan
     *            classe issue du fichier XML
     * @param evaluationEleveLSU
     *            El�ve auquel appartient la p�riode
     */
    public EvaluationBilanPeriodeLSU(BilanPeriodique bilan, EvaluationEleveLSU evaluationEleveLSU) {
        this();
        this.evaluationEleveLSU = evaluationEleveLSU;
        this.idEtablissement = bilan.getCodeUai();
        this.division = ChaineUtils.conversionUtf8VersIso15(bilan.getDivision());
        this.valeurPeriode = bilan.getIndicePeriode();
        this.nbPeriode = bilan.getNbPeriodes();

        for (Acquis acquis : bilan.getAcquis()) {
            this.disciplines.add(new EvaluationDisciplineLSU(acquis, this));
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the id_etablissement
     */
    public String getIdEtablissement() {
        return idEtablissement;
    }

    /**
     * @param idEtablissement
     *            the idEtablissement to set
     */
    public void setIdEtablissement(String idEtablissement) {
        this.idEtablissement = idEtablissement;
    }

    /**
     * @return the division
     */
    public String getDivision() {
        return division;
    }

    /**
     * @param division
     *            the division to set
     */
    public void setDivision(String division) {
        this.division = division;
    }

    /**
     * @return the valeurPeriode
     */
    public int getValeurPeriode() {
        return valeurPeriode;
    }

    /**
     * @param valeurPeriode
     *            the valeurPeriode to set
     */
    public void setValeurPeriode(int valeurPeriode) {
        this.valeurPeriode = valeurPeriode;
    }

    /**
     * @return the nbPeriode
     */
    public int getNbPeriode() {
        return nbPeriode;
    }

    /**
     * @param nbPeriode
     *            the nbPeriode to set
     */
    public void setNbPeriode(int nbPeriode) {
        this.nbPeriode = nbPeriode;
    }

    /**
     * @return the disciplines
     */
    public Set<EvaluationDisciplineLSU> getDisciplines() {
        return disciplines;
    }

    /**
     * @param disciplines
     *            the disciplines to set
     */
    public void setDisciplines(Set<EvaluationDisciplineLSU> disciplines) {
        this.disciplines = disciplines;
    }

    /**
     * @return the evaluationEleveLSU
     */
    public EvaluationEleveLSU getEvaluationEleveLSU() {
        return evaluationEleveLSU;
    }

    /**
     * @param evaluationEleveLSU
     *            the evaluationEleveLSU to set
     */
    public void setEvaluationEleveLSU(EvaluationEleveLSU evaluationEleveLSU) {
        this.evaluationEleveLSU = evaluationEleveLSU;
    }

    @Override
    public int compareTo(EvaluationBilanPeriodeLSU o) {
        int result = (int) (this.evaluationEleveLSU.getId() - o.evaluationEleveLSU.getId());
        if (result == 0) {
            result = this.valeurPeriode - o.getValeurPeriode();
        }
        if (result == 0) {
            result = (int) (this.id - o.id);
        }
        return result;
    }
}
