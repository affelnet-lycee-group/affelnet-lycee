/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.Acquis;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.DisciplineXml;
import fr.edu.academie.affelnet.utils.ChaineUtils;

/**
 * Classe pour l'import des donn�es de LSU.
 * Repr�sente l'�valuation d'une disciplineXml.
 */
public class EvaluationDisciplineLSU {
    /** id de l'�valuation. */
    private Long id;

    /** P�riode � laquelle appartient l'�valuation. */
    private EvaluationBilanPeriodeLSU evaluationBilanPeriodeLSU;

    /** Code de la mati�re. */
    private String codeMat;

    /** Libell� de la mati�re. */
    private String libelleMat;

    /** Code de la modalit� d'�lection. */
    private char modaliteElection;

    /** Groupe de niveau de l'�valuation. */
    private Integer niveau;

    /** Valeur alphanum�rique re�ue. */
    private String valeur;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationDisciplineLSU() {
        super();
    }

    /**
     * Constructeur avec la classe XML.
     * 
     * @param acquis
     *            classe issue du fichier XML
     * @param evaluationBilanPeriodeLSU
     *            P�riode � laquelle appartient l'�valuation
     */
    public EvaluationDisciplineLSU(Acquis acquis, EvaluationBilanPeriodeLSU evaluationBilanPeriodeLSU) {
        this();
        this.evaluationBilanPeriodeLSU = evaluationBilanPeriodeLSU;
        DisciplineXml disciplineXml = (DisciplineXml) acquis.getDisciplineRef();
        this.codeMat = ChaineUtils.conversionUtf8VersIso15(disciplineXml.getCode());
        this.libelleMat = ChaineUtils.conversionUtf8VersIso15(disciplineXml.getLibelle());
        this.modaliteElection = disciplineXml.getModaliteElection().value().charAt(0);
        if (acquis.getNote() != null) {
            this.valeur = ChaineUtils.conversionUtf8VersIso15(acquis.getNote());
            this.niveau = null;
        } else if (acquis.getPositionnement() != null) {
            this.niveau = acquis.getPositionnement();
            this.valeur = null;
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the evaluationBilanPeriodeLSU
     */
    public EvaluationBilanPeriodeLSU getBilanPeriodeEvaluation() {
        return evaluationBilanPeriodeLSU;
    }

    /**
     * @param evaluationBilanPeriodeLSU
     *            the evaluationBilanPeriodeLSU to set
     */
    public void setBilanPeriodeEvaluation(EvaluationBilanPeriodeLSU evaluationBilanPeriodeLSU) {
        this.evaluationBilanPeriodeLSU = evaluationBilanPeriodeLSU;
    }

    /**
     * @return the codeMat
     */
    public String getCodeMat() {
        return codeMat;
    }

    /**
     * @param codeMat
     *            the codeMat to set
     */
    public void setCodeMat(String codeMat) {
        this.codeMat = codeMat;
    }

    /**
     * @return the modaliteElection
     */
    public char getModaliteElection() {
        return modaliteElection;
    }

    /**
     * @param modaliteElection
     *            the modaliteElection to set
     */
    public void setModaliteElection(char modaliteElection) {
        this.modaliteElection = modaliteElection;
    }

    /**
     * @return the niveau
     */
    public Integer getNiveau() {
        return niveau;
    }

    /**
     * @param niveau
     *            the niveau to set
     */
    public void setNiveau(Integer niveau) {
        this.niveau = niveau;
    }

    /**
     * @return the valeur
     */
    public String getValeur() {
        return valeur;
    }

    /**
     * @param valeur
     *            the valeur to set
     */
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    /**
     * Fournit la discipline au format texte avec le libell� et le code mati�re.
     * 
     * @return la discipline au format texte
     */
    public String disciplineToString() {
        return libelleMat + " (" + codeMat + ")";
    }
}
