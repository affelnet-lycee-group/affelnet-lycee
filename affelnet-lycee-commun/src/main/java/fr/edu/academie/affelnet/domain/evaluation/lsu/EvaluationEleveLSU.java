/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.DomaineSocle;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.EleveXml;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.EleveXml.BilansPeriodiques.BilanPeriodique;
import fr.edu.academie.affelnet.utils.ChaineUtils;

/**
 * Classe pour l'import des donn�es de LSU.
 * Repr�sente les �l�ves.
 */
public class EvaluationEleveLSU {
    /** id technique. */
    private Long id;

    /** Demande � laquelle appartient l'�l�ve. */
    private DemandeEvaluation demandeEvaluation;

    /** Nom de l'�l�ve. */
    private String nom;

    /** Pr�nom de l'�l�ve. */
    private String prenom;

    /** INE de l'�l�ve. */
    private String ine;

    /** Date de naissance de l'�l�ve. */
    private Date dateNaissance;

    /** Flag indiquant si l'�l�ve a �t� int�gr� ou non. */
    private String flagIntegre;

    /** Liste des �valuations du socle. */
    private Set<EvaluationSocleLSU> socle;

    /** Liste des p�riodes. */
    private Set<EvaluationBilanPeriodeLSU> bilans;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationEleveLSU() {
        super();
        this.socle = new HashSet<EvaluationSocleLSU>();
        this.bilans = new HashSet<EvaluationBilanPeriodeLSU>();
        this.flagIntegre = Flag.NON;
    }

    /**
     * Constructeur � partir d'un EleveXml.
     * 
     * @param eleve
     *            Classe issue du fichier XML
     * @param demandeEvaluation
     *            demande � laquelle appartient l'�l�ve
     */
    public EvaluationEleveLSU(EleveXml eleve, DemandeEvaluation demandeEvaluation) {
        this();
        this.demandeEvaluation = demandeEvaluation;
        this.ine = eleve.getIne();
        this.nom = troncature100(ChaineUtils.conversionUtf8VersIso15(eleve.getNom()));
        this.prenom = troncature100(ChaineUtils.conversionUtf8VersIso15(eleve.getPrenom()));
        if (eleve.getDateNaissance() != null) {
            this.dateNaissance = eleve.getDateNaissance().toGregorianCalendar().getTime();
        }

        for (DomaineSocle domaine : eleve.getSocle().getDomaine()) {
            this.socle.add(new EvaluationSocleLSU(domaine, this));
        }

        for (BilanPeriodique bilan : eleve.getBilansPeriodiques().getBilanPeriodique()) {
            this.bilans.add(new EvaluationBilanPeriodeLSU(bilan, this));
        }
    }

    /**
     * Troncature � 100 caract�re si de taille sup�rieure.
     * 
     * @param inputString
     *            String issu de la conversion de l'ISO
     * @return String tronqu� � 100 caract�res si n�cessaire
     */
    private String troncature100(String inputString) {
        if (inputString.length() > 100) {
            return inputString.substring(0, 100);
        }
        return inputString;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the demandeEvaluation
     */
    public DemandeEvaluation getDemandeEvaluation() {
        return demandeEvaluation;
    }

    /**
     * @param demandeEvaluation
     *            the demandeEvaluation to set
     */
    public void setDemandeEvaluation(DemandeEvaluation demandeEvaluation) {
        this.demandeEvaluation = demandeEvaluation;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom
     *            the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom
     *            the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the ine
     */
    public String getIne() {
        return ine;
    }

    /**
     * @param ine
     *            the ine to set
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    /**
     * @return the dateNaissance
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * @param dateNaissance
     *            the dateNaissance to set
     */
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * @return the flagIntegre
     */
    public String getFlagIntegre() {
        return flagIntegre;
    }

    /**
     * @param flagIntegre
     *            the flagIntegre to set
     */
    public void setFlagIntegre(String flagIntegre) {
        this.flagIntegre = flagIntegre;
    }

    /**
     * @return the socle
     */
    public Set<EvaluationSocleLSU> getSocle() {
        return socle;
    }

    /**
     * @param socle
     *            the socle to set
     */
    public void setSocle(Set<EvaluationSocleLSU> socle) {
        this.socle = socle;
    }

    /**
     * @return the bilans
     */
    public Set<EvaluationBilanPeriodeLSU> getBilans() {
        return bilans;
    }

    /**
     * @param bilans
     *            the bilans to set
     */
    public void setBilans(Set<EvaluationBilanPeriodeLSU> bilans) {
        this.bilans = bilans;
    }

    @Override
    public String toString() {
        return nom + " " + prenom + " (" + ine + ")";
    }
}
