/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.DomaineSocle;

/**
 * Classe pour l'import des donn�es de LSU.
 * Repr�sente une �valuation d'une comp�tence du socle.
 */
public class EvaluationSocleLSU {
    /** id compos� du socle. */
    private EvaluationSocleLsuPK id;

    /** Degr� de note de la comp�tence. */
    private Integer degre;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationSocleLSU() {
        super();
    }

    /**
     * @param degre
     *            degr� de note de la comp�tence
     * @param id
     *            id composite
     */
    public EvaluationSocleLSU(EvaluationSocleLsuPK id, Integer degre) {
        this();
        if (degre == 0) {
            this.degre = null;
        }
        this.degre = degre;
        this.id = id;
    }

    /**
     * Constructeur � partir de la classe issue du fichier XML.
     * 
     * @param domaine
     *            classe XML
     * @param evaluationEleveLSU
     *            �l�ve auquel appartient l'�valuation
     */
    public EvaluationSocleLSU(DomaineSocle domaine, EvaluationEleveLSU evaluationEleveLSU) {
        this(new EvaluationSocleLsuPK(evaluationEleveLSU, domaine.getCode().value()), domaine.getPositionnement());
    }

    /**
     * @return the degre
     */
    public Integer getDegre() {
        return degre;
    }

    /**
     * @param degre
     *            the degre to set
     */
    public void setDegre(Integer degre) {
        this.degre = degre;
    }

    /**
     * @return the id
     */
    public EvaluationSocleLsuPK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(EvaluationSocleLsuPK id) {
        this.id = id;
    }
}
