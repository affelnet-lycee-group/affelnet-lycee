/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.utils.ChaineUtils;

/**
 * Cl� primaire du socle des demandes d'�valuation LSU.
 */
public class EvaluationSocleLsuPK implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** El�ve auquel appartient le socle. */
    private EvaluationEleveLSU evaluationEleveLSU;

    /** Code de la comp�tence. */
    private String competence;

    /**
     * Constructeur par d�faut.
     */
    public EvaluationSocleLsuPK() {
    }

    /**
     * Constructeur avec les param�tres pour compl�ter la cl�.
     * 
     * @param evaluationEleveLSU
     *            El�ve auquel appartient le socle
     * @param competence
     *            code de la comp�tence
     */
    public EvaluationSocleLsuPK(EvaluationEleveLSU evaluationEleveLSU, String competence) {
        super();
        this.evaluationEleveLSU = evaluationEleveLSU;
        this.competence = ChaineUtils.conversionUtf8VersIso15(competence);
    }

    /**
     * @return the evaluationEleveLSU
     */
    public EvaluationEleveLSU getEvaluationEleveLSU() {
        return evaluationEleveLSU;
    }

    /**
     * @param evaluationEleveLSU
     *            the evaluationEleveLSU to set
     */
    public void setEvaluationEleveLSU(EvaluationEleveLSU evaluationEleveLSU) {
        this.evaluationEleveLSU = evaluationEleveLSU;
    }

    /**
     * @return the competence
     */
    public String getCompetence() {
        return competence;
    }

    /**
     * @param competence
     *            the competence to set
     */
    public void setCompetence(String competence) {
        this.competence = competence;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof EvaluationSocleLsuPK)) {
            return false;
        }
        EvaluationSocleLsuPK castOther = (EvaluationSocleLsuPK) other;
        return new EqualsBuilder().append(this.getEvaluationEleveLSU(), castOther.getEvaluationEleveLSU())
                .append(this.getCompetence(), castOther.getCompetence()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getEvaluationEleveLSU()).append(getCompetence()).toHashCode();
    }
}
