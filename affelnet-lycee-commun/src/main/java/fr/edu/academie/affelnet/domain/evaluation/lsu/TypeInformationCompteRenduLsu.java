/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.evaluation.lsu;

import java.util.HashMap;
import java.util.Map;

/**
 * �num�ration des natures possibles pour les param�tres du compte rendu de la r�ception des fichiers LSU.
 */
public enum TypeInformationCompteRenduLsu {

    /** Les correspondances et les �l�ves associ�s. */
    ELEVES_PAR_CORRESPONDANCE("elevesParCorrespondance"),

    /** Les disciplines inconnues avec la formations et les �l�ves concern�s. */
    DISCIPLINES_INCONNUES_POUR_FORMATIONS("disciplinesInconnues"),

    /** Les �l�ves dont le socle est incomplet. */
    ELEVES_SOCLE_INCOMPLET("elevesSocleIncomplet"),

    /** Les �l�ves sans �valuation. */
    ELEVES_SANS_EVALUATION("elevesSansEvaluation"),

    /** Les �l�ves attendus par l'�tablissement et absents du fichier. */
    ELEVES_ABSENTS_DE_LSU("elevesAbsentsDeLsu"),

    /** Les �l�ves pr�sents dans le fichier LSU et non dans l'�tablissement pour Affelnet. */
    ELEVES_ABSENTS_AFFELNET("elevesAbsentsAffelnet");

    /** Table des natures de param�tre pour le d�codage par code. */
    static private final Map<String, TypeInformationCompteRenduLsu> NATURE_PAR_CODE = new HashMap<String, TypeInformationCompteRenduLsu>();

    static {
        for (TypeInformationCompteRenduLsu nature : TypeInformationCompteRenduLsu.values()) {
            NATURE_PAR_CODE.put(nature.getCode(), nature);
        }
    }
    /** Le code � enregistrer en base. */
    private String code;

    /**
     * Le constructeur de l'�num�ration.
     * 
     * @param code
     *            Code de l'�num�ration en base de donn�es
     */
    TypeInformationCompteRenduLsu(String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Fournit l'�num�ration correspondant au code.
     * 
     * @param code
     *            le code de l'�num�ration
     * @return l'�um�ration associ�e
     */
    public static TypeInformationCompteRenduLsu getPourCode(String code) {
        return NATURE_PAR_CODE.get(code);
    }
}
