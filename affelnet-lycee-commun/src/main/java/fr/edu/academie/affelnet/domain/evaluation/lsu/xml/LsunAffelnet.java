/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2016.12.13 at 10:03:16 AM CET
//

package fr.edu.academie.affelnet.domain.evaluation.lsu.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * D�crit la structure de l'�l�ment racine du document
 * 
 * 
 * <p>
 * Java class for LsunAffelnet complex type.
 * </p>
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * </p>
 * 
 * <pre>
 * &lt;complexType name="LsunAffelnet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entete" type="{urn:fr:edu:scolarite:lsun:affelnet:export}Entete"/&gt;
 *         &lt;element name="donnees" type="{urn:fr:edu:scolarite:lsun:affelnet:export}Donnees"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="schemaVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LsunAffelnet", propOrder = { "entete", "donnees" })
public class LsunAffelnet {

    @XmlElement(required = true)
    protected Entete entete;
    @XmlElement(required = true)
    protected Donnees donnees;
    @XmlAttribute(name = "schemaVersion", required = true)
    protected String schemaVersion;

    /**
     * Gets the value of the entete property.
     * 
     * @return
     *         possible object is
     *         {@link Entete }
     * 
     */
    public Entete getEntete() {
        return entete;
    }

    /**
     * Sets the value of the entete property.
     * 
     * @param value
     *            allowed object is
     *            {@link Entete }
     * 
     */
    public void setEntete(Entete value) {
        this.entete = value;
    }

    /**
     * Gets the value of the donnees property.
     * 
     * @return
     *         possible object is
     *         {@link Donnees }
     * 
     */
    public Donnees getDonnees() {
        return donnees;
    }

    /**
     * Sets the value of the donnees property.
     * 
     * @param value
     *            allowed object is
     *            {@link Donnees }
     * 
     */
    public void setDonnees(Donnees value) {
        this.donnees = value;
    }

    /**
     * Gets the value of the schemaVersion property.
     * 
     * @return
     *         possible object is
     *         {@link String }
     * 
     */
    public String getSchemaVersion() {
        if (schemaVersion == null) {
            return "1.0";
        } else {
            return schemaVersion;
        }
    }

    /**
     * Sets the value of the schemaVersion property.
     * 
     * @param value
     *            allowed object is
     *            {@link String }
     * 
     */
    public void setSchemaVersion(String value) {
        this.schemaVersion = value;
    }

}
