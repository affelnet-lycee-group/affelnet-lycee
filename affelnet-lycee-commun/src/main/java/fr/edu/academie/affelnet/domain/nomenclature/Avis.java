/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Avis. */
public class Avis extends Datable implements Serializable {

    /** Code pour les avis par d�faut. */
    public static final String CODE_AVIS_DEFAUT = "00";

    /** Num�rod e version de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private AvisPK id;

    /** Libell� court. */
    private String libelleCourt;

    /** Valeur du bonus. */
    private Integer bonus;

    /** Cet avis doit-il �tre affich� en saisie des voeux par �tablissement ? */
    private String affichageEtablissement;

    /**
     * @return the id
     */
    public AvisPK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(AvisPK id) {
        this.id = id;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the bonus
     */
    public Integer getBonus() {
        return bonus;
    }

    /**
     * @param bonus
     *            the bonus to set
     */
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    /**
     * @return the affichageEtablissement
     */
    public String getAffichageEtablissement() {
        return affichageEtablissement;
    }

    /**
     * @param affichageEtablissement
     *            the affichageEtablissement to set
     */
    public void setAffichageEtablissement(String affichageEtablissement) {
        this.affichageEtablissement = affichageEtablissement;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof Avis)) {
            return false;
        }
        Avis castOther = (Avis) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
