/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** Cl� d'une nomenclature avis. */
public class AvisPK implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1409333315861128129L;

    /** Code de l'avis. */
    private String code;

    /** Type de l'avis. */
    private TypeAvis type;

    /**
     * Full constructor.
     * 
     * @param code
     *            le code de l'avis
     * @param type
     *            le type de l'avis
     */
    public AvisPK(String code, TypeAvis type) {
        this.code = code;
        this.type = type;
    }

    /** Default constructor. */
    public AvisPK() {
    }

    /**
     * @return Returns the code.
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            The code to set.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return Returns the type.
     */
    public TypeAvis getType() {
        return this.type;
    }

    /**
     * @param type
     *            The type to set.
     */
    public void setType(TypeAvis type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", getCode()).append("type", getType()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof AvisPK)) {
            return false;
        }
        AvisPK castOther = (AvisPK) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode())
                .append(this.getType(), castOther.getType()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).append(getType()).toHashCode();
    }
}
