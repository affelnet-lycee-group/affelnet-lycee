/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/**
 * Param�trage de la saisie de l'avis du chef d'�tablissement d'origine en fonction de
 * l'origine et de l'accueil de l'�l�ve.
 */
public class AvisParFormationOrigine extends Datable implements java.io.Serializable {
    /** Serial id. */
    private static final long serialVersionUID = 1L;

    /** Identifiant (auto-g�n�r�). */
    private long id;

    /** Param�tre par formation d'accueil correspondant. */
    private ParametresFormationAccueil parametresFormationAccueil;

    /** Formation d'origine. */
    private Formation formation;

    /** Mn�monique. */
    private String mnemonique;

    /** M�tier. */
    private String codeSpecialite;

    /** Option d'origine 1. */
    private Matiere option1;
    /** Option d'origine 2. */
    private Matiere option2;

    /** Constructeur par d�faut. */
    public AvisParFormationOrigine() {
    }

    /**
     * Constructeur minimal.
     * 
     * @param id
     *            l'identifiant
     * @param parametresFormationAccueil
     *            le param�tre par formation d'accueil
     * @param dacAvismefo
     *            la date de cr�ation
     */
    public AvisParFormationOrigine(long id, ParametresFormationAccueil parametresFormationAccueil,
            Date dacAvismefo) {
        this.id = id;
        this.parametresFormationAccueil = parametresFormationAccueil;
        this.setDateCreation(dacAvismefo);
    }

    /**
     * Constructeur.
     * 
     * @param id
     *            l'identifiant
     * @param parametresFormationAccueil
     *            le param�tre par formation d'accueil
     * @param formation
     *            la formation d'origine
     * @param option1
     *            l'option origine 1
     * @param option2
     *            l'option origine 2
     * @param dacAvismefo
     *            la date de cr�ation
     * @param damAvismefo
     *            la date de derni�re mise � jour
     */
    public AvisParFormationOrigine(long id, ParametresFormationAccueil parametresFormationAccueil,
            Formation formation, Matiere option1, Matiere option2, Date dacAvismefo, Date damAvismefo) {
        this.id = id;
        this.parametresFormationAccueil = parametresFormationAccueil;
        this.setDateCreation(dacAvismefo);
        this.setDateMAJ(damAvismefo);
        this.formation = formation;
        this.option1 = option1;
        this.option2 = option2;
    }

    /**
     * @return the option1
     */
    public Matiere getOption1() {
        return option1;
    }

    /**
     * @param option1
     *            the option1 to set
     */
    public void setOption1(Matiere option1) {
        this.option1 = option1;
    }

    /**
     * @return the option2
     */
    public Matiere getOption2() {
        return option2;
    }

    /**
     * @param option2
     *            the option2 to set
     */
    public void setOption2(Matiere option2) {
        this.option2 = option2;
    }

    /**
     * @return le mn�monique de la formation d'origine
     */
    public String getMnemonique() {
        return mnemonique;
    }

    /**
     * @param mnemonique
     *            le mn�monique de la formation d'origine
     */
    public void setMnemonique(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    /**
     * @return le code de la sp�cialit� de la formation d'origine
     */
    public String getCodeSpecialite() {
        return codeSpecialite;
    }

    /**
     * @param codeSpecialite
     *            le code de la sp�cialit� de la formation d'origine
     */
    public void setCodeSpecialite(String codeSpecialite) {
        this.codeSpecialite = codeSpecialite;
    }

    /**
     * @return the formation
     */
    public Formation getFormation() {
        return formation;
    }

    /**
     * @param formation
     *            the formation to set
     */
    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the parametresFormationAccueil
     */
    public ParametresFormationAccueil getParametresFormationAccueil() {
        return parametresFormationAccueil;
    }

    /**
     * @param parametresFormationAccueil
     *            the parametresFormationAccueil to set
     */
    public void setParametresFormationAccueil(ParametresFormationAccueil parametresFormationAccueil) {
        this.parametresFormationAccueil = parametresFormationAccueil;
    }

    @Override
    public String toString() {
        String message = "id paramefa : ";

        if (this.getParametresFormationAccueil() != null) {
            message += this.getParametresFormationAccueil().getId();
        } else {
            message += "''";
        }

        message += ", formation origine : ";
        if (formation != null) {
            message += formation.getId().toPrettyString();
        } else {
            if (mnemonique == null) {
                message += "$";
            } else {
                message += "'" + mnemonique + "'";
            }

            message += ",";
            if (codeSpecialite == null) {
                message += "$";
            } else {
                message += "'" + codeSpecialite + "'";
            }
        }

        message += "; cgOpt1=";

        if (this.getOption1() != null) {
            message += this.getOption1().getCleGestion();
        } else {
            message += "''";
        }
        message += "; cgOpt2=";
        if (this.getOption2() != null) {
            message += this.getOption2().getCleGestion();
        } else {
            message += "''";
        }

        message += "]";

        return message;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof AvisParFormationOrigine)) {
            return false;
        }
        AvisParFormationOrigine avisParMefO = (AvisParFormationOrigine) o;

        // V�rification de formations origine
        if (this.getFormation() == null) {

            // Pas de formation
            if (avisParMefO.getFormation() != null) {
                return false;
            }

            // Il faut avoir �galit� des codes mn�moniques et sp�cialit�
            if (!StringUtils.equals(this.getMnemonique(), avisParMefO.getMnemonique())
                    || !StringUtils.equals(this.getCodeSpecialite(), avisParMefO.getCodeSpecialite())) {
                return false;
            }

        } else {

            // Formation existante
            if (avisParMefO.getFormation() == null) {
                return false;
            }

            // Il faut avoir �galit� des formations
            if (!this.getFormation().equals(avisParMefO.getFormation())) {
                return false;
            }

        }

        // V�rification de l'�galit� de l'option 1
        if ((this.getOption1() == null && avisParMefO.getOption1() != null)
                || (this.getOption1() != null && avisParMefO.getOption1() == null)
                || ((this.getOption1() != null && avisParMefO.getOption1() != null) && !StringUtils
                        .equals(this.getOption1().getCleGestion(), avisParMefO.getOption1().getCleGestion()))) {
            return false;
        }

        // V�rification de l'�galit� de l'option 2
        if ((this.getOption2() == null && avisParMefO.getOption2() != null)
                || (this.getOption2() != null && avisParMefO.getOption2() == null)
                || ((this.getOption2() != null && avisParMefO.getOption2() != null) && !StringUtils
                        .equals(this.getOption2().getCleGestion(), avisParMefO.getOption2().getCleGestion()))) {
            return false;
        }

        // V�rification du param�tre par formation d'accueil
        if (this.getParametresFormationAccueil() != null && (this.getParametresFormationAccueil().getId()
                .longValue() != avisParMefO.getParametresFormationAccueil().getId().longValue())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).append(this.mnemonique).append(this.codeSpecialite)
                .toHashCode();
    }
}
