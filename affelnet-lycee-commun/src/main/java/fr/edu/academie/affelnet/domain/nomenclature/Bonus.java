/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Une classe repr�sentant un bonus.
 */
@Entity
@Table(name = "GN_BONUS")
public class Bonus implements Comparable<Bonus> {
    /** La valeur minimale de bonus. */
    public static final int BONUS_MINIMAL = 0;

    /** La valeur maximale de bonus. */
    public static final int BONUS_MAXIMAL = 99999;

    /** Valeur par d�faut pour les bonus. */
    public static final Integer BONUS_PAR_DEFAUT = 0;

    /** Le type de bonus. */
    @Id
    @Column(name = "CO_BONUS")
    @Enumerated(EnumType.STRING)
    private TypeBonus type;

    /** Le libell� du bonus. */
    @Column(name = "LIL_BONUS")
    private String libelle;

    /** La valeur du bonus. */
    @Column(name = "VAL_BONUS")
    private Integer valeur;

    /** L'ordre d'affichage du bonus. */
    @Column(name = "VAL_ORD")
    private Integer ordre;

    /** Date de cr�ation. */
    @Column(name = "DAC_BONUS")
    private Date dateCreation;

    /** Date de mise � jour. */
    @Column(name = "DAM_BONUS")
    private Date dateMAJ;

    /**
     * Le constructeur par d�faut du Bonus.
     */
    public Bonus() {
        dateCreation = new Date();
    }

    /**
     * @return the type
     */
    public TypeBonus getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(TypeBonus type) {
        this.type = type;
    }

    /**
     * @return the valeur
     */
    public Integer getValeur() {
        return valeur;
    }

    /**
     * @param valeur
     *            the valeur to set
     */
    public void setValeur(Integer valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the ordre
     */
    public Integer getOrdre() {
        return ordre;
    }

    /**
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    /**
     * @return the dateCreation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * @param dateCreation
     *            the dateCreation to set
     */
    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * @return the dateMAJ
     */
    public Date getDateMAJ() {
        return dateMAJ;
    }

    /**
     * @param dateMAJ
     *            the dateMAJ to set
     */
    public void setDateMAJ(Date dateMAJ) {
        this.dateMAJ = dateMAJ;
    }

    /**
     * V�rifie si les valeurs du bonus sont valide.
     * 
     * @return true si la valeur du bonus est valide
     */
    public boolean isValide() {
        return this.valeur >= BONUS_MINIMAL && this.valeur <= BONUS_MAXIMAL;
    }

    /**
     * V�rifie si la valeur fournie est valide comme bonus.
     * 
     * @param valeur
     *            la valeur � tester
     * @return true si la valeur fournie est valide comme bonus
     */
    public static boolean isValide(int valeur) {
        return valeur >= BONUS_MINIMAL && valeur <= BONUS_MAXIMAL;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Bonus)) {
            return false;
        }
        Bonus castOther = (Bonus) other;
        return new EqualsBuilder().append(this.getType(), castOther.getType())
                .append(this.getOrdre(), castOther.getOrdre()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getType()).append(getOrdre()).toHashCode();
    }

    @Override
    public int compareTo(Bonus o) {
        int result = this.getOrdre().compareTo(o.getOrdre());
        if (result != 0) {
            return result;
        }
        return this.getType().compareTo(o.getType());
    }
}
