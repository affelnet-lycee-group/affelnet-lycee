/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Nomenclature repr�sentant un bonus de fili�re. */
public class BonusFiliere extends Datable implements Serializable, FormationAccueilGenerique {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant (g�n�r� automatiquement). */
    private Long id;

    /** Formation d'origine (peut �tre nulle). */
    private Formation formationOrigine;

    /** Mnemonique formation origine. */
    private String mnemoniqueOrigine;

    /** Code sp�cialit� formation origine. */
    private String codeSpecialiteOrigine;

    /** Option origine 1 (peut �tre nulle). */
    private Matiere optionOrigine1;

    /** Option origine 2 (peut �tre nulle). */
    private Matiere optionOrigine2;

    /** Formation d'accueil (peut �tre nulle). */
    private Formation formationAccueil;

    /** Mnemonique formation accueil. */
    private String mnemoniqueAccueil;

    /** Code sp�cialit� formation accueil. */
    private String codeSpecialiteAccueil;

    /** Mati�re de l'enseignement optionnel en 2-GT. */
    private Matiere matiereEnseigOptionnel;

    /** Indicateur d'utilisation du symbole "$" pour l'EO. */
    private boolean isDollarAutorisePourEO = false;

    /** Valeur du bonus. */
    private Integer bonus;

    /** Constructeur par d�faut. */
    public BonusFiliere() {
        super();
    }

    /**
     * Pour la gestion du caract�re '$'.
     * 
     * @param formation
     *            la formation d'origine
     * @param mnemo
     *            le mn�monique
     */
    public void setFormationOrigine(Formation formation, String mnemo) {
        if (formation != null) {
            setFormationOrigine(formation);
            setMnemoniqueOrigine(formation.getId().getMnemonique());
            setCodeSpecialiteOrigine(formation.getId().getCodeSpecialite());
        } else if (mnemo != null) {
            setFormationOrigine(null);
            setMnemoniqueOrigine(mnemo);
            setCodeSpecialiteOrigine(null);
        }
    }

    /**
     * Pour la gestion du caract�re '$'.
     * 
     * @param formation
     *            la formation d'accueil
     * @param mnemo
     *            le mn�monique
     */
    public void setFormationAccueil(Formation formation, String mnemo) {
        if (formation != null) {
            setFormationAccueil(formation);
            setMnemoniqueAccueil(formation.getId().getMnemonique());
            setCodeSpecialiteAccueil(formation.getId().getCodeSpecialite());
        } else if (mnemo != null) {
            setFormationAccueil(null);
            setMnemoniqueAccueil(mnemo);
            setCodeSpecialiteAccueil(null);
        } else {
            setFormationAccueil(null);
            setMnemoniqueAccueil(null);
            setCodeSpecialiteAccueil(null);
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the formationOrigine
     */
    public Formation getFormationOrigine() {
        return formationOrigine;
    }

    /**
     * @param formationOrigine
     *            the formationOrigine to set
     */
    public void setFormationOrigine(Formation formationOrigine) {
        this.formationOrigine = formationOrigine;
    }

    /**
     * @return the mnemoniqueOrigine
     */
    public String getMnemoniqueOrigine() {
        return mnemoniqueOrigine;
    }

    /**
     * @param mnemoniqueOrigine
     *            the mnemoniqueOrigine to set
     */
    public void setMnemoniqueOrigine(String mnemoniqueOrigine) {
        this.mnemoniqueOrigine = mnemoniqueOrigine;
    }

    /**
     * @return the codeSpecialiteOrigine
     */
    public String getCodeSpecialiteOrigine() {
        return codeSpecialiteOrigine;
    }

    /**
     * @param codeSpecialiteOrigine
     *            the codeSpecialiteOrigine to set
     */
    public void setCodeSpecialiteOrigine(String codeSpecialiteOrigine) {
        this.codeSpecialiteOrigine = codeSpecialiteOrigine;
    }

    /**
     * @return the optionOrigine1
     */
    public Matiere getOptionOrigine1() {
        return optionOrigine1;
    }

    /**
     * @param optionOrigine1
     *            the optionOrigine1 to set
     */
    public void setOptionOrigine1(Matiere optionOrigine1) {
        this.optionOrigine1 = optionOrigine1;
    }

    /**
     * @return the optionOrigine2
     */
    public Matiere getOptionOrigine2() {
        return optionOrigine2;
    }

    /**
     * @param optionOrigine2
     *            the optionOrigine2 to set
     */
    public void setOptionOrigine2(Matiere optionOrigine2) {
        this.optionOrigine2 = optionOrigine2;
    }

    @Override
    public Formation getFormationAccueil() {
        return formationAccueil;
    }

    @Override
    public void setFormationAccueil(Formation formationAccueil) {
        this.formationAccueil = formationAccueil;
    }

    @Override
    public String getMnemoniqueAccueil() {
        return mnemoniqueAccueil;
    }

    @Override
    public void setMnemoniqueAccueil(String mnemoniqueAccueil) {
        this.mnemoniqueAccueil = mnemoniqueAccueil;
    }

    @Override
    public String getCodeSpecialiteAccueil() {
        return codeSpecialiteAccueil;
    }

    @Override
    public void setCodeSpecialiteAccueil(String codeSpecialiteAccueil) {
        this.codeSpecialiteAccueil = codeSpecialiteAccueil;
    }

    /**
     * @return the matiereEnseigOptionnel
     */
    @Override
    public Matiere getMatiereEnseigOptionnel() {
        return matiereEnseigOptionnel;
    }

    /**
     * @param matiereEnseigOptionnel
     *            the matiereEnseigOptionnel to set
     */
    @Override
    public void setMatiereEnseigOptionnel(Matiere matiereEnseigOptionnel) {
        this.matiereEnseigOptionnel = matiereEnseigOptionnel;
    }

    @Override
    public boolean isEOGeneriqueAutorise() {
        return this.isDollarAutorisePourEO;
    }

    @Override
    public void setEOGeneriqusAutorise(boolean autorise) {
        this.isDollarAutorisePourEO = autorise;
    }

    /**
     * @return the bonus
     */
    public Integer getBonus() {
        return bonus;
    }

    /**
     * @param bonus
     *            the bonus to set
     */
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        String chaineBonusFiliere = "";

        // Formation d'origine
        if (getFormationOrigine() != null) {
            chaineBonusFiliere += getFormationOrigine().getId().getMnemonique() + " "
                    + getFormationOrigine().getId().getCodeSpecialite();
        } else {
            chaineBonusFiliere += getMnemoniqueOrigine() + " $";
        }
        chaineBonusFiliere += "[";
        chaineBonusFiliere += getOptionOrigine1() != null ? getOptionOrigine1().getCleGestion() : " $ ";
        chaineBonusFiliere += "] , [";
        chaineBonusFiliere += getOptionOrigine2() != null ? getOptionOrigine2().getCleGestion() : " $ ";
        chaineBonusFiliere += "] > ";

        // Formation d'accueil
        if (getFormationAccueil() != null) {
            chaineBonusFiliere += getFormationAccueil().getId().getMnemonique() + " "
                    + getFormationAccueil().getId().getCodeSpecialite();
        } else if (StringUtils.isNotEmpty(getMnemoniqueAccueil())) {
            chaineBonusFiliere += getMnemoniqueAccueil() + " $";
        } else {
            chaineBonusFiliere += "$ $";
        }

        // Enseignement optionnel
        chaineBonusFiliere += "[";
        chaineBonusFiliere += getMatiereEnseigOptionnel() != null ? getMatiereEnseigOptionnel().getCleGestion()
                : " $ ";
        chaineBonusFiliere += "]";

        return chaineBonusFiliere;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof BonusFiliere)) {
            return false;
        }
        BonusFiliere castOther = (BonusFiliere) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
