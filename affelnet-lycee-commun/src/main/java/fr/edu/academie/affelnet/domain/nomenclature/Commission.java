/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.Flag;

/** Commission. */
public class Commission extends Datable implements Serializable {

    /** Taille maximale du code commission. */
    public static final int TAILLE_MAXIMALE_CODE_COMMISSION = 8;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** le code. */
    private String code;

    /** le libelle court. */
    private String libelleCourt;

    /** le libelle long. */
    private String libelleLong;

    /** le flag de prise en compte du bar�me. */
    private String flPam;

    /** le libell� du flag de prise en compte du bar�me. */
    private String libelleFlPam;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Teste la validit� d'un code commission.
     * 
     * @param codeCommission
     *            le code commission � tester
     * @return vrai si le code commission est syntaxiquement valide, sinon faux
     */
    public static boolean estCodeCommissionValide(String codeCommission) {
        return StringUtils.isNotEmpty(codeCommission)
                && (codeCommission.length() <= TAILLE_MAXIMALE_CODE_COMMISSION);
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the flPam
     */
    public String getFlPam() {
        return flPam;
    }

    /**
     * @param flPam
     *            the flPam to set
     */
    public void setFlPam(String flPam) {
        this.flPam = flPam;
    }

    /**
     * @return the libelle of flPam
     */
    public String getLibelleFlPam() {
        return Flag.convertitEnLibelle(flPam);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", getCode()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Commission)) {
            return false;
        }
        Commission castOther = (Commission) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    /**
     * @return vrai si la commission est "avec prise en compte du bar�me", sinon faux
     */
    public boolean estAvecPriseEnCompteBareme() {
        return Flag.OUI.equals(flPam);
    }
}
