/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** D�rogations. */
public class Derogation extends Datable implements Serializable, Comparable<Derogation> {

    /** Code pour la d�rogation "Autre motif". */
    public static final Integer CODE_DEROGATION_AUTRE_MOTIF = 7;

    /** Code pour la d�rogation "Parcours scolaire particulier". */
    public static final Integer CODE_DEROGATION_PARCOURS_SCOLAIRE_PARTICULIER = 4;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Code de la d�rogation. */
    private Integer id;

    /** la valeur du libelle du critere derogation. */
    private String libelle;

    /** la valeur de l'ordre d'importance du critere de derogation. */
    private Integer ordre;

    /** la valeur du bonus de derogation (entier). */
    private Integer bonus;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the ordre
     */
    public Integer getOrdre() {
        return ordre;
    }

    /**
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    /**
     * @return the bonus
     */
    public Integer getBonus() {
        return bonus;
    }

    /**
     * @param bonus
     *            the bonus to set
     */
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).append("bonus", getBonus()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Derogation)) {
            return false;
        }
        Derogation castOther = (Derogation) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId())
                .append(this.getOrdre(), castOther.getOrdre()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).append(getOrdre()).toHashCode();
    }

    @Override
    public int compareTo(Derogation o) {
        int result = this.getOrdre().compareTo(o.getOrdre());
        if (result != 0) {
            return result;
        }
        return this.getId().compareTo(o.getId());
    }
}
