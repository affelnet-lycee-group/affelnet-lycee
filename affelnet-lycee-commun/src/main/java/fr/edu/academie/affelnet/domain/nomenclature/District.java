/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** Districts pour les �tablissements. */
public class District implements Ouvrable, Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Code du district. */
    private String code;

    /** Libell� court. */
    private String libelleCourt;

    /** Libell� long. */
    private String libelleLong;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /** Default constructor. */
    public District() {
    }

    /**
     * Full constructor.
     * 
     * @param code
     *            le code du district
     * @param libelleCourt
     *            le libell� court
     * @param libelleLong
     *            le libell� long
     * @param dateOuverture
     *            la <code>Date</code> d'ouverture
     * @param dateFermeture
     *            la <code>Date</code> de fermeture
     */
    public District(String code, String libelleCourt, String libelleLong, Date dateOuverture, Date dateFermeture) {
        this.code = code;
        this.libelleCourt = libelleCourt;
        this.libelleLong = libelleLong;
        this.setDateOuverture(dateOuverture);
        this.setDateFermeture(dateFermeture);
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the dateOuverture
     */
    public Date getDateOuverture() {
        return dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @return the dateFermeture
     */
    public Date getDateFermeture() {
        return dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", getCode()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof District)) {
            return false;
        }
        District castOther = (District) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }
}
