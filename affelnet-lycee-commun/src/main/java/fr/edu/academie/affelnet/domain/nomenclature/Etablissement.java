/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.evaluation.lsu.DemandeEvaluation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.enumeration.resultats.ConsignesEtatEnum;
import fr.edu.academie.affelnet.enumeration.resultats.TypeConsigneEtabEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Etablissements de l'acad�mie. */
public class Etablissement extends Datable implements Ouvrable, Serializable {

    /**
     * Code interne pour les �tablissements de type 'AFFECTATION'.
     * Il s'agit d'�tablissements cr��s provisoirement pour les besoins de l'affectation
     * en attente de cr�ation au niveau Ramsese.
     */
    public static final String CODE_INTERNE_AFFECTATION = "AFFECTAT";

    /** Code secteur pour un �tablissement PUBLIC. */
    public static final String SECTEUR_PUBLIC = "PU";
    /** Code secteur pour un �tablissement PRIVE. */
    public static final String SECTEUR_PRIVE = "PR";

    /** Type pour les �tablissements priv�s HORS contrat. */
    public static final String TYPE_CONTRAT_HORS_CONTRAT = "10";
    /** Type pour les �tablissements priv�s SOUS contrat. */
    public static final String TYPE_CONTRAT_SOUS_CONTRAT = "30";

    /** Code Minist�re Education Nationale. */
    public static final String CODE_MINISTERE_EDUCATION_NATIONALE = "06";
    /** Code Minist�re Agriculture. */
    public static final String CODE_MINISTERE_AGRICULTURE = "03";
    /** Code Minist�re de la d�fense. */
    public static final String CODE_MINISTERE_DEFENSE = "80";

    /** Latitude maximale. */
    public static final int LATITUDE_MAX = 90;

    /** Longitude maximale. */
    public static final int LONGITUDE_MAX = 180;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant (num�ro UAI / RNE). */
    private String id;

    /** Code interne (�gal au num�ro UAI ou 'AFFECTAT'). */
    private String codeInterne;

    /** Code Minist�re. */
    private String codeMinistere;

    /** D�nomination compl�mentaire. */
    private String denominationComplementaire;

    /** Adresse. */
    private String adresse;

    /** Ville. */
    private String ville;

    /** Libell� de la ville affich� au public. */
    private String libelleVillePublic;

    /** Code postal. */
    private String codePostal;

    /** Type de contrat pour les �tablissements priv�s. */
    private String typeContrat;

    /** Code secteur. */
    private String codeSecteur;

    /** T�l�phone. */
    private String numeroTelephone;

    /** Adresse �lectronique. */
    private String mail;

    /** L'�tablissement poss�de-t-il un internat ? */
    private String flagInternat;

    /** Flag R�seau Ambition R�ussite (RAR). */
    private String flagAmbitionReussite;

    /** Flag saisie valid�e par le chef d'�tablissement. */
    private String flagValidationSaisie;

    /** Flag indiquant si on utilise le libell� ONISEP ou personnalis�. */
    private String flagLibelleOnisep;

    /**
     * Le libell� � afficher au grand public.
     * Peut �tre r�cup�r� de l'ext�rieur ou �tre saisi par l'utilisateur.
     */
    private String libellePublic;

    /** Flag indiquant si on utilise le lien vers la fiche ONISEP ou une URL personnalis�e. */
    private String flagUrlOnisep;

    /** Le lien vers la fiche descriptive de l'�tablissement. */
    private String urlDescription;

    /** Latitude de l'�tablissement. */
    private Double latitude;

    /** Longitude de l'�tablissement. */
    private Double longitude;

    /** Date d'ouverture. */
    private Date dateOuverture;

    /** Date de fermeture. */
    private Date dateFermeture;

    /** Zone g�ographique. */
    private ZoneGeo zoneGeo;

    /** District. */
    private District district;

    /** Bassin. */
    private Bassin zoneBassin;

    /**
     * Type.
     */
    private TypeEtablissement typeEtablissement;

    /**
     * Nature.
     */
    private Nature nature;

    /**
     * CIO de rattachement.
     */
    private Cio cio;

    /**
     * Les �l�ves de l'�tablissements.
     */
    private Set<Eleve> eleves = new HashSet<>(0);

    /**
     * Les consignes de l'�tablissement
     */
    private Set<Consignes> consignes = new HashSet<>(0);

    /**
     * La demande d'�valuation LSU.
     */
    private DemandeEvaluation demandeEvaluation;
    /**
     * Num�ro de siret
     */
    private String siret;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the codeInterne
     */
    public String getCodeInterne() {
        return codeInterne;
    }

    /**
     * @param codeInterne
     *            the codeInterne to set
     */
    public void setCodeInterne(String codeInterne) {
        this.codeInterne = codeInterne;
    }

    /**
     * @return the codeMinistere
     */
    public String getCodeMinistere() {
        return codeMinistere;
    }

    /**
     * @param codeMinistere
     *            the codeMinistere to set
     */
    public void setCodeMinistere(String codeMinistere) {
        this.codeMinistere = codeMinistere;
    }

    /**
     * @return the denominationComplementaire
     */
    public String getDenominationComplementaire() {
        return denominationComplementaire;
    }

    /**
     * @param denominationComplementaire
     *            the denominationComplementaire to set
     */
    public void setDenominationComplementaire(String denominationComplementaire) {
        this.denominationComplementaire = denominationComplementaire;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse
     *            the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param ville
     *            the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the libelleVillePublic
     */
    public String getLibelleVillePublic() {
        return libelleVillePublic;
    }

    /**
     * @param libelleVillePublic
     *            the libelleVillePublic to set
     */
    public void setLibelleVillePublic(String libelleVillePublic) {
        this.libelleVillePublic = libelleVillePublic;
    }

    /**
     * @return the codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal
     *            the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the typeContrat
     */
    public String getTypeContrat() {
        return typeContrat;
    }

    /**
     * @param typeContrat
     *            the typeContrat to set
     */
    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    /**
     * @return the codeSecteur
     */
    public String getCodeSecteur() {
        return codeSecteur;
    }

    /**
     * @param codeSecteur
     *            the codeSecteur to set
     */
    public void setCodeSecteur(String codeSecteur) {
        this.codeSecteur = codeSecteur;
    }

    /**
     * @return the numeroTelephone
     */
    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    /**
     * @param numeroTelephone
     *            the numeroTelephone to set
     */
    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail
     *            the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return the flagInternat
     */
    public String getFlagInternat() {
        return flagInternat;
    }

    /**
     * @param flagInternat
     *            the flagInternat to set
     */
    public void setFlagInternat(String flagInternat) {
        this.flagInternat = flagInternat;
    }

    /**
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the dateOuverture
     */
    @Override
    public Date getDateOuverture() {
        return dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @return the dateFermeture
     */
    @Override
    public Date getDateFermeture() {
        return dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @return the zoneGeo
     */
    public ZoneGeo getZoneGeo() {
        return zoneGeo;
    }

    /**
     * @param zoneGeo
     *            the zoneGeo to set
     */
    public void setZoneGeo(ZoneGeo zoneGeo) {
        this.zoneGeo = zoneGeo;
    }

    /**
     * @return the district
     */
    public District getDistrict() {
        return district;
    }

    /**
     * @param district
     *            the district to set
     */
    public void setDistrict(District district) {
        this.district = district;
    }

    /**
     * @return the zoneBassin
     */
    public Bassin getZoneBassin() {
        return zoneBassin;
    }

    /**
     * @param zoneBassin
     *            the zoneBassin to set
     */
    public void setZoneBassin(Bassin zoneBassin) {
        this.zoneBassin = zoneBassin;
    }

    /**
     * @return the typeEtablissement
     */
    public TypeEtablissement getTypeEtablissement() {
        return typeEtablissement;
    }

    /**
     * @param typeEtablissement
     *            the typeEtablissement to set
     */
    public void setTypeEtablissement(TypeEtablissement typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    /**
     * @return the nature
     */
    public Nature getNature() {
        return nature;
    }

    /**
     * @param nature
     *            the nature to set
     */
    public void setNature(Nature nature) {
        this.nature = nature;
    }

    /**
     * @return the cio
     */
    public Cio getCio() {
        return cio;
    }

    /**
     * @param cio
     *            the cio to set
     */
    public void setCio(Cio cio) {
        this.cio = cio;
    }

    /**
     * @return Returns the flagValidationSaisie.
     */
    public String getFlagValidationSaisie() {
        return flagValidationSaisie;
    }

    /**
     * @param flagValidationSaisie
     *            The flagValidationSaisie to set.
     */
    public void setFlagValidationSaisie(String flagValidationSaisie) {
        this.flagValidationSaisie = flagValidationSaisie;
    }

    /**
     * @return Returns the flagAmbitionReussite.
     */
    public String getFlagAmbitionReussite() {
        return flagAmbitionReussite;
    }

    /**
     * @param flagAmbitionReussite
     *            The flagAmbitionReussite to set.
     */
    public void setFlagAmbitionReussite(String flagAmbitionReussite) {
        this.flagAmbitionReussite = flagAmbitionReussite;
    }

    /**
     * @return the demandeEvaluation
     */
    public DemandeEvaluation getDemandeEvaluation() {
        return demandeEvaluation;
    }

    /**
     * @param demandeEvaluation
     *            the demandeEvaluation to set
     */
    public void setDemandeEvaluation(DemandeEvaluation demandeEvaluation) {
        this.demandeEvaluation = demandeEvaluation;
    }

    /**
     * @return the libellePublic
     */
    public String getLibellePublic() {
        return libellePublic;
    }

    /**
     * @param libellePublic
     *            the libellePublic to set
     */
    public void setLibellePublic(String libellePublic) {
        this.libellePublic = libellePublic;
    }

    /**
     * @return the urlDescription
     */
    public String getUrlDescription() {
        return urlDescription;
    }

    /**
     * @param urlDescription
     *            the urlDescription to set
     */
    public void setUrlDescription(String urlDescription) {
        this.urlDescription = urlDescription;
    }

    /**
     * @return the flagLibelleOnisep
     */
    public String getFlagLibelleOnisep() {
        return flagLibelleOnisep;
    }

    /**
     * @param flagLibelleOnisep
     *            the flagLibelleOnisep to set
     */
    public void setFlagLibelleOnisep(String flagLibelleOnisep) {
        this.flagLibelleOnisep = flagLibelleOnisep;
    }

    /**
     * @return the flagUrlOnisep
     */
    public String getFlagUrlOnisep() {
        return flagUrlOnisep;
    }

    /**
     * @param flagUrlOnisep
     *            the flagUrlOnisep to set
     */
    public void setFlagUrlOnisep(String flagUrlOnisep) {
        this.flagUrlOnisep = flagUrlOnisep;
    }

    @Override
    public String toString() {
        return getLibelleAffichage();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Etablissement)) {
            return false;
        }
        Etablissement castOther = (Etablissement) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * @return the eleves
     */
    public Set<Eleve> getEleves() {
        return eleves;
    }

    /**
     * @param eleves
     *            the eleves to set
     */
    public void setEleves(Set<Eleve> eleves) {
        this.eleves = eleves;
    }

    public Set<Consignes> getConsignes() {
        return consignes;
    }

    public void setConsignes(Set<Consignes> consignes) {
        this.consignes = consignes;
    }

    /**
     * @return num�ro de SIRET
     */
    public String getSiret() {
        return siret;
    }

    /**
     * @param siret num�ro de SIRET
     */
    public void setSiret(String siret) {
        this.siret = siret;
    }

    public ConsignesEtatEnum getEtatConsignesAccueil() {
        return getEtatConsignes(TypeConsigneEtabEnum.ACCUEIL);
    }

    public ConsignesEtatEnum getEtatConsignesOrigine() {
        return getEtatConsignes(TypeConsigneEtabEnum.ORIGINE);
    }

    private ConsignesEtatEnum getEtatConsignes(TypeConsigneEtabEnum typeConsigne) {
        if (this.consignes != null) {
            for (Consignes curConsigne : this.consignes) {
                if (!typeConsigne.getCode().equalsIgnoreCase(curConsigne.getId().getType())) {
                    continue;
                }
                return curConsigne.getEtat();
            }
        }

        return null;
    }

    /**
     * Teste s'il s'agit d'un �tablissement de l'�ducation Nationale.
     * 
     * @return vrai s'il s'agit d'un �tablissement rattach� au MEN, sinon faux.
     */
    public boolean isEtablissementMen() {
        return Etablissement.CODE_MINISTERE_EDUCATION_NATIONALE.equals(codeMinistere);
    }

    /**
     * @return Le libell� d'affichage d'un �tablissement.
     *         Il est compos� de l'identifiant, le libell� court du type, la d�nomination compl�mentaire et la
     *         ville.
     */
    public String getLibelleAffichage() {
        return idLibelleEtablissement("");
    }

    /**
     * @return Le libell� d'affichage d'un �tablissement.
     *         Il est compos� de l'identifiant, le libell� court du type, la d�nomination compl�mentaire et la
     *         ville.
     */
    public String getLibelleAffichageJson() {
        return idLibelleEtablissement("\n");
    }

    /**
     * M�thode de g�n�ration du libell� de l'�tablissement.
     * 
     * @return le libell� de l'�tablissement
     */
    public String libelleEtablissement() {
        StringWriter sw = new StringWriter();

        if (this.typeEtablissement != null && StringUtils.isNotEmpty(this.typeEtablissement.getLibelleCourt())) {
            sw.write(this.typeEtablissement.getLibelleCourt());
        }

        if (StringUtils.isNotEmpty(this.denominationComplementaire)) {
            sw.write(" " + this.denominationComplementaire);
        }

        if (StringUtils.isNotEmpty(this.ville)) {
            sw.write(" " + this.ville);
        }

        return sw.toString();
    }

    /**
     * M�thode de g�n�ration du libell� de l'�tablissement.
     * 
     * @param sep
     *            le caract�re s�parateur
     * @return le libell� de l'�tablissement
     */
    private String idLibelleEtablissement(String sep) {
        StringWriter sw = new StringWriter();
        sw.write(this.id + sep);
        if (StringUtils.isEmpty(sep)) {
            sw.write(" ");
        }

        sw.write(libelleEtablissement());

        return sw.toString();
    }

    /**
     * Lorsque l'�l�ve est affect� on doit d�terminer le documents d'affectation � lui transmettre selon
     * l'�tablissement d'accueil.
     * 
     * <p>
     * On �dite une <em>notification d'affectation</em> pour les �tablissements publics, hors CFA (centre de
     * formation d'apprentis). Pour tour les autres, on �dite une <em>proposition d'admission</em>.
     * </p>
     * 
     * @return vrai si l'�tablissement est concern� par les propositions d'accueil, sinon faux.
     */
    public boolean utilisePropositionsAdmission() {

        return TypeEtablissement.TYPE_ETABLISSEMENT_CFA.equals(typeEtablissement.getCode())
                || SECTEUR_PRIVE.equals(codeSecteur);
    }

    /** @return vrai si l'�tablissement est un coll�ge du secteur public, sinon faux */
    public boolean estCollegePublic() {

        return SECTEUR_PUBLIC.equals(codeSecteur)
                && TypeEtablissement.TYPE_ETABLISSEMENT_COLLEGE.equals(typeEtablissement.getCode());
    }

    /**
     * M�thode permettant de savoir si un lyc�e est de type Lyc�e professionnel.
     * 
     * @return vrai si l'�tablissement est un lyc�e professionnel
     */
    public boolean estLP() {
        return TypeEtablissement.TYPE_ETABLISSEMENT_LYCEE_PROFESSIONNEL.equals(typeEtablissement.getCode());
    }

    /**
     * M�thode permettant de savoir si un �tablissement est de type public.
     * 
     * @return vrai si l'�tablissement est un �tablissement public
     */
    public boolean estPublic() {
        return SECTEUR_PUBLIC.equalsIgnoreCase(codeSecteur);
    }

    /**
     * M�thode permettant de savoir si un �tablissement est de type priv�.
     * 
     * @return vrai si l'�tablissement est un �tablissement priv�
     */
    public boolean estPrive() {
        return SECTEUR_PRIVE.equalsIgnoreCase(codeSecteur);
    }

    /**
     * M�thode permettant de savoir si un lyc�e est de type EREA.
     * 
     * @return vrai si l'�tablissement est un �tablissement r�gional d'enseignement adapt�
     */
    public boolean estEREA() {
        return TypeEtablissement.TYPE_ETABLISSEMENT_EREA.equals(typeEtablissement.getCode());
    }

    /**
     * M�thode permettant de savoir si un �tablissement est de type public et pas EREA.
     * 
     * @return vrai si l'�tablissement est un �tablissement public et diff�rent de EREA
     */
    public boolean estPublicNonEREA() {
        return estPublic() && !(estEREA());
    }

    /**
     * Extrait la liste des UAI d'une liste d'�tablissements donn�e.
     * 
     * @param etabs
     *            �tablissements
     * @return liste des num�ros UAI.
     */
    public static List<String> extraireListeUai(List<Etablissement> etabs) {
        List<String> uaiEtabATraiter = new ArrayList<>(etabs.size());
        for (Etablissement etablissement : etabs) {
            uaiEtabATraiter.add(etablissement.getId());
        }
        return uaiEtabATraiter;
    }

    /**
     * Teste si l'�tablissement a �t� cr�� sp�cifiquement dans l'application pour les besoins de l'affectation.
     * 
     * <p>
     * Remarque : un �tablissement <em>affectation</em> n'a pas vocation � sortir d'Affelnet-Lyc�e.
     * </p>
     * 
     * @return vrai si l'�tablissement est de type 'Affectation', sinon faux.
     */
    public boolean isAffectation() {
        return Etablissement.CODE_INTERNE_AFFECTATION.equals(codeInterne);
    }

    /**
     * M�thode permettant d'extraire de la cl� le code d�partement.
     * 
     * @return Le code d�partement de l'�tablissement.
     */
    public String getCodeDepartement() {
        return StringUtils.substring(this.id, 0, 3);
    }

    /**
     * Test si les informations publiques de l'�tablissement sont valides.
     * On v�rifie la pr�sence :
     * - du libell� public de l'�tablissement
     * - du libell� public de la ville
     * - de l'URl de la fiche descriptive
     * - de la latitude et de la longitude (et leur coh�rence)
     * 
     * @return true si les informations publiques sont valides
     */
    public boolean isValiditeInformationsPubliques() {
        boolean champsPresent = StringUtils.isNoneBlank(libellePublic, libelleVillePublic, urlDescription)
                && latitude != null && longitude != null;
        if (!champsPresent) {
            return false;
        }

        return isCoordonneesValides();
    }

    /**
     * V�rifie si les coordonn�es sont pr�sentes et dans un intervalle coh�rent.
     * 
     * @return true si les coordonn�es sont valides
     */
    public boolean isCoordonneesValides() {
        return latitude != null && longitude != null && Math.abs(latitude) < LATITUDE_MAX
                && Math.abs(longitude) < LONGITUDE_MAX;
    }
}
