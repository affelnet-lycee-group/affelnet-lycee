/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Formations (MEF). */
public class Formation extends Datable implements Ouvrable, Serializable, FormationGenerique {

    /** Code interne pour les formations de type AFFECTATION. */
    public static final String CODE_INTERNE_AFFECTATION = "AFFECTATION";

    /**
     * Mn�monique pour la formation d'affection INDETERMIN�E.
     * Elle est ajout�e dans la table des nomenclature nationale.
     */
    public static final String MNEMO_INDET = "INDET";

    /**
     * Code sp�cialit� pour la formation d'affection INDETERMIN�E.
     */
    public static final String COSPE_INDET = "99999";

    /**
     * Mn�monique pour la formation d'affection VIE ACTIVE.
     * Elle est ajout�e dans la table des nomenclature nationale.
     */
    public static final String MNEMO_VACTIV = "VACTIV";

    /**
     * Code sp�cialit� pour la formation d'affection VIE ACTIVE.
     */
    public static final String COSPE_VACTIV = "9011";

    /**
     * Mn�monique pour la formation d'affection NON SCOLARIS�.
     * Elle est ajout�e dans la table des nomenclature nationale.
     */
    public static final String MNEMO_NONSC = "NON SC";

    /**
     * Code sp�cialit� pour la formation d'affection NON SCOLARIS�.
     */
    public static final String COSPE_NONSC = "9991";

    /** Mn�monique pour les formations 3EME. */
    public static final String MNEMO_3EME = "3EME";

    /** Mn�monique pour les formations 3EME. Pr�pa Pro */
    public static final String MNEMO_3EME_PP = "3PPRO";

    /** Mn�monique pour les formations 2-GT. */
    public static final String SECONDE_2GT = "2-GT";

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** l'identifiant. */
    private FormationPK id;

    /** le code interne. */
    private String codeInterne;

    /**
     * le libell� long.
     */
    private String libelleLong;

    /**
     * Le domaine-sous domaine de l'onisep.
     */
    private String domainesOnisep;

    /**
     * mot-cl�s onisep.
     */
    private String discipline;
    /**
     * D�bouch�s m�tiers ONISEP
     */
    private String metiersOnisep;
    /**
     * Libell�s de poursuite d'�tudes ONISEP
     */
    private String libellesPoursuiteOnisep;

    /**
     * Codes ROME.
     */
    private String codesRome;

    /**
     * la date d'ouverture.
     */
    private Date dateOuverture;

    /**
     * la date de fermeture.
     */
    private Date dateFermeture;

    /** le MEFSTAT. */
    private MefStat mefStat;

    /**
     * Le code du MefStat4 associ� � la formation.
     * 
     * Remarque : dans le cadre de la persistance, il s'agit d'une valeur calcul�e donc en lecture seule.
     */
    private String codeMefStat4;

    /** Le code permettant de lier le MEF � la nomenclature formation-dipl�me. */
    private String codeFormationDiplome;

    /** Flag indiquant si on utilise le libell� ONISEP ou personnalis�. */
    private String flagLibelleOnisep;

    /**
     * Le libell� � afficher au grand public.
     * Peut �tre r�cup�r� de l'ext�rieur ou �tre saisi par l'utilisateur.
     */
    private String libellePublic;

    /** Flag indiquant si on utilise le lien vers la fiche ONISEP ou une URL personnalis�e. */
    private String flagUrlOnisep;

    /** Le lien vers la fiche descriptive de la formation. */
    private String urlDescription;

    /**
     * @return the id
     */
    @Override
    public FormationPK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(FormationPK id) {
        this.id = id;
    }

    /**
     * @return the codeInterne
     */
    public String getCodeInterne() {
        return codeInterne;
    }

    /**
     * @param codeInterne
     *            the codeInterne to set
     */
    public void setCodeInterne(String codeInterne) {
        this.codeInterne = codeInterne;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     *
     * @return
     */
    public String getDomainesOnisep() {
        return domainesOnisep;
    }

    /**
     *
     * @param domainesOnisep
     */
    public void setDomainesOnisep(String domainesOnisep) {
        this.domainesOnisep = domainesOnisep;
    }

    /**
     *
     * @return
     */
    public String getDiscipline() {
        return discipline;
    }

    /**
     *
     * @param discipline
     */
    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    /**
     * @return the codesRome
     */
    public String getCodesRome() {
        return codesRome;
    }

    /**
     * @param codesRome
     *            the codesRome to set
     */
    public void setCodesRome(String codesRome) {
        this.codesRome = codesRome;
    }

    /**
     * @return the dateOuverture
     */
    @Override
    public Date getDateOuverture() {
        return dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @return the dateFermeture
     */
    @Override
    public Date getDateFermeture() {
        return dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @return the mefStat
     */
    @Override
    public MefStat getMefStat() {
        return mefStat;
    }

    /**
     * @param mefStat
     *            the mefStat to set
     */
    public void setMefStat(MefStat mefStat) {
        this.mefStat = mefStat;
    }

    /**
     * Attention, dans le cadre de la persistance, le code MefStat4 est calcul� donc en lecture seule.
     * 
     * @param codeMefStat4
     *            le code MefStat4 associ� � la formation
     */
    public void setCodeMefStat4(String codeMefStat4) {
        this.codeMefStat4 = codeMefStat4;
    }

    /**
     * @return le code MefStat4 associ� � la formation
     */
    public String getCodeMefStat4() {
        return this.codeMefStat4;
    }

    /**
     * @return the codeFormationDiplome
     */
    public String getCodeFormationDiplome() {
        return codeFormationDiplome;
    }

    /**
     * @param codeFormationDiplome
     *            the codeFormationDiplome to set
     */
    public void setCodeFormationDiplome(String codeFormationDiplome) {
        this.codeFormationDiplome = codeFormationDiplome;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof Formation)) {
            return false;
        }
        Formation castOther = (Formation) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * M�thode indiquant si la formation est une formations d'affectation.
     * 
     * @return
     *         <ul>
     *         <li>true si le code de la formation est AFFECTATION</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean estAffectation() {
        return StringUtils.equals(codeInterne, CODE_INTERNE_AFFECTATION);
    }

    /**
     * @return the flagLibelleOnisep
     */
    public String getFlagLibelleOnisep() {
        return flagLibelleOnisep;
    }

    /**
     * @param flagLibelleOnisep
     *            the flagLibelleOnisep to set
     */
    public void setFlagLibelleOnisep(String flagLibelleOnisep) {
        this.flagLibelleOnisep = flagLibelleOnisep;
    }

    /**
     * @return the flagUrlOnisep
     */
    public String getFlagUrlOnisep() {
        return flagUrlOnisep;
    }

    /**
     * @param flagUrlOnisep
     *            the flagUrlOnisep to set
     */
    public void setFlagUrlOnisep(String flagUrlOnisep) {
        this.flagUrlOnisep = flagUrlOnisep;
    }

    /**
     * Set le nouveau libell� affich� sur le portail.
     *
     * @param libellePublic
     *            Nouveau libell� affich� sur le portail.
     */
    public void setLibellePublic(String libellePublic) {
        this.libellePublic = libellePublic;
    }

    /**
     * R�cup�re le libell� affich� sur le portail.
     *
     * @return Value of le libell� affich� sur le portail.
     */
    public String getLibellePublic() {
        return libellePublic;
    }

    /**
     * R�cup�re l'URL pointant sur la page descriptive de la formation.
     *
     * @return Valeur de l'URL pointant sur une page descriptive de la formation.
     */
    public String getUrlDescription() {
        return urlDescription;
    }

    /**
     * Set l'URL pointant sur une page descriptive de la formation.
     *
     * @param urlDescription
     *            Nouvelle URL pointant sur une page descriptive de la formation.
     */
    public void setUrlDescription(String urlDescription) {
        this.urlDescription = urlDescription;
    }

    /**
     * R�cuperer la valeur des D�bouch�s m�tiers ONISEP
     *
     * @return D�bouch�s m�tiers ONISEP
     */
    public String getMetiersOnisep() {
        return metiersOnisep;
    }

    /**
     * Set D�bouch�s m�tiers ONISEP
     *
     * @param metiersOnisep
     *            D�bouch�s m�tiers ONISEP
     */
    public void setMetiersOnisep(String metiersOnisep) {
        this.metiersOnisep = metiersOnisep;
    }

    /**
     * R�cup�rer la valeur des Libell�s de poursuite d'�tudes ONISEP
     *
     * @return Libell�s de poursuite d'�tudes ONISEP
     */
    public String getLibellesPoursuiteOnisep() {
        return libellesPoursuiteOnisep;
    }

    /**
     * Set Libell�s de poursuite d'�tudes ONISEP
     *
     * @param libellesPoursuiteOnisep
     *            Libell�s de poursuite d'�tudes ONISEP
     */
    public void setLibellesPoursuiteOnisep(String libellesPoursuiteOnisep) {
        this.libellesPoursuiteOnisep = libellesPoursuiteOnisep;
    }

    /**
     * V�rifie si les informations publiques de la formation sont valides.
     *
     * @return true si les informations publiques d ela formation sont valides
     */
    public boolean isValiditeInformationsPubliques() {
        return StringUtils.isNoneBlank(libellePublic, urlDescription);
    }

    /**
     * Indique si la formation est nationale ou acad�mique.
     * 
     * @return true si la nomenclature est nationale
     */
    public boolean isNomenclatureNationale() {
        if (StringUtils.isBlank(codeInterne)) {
            return false;
        }

        return codeInterne.substring(10, 11).equals("0");
    }
}
