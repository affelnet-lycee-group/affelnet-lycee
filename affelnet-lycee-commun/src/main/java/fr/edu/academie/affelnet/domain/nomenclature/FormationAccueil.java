/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

/**
 * Interface pour les beans utilisant des formations d'accueil.
 */
public interface FormationAccueil {

    /**
     * Retourne la <code>Formation</code> d'accueil.
     * 
     * @return la formation d'accueil.
     */
    Formation getFormationAccueil();

    /**
     * Affecte la <code>Formation</code> d'accueil.
     * 
     * @param formationAccueil
     *            la formation d'accueil.
     */
    void setFormationAccueil(Formation formationAccueil);

    /**
     * Retourne la mati�re optionnelle d'accueil
     * 
     * @return la mati�re optionnelle d'accueil
     */
    Matiere getMatiereEnseigOptionnel();

    /**
     * Affecte la mati�re optionnelle � cette formation d'accueil.
     * 
     * @param matiereEnseigOptionnel
     *            Mati�re optionnelle � affecter � cette formation d'accueil.
     */
    void setMatiereEnseigOptionnel(Matiere matiereEnseigOptionnel);

    /**
     * Indique si l'utilisation du symbole "dollar" pour la saisie de l'enseignement optionnel est autoris�e pour
     * ce bean.
     * 
     * @return vrai si "$" est autoris� pour l'EO.
     */
    boolean isEOGeneriqueAutorise();

    /**
     * Positionne l'indicateur d'utilisation du symbole "dollar" pour la saisie de l'enseignement optionnel
     * pour ce bean.
     * 
     * @param autorise
     *            vrai si "$" est autoris� pour l'EO.
     */
    void setEOGeneriqusAutorise(boolean autorise);
}
