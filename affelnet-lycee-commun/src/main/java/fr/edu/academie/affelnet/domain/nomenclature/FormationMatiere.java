/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Formations-Mati�res : Mati�res utilisables en tant qu'enseignements optionnels
 */
public class FormationMatiere implements Ouvrable, Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 7891459498380949417L;

    /** l'identifiant. */
    private FormationMatierePK id;

    /** Le code interne de la mati�re associ�e. */
    private String codeInterneMatiere;

    /** la modalit� d'�lection. */
    private String modaliteElection;

    /** horaire unique. */
    private Double horaireUnique;

    /** horaire mini. */
    private Double horaireMinimum;

    /** horaire maxi. */
    private Double horaireMaximum;

    /** la date d'ouverture. */
    private Date dateOuverture;

    /** la date de fermeture. */
    private Date dateFermeture;

    /** La mat�re associ�e. **/
    private Matiere matiere;

    /**
     * Constructeur.
     * 
     * @param id
     *            cl� primaire.
     * @param matiere
     *            mati�re.
     * @param codeInterneMatiere
     *            code interne mati�re.
     */
    public FormationMatiere(FormationMatierePK id, Matiere matiere, String codeInterneMatiere) {
        this.id = id;
        this.matiere = matiere;
        this.codeInterneMatiere = codeInterneMatiere;
    }

    /** default constructor. */
    public FormationMatiere() {
    }

    /**
     * @return the id
     */
    public FormationMatierePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(FormationMatierePK id) {
        this.id = id;
    }

    /**
     * @return the codeInterneMatiere
     */
    public String getCodeInterneMatiere() {
        return codeInterneMatiere;
    }

    /**
     * @param codeInterneMatiere
     *            the codeInterneMatiere to set
     */
    public void setCodeInterneMatiere(String codeInterneMatiere) {
        this.codeInterneMatiere = codeInterneMatiere;
    }

    /**
     * @return the modaliteElection
     */
    public String getModaliteElection() {
        return modaliteElection;
    }

    /**
     * @param modaliteElection
     *            the modaliteElection to set
     */
    public void setModaliteElection(String modaliteElection) {
        this.modaliteElection = modaliteElection;
    }

    /**
     * @return the horaireUnique
     */
    public Double getHoraireUnique() {
        return horaireUnique;
    }

    /**
     * @param horaireUnique
     *            the horaireUnique to set
     */
    public void setHoraireUnique(Double horaireUnique) {
        this.horaireUnique = horaireUnique;
    }

    /**
     * @return the horaireMinimum
     */
    public Double getHoraireMinimum() {
        return horaireMinimum;
    }

    /**
     * @param horaireMinimum
     *            the horaireMinimum to set
     */
    public void setHoraireMinimum(Double horaireMinimum) {
        this.horaireMinimum = horaireMinimum;
    }

    /**
     * @return the horaireMaximum
     */
    public Double getHoraireMaximum() {
        return horaireMaximum;
    }

    /**
     * @param horaireMaximum
     *            the horaireMaximum to set
     */
    public void setHoraireMaximum(Double horaireMaximum) {
        this.horaireMaximum = horaireMaximum;
    }

    /**
     * @return the dateOuverture
     */
    @Override
    public Date getDateOuverture() {
        return dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @param dateOuverture
     *            the dateOuverture to set
     */
    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture != null ? new Date(dateOuverture.getTime()) : null;
    }

    /**
     * @return the dateFermeture
     */
    @Override
    public Date getDateFermeture() {
        return dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @param dateFermeture
     *            the dateFermeture to set
     */
    public void setDateFermeture(Date dateFermeture) {
        this.dateFermeture = dateFermeture != null ? new Date(dateFermeture.getTime()) : null;
    }

    /**
     * @return the matiere
     */
    public Matiere getMatiere() {
        return matiere;
    }

    /**
     * @param matiere
     *            the matiere to set
     */
    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof FormationMatiere)) {
            return false;
        }
        FormationMatiere castOther = (FormationMatiere) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
