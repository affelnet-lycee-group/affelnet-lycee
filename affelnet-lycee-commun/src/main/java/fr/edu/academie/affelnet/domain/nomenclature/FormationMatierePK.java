/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** La cl� d'une formation-mati�re : mn�mo. MEF + m�tier MEF + code interne mat. */
public class FormationMatierePK implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 7738333016522463592L;

    /** mn�monique formation. */
    private String mnemonique;

    /** code sp�cialit� formation. */
    private String codeSpecialite;

    /** cl� gestion mati�re. */
    private String cleGestionMatiere;

    /** default constructor. */
    public FormationMatierePK() {
    }

    /**
     * Constructeur.
     * 
     * @param mnemonique
     *            mn�monique formation.
     * @param codeSpecialite
     *            code sp�cialit� formation.
     * @param cleGestionMatiere
     *            cl� gestion mati�re.
     */
    public FormationMatierePK(String mnemonique, String codeSpecialite, String cleGestionMatiere) {
        this.mnemonique = mnemonique;
        this.codeSpecialite = codeSpecialite;
        this.cleGestionMatiere = cleGestionMatiere;
    }

    /**
     * @return the mnemonique
     */
    public String getMnemonique() {
        return mnemonique;
    }

    /**
     * @param mnemonique
     *            the mnemonique to set
     */
    public void setMnemonique(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    /**
     * @return the codeSpecialite
     */
    public String getCodeSpecialite() {
        return codeSpecialite;
    }

    /**
     * @param codeSpecialite
     *            the codeSpecialite to set
     */
    public void setCodeSpecialite(String codeSpecialite) {
        this.codeSpecialite = codeSpecialite;
    }

    /**
     * @return the cleGestionMatiere
     */
    public String getCleGestionMatiere() {
        return cleGestionMatiere;
    }

    /**
     * @param cleGestionMatiere
     *            the cleGestionMatiere to set
     */
    public void setCleGestionMatiere(String cleGestionMatiere) {
        this.cleGestionMatiere = cleGestionMatiere;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mnemonique", getMnemonique())
                .append("codeSpecialite", getCodeSpecialite()).append("cleGestionMatiere", getCleGestionMatiere())
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof FormationMatierePK)) {
            return false;
        }
        FormationMatierePK castOther = (FormationMatierePK) other;
        return new EqualsBuilder().append(this.getMnemonique(), castOther.getMnemonique())
                .append(this.getCodeSpecialite(), castOther.getCodeSpecialite())
                .append(this.getCleGestionMatiere(), castOther.getCleGestionMatiere()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getMnemonique()).append(getCodeSpecialite())
                .append(getCleGestionMatiere()).toHashCode();
    }
}
