/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

/**
 * Interface pour les beans utilisant des formations d'origne dans lesquelles ont peut utiliser le symbole dollar.
 */
public interface FormationOrigineGenerique extends FormationOrigine {
    /**
     * @return le mn�monique de la formation d'origne.
     */
    String getMnemoniqueOrigine();

    /**
     * @param mnemoniqueOrigine
     *            le mn�monique de la formation d'origne.
     */
    void setMnemoniqueOrigine(String mnemoniqueOrigine);

    /**
     * @return the code sp�cialit� de la formation d'origne.
     */
    String getCodeSpecialiteOrigine();

    /**
     * @param codeSpecialiteOrigine
     *            the code sp�cialit� de la formation d'origne.
     */
    void setCodeSpecialiteOrigine(String codeSpecialiteOrigine);
}
