/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Formation origine modifi�e. */
public class FormationOrigineModifiee extends Datable implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant (g�n�r� automatiquement). */
    private Long id;

    /** Formation initiale. */
    private Formation formationInitiale;

    /** Option 1 initiale. */
    private Matiere optionInitiale1;

    /** Option 2 initiale. */
    private Matiere optionInitiale2;

    /** Formation finale. */
    private Formation formationFinale;

    /** Option 1 finale. */
    private Matiere optionFinale1;

    /** Option 2 finale. */
    private Matiere optionFinale2;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the formationInitiale
     */
    public Formation getFormationInitiale() {
        return formationInitiale;
    }

    /**
     * @param formationInitiale
     *            the formationInitiale to set
     */
    public void setFormationInitiale(Formation formationInitiale) {
        this.formationInitiale = formationInitiale;
    }

    /**
     * @return the optionInitiale1
     */
    public Matiere getOptionInitiale1() {
        return optionInitiale1;
    }

    /**
     * @param optionInitiale1
     *            the optionInitiale1 to set
     */
    public void setOptionInitiale1(Matiere optionInitiale1) {
        this.optionInitiale1 = optionInitiale1;
    }

    /**
     * @return the optionInitiale2
     */
    public Matiere getOptionInitiale2() {
        return optionInitiale2;
    }

    /**
     * @param optionInitiale2
     *            the optionInitiale2 to set
     */
    public void setOptionInitiale2(Matiere optionInitiale2) {
        this.optionInitiale2 = optionInitiale2;
    }

    /**
     * @return the formationFinale
     */
    public Formation getFormationFinale() {
        return formationFinale;
    }

    /**
     * @param formationFinale
     *            the formationFinale to set
     */
    public void setFormationFinale(Formation formationFinale) {
        this.formationFinale = formationFinale;
    }

    /**
     * @return the optionFinale1
     */
    public Matiere getOptionFinale1() {
        return optionFinale1;
    }

    /**
     * @param optionFinale1
     *            the optionFinale1 to set
     */
    public void setOptionFinale1(Matiere optionFinale1) {
        this.optionFinale1 = optionFinale1;
    }

    /**
     * @return the optionFinale2
     */
    public Matiere getOptionFinale2() {
        return optionFinale2;
    }

    /**
     * @param optionFinale2
     *            the optionFinale2 to set
     */
    public void setOptionFinale2(Matiere optionFinale2) {
        this.optionFinale2 = optionFinale2;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
