/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** La cl� d'une formation : mn�mo + m�tier. */
public class FormationPK implements Serializable {

    /** Identifiant pour les formations 'non scolaris�'. */
    public static final FormationPK MEF_NON_SCOLARISE = new FormationPK("9991", "NON SC");

    /** Identifiant pour les formations 'vie active'. */
    public static final FormationPK MEF_VIE_ACTIVE = new FormationPK("9011", "VACTIV");

    /** Identifiant pour les formations 'ind�termin�e'. */
    public static final FormationPK MEF_INDETERMINE = new FormationPK("99999", "INDET");

    /** Identifiant pour les formations 3�me. */
    public static final FormationPK MEF_3EME = new FormationPK("", "3EME");

    /** Identifiant pour la formation 2-GT de seconde g�n�rale ou technologique (r�forme lyc�e 2019 - Bac 2021). 
     * 
     * Attention, l'espacement est important : Ne pas confondre avec le MEF 2GT est ferm� depuis 2010. 
     */
    public static final FormationPK MEF_2_GT = new FormationPK("", "2-GT");

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 5387054702913079040L;


    /** le code sp�cialit� de formation. */
    private String codeSpecialite;

    /** le mn�monique. */
    private String mnemonique;

    /**
     * Constructeur.
     * 
     * @param codeSpecialite
     *            le m�tier
     * @param mnemonique
     *            le mn�monique
     */
    public FormationPK(String codeSpecialite, String mnemonique) {
        this.codeSpecialite = StringUtils.upperCase(codeSpecialite);
        this.mnemonique = StringUtils.upperCase(mnemonique);
    }

    /** default constructor. */
    public FormationPK() {
    }

    /**
     * @return the codeSpecialite
     */
    public String getCodeSpecialite() {
        return codeSpecialite;
    }

    /**
     * @param codeSpecialite
     *            the codeSpecialite to set
     */
    public void setCodeSpecialite(String codeSpecialite) {
        this.codeSpecialite = codeSpecialite;
    }

    /**
     * @return the mnemonique
     */
    public String getMnemonique() {
        return mnemonique;
    }

    /**
     * @param mnemonique
     *            the mnemonique to set
     */
    public void setMnemonique(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("codeSpecialite", getCodeSpecialite())
                .append("mnemonique", getMnemonique()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof FormationPK)) {
            return false;
        }
        FormationPK castOther = (FormationPK) other;
        return new EqualsBuilder().append(this.getCodeSpecialite(), castOther.getCodeSpecialite())
                .append(this.getMnemonique(), castOther.getMnemonique()).isEquals();
    }

    /**
     * Teste si cet identifiant de formation est �gal � celui fourni tenir compte de la casse.
     * 
     * @param other
     *            autre objet (identifiant de formation) avec lequel comparer
     * @return vrai si les identifiants sont �gaux sans tenir compte de la casse, sinon faux.
     */
    public boolean equalsIgnoreCase(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof FormationPK)) {
            return false;
        }

        FormationPK autreFormationPk = (FormationPK) other;
        String autreMnemonique = autreFormationPk.getMnemonique();
        String autreSpecialite = autreFormationPk.getCodeSpecialite();

        boolean memeMnemonique = ((this.mnemonique == null) && (autreMnemonique == null))
                || ((this.mnemonique != null) && this.mnemonique.equalsIgnoreCase(autreMnemonique));

        boolean memeSpecialite = ((this.codeSpecialite == null) && (autreSpecialite == null))
                || ((this.codeSpecialite != null) && this.codeSpecialite.equalsIgnoreCase(autreSpecialite));

        return memeMnemonique && memeSpecialite;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCodeSpecialite()).append(getMnemonique()).toHashCode();
    }

    /** @return cha�ne simple compos�e du mn�monique et de la sp�cialit� MEF. */
    public String toPrettyString() {
        return "('" + mnemonique + "','" + codeSpecialite + "')";
    }
}
