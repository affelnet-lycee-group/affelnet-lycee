/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Groupe origine pour les �l�ves. */
public class GroupeOrigine extends Datable implements Serializable {

    /**
     * Code du groupe origine 'Autres' qui collecte les �l�ves qui n'ont pas �t� class�s
     * dans un autre groupe origine lors de l'attribution des groupes.
     */
    public static final String CODE_GROUPE_AUTRES = "99";

    /** Secteur public. */
    public static final String SECTEUR_PUBLIC = "PU";
    /** Secteur priv�. */
    public static final String SECTEUR_PRIVE = "PR";
    /** Tous les secteurs. */
    public static final String SECTEUR_TOUS = "$$";

    /** Hors contrat (secteur priv�). */
    public static final String TYPE_CONTRAT_HORS_CONTRAT = "HC";
    /** Sous contrat (secteur priv�). */
    public static final String TYPE_CONTRAT_SOUS_CONTRAT = "CC";
    /** Tous types de contrats (secteur priv�). */
    public static final String TYPE_CONTRAT_TOUS = "$$";

    /** Indicateur acad�mie. */
    public static final String IND_ACA = "A";
    /** Indicateur hors acad�mie. */
    public static final String IND_ACA_HORSACA = "H";

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = -6992310261866423927L;

    /** le code. */
    private String code;

    /** le libell�. */
    private String libelle;

    /** le code public / priv�. */
    private String codePublicPrive;

    /** le code contrat. */
    private String codeContrat;

    /** l'indicateur acad�mie. */
    private String indicateurAcademie;

    /** le coefficient de pond�ration. */
    private Double coefficient;

    /** le nombre d'�l�ves. */
    private Integer nombreEleves;

    /** le code type �tablissement 1. */
    private TypeEtablissement typeEtablissement1;

    /** le code type �tablissement 2. */
    private TypeEtablissement typeEtablissement2;

    /** le code type �tablissement 3. */
    private TypeEtablissement typeEtablissement3;

    /** le code nature 1. */
    private Nature nature1;

    /** le code nature 2. */
    private Nature nature2;

    /** le code nature 3. */
    private Nature nature3;

    /** liste des formations. */
    private List<GroupeOrigineFormation> formations;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the codePublicPrive
     */
    public String getCodePublicPrive() {
        return codePublicPrive;
    }

    /**
     * @param codePublicPrive
     *            the codePublicPrive to set
     */
    public void setCodePublicPrive(String codePublicPrive) {
        this.codePublicPrive = codePublicPrive;
    }

    /**
     * @return the codeContrat
     */
    public String getCodeContrat() {
        return codeContrat;
    }

    /**
     * @param codeContrat
     *            the codeContrat to set
     */
    public void setCodeContrat(String codeContrat) {
        this.codeContrat = codeContrat;
    }

    /**
     * @return the indicateurAcademie
     */
    public String getIndicateurAcademie() {
        return indicateurAcademie;
    }

    /**
     * @param indicateurAcademie
     *            the indicateurAcademie to set
     */
    public void setIndicateurAcademie(String indicateurAcademie) {
        this.indicateurAcademie = indicateurAcademie;
    }

    /**
     * @return the coefficient
     */
    public Double getCoefficient() {
        return coefficient;
    }

    /**
     * @param coefficient
     *            the coefficient to set
     */
    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }

    /**
     * @return the nombreEleves
     */
    public Integer getNombreEleves() {
        return nombreEleves;
    }

    /**
     * @param nombreEleves
     *            the nombreEleves to set
     */
    public void setNombreEleves(Integer nombreEleves) {
        this.nombreEleves = nombreEleves;
    }

    /**
     * @return the typeEtablissement1
     */
    public TypeEtablissement getTypeEtablissement1() {
        return typeEtablissement1;
    }

    /**
     * @param typeEtablissement1
     *            the typeEtablissement1 to set
     */
    public void setTypeEtablissement1(TypeEtablissement typeEtablissement1) {
        this.typeEtablissement1 = typeEtablissement1;
    }

    /**
     * @return the typeEtablissement2
     */
    public TypeEtablissement getTypeEtablissement2() {
        return typeEtablissement2;
    }

    /**
     * @param typeEtablissement2
     *            the typeEtablissement2 to set
     */
    public void setTypeEtablissement2(TypeEtablissement typeEtablissement2) {
        this.typeEtablissement2 = typeEtablissement2;
    }

    /**
     * @return the typeEtablissement3
     */
    public TypeEtablissement getTypeEtablissement3() {
        return typeEtablissement3;
    }

    /**
     * @param typeEtablissement3
     *            the typeEtablissement3 to set
     */
    public void setTypeEtablissement3(TypeEtablissement typeEtablissement3) {
        this.typeEtablissement3 = typeEtablissement3;
    }

    /**
     * @return the nature1
     */
    public Nature getNature1() {
        return nature1;
    }

    /**
     * @param nature1
     *            the nature1 to set
     */
    public void setNature1(Nature nature1) {
        this.nature1 = nature1;
    }

    /**
     * @return the nature2
     */
    public Nature getNature2() {
        return nature2;
    }

    /**
     * @param nature2
     *            the nature2 to set
     */
    public void setNature2(Nature nature2) {
        this.nature2 = nature2;
    }

    /**
     * @return the nature3
     */
    public Nature getNature3() {
        return nature3;
    }

    /**
     * @param nature3
     *            the nature3 to set
     */
    public void setNature3(Nature nature3) {
        this.nature3 = nature3;
    }

    /**
     * @return Returns the formations.
     */
    public List<GroupeOrigineFormation> getFormations() {
        return formations;
    }

    /**
     * @param formations
     *            The formations to set.
     */
    public void setFormations(List<GroupeOrigineFormation> formations) {
        this.formations = formations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", getCode()).toString();
    }

    /** @return description du groupe origine */
    public String toStringPretty() {

        StringBuilder description = new StringBuilder();
        description.append("code ");
        description.append(code);
        if (StringUtils.isNotBlank(libelle)) {
            description.append(" (");
            description.append(libelle);
            description.append(")");
        }
        return description.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof GroupeOrigine)) {
            return false;
        }
        GroupeOrigine castOther = (GroupeOrigine) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    /** @return vrai si le groupe correspond au groupe 'Autres', sinon faux. */
    public boolean estGroupeAutre() {
        return code.equals(CODE_GROUPE_AUTRES);
    }

    /**
     * Liste les groupes origine sous forme d'un tableau en mode texe.
     * 
     * @param groupes
     *            les groupes origine
     * @return un tableau en mode texte listant les groupes et leur effectif
     */
    public static String genererTexteTableauGroupes(List<GroupeOrigine> groupes) {

        StringBuffer chaineTableauGroupe = new StringBuffer("\n\tCode - Nb �l�ves \n");
        chaineTableauGroupe.append("\t-------------------------\n");

        int total = 0;

        for (GroupeOrigine groupe : groupes) {
            total += groupe.getNombreEleves();
            chaineTableauGroupe.append("\t" + groupe.getCode() + "  -  " + groupe.getNombreEleves() + "\n");
        }
        chaineTableauGroupe.append("\t-------------------------\n");
        chaineTableauGroupe.append("\ttotal : ");
        chaineTableauGroupe.append(total);
        return chaineTableauGroupe.toString();

    }
}
