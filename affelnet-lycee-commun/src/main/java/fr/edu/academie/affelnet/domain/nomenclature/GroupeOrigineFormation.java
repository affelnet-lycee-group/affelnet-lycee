/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** Formations pour les groupes origine. */
public class GroupeOrigineFormation implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1484375737574348196L;

    /** Identifiant (g�n�r� automatiquement). */
    private Long id;

    /** Groupe orgine. */
    private GroupeOrigine groupeOrigine;

    /** Formation. */
    private Formation formation;

    /** Mn�monique de la formation. */
    private String mnemonique;

    /** M�tier (code sp�cialit�) de la formation. */
    private String codeSpecialite;

    /** Option origine 1. */
    private Matiere optionOrigine1;

    /** Option origine 2. */
    private Matiere optionOrigine2;

    /** Constructeur par d�faut. */
    public GroupeOrigineFormation() {
    }

    /**
     * Affecte les champs concernant la formation, le mn�monique et le m�tier en
     * fonction de la pr�sence des $.
     * 
     * @param formation
     *            la formation
     * @param mnemo
     *            le mn�monique
     */
    public void setFormation(Formation formation, String mnemo) {
        if (formation != null) {
            setFormation(formation);
            setMnemonique(formation.getId().getMnemonique());
            setCodeSpecialite(formation.getId().getCodeSpecialite());
        } else if (mnemo != null) {
            setFormation(null);
            setMnemonique(mnemo);
            setCodeSpecialite(null);
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the groupeOrigine
     */
    public GroupeOrigine getGroupeOrigine() {
        return groupeOrigine;
    }

    /**
     * @param groupeOrigine
     *            the groupeOrigine to set
     */
    public void setGroupeOrigine(GroupeOrigine groupeOrigine) {
        this.groupeOrigine = groupeOrigine;
    }

    /**
     * @return the formation
     */
    public Formation getFormation() {
        return formation;
    }

    /**
     * @param formation
     *            the formation to set
     */
    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    /**
     * @return the mnemonique
     */
    public String getMnemonique() {
        return mnemonique;
    }

    /**
     * @param mnemonique
     *            the mnemonique to set
     */
    public void setMnemonique(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    /**
     * @return the codeSpecialite
     */
    public String getCodeSpecialite() {
        return codeSpecialite;
    }

    /**
     * @param codeSpecialite
     *            the codeSpecialite to set
     */
    public void setCodeSpecialite(String codeSpecialite) {
        this.codeSpecialite = codeSpecialite;
    }

    /**
     * @return the optionOrigine1
     */
    public Matiere getOptionOrigine1() {
        return optionOrigine1;
    }

    /**
     * @param optionOrigine1
     *            the optionOrigine1 to set
     */
    public void setOptionOrigine1(Matiere optionOrigine1) {
        this.optionOrigine1 = optionOrigine1;
    }

    /**
     * @return the optionOrigine2
     */
    public Matiere getOptionOrigine2() {
        return optionOrigine2;
    }

    /**
     * @param optionOrigine2
     *            the optionOrigine2 to set
     */
    public void setOptionOrigine2(Matiere optionOrigine2) {
        this.optionOrigine2 = optionOrigine2;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof GroupeOrigineFormation)) {
            return false;
        }
        GroupeOrigineFormation castOther = (GroupeOrigineFormation) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
