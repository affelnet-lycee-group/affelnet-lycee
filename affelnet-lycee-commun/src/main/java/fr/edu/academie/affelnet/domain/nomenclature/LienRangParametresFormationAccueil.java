/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Coefficient d'une mati�re / comp�tence (avec le rang associ�) d�fini dans un parametre par formation d'accueil.
 */
public class LienRangParametresFormationAccueil
        implements java.io.Serializable, Comparable<LienRangParametresFormationAccueil> {

    /** Le serial ID. */
    private static final long serialVersionUID = -8608859154210841490L;

    /** La cl� composite comprenant l'identifiant du paramefa et la cl� de gestion de la mati�re. */
    private LienRangParametresFormationAccueilPK id;

    /** La valeur du coefficient. */
    private Integer valCoef;

    /** L'objet rang associ� � ce coefficient de param�tre par formation d'accueil. */
    private Rang rang;

    /** L'objet parametre par formation d'accueil. */
    private ParametresFormationAccueil paramefa;

    /** Cr�e une association (par d�faut) donnant le coefficient par marti�re/comp�tence pour un paramefa. */
    public LienRangParametresFormationAccueil() {
        this.valCoef = Integer.valueOf(0);
    }

    /**
     * Cr�e une association donnant le coefficient par marti�re/comp�tence pour un paramefa.
     * 
     * @param id
     *            La cl� composite de l'association (identifiant paramefa, cl� gestion mati�re)
     */
    public LienRangParametresFormationAccueil(LienRangParametresFormationAccueilPK id) {
        this.id = id;
        this.valCoef = Integer.valueOf(0);
    }

    /**
     * Cr�e une association donnant le coefficient par marti�re / comp�tence pour un paramefa.
     * 
     * @param id
     *            La cl� composite de l'association (identifiant paramefa, cl� gestion mati�re)
     * @param valCoef
     *            La valeur du coefficient.
     */
    public LienRangParametresFormationAccueil(LienRangParametresFormationAccueilPK id, Integer valCoef) {
        this.id = id;
        this.valCoef = valCoef;
    }

    /**
     * @return La cl� composite de l'association (identifiant paramefa, cl� gestion mati�re)
     */
    public LienRangParametresFormationAccueilPK getId() {
        return this.id;
    }

    /**
     * @param id
     *            La cl� composite de l'association (identifiant paramefa, cl� gestion mati�re)
     */
    public void setId(LienRangParametresFormationAccueilPK id) {
        this.id = id;
    }

    /** @return le coefficient de pond�ration pour la mati�re / comp�tence lorsque le paramefa s'applique */
    public Integer getValCoef() {
        return this.valCoef;
    }

    /**
     * @param valCoef
     *            le coefficient de pond�ration pour la mati�re / comp�tence lorsque le paramefa s'applique
     */
    public void setValCoef(Integer valCoef) {
        this.valCoef = valCoef;
    }

    /** @return le rang de la mati�re / comp�tence concern� par le coefficient lorsque le paramefa s'applique */
    public Rang getRang() {
        return rang;
    }

    /**
     * @param rang
     *            le rang de la mati�re / comp�tence concern� par le coefficient lorsque le paramefa s'applique
     */
    public void setRang(Rang rang) {
        this.rang = rang;
    }

    /** @return le parametre par formation d'accueil concern� */
    public ParametresFormationAccueil getParamefa() {
        return paramefa;
    }

    /**
     * @param paramefa
     *            le parametre par formation d'accueil concern�
     */
    public void setParamefa(ParametresFormationAccueil paramefa) {
        this.paramefa = paramefa;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof LienRangParametresFormationAccueil)) {
            return false;
        }
        LienRangParametresFormationAccueil castOther = (LienRangParametresFormationAccueil) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public int compareTo(LienRangParametresFormationAccueil lienRangParametresFormationAccueil) {
        return (this.getRang().getValeur()).compareTo(lienRangParametresFormationAccueil.getRang().getValeur());
    }
}
