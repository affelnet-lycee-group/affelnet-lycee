/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Liens zones g�ographiques. */
public class LienZoneGeo extends Datable implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant (g�n�r� automatiquement). */
    private Long id;

    /** Zone g�ographique d'origine (null si la zone est g�n�rique). */
    private ZoneGeo zoneGeoOrigine;

    /** Code zone g�ographique d'origine (null si la zone est g�n�rique). */
    private String codeZoneGeoOrigine;

    /** Voeu. */
    private Voeu voeu;

    /** Valeur du bonus. */
    private Integer bonus;

    /**
     * Gestion du caract�re '$'.
     * 
     * @param zoneGeo
     *            la zone g�ographique d'origine (null si la zone est g�n�rique)
     * @param codeZoneGeo
     *            le code de la zone g�ographique d'origine (null si la zone est g�n�rique)
     */
    public void setZoneGeoOrigine(ZoneGeo zoneGeo, String codeZoneGeo) {
        if (zoneGeo != null) {
            setZoneGeoOrigine(zoneGeo);
            setCodeZoneGeoOrigine(zoneGeo.getCode());
        } else {
            setZoneGeoOrigine(null);
            setCodeZoneGeoOrigine(codeZoneGeo);
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the zoneGeoOrigine
     */
    public ZoneGeo getZoneGeoOrigine() {
        return zoneGeoOrigine;
    }

    /**
     * @param zoneGeoOrigine
     *            the zoneGeoOrigine to set
     */
    public void setZoneGeoOrigine(ZoneGeo zoneGeoOrigine) {
        this.zoneGeoOrigine = zoneGeoOrigine;
    }

    /**
     * @return the codeZoneGeoOrigine
     */
    public String getCodeZoneGeoOrigine() {
        return codeZoneGeoOrigine;
    }

    /**
     * @param codeZoneGeoOrigine
     *            the codeZoneGeoOrigine to set
     */
    public void setCodeZoneGeoOrigine(String codeZoneGeoOrigine) {
        this.codeZoneGeoOrigine = codeZoneGeoOrigine;
    }

    /**
     * @return the voeu
     */
    public Voeu getVoeu() {
        return voeu;
    }

    /**
     * @param voeu
     *            the voeu to set
     */
    public void setVoeu(Voeu voeu) {
        this.voeu = voeu;
    }

    /**
     * @return the bonus
     */
    public Integer getBonus() {
        return bonus;
    }

    /**
     * @param bonus
     *            the bonus to set
     */
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof LienZoneGeo)) {
            return false;
        }
        LienZoneGeo castOther = (LienZoneGeo) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * Construit une cha�ne descriptive du lien zone g�ographique.
     * 
     * @return cha�ne descriptive du lien
     */
    public String toStringPretty() {
        StringBuffer sbLien = new StringBuffer();
        if (codeZoneGeoOrigine == null) {
            sbLien.append("Lien de toute zone g�ographique");
        } else {
            sbLien.append("Lien de la zone g�ographique ");
            sbLien.append(codeZoneGeoOrigine);
        }

        sbLien.append(" vers l'offre de formation ");
        sbLien.append(voeu.getCode());

        sbLien.append(", bonus = ");
        sbLien.append(bonus);
        return sbLien.toString();
    }

    /**
     * D�termine si l'offre li�e correspond � une offre de secteur.
     * 
     * @return true si l'offre correspond � une offre de secteur
     */
    public boolean isLienVersOffreSecteur() {
        return this.bonus > 0 && this.voeu != null && this.voeu.getVoie() != null
                && Voie.VOIE_ORIENTATION_2GT.equals(this.voeu.getVoie().getCode());
    }
}
