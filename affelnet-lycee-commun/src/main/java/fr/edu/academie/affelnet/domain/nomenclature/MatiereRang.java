/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

/** Rangs pour les mati�res et les comp�tences. */
public class MatiereRang implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = -4484196641248119188L;

    /** la valeur du rang. */
    private Integer valeur;

    /** la valeur du flag autorisation non-not�e ('N' pour non ou 'O' pour oui). */
    private String autorisationNonNote;

    /** la cl� de gestion. */
    private String cleGestion;

    /** le code interne de la mati�re. */
    private String codeInterne;

    /** le libell� court. */
    private String libelleCourt;

    /** le libell� long. */
    private String libelleLong;

    /** Constructeur par d�faut. */
    public MatiereRang() {
    }

    /**
     * @return la valeur du rang
     */
    public Integer getValeur() {
        return valeur;
    }

    /**
     * @param valeur
     *            la valeur du rang
     */
    public void setValeur(Integer valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the autorisationNonNote
     */
    public String getAutorisationNonNote() {
        return autorisationNonNote;
    }

    /**
     * @param autorisationNonNote
     *            the autorisationNonNote to set
     */
    public void setAutorisationNonNote(String autorisationNonNote) {
        this.autorisationNonNote = autorisationNonNote;
    }

    /**
     * @return the cleGestion
     */
    public String getCleGestion() {
        return cleGestion;
    }

    /**
     * @param cleGestion
     *            the cleGestion to set
     */
    public void setCleGestion(String cleGestion) {
        this.cleGestion = cleGestion;
    }

    /**
     * @return le code interne de la mati�re
     */
    public String getCodeInterne() {
        return codeInterne;
    }

    /**
     * @param codeInterne
     *            le code interne de la mati�re
     */
    public void setCodeInterne(String codeInterne) {
        this.codeInterne = codeInterne;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }
}
