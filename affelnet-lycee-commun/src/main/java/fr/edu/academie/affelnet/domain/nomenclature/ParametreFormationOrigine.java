/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Param�tre par formation d'origine. */
public class ParametreFormationOrigine extends Datable implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Long id;

    /** Mn�monique. */
    private String mnemonique;

    /** M�tier. */
    private String codeSpecialite;

    /** Formation origine. */
    private Formation formationOrigine;

    /** Option origine 1. */
    private Matiere option1;

    /** Option origine 2. */
    private Matiere option2;

    /** Valeur du bonu retard scolaire. */
    private Integer bonusRetardScolaire;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the formationOrigine
     */
    public Formation getFormationOrigine() {
        return formationOrigine;
    }

    /**
     * @param formationOrigine
     *            the formationOrigine to set
     */
    public void setFormationOrigine(Formation formationOrigine) {
        this.formationOrigine = formationOrigine;
    }

    /**
     * @return the mnemonique
     */
    public String getMnemonique() {
        return mnemonique;
    }

    /**
     * @param mnemonique
     *            the mnemonique to set
     */
    public void setMnemonique(String mnemonique) {
        this.mnemonique = mnemonique;
    }

    /**
     * @return the codeSpecialite
     */
    public String getCodeSpecialite() {
        return codeSpecialite;
    }

    /**
     * @param codeSpecialite
     *            the codeSpecialite to set
     */
    public void setCodeSpecialite(String codeSpecialite) {
        this.codeSpecialite = codeSpecialite;
    }

    /**
     * @return the option1
     */
    public Matiere getOption1() {
        return option1;
    }

    /**
     * @param option1
     *            the option1 to set
     */
    public void setOption1(Matiere option1) {
        this.option1 = option1;
    }

    /**
     * @return the option2
     */
    public Matiere getOption2() {
        return option2;
    }

    /**
     * @param option2
     *            the option2 to set
     */
    public void setOption2(Matiere option2) {
        this.option2 = option2;
    }

    /**
     * @return the bonusRetardScolaire
     */
    public Integer getBonusRetardScolaire() {
        return bonusRetardScolaire;
    }

    /**
     * @param bonusRetardScolaire
     *            the bonusRetardScolaire to set
     */
    public void setBonusRetardScolaire(Integer bonusRetardScolaire) {
        this.bonusRetardScolaire = bonusRetardScolaire;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mnemonique", getMnemonique()).toString();
    }

    /** @return cha�ne descriptive du param�tre par formation d'origine. */
    public String toPrettyString() {
        StringBuilder descriptionParamefo = new StringBuilder();
        descriptionParamefo.append("paramefo id = ");
        descriptionParamefo.append(id);
        descriptionParamefo.append(", mef = ");
        if (formationOrigine != null) {
            descriptionParamefo.append(formationOrigine.getId().toPrettyString());
        } else {
            descriptionParamefo.append("('");
            descriptionParamefo.append(mnemonique);
            descriptionParamefo.append("','");
            if (codeSpecialite == null) {
                descriptionParamefo.append("$");
            } else {
                descriptionParamefo.append(codeSpecialite);
            }
            descriptionParamefo.append("')");
        }
        descriptionParamefo.append(", option 1 = ");
        if (option1 == null) {
            descriptionParamefo.append("$");
        } else {
            descriptionParamefo.append(option1.getCleGestion());
        }
        descriptionParamefo.append(", option 2 = ");
        if (option2 == null) {
            descriptionParamefo.append("$");
        } else {
            descriptionParamefo.append(option2.getCleGestion());
        }
        descriptionParamefo.append(", bonus retard scolaire = ");
        descriptionParamefo.append(bonusRetardScolaire);
        return descriptionParamefo.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof ParametreFormationOrigine)) {
            return false;
        }
        ParametreFormationOrigine castOther = (ParametreFormationOrigine) other;
        return new EqualsBuilder().append(this.getMnemonique(), castOther.getMnemonique()).isEquals();
    }

    /**
     * Indique si un autre objet est "�gal" � ce param�tre par formation origine, en incluant les options.
     * La relation d'�galit� suit les r�gles de la m�thode "equals" : {{@link #equals(Object)}.
     * 
     * Exemple :
     * 
     * mnemonique='3EME' sp�cialite='' opt1=$ opt2=$ et 3EME
     * 
     * 
     * @param autre
     *            l'autre objet auquel comparer
     * @return vrai si l'objet est le m�me en tenant compte des options, sinon faux
     */
    public boolean equalsAvecOptions(Object autre) {
        if (!(autre instanceof ParametreFormationOrigine)) {
            return false;
        }
        ParametreFormationOrigine autreParamefo = (ParametreFormationOrigine) autre;

        return new EqualsBuilder().append(this.getMnemonique(), autreParamefo.getMnemonique())
                .append(this.getCodeSpecialite(), autreParamefo.getCodeSpecialite())
                .append(this.getOption1(), autreParamefo.getOption1())
                .append(this.getOption2(), autreParamefo.getOption2()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getMnemonique()).toHashCode();
    }
}
