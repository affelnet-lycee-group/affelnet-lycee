/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientDisciplineAcademique;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DomaineSpecialite;

/** Param�tres de l'affectation li�s � la formation d'accueil. */
public class ParametresFormationAccueil extends Datable implements Serializable, FormationAccueil {

    /** Constante pour les modes de bareme notes. */
    public static final String MODE_BAREME_NOTE = "NOTE_RANG";
    /** Constante pour les modes de bareme evaluation. */
    public static final String MODE_BAREME_EVALUATION = "EVAL_COMP";

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant. */
    private Long id;

    /** Formation d'accueil. */
    private Formation formationAccueil;

    /** Mati�re de l'enseignement optionnel en 2-GT. */
    private Matiere matiereEnseigOptionnel;

    /** Indicateur d'utilisation du symbole "$" pour l'EO. */
    private boolean isDollarAutorisePourEO = false;

    /** Bonus pour le doublement automatique. */
    private Integer bonusDoublement;

    /** Indicateur pour la saisie de l'avis du chef d'�tab. / conseil de classe. */
    private String indicateurSaisieAvis;

    /** Indicateur permettant de d�finir si on est sur un bar�me avec note ou �valuation. */
    private String modeBareme;

    /** Les formations d'origines pour lesquelles l'avis est de demand�. */
    private Set<AvisParFormationOrigine> avisParFormationOrigines = new HashSet<>(0);

    /** Liste des coefficients par formation d'accueil pour un rang . */
    private Set<LienRangParametresFormationAccueil> coefficients = new HashSet<>(0);

    /** Liste des coefficients par formation d'accueil pour un champ disciplinaire. */
    private Set<CoefficientDisciplineAcademique> coefficientsDisciplineAca = new HashSet<>(0);

    /** Liste des coefficients par formation d'accueil pour une �valuation compl�mentaire. */
    private Set<CoefficientEvaluationComplementaire> coefficientsEvalCompl = new HashSet<>(0);

    /** Domaine de sp�cialit� . */
    private DomaineSpecialite domaineSpecialite;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Formation getFormationAccueil() {
        return formationAccueil;
    }

    @Override
    public void setFormationAccueil(Formation formationAccueil) {
        this.formationAccueil = formationAccueil;
    }

    /**
     * @return the matiereEnseigOptionnel
     */
    @Override
    public Matiere getMatiereEnseigOptionnel() {
        return matiereEnseigOptionnel;
    }

    /**
     * @param matiereEnseigOptionnel
     *            the matiereEnseigOptionnel to set
     */
    @Override
    public void setMatiereEnseigOptionnel(Matiere matiereEnseigOptionnel) {
        this.matiereEnseigOptionnel = matiereEnseigOptionnel;
    }

    @Override
    public boolean isEOGeneriqueAutorise() {
        return this.isDollarAutorisePourEO;
    }

    @Override
    public void setEOGeneriqusAutorise(boolean autorise) {
        this.isDollarAutorisePourEO = autorise;
    }

    /**
     * @return the bonusDoublement
     */
    public Integer getBonusDoublement() {
        return bonusDoublement;
    }

    /**
     * @param bonusDoublement
     *            the bonusDoublement to set
     */
    public void setBonusDoublement(Integer bonusDoublement) {
        this.bonusDoublement = bonusDoublement;
    }

    /**
     * @return the indicateurSaisieAvis
     */
    public String getIndicateurSaisieAvis() {
        return indicateurSaisieAvis;
    }

    /**
     * @param indicateurSaisieAvis
     *            the indicateurSaisieAvis to set
     */
    public void setIndicateurSaisieAvis(String indicateurSaisieAvis) {
        this.indicateurSaisieAvis = indicateurSaisieAvis;
    }

    @Override
    public String toString() {
        String chainePfa = getId() + " - " + getFormationAccueil().getId().getMnemonique() + " "
                + getFormationAccueil().getId().getCodeSpecialite();
        if (getMatiereEnseigOptionnel() != null) {
            chainePfa += " , [" + getMatiereEnseigOptionnel().getCleGestion() + "]";
        }

        return chainePfa;
    }

    /**
     * @return the avisParFormationOrigines
     */
    public Set<AvisParFormationOrigine> getAvisParFormationOrigines() {
        return avisParFormationOrigines;
    }

    /**
     * @param avisParFormationOrigines
     *            the avisParFormationOrigines to set
     */
    public void setAvisParFormationOrigines(Set<AvisParFormationOrigine> avisParFormationOrigines) {
        this.avisParFormationOrigines = avisParFormationOrigines;
    }

    /**
     * @return the coefficients
     */
    public Set<LienRangParametresFormationAccueil> getCoefficients() {
        return coefficients;
    }

    /**
     * @param coefficients
     *            the coefficients to set
     */
    public void setCoefficients(Set<LienRangParametresFormationAccueil> coefficients) {
        this.coefficients = coefficients;
    }

    /**
     * M�thode permettant de r�cup�rer le mode de bar�me.
     * 
     * @return un mode de bar�me qui �quivaut au constante MODE_BAREME_*
     */
    public String getModeBareme() {
        return modeBareme;
    }

    /**
     * M�thode mettant � jour le mode de bar�me.
     * 
     * @param modeBareme
     *            qui �quivaut au constante MODE_BAREME_*
     */
    public void setModeBareme(String modeBareme) {
        this.modeBareme = modeBareme;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des coefficients des disciplines acad�miques.
     *
     * @return les coefficients des disciplines acad�miques
     */
    public Set<CoefficientDisciplineAcademique> getCoefficientsDisciplineAca() {
        return coefficientsDisciplineAca;
    }

    /**
     * M�thode mettant � jour la liste des coefficients des disciplines acad�miques.
     *
     * @param coefficientsDisciplineAca
     *            les coefficients des disciplines acad�miques
     */
    public void setCoefficientsDisciplineAca(Set<CoefficientDisciplineAcademique> coefficientsDisciplineAca) {
        this.coefficientsDisciplineAca = coefficientsDisciplineAca;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des coefficients des �valuations compl�mentaires.
     * 
     * @return les coefficients des �valuations compl�mentaires
     */
    public Set<CoefficientEvaluationComplementaire> getCoefficientsEvalCompl() {
        return coefficientsEvalCompl;
    }

    /**
     * M�thode mettant � jour la liste des coefficients des �valuations compl�mentaires.
     * 
     * @param coefficientsEvalCompl
     *            les coefficients des �valuations compl�mentaires
     */
    public void setCoefficientsEvalCompl(Set<CoefficientEvaluationComplementaire> coefficientsEvalCompl) {
        this.coefficientsEvalCompl = coefficientsEvalCompl;
    }

    /**
     * M�thode d'ajout de LienRangParametresFormationAccueil.
     * 
     * @param lienRangParametresFormationAccueil
     *            le lien � ajouter
     */
    public void ajouteCoefficient(LienRangParametresFormationAccueil lienRangParametresFormationAccueil) {
        if (lienRangParametresFormationAccueil != null) {
            lienRangParametresFormationAccueil.setParamefa(this);
            this.coefficients.add(lienRangParametresFormationAccueil);
        }
    }

    /**
     * M�thode d'ajout de CoefficientDisciplineAcademique.
     *
     * @param coefficientDisciplineAcademique
     *            le lien � ajouter
     */
    public void ajouteCoefficient(CoefficientDisciplineAcademique coefficientDisciplineAcademique) {
        if (coefficientDisciplineAcademique != null) {
            coefficientDisciplineAcademique.setParamefa(this);
            this.coefficientsDisciplineAca.add(coefficientDisciplineAcademique);
        }
    }

    /**
     * M�thode d'ajout de CoefficientEvaluationComplementaire.
     * 
     * @param coefficientEvaluationComplementaire
     *            le lien � ajouter
     */
    public void ajouteCoefficient(CoefficientEvaluationComplementaire coefficientEvaluationComplementaire) {
        if (coefficientEvaluationComplementaire != null) {
            coefficientEvaluationComplementaire.setParamefa(this);
            this.coefficientsEvalCompl.add(coefficientEvaluationComplementaire);
        }
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof ParametresFormationAccueil)) {
            return false;
        }
        ParametresFormationAccueil castOther = (ParametresFormationAccueil) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /** @return cha�ne descriptive du param�tre par formation d'accueil. */
    public String toPrettyString() {
        StringBuilder descriptionParamefa = new StringBuilder();
        descriptionParamefa.append("paramefa id = ");
        descriptionParamefa.append(id);
        descriptionParamefa.append(", mef = ");
        descriptionParamefa.append(formationAccueil.getId().toPrettyString());
        descriptionParamefa.append(", enseignement 1ou2 = ");
        if (matiereEnseigOptionnel == null) {
            descriptionParamefa.append("$");
        } else {
            descriptionParamefa.append(matiereEnseigOptionnel.getCleGestion());
        }
        return descriptionParamefa.toString();
    }

    /**
     * M�thode permettant de r�cup�rer le domaine de sp�cialit� associ� au param�tre par formation.
     * 
     * @return le domaine de sp�cialit�
     */
    public DomaineSpecialite getDomaineSpecialite() {
        return domaineSpecialite;
    }

    /**
     * M�thode mettant � jour le domaine de sp�cialit�.
     * 
     * @param domaineSpecialite
     *            le domaine de sp�cialit�
     */
    public void setDomaineSpecialite(DomaineSpecialite domaineSpecialite) {
        this.domaineSpecialite = domaineSpecialite;
    }

}
