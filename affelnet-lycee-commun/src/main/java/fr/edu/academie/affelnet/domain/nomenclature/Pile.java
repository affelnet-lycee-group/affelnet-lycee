/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/**
 * Une pile regroupe les donn�es 'dynamiques' associ�es � une offre de formation pour un tour donn�.
 * <p>
 * On y retrouve une partie d�pendante des <em>nomenclatures de l'affectation</em> :
 * </p>
 * <ul>
 * <li>la capacit� d'accueil (d�falqu�e � chaque tour du nombre des �l�ves affect�s)</li>
 * <li>la capacit� d'accueil de r�f�rence</li>
 * <li>la capacit� de carte scolaire (DOS).</li>
 * <li>le nombre maximal d'�l�ves pris en commission (pour les offres sans bar�me).</li>
 * <li>L'indication du <em>param�tre par formation d'accueil</em> qui s'applique (mise en cache)</li>
 * <li>La modalit� de saisie de <em>l'avis du chef d'�tablissement</em> sur les voeux correspondants (mise en
 * cache)</li>
 * </ul>
 * 
 * <p>
 * Apr�s le traitement de <em>calcul des piles</em>, on dispose �galement :
 * </p>
 * <ul>
 * <li>des compteurs de demandes et d'affect�s (suite au classement)</li>
 * <li>des valeurs de bar�me mini, maxi et dernier affect�</li>
 * </ul>
 */
public class Pile extends Datable implements Serializable {

    /** Valeur minimale pouvant �tre utilis�e dans les capacit�s d'une offre de formation. */
    public static final int CAPACITE_MINIMALE = 0;

    /** Valeur maximale pouvant �tre utilis�e dans les capacit�s d'une offre de formation. */
    public static final int CAPACITE_MAXIMALE = 9999;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant de la pile. */
    private PilePK id;

    /** Le code de l'offre de formation associ�e. */
    private String codeVoeu;

    /** L'offre de formation associ�e. */
    private Voeu voeu;

    /** Le bar�me mini (dans le cas d'une offre de formation avec bar�me). */
    private Double baremeMini;

    /** Le bar�me maxi (dans le cas d'une offre de formation avec bar�me). */
    private Double baremeMaxi;

    /** Le bar�me du dernier admis (dans le cas d'une offre de formation avec bar�me). */
    private Double baremeDernierAdmis;

    /** La capacit� carte scolaire (DOS). */
    private Integer capaciteCarteScolaire;

    /** La capacit� d'affectation. */
    private Integer capaciteAffectation;

    /** La capacit� d'affectation de r�f�rence. */
    private Integer capaciteReference;

    /** La capacit� en LS. */
    private Integer capaciteLS;

    /** Le maximum de candidats pris (pour le travail en commission). */
    private Integer maximumP;

    /** Le nombre d'�l�ves qui ont formul� un voeu sur l'offre de formation. */
    private Integer nombreElevesDemandeurs;

    /** Le nombre d'�l�ves admis sur cette offre de formation. */
    private Integer nombreElevesAdmis;

    /**
     * Indicateur pour la saisie de l'avis du chef d'�tablissement.
     * Il s'agit d'un cache li� au Parm�tre par Formation d'Accueil qui s'applique.
     */
    private String indicateurSaisieAvis;

    /**
     * Le param�tre par formation d'accueil qui s'applique (utile lors de la
     * saisie des voeux en �tablissement) pour optimiser les temps d'acc�s.
     */
    private ParametresFormationAccueil parametreFormationAccueil;

    /**
     * @return the id
     */
    public PilePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(PilePK id) {
        this.id = id;
    }

    /**
     * @return le code de l'offre de formation associ�e � la pile
     */
    public String getCodeVoeu() {
        return this.codeVoeu;
    }

    /**
     * @param codeVoeu
     *            le code de l'offre de formation associ�e � la pile
     */
    public void setCodeVoeu(String codeVoeu) {
        this.codeVoeu = codeVoeu;
    }

    /**
     * @return the voeu
     */
    public Voeu getVoeu() {
        return voeu;
    }

    /**
     * @param voeu
     *            the voeu to set
     */
    public void setVoeu(Voeu voeu) {
        this.voeu = voeu;
    }

    /**
     * @return bareme mini
     */
    public Double getBaremeMini() {
        return this.baremeMini;
    }

    /**
     * @param baremeMini
     *            bareme mini
     */
    public void setBaremeMini(Double baremeMini) {
        this.baremeMini = baremeMini;
    }

    /**
     * @return bareme maxi
     */
    public Double getBaremeMaxi() {
        return this.baremeMaxi;
    }

    /**
     * @param baremeMaxi
     *            bareme maxi
     */
    public void setBaremeMaxi(Double baremeMaxi) {
        this.baremeMaxi = baremeMaxi;
    }

    /**
     * @return bareme du dernier admis
     */
    public Double getBaremeDernierAdmis() {
        return this.baremeDernierAdmis;
    }

    /**
     * @param baremeDernierAdmis
     *            bareme du dernier admis
     */
    public void setBaremeDernierAdmis(Double baremeDernierAdmis) {
        this.baremeDernierAdmis = baremeDernierAdmis;
    }

    /**
     * @return capacit� DOS
     */
    public Integer getCapaciteCarteScolaire() {
        return this.capaciteCarteScolaire;
    }

    /**
     * @param capaciteCarteScolaire
     *            capacit� DOS
     */
    public void setCapaciteCarteScolaire(Integer capaciteCarteScolaire) {
        this.capaciteCarteScolaire = capaciteCarteScolaire;
    }

    /**
     * @return capacit� d'affectation
     */
    public Integer getCapaciteAffectation() {
        return capaciteAffectation;
    }

    /**
     * @param capaciteAffectation
     *            capacit� d'affectation
     */
    public void setCapaciteAffectation(Integer capaciteAffectation) {
        this.capaciteAffectation = capaciteAffectation;
    }

    /**
     * @return the capaciteReference
     */
    public Integer getCapaciteReference() {
        return capaciteReference;
    }

    /**
     * @param capaciteReference
     *            the capaciteReference to set
     */
    public void setCapaciteReference(Integer capaciteReference) {
        this.capaciteReference = capaciteReference;
    }

    /**
     * @return the capaciteLS
     */
    public Integer getCapaciteLS() {
        return capaciteLS;
    }

    /**
     * @param capaciteLS
     *            the capaciteLS to set
     */
    public void setCapaciteLS(Integer capaciteLS) {
        this.capaciteLS = capaciteLS;
    }

    /**
     * @return le max de P (commission)
     */
    public Integer getMaximumP() {
        return this.maximumP;
    }

    /**
     * @param maximumP
     *            le max de P (commission)
     */
    public void setMaximumP(Integer maximumP) {
        this.maximumP = maximumP;
    }

    /**
     * @return le nb de demandes
     */
    public Integer getNombreElevesDemandeurs() {
        return this.nombreElevesDemandeurs;
    }

    /**
     * @param nombreElevesDemandeurs
     *            le nb de demandes
     */
    public void setNombreElevesDemandeurs(Integer nombreElevesDemandeurs) {
        this.nombreElevesDemandeurs = nombreElevesDemandeurs;
    }

    /**
     * @return le nb d'admis
     */
    public Integer getNombreElevesAdmis() {
        return this.nombreElevesAdmis;
    }

    /**
     * @param nombreElevesAdmis
     *            le nb d'admis
     */
    public void setNombreElevesAdmis(Integer nombreElevesAdmis) {
        this.nombreElevesAdmis = nombreElevesAdmis;
    }

    /**
     * @return la capacit� restante sur l'offre de formation
     */
    public Integer getCapaciteRestante() {
        // 09/01/23 : on prend la capacit� d'affectation et non plus la capacit� de la carte scolaire
        int result = 0;
        if (getCapaciteAffectation() != null) {
            result += getCapaciteAffectation();
        }
        if (getNombreElevesAdmis() != null) {
            result -= getNombreElevesAdmis();
        }
        if (result < 0) {
            result = 0;
        }
        return result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("num�ro de tour", getId().getNumeroTour())
                .append("code offre de formation", getId().getCodeOffreFormation()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof Pile)) {
            return false;
        }
        Pile castOther = (Pile) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * @return le param�tre par formation d'accueil
     */
    public ParametresFormationAccueil getParametreFormationAccueil() {
        return parametreFormationAccueil;
    }

    /**
     * @param parametreFormationAccueil
     *            le param�tre par formation d'accueil
     */
    public void setParametreFormationAccueil(ParametresFormationAccueil parametreFormationAccueil) {
        this.parametreFormationAccueil = parametreFormationAccueil;
    }

    /**
     * @return the indicateurSaisieAvis
     */
    public String getIndicateurSaisieAvis() {
        return indicateurSaisieAvis;
    }

    /**
     * @param indicateurSaisieAvis
     *            the indicateurSaisieAvis to set
     */
    public void setIndicateurSaisieAvis(String indicateurSaisieAvis) {
        this.indicateurSaisieAvis = indicateurSaisieAvis;
    }
}
