/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** Cl� primaire pour les piles. */
public class PilePK implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Le num�ro du tour. */
    private Short numeroTour;

    /** Le code de l'offre de formation. **/
    private String codeOffreFormation;

    /** default constructor. */
    public PilePK() {
    }

    /**
     * Le constructeur avec param�tres.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param codeOffreFormation
     *            le code de l'offre de formation
     */
    public PilePK(Short numeroTour, String codeOffreFormation) {
        this.numeroTour = numeroTour;
        this.codeOffreFormation = codeOffreFormation;
    }

    /**
     * @return the numeroTour
     */
    public Short getNumeroTour() {
        return numeroTour;
    }

    /**
     * @param numeroTour
     *            the numeroTour to set
     */
    public void setNumeroTour(Short numeroTour) {
        this.numeroTour = numeroTour;
    }

    /**
     * @return the codeOffreFormation
     */
    public String getCodeOffreFormation() {
        return codeOffreFormation;
    }

    /**
     * @param codeOffreFormation
     *            the codeOffreFormation to set
     */
    public void setCodeOffreFormation(String codeOffreFormation) {
        this.codeOffreFormation = codeOffreFormation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("num�ro de tour", getNumeroTour())
                .append("code offre de formation", getCodeOffreFormation()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof PilePK)) {
            return false;
        }
        PilePK castOther = (PilePK) other;
        return new EqualsBuilder().append(this.getNumeroTour(), castOther.getNumeroTour())
                .append(this.getCodeOffreFormation(), castOther.getCodeOffreFormation()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getNumeroTour()).append(getCodeOffreFormation()).toHashCode();
    }

    /**
     * Donne une repr�sentation de l'identifiant de voeu.
     * 
     * @return description de l'identifiant de voeu
     */
    public String toPrettyString() {
        return "(Code de l'offre de formation : " + codeOffreFormation + ", Num�ro de Tour : " + numeroTour + ")";
    }
}
