/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;

/**
 * Les rangs entrent dans la constitution des bar�mes d'affectation s'appuyant sur des notes des �l�ves.
 * 
 * <ul>
 * <li>Chaque rang est associ� � une <em>mati�re</em> par sa cl� de gestion</li>
 * <li>La <em>valeur</em> du rang permet de retrouver l'indice de la note de la mati�re correspondante dans les
 * donn�e �l�ves. La valeur du rang doit �tre unique dans l'intervalle [0; 20]. La valeur est �galement utilis�e
 * dans les notes liss�es et les coefficients de pond�ration des param�tres par formation d'accueil.</li>
 * <li>Un rang peut �tre d�clar� obligatoire, dans ce cas les �l�ves qui ont des voeux trait�s
 * <em>avec bar�me note</em> doivent avoir une note pour le rang correspondant. Dans le cas contraire, il est dit
 * <em>autoris� non not�</em>.</li>
 * <li>Pour chaque param�tre par formation d'accueil, on peut associer un <em>coefficient</em> multiplicatif pour
 * le rang.</li>
 * </ul>
 */
public class Rang extends Datable implements Serializable {

    /** Nombre maximum de rangs g�r�s dans l'application. */
    public static final int NOMBRE_MAXIMUM = 20;

    /** Valeur maximum que peut prendre le coefficient d'un rang */
    public static final int VALEUR_COEFFICIENT_MAXIMUM = 20;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** L'identifiant technique du rang. */
    private Long id;

    /** La cl� de gestion de la mati�re associ�e au rang. */
    private String cleGestion;

    /** Indice du rang dans les notes des �l�ves, dans l'intervalle [1; 20]. */
    private Integer valeur;

    /** La valeur du flag autorisation non-not�e('N' pour non ou 'O' pour oui). */
    private String autorisationNonNote;

    /** La mati�re � laquelle le rang est associ�. */
    private Matiere matiere;

    /** Les coefficients multiplicatifs associ�s au rang dans les param�tres par formation d'accueil. */
    private Set<LienRangParametresFormationAccueil> coefficients = new HashSet<LienRangParametresFormationAccueil>();

    /** Le champ disciplinaire qui est associ� au rang. */
    private ChampDisciplinaire champDisciplinaire;

    /**
     * @return Indice du rang dans les notes des �l�ves et les coefficients multiplicatifs, dans l'intervalle [1;
     *         20].
     */
    public Integer getValeur() {
        return this.valeur;
    }

    /**
     * @param valeur
     *            Indice du rang dans les notes des �l�ves et les coefficients multiplicatifs
     */
    public void setValeur(Integer valeur) {
        this.valeur = valeur;
    }

    /**
     * @return la valeur du flag autorisation non-not�e('N' pour non ou 'O' pour
     *         oui)
     */
    public String getAutorisationNonNote() {
        return this.autorisationNonNote;
    }

    /**
     * 
     * @param autorisationNonNote
     *            la valeur du flag "Autorisation non not�e"
     */
    public void setAutorisationNonNote(String autorisationNonNote) {
        this.autorisationNonNote = autorisationNonNote;
    }

    /**
     * @return la mati�re � laquelle le rang est associ�
     */
    public Matiere getMatiere() {
        return this.matiere;
    }

    /**
     * @param matiere
     *            la mati�re � laquelle le rang est associ�
     */
    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    /**
     * @return la cl� de gestion de la mati�re associ�e au rang
     */
    public String getCleGestion() {
        return cleGestion;
    }

    /**
     * @param cleGestion
     *            la cl� de gestion de la mati�re associ�e au rang
     */
    public void setCleGestion(String cleGestion) {
        this.cleGestion = cleGestion;
    }

    /**
     * @return les coefficients multiplicatifs associ�s au rang dans les param�tres par formation d'accueil
     */
    public Set<LienRangParametresFormationAccueil> getCoefficients() {
        return coefficients;
    }

    /**
     * @param coefficients
     *            les coefficients multiplicatifs associ�s au rang dans les param�tres par formation d'accueil
     */
    public void setCoefficients(Set<LienRangParametresFormationAccueil> coefficients) {
        this.coefficients = coefficients;
    }

    /**
     * @return l'identifiant technique du rang
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            l'identifiant technique du rang
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param lienRangParametresFormationAccueil
     *            le lien entre rangs et param�tres par formation d'accueil
     */
    public void ajouteCoefficient(LienRangParametresFormationAccueil lienRangParametresFormationAccueil) {
        if (lienRangParametresFormationAccueil != null) {
            lienRangParametresFormationAccueil.setRang(this);
            this.coefficients.add(lienRangParametresFormationAccueil);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cleGestion", getCleGestion()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Rang)) {
            return false;
        }
        Rang castOther = (Rang) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * Redonne le champ disciplinaire.
     * 
     * @return un champ disciplinaire
     */
    public ChampDisciplinaire getChampDisciplinaire() {
        return champDisciplinaire;
    }

    /**
     * Mise � jour du champ disciplinaire.
     * 
     * @param champDisciplinaire
     *            associ� au rang
     */
    public void setChampDisciplinaire(ChampDisciplinaire champDisciplinaire) {
        this.champDisciplinaire = champDisciplinaire;
    }

}
