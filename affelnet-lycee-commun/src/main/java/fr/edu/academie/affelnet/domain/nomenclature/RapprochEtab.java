/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.utils.ChaineUtils;

/** Rapprochement entre �tablissements. */
public class RapprochEtab extends Datable implements Serializable, FormationAccueilGenerique, FormationOrigineGenerique {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Identifiant (g�n�r� automatiquement). */
    private Long id;

    /** La formation d'accueil (peut �tre nulle). */
    private Formation formationAccueil;

    /** La formation d'origine (peut �tre nulle). */
    private Formation formationOrigine;

    /** Le mnemonique de la formation d'accueil. */
    private String mnemoniqueAccueil;

    /** Le code sp�cialit� de la formation d'accueil. */
    private String codeSpecialiteAccueil;

    /** Le mnemonique de la formation d'origine. */
    private String mnemoniqueOrigine;

    /** Le code sp�cialit� de la formation d'origine. */
    private String codeSpecialiteOrigine;

    /** Mati�re de l'enseignement optionnel d'accueil en 2-GT.  */
    private Matiere matiereEnseigOptionnelAccueil;


    /** Indicateur d'utilisation du symbole "$" pour l'EO. */
    private boolean isDollarAutorisePourEO = false;

    /** L'�tablissement d'accueil. */
    private Etablissement etablissementAccueil;

    /** L'�tablissement d'origine (peut �tre null). */
    private Etablissement etablissementOrigine;

    /** L'id de l'�tablissement d'origine. */
    private String idEtablissementOrigine;

    /** Valeur du bonus. */
    private Integer bonus;

    /** Constructeur par d�faut. */
    public RapprochEtab() {
        super();
    }

    /**
     * Affecte l'�tablissement d'origine.
     * 
     * @param etab
     *            l'�tablissement d'origine.
     * @param codeMenDep
     *            le code men du d�partement.
     */
    public void setEtablissementOrigine(Etablissement etab, String codeMenDep) {
        if (etab != null) {
            setEtablissementOrigine(etab);
            setIdEtablissementOrigine(etab.getId());
        } else if (StringUtils.isNotEmpty(codeMenDep)) {
            setEtablissementOrigine(null);
            setIdEtablissementOrigine(codeMenDep);
        } else {
            setEtablissementOrigine(null);
            setIdEtablissementOrigine(null);
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Formation getFormationAccueil() {
        return formationAccueil;
    }

    @Override
    public void setFormationAccueil(Formation formationAccueil) {
        this.formationAccueil = formationAccueil;
    }

    @Override
    public String getMnemoniqueAccueil() {
        return mnemoniqueAccueil;
    }

    @Override
    public void setMnemoniqueAccueil(String mnemoniqueAccueil) {
        this.mnemoniqueAccueil = mnemoniqueAccueil;
    }

    @Override
    public Formation getFormationOrigine() {
        return this.formationOrigine;
    }

    @Override
    public void setFormationOrigine(Formation formationOrigine) {
        this.formationOrigine = formationOrigine;
    }

    /**
     * @return the matiereEnseigOptionnel
     */
    @Override
    public Matiere getMatiereEnseigOptionnel() {
        return matiereEnseigOptionnelAccueil;
    }

    /**
     * @param matiereEnseigOptionnel
     *            the matiereEnseigOptionnel to set
     */
    @Override
    public void setMatiereEnseigOptionnel(Matiere matiereEnseigOptionnel) {
        this.matiereEnseigOptionnelAccueil = matiereEnseigOptionnel;
    }

    @Override
    public String getCodeSpecialiteAccueil() {
        return codeSpecialiteAccueil;
    }

    @Override
    public void setCodeSpecialiteAccueil(String codeSpecialiteAccueil) {
        this.codeSpecialiteAccueil = codeSpecialiteAccueil;
    }

    @Override
    public boolean isEOGeneriqueAutorise() {
        return this.isDollarAutorisePourEO;
    }

    @Override
    public void setEOGeneriqusAutorise(boolean autorise) {
        this.isDollarAutorisePourEO = autorise;
    }

    /**
     * @return the etablissementAccueil
     */
    public Etablissement getEtablissementAccueil() {
        return etablissementAccueil;
    }

    /**
     * @param etablissementAccueil
     *            the etablissementAccueil to set
     */
    public void setEtablissementAccueil(Etablissement etablissementAccueil) {
        this.etablissementAccueil = etablissementAccueil;
    }

    /**
     * @return the etablissementOrigine
     */
    public Etablissement getEtablissementOrigine() {
        return etablissementOrigine;
    }

    /**
     * @param etablissementOrigine
     *            the etablissementOrigine to set
     */
    public void setEtablissementOrigine(Etablissement etablissementOrigine) {
        this.etablissementOrigine = etablissementOrigine;
    }

    /**
     * @return the idEtablissementOrigine
     */
    public String getIdEtablissementOrigine() {
        return idEtablissementOrigine;
    }

    /**
     * @param idEtablissementOrigine
     *            the idEtablissementOrigine to set
     */
    public void setIdEtablissementOrigine(String idEtablissementOrigine) {
        this.idEtablissementOrigine = idEtablissementOrigine;
    }

    @Override
    public String getMnemoniqueOrigine() {
        return this.mnemoniqueOrigine;
    }

    @Override
    public void setMnemoniqueOrigine(String mnemoniqueOrigine) {
        this.mnemoniqueOrigine = mnemoniqueOrigine;
    }

    @Override
    public String getCodeSpecialiteOrigine() {
        return this.codeSpecialiteOrigine;
    }

    @Override
    public void setCodeSpecialiteOrigine(String codeSpecialiteOrigine) {
        this.codeSpecialiteOrigine = codeSpecialiteOrigine;
    }

    /**
     * @return the bonus
     */
    public Integer getBonus() {
        return bonus;
    }

    /**
     * @param bonus
     *            the bonus to set
     */
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        StringBuilder descriptionRapEtab = new StringBuilder();

        descriptionRapEtab.append("RapEtab #");
        descriptionRapEtab.append(id);

        descriptionRapEtab.append(", etabOrigine = ");
        if (etablissementOrigine != null) {
            descriptionRapEtab.append(etablissementOrigine.getId());
        } else {
            descriptionRapEtab.append(ChaineUtils.valeurOuGeneriqueSiNull(idEtablissementOrigine));
        }
        descriptionRapEtab.append(", formOrigine = ");
        if (formationOrigine != null) {
            descriptionRapEtab.append(formationOrigine.getId().toPrettyString());

        } else {

            descriptionRapEtab.append("( ");
            descriptionRapEtab.append(ChaineUtils.valeurOuGeneriqueSiNull(mnemoniqueOrigine));
            descriptionRapEtab.append(", ");
            descriptionRapEtab.append(ChaineUtils.valeurOuGeneriqueSiNull(codeSpecialiteOrigine));
            descriptionRapEtab.append(")");
        }

        descriptionRapEtab.append(", etabAccueil = ");
        if (etablissementAccueil != null) {
            descriptionRapEtab.append(etablissementAccueil.getId());
        } else {
            descriptionRapEtab.append("null");
        }

        descriptionRapEtab.append(", formAccueil = ");
        if (formationAccueil != null) {
            descriptionRapEtab.append(formationAccueil.getId().toPrettyString());

        } else {

            descriptionRapEtab.append("( ");
            descriptionRapEtab.append(ChaineUtils.valeurOuGeneriqueSiNull(mnemoniqueAccueil));
            descriptionRapEtab.append(", ");
            descriptionRapEtab.append(ChaineUtils.valeurOuGeneriqueSiNull(codeSpecialiteAccueil));
            descriptionRapEtab.append(")");
        }

        descriptionRapEtab.append(" [");
        if (matiereEnseigOptionnelAccueil != null) {
            descriptionRapEtab.append(matiereEnseigOptionnelAccueil.getCleGestion());
        }
        descriptionRapEtab.append("]");

        descriptionRapEtab.append(", bonus = ");
        descriptionRapEtab.append(bonus);

        return descriptionRapEtab.toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof RapprochEtab)) {
            return false;
        }
        RapprochEtab castOther = (RapprochEtab) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }


}
