/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.utils.exceptions.BusinessException;

/**
 * Le type d'avis.
 */
public class TypeAvis implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /**
     * Enumeration d�finissant les codes des types d'avis existants.
     */
    public enum CodeTypeAvis {

        /** L'avis DSDEN. */
        AVIS_DSDEN("D", "avisDSDEN", "bonusAvisDsden"),

        /** L'avis de gestion. */
        AVIS_DE_GESTION("E", "avisEntretien", "bonusAvisDeGestion"),

        /** L'avis du chef d'�tablissement d'origine. */
        AVIS_DU_CHEF_D_ETABLISSEMENT("C", "avisConseilClasse", "bonusAvisChefEtablissement"),

        /** L'avis passerelle. */
        AVIS_PASSERELLE("P", "avisPasserelle", "bonusAvisPasserelle");

        /**
         * Le code du type d'avis.
         */
        private String code;

        /** Le nom de l'attribut contenant l'avis dans les voeux. */
        private String nomAttributAvisVoeu;

        /** Le nom de l'attribut pour le bonus correspondant dans les r�sultats provisoire. */
        private String nomAttributBonusResultatProvisoire;

        /**
         * Le constructeur de CodeTypeAvis.
         * 
         * @param code
         *            du type d'avis
         * @param nomAttributAvisVoeu
         *            Le nom de l'attribut contenant l'avis dans les voeux
         * @param nomAttributBonusResultatProvisoire
         *            nom de l'attribut pour le bonus correspondant dans les r�sultats provisoires
         */
        private CodeTypeAvis(String code, String nomAttributAvisVoeu, String nomAttributBonusResultatProvisoire) {
            this.code = code;
            this.nomAttributAvisVoeu = nomAttributAvisVoeu;
            this.nomAttributBonusResultatProvisoire = nomAttributBonusResultatProvisoire;
        }

        /**
         * L'accesseur du code du type d'avis.
         *
         * @return le code du type d'avis
         */
        public String getCode() {
            return code;
        }

        /** @return nom de l'attribut contenant l'avis dans les voeux. */
        public String getNomAttributAvisVoeu() {
            return nomAttributAvisVoeu;
        }

        /**
         * @return nom de l'attribut pour le bonus correspondant dans les r�sultats provisoire
         */
        public String getNomAttributBonusResultatProvisoire() {
            return nomAttributBonusResultatProvisoire;
        }

        /**
         * Donne le code du type d'avis correspondant � la cha�ne code demand�e.
         * 
         * Renvoie une exception si le code est inconnu.
         * 
         * @param codeDemande
         *            code demand�
         * @return code du type d'avis
         */
        public static CodeTypeAvis pourCode(String codeDemande) {
            for (CodeTypeAvis constanteTestee : values()) {
                if (constanteTestee.code.equals(codeDemande)) {
                    return constanteTestee;
                }
            }
            throw new BusinessException("Le code de type d'avis " + codeDemande + " n'est pas r�f�renc�.");
        }
    }

    /**
     * Le code du type d'avis.
     */
    private String code;

    /**
     * Un libell� descrivant le type d'avis.
     */
    private String libelle;

    /**
     * Le constructeur par d�faut de TypeAvis.
     */
    public TypeAvis() {
    }

    /**
     * Le constructeur de TypeAvis.
     * 
     * @param code
     *            du type d'avis
     * @param libelle
     *            descrivant le type d'avis
     */
    public TypeAvis(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }

    /**
     * @param code
     *            le nouveau code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * L'accesseur du code du type d'avis.
     * 
     * @return le code du type d'avis
     */
    public String getCode() {
        return code;
    }

    /**
     * @param libelle
     *            le nouveau libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * L'accesseur du libell� du type d'avis.
     * 
     * @return le libell� du type d'avis
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Le libell� du type d'avis avec le premier caract�re en majuscule.
     * 
     * @return le libell� du type d'avis avec le premier caract�re en majuscule
     */
    public String getLibelleAvecMajuscule() {
        return libelle.substring(0, 1).toUpperCase() + libelle.substring(1);
    }

    @Override
    public boolean equals(Object objet) {
        if (this == objet) {
            return true;
        }
        if (!(objet instanceof TypeAvis)) {
            return false;
        }
        TypeAvis autreTypeAvis = (TypeAvis) objet;
        return new EqualsBuilder().append(getCode(), autreTypeAvis.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.code).toHashCode();
    }

    /** @return donne la valeur du code de type d'avis */
    public CodeTypeAvis decode() {
        return CodeTypeAvis.pourCode(code);
    }
}
