/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Type d'offre de formation.
 * 
 * Le type d'une offre d�termine le crit�re de classement � possibles des voeux sur cette offre.
 */
public enum TypeOffre {

    /** Classement des voeux selon des d�cisions prises en commission. */
    COMMISSION("0", "commission", "Commission"),

    /** Classement des voeux selon un bar�me ne prenant pas en compte les notes / �valuations. */
    BAREME_SANS_NOTE_EVAL("1", "sans �val. / note", "Bar�me sans �valuation / note"),

    /** Classement des voeux des offres selon un bar�me prenant en compte les notes / �valuations. */
    BAREME_AVEC_NOTE_EVAL("2", "avec �val. / notes", "Bar�me avec �valuations / notes");

    /** Table des types d'offres pour le d�codage par indicateur. */
    static final Map<String, TypeOffre> TYPE_OFFRE_PAR_INDICATEUR = new HashMap<String, TypeOffre>();

    static {
        for (TypeOffre type : TypeOffre.values()) {
            TYPE_OFFRE_PAR_INDICATEUR.put(type.getIndicateur(), type);
        }
    }

    /** Valeur de l'indicateur. */
    private String indicateur;

    /** Libell� court associ�e au crit�re de classement. */
    private String libelleCourt;

    /** Libell� long associ�e au crit�re de classement. */
    private String libelleLong;

    /**
     * Constructeur du type d'offre.
     * 
     * @param valeur
     *            Valeur de l'indicateur
     * @param libelleCourt
     *            Libell� court associ�e � l'indicateur
     * @param libelleLong
     *            Libell� long associ�e � l'indicateur
     */
    private TypeOffre(String valeur, String libelleCourt, String libelleLong) {
        this.indicateur = valeur;
        this.libelleCourt = libelleCourt;
        this.libelleLong = libelleLong;
    }

    /**
     * @return Valeur de l'indicateur
     */
    public String getIndicateur() {
        return indicateur;
    }

    /**
     * @return Libell� court associ�e au crit�re de classement
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @return Libell� indiquant seulement les informations de bar�me en omettant les commissions
     */
    public String getLibelleBareme() {
        if (this == COMMISSION) {
            return StringUtils.EMPTY;
        }
        return libelleCourt;
    }

    /**
     * @return Libell� long associ�e au crit�re de classement
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * Donne le type d'offre : crit�re de classement correspondant � l'indicateur fourni.
     * 
     * @param indicateurADecoder
     *            la valeur de l'indicateur demand�
     * @return le type d'offre ou null si aucune valeur ne correspond � l'indicateur donn�
     */
    public static TypeOffre getPourIndicateur(String indicateurADecoder) {
        return TYPE_OFFRE_PAR_INDICATEUR.get(indicateurADecoder);
    }

    /** @return table d'association des types d'offres par indicateur. */
    public static Map<String, TypeOffre> mapTypeOffreParIndicateur() {
        return TYPE_OFFRE_PAR_INDICATEUR;
    }

    /**
     * Donne les types d'offre dans l'ordre utilis� historiquement dans la saisie d'une offre.
     * C'est l'ordre inverse de la valeur de l'indicateur.
     * 
     * @return les valeurs de types d'offre dans l'ordre invers�.
     */
    public static TypeOffre[] reversedValues() {
        TypeOffre[] values = values();
        ArrayUtils.reverse(values);
        return values;
    }

}
