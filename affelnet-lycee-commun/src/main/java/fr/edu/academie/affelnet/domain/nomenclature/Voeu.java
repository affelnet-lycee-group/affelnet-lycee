/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.StatutEleve;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import java.io.Serializable;
import java.util.Map;

/** Offre de formation. */
public class Voeu extends Datable implements Serializable, FormationAccueil {

    /** Pr�fixe utilis� pour les offres de formation hors acad�mie : voeux affelnet de recensement. */
    public static final String PREFIXE_OFFRE_FORMATION_INSTANCE = "HAC";

    /** Suffixe utilis� pour les offres de formation hors acad�mie : voeux affelnet de recensement. */
    public static final String SUFFIXE_OFFRE_FORMATION_INSTANCE = "00";

    /** Nombre maxi de voeux (12 - 1 r�serv� pour la proposition d'accueil). */
    public static final int NBMAX_VOEUX_ELEVE = 11;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Code de l'offre de formation (pr�fixe sur 3 caract�res + incr�ment). */
    private String code;

    /** Description (sert pour la recherche d'une offre de formation). */
    private String description;

    /**
     * Indicateur de prise en compte du bar�me.
     * Voir les valeurs des indicateurs au niveau des types d'offre : {@link TypeOffre#getIndicateur()}.
     */
    private String indicateurPam;

    /**
     * Flag d'autorisation pour l'affichage de l'avis de gestion.
     */
    private String flagAvisEnt;

    /** Etablissement (acad�mique). */
    private Etablissement etablissement;

    /** Formation d'accueil. */
    private Formation formationAccueil;

    /** Mati�re de l'enseignement optionnel en 2-GT. */
    private Matiere matiereEnseigOptionnel;

    /** Indicateur d'utilisation du symbole "$" pour l'EO. */
    private boolean isDollarAutorisePourEO = false;

    /** Voie d'orientation (obligatoire en post-3�me). */
    private Voie voie;

    /** Commission. */
    private Commission commission;

    /** Piles (capacit�s, nb demandes ...) par tour. */
    private Map<Short, Pile> piles;

    /** La pile du tour courant. */
    private Pile pile;

    /** Offre de formation de recensement. */
    private String flagRecensement;

    /** Indicateur de saisie autoris�e pour le niveau 3�me. **/
    private String saisieAutorisee3EME;

    /** Indicateur de saisie autoris�e pour le niveau 2nde. **/
    private String saisieAutorisee2NDE;

    /** Flag indiquant si l'offre de formation est r�serv�e aux �l�ves de 3�me SGPA. **/
    private String flagReserve3SEGPA;

    /** L'OPA dans laquelle se situe l'offre de formation pour le tour principal. */
    private OperationProgrammeeAffectation opaTourPrincipal;

    /** Statut de l'offre. */
    private StatutEleve statut;

    /** Visibilit� sur le portail */
    private String visiblePortail;

    /** Campus m�tier */
    private String campusMetier;

    /** Modalit�s particuli�res d'acc�s � l'offre de formation */
    private String modalitesParticulieres;

    /**
     * Flag indiquant si l'offre provient du catalogue apprentissage
     */
    private String flagCatalogueApprentissage;
    private String cleMinistereEducatif;


    /** @return le libell� long de l'offre de formation. */
    public String getLibelleLong() {
        String mnemoniqueFormation = null;
        String codeSpecialiteFormation = null;
        String cleGestionEnsOpt = null;
        String idEtablissement = null;
        String libelleCourtTypeEtablissement = null;
        String denominationComplementaireEtablissement = null;
        String villeEtablissement = null;
        String libelleStatut = null;

        if (this.getFormationAccueil() != null) {
            mnemoniqueFormation = this.getFormationAccueil().getId().getMnemonique();
            codeSpecialiteFormation = this.getFormationAccueil().getId().getCodeSpecialite();
        }
        if (this.getStatut() != null) {
            libelleStatut = this.getStatut().getLibelleLong();
        }
        if (this.getMatiereEnseigOptionnel() != null) {
            cleGestionEnsOpt = this.getMatiereEnseigOptionnel().getCleGestion();
        }
        if (this.getEtablissement() != null) {
            if (this.getEtablissement().getTypeEtablissement() != null) {
                libelleCourtTypeEtablissement = this.getEtablissement().getTypeEtablissement().getLibelleCourt();
            }
            idEtablissement = getEtablissement().getId();
            denominationComplementaireEtablissement = this.getEtablissement().getDenominationComplementaire();
            villeEtablissement = this.getEtablissement().getVille();
        }

        return formaterLibelleLong(mnemoniqueFormation, codeSpecialiteFormation, cleGestionEnsOpt, idEtablissement,
                libelleCourtTypeEtablissement, denominationComplementaireEtablissement, villeEtablissement,
                libelleStatut);
    }

    /**
     * @param mnemoniqueFormation
     *            le mn�monique
     * @param codeSpecialiteFormation
     *            le m�tier (code sp�cialit�)
     * @param cleGestionEnsOpt
     *            la cl� de gestion de l'enseignement optionnem
     * @param idEtablissement
     *            l'�tablissement
     * @param libelleCourtTypeEtablissement
     *            le libell� court du type �tablissement
     * @param denominationComplementaireEtablissement
     *            la d�nomination compl�mentaire de l'�tablissement
     * @param villeEtablissement
     *            la ville de l'�tablissement
     * @param libelleStatut
     *            le libelle du statut
     * @return le libell� long formatt� pour l'offre de formation
     */
    public static String formaterLibelleLong(String mnemoniqueFormation, String codeSpecialiteFormation,
            String cleGestionEnsOpt, String idEtablissement, String libelleCourtTypeEtablissement,
            String denominationComplementaireEtablissement, String villeEtablissement, String libelleStatut) {
        StringBuilder sb = new StringBuilder();

        // formation
        if (StringUtils.isBlank(mnemoniqueFormation)) {
            return "";
        }
        sb.append(mnemoniqueFormation);
        if (StringUtils.isNotBlank(codeSpecialiteFormation)) {
            sb.append(" ");
            sb.append(codeSpecialiteFormation);
        }

        // enseignement optionnel
        if (StringUtils.isNotBlank(cleGestionEnsOpt)) {
            sb.append("(");
            sb.append(cleGestionEnsOpt);
            sb.append(")");
        }

        // Statut
        if (StringUtils.isNotBlank(libelleStatut)) {
            sb.append(" - ");
            sb.append(libelleStatut);
        }

        // etablissement
        if (StringUtils.isNotBlank(libelleCourtTypeEtablissement)
                || StringUtils.isNotBlank(denominationComplementaireEtablissement)
                || StringUtils.isNotBlank(villeEtablissement)) {
            sb.append(" -");
            if (StringUtils.isNotBlank(idEtablissement)) {
                sb.append(" ");
                sb.append(idEtablissement);
            }
            if (StringUtils.isNotBlank(libelleCourtTypeEtablissement)) {
                sb.append(" ");
                sb.append(libelleCourtTypeEtablissement);
            }
            if (StringUtils.isNotBlank(denominationComplementaireEtablissement)) {
                sb.append(" ");
                sb.append(denominationComplementaireEtablissement);
            }
            if (StringUtils.isNotBlank(villeEtablissement)) {
                sb.append(" ");
                sb.append(villeEtablissement);
            }
        }

        return sb.toString();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Donne la valeur de l'indicateur du type de l'offre : {@link TypeOffre#getIndicateur()}.
     * 
     * @return l'indicateur du type de l'offre de formation
     */
    public String getIndicateurPam() {
        return indicateurPam;
    }

    /**
     * Fixe la valeur de l'indicateur pour le type de l'offre : {@link TypeOffre#getIndicateur()}.
     * 
     * @param indicateurPam
     *            Indicateur du type de l'offre de formation
     */
    public void setIndicateurPam(String indicateurPam) {
        this.indicateurPam = indicateurPam;
    }

    /**
     * D�termine le type d'offre associ� selon l'indicateur.
     * 
     * @return le type d'offre associ�
     */
    public TypeOffre typeOffre() {
        return TypeOffre.getPourIndicateur(indicateurPam);
    }

    /**
     * Indique si l'avis de gestion IA est requis lors de la saisie des offres de formation en �tablissement.
     * 
     * @return vrai si on doit proposer l'avis de gestion d'IA, sinon faux
     *         Ce flag ne concerne que le post-3�me, la valeur pour l'entr�e en 1�re est toujours 'N'.
     */
    public String getFlagAvisEnt() {
        return flagAvisEnt;
    }

    /**
     * Indique si l'avis de gestion IA est requis lors de la saisie des offres de formation en �tablissement.
     * 
     * @param flagAvisEnt
     *            vrai si on doit proposer l'avis de gestion d'IA, sinon faux
     *            Ce flag ne concerne que le post-3�me, la valeur pour l'entr�e en 1�re est toujours 'N'.
     */
    public void setFlagAvisEnt(String flagAvisEnt) {
        this.flagAvisEnt = flagAvisEnt;
    }

    /**
     * @return the etablissement
     */
    public Etablissement getEtablissement() {
        return etablissement;
    }

    /**
     * @param etablissement
     *            the etablissement to set
     */
    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    @Override
    public Formation getFormationAccueil() {
        return formationAccueil;
    }

    @Override
    public void setFormationAccueil(Formation formationAccueil) {
        this.formationAccueil = formationAccueil;
    }

    /**
     * @return the matiereEnseigOptionnel
     */
    @Override
    public Matiere getMatiereEnseigOptionnel() {
        return matiereEnseigOptionnel;
    }

    /**
     * @param matiereEnseigOptionnel
     *            the matiereEnseigOptionnel to set
     */
    @Override
    public void setMatiereEnseigOptionnel(Matiere matiereEnseigOptionnel) {
        this.matiereEnseigOptionnel = matiereEnseigOptionnel;
    }

    @Override
    public boolean isEOGeneriqueAutorise() {
        return this.isDollarAutorisePourEO;
    }

    @Override
    public void setEOGeneriqusAutorise(boolean autorise) {
        this.isDollarAutorisePourEO = autorise;
    }

    /**
     * @return the voie
     */
    public Voie getVoie() {
        return voie;
    }

    /**
     * @param voie
     *            the voie to set
     */
    public void setVoie(Voie voie) {
        this.voie = voie;
    }

    /**
     * @return the commission
     */
    public Commission getCommission() {
        return commission;
    }

    /**
     * @param commission
     *            the commission to set
     */
    public void setCommission(Commission commission) {
        this.commission = commission;
    }

    /**
     * @return the flagRecensement
     */
    public String getFlagRecensement() {
        return flagRecensement;
    }

    /**
     * @return the saisieAutorisee3EME
     */
    public String getSaisieAutorisee3EME() {
        return saisieAutorisee3EME;
    }

    /**
     * @param saisieAutorisee3EME
     *            the saisieAutorisee3EME to set
     */
    public void setSaisieAutorisee3EME(String saisieAutorisee3EME) {
        this.saisieAutorisee3EME = saisieAutorisee3EME;
    }

    /**
     * @return the saisieAutorisee2NDE
     */
    public String getSaisieAutorisee2NDE() {
        return saisieAutorisee2NDE;
    }

    /**
     * @param saisieAutorisee2NDE
     *            the saisieAutorisee2NDE to set
     */
    public void setSaisieAutorisee2NDE(String saisieAutorisee2NDE) {
        this.saisieAutorisee2NDE = saisieAutorisee2NDE;
    }

    /**
     * @param flagRecensement
     *            the flagRecensement to set
     */
    public void setFlagRecensement(String flagRecensement) {
        this.flagRecensement = flagRecensement;
    }

    /**
     * @return vrai s'il s'agit d'une offre de formation de recensement, sinon faux.
     */
    public boolean estRecensement() {
        return Flag.OUI.equals(getFlagRecensement());
    }

    /**
     * @return the flagReserve3SEGPA
     */
    public String getFlagReserve3SEGPA() {
        return flagReserve3SEGPA;
    }

    /**
     * @param flagReserve3SEGPA
     *            the flagReserve3SEGPA to set
     */
    public void setFlagReserve3SEGPA(String flagReserve3SEGPA) {
        this.flagReserve3SEGPA = flagReserve3SEGPA;
    }

    /**
     * @return the piles
     */
    public Map<Short, Pile> getPiles() {
        return piles;
    }

    /**
     * @param piles
     *            the piles to set
     */
    public void setPiles(Map<Short, Pile> piles) {
        this.piles = piles;
    }

    /**
     * @return Returns the pile.
     */
    public Pile getPile() {
        return pile;
    }

    /**
     * @param pile
     *            The pile to set.
     */
    public void setPile(Pile pile) {
        this.pile = pile;
    }

    /**
     * @return the statut
     */
    public StatutEleve getStatut() {
        return statut;
    }

    /**
     * @param statut
     *            the statut to set
     */
    public void setStatut(StatutEleve statut) {
        this.statut = statut;
    }

    @Override
    public String toString() {
        String chaineVoeu = getCode();
        if (getFormationAccueil() != null) {
            chaineVoeu += " - " + getFormationAccueil().getId().getMnemonique() + " "
                    + getFormationAccueil().getId().getCodeSpecialite();
        }

        if (getMatiereEnseigOptionnel() != null) {
            chaineVoeu += " , [" + getMatiereEnseigOptionnel().getCleGestion() + "]";
        }
        // Affichage de l'identifiant de l'�tablissement
        if (getEtablissement() != null) {
            chaineVoeu += " - " + getEtablissement().getId();
        }
        return chaineVoeu;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Voeu)) {
            return false;
        }
        Voeu castOther = (Voeu) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    /**
     * @return the opaTourPrincipal
     */
    public OperationProgrammeeAffectation getOpaTourPrincipal() {
        return opaTourPrincipal;
    }

    /**
     * @param opaTourPrincipal
     *            the opaTourPrincipal to set
     */
    public void setOpaTourPrincipal(OperationProgrammeeAffectation opaTourPrincipal) {
        this.opaTourPrincipal = opaTourPrincipal;
    }

    /**
     * M�thode permettant de v�rifier si l'offre de formation est saisissable pour un palier.
     * 
     * @param codePalier
     *            le code du palier
     * @return vrai si l'offre de formation est saisissable pour un palier sp�cifi�, faux sinon
     */
    public boolean saisissablePourPalier(Short codePalier) {
        return Palier.CODE_PALIER_3EME.equals(codePalier) && Flag.OUI.equals(saisieAutorisee3EME)
                || Palier.CODE_PALIER_2NDE.equals(codePalier) && Flag.OUI.equals(saisieAutorisee2NDE);
    }

    /** @return vrai si l'offre de formation est r�serv�e aux �l�ves venant de 3�me SEGPA. */
    public boolean estSaisieReservee3emeSegpa() {
        return Flag.OUI.equals(flagReserve3SEGPA);
    }

    /**
     * Teste si le traitement de cette offre de formation s'effectue en commission.
     * 
     * @return vrai si l'acc�s � cette offre de formation est trait� en commission, sinon faux
     */
    public boolean estTraiteeEnCommission() {
        return TypeOffre.COMMISSION.getIndicateur().equals(indicateurPam);
    }

    /**
     * Teste si l'acc�s � cette offre de formation s'effectue avec un bar�me.
     * 
     * @return vrai si l'acc�s � cette offre de formation s'appuie sur un bar�me, faux si l'acc�s trait� en
     *         commission
     */
    public boolean utiliseBareme() {
        return !TypeOffre.COMMISSION.getIndicateur().equals(indicateurPam);
    }

    /**
     * Teste si l'affectation sur cette offre de formation s'effectue selon un bar�me avec �valuations/notes.
     * 
     * @return vrai si l'offre est de type Bar�me avec �valuations/notes, sinon faux
     */
    public boolean utiliseBaremeNotesEvaluations() {
        return TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur().equals(indicateurPam);
    }

    /**
     * On ne permet pas le for�age des voeux portant sur les offres de recensement et les offres de statut
     * apprentissage trait�es en commission. Dans le cas contraire le forcage est permis, sur le principe.
     * 
     * @return les voeux sur cette offre peuvent ils �tre forc�s, <strong>sur le principe</strong> ?
     */
    public Boolean permetForcage() {

        boolean apprentissageCommission = statut.isApprentissage() && estTraiteeEnCommission();
        return !estRecensement() && !apprentissageCommission;
    }

    /**
     * Teste si l'offre de formation est trait�e comme une offre d'apprentissage dans le reclassement final.
     * Pour cela, le statut doit �tre apprentissage et le bar�me non pris en compte (commission)
     * 
     * @return vrai si l'offre est consid�r� comme une offre de d'apprentissage
     */
    public boolean estReclassementApprentissage() {
        if (statut != null) {
            return statut.isApprentissage() && !utiliseBareme();
        }
        return false;
    }

    /**
     * Campus m�tier.
     *
     * @return Campus m�tier.
     */
    public String getCampusMetier() {
        return campusMetier;
    }

    /**
     * R�cup�re la Visibilit� sur le portail.
     *
     * @return Visibilit� sur le portail.
     */
    public String getVisiblePortail() {
        return visiblePortail;
    }

    /**
     * Set la Visibilit� sur le portail.
     *
     * @param visiblePortail
     *            Nouvelle valeur de Visibilit� sur le portail.
     */
    public void setVisiblePortail(String visiblePortail) {
        this.visiblePortail = visiblePortail;
    }

    /**
     * Set la nouvelle valeur de Campus m�tier.
     *
     * @param campusMetier
     *            Nouvelle valeur du Campus m�tier.
     */
    public void setCampusMetier(String campusMetier) {
        this.campusMetier = campusMetier;
    }

    /**
     * R�cup�re les Modalit�s d'acc�s.
     *
     * @return Modalit�s d'acc�s.
     */
    public String getModalitesParticulieres() {
        return modalitesParticulieres;
    }

    /**
     * Set la nouvelle valeur de Modalit�s d'acc�s.
     *
     * @param modalitesParticulieres
     *            Nouvelle valeur de Modalit�s d'acc�s.
     */
    public void setModalitesParticulieres(String modalitesParticulieres) {
        this.modalitesParticulieres = modalitesParticulieres;
    }

    /**
     * @return the flagCatalogueApprentissage
     */
    public String getFlagCatalogueApprentissage() {
        return flagCatalogueApprentissage;
    }

    /**
     * @param flagCatalogueApprentissage
     *            the flagCatalogueApprentissage to set
     */
    public void setFlagCatalogueApprentissage(String flagCatalogueApprentissage) {
        this.flagCatalogueApprentissage = flagCatalogueApprentissage;
    }

    /**
     * @param cleMinistereEducatif
     */
    public void setCleMinistereEducatif(String cleMinistereEducatif) {
        this.cleMinistereEducatif = cleMinistereEducatif;
    }

    /**
     * @return
     */
    public String getCleMinistereEducatif() {
        return cleMinistereEducatif;
    }
}
