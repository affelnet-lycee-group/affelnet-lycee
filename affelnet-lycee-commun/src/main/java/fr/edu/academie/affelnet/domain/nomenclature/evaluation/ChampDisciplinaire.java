/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;

/**
 * Les champs disciplinaires permettent au niveau des rangs de faire le lien entre la
 * notation des �l�ves du palier seconde et le LSU des �l�ves du paliers troisi�me.
 * 
 * Liens vers la table "GN_CHAMP_DISCIPLINAIRE"
 * 
 */
public class ChampDisciplinaire implements Serializable, Comparable<ChampDisciplinaire> {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(ChampDisciplinaire.class);

    /** Identifiant du code de champ disciplinaire. */
    private String code;

    /** Libell� de champ disciplinaire. */
    private String libelle;

    /** num�ro d'ordre du champ disciplinaire. */
    private int valeurOrdre;

    /**
     * Les coefficients multiplicatifs associ�s au champ disciplinaire dans les param�tres par formation d'accueil.
     */
    private Map<ParametresFormationAccueil, CoefficientDisciplineAcademique> coefficientsDisciplineAca = new HashMap<ParametresFormationAccueil, CoefficientDisciplineAcademique>();

    /**
     * R�cup�ration du code du champ disciplinaire.
     * 
     * @return un code correspondant au champ disciplinaire
     */
    public String getCode() {
        return code;
    }

    /**
     * Mise � jour du code du champ disciplinaire.
     * 
     * @param code
     *            code du champ disciplinaire
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * R�cup�ration du libell� du champ disciplinaire.
     * 
     * @return un libell� associ� au code
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Mise � jour du libell� du champ disciplinaire.
     * 
     * @param libelle
     *            du champ disciplinaire
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * R�cup�ration de la valeur permettant d'ordonner les champs disciplinaires.
     * 
     * @return la valeur ordonn�e
     */
    public int getValeurOrdre() {
        return valeurOrdre;
    }

    /**
     * Mise � jour de l'ordre des champs disciplinaires.
     * 
     * @param valeurOrdre
     *            du champ disciplinaire
     */
    public void setValeurOrdre(int valeurOrdre) {
        this.valeurOrdre = valeurOrdre;
    }

    /**
     * R�cup�ration de la liste des coefficients.
     * 
     * @return la liste des coefficients pour le champ disciplinaire
     */
    public Map<ParametresFormationAccueil, CoefficientDisciplineAcademique> getCoefficientsDisciplineAca() {
        return coefficientsDisciplineAca;
    }

    /**
     * Mise � jour de la liste des coefficients.
     * 
     * @param coefficientsDisciplineAca
     *            les coefficients multiplicatifs associ�s
     */
    public void setCoefficientsDisciplineAca(
            Map<ParametresFormationAccueil, CoefficientDisciplineAcademique> coefficientsDisciplineAca) {
        this.coefficientsDisciplineAca = coefficientsDisciplineAca;
    }

    /**
     * M�thode permettant d'ajouter les coefficients de l'�valuation compl�mentaire.
     * 
     * @param coefficientsDisciplineAca
     *            le coefficient
     */
    public void ajouteCoefficient(CoefficientDisciplineAcademique coefficientsDisciplineAca) {
        LOG.debug("Ajout du coefficient");
        if (coefficientsDisciplineAca != null) {
            coefficientsDisciplineAca.setChampDiscipline(this);
            this.coefficientsDisciplineAca.put(coefficientsDisciplineAca.getParamefa(), coefficientsDisciplineAca);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", getCode()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof ChampDisciplinaire)) {
            return false;
        }
        ChampDisciplinaire castOther = (ChampDisciplinaire) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getValeurOrdre()).append(getCode()).toHashCode();
    }

    @Override
    public int compareTo(ChampDisciplinaire autre) {
        if (autre == null) {
            return -1;
        }
        return Integer.compare(this.valeurOrdre, autre.valeurOrdre);
    }

}
