/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;

/**
 * Classe permettant d'acc�der au information des coefficients des champs disciplinaire
 * que l'on retrouve dans la table GN_COEF_DISCIPLINE_ACA.
 * Cette table fait le lien entre les param�tres par formation d'accueil,
 * leur coefficient et le champ disciplinaire
 *
 */
public class CoefficientDisciplineAcademique implements Serializable, Comparable<CoefficientDisciplineAcademique> {

    /**
     * Taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /** Objet redonnant le couple de code param�tre par formation et champ disciplinaire. */
    private CoefficientDisciplineAcademiquePK coefficientDisciplineAcademiquePK;

    /**
     * Le param�tre par formation d'accueil.
     */
    private ParametresFormationAccueil paramefa;

    /** Le champ disciplinaire. */
    private ChampDisciplinaire champDiscipline;

    /** La valeur du coefficient. */
    private Integer valCoef;

    /**
     * Constructeur par d�faut.
     */
    public CoefficientDisciplineAcademique() {
        this.valCoef = Integer.valueOf(0);
    }

    /**
     * Constructeur permettant de cr�er un lien entre le coefficient initilialis� par d�faut � z�ro,
     * le champ disciplinaire et le param�tre par formation d'accueil.
     * 
     * @param coefficientDisciplineAcademiquePK
     *            le couple de code champ disciplinaire et param�tre par formation
     */
    public CoefficientDisciplineAcademique(CoefficientDisciplineAcademiquePK coefficientDisciplineAcademiquePK) {
        this.valCoef = Integer.valueOf(0);
        this.coefficientDisciplineAcademiquePK = coefficientDisciplineAcademiquePK;
    }

    /**
     * Constructeur permettant de cr�er un lien entre le coefficient initilialis� par d�faut � z�ro,
     * le champ disciplinaire et le param�tre par formation d'accueil.
     * 
     * @param coefficientDisciplineAcademiquePK
     *            le couple de code champ disciplinaire et discipline de sp�cialit�
     * @param valCoef
     *            la valeur du coefficient
     */
    public CoefficientDisciplineAcademique(CoefficientDisciplineAcademiquePK coefficientDisciplineAcademiquePK,
            Integer valCoef) {
        this.coefficientDisciplineAcademiquePK = coefficientDisciplineAcademiquePK;
        this.valCoef = valCoef;
    }

    /**
     * M�thode permettant de retourner la valeur du couple de code de champ disciplinaire et le param�tre par
     * formation d'accueil.
     * 
     * @return l'objet coefficientDisciplineAcademiquePK
     */
    public CoefficientDisciplineAcademiquePK getCoefficientDisciplineAcademiquePK() {
        return coefficientDisciplineAcademiquePK;
    }

    /**
     * M�thode mettant � jour le couple de code du champ disciplinaire et du param�tre du formation accueil.
     * 
     * @param coefficientDisciplineAcademiquePK
     *            le couple de code discipline de sp�cialit� et champ disciplinaire
     */
    public void setCoefficientDisciplineAcademiquePK(
            CoefficientDisciplineAcademiquePK coefficientDisciplineAcademiquePK) {
        this.coefficientDisciplineAcademiquePK = coefficientDisciplineAcademiquePK;
    }

    /**
     * M�thode permettant de r�cup�rer un param�tre de formation accueil.
     * 
     * @return le param�tre de formation accueil
     */
    public ParametresFormationAccueil getParamefa() {
        return paramefa;
    }

    /**
     * M�thode mettant � jour le param�tre de formation accueil.
     * 
     * @param paramefa
     *            le param�tre de formation accueil
     */
    public void setParamefa(ParametresFormationAccueil paramefa) {
        this.paramefa = paramefa;
    }

    /**
     * M�thode permettant de retourner le champ disciplinaire.
     * 
     * @return l'objet champ disciplinaire
     */
    public ChampDisciplinaire getChampDiscipline() {
        return champDiscipline;
    }

    /**
     * M�thode mettant � jour le champ disciplinaire.
     * 
     * @param champDiscipline
     *            l'objet champ disciplinaire
     */
    public void setChampDiscipline(ChampDisciplinaire champDiscipline) {
        this.champDiscipline = champDiscipline;
    }

    /**
     * M�thode permettant de retourner le coefficient.
     * 
     * @return la valeur du coefficient
     */
    public Integer getValCoef() {
        return valCoef;
    }

    /**
     * M�thode mettant � jour le coefficient.
     * 
     * @param valCoef
     *            la valeur du coefficient
     */
    public void setValCoef(Integer valCoef) {
        this.valCoef = valCoef;
    }

    @Override
    public int compareTo(CoefficientDisciplineAcademique o) {
        int val1 = this.getChampDiscipline().getValeurOrdre();
        int val2 = o.getChampDiscipline().getValeurOrdre();
        int result = val1 - val2;
        if (result == 0) {
            result = this.getChampDiscipline().getCode().compareTo(o.getChampDiscipline().getCode());
        }
        return result;
    }

    @Override
    public boolean equals(Object other) {

        if (!(other instanceof CoefficientDisciplineAcademique)) {
            return false;
        }
        CoefficientDisciplineAcademique castOther = (CoefficientDisciplineAcademique) other;
        return new EqualsBuilder()
                .append(getCoefficientDisciplineAcademiquePK(), castOther.getCoefficientDisciplineAcademiquePK())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCoefficientDisciplineAcademiquePK()).toHashCode();
    }
}
