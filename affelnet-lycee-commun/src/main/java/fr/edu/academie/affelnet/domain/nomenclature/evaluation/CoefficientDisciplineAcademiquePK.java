/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Classe permettant d'avoir le couple de primary key pour la classe
 * CoefficientDisciplineAcademique.
 */
public class CoefficientDisciplineAcademiquePK implements Serializable {

    /**
     * Taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /** identifiant de param�tre par formation. */
    private Long idParamefa;

    /** code du champ disciplinaire. */
    private String codeChampDiscipline;

    /**
     * Constructeur par d�faut.
     */
    public CoefficientDisciplineAcademiquePK() {
        super();
    }

    /**
     * Constructeur permettant d'obtenir le couple d'identifiant.
     * 
     * @param idParamefa
     *            identifiant du param�tre par formation accueil
     * @param codeChampDiscipline
     *            code du champ disciplinaire
     */
    public CoefficientDisciplineAcademiquePK(Long idParamefa, String codeChampDiscipline) {
        this.idParamefa = idParamefa;
        this.codeChampDiscipline = codeChampDiscipline;
    }

    /**
     * M�thode permettant de r�cup�rer l'identifiant du param�tre par formation.
     * 
     * @return l'identifiant du param�tre par formation
     */
    public Long getIdParamefa() {
        return idParamefa;
    }

    /**
     * M�thode mettant � jour l'identifiant du param�tre par formation.
     * 
     * @param idParamefa
     *            l'identifiant du param�tre par formation
     */
    public void setIdParamefa(Long idParamefa) {
        this.idParamefa = idParamefa;
    }

    /**
     * M�thode permettant de r�cup�rer le code du champ disciplinaire.
     * 
     * @return le code du champ disciplinaire
     */
    public String getCodeChampDiscipline() {
        return codeChampDiscipline;
    }

    /**
     * M�thode mettant � jour le code du champ disciplinaire.
     * 
     * @param codeChampDiscipline
     *            le code
     */
    public void setCodeChampDiscipline(String codeChampDiscipline) {
        this.codeChampDiscipline = codeChampDiscipline;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("IdParamefa:", getIdParamefa())
                .append("CodeChampDiscipline", getCodeChampDiscipline()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof CoefficientDisciplineAcademiquePK)) {
            return false;
        }
        CoefficientDisciplineAcademiquePK castOther = (CoefficientDisciplineAcademiquePK) other;
        return new EqualsBuilder().append(this.getIdParamefa(), castOther.getIdParamefa())
                .append(getCodeChampDiscipline(), castOther.getCodeChampDiscipline()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIdParamefa()).append(getCodeChampDiscipline()).toHashCode();
    }

}
