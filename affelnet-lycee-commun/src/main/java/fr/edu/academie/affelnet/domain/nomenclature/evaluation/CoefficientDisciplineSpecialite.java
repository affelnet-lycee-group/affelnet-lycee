/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Classe permettant d'acc�der au information des coefficients des disciplines de sp�cialisation
 * que l'on retrouve dans la table GN_COEF_DISCIPLINE_SPECIALITE.
 * Cette table fait le lien entre les disciplines de sp�cialisation,
 * leur coefficient et le champ disciplinaire
 *
 */
public class CoefficientDisciplineSpecialite implements Serializable, Comparable<CoefficientDisciplineSpecialite> {

    /**
     * Taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /** Objet redonnant le couple de code domaine Sp�cialit� et champ disciplinaire. */
    private CoefficientDisciplineSpecialitePK coefficientDisciplineSpecialitePK;

    /**
     * Le domaine sp�cialit�.
     */
    private DomaineSpecialite domaineSpecialite;

    /** Le champ disciplinaire. */
    private ChampDisciplinaire champDiscipline;

    /** La valeur du coefficient. */
    private Integer valCoef;

    /**
     * Constructeur par d�faut.
     */
    public CoefficientDisciplineSpecialite() {
        this.valCoef = Integer.valueOf(0);
    }

    /**
     * Constructeur permettant de cr�er un lien entre le coefficient initilialis� par d�faut � z�ro,
     * le champ disciplinaire et le discipline de sp�cialit�.
     * 
     * @param coefficientDisciplineSpecialitePK
     *            le couple de code champ disciplinaire et discipline de sp�cialit�
     */
    public CoefficientDisciplineSpecialite(CoefficientDisciplineSpecialitePK coefficientDisciplineSpecialitePK) {
        this.valCoef = Integer.valueOf(0);
        this.coefficientDisciplineSpecialitePK = coefficientDisciplineSpecialitePK;
    }

    /**
     * Constructeur permettant de cr�er un lien entre le coefficient initilialis� par d�faut � z�ro,
     * le champ disciplinaire et le discipline de sp�cialit�.
     * 
     * @param coefficientDisciplineSpecialitePK
     *            le couple de code champ disciplinaire et discipline de sp�cialit�
     * @param valCoef
     *            la valeur du coefficient
     */
    public CoefficientDisciplineSpecialite(CoefficientDisciplineSpecialitePK coefficientDisciplineSpecialitePK,
            Integer valCoef) {
        this.coefficientDisciplineSpecialitePK = coefficientDisciplineSpecialitePK;
        this.valCoef = valCoef;
    }

    /**
     * M�thode permettant de retourner la valeur du couple de code discipline de sp�cialit� et champ disciplinaire.
     * 
     * @return l'objet coefficientDisciplineSpecialitePK
     */
    public CoefficientDisciplineSpecialitePK getCoefficientDisciplineSpecialitePK() {
        return coefficientDisciplineSpecialitePK;
    }

    /**
     * M�thode mettant � jour le couple de code discipline de sp�cialit� et champ disciplinaire.
     * 
     * @param coefficientDisciplineSpecialitePK
     *            le couple de code discipline de sp�cialit� et champ disciplinaire
     */
    public void setCoefficientDisciplineSpecialitePK(
            CoefficientDisciplineSpecialitePK coefficientDisciplineSpecialitePK) {
        this.coefficientDisciplineSpecialitePK = coefficientDisciplineSpecialitePK;
    }

    /**
     * M�thode permettant de retourner le domaine de sp�cialit�.
     * 
     * @return l'objet domaine sp�cialit�
     */
    public DomaineSpecialite getDomaineSpecialite() {
        return domaineSpecialite;
    }

    /**
     * M�thode mettant � jour l'objet domaine de sp�cialit�.
     * 
     * @param domaineSpecialite
     *            l'objet domaine de sp�cialit�
     */
    public void setDomaineSpecialite(DomaineSpecialite domaineSpecialite) {
        this.domaineSpecialite = domaineSpecialite;
    }

    /**
     * M�thode permettant de retourner le champ disciplinaire.
     * 
     * @return l'objet champ disciplinaire
     */
    public ChampDisciplinaire getChampDiscipline() {
        return champDiscipline;
    }

    /**
     * M�thode mettant � jour le champ disciplinaire.
     * 
     * @param champDiscipline
     *            l'objet champ disciplinaire
     */
    public void setChampDiscipline(ChampDisciplinaire champDiscipline) {
        this.champDiscipline = champDiscipline;
    }

    /**
     * M�thode permettant de retourner le coefficient.
     * 
     * @return la valeur du coefficient
     */
    public Integer getValCoef() {
        return valCoef;
    }

    /**
     * M�thode mettant � jour le coefficient.
     * 
     * @param valCoef
     *            la valeur du coefficient
     */
    public void setValCoef(Integer valCoef) {
        this.valCoef = valCoef;
    }

    @Override
    public int compareTo(CoefficientDisciplineSpecialite o) {
        int result = this.getChampDiscipline().getValeurOrdre() - o.champDiscipline.getValeurOrdre();
        if (result == 0) {
            result = this.getChampDiscipline().getCode().compareTo(o.getChampDiscipline().getCode());
        }
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CoefficientDisciplineSpecialite)) {
            return false;
        }
        CoefficientDisciplineSpecialite castOther = (CoefficientDisciplineSpecialite) other;
        return new EqualsBuilder()
                .append(getCoefficientDisciplineSpecialitePK(), castOther.getCoefficientDisciplineSpecialitePK())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCoefficientDisciplineSpecialitePK()).toHashCode();
    }
}
