/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;

/**
 * Classe permettant d'acc�der au information des coefficients des �valuations compl�mentaires
 * que l'on retrouve dans la table GN_COEF_EVAL_COMPLEMENTAIRE.
 * Cette table fait le lien entre les �valuations compl�mentaires,
 * leur coefficient et le param�tre par formation accueil
 */
public class CoefficientEvaluationComplementaire
        implements Serializable, Comparable<CoefficientEvaluationComplementaire> {

    /**
     * taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le couple d'identifiant.
     */
    private CoefficientEvaluationComplementairePK coefficientEvaluationComplementairePK;

    /** Evaluation compl�mentaire associ� � un coefficient. */
    private EvaluationComplementaire evalComp;

    /** L'objet param�tre par formation d'accueil. */
    private ParametresFormationAccueil paramefa;

    /** La valeur du coefficient. */
    private Integer valCoef;

    /**
     * Constructeur par d�faut.
     */
    public CoefficientEvaluationComplementaire() {
        this.valCoef = Integer.valueOf(0);
    }

    /**
     * Constructeur permettant de cr�er un lien entre le coefficient,
     * l'�valuation compl�mentaire et le paramefa.
     * 
     * @param id
     *            du couple �valuation compl�mentaire / paramefa
     */
    public CoefficientEvaluationComplementaire(CoefficientEvaluationComplementairePK id) {
        this.coefficientEvaluationComplementairePK = id;
        this.valCoef = Integer.valueOf(0);
    }

    /**
     * Constructeur permettant de cr�er un lien entre le coefficient,
     * l'�valuation compl�mentaire et le paramefa.
     * 
     * @param id
     *            du couple �valuation compl�mentaire / paramefa
     * @param valCoef
     *            valeur du coefficient
     */
    public CoefficientEvaluationComplementaire(CoefficientEvaluationComplementairePK id, Integer valCoef) {
        this.coefficientEvaluationComplementairePK = id;
        this.valCoef = valCoef;
    }

    /**
     * M�thode permettant de r�cup�rer le couple d'identifiant.
     * 
     * @return les identifiants des �valuations compl�mentaires et les coefficients
     */
    public CoefficientEvaluationComplementairePK getCoefficientEvaluationComplementairePK() {
        return coefficientEvaluationComplementairePK;
    }

    /**
     * M�thode mettant � jour le couple d'identifiant.
     * 
     * @param coefficientEvaluationComplementairePK
     *            le couple d'identifiant
     */
    public void setCoefficientEvaluationComplementairePK(
            CoefficientEvaluationComplementairePK coefficientEvaluationComplementairePK) {
        this.coefficientEvaluationComplementairePK = coefficientEvaluationComplementairePK;
    }

    /**
     * M�thode permettant de r�cup�rer la valeur du coefficient.
     * 
     * @return la valeur du coefficient
     */
    public Integer getValCoef() {
        return valCoef;
    }

    /**
     * M�thode mettant � jour la valeur du coefficient.
     * 
     * @param valCoef
     *            la valeur du coefficient
     */
    public void setValCoef(Integer valCoef) {
        this.valCoef = valCoef;
    }

    /**
     * M�thode permettant de r�cup�rer l'�valuation compl�mentaire.
     * 
     * @return l'�valuation compl�mentaire
     */
    public EvaluationComplementaire getEvalComp() {
        return evalComp;
    }

    /**
     * M�thode mettant � jour l'�valuation compl�mentaire.
     * 
     * @param evalCompl
     *            l'�valuation compl�mentaire
     */
    public void setEvalComp(EvaluationComplementaire evalCompl) {
        this.evalComp = evalCompl;
    }

    /**
     * M�thode permettant de r�cup�rer le param�tre par formation d'accueil.
     * 
     * @return une param�tre par formation d'accueil
     */
    public ParametresFormationAccueil getParamefa() {
        return paramefa;
    }

    /**
     * M�thode mettant � jour le param�tre de formation d'accueil.
     * 
     * @param paramefa
     *            le param�tre d'accueil
     */
    public void setParamefa(ParametresFormationAccueil paramefa) {
        this.paramefa = paramefa;
    }

    @Override
    public int compareTo(CoefficientEvaluationComplementaire o) {
        Integer val1 = this.getEvalComp().getOrdreAffichage();
        Integer val2 = o.getEvalComp().getOrdreAffichage();
        return val1.compareTo(val2);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", getCoefficientEvaluationComplementairePK()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CoefficientEvaluationComplementaire)) {
            return false;
        }
        CoefficientEvaluationComplementaire castOther = (CoefficientEvaluationComplementaire) other;
        return new EqualsBuilder().append(getCoefficientEvaluationComplementairePK(),
                castOther.getCoefficientEvaluationComplementairePK()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCoefficientEvaluationComplementairePK()).toHashCode();
    }

}
