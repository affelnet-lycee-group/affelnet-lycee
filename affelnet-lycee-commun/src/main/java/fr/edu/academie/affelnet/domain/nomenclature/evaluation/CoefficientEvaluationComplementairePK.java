/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Classe permettant d'avoir le couple de primary key pour la classe
 * "CoefficientEvaluationComplementaire".
 *
 */
public class CoefficientEvaluationComplementairePK implements Serializable {

    /**
     * Taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant des param�tres par formations d'accueil.
     */
    private Long idParamefa;

    /**
     * Identifiant des �valuations compl�mentaires.
     */
    private Long idEvalComp;

    /**
     * Construteur vide.
     */
    public CoefficientEvaluationComplementairePK() {
        super();
    }

    /**
     * Constructeur permettant d'obtenir le couple d'identifiant
     * param�tre par formations d'accueil et �valuation compl�mentaire.
     * 
     * @param idParamefa
     *            identifiant du param�tre par formation d'accueil
     * @param idEvalComp
     *            identifiant de l'�valuation compl�mentaire
     */
    public CoefficientEvaluationComplementairePK(Long idParamefa, Long idEvalComp) {
        this.idEvalComp = idEvalComp;
        this.idParamefa = idParamefa;
    }

    /**
     * M�thode r�cup�rant l'identifiant des param�tres par formation d'accueil.
     * 
     * @return l'identifiant du param�tre par formation
     */
    public Long getIdParamefa() {
        return idParamefa;
    }

    /**
     * M�thode mettant � jour les identifiants par formation d'accueil.
     * 
     * @param idParamefa
     *            l'identifiant associ�
     */
    public void setIdParamefa(Long idParamefa) {
        this.idParamefa = idParamefa;
    }

    /**
     * M�thode r�cup�rant l'identifiant des �valuations compl�mentaires.
     * 
     * @return l'identifiant par �valuation compl�mentaire
     */
    public Long getIdEvalComp() {
        return idEvalComp;
    }

    /**
     * M�thode mettant � jour l'identifiant d'�valuation compl�mentaire.
     * 
     * @param idEvalComp
     *            l'identifiant
     */
    public void setIdEvalComp(Long idEvalComp) {
        this.idEvalComp = idEvalComp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("IdEvalComp:", getIdEvalComp())
                .append("IdParamefa", getIdParamefa()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof CoefficientEvaluationComplementairePK)) {
            return false;
        }
        CoefficientEvaluationComplementairePK castOther = (CoefficientEvaluationComplementairePK) other;
        return new EqualsBuilder().append(this.getIdEvalComp(), castOther.getIdEvalComp())
                .append(this.getIdParamefa(), castOther.getIdParamefa()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIdEvalComp()).append(getIdParamefa()).toHashCode();
    }
}
