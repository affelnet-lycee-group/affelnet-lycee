/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Comp�tence du socle.
 */
public class CompetenceSocle implements Comparable<CompetenceSocle>, EvaluationNomenclature {
    /** Code de la comp�tence. */
    private String code;

    /** Libell� court. */
    private String libelleCourt;

    /** Libell� long. */
    private String libelleLong;

    /** Ordre de la comp�tence. */
    private int ordre;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the libelleCourt
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param libelleCourt
     *            the libelleCourt to set
     */
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    /**
     * @return the libelleLong
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * @param libelleLong
     *            the libelleLong to set
     */
    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    /**
     * @return the ordre
     */
    public int getOrdre() {
        return ordre;
    }

    /**
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getCode()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CompetenceSocle)) {
            return false;
        }
        CompetenceSocle castOther = (CompetenceSocle) other;
        return new EqualsBuilder().append(this.getCode(), castOther.getCode()).isEquals();
    }

    @Override
    public String toString() {
        return getCode() + " " + getLibelleCourt();
    }

    @Override
    public int compareTo(CompetenceSocle o) {
        int result = this.ordre - o.getOrdre();
        if (result == 0) {
            result = this.code.compareTo(o.getCode());
        }
        return result;
    }

    @Override
    public String getLibelle() {
        return libelleCourt;
    }

    @Override
    public String getTypeName() {
        return "Comp�tences du socle";
    }

}
