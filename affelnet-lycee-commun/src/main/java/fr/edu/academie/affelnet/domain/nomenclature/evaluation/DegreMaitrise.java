/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

/**
 * Degr� de ma�trise d'une �valuation du socle.
 */
public class DegreMaitrise {

    /**
     * Nombre de points attribu�s � d�faut pour un eleve du palier 2nde.
     * On consid�re l'�quivalence d'un degr� de ma�trise satisfaisant.
     */
    public static final int POINTS_DEFAUT_PALIER_2NDE = 40;

    /** Valeur du degr�. */
    private Integer valeur;

    /** Libell� du degr� de ma�trise. */
    private String libelle;

    /** Nombre de point attribu� pour le degr� de ma�trise. */
    private Integer points;

    /**
     * @return the valeur
     */
    public Integer getValeur() {
        return valeur;
    }

    /**
     * @param valeur
     *            the valeur to set
     */
    public void setValeur(Integer valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle
     *            the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the points
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(Integer points) {
        this.points = points;
    }
}
