/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;

/**
 * Classe repr�sentant une discipline.
 */
public class Discipline implements Comparable<Discipline>, EvaluationNomenclature {

    /** Valeur maximum que peuvent prendre les coefficients de discipline ainsi que leur somme. */
    public static final int VALEUR_MAXIMUM = 30;

    /** id de la discipline. */
    private long id;

    /** Champs Disciplinaire associ� � la discipline. */
    private ChampDisciplinaire champDisciplinaire;

    /** MefStat de la discipline. */
    private MefStat mefStat;

    /** Mati�re associ�e � la discipline. */
    private Matiere matiere;

    /** Code de la mati�re. */
    private String codeMatiere;

    /** Le num�ro d'ordre pour l'affichage. */
    private Integer ordre;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the champDisciplinaire
     */
    public ChampDisciplinaire getChampDisciplinaire() {
        return champDisciplinaire;
    }

    /**
     * @param champDisciplinaire
     *            the champDisciplinaire to set
     */
    public void setChampDisciplinaire(ChampDisciplinaire champDisciplinaire) {
        this.champDisciplinaire = champDisciplinaire;
    }

    /**
     * @return the mefStat
     */
    public MefStat getMefStat() {
        return mefStat;
    }

    /**
     * @param mefStat
     *            the mefStat to set
     */
    public void setMefStat(MefStat mefStat) {
        this.mefStat = mefStat;
    }

    /**
     * @return the matiere
     */
    public Matiere getMatiere() {
        return matiere;
    }

    /**
     * @param matiere
     *            the matiere to set
     */
    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    /**
     * @return the codeMatiere
     */
    public String getCodeMatiere() {
        return codeMatiere;
    }

    /**
     * @param codeMatiere
     *            the codeMatiere to set
     */
    public void setCodeMatiere(String codeMatiere) {
        this.codeMatiere = codeMatiere;
    }

    /**
     * @return le num�ro d'ordre pour l'affichage
     */
    public Integer getOrdre() {
        return ordre;
    }

    /**
     * @param ordre
     *            le num�ro d'ordre pour l'affichage
     */
    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Discipline)) {
            return false;
        }
        Discipline castOther = (Discipline) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Discipline id=");
        sb.append(getId());
        sb.append(" (");
        sb.append(mefStat.getLibelleCourt());
        sb.append("-");
        sb.append(matiere.getLibelleCourt());
        sb.append(")");
        return sb.toString();
    }

    @Override
    public int compareTo(Discipline o) {
        Integer result = this.ordre - o.getOrdre();
        if (result == 0) {
            result = (int) (this.id - o.getId());
        }
        return result;
    }

    @Override
    public String getLibelle() {
        return matiere.getLibelleCourt();
    }

    @Override
    public String getTypeName() {
        return "Disciplines";
    }

}
