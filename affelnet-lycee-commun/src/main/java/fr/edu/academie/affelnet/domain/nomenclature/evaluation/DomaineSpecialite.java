/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

/**
 * Classe permettant d'acc�der au donn�e de AN_DOMAINE_SPECIALITE.
 * 
 */

public class DomaineSpecialite implements Serializable {

    /**
     * Tag de s�rialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * code du domaine sp�cialit�.
     */
    private String code;
    /**
     * Libell� associ� au code sp�cialit�.
     */
    private String libelle;

    /**
     * M�thode redonnant le code sur 2 caract�res associ�s au domaine de sp�cialit�.
     * 
     * @return le code du domaine de sp�cialit�
     */
    public String getCode() {
        return code;
    }

    /**
     * M�thode permettant de modifier le code.
     * 
     * @param code
     *            sur 2 caract�res associ�s au domaine de sp�cialit�
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * M�thode redonnant le libell� associ� au domaine de sp�cialit�.
     * 
     * @return le libell� du domaine de sp�cialit�
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * M�thode permettant de modifier le libell�.
     * 
     * @param libelle
     *            le libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

}
