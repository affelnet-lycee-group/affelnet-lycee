/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;

/**
 * Classe permettant de faire le lien avec la table GN_ECHELON_EVAL.
 * La table GN_ECHELON_EVAL d�crit les �chelons pour les �valuations compl�mentaires cr��es si besoin par
 * l'acad�mie.
 * Il s'agit d'une nomenclature interne commune � toutes les acad�mies.
 *
 */
public class EchelonEvaluation implements Serializable {

    /**
     * Taggue de s�rialisation.
     */
    private static final long serialVersionUID = 1L;
    /** Valeur de l'�chelon de 1 � 4. */
    private Integer valeurEchelon;
    /** Libell� de l'�chelon. */
    private String libelle;
    /** Valeur du point pour cet �chelon. */
    private Integer valeurPoint;

    /**
     * M�thode permettant de r�cup�rer la valeur de l'�chelon.
     * 
     * @return la valeur de l'�chelon
     */
    public Integer getValeurEchelon() {
        return valeurEchelon;
    }

    /**
     * M�thode mettant � jour la valeur de l'�chelon.
     * 
     * @param valeurEchelon
     *            la valeur de l'�chelon
     */
    public void setValeurEchelon(Integer valeurEchelon) {
        this.valeurEchelon = valeurEchelon;
    }

    /**
     * M�thode permettant de r�cup�rer le libell�.
     * 
     * @return le libell�
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * M�thode mettant � jour le libell�.
     * 
     * @param libelle
     *            le libell�
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * M�thode permettant de r�cup�rer la valeur du point.
     * 
     * @return la valeur du point
     */
    public Integer getValeurPoint() {
        return valeurPoint;
    }

    /**
     * M�thode mettant � jour la valeur du point.
     * 
     * @param valeurPoint
     *            la valeur du point
     */
    public void setValeurPoint(Integer valeurPoint) {
        this.valeurPoint = valeurPoint;
    }

}
