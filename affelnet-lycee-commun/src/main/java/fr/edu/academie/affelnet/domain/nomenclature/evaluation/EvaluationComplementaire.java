/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.nomenclature.evaluation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;

/**
 * Classe permettant d'acc�der au information des �valuations compl�mentaires
 * que l'on retrouve dans la table GN_EVAL_COMPLEMENTAIRE.
 */
public class EvaluationComplementaire extends Datable
        implements Serializable, Comparable<EvaluationComplementaire>, EvaluationNomenclature {

    /** Nombre maximum d'�valuation compl�mentaire g�r�s dans l'application. */
    public static final int NOMBRE_MAXIMUM = 5;

    /**
     * Tag de s�rialisation par d�faut.
     */
    private static final long serialVersionUID = 1L;

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(EvaluationComplementaire.class);

    /** Le libell� de l'�valuation compl�mentaire. */
    private String libelle;

    /** Le num�ro d'affichage qui est compris entre 1 et 5. */
    private int ordreAffichage;

    /** Identifiant de l'�valuation compl�mentaire (g�n�r� automatiquement). */
    private Long id;

    /**
     * Les coefficients multiplicatifs associ�s � l'�valuation compl�mentaire dans les param�tres par formation
     * d'accueil.
     */
    private Map<ParametresFormationAccueil, CoefficientEvaluationComplementaire> coefficientsEvalCompl = new HashMap<ParametresFormationAccueil, CoefficientEvaluationComplementaire>();

    /**
     * M�thode redonnant le libell� de l'�valuation compl�mentaire.
     * 
     * @return le libell�
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * M�thode qui remplace le libell�.
     * 
     * @param libelle
     *            fourni
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * M�thode redonnant l'identifiant des �valuations compl�mentaires.
     * 
     * @return l'identifiant auto g�n�r� de la base de donn�e
     */
    public Long getId() {
        return id;
    }

    /**
     * M�thode qui remplace l'identifiant de l'�valuation compl�mentaire.
     * 
     * @param identifiant
     *            de l'�valuation compl�mentaire
     */
    public void setId(Long identifiant) {
        this.id = identifiant;
    }

    /**
     * M�thode redonnant l'ordre d'affichage.
     * 
     * @return le num�ro indiquant l'ordre
     */
    public int getOrdreAffichage() {
        return ordreAffichage;
    }

    /**
     * M�thode qui remplace le num�ro d'affichage.
     * 
     * @param ordreAffichage
     *            de l'�valuation compl�mentaire
     */
    public void setOrdreAffichage(int ordreAffichage) {
        this.ordreAffichage = ordreAffichage;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des coefficients.
     * 
     * @return la liste des coefficients des �valuations compl�mentaires
     */
    public Map<ParametresFormationAccueil, CoefficientEvaluationComplementaire> getCoefficientsEvalCompl() {
        return coefficientsEvalCompl;
    }

    /**
     * M�thode mettant � jour les coefficients de l'�valuation compl�mentaire.
     * 
     * @param coefficientsEvalCompl
     *            la liste de coefficients
     */
    public void setCoefficientsEvalCompl(
            Map<ParametresFormationAccueil, CoefficientEvaluationComplementaire> coefficientsEvalCompl) {
        this.coefficientsEvalCompl = coefficientsEvalCompl;
    }

    /**
     * M�thode permettant d'ajouter les coefficients de l'�valuation compl�mentaire.
     * 
     * @param coefficientEvaluationComplementaire
     *            le coefficient
     */
    public void ajouteCoefficient(CoefficientEvaluationComplementaire coefficientEvaluationComplementaire) {
        LOG.debug("Ajout du coefficient");
        if (coefficientEvaluationComplementaire != null) {
            coefficientEvaluationComplementaire.setEvalComp(this);
            ParametresFormationAccueil paramefa = coefficientEvaluationComplementaire.getParamefa();
            if (paramefa == null) {
                Long idparamefa = coefficientEvaluationComplementaire.getCoefficientEvaluationComplementairePK()
                        .getIdParamefa();
                paramefa = new ParametresFormationAccueil();
                paramefa.setId(idparamefa);
            }
            this.coefficientsEvalCompl.put(paramefa, coefficientEvaluationComplementaire);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EvaluationComplementaire)) {
            return false;
        }
        EvaluationComplementaire castOther = (EvaluationComplementaire) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * Setter appel� lors de la mise � jour en base de donn�es de l'objet.
     * Cette m�thode a �t� cr�� pour �viter le probl�me "A collection with cascade="all-delete-orphan" was no
     * longer referenced by the owning entity instance:
     * fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire.coefficients
     * 
     * @param eval
     *            Evaluation � copier
     */
    public void majProperties(EvaluationComplementaire eval) {
        this.libelle = eval.getLibelle();
        this.ordreAffichage = eval.getOrdreAffichage();

        this.setDateMAJ(eval.getDateMAJ());
        this.setDateCreation(eval.getDateCreation());

        Map<ParametresFormationAccueil, CoefficientEvaluationComplementaire> coeff = eval
                .getCoefficientsEvalCompl();
        if (coeff != null && coeff.size() != 0) {
            this.coefficientsEvalCompl = coeff;
        }
    }

    @Override
    public int compareTo(EvaluationComplementaire o) {
        Integer result = this.ordreAffichage - o.getOrdreAffichage();
        if (result == 0) {
            result = (int) (this.id - o.getId());
        }
        return result;
    }

    @Override
    public String getTypeName() {
        return "�valuations compl�mentaires";
    }

}
