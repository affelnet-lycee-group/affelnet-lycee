/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;

/**
 * La candidature d'un �l�ve pour un tour donn�.
 */
public class Candidature extends Datable implements Serializable, Comparable<Candidature> {

    /** L'identifiant de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** L'identifiant. */
    private CandidaturePK id;

    /** Code d�cision finale. */
    private Integer codeDecisionFinale;

    /** Enum�ration de la d�cision finale. */
    @Transient
    private DecisionFinale decisionFinale;

    /** Rang du voeu d'admission (pour les �l�ves affect�s). */
    private Integer rangAdmission;

    /**
     * Flag indiquant un for�age global sur l'�l�ve.
     */
    private String flagEleveForce;

    /** Flag de validit� des avis (du point de vue de l'�tablissement). */
    private String flagValidationAvis;

    /**
     * Indicateur global (calcul�) de validit� de la saisie �l�ve du point de vue de l'�tablissement.
     * Il d�pend des informations �l�ve (ex zone g�o en post-3�me), des voeux (pr�sence des avis obligatoires),
     * des notes (pour les voeux bar�me avec �valuations/notes).
     */
    private String flagSaisieValide;

    /** La table des notes liss�es index�es selon leur rang. */
    private Map<Integer, Double> notesLissees = new TreeMap<>();

    /** Horodatage (dd/MM/yyyy hh:mm:ss) de derni�re mise � jour de l'�l�ves. */
    private Date horodatage;

    /** L'�l�ve concern�. */
    private Eleve eleve;

    /** Liste des voeux �l�ves. */
    private List<VoeuEleve> voeux;

    /** Flag indiquant si le ou les voeux sont conformes ou non avec les d�cisions d'orientation. */
    private String flagConformeDo;

    /**
     * Le constructeur par d�faut de candidature.
     */
    public Candidature() {
        super();
        flagValidationAvis = Flag.NON;
        flagSaisieValide = Flag.NON;
        flagEleveForce = Flag.NON;
        voeux = new ArrayList<>();
        horodatage = new Date();
        flagConformeDo = Flag.OUI;
    }

    /**
     * Le constructeur de candidature en pr�cisant une clef.
     * 
     * @param clef
     *            la clef de la candidature
     */
    public Candidature(CandidaturePK clef) {
        this();
        id = clef;
    }

    /**
     * @return l'identifiant
     */
    public CandidaturePK getId() {
        return id;
    }

    /**
     * @param id
     *            l'identifiant
     */
    public void setId(CandidaturePK id) {
        this.id = id;
    }

    /**
     * @return le code de la d�cision finale d'affectation de l'�l�ve pour le tour
     */
    public Integer getCodeDecisionFinale() {
        return codeDecisionFinale;
    }

    /**
     * @param codeDecisionFinale
     *            le code de la d�cision finale d'affectation de l'�l�ve pour le tour
     */
    public void setCodeDecisionFinale(Integer codeDecisionFinale) {
        this.codeDecisionFinale = codeDecisionFinale;
    }

    /**
     * @return le rang d'admission (pour un �l�ve admis dans le tour)
     */
    public Integer getRangAdmission() {
        return rangAdmission;
    }

    /**
     * @param rangAdmission
     *            le rang d'admission (pour un �l�ve admis dans le tour)
     */
    public void setRangAdmission(Integer rangAdmission) {
        this.rangAdmission = rangAdmission;
    }

    /**
     * @return Flag indiquant un for�age global sur l'�l�ve
     */
    public String getFlagEleveForce() {
        return flagEleveForce;
    }

    /**
     * @param flagEleveForce
     *            Flag indiquant un for�age global sur l'�l�ve
     */
    public void setFlagEleveForce(String flagEleveForce) {
        this.flagEleveForce = flagEleveForce;
    }

    /** @return vrai si la candidature est forc�e globalement, sinon faux. */
    public boolean estForcee() {
        return Flag.OUI.equals(this.flagEleveForce);
    }

    /**
     * @return L'�l�ve concern�
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            L'�l�ve concern�
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the flagValidationAvis
     */
    public String getFlagValidationAvis() {
        return flagValidationAvis;
    }

    /**
     * @param flagValidationAvis
     *            the flagValidationAvis to set
     */
    public void setFlagValidationAvis(String flagValidationAvis) {
        this.flagValidationAvis = flagValidationAvis;
    }

    /**
     * @return the flagSaisieValide
     */
    public String getFlagSaisieValide() {
        return flagSaisieValide;
    }

    /**
     * @param flagSaisieValide
     *            the flagSaisieValide to set
     */
    public void setFlagSaisieValide(String flagSaisieValide) {
        this.flagSaisieValide = flagSaisieValide;
    }

    /**
     * @return the horodatage
     */
    public Date getHorodatage() {
        return horodatage != null ? new Date(horodatage.getTime()) : null;
    }

    /**
     * @param horodatage
     *            the horodatage to set
     */
    public void setHorodatage(Date horodatage) {
        this.horodatage = horodatage != null ? new Date(horodatage.getTime()) : null;
    }

    /**
     * @return the voeux
     */
    public List<VoeuEleve> getVoeux() {
        return voeux;
    }

    /**
     * @param voeux
     *            the voeux to set
     */
    public void setVoeux(List<VoeuEleve> voeux) {
        this.voeux = voeux;
    }

    /**
     * @return the notesLissees
     */
    public Map<Integer, Double> getNotesLissees() {
        return notesLissees;
    }

    /**
     * @return The decision finale from codeDecisionFinale
     */
    public DecisionFinale getDecisionFinale() {
        return DecisionFinale.getPourCode(codeDecisionFinale);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("codeDecisionFinale", getCodeDecisionFinale()).append("rangAdmission", getRangAdmission())
                .append("flagEleveForce", getFlagEleveForce()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof Candidature)) {
            return false;
        }
        Candidature castOther = (Candidature) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public int compareTo(Candidature candidature) {
        return this.getId().getNumeroTour() - candidature.getId().getNumeroTour();
    }

    /**
     * Teste si la candidature comporte un voeu sur une offre de type bar�me avec �valuations/notes.
     * 
     * @return vrai si la candidature comporte un voeu bar�me avec �valuations/notes, sinon faux
     */
    public boolean getPossedeVoeuBaremeAvecNoteEvaluation() {
        boolean possedeVoeuBaremeAvecNotesEvaluations = false;

        Iterator<VoeuEleve> voeuxIt = voeux.iterator();

        while (!possedeVoeuBaremeAvecNotesEvaluations && voeuxIt.hasNext()) {
            VoeuEleve voeu = voeuxIt.next();
            possedeVoeuBaremeAvecNotesEvaluations = voeu.getVoeu().utiliseBaremeNotesEvaluations();
        }

        return possedeVoeuBaremeAvecNotesEvaluations;
    }

    /**
     * Fournis le voeu du rang en entr�e.
     * 
     * @param rang
     *            rang du voeu
     * @return le voeu associ�
     */
    public VoeuEleve voeuDeRang(Integer rang) {
        if (voeux == null || rang > voeux.size()) {
            return null;
        }
        return voeux.get(rang - 1);
    }

    /**
     * @return the flagConformeDo
     */
    public String getFlagConformeDo() {
        return flagConformeDo;
    }

    /**
     * @param flagConformeDo
     *            the flagConformeDo to set
     */
    public void setFlagConformeDo(String flagConformeDo) {
        this.flagConformeDo = flagConformeDo;
    }

}
