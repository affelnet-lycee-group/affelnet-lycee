/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** La clef pour la candidature d'un �l�ve pour un tour. */
public class CandidaturePK implements Serializable {

    /** L'identifiant de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Le num�ro du tour. */
    private Short numeroTour;

    /** l'identifiant national �l�ve. */
    private String ine;

    /** Cr�e une cl� identifiante pour une candidature d'�l�ve pour un tour donn�. */
    public CandidaturePK() {
    }

    /**
     * Cr�e une cl� identifiante pour une candidature d'�l�ve pour un tour donn�.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param ine
     *            identifiant de l'�l�ve
     */
    public CandidaturePK(short numeroTour, String ine) {
        this.numeroTour = numeroTour;
        this.ine = ine;
    }

    /**
     * @return the numeroTour
     */
    public Short getNumeroTour() {
        return numeroTour;
    }

    /**
     * @param numeroTour
     *            the numeroTour to set
     */
    public void setNumeroTour(Short numeroTour) {
        this.numeroTour = numeroTour;
    }

    /**
     * @return the ine
     */
    public String getIne() {
        return ine;
    }

    /**
     * @param ine
     *            the ine to set
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("ine", getIne()).append("numeroTour", getNumeroTour()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof CandidaturePK)) {
            return false;
        }
        CandidaturePK castOther = (CandidaturePK) other;
        return new EqualsBuilder().append(this.getIne(), castOther.getIne())
                .append(this.getNumeroTour(), castOther.getNumeroTour()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIne()).append(getNumeroTour()).toHashCode();
    }
}
