/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;

/** Demande de d�rogation pour un voeu d'un �l�ve. */
public class DerogationVoeuEleve extends Datable implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** L'id. */
    private DerogationVoeuElevePK id;

    /**
     * @return the id
     */
    public DerogationVoeuElevePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(DerogationVoeuElevePK id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
}
