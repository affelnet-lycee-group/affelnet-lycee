/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.nomenclature.Derogation;

/**
 * Cl� primaire pour une demande de d�rogation pour un voeu d'un �l�ve.
 * (post-3�me uniquement)
 */
public class DerogationVoeuElevePK implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = -1469565133425231025L;

    /** Le voeu de l'�l�ve. */
    private VoeuEleve voeuEleve;

    /** La d�rogation. */
    private Derogation derogation;

    /** default constructor. */
    public DerogationVoeuElevePK() {
    }

    /**
     * full constructor.
     * 
     * @param voeuEleve
     *            voeu de l'�l�ve
     * @param derogation
     *            d�rogation
     */
    public DerogationVoeuElevePK(VoeuEleve voeuEleve, Derogation derogation) {
        this.voeuEleve = voeuEleve;
        this.derogation = derogation;
    }

    /**
     * @return the voeuEleve
     */
    public VoeuEleve getVoeuEleve() {
        return voeuEleve;
    }

    /**
     * @param voeuEleve
     *            the voeuEleve to set
     */
    public void setVoeuEleve(VoeuEleve voeuEleve) {
        this.voeuEleve = voeuEleve;
    }

    /**
     * @return the derogation
     */
    public Derogation getDerogation() {
        return derogation;
    }

    /**
     * @param derogation
     *            the derogation to set
     */
    public void setDerogation(Derogation derogation) {
        this.derogation = derogation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("voeuEleve", getVoeuEleve()).append("derogation", getDerogation())
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        } else if (!(other instanceof DerogationVoeuElevePK)) {
            return false;
        }
        DerogationVoeuElevePK castOther = (DerogationVoeuElevePK) other;
        return new EqualsBuilder().append(this.getVoeuEleve(), castOther.getVoeuEleve())
                .append(this.getDerogation(), castOther.getDerogation()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getVoeuEleve()).append(getDerogation()).toHashCode();
    }
}
