/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationDiscipline;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.Departement;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.EtablissementNational;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.enumeration.StatutZoneGeoEnum;
import fr.edu.academie.affelnet.helper.EleveHelper;
import fr.edu.academie.affelnet.helper.MatiereHelper;
import fr.edu.academie.affelnet.service.nomenclature.OuvrableManager;
import fr.edu.academie.affelnet.web.utils.SpringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/** Mod�lise les donn�es d'un �l�ve prenant part � l'affectation. */
public class Eleve extends Datable implements Serializable, Personne {

    /** Code pour le sexe masculin. */
    public static final String CODE_SEXE_MASCULIN = "1";

    /** Code pour le sexe feminin. */
    public static final String CODE_SEXE_FEMININ = "2";

    /** Age minimum. */
    public static final int AGE_MINI = 10;

    /** Age maximum. */
    public static final int AGE_MAXI = 30;

    /** Valeur maximum de valeur de la note g�r�s dans l'application. */
    public static final int VALEUR_MAXIMUM_NOTE = 20;

    /** Valeur minimum de valeur de la note g�r�s dans l'application. */
    public static final int VALEUR_MINIMUM_NOTE = 0;

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Loggeur de la classe. */
    private static final Log LOG = LogFactory.getLog(Eleve.class);

    /** Identifiant National �l�ve ou INE provisoire. */
    private String ine;

    /** Nom de famille. */
    private String nom;

    /** Nom d'usage. */
    private String nomUsage;

    /** Premier pr�nom. */
    private String prenom;

    /** Second pr�nom de l'�l�ve. */
    private String prenom2;

    /** Troisi�me pr�nom de l'�l�ve. */
    private String prenom3;

    /** Date de naissance de l'�l�ve. */
    private Date dateNaissance;

    /** Age de l'�l�ve (en ann�es). */
    private Integer age;

    /** Code du sexe de l'�l�ve. */
    private String codeSexe;

    /** Responsables de l'�l�ve. */
    private Set<Responsable> responsables;

    /** Adresse de r�sidence. */
    private Adresse adresseResidence;

    /** Le num�ro de t�l�phone personnel. */
    private String numeroTelephonePersonnel;

    /** Le num�ro de t�l�phone professionnel. */
    private String numeroTelephoneProfessionnel;

    /** Le num�ro de t�l�phone portable. */
    private String numeroTelephonePortable;

    /** L'adresse m�l. */
    private String mel;

    /** Classe dans la scolarit� d'origine. */
    private String classe;

    /** Flag indiquant un retard scolaire. */
    private String flagRetardScolaire;

    /** Flag indiquant un doublement. */
    private String flagDoublement;

    /** Flag indiquant que l'�l�ve n'est pas de l'acad�mie ma�tre. */
    private String flagHorsAcademie;

    /** Flag indiquant si l'�l�ve est boursier. */
    private String flagBoursier;

    /** Valeur de la note de rang 1. */
    private Double note1;
    /** Valeur de la note de rang 2. */
    private Double note2;
    /** Valeur de la note de rang 3. */
    private Double note3;
    /** Valeur de la note de rang 4. */
    private Double note4;
    /** Valeur de la note de rang 5. */
    private Double note5;
    /** Valeur de la note de rang 6. */
    private Double note6;
    /** Valeur de la note de rang 7. */
    private Double note7;
    /** Valeur de la note de rang 8. */
    private Double note8;
    /** Valeur de la note de rang 9. */
    private Double note9;
    /** Valeur de la note de rang 10. */
    private Double note10;
    /** Valeur de la note de rang 11. */
    private Double note11;
    /** Valeur de la note de rang 12. */
    private Double note12;
    /** Valeur de la note de rang 13. */
    private Double note13;
    /** Valeur de la note de rang 14. */
    private Double note14;
    /** Valeur de la note de rang 15. */
    private Double note15;
    /** Valeur de la note de rang 16. */
    private Double note16;
    /** Valeur de la note de rang 17. */
    private Double note17;
    /** Valeur de la note de rang 18. */
    private Double note18;
    /** Valeur de la note de rang 19. */
    private Double note19;
    /** Valeur de la note de rang 20. */
    private Double note20;

    /** Valeur de la moyenne des notes de l'�l�ve. */
    private Double noteMoyenne;

    /** La table des notes index�es selon leur rang. */
    private Map<Integer, Double> notes = new TreeMap<>();

    /** Vaut 'N' si la saisie de l'�l�ve a �t� r�alis�e en �tablissement. Sinon vaut 'O'. */
    private String flagSaisieIndividuelle;

    /** Flag commission (vrai si un �l�ve a �t� cr�� pendant une commission). */
    private String flagCommission;

    /** Flag �l�ve en probabilit� de doublons. */
    private String flagDoublon;

    /** Flag �l�ve en probabilit� de doublons trait�. */
    private String flagDoublonTraite;

    /** Formation d'origine. */
    private Formation formation;

    /** Etablissement national d'origine. */
    private EtablissementNational etablissementNational;

    /** Etablissement acad�mique d'origine. */
    private Etablissement etablissement;

    /** D�partement d'origine. */
    private Departement departement;

    /** Groupe origine pour l'�l�ve. */
    private GroupeOrigine groupeOrigine;

    /** Zone g�ographique. */
    private ZoneGeo zoneGeo;

    /** Langue Vivante 1. */
    private Matiere matiereLv1;

    /** Langue Vivante 2. */
    private Matiere matiereLv2;

    /** Option origine 1. */
    private Matiere optionOrigine2;

    /** Option origine 2. */
    private Matiere optionOrigine3;

    /** Flag pour le bonus acad�mique. */
    private String flagBonusAcademique;

    /** Code MEFSTAT4 d'origine. */
    private String mefstat4Origine;

    /** Le palier d'origine. **/
    private Palier palierOrigine;

    /** Les d�cisions d'orientation. */
    private Set<DecisionOrientationEleve> decisionsOrientationEleve;

    /** Les �valuations des disciplines de l'�l�ve. */
    private Map<Discipline, EvaluationDiscipline> evaluationsDiscipline;

    /** Les �valuations du socle de l'�l�ve. */
    private Map<CompetenceSocle, EvaluationSocle> evaluationsSocle;

    /** Les �valuations compl�mentaires acad�miques de l'�l�ve. */
    private Map<EvaluationComplementaire, EvaluationComplementaireEleve> evaluationsComplementaires;

    /** Les candidatures de l'�l�ve par tour. */
    private Map<Short, Candidature> candidatures;

    /** Identifiant SIECLE de l'�l�ve, le cas �chant. */
    private Integer eleveIdSiecle;

    /** Flag indiquant une decision d'orientation saisie en manuel ou re�ue de SIECLE. */
    private String flagDecOriManuelle;

    /** Ensemble des voeux de l'�l�ve saisis depuis le t�l�service affectation. */
    private Set<VoeuTeleservice> voeuxTeleservice;
    
    /** Statut de la d�termination de la zone g�ographique. */
    private StatutZoneGeoEnum statutZoneGeo;

    /** default constructor. */
    public Eleve() {
        responsables = new TreeSet<>(new ResponsableComparator());
        flagDoublement = Flag.NON;
        flagBoursier = Flag.NON;
        flagRetardScolaire = Flag.NON;
        flagCommission = Flag.NON;
        flagDoublon = Flag.NON;
        flagDoublonTraite = Flag.NON;
        flagBonusAcademique = Flag.NON;
        flagCommission = Flag.NON;
        flagSaisieIndividuelle = Flag.NON;
        decisionsOrientationEleve = new TreeSet<>();
        evaluationsDiscipline = new HashMap<>();
        evaluationsSocle = new HashMap<>();
        evaluationsComplementaires = new HashMap<>();
        candidatures = new TreeMap<>();
        flagDecOriManuelle = Flag.NON;
        voeuxTeleservice = new TreeSet<>();
        statutZoneGeo = StatutZoneGeoEnum.NON_TRAITE;
    }

    /**
     * @return the ine
     */
    public String getIne() {
        return ine;
    }

    /**
     * @param ine
     *            the ine to set
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    /**
     * @return Nom de famille
     */
    @Override
    public String getNom() {
        return nom;
    }

    /**
     * @param nom
     *            Nom de famille
     */
    @Override
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return Nom d'usage
     */
    @Override
    public String getNomUsage() {
        return nomUsage;
    }

    /**
     * @param nomUsage
     *            Nom d'usage
     */
    @Override
    public void setNomUsage(String nomUsage) {
        this.nomUsage = nomUsage;
    }

    /**
     * @return Premier pr�nom
     */
    @Override
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom
     *            Premier pr�nom
     */
    @Override
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the prenom2
     */
    public String getPrenom2() {
        return prenom2;
    }

    /**
     * @param prenom2
     *            the prenom2 to set
     */
    public void setPrenom2(String prenom2) {
        this.prenom2 = prenom2;
    }

    /**
     * @return the prenom3
     */
    public String getPrenom3() {
        return prenom3;
    }

    /**
     * @param prenom3
     *            the prenom3 to set
     */
    public void setPrenom3(String prenom3) {
        this.prenom3 = prenom3;
    }

    /**
     * Liste les pr�noms de l'�l�ve dans l'ordre sous forme tass�e (en omettant les vides, nulls ...).
     * 
     * @return Liste tass�e des pr�noms.
     */
    public List<String> listePrenoms() {

        List<String> prenoms = new ArrayList<>(3);

        if (!StringUtils.isEmpty(prenom)) {
            prenoms.add(prenom);
        }

        if (!StringUtils.isEmpty(prenom2)) {
            prenoms.add(prenom2);
        }

        if (!StringUtils.isEmpty(prenom3)) {
            prenoms.add(prenom3);
        }

        return prenoms;
    }

    /**
     * @return the dateNaissance
     */
    public Date getDateNaissance() {
        return dateNaissance != null ? new Date(dateNaissance.getTime()) : null;
    }

    /**
     * @param dateNaissance
     *            the dateNaissance to set
     */
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance != null ? new Date(dateNaissance.getTime()) : null;
    }

    /**
     * @return the age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age
     *            the age to set
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return the codeSexe
     */
    public String getCodeSexe() {
        return codeSexe;
    }

    /**
     * @param codeSexe
     *            the codeSexe to set
     */
    public void setCodeSexe(String codeSexe) {
        this.codeSexe = codeSexe;
    }

    /**
     * D�termine les responsables notifiables.
     * 
     * On notifie tous les repr�sentant l�gaux ayant une adresse valide.
     * 
     * @return la liste des responsables notifiables (vide s'il n'y en a pas)
     */
    public List<Responsable> getResponsablesNotifiables() {
        List<Responsable> result = new ArrayList<>();

        for (Responsable responsable : getResponsables()) {
            if (responsable.estNotifiable()) {
                result.add(responsable);
            }
        }

        return result;
    }

    /**
     * @return responsables de l'�l�ve
     */
    public Set<Responsable> getResponsables() {
        return responsables;
    }

    /**
     * Attention, l'ensemble fourni doit �tre ordonn� !
     * 
     * @param responsables
     *            responsables de l'�l�ve
     * @see ResponsableComparator
     */
    public void setResponsables(Set<Responsable> responsables) {
        this.responsables = responsables;
    }

    /**
     * @return adresse de r�sidence
     */
    @Override
    public Adresse getAdresseResidence() {
        return adresseResidence;
    }

    /**
     * @param adresseResidence
     *            adresse de r�sidence
     */
    @Override
    public void setAdresseResidence(Adresse adresseResidence) {
        this.adresseResidence = adresseResidence;
    }

    /**
     * @return Le num�ro de t�l�phone personnel
     */
    @Override
    public String getNumeroTelephonePersonnel() {
        return numeroTelephonePersonnel;
    }

    /**
     * @param numeroTelephonePersonnel
     *            Le num�ro de t�l�phone personnel
     */
    @Override
    public void setNumeroTelephonePersonnel(String numeroTelephonePersonnel) {
        this.numeroTelephonePersonnel = numeroTelephonePersonnel;
    }

    /**
     * @return Le num�ro de t�l�phone professionnel
     */
    @Override
    public String getNumeroTelephoneProfessionnel() {
        return numeroTelephoneProfessionnel;
    }

    /**
     * @param numeroTelephoneProfessionnel
     *            Le num�ro de t�l�phone professionnel
     */
    @Override
    public void setNumeroTelephoneProfessionnel(String numeroTelephoneProfessionnel) {
        this.numeroTelephoneProfessionnel = numeroTelephoneProfessionnel;
    }

    /**
     * @return Le num�ro de t�l�phone portable
     */
    @Override
    public String getNumeroTelephonePortable() {
        return numeroTelephonePortable;
    }

    /**
     * @param numeroTelephonePortable
     *            Le num�ro de t�l�phone portable
     */
    @Override
    public void setNumeroTelephonePortable(String numeroTelephonePortable) {
        this.numeroTelephonePortable = numeroTelephonePortable;
    }

    /**
     * Constitue la liste des num�ros de t�l�phones de l'�l�ve
     * et de ses responsables, dans l'ordre portable, perso, pro.
     * 
     * @return La liste des num�ros
     */
    public List<String> getNumerosTelephone() {
        List<String> result = new ArrayList<>();

        if (this.getNumeroTelephonePortable() != null) {
            result.add(this.getNumeroTelephonePortable());
        }
        if (this.getNumeroTelephonePersonnel() != null) {
            result.add(this.getNumeroTelephonePersonnel());
        }
        if (this.getNumeroTelephoneProfessionnel() != null) {
            result.add(this.getNumeroTelephoneProfessionnel());
        }

        for (Responsable responsable : this.getResponsables()) {
            if (responsable.getNumeroTelephonePortable() != null) {
                result.add(responsable.getNumeroTelephonePortable());
            }
            if (responsable.getNumeroTelephonePersonnel() != null) {
                result.add(responsable.getNumeroTelephonePersonnel());
            }
            if (responsable.getNumeroTelephoneProfessionnel() != null) {
                result.add(responsable.getNumeroTelephoneProfessionnel());
            }
        }

        return result;
    }

    /**
     * Constitue la liste des num�ros de t�l�phones des responsables de l'�l�ve
     * , dans l'ordre portable, perso, pro.
     *
     * @return La liste des num�ros
     */
    public List<String> getNumerosTelephoneResponsables() {
        List<String> result = new ArrayList<>();

        for (Responsable responsable : this.getResponsables()) {
            if (responsable.getNumeroTelephonePortable() != null) {
                result.add(responsable.getNumeroTelephonePortable());
            }
            if (responsable.getNumeroTelephonePersonnel() != null) {
                result.add(responsable.getNumeroTelephonePersonnel());
            }
            if (responsable.getNumeroTelephoneProfessionnel() != null) {
                result.add(responsable.getNumeroTelephoneProfessionnel());
            }
        }

        return result;
    }

    /**
     * @return L'adresse m�l
     */
    @Override
    public String getMel() {
        return mel;
    }

    /**
     * @param mel
     *            L'adresse m�l
     */
    @Override
    public void setMel(String mel) {
        this.mel = mel;
    }

    /**
     * @return the classe
     */
    public String getClasse() {
        return classe;
    }

    /**
     * @param classe
     *            the classe to set
     */
    public void setClasse(String classe) {
        this.classe = classe;
    }

    /**
     * @return the flagRetardScolaire
     */
    public String getFlagRetardScolaire() {
        return flagRetardScolaire;
    }

    /**
     * @param flagRetardScolaire
     *            the flagRetardScolaire to set
     */
    public void setFlagRetardScolaire(String flagRetardScolaire) {
        this.flagRetardScolaire = flagRetardScolaire;
    }

    /**
     * @return the flagDoublement
     */
    public String getFlagDoublement() {
        return flagDoublement;
    }

    /**
     * @param flagDoublement
     *            the flagDoublement to set
     */
    public void setFlagDoublement(String flagDoublement) {
        this.flagDoublement = flagDoublement;
    }

    /**
     * @return the flagHorsAcademie
     */
    public String getFlagHorsAcademie() {
        return flagHorsAcademie;
    }

    /**
     * @param flagHorsAcademie
     *            the flagHorsAcademie to set
     */
    public void setFlagHorsAcademie(String flagHorsAcademie) {
        this.flagHorsAcademie = flagHorsAcademie;
    }

    /**
     * @return flag indiquant si l'�l�ve est boursier
     */
    public String getFlagBoursier() {
        return flagBoursier;
    }

    /**
     * @param flagBoursier
     *            Flag indiquant si l'�l�ve est boursier
     */
    public void setFlagBoursier(String flagBoursier) {
        this.flagBoursier = flagBoursier;
    }

    /**
     * @return the note1
     */
    public Double getNote1() {
        return note1;
    }

    /**
     * @param note1
     *            the note1 to set
     */
    public void setNote1(Double note1) {
        this.note1 = note1;
        notes.put(1, note1);
    }

    /**
     * @return the note2
     */
    public Double getNote2() {
        return note2;
    }

    /**
     * @param note2
     *            the note2 to set
     */
    public void setNote2(Double note2) {
        this.note2 = note2;
        notes.put(2, note2);
    }

    /**
     * @return the note3
     */
    public Double getNote3() {
        return note3;
    }

    /**
     * @param note3
     *            the note3 to set
     */
    public void setNote3(Double note3) {
        this.note3 = note3;
        notes.put(3, note3);
    }

    /**
     * @return the note4
     */
    public Double getNote4() {
        return note4;
    }

    /**
     * @param note4
     *            the note4 to set
     */
    public void setNote4(Double note4) {
        this.note4 = note4;
        notes.put(4, note4);
    }

    /**
     * @return the note5
     */
    public Double getNote5() {
        return note5;
    }

    /**
     * @param note5
     *            the note5 to set
     */
    public void setNote5(Double note5) {
        this.note5 = note5;
        notes.put(5, note5);
    }

    /**
     * @return the note6
     */
    public Double getNote6() {
        return note6;
    }

    /**
     * @param note6
     *            the note6 to set
     */
    public void setNote6(Double note6) {
        this.note6 = note6;
        notes.put(6, note6);
    }

    /**
     * @return the note7
     */
    public Double getNote7() {
        return note7;
    }

    /**
     * @param note7
     *            the note7 to set
     */
    public void setNote7(Double note7) {
        this.note7 = note7;
        notes.put(7, note7);
    }

    /**
     * @return the note8
     */
    public Double getNote8() {
        return note8;
    }

    /**
     * @param note8
     *            the note8 to set
     */
    public void setNote8(Double note8) {
        this.note8 = note8;
        notes.put(8, note8);
    }

    /**
     * @return the note9
     */
    public Double getNote9() {
        return note9;
    }

    /**
     * @param note9
     *            the note9 to set
     */
    public void setNote9(Double note9) {
        this.note9 = note9;
        notes.put(9, note9);
    }

    /**
     * @return the note10
     */
    public Double getNote10() {
        return note10;
    }

    /**
     * @param note10
     *            the note10 to set
     */
    public void setNote10(Double note10) {
        this.note10 = note10;
        notes.put(10, note10);
    }

    /**
     * @return the note11
     */
    public Double getNote11() {
        return note11;
    }

    /**
     * @param note11
     *            the note11 to set
     */
    public void setNote11(Double note11) {
        this.note11 = note11;
        notes.put(11, note11);
    }

    /**
     * @return the note12
     */
    public Double getNote12() {
        return note12;
    }

    /**
     * @param note12
     *            the note12 to set
     */
    public void setNote12(Double note12) {
        this.note12 = note12;
        notes.put(12, note12);
    }

    /**
     * @return the note13
     */
    public Double getNote13() {
        return note13;
    }

    /**
     * @param note13
     *            the note13 to set
     */
    public void setNote13(Double note13) {
        this.note13 = note13;
        notes.put(13, note13);
    }

    /**
     * @return the note14
     */
    public Double getNote14() {
        return note14;
    }

    /**
     * @param note14
     *            the note14 to set
     */
    public void setNote14(Double note14) {
        this.note14 = note14;
        notes.put(14, note14);
    }

    /**
     * @return the note15
     */
    public Double getNote15() {
        return note15;
    }

    /**
     * @param note15
     *            the note15 to set
     */
    public void setNote15(Double note15) {
        this.note15 = note15;
        notes.put(15, note15);
    }

    /**
     * @return the note16
     */
    public Double getNote16() {
        return note16;
    }

    /**
     * @param note16
     *            the note16 to set
     */
    public void setNote16(Double note16) {
        this.note16 = note16;
        notes.put(16, note16);
    }

    /**
     * @return the note17
     */
    public Double getNote17() {
        return note17;
    }

    /**
     * @param note17
     *            the note17 to set
     */
    public void setNote17(Double note17) {
        this.note17 = note17;
        notes.put(17, note17);
    }

    /**
     * @return the note18
     */
    public Double getNote18() {
        return note18;
    }

    /**
     * @param note18
     *            the note18 to set
     */
    public void setNote18(Double note18) {
        this.note18 = note18;
        notes.put(18, note18);
    }

    /**
     * @return the note19
     */
    public Double getNote19() {
        return note19;
    }

    /**
     * @param note19
     *            the note19 to set
     */
    public void setNote19(Double note19) {
        this.note19 = note19;
        notes.put(19, note19);
    }

    /**
     * @return the note20
     */
    public Double getNote20() {
        return note20;
    }

    /**
     * @param note20
     *            the note20 to set
     */
    public void setNote20(Double note20) {
        this.note20 = note20;
        notes.put(20, note20);
    }

    /**
     * @return the noteMoyenne
     */
    public Double getNoteMoyenne() {
        return noteMoyenne;
    }

    /**
     * @param noteMoyenne
     *            the noteMoyenne to set
     */
    public void setNoteMoyenne(Double noteMoyenne) {
        this.noteMoyenne = noteMoyenne;
    }

    /**
     * @return the flagSaisieIndividuelle
     */
    public String getFlagSaisieIndividuelle() {
        return flagSaisieIndividuelle;
    }

    /**
     * @param flagSaisieIndividuelle
     *            the flagSaisieIndividuelle to set
     */
    public void setFlagSaisieIndividuelle(String flagSaisieIndividuelle) {
        this.flagSaisieIndividuelle = flagSaisieIndividuelle;
    }

    /**
     * @return the flagCommission
     */
    public String getFlagCommission() {
        return flagCommission;
    }

    /**
     * @param flagCommission
     *            the flagCommission to set
     */
    public void setFlagCommission(String flagCommission) {
        this.flagCommission = flagCommission;
    }

    /**
     * @return Identifiant SIECLE de l'�l�ve, le cas �chant.
     */
    public Integer getEleveIdSiecle() {
        return eleveIdSiecle;
    }

    /**
     * @param eleveIdSiecle
     *            Identifiant SIECLE de l'�l�ve, le cas �chant.
     */
    public void setEleveIdSiecle(Integer eleveIdSiecle) {
        this.eleveIdSiecle = eleveIdSiecle;
    }

    /**
     * @return the formation
     */
    public Formation getFormation() {
        return formation;
    }

    /**
     * @param formation
     *            the formation to set
     */
    public void setFormation(Formation formation) {
        this.formation = formation;
    }

    /**
     * @return the etablissementNational
     */
    public EtablissementNational getEtablissementNational() {
        return etablissementNational;
    }

    /**
     * @param etablissementNational
     *            the etablissementNational to set
     */
    public void setEtablissementNational(EtablissementNational etablissementNational) {
        this.etablissementNational = etablissementNational;
    }

    /**
     * @return the etablissement
     */
    public Etablissement getEtablissement() {
        return etablissement;
    }

    /**
     * @param etablissement
     *            the etablissement to set
     */
    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    /**
     * @return the departement
     */
    public Departement getDepartement() {
        return departement;
    }

    /**
     * @param departement
     *            the departement to set
     */
    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    /**
     * @return the groupeOrigine
     */
    public GroupeOrigine getGroupeOrigine() {
        return groupeOrigine;
    }

    /**
     * @param groupeOrigine
     *            the groupeOrigine to set
     */
    public void setGroupeOrigine(GroupeOrigine groupeOrigine) {
        this.groupeOrigine = groupeOrigine;
    }

    /**
     * @return the zoneGeo
     */
    public ZoneGeo getZoneGeo() {
        return zoneGeo;
    }

    /**
     * @param zoneGeo
     *            the zoneGeo to set
     */
    public void setZoneGeo(ZoneGeo zoneGeo) {
        this.zoneGeo = zoneGeo;
    }

    /**
     * @return the mefstat4Origine
     */
    public String getMefstat4Origine() {
        return mefstat4Origine;
    }

    /**
     * @param mefstat4Origine
     *            the mefstat4Origine to set
     */
    public void setMefstat4Origine(String mefstat4Origine) {
        this.mefstat4Origine = mefstat4Origine;
    }

    /**
     * @return the matiereLv1
     */
    public Matiere getMatiereLv1() {
        return matiereLv1;
    }

    /**
     * @param matiereLv1
     *            the matiereLv1 to set
     */
    public void setMatiereLv1(Matiere matiereLv1) {
        this.matiereLv1 = matiereLv1;
    }

    /**
     * @return the optionOrigine2
     */
    public Matiere getOptionOrigine2() {
        return optionOrigine2;
    }

    /**
     * @param optionOrigine2
     *            the optionOrigine2 to set
     */
    public void setOptionOrigine2(Matiere optionOrigine2) {
        this.optionOrigine2 = optionOrigine2;
    }

    /**
     * @return the optionOrigine3
     */
    public Matiere getOptionOrigine3() {
        return optionOrigine3;
    }

    /**
     * @param optionOrigine3
     *            the optionOrigine3 to set
     */
    public void setOptionOrigine3(Matiere optionOrigine3) {
        this.optionOrigine3 = optionOrigine3;
    }

    /**
     * @return Returns the flagDoublon.
     */
    public String getFlagDoublon() {
        return flagDoublon;
    }

    /**
     * @param flagDoublon
     *            The flagDoublon to set.
     */
    public void setFlagDoublon(String flagDoublon) {
        this.flagDoublon = flagDoublon;
    }

    /**
     * @return Returns the flagDoublonTraite.
     */
    public String getFlagDoublonTraite() {
        return flagDoublonTraite;
    }

    /**
     * @param flagDoublonTraite
     *            The flagDoublonTraite to set.
     */
    public void setFlagDoublonTraite(String flagDoublonTraite) {
        this.flagDoublonTraite = flagDoublonTraite;
    }

    /**
     * @return the flagBonusAcademique
     */
    public String getFlagBonusAcademique() {
        return flagBonusAcademique;
    }

    /**
     * @param flagBonusAcademique
     *            the flagBonusAcademique to set
     */
    public void setFlagBonusAcademique(String flagBonusAcademique) {
        this.flagBonusAcademique = flagBonusAcademique;
    }

    /**
     * @return the matiereLv2
     */
    public Matiere getMatiereLv2() {
        return matiereLv2;
    }

    /**
     * @param matiereLv2
     *            the matiereLv2 to set
     */
    public void setMatiereLv2(Matiere matiereLv2) {
        this.matiereLv2 = matiereLv2;
    }

    /**
     * @return the notes
     */
    public Map<Integer, Double> getNotes() {
        return notes;
    }

    /**
     * @return the evaluationsDiscipline
     */
    public Map<Discipline, EvaluationDiscipline> getEvaluationsDiscipline() {
        return evaluationsDiscipline;
    }

    /**
     * @param evaluationsDiscipline
     *            the evaluationsDiscipline to set
     */
    public void setEvaluationsDiscipline(Map<Discipline, EvaluationDiscipline> evaluationsDiscipline) {
        this.evaluationsDiscipline = evaluationsDiscipline;
    }

    /**
     * @return the evaluationsSocle
     */
    public Map<CompetenceSocle, EvaluationSocle> getEvaluationsSocle() {
        return evaluationsSocle;
    }

    /**
     * @param evaluationsSocle
     *            the evaluationsSocle to set
     */
    public void setEvaluationsSocle(Map<CompetenceSocle, EvaluationSocle> evaluationsSocle) {
        this.evaluationsSocle = evaluationsSocle;
    }

    /**
     * @return les evaluations complementaires acad�miques de l'�l�ve
     */
    public Map<EvaluationComplementaire, EvaluationComplementaireEleve> getEvaluationsComplementaires() {
        return evaluationsComplementaires;
    }

    /**
     * @param evaluationsComplementaires
     *            les evaluations complementaires acad�miques de l'�l�ve
     */
    public void setEvaluationsComplementaires(
            Map<EvaluationComplementaire, EvaluationComplementaireEleve> evaluationsComplementaires) {
        this.evaluationsComplementaires = evaluationsComplementaires;
    }

    /**
     * Donne la valeur la note dont l'indice est fourni.
     * 
     * @param idxNote
     *            indice de la note
     * @return valeur de la note
     */
    public Double getNote(int idxNote) {
        try {
            Field fld = getClass().getDeclaredField("note" + idxNote);
            return (Double) fld.get(this);
        } catch (Exception e) {
            LOG.error("Echec lors de l'acc�s en GET au champ note" + idxNote + ".", e);
            return null;
        }
    }

    /**
     * Change la valeur la note dont l'indice est fourni.
     * 
     * @param idxNote
     *            indice de la note
     * @param valeur
     *            valeur de la note
     */
    public void setNote(int idxNote, Double valeur) {
        try {
            Field fld = getClass().getDeclaredField("note" + idxNote);
            fld.set(this, valeur);
            notes.put(idxNote, valeur);
        } catch (Exception e) {
            LOG.error("Echec lors de l'acc�s en SET au champ note" + idxNote + ".", e);
        }
    }

    /**
     * R�cup�re un tableau de notes.
     * 
     * <em>Attention, le tableau de notes est indic� dans l'intervalle [0; 19].
     * Dans le cas d'un acc�s par {@link Rang}, il faut retirer 1 � la valeur du rang.</em>
     * 
     * <pre>
     * batchGetNotes[0] = note1
     * ...
     * batchGetNotes[19] = note20
     * </pre>
     * 
     * @return tableau des notes
     */
    public Double[] batchGetNotes() {
        Double[] tableauNotes = new Double[20];
        tableauNotes[0] = note1;
        tableauNotes[1] = note2;
        tableauNotes[2] = note3;
        tableauNotes[3] = note4;
        tableauNotes[4] = note5;
        tableauNotes[5] = note6;
        tableauNotes[6] = note7;
        tableauNotes[7] = note8;
        tableauNotes[8] = note9;
        tableauNotes[9] = note10;
        tableauNotes[10] = note11;
        tableauNotes[11] = note12;
        tableauNotes[12] = note13;
        tableauNotes[13] = note14;
        tableauNotes[14] = note15;
        tableauNotes[15] = note16;
        tableauNotes[16] = note17;
        tableauNotes[17] = note18;
        tableauNotes[18] = note19;
        tableauNotes[19] = note20;
        return tableauNotes;
    }

    /**
     * Valorise les notes de l'�l�ve � partir d'un tableau de notes.
     * 
     * <em>Attention, le tableau de notes est indic� dans l'intervalle [0; 19].
     * Dans le cas d'un acc�s par {@link Rang}, il faut retirer 1 � la valeur du rang.</em>
     * 
     * <pre>
     * note1 = tableauNotes[0]
     * ...
     * note20 = tableauNotes[19]
     * </pre>
     * 
     * @param tableauNotes
     *            tableau des notes
     */
    public void batchSetNotes(Double[] tableauNotes) {
        note1 = tableauNotes[0];
        note2 = tableauNotes[1];
        note3 = tableauNotes[2];
        note4 = tableauNotes[3];
        note5 = tableauNotes[4];
        note6 = tableauNotes[5];
        note7 = tableauNotes[6];
        note8 = tableauNotes[7];
        note9 = tableauNotes[8];
        note10 = tableauNotes[9];
        note11 = tableauNotes[10];
        note12 = tableauNotes[11];
        note13 = tableauNotes[12];
        note14 = tableauNotes[13];
        note15 = tableauNotes[14];
        note16 = tableauNotes[15];
        note17 = tableauNotes[16];
        note18 = tableauNotes[17];
        note19 = tableauNotes[18];
        note20 = tableauNotes[19];
    }

    /**
     * Fournis le voeu de l'�l�ve correspondant au tour et au rang donn�s.
     * Si il n'y a pas de candidature ou de voeu, renvoie null.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param rang
     *            le rang du voeu
     * @return le voeu correspondant au tour et au rang donn�s, null s'il n'existe pas
     */
    public VoeuEleve voeuDuTourEtDeRang(short numeroTour, Integer rang) {
        Candidature candidature = candidatures.get(numeroTour);
        if (candidature == null) {
            return null;
        }

        return candidature.voeuDeRang(rang);
    }

    /**
     * @return vrai si l'�l�ve poss�de au moins un voeu sur une offre de type "bar�me avec �valuations/notes"
     *         pour toute la campagne d'affectation (quelque soit le tour).
     */
    public boolean getPossedeVoeuBaremeAvecNoteEvaluationPourCampagne() {

        boolean possedeVoeuBaremeAvecNotesEvaluations = false;

        Iterator<Candidature> candIt = candidatures.values().iterator();

        while (!possedeVoeuBaremeAvecNotesEvaluations && candIt.hasNext()) {

            Candidature candidature = candIt.next();
            possedeVoeuBaremeAvecNotesEvaluations = candidature.getPossedeVoeuBaremeAvecNoteEvaluation();
        }

        LOG.debug("INE=" + ine + " - possedeVoeuBaremeAvecNotesEvaluations = "
                + possedeVoeuBaremeAvecNotesEvaluations);
        return possedeVoeuBaremeAvecNotesEvaluations;
    }

    @Override
    public String toString() {
        String chaineEleve = "";
        if (getEtablissementNational() != null) {
            chaineEleve += getEtablissementNational().getId() + " - ";
        }

        chaineEleve += getIne() + " - " + getNom() + " " + getPrenom();
        if (StringUtils.isNotEmpty(getPrenom2())) {
            chaineEleve += " " + getPrenom2();
        }
        if (StringUtils.isNotEmpty(getPrenom3())) {
            chaineEleve += " " + getPrenom3();
        }

        return chaineEleve;
    }

    /**
     * toString alternatif avec la division de l'�l�ve.
     * 
     * @return une cha�ne de caract�re correspondant � l'�l�ve
     */
    public String toStringAvecDivision() {
        String chaineEleve = "";

        chaineEleve += getNom() + " " + getPrenom();
        if (StringUtils.isNotBlank(getPrenom2())) {
            chaineEleve += " " + getPrenom2();
        }
        if (StringUtils.isNotBlank(getPrenom3())) {
            chaineEleve += " " + getPrenom3();
        }
        chaineEleve += " (" + getIne();
        if (StringUtils.isNotBlank(getClasse())) {
            chaineEleve += " - " + getClasse();
        }
        chaineEleve += ")";
        return chaineEleve;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof Eleve)) {
            return false;
        }
        Eleve castOther = (Eleve) other;
        return new EqualsBuilder().append(this.getIne(), castOther.getIne()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getIne()).toHashCode();
    }

    /**
     * @return the palierOrigine
     */
    public Palier getPalierOrigine() {
        return palierOrigine;
    }

    /**
     * @param palierOrigine
     *            the palierOrigine to set
     */
    public void setPalierOrigine(Palier palierOrigine) {
        this.palierOrigine = palierOrigine;
    }

    /**
     * @return the decisionsOrientationEleve
     */
    public Set<DecisionOrientationEleve> getDecisionsOrientationEleve() {
        return decisionsOrientationEleve;
    }

    /**
     * @param decisionsOrientationEleve
     *            the decisionsOrientationEleve to set
     */
    public void setDecisionsOrientationEleve(Set<DecisionOrientationEleve> decisionsOrientationEleve) {
        this.decisionsOrientationEleve = decisionsOrientationEleve;
    }

    /**
     * @return the candidatures
     */
    public Map<Short, Candidature> getCandidatures() {
        return candidatures;
    }

    /**
     * @param candidatures
     *            the candidatures to set
     */
    public void setCandidatures(Map<Short, Candidature> candidatures) {
        this.candidatures = candidatures;
    }

    /**
     * Donne la candidature de l'�l�ve pour le tour demand�, si elle existe.
     * 
     * @param numeroTour
     *            le num�ro du tour demand�
     * @return la candidature du tour en cours pour l'�l�ve, ou null si l'�l�ve n'est pas candidat
     */
    public Candidature getCandidature(short numeroTour) {
        return candidatures.get(numeroTour);
    }

    /**
     * @return the flagDecOriManuelle
     */
    public String getFlagDecOriManuelle() {
        return flagDecOriManuelle;
    }

    /**
     * @param flagDecOriManuelle
     *            the flagDecOriManuelle to set
     */
    public void setFlagDecOriManuelle(String flagDecOriManuelle) {
        this.flagDecOriManuelle = flagDecOriManuelle;
    }

    /**
     * Fournit l'ensemble des voeux de l'�l�ve saisis depuis le t�l�service affectation.
     *
     * @return Ensemble des voeux de l'�l�ve saisis depuis le t�l�service affectation.
     */
    public Set<VoeuTeleservice> getVoeuxTeleservice() {
        return voeuxTeleservice;
    }

    /**
     * Sp�cifie l'ensemble des voeux de l'�l�ve saisis depuis le t�l�service affectation.
     *
     * @param voeuxTeleservice
     *            Ensemble des voeux de l'�l�ve saisis depuis le t�l�service affectation.
     */
    public void setVoeuxTeleservice(Set<VoeuTeleservice> voeuxTeleservice) {
        this.voeuxTeleservice = voeuxTeleservice;
    }

    /**
     * Sp�cifie le statut de la d�termination de la zone g�ographique.
     *
     * @param statutZoneGeo
     *            Statut de la d�termination de la zone g�ographique.
     */
    public void setStatutZoneGeo(StatutZoneGeoEnum statutZoneGeo) {
        this.statutZoneGeo = statutZoneGeo;
    }

    /**
     * Fournit le statut de la d�termination de la zone g�ographique.
     *
     * @return Statut de la d�termination de la zone g�ographique.
     */
    public StatutZoneGeoEnum getStatutZoneGeo() {
        return statutZoneGeo;
    }

    /**
     * M�thode de r�cup�ration des voeux de l'�l�ve pour un tour donn�.
     * 
     * @param numeroTour
     *            le num�ro de tour s�lectionn�
     * @return la liste des voeux de l'�l�ve pour le tour sp�cifi�.
     */
    public List<VoeuEleve> voeuxDuTour(short numeroTour) {
        Candidature candidature = candidatures.get(numeroTour);
        return candidature != null ? candidature.getVoeux() : new ArrayList<VoeuEleve>();
    }

    /** @return vrai si l'�l�ve vient d'une formation 3�me SEGPA, sinon faux. */
    public boolean estFormationOrigine3emeSegpa() {

        if (formation == null) {
            return false;
        }

        return MefStat.CODE_MEFSTAT4_3SEGPA.equals(formation.getMefStat().getCodeMefStat4());
    }

    /**
     * M�thode de v�rification de la validit� des informations d'identit� de l'�l�ve.
     * La coh�rence par rapport aux voeux formul�s n'est pas effectu�e.
     * 
     * @return vrai si les informations sont correctes, faux si des incoh�rences ont �t� d�tect�es
     */
    public boolean identiteValide() {

        if (age != null && (age < AGE_MINI || age > AGE_MAXI)) {
            return false;
        }

        if (!hasResponsableNotifiable()) {
            return false;
        }

        // si l'�tablissement n'est pas renseign�, le d�partement doit �tre indiqu�
        if (etablissementNational == null && departement == null) {
            return false;
        }

        OuvrableManager ouvrableManager = SpringUtils.getBean(OuvrableManager.class);
        // saisie d'un �tab. origine ouvert
        if (etablissementNational != null && !ouvrableManager.isOrigineEleve(etablissementNational)) {
            return false;
        }

        // saisie d'une formation origine ouverte et pas de type AFFECTATION
        if (formation != null && (!ouvrableManager.isOrigine(formation)
                || formation.getCodeInterne().equals(Formation.CODE_INTERNE_AFFECTATION))) {
            return false;
        }

        // saisie d'une LV1 origine ouverte
        if (matiereLv1 != null && (!MatiereHelper.isLv1(matiereLv1) || !ouvrableManager.isOrigine(matiereLv1))) {
            return false;
        }

        // contr�les sur la LV2 :
        // - la LV1 doit �tre saisie si la LV2 est saisie
        // - la LV1 et la LV2 doivent �tre diff�rentes
        // - la LV2 saisie doit �tre une LV2 ou une LV1
        // - la LV2 saisie doit r�pondre � la notion d'origine
        if (matiereLv2 != null && (matiereLv1 == null || matiereLv1.equals(matiereLv2)
                || (!MatiereHelper.isLv2(matiereLv2) && !MatiereHelper.isLv1(matiereLv2))
                || !ouvrableManager.isOrigine(matiereLv2))) {
            return false;
        }

        // les options :
        // - doivent r�pondre � la notion d'origine
        // - ne doivent pas �tre des comp�tences
        if (optionOrigine2 != null
                && (!ouvrableManager.isOrigine(optionOrigine2) || MatiereHelper.isCompetence(optionOrigine2))) {
            return false;
        }

        if (optionOrigine3 != null && (MatiereHelper.isLv1(optionOrigine3)
                || !ouvrableManager.isOrigine(optionOrigine3) || MatiereHelper.isCompetence(optionOrigine3))) {
            return false;
        }

        // les options doivent �tre diff�rentes
        if (optionOrigine2 != null && optionOrigine2.equals(optionOrigine3)) {
            return false;
        }

        // la zone g�o est obligatoire
        if (zoneGeo == null) {
            return false;
        }

        // L'adresse est obligatoire
        if (adresseResidence == null) {
            return false;
        }
        if (!adresseResidence.estValide()) {
            return false;
        }
        return true;
    }

    /**
     * Calcule la moyenne des notes de l'�l�ve.
     * 
     * La moyenne est arrondie � deux d�cimales.
     * Dans le cas o� l'�l�ve n'a pas de notes, la moyenne r�sultante est nulle.
     * 
     * @param nombreRang
     *            le nombre de rangs � prendre en compte
     * @return la moyenne (ou nulle si elle n'existe pas)
     */
    public Double calculerMoyenne(int nombreRang) {
        // Lecture des notes de l'�l�ve
        Double[] tableauNotes = batchGetNotes();

        int nbNote = 0;
        double total = 0;
        for (int i = 0; i < nombreRang; i++) {

            // On prend en compte la note de ce rang pour la moyenne s'il y a une note
            Double notePourRang = tableauNotes[i];
            if (notePourRang != null) {
                nbNote++;
                total += notePourRang;
            }
        }

        // Calcul de la moyenne si elle est d�finie
        if (nbNote != 0) {
            return Math.round(total / nbNote * 100) / 100.0;
        } else {
            return null;
        }
    }

    /**
     * M�thode d�terminant si l'�l�ve poss�de au moins une note saisie.
     * 
     * @return vrai si l'�l�ve poss�de au moins une note, faux sinon
     */
    public boolean possedeNotes() {
        return note1 != null || note2 != null || note3 != null || note4 != null || note5 != null || note6 != null
                || note7 != null || note8 != null || note9 != null || note10 != null || note11 != null
                || note12 != null || note13 != null || note14 != null || note15 != null || note16 != null
                || note17 != null || note18 != null || note19 != null || note20 != null;
    }

    /** Supprime toutes les notes et la moyenne de l'�l�ve. */
    public void supprimerNotes() {
        LOG.debug("Suppression de toutes les notes et de la moyenne de l'�l�ve " + ine);
        for (int idxNote = 1; idxNote <= Rang.NOMBRE_MAXIMUM; idxNote++) {
            setNote(idxNote, null);
        }
        noteMoyenne = null;
    }

    /**
     * M�thode d�terminant si l'�l�ve poss�de au moins �valuation de socle et une �valuation de discipline.
     * 
     * @return vrai si l'�l�ve a au moins une �valuation de socle et de discipline
     */
    public boolean possedeEvaluationMinimaliste() {
        return (!evaluationsSocle.isEmpty()) && (!evaluationsDiscipline.isEmpty());
    }

    /** Supprime toutes les �valuations de l'�l�ve. */
    public void supprimerToutesEvaluations() {
        LOG.info("Suppression de toutes les �valuations (socle, disciplines, compl�mentaires) de l'�l�ve " + ine);
        evaluationsSocle.clear();
        evaluationsDiscipline.clear();
        evaluationsComplementaires.clear();
    }

    /** Supprime les �valuations de disciplines de l'�l�ve. */
    public void supprimerEvaluationsDisciplines() {
        LOG.info("Suppression de �valuations de disciplines de l'�l�ve " + ine);
        evaluationsDiscipline.clear();
    }


    /**
     * Cette m�thode permet d'obtenir un code pour l'�tablissement ou � d�faut le d�partement d'origine de l'�l�ve.
     * 
     * <ul>
     * <li>Si l'�tablissement est connu, on renvoie le num�ro UAI (code RNE).</li>
     * <li>Si l'�tablissement est inconnu, on renvoie le code du d�partement MEN d'origine de l'�l�ve.</li>
     * </ul>
     * 
     * Exemple : 0540001H ou � d�faut 054, ce qui permet de filtrer sur ce qui commence par le pr�fixe 054.
     * 
     * @return num�ro UAI (code RNE) de l'�tablissement d'origine ou � d�faut le code d�partement MEN origine.
     */
    public String numeroUaiOuDepartementMenOrigine() {

        if (etablissement == null) {
            return departement.getCodeMen();
        }

        return etablissement.getId();
    }

    /**
     * Liste les mati�res origine de l'�l�ve.
     * La liste contient dans l'ordre les langues vivantes 1 et 2 puis les options.
     * La liste est tass�e : il n'y a pas de null si la valeur correspondante n'est pas renseign�e.
     * 
     * @return liste des mati�res tass�es d'origine de l'�l�ve
     */
    public List<Matiere> listeMatieresOrigine() {

        List<Matiere> matieresOrigine = new ArrayList<>(4);

        if (matiereLv1 != null) {
            matieresOrigine.add(matiereLv1);
        }
        if (matiereLv2 != null) {
            matieresOrigine.add(matiereLv2);
        }
        if (optionOrigine2 != null) {
            matieresOrigine.add(optionOrigine2);
        }
        if (optionOrigine3 != null) {
            matieresOrigine.add(optionOrigine3);
        }

        return matieresOrigine;
    }

    /** @return D�termine le mode �valuation/note pour l'�l�ve. */
    public ModeEvalNote modeEvalNote() {

        ModeEvalNote mode = ModeEvalNote.INDETERMINE;

        StringBuffer diagnosticBuffer = new StringBuffer();
        diagnosticBuffer.append("El�ve INE:");
        diagnosticBuffer.append(ine);

        diagnosticBuffer.append(" Palier:");
        if (palierOrigine == null) {
            diagnosticBuffer.append("ind�termin�");
        } else {
            diagnosticBuffer.append(palierOrigine.getCode());
            mode = ModeEvalNote.getModePourPalier(palierOrigine.getCode());
        }

        if (LOG.isDebugEnabled()) {
            diagnosticBuffer.append(" => mode ");
            diagnosticBuffer.append(mode.getLibelle());
            LOG.debug(diagnosticBuffer.toString());
        }

        return mode;
    }

    /**
     * D�termine si l'�l�ve poss�de au moins un responsable notifiable.
     *
     * @return vrai si l'�l�ve poss�de au moins un responsable notifiable.
     */
    public boolean hasResponsableNotifiable() {
        return (!getResponsablesNotifiables().isEmpty());
    }

    /**
     * D�termine si l'�l�ve poss�de au moins un responsable NON notifiable.
     *
     * @return vrai si l'�l�ve poss�de au moins un responsable NON notifiable.
     */
    public boolean hasResponsableLegalNonNotifiable() {
        Set<Responsable> responsables = getResponsables();
        List<Responsable> notifiables = getResponsablesNotifiables();
        if (responsables != null) {
            List<Responsable> responsablesLegaux = responsables.stream().filter(responsable -> responsable.estRepresentantLegal()).collect(Collectors.toList());
            if (responsablesLegaux != null && notifiables != null) {
                return responsablesLegaux.size() != notifiables.size();
            }
        }
        return notifiables == null || notifiables.isEmpty();
    }

    /**
     * @return vrai si l'�l�ve peut �tre concern� pour recevoir des �valuations de LSU, sinon faux.
     */
    public boolean estPotentiellementConcerneImportsLsu() {

        if (etablissement == null || palierOrigine == null) {
            return false;
        }

        return Palier.CODE_PALIER_3EME.equals(palierOrigine.getCode());
    }

    /**
     * D�termine si un �l�ve est majeur ou non.
     * 
     * @return true si majeur
     */
    public boolean isMajeur() {
        return EleveHelper.isMajeur(dateNaissance);
    }
}
