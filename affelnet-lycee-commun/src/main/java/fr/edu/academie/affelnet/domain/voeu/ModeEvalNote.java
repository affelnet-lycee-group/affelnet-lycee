/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import fr.edu.academie.affelnet.domain.nomenclature.Palier;

/** Cette �num�ration permet d'identifier si l'on �value ou si l'on note un �l�ve. */
public enum ModeEvalNote {

    /** Mode ind�termin�. */
    INDETERMINE("ind�termin�", false),

    /** Utilisation d'�valuations (Compatibles LSU : comp�tences, disciplines, compl�mentaires). */
    EVALUATION("�valuation", true),

    /** Utilisation de notes (sur 20). */
    NOTE("note", false);

    /** Libell� d�crivant le mode. */
    private String libelle;

    /** Le mode �valuation/note est-il sensible au MefStat11 pr�s. */
    private boolean sensibleMefStat11;

    /**
     * Constructeur.
     * 
     * @param libelle
     *            le libell� d�crivant le mode
     * @param sensibleMefStat11
     *            le mode est-il sensible au MefStat11 pr�s
     */
    private ModeEvalNote(String libelle, boolean sensibleMefStat11) {
        this.libelle = libelle;
        this.sensibleMefStat11 = sensibleMefStat11;
    }

    /**
     * @return libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle
     *            le libell� d�crivant le mode
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * D�termine le mode d'�valuation selon le code de palier fourni.
     * 
     * @param codePalier
     *            code palier
     * @return mode d'�valuation
     */
    public static ModeEvalNote getModePourPalier(Short codePalier) {

        if (codePalier == null) {
            return INDETERMINE;
        }

        if (Palier.CODE_PALIER_3EME.equals(codePalier)) {
            return EVALUATION;
        }

        if (Palier.CODE_PALIER_2NDE.equals(codePalier)) {
            return NOTE;
        }

        return INDETERMINE;
    }

    /**
     * @return Le mode �valuation/note est-il sensible au MefStat11 pr�s ?
     */
    public boolean isSensibleMefStat11() {
        return sensibleMefStat11;
    }

}
