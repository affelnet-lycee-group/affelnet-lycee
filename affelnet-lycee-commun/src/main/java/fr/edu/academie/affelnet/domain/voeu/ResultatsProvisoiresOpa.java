/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;

/**
 * Les r�sultats provisoires d'une OPA.
 */
public class ResultatsProvisoiresOpa extends Datable implements Serializable {

    /** Code de d�cision provisoire - Affect� provisoire. */
    public static final short CODE_DECISION_PROVISOIRE_AFFECTE = 1;

    /** Code de d�cision provisoire - Liste suppl�mentaire provisoire. */
    public static final short CODE_DECISION_PROVISOIRE_LISTE_SUPPLEMENTAIRE = 2;

    /** Code de d�cision provisoire - Refus� provisoire. */
    public static final short CODE_DECISION_PROVISOIRE_REFUSE = 3;

    /** Code de d�cision provisoire - Abandon (saisie manuelle uniquement). */
    public static final short CODE_DECISION_PROVISOIRE_ABANDON = 4;

    /** Code de d�cision provisoire - En attente de signature du contrat (apprentissage en commission). */
    public static final short CODE_DECISION_PROVISOIRE_EN_ATTENTE_SIGNATURE_CONTRAT = 5;

    /** Code de d�cision provisoire - Admis, contrat sign� (apprentissage en commission)). */
    public static final short CODE_DECISION_PROVISOIRE_ADMIS_CONTRAT_SIGNE = 6;

    /** Code de d�cision provisoire - Dossier absent (commission). */
    public static final short CODE_DECISION_PROVISOIRE_DOSSIER_ABSENT = 8;

    /** Code de d�cision provisoire - Non trait�. */
    public static final short CODE_DECISION_PROVISOIRE_NON_TRAITE = 9;

    /** Code de d�cision provisoire - Num�ro de version de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /**
     * L'identifiant.
     */
    private VoeuElevePK id;

    /** L'op�ration programm�e d'affectation � l'origine de ce r�sultat. */
    private OperationProgrammeeAffectation opa;

    /** Le voeu de l'�l�ve pour lequel on a ce r�sultat dans l'OPA. */
    private VoeuEleve voeuEleve;

    /**
     * Le bonus de l'avis du chef d'�tablissement d'origine.
     */
    private Integer bonusAvisChefEtablissement;

    /**
     * Le bonus de l'avis de gestion.
     */
    private Integer bonusAvisDeGestion;

    /**
     * Le bonus de l'avis DSDEN.
     */
    private Integer bonusAvisDsden;

    /**
     * Le bonus de l'avis passerelle.
     */
    private Integer bonusAvisPasserelle;

    /**
     * Le bonus pour le redoublement.
     */
    private Integer bonusDoublement;

    /** Le bonus pour les �l�ves boursiers. */
    private Integer bonusBoursier;

    /**
     * Le bonus fili�re.
     */
    private Integer bonusFiliere;

    /**
     * Le bonus lien zone g�ographique.
     */
    private Integer bonusLienZoneGeo;

    /**
     * Le bonus pour le premi�re voeu.
     */
    private Integer bonusVoeu1;

    /**
     * Le bonus rapprochement.
     */
    private Integer bonusRapprochement;

    /**
     * Le bonus pour le retard scolaire.
     */
    private Integer bonusRetardScolaire;

    /**
     * Le bonus pour le voeu de fili�re.
     */
    private Integer bonusVoeuDeFiliere;

    /**
     * le bonus acad�mique.
     */
    private Integer bonusAcademique;

    /**
     * Le bonus de d�rogation.
     */
    private Integer bonusDerogation;

    /**
     * Le bar�me pour les notes.
     */
    private Double baremeNotes;

    /**
     * Le bar�me pour les �valuations du socle.
     */
    private Double baremeEvalSocle;

    /**
     * Le bar�me pour les �valuations des disciplines.
     */
    private Double baremeEvalDiscipline;

    /**
     * Le bar�me pour les �valuations compl�mentaires.
     */
    private Double baremeEvalCompl;

    /**
     * Le bar�me pour les evaluations.
     */
    private Double baremeEvaluations;

    /**
     * Le bar�me.
     */
    private Double bareme;

    /**
     * Le code de d�cision provisoire.
     */
    private Short codeDecisionProvisoire;

    /**
     * Le num�ro en liste suppl�mentaire.
     */
    private Short numeroListeSupp;

    /**
     * L'indicateur de s�curisation.
     */
    private String flagSecurisation;

    /**
     * Coefficient du groupe origine de l'�l�ve.
     */
    private Double coefficientGroupeOrigine;

    /** Constructeur de r�sultat provisoire d'OPA. */
    public ResultatsProvisoiresOpa() {

        // Initialisation des bonus
        bonusAvisChefEtablissement = 0;
        bonusAvisDeGestion = 0;
        bonusAvisDsden = 0;
        bonusAvisPasserelle = 0;
        bonusDoublement = 0;
        bonusBoursier = 0;
        bonusFiliere = 0;
        bonusLienZoneGeo = 0;
        bonusVoeu1 = 0;
        bonusRapprochement = 0;
        bonusRetardScolaire = 0;
        bonusVoeuDeFiliere = 0;
        bonusAcademique = 0;
        bonusDerogation = 0;

        // Initialisation des bar�mes
        baremeNotes = 0.0D;
        baremeEvalSocle = 0.0D;
        baremeEvalDiscipline = 0.0D;
        baremeEvalCompl = 0.0D;
        baremeEvaluations = baremeEvalSocle + baremeEvalDiscipline + baremeEvalCompl;
        bareme = 0.0D;
        coefficientGroupeOrigine = null;

        // Initialisation � vide des d�cisions provisoires
        codeDecisionProvisoire = null;
        numeroListeSupp = null;

        // initiation du flag de s�curisation
        flagSecurisation = Flag.NON;

        // Dates
        setDateCreation(new Date());
        setDateMAJ(null);
    }

    /**
     * @return the id
     */
    public VoeuElevePK getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(VoeuElevePK id) {
        this.id = id;
    }

    /**
     * @return L'op�ration programm�e d'affectation � l'origine de ce r�sultat
     */
    public OperationProgrammeeAffectation getOpa() {
        return opa;
    }

    /**
     * @param opa
     *            L'op�ration programm�e d'affectation � l'origine de ce r�sultat
     */
    public void setOpa(OperationProgrammeeAffectation opa) {
        this.opa = opa;
    }

    /**
     * @return Le voeu de l'�l�ve pour lequel on a ce r�sultat dans l'OPA
     */
    public VoeuEleve getVoeuEleve() {
        return voeuEleve;
    }

    /**
     * @param voeuEleve
     *            Le voeu de l'�l�ve pour lequel on a ce r�sultat dans l'OPA
     */
    public void setVoeuEleve(VoeuEleve voeuEleve) {
        this.voeuEleve = voeuEleve;
    }

    /**
     * @return the bonusAvisChefEtablissement
     */
    public Integer getBonusAvisChefEtablissement() {
        return bonusAvisChefEtablissement;
    }

    /**
     * @param bonusAvisChefEtablissement
     *            the bonusAvisChefEtablissement to set
     */
    public void setBonusAvisChefEtablissement(Integer bonusAvisChefEtablissement) {
        this.bonusAvisChefEtablissement = bonusAvisChefEtablissement;
    }

    /**
     * @return the bonusAvisDeGestion
     */
    public Integer getBonusAvisDeGestion() {
        return bonusAvisDeGestion;
    }

    /**
     * @param bonusAvisDeGestion
     *            the bonusAvisDeGestion to set
     */
    public void setBonusAvisDeGestion(Integer bonusAvisDeGestion) {
        this.bonusAvisDeGestion = bonusAvisDeGestion;
    }

    /**
     * @return the bonusAvisDsden
     */
    public Integer getBonusAvisDsden() {
        return bonusAvisDsden;
    }

    /**
     * @param bonusAvisDsden
     *            the bonusAvisDsden to set
     */
    public void setBonusAvisDsden(Integer bonusAvisDsden) {
        this.bonusAvisDsden = bonusAvisDsden;
    }

    /**
     * @return the bonusAvisPasserelle
     */
    public Integer getBonusAvisPasserelle() {
        return bonusAvisPasserelle;
    }

    /**
     * @param bonusAvisPasserelle
     *            the bonusAvisPasserelle to set
     */
    public void setBonusAvisPasserelle(Integer bonusAvisPasserelle) {
        this.bonusAvisPasserelle = bonusAvisPasserelle;
    }

    /**
     * @return the bonusDoublement
     */
    public Integer getBonusDoublement() {
        return bonusDoublement;
    }

    /**
     * @param bonusDoublement
     *            the bonusDoublement to set
     */
    public void setBonusDoublement(Integer bonusDoublement) {
        this.bonusDoublement = bonusDoublement;
    }

    /**
     * @return Le bonus pour les �l�ves boursiers.
     */
    public Integer getBonusBoursier() {
        return bonusBoursier;
    }

    /**
     * @param bonusBoursier
     *            Le bonus pour les �l�ves boursiers.
     */
    public void setBonusBoursier(Integer bonusBoursier) {
        this.bonusBoursier = bonusBoursier;
    }

    /**
     * @return the bonusFiliere
     */
    public Integer getBonusFiliere() {
        return bonusFiliere;
    }

    /**
     * @param bonusFiliere
     *            the bonusFiliere to set
     */
    public void setBonusFiliere(Integer bonusFiliere) {
        this.bonusFiliere = bonusFiliere;
    }

    /**
     * @return the bonusLienZoneGeo
     */
    public Integer getBonusLienZoneGeo() {
        return bonusLienZoneGeo;
    }

    /**
     * @param bonusLienZoneGeo
     *            the bonusLienZoneGeo to set
     */
    public void setBonusLienZoneGeo(Integer bonusLienZoneGeo) {
        this.bonusLienZoneGeo = bonusLienZoneGeo;
    }

    /**
     * @return the bonusVoeu1
     */
    public Integer getBonusVoeu1() {
        return bonusVoeu1;
    }

    /**
     * @param bonusVoeu1
     *            the bonusVoeu1 to set
     */
    public void setBonusVoeu1(Integer bonusVoeu1) {
        this.bonusVoeu1 = bonusVoeu1;
    }

    /**
     * @return the bonusRapprochement
     */
    public Integer getBonusRapprochement() {
        return bonusRapprochement;
    }

    /**
     * @param bonusRapprochement
     *            the bonusRapprochement to set
     */
    public void setBonusRapprochement(Integer bonusRapprochement) {
        this.bonusRapprochement = bonusRapprochement;
    }

    /**
     * @return the bonusRetardScolaire
     */
    public Integer getBonusRetardScolaire() {
        return bonusRetardScolaire;
    }

    /**
     * @param bonusRetardScolaire
     *            the bonusRetardScolaire to set
     */
    public void setBonusRetardScolaire(Integer bonusRetardScolaire) {
        this.bonusRetardScolaire = bonusRetardScolaire;
    }

    /**
     * @return the bonusVoeuDeFiliere
     */
    public Integer getBonusVoeuDeFiliere() {
        return bonusVoeuDeFiliere;
    }

    /**
     * @param bonusVoeuDeFiliere
     *            the bonusVoeuDeFiliere to set
     */
    public void setBonusVoeuDeFiliere(Integer bonusVoeuDeFiliere) {
        this.bonusVoeuDeFiliere = bonusVoeuDeFiliere;
    }

    /**
     * @return the bonusAcademique
     */
    public Integer getBonusAcademique() {
        return bonusAcademique;
    }

    /**
     * @param bonusAcademique
     *            the bonusAcademique to set
     */
    public void setBonusAcademique(Integer bonusAcademique) {
        this.bonusAcademique = bonusAcademique;
    }

    /**
     * @return the bonusDerogation
     */
    public Integer getBonusDerogation() {
        return bonusDerogation;
    }

    /**
     * @param bonusDerogation
     *            the bonusDerogation to set
     */
    public void setBonusDerogation(Integer bonusDerogation) {
        this.bonusDerogation = bonusDerogation;
    }

    /**
     * @return the baremeNotes
     */
    public Double getBaremeNotes() {
        return baremeNotes;
    }

    /**
     * @param baremeNotes
     *            the baremeNotes to set
     */
    public void setBaremeNotes(Double baremeNotes) {
        this.baremeNotes = baremeNotes;
    }

    /**
     * @return the bareme
     */
    public Double getBareme() {
        return bareme;
    }

    /**
     * @param bareme
     *            the bareme to set
     */
    public void setBareme(Double bareme) {
        this.bareme = bareme;
    }

    /**
     * @return the codeDecisionProvisoire
     */
    public Short getCodeDecisionProvisoire() {
        return codeDecisionProvisoire;
    }

    /**
     * @param codeDecisionProvisoire
     *            the codeDecisionProvisoire to set
     */
    public void setCodeDecisionProvisoire(Short codeDecisionProvisoire) {
        this.codeDecisionProvisoire = codeDecisionProvisoire;
    }

    /**
     * @return the numeroListeSupp
     */
    public Short getNumeroListeSupp() {
        return numeroListeSupp;
    }

    /**
     * @param numeroListeSupp
     *            the numeroListeSupp to set
     */
    public void setNumeroListeSupp(Short numeroListeSupp) {
        this.numeroListeSupp = numeroListeSupp;
    }

    /**
     * @return the flagSecurisation
     */
    public String getFlagSecurisation() {
        return flagSecurisation;
    }

    /**
     * @param flagSecurisation
     *            the flagSecurisation to set
     */
    public void setFlagSecurisation(String flagSecurisation) {
        this.flagSecurisation = flagSecurisation;
    }

    /**
     * @return the baremeEvalSocle
     */
    public Double getBaremeEvalSocle() {
        return baremeEvalSocle;
    }

    /**
     * @param baremeEvalSocle
     *            the baremeEvalSocle to set
     */
    public void setBaremeEvalSocle(Double baremeEvalSocle) {
        this.baremeEvalSocle = baremeEvalSocle;
    }

    /**
     * @return the baremeEvalDiscipline
     */
    public Double getBaremeEvalDiscipline() {
        return baremeEvalDiscipline;
    }

    /**
     * @param baremeEvalDiscipline
     *            the baremeEvalDiscipline to set
     */
    public void setBaremeEvalDiscipline(Double baremeEvalDiscipline) {
        this.baremeEvalDiscipline = baremeEvalDiscipline;
    }

    /**
     * @return the baremeEvalCompl
     */
    public Double getBaremeEvalCompl() {
        return baremeEvalCompl;
    }

    /**
     * @param baremeEvalCompl
     *            the baremeEvalCompl to set
     */
    public void setBaremeEvalCompl(Double baremeEvalCompl) {
        this.baremeEvalCompl = baremeEvalCompl;
    }

    /**
     * @return the baremeEvaluations
     */
    public Double getBaremeEvaluations() {
        return baremeEvaluations;
    }

    /**
     * @param baremeEvaluations
     *            the baremeEvaluations to set
     */
    public void setBaremeEvaluations(Double baremeEvaluations) {
        this.baremeEvaluations = baremeEvaluations;
    }

    /**
     * @return the coefficientGroupeOrigine
     */
    public Double getCoefficientGroupeOrigine() {
        return coefficientGroupeOrigine;
    }

    /**
     * @param coefficientGroupeOrigine
     *            the coefficientGroupeOrigine to set
     */
    public void setCoefficientGroupeOrigine(Double coefficientGroupeOrigine) {
        this.coefficientGroupeOrigine = coefficientGroupeOrigine;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof ResultatsProvisoiresOpa)) {
            return false;
        }
        ResultatsProvisoiresOpa castOther = (ResultatsProvisoiresOpa) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * R�initialise un r�sultat d'op�ration provisoire d'affectation.
     * 
     * Cette op�ration est consid�r�e comme une mise � jour : la date de mise � jour est modifi�e.
     * 
     * @param conserverSecurisation
     *            Vrai s'il faut conserver le bonus s�curisation, sinon faux
     */
    public void reinitialiserResultats(boolean conserverSecurisation) {

        bonusAvisChefEtablissement = 0;
        bonusAvisDeGestion = 0;
        bonusAvisDsden = 0;
        bonusAvisPasserelle = 0;
        bonusDoublement = 0;
        bonusBoursier = 0;
        bonusFiliere = 0;
        bonusLienZoneGeo = 0;
        bonusVoeu1 = 0;
        bonusRapprochement = 0;
        bonusRetardScolaire = 0;
        bonusVoeuDeFiliere = 0;
        bonusAcademique = 0;
        bonusDerogation = 0;

        // On r�initialie le flag de s�curisation
        // - s'il n'est pas demand� de le conserver
        // - ou si le r�sultat provisoire n'est plus concern� par la s�curisation
        if (!conserverSecurisation || !securisable()) {
            flagSecurisation = Flag.NON;
        }

        baremeNotes = 0.0D;
        baremeEvalSocle = 0.0D;
        baremeEvalDiscipline = 0.0D;
        baremeEvalCompl = 0.0D;
        baremeEvaluations = baremeEvalSocle + baremeEvalDiscipline + baremeEvalCompl;
        bareme = 0.0D;
        coefficientGroupeOrigine = null;

        codeDecisionProvisoire = null;
        numeroListeSupp = null;

        // La r�initialisation est une mise � jour
        setDateMAJ(new Date());
    }

    /** @return le libell� de la d�cision provisoire */
    public String decisionProvisoire() {
        switch (getCodeDecisionProvisoire()) {
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_AFFECTE:
                return "Pris";
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_LISTE_SUPPLEMENTAIRE:
                return "Liste suppl�mentaire n�" + getNumeroListeSupp();
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_REFUSE:
                return "Refus�";
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_ABANDON:
                return "Abandon";
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_EN_ATTENTE_SIGNATURE_CONTRAT:
                return "En attente de ignature du contrat";
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_ADMIS_CONTRAT_SIGNE:
                return "Admis, contrat sign�";
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_DOSSIER_ABSENT:
                return "Dossier absent";
            case ResultatsProvisoiresOpa.CODE_DECISION_PROVISOIRE_NON_TRAITE:
                return "Non trait�";
            default:
                return "";
        }
    }

    /**
     * M�thode d�terminant si un r�sultat provisoire est concern� par la s�curisation de l'OPA qui le produit.
     * 
     * @return vrai si le r�sultat provisoire peut �tre s�curis� par l'OPA, faux sinon.
     */
    public boolean securisable() {
        Set<MefStat> mefStats4Securises = opa.getMefstatsSecurises();

        MefStat mefStatResultat = getVoeuEleve().getEleve().getFormation().getMefStat().getMefstat4();

        return mefStats4Securises.contains(mefStatResultat);
    }
}
