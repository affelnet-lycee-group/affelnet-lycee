/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fr.edu.academie.affelnet.domain.Datable;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.affectation.DecisionCommission;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;

/** Demande pour un �l�ve (voeu d'�l�ve). */
public class VoeuEleve extends Datable implements Serializable, Comparable<VoeuEleve> {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Valeur maximale pour le num�ro de d�partage entre les voeux des �l�ves de bar�me identique. */
    private static final int VALEUR_DEPARTAGE_MAXIMALE = 999999999;

    /**
     * Objet permettant la g�n�ration de nombre al�atoires.
     * Utilis� pour les valeurs de d�partage des voeux de bar�mes identiques.
     */
    private static final Random RANDOM = new Random();

    /** l'identifiant. */
    private VoeuElevePK id;

    /** l'eleve. */
    private Eleve eleve;

    /** La candidature associ�e au voeu. */
    private Candidature candidature;

    /** le voeu. */
    private Voeu voeu;

    /** Langue vivante 1 saisie par l'�l�ve. */
    private Matiere lv1;

    /** Langue vivante 2 saisie par l'�l�ve. */
    private Matiere lv2;

    /**
     * Code la d�cision prise en commission (anc. "provisoire").
     *
     * La valeur par d�faut est :
     * <ul>
     * <li>un param�tre acad�mique pour le statut scolaire</li>
     * <li>{@link DecisionCommission#EN_ATTENTE_SIGNATURE_CONTRAT} pour le statut apprentissage</li>
     * </ul>
     */
    private String codeDecisionProvisoire;

    /** Rang provisoire sur la LS (commission). */
    private Double numeroListeSuppProvisoire;

    /** Code d�cision finale sur le voeu. */
    private Integer codeDecisionFinal;

    /** D�cision finale sur le voeu. */
    @Transient
    private DecisionFinale decisionFinale;

    /** Rang sur la LS sur le voeu. */
    private Integer numeroListeSuppFinal;

    /** Code d�cision � l'issue du reclassement (pour m�moire en cas de for�age). */
    private Integer codeDecisionPostReclassement;

    /** Rang sur LS � l'issue du reclassement (pour m�moire en cas de for�age). */
    private Integer numeroListeSuppPostReclassement;

    /** Avis de gestion. */
    private Avis avisEntretien;

    /** Avis du chef d'�tablissement d'origine. */
    private Avis avisConseilClasse;

    /** Avis DSDEN. */
    private Avis avisDSDEN;

    /** Avis passerelle. */
    private Avis avisPasserelle;

    /** D�rogations pour ce voeu �l�ve. */
    private Map<Integer, DerogationVoeuEleve> derogationsVoeuEleve = new TreeMap<>();

    /**
     * Si "O", le voeu est refus� individuellement Le flag n'est pas positionn�
     * � "O" si tous les voeux de l'eleve sont refus�s Ce flag n'est donc � "O"
     * si et seulement si le voeu est refus� individuellement.
     */
    private String flagRefusVoeu = Flag.NON; // valeur par d�faut

    /** Voeu d�rogatoire ? Oui / Non. */
    private String flagVoeuDerogation = Flag.NON;

    /**
     * Validation par l'administration de la d�rogation "Parcours scolaire particulier".
     */
    private String flagValidationParcoursScolaireParticulier;

    /** Le voeu est-il une proposition d'accueil ? */
    private String flagPropositionAccueil = Flag.NON;

    /** Indicateur de dossier de candidature en internat demand�. */
    private String flagInternatDemande = Flag.NON;

    /** Les r�sultats provisoires de l'OPA. */
    private ResultatsProvisoiresOpa resultatsProvisoiresOpa;

    /** La valeur g�n�r�e pour le voeu et utiliser pour d�partager des voeux avec un bar�me �gal. */
    private int valeurDepartage;

    /**
     * @return the decision finale enum from codeDecisionFinal
     */
    public DecisionFinale getDecisionFinale() {
        return DecisionFinale.getPourCode(codeDecisionFinal);
    }

    /**
     * M�thode de g�n�ration d'une valeur de d�partage pour un voeu d'�l�ve.
     * 
     * @return la valeur g�n�r�e
     */
    public static int genererValeurDepartage() {
        return RANDOM.nextInt(VALEUR_DEPARTAGE_MAXIMALE);
    }

    /**
     * @return Returns the eleve.
     */
    public Eleve getEleve() {
        return eleve;
    }

    /**
     * @param eleve
     *            The eleve to set.
     */
    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    /**
     * @return the candidature
     */
    public Candidature getCandidature() {
        return candidature;
    }

    /**
     * @param candidature
     *            the candidature to set
     */
    public void setCandidature(Candidature candidature) {
        this.candidature = candidature;
    }

    /**
     * @return Returns the avisConseilClasse.
     */
    public Avis getAvisConseilClasse() {
        return avisConseilClasse;
    }

    /**
     * @param avisConseilClasse
     *            The avisConseilClasse to set.
     */
    public void setAvisConseilClasse(Avis avisConseilClasse) {
        this.avisConseilClasse = avisConseilClasse;
    }

    /**
     * @return Returns the avisEntretien.
     */
    public Avis getAvisEntretien() {
        return avisEntretien;
    }

    /**
     * @param avisEntretien
     *            The avisEntretien to set.
     */
    public void setAvisEntretien(Avis avisEntretien) {
        this.avisEntretien = avisEntretien;
    }

    /**
     * @return the avisDSDEN
     */
    public Avis getAvisDSDEN() {
        return avisDSDEN;
    }

    /**
     * @param avisDSDEN
     *            the avisDSDEN to set
     */
    public void setAvisDSDEN(Avis avisDSDEN) {
        this.avisDSDEN = avisDSDEN;
    }

    /**
     * @return the avisPasserelle
     */
    public Avis getAvisPasserelle() {
        return avisPasserelle;
    }

    /**
     * @param avisPasserelle
     *            the avisPasserelle to set
     */
    public void setAvisPasserelle(Avis avisPasserelle) {
        this.avisPasserelle = avisPasserelle;
    }

    /**
     * @return Returns the codeDecisionPostReclassement.
     */
    public Integer getCodeDecisionPostReclassement() {
        return codeDecisionPostReclassement;
    }

    /**
     * @param codeDecisionPostReclassement
     *            The codeDecisionPostReclassement to set.
     */
    public void setCodeDecisionPostReclassement(Integer codeDecisionPostReclassement) {
        this.codeDecisionPostReclassement = codeDecisionPostReclassement;
    }

    /**
     * @return Returns the codeDecisionFinal.
     */
    public Integer getCodeDecisionFinal() {
        return codeDecisionFinal;
    }

    /**
     * @param codeDecisionFinal
     *            The codeDecisionFinal to set.
     */
    public void setCodeDecisionFinal(Integer codeDecisionFinal) {
        this.codeDecisionFinal = codeDecisionFinal;
    }

    /**
     * Code la d�cision prise en commission (anc. "provisoire").
     *
     * La valeur par d�faut est :
     * <ul>
     * <li>un param�tre acad�mique pour le statut scolaire</li>
     * <li>et {@link DecisionCommission#EN_ATTENTE_SIGNATURE_CONTRAT} pour le statut apprentissage</li>
     * </ul>
     * 
     * @return le code de la d�cision prise en commission
     */
    public String getCodeDecisionProvisoire() {
        return codeDecisionProvisoire;
    }

    /**
     * Code la d�cision prise en commission (anc. "provisoire").
     *
     * La valeur par d�faut est :
     * <ul>
     * <li>un param�tre acad�mique pour le statut scolaire</li>
     * <li>et {@link DecisionCommission#EN_ATTENTE_SIGNATURE_CONTRAT} pour le statut apprentissage</li>
     * </ul>
     * 
     * @param codeDecisionProvisoire
     *            le code de la d�cision prise en commission
     */
    public void setCodeDecisionProvisoire(String codeDecisionProvisoire) {
        this.codeDecisionProvisoire = codeDecisionProvisoire;
    }

    /**
     * @return Returns the lv1.
     */
    public Matiere getLv1() {
        return lv1;
    }

    /**
     * @param lv1
     *            The lv1 to set.
     */
    public void setLv1(Matiere lv1) {
        this.lv1 = lv1;
    }

    /**
     * @return Returns the lv2.
     */
    public Matiere getLv2() {
        return lv2;
    }

    /**
     * @param lv2
     *            The lv2 to set.
     */
    public void setLv2(Matiere lv2) {
        this.lv2 = lv2;
    }

    /**
     * @return Returns the numeroListeSuppPostReclassement.
     */
    public Integer getNumeroListeSuppPostReclassement() {
        return numeroListeSuppPostReclassement;
    }

    /**
     * @param numeroListeSuppPostReclassement
     *            The numeroListeSuppPostReclassement to set.
     */
    public void setNumeroListeSuppPostReclassement(Integer numeroListeSuppPostReclassement) {
        this.numeroListeSuppPostReclassement = numeroListeSuppPostReclassement;
    }

    /**
     * @return Returns the numeroListeSuppFinal.
     */
    public Integer getNumeroListeSuppFinal() {
        return numeroListeSuppFinal;
    }

    /**
     * @param numeroListeSuppFinal
     *            The numeroListeSuppFinal to set.
     */
    public void setNumeroListeSuppFinal(Integer numeroListeSuppFinal) {
        this.numeroListeSuppFinal = numeroListeSuppFinal;
    }

    /**
     * @return Returns the numeroListeSuppProvisoire.
     */
    public Double getNumeroListeSuppProvisoire() {
        return numeroListeSuppProvisoire;
    }

    /**
     * @param numeroListeSuppProvisoire
     *            The numeroListeSuppProvisoire to set.
     */
    public void setNumeroListeSuppProvisoire(Double numeroListeSuppProvisoire) {
        this.numeroListeSuppProvisoire = numeroListeSuppProvisoire;
    }

    /**
     * @return Returns the voeu.
     */
    public Voeu getVoeu() {
        return voeu;
    }

    /**
     * @param voeu
     *            The voeu to set.
     */
    public void setVoeu(Voeu voeu) {
        this.voeu = voeu;
    }

    /**
     * @return Returns the id.
     */
    public VoeuElevePK getId() {
        return id;
    }

    /**
     * @param id
     *            The id to set.
     */
    public void setId(VoeuElevePK id) {
        this.id = id;
    }

    /**
     * @return the valeurDepartage
     */
    public int getValeurDepartage() {
        return valeurDepartage;
    }

    /**
     * @param valeurDepartage
     *            the valeurDepartage to set
     */
    public void setValeurDepartage(int valeurDepartage) {
        this.valeurDepartage = valeurDepartage;
    }

    @Override
    public String toString() {
        // Affichage de l'�l�ve
        Eleve elv = getEleve();
        String chaineVoeuEleve = elv.getIne() + " - " + elv.getNom() + " " + elv.getPrenom();
        if (StringUtils.isNotEmpty(elv.getPrenom2())) {
            chaineVoeuEleve += " " + elv.getPrenom2();
        }
        if (StringUtils.isNotEmpty(elv.getPrenom3())) {
            chaineVoeuEleve += " " + elv.getPrenom3();
        }

        // Affichage du voeu
        chaineVoeuEleve += " / Voeu de rang n�" + getId().getRang().toString() + " [" + getVoeu().getCode() + "]";
        return chaineVoeuEleve;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof VoeuEleve)) {
            return false;
        }
        VoeuEleve castOther = (VoeuEleve) other;
        return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    /**
     * @return the flagRefusVoeu
     */
    public String getFlagRefusVoeu() {
        return flagRefusVoeu;
    }

    /**
     * @param flagRefusVoeu
     *            the flagRefusVoeu to set
     */
    public void setFlagRefusVoeu(String flagRefusVoeu) {
        this.flagRefusVoeu = flagRefusVoeu;
    }

    /**
     * @return the flagVoeuDerogation
     */
    public String getFlagVoeuDerogation() {
        return flagVoeuDerogation;
    }

    /**
     * @param flagVoeuDerogation
     *            the flagVoeuDerogation to set
     */
    public void setFlagVoeuDerogation(String flagVoeuDerogation) {
        this.flagVoeuDerogation = flagVoeuDerogation;
    }

    /**
     * @return the flagValidationParcoursScolaireParticulier
     */
    public String getFlagValidationParcoursScolaireParticulier() {
        return flagValidationParcoursScolaireParticulier;
    }

    /**
     * @param flagValidationParcoursScolaireParticulier
     *            the flagValidationParcoursScolaireParticulier to set
     */
    public void setFlagValidationParcoursScolaireParticulier(String flagValidationParcoursScolaireParticulier) {
        this.flagValidationParcoursScolaireParticulier = flagValidationParcoursScolaireParticulier;
    }

    /**
     * @return the flagPropositionAccueil
     */
    public String getFlagPropositionAccueil() {
        return flagPropositionAccueil;
    }

    /**
     * @param flagPropositionAccueil
     *            the flagPropositionAccueil to set
     */
    public void setFlagPropositionAccueil(String flagPropositionAccueil) {
        this.flagPropositionAccueil = flagPropositionAccueil;
    }

    /**
     * @return the derogationsVoeuEleve
     */
    public Map<Integer, DerogationVoeuEleve> getDerogationsVoeuEleve() {
        return this.derogationsVoeuEleve;
    }

    /**
     * @param derogationsVoeuEleve
     *            the derogationsVoeuEleve to set
     */
    public void setDerogationsVoeuEleve(Map<Integer, DerogationVoeuEleve> derogationsVoeuEleve) {
        this.derogationsVoeuEleve = derogationsVoeuEleve;
    }

    /**
     * @return the flagInternatDemande
     */
    public String getFlagInternatDemande() {
        return flagInternatDemande;
    }

    /**
     * @param flagInternatDemande
     *            the flagInternatDemande to set
     */
    public void setFlagInternatDemande(String flagInternatDemande) {
        this.flagInternatDemande = flagInternatDemande;
    }

    /**
     * @return the resultatsProvisoiresOpa
     */
    public ResultatsProvisoiresOpa getResultatsProvisoiresOpa() {
        return resultatsProvisoiresOpa;
    }

    /**
     * @param resultatsProvisoiresOpa
     *            the resultatsProvisoiresOpa to set
     */
    public void setResultatsProvisoiresOpa(ResultatsProvisoiresOpa resultatsProvisoiresOpa) {
        this.resultatsProvisoiresOpa = resultatsProvisoiresOpa;
    }

    /**
     * M�thode de r�cup�ration d'un avis du voeu.
     * 
     * @param typeAvis
     *            le type d'avis.
     * @return l'avis correspondant au type pr�cis�
     */
    public Avis getAvis(CodeTypeAvis typeAvis) {
        switch (typeAvis) {
            case AVIS_DSDEN:
                return getAvisDSDEN();
            case AVIS_DE_GESTION:
                return getAvisEntretien();
            case AVIS_DU_CHEF_D_ETABLISSEMENT:
                return getAvisConseilClasse();
            case AVIS_PASSERELLE:
                return getAvisPasserelle();
            default:
                return null;
        }
    }

    @Override
    public int compareTo(VoeuEleve voeuEleve) {
        return this.getId().getRang() - voeuEleve.getId().getRang();
    }

    /**
     * Teste si le voeu est valide vis-�-vis de la r�servation pour les �l�ves venant d'une formation origine de
     * 3�me SEGPA.
     * 
     * @return vrai si le voeu est valide, sinon faux.
     */
    public boolean estValideReservation3Segpa() {
        return !voeu.estSaisieReservee3emeSegpa() || eleve.estFormationOrigine3emeSegpa();
    }
}
