/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain.voeu;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/** Cl� primaire pour le voeu d'un �l�ve. */
public class VoeuElevePK implements Serializable {

    /** Tag de s�rialisation. */
    private static final long serialVersionUID = 1L;

    /** Le num�ro du tour. */
    private Short numeroTour;

    /** l'identifiant national �l�ve. */
    private String ine;

    /** le rang du voeu. */
    private Integer rang;

    /** default constructor. */
    public VoeuElevePK() {
    }

    /**
     * Le constructeur avec param�tres.
     * 
     * @param numeroTour
     *            le num�ro du tour
     * @param ine
     *            l'identifiant national �l�ve
     * @param rang
     *            le rang du voeu
     */
    public VoeuElevePK(Short numeroTour, String ine, Integer rang) {
        this.numeroTour = numeroTour;
        this.ine = ine;
        this.rang = rang;
    }

    /**
     * @return the numeroTour
     */
    public Short getNumeroTour() {
        return numeroTour;
    }

    /**
     * @param numeroTour
     *            the numeroTour to set
     */
    public void setNumeroTour(Short numeroTour) {
        this.numeroTour = numeroTour;
    }

    /**
     * @return Returns the ine.
     */
    public String getIne() {
        return ine;
    }

    /**
     * @param ine
     *            The ine to set.
     */
    public void setIne(String ine) {
        this.ine = ine;
    }

    /**
     * @return Returns the rang.
     */
    public Integer getRang() {
        return rang;
    }

    /**
     * @param rang
     *            The rang to set.
     */
    public void setRang(Integer rang) {
        this.rang = rang;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("num�ro de tour", getNumeroTour()).append("ine", getIne())
                .append("rang", getRang()).toString();
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if (!(other instanceof VoeuElevePK)) {
            return false;
        }
        VoeuElevePK castOther = (VoeuElevePK) other;
        return new EqualsBuilder().append(this.getNumeroTour(), castOther.getNumeroTour())
                .append(this.getIne(), castOther.getIne()).append(this.getRang(), castOther.getRang()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getNumeroTour()).append(getIne()).append(getRang()).toHashCode();
    }

    /**
     * Donne une repr�sentation de l'identifiant de voeu.
     * 
     * @return description de l'identifiant de voeu
     */
    public String toPrettyString() {
        return "(Num�ro de Tour : " + numeroTour + ", Voeu ine : " + ine + ", rang : " + rang + ")";
    }
}
