/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.voeu.CandidatureDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.CandidaturePK;
import fr.edu.academie.affelnet.domain.voeu.Eleve;

/**
 * Gestionnaire des candidatures.
 */
@Service
public class CandidatureManager extends AbstractManager {

    /**
     * Le manager utilis� pour les campagnes.
     */
    private CampagneAffectationManager campagneManager;

    /**
     * Le manager utilis� pour les param�tres.
     */
    private ParametreManager parametreManager;

    /**
     * Le Dao pour les candidatures.
     */
    private CandidatureDao candidatureDao;

    /**
     * @param campagneManager
     *            the campagneManager to set
     */
    @Autowired
    public void setCampagneManager(CampagneAffectationManager campagneManager) {
        this.campagneManager = campagneManager;
    }

    /**
     * @param candidatureDao
     *            the candidatureDao to set
     */
    @Autowired
    public void setCandidatureDao(CandidatureDao candidatureDao) {
        this.candidatureDao = candidatureDao;
    }

    /**
     *
     * @param parametreManager
     *            the parametreManager to set
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * M�thode de cr�ation d'une candidature pour un �l�ve dans le tour courant.
     * 
     * @param eleve
     *            l'�l�ve concern� par la candidature
     * @return la candidature cr��e
     */
    public Candidature creer(Eleve eleve) {
        short numeroTour = campagneManager.getNumeroTourCourant();
        return candidatureDao.creer(nouvelleCandidature(eleve, numeroTour));
    }

    /**
     * M�thode d'initialisation d'une nouvelle candidature pour un �l�ve dans le tour voulu.
     * 
     * @param eleve
     *            l'�l�ve porteur de la nouvelle candidature
     * @param numeroTour
     *            le tour voulu
     * @return la candidature cr��e
     */
    public Candidature nouvelleCandidature(Eleve eleve, short numeroTour) {
        Candidature candidature = new Candidature(new CandidaturePK(numeroTour, eleve.getIne()));
        candidature.setFlagConformeDo(initFlagConformeDo(eleve));
        eleve.getCandidatures().put(numeroTour, candidature);
        candidature.setEleve(eleve);
        return candidature;
    }

    /**
     * M�thode de modification d'une candidature.
     * 
     * @param candidature
     *            la candidature � modifier
     */
    public void modifier(Candidature candidature) {
        candidature.setDateMAJ(new Date());
        candidature.setHorodatage(candidature.getDateMAJ());
        candidatureDao.maj(candidature, candidature.getId());
    }

    /**
     * M�thode de suppression d'une candidature.
     * 
     * @param candidature
     *            la candidature � supprimer
     */
    public void supprimer(Candidature candidature) {
        candidatureDao.supprimer(candidature);
    }

    /**
     * M�thode d'initialisation du flag de conformit� des voeux avec les d�cisions d'orientation
     * sur la candidature de l'�l�ve lors de sa cr�ation / modification.
     * 
     * @param eleve
     *            l'�l�ve porteur de la candidature
     * @return la valeur du flag de conformit�
     */
    private String initFlagConformeDo(Eleve eleve) {
        String flagConformeDo = Flag.OUI;
        if (eleve != null && eleve.getPalierOrigine() != null) {
            short palierEleve = eleve.getPalierOrigine().getCode().shortValue();
            if (Palier.CODE_PALIER_3EME.shortValue() == palierEleve) {
                flagConformeDo = Flag.NON;
            }
        }
        return flagConformeDo;
    }

    /**
     * Obtenir la candidature de l'�l�ve par son ine et pour un tour donn�.
     *
     * @param ine
     *            l'ine de l'�l�ve
     * @return la candidature de l'�l�ve
     */
    public Candidature getCandidatureEleveDernierTourEnreg(String ine) {
        String numeroDernierTourEnregistre = parametreManager.getNumeroDernierTourEnregistre();

        Short numeroTour = StringUtils.isNotBlank(numeroDernierTourEnregistre)
                ? Short.valueOf(numeroDernierTourEnregistre)
                : Short.valueOf("0");

        return candidatureDao.getCandidatureEleve(ine, numeroTour);
    }

    /**
     * Mise � jour des flags flagValidationAvis, flagSaisieValide, flagConformeDo sur la candidature de l'�l�ve.
     * Mse � jour �galement de la date de mise � jour de la candidature.
     * Utilis� dans les traitements de masse (batch d'int�gration finale).
     *
     * @param candidature
     *            Candidature
     * @return nombre d'enregistrements mis � jour
     */
    public int majFlagsCandidature(Candidature candidature) {
        return candidatureDao.majFlagsCandidature(candidature);
    }

    /**
     * R�cup�re les candidatures du tour des �l�ves en zone g�ographique unique en fonction du tour.
     *
     * @param numeroTour
     *            Num�ro du tour.
     * @return Les candidatures du tour des �l�ves en zone g�ographique unique en fonction du tour.
     */
    public List<Candidature> getCandidaturesZoneGeoUnique(short numeroTour) {
        return candidatureDao.getCandidaturesZoneGeoUnique(numeroTour);
    }
}
