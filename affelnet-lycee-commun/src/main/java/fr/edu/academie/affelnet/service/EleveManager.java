/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.utils.CollecteurScrollableResultSet;
import fr.edu.academie.affelnet.dao.hibernate.utils.DaoUtils;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.parametrage.ParametreDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.domain.EnvoiModifResponsable;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;
import fr.edu.academie.affelnet.domain.affectation.VoeuEleveDecisionFinaleComparateur;
import fr.edu.academie.affelnet.domain.carteScolaire.TronconCarteScolaire;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.AvisParFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.DecisionOrientation;
import fr.edu.academie.affelnet.domain.nomenclature.DecisionOrientation.TypeDecisionOrientation;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.nomenclature.TypeEtablissement;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.Voie;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.Departement;
import fr.edu.academie.affelnet.domain.orientation.DecisionOrientationRecueCouplePalierCode;
import fr.edu.academie.affelnet.domain.orientation.TypeReceptionDO;
import fr.edu.academie.affelnet.domain.parametrage.TypeTour;
import fr.edu.academie.affelnet.domain.voeu.Adresse;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.ComparatorCovoi;
import fr.edu.academie.affelnet.domain.voeu.DecisionOrientationEleve;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.EtatSaisieEleve;
import fr.edu.academie.affelnet.domain.voeu.Responsable;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;
import fr.edu.academie.affelnet.domain.voeu.VoeuTeleservice;
import fr.edu.academie.affelnet.dto.voeu.suivicandidature.CriteresNotifiantsDetectesDto;
import fr.edu.academie.affelnet.dto.voeu.suivicandidature.EleveSuiviCandidatureDto;
import fr.edu.academie.affelnet.dto.voeu.suivicandidature.VoeuSuiviCandidatureDto;
import fr.edu.academie.affelnet.enumeration.CritereNotifiantEnum;
import fr.edu.academie.affelnet.enumeration.StatutZoneGeoEnum;
import fr.edu.academie.affelnet.helper.EleveHelper;
import fr.edu.academie.affelnet.helper.EtablissementHelper;
import fr.edu.academie.affelnet.mapper.voeu.suivicandidature.EleveSuiviCandidatureMapper;
import fr.edu.academie.affelnet.service.nomenclature.AvisManager;
import fr.edu.academie.affelnet.service.nomenclature.ParametresFormationAccueilManager;
import fr.edu.academie.affelnet.service.nomenclature.TypeAvisManager;
import fr.edu.academie.affelnet.service.orientation.ReceptionDOManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.service.voeu.VoeuTeleserviceManager;
import fr.edu.academie.affelnet.utils.ApplicationMessage;
import fr.edu.academie.affelnet.utils.ChaineUtils;
import fr.edu.academie.affelnet.utils.Collecteur;
import fr.edu.academie.affelnet.utils.DateUtils;
import fr.edu.academie.affelnet.utils.exceptions.BusinessException;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/** Classe permettant de g�rer les �l�ves. */
@Service
public class EleveManager extends AbstractManager {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(EleveManager.class);

    /**
     * Champs que l'on peut demander � Hibernate de charger en m�me temps que
     * l'�l�ve.
     */
    public enum ChampsEnum {
        /**
         * Champs li�s aux v�ux de l'�l�ve.
         */
        INFO_VOEUX,
        /**
         * Champs li�s aux responsables de l'�l�ve et adresses associ�es.
         */
        RESPONSABLES_ADRESSES,
        /**
         * Combinaison des deux cat�gories pr�c�dentes.
         */
        ALL
    }

    /**
     * Le dao utilis� pour les �l�ves.
     */
    private EleveDao eleveDao;

    /**
     * Le dao utilis� pour les param�tres.
     */
    private ParametreDao parametreDao;

    /**
     * Le dao utilis� pour les voeux des �l�ves.
     */
    private VoeuEleveDao voeuEleveDao;

    /**
     * Le manager utilis� pour les param�tres.
     */
    private ParametreManager parametreManager;

    /**
     * Le manager utilis� pour le journal.
     */
    private JournalManager journalManager;

    /**
     * Le manager utilis� pour les avis.
     */
    private AvisManager avisManager;

    /**
     * Le manager utilis� pour les param�tres par formation d'accueil.
     */
    private ParametresFormationAccueilManager parametresFormationAccueilManager;

    /**
     * Le manager utilis� pour les voeux des �l�ves.
     */
    private VoeuEleveManager voeuEleveManager;

    /**
     * Le manager utilis� pour les �valuations et les notes.
     */
    private EvalNoteManager evalNoteManager;

    /**
     * Le manager utilis� pour les notes.
     */
    private NotesManager notesManager;

    /**
     * Le manager pour les campagnes d'affectation.
     */
    private CampagneAffectationManager campagneManager;

    /**
     * Le manager pour les OPA.
     */
    private OperationProgrammeeAffectationManager opaManager;

    /**
     * Le manager des candidatures.
     */
    private CandidatureManager candidatureManager;

    /**
     * Le manager qui sauvegarde la r�ception des d�cisions d'orientation.
     */
    private ReceptionDOManager receptionDOManager;

    /** Manager des voeux issus du t�l�service affectation. */
    private VoeuTeleserviceManager voeuTeleserviceManager;

    /** Manager des notifications envoy�es ou � envoyer � des responsables. */
    private EnvoiModifResponsableManager envoiModifResponsableManager;

    /** Manager du pojo CarteScolaireAccueil. */
    private CarteScolaireManager carteScolaireManager;

    /**
     * Le Mapper de la class EleveSuiviCandidatureDto.
     */
    private EleveSuiviCandidatureMapper eleveSuiviCandidatureMapper;

    /**
     * Sp�cifie le manager de la carte scolaire.
     *
     * @param carteScolaireManager
     *            Manager de la carte scolaire.
     */
    @Autowired
    public void setCarteScolaireManager(CarteScolaireManager carteScolaireManager) {
        this.carteScolaireManager = carteScolaireManager;
    }

    /**
     * Sp�cifie le mapper de la class EleveSuiviCandidatureDto.
     *
     * @param eleveSuiviCandidatureMapper
     *            Le mapper EleveSuiviCandidatureMapper.
     */
    @Autowired
    public void setEleveSuiviCandidatureMapper(EleveSuiviCandidatureMapper eleveSuiviCandidatureMapper) {
        this.eleveSuiviCandidatureMapper = eleveSuiviCandidatureMapper;
    }

    /**
     * Sp�cifie le manager des notifications envoy�es ou � envoyer � des responsables.
     *
     * @param envoiModifResponsableManager
     *            Le Manager des notifications envoy�es ou � envoyer � des responsables.
     */
    @Autowired
    public void setEnvoiModifResponsableManager(EnvoiModifResponsableManager envoiModifResponsableManager) {
        this.envoiModifResponsableManager = envoiModifResponsableManager;
    }

    /**
     * @param eleveDao
     *            the eleveDao to set
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * @param parametreDao
     *            the parametreDao to set
     */
    @Autowired
    public void setParametreDao(ParametreDao parametreDao) {
        this.parametreDao = parametreDao;
    }

    /**
     * @param voeuEleveDao
     *            the voeuEleveDao to set
     */
    @Autowired
    public void setVoeuEleveDao(VoeuEleveDao voeuEleveDao) {
        this.voeuEleveDao = voeuEleveDao;
    }

    /**
     * @param parametreManager
     *            the parametreManager to set
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param journalManager
     *            the journalManager to set
     */
    @Autowired
    public void setJournalManager(JournalManager journalManager) {
        this.journalManager = journalManager;
    }

    /**
     * @param avisManager
     *            the avisManager to set
     */
    @Autowired
    public void setAvisManager(AvisManager avisManager) {
        this.avisManager = avisManager;
    }

    /**
     * @param parametresFormationAccueilManager
     *            the parametresFormationAccueilManager to set
     */
    @Autowired
    public void setParametresFormationAccueilManager(
            ParametresFormationAccueilManager parametresFormationAccueilManager) {
        this.parametresFormationAccueilManager = parametresFormationAccueilManager;
    }

    /**
     * @param voeuEleveManager
     *            the voeuEleveManager to set
     */
    @Autowired
    public void setVoeuEleveManager(VoeuEleveManager voeuEleveManager) {
        this.voeuEleveManager = voeuEleveManager;
    }

    /**
     * @param notesManager
     *            the notesManager to set
     */
    @Autowired
    public void setNotesManager(NotesManager notesManager) {
        this.notesManager = notesManager;
    }

    /**
     * @param campagneAffectationManager
     *            the campagneAffectationManager to set
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneManager = campagneAffectationManager;
    }

    /**
     * @param opaManager
     *            the opaManager to set
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * @param campagneManager
     *            the campagneManager to set
     */
    @Autowired
    public void setCampagneManager(CampagneAffectationManager campagneManager) {
        this.campagneManager = campagneManager;
    }

    /**
     * @param candidatureManager
     *            the candidatureManager to set
     */
    @Autowired
    public void setCandidatureManager(CandidatureManager candidatureManager) {
        this.candidatureManager = candidatureManager;
    }

    /**
     * @param evalNoteManager
     *            the evalNoteManager to set
     */
    @Autowired
    public void setEvalNoteManager(EvalNoteManager evalNoteManager) {
        this.evalNoteManager = evalNoteManager;
    }

    /**
     * @param receptionDOManager
     *            the receptionDOManager to set
     */
    @Autowired
    public void setReceptionDOManager(ReceptionDOManager receptionDOManager) {
        this.receptionDOManager = receptionDOManager;
    }

    /**
     * Sp�cifie le Manager des voeux issus du t�l�service affectation.
     *
     * @param voeuTeleserviceManager
     *            Manager des voeux issus du t�l�service affectation.
     */
    @Autowired
    public void setVoeuTeleserviceManager(VoeuTeleserviceManager voeuTeleserviceManager) {
        this.voeuTeleserviceManager = voeuTeleserviceManager;
    }

    /**
     * R�cup�re les informations de l'�l�ve � partir de son INE.
     *
     * @param ine
     *            INE de l'�l�ve � charger
     * @return �l�ve correspondant � l'INE demand�
     */
    public Eleve chargerParIne(String ine) {
        return eleveDao.charger(ine);
    }

    /**
     * R�cup�re les informations de l'�l�ve � partir de son INE, avec des champs
     * suppl�mentaires.
     *
     * @param ine
     *            INE de l'�l�ve � charger
     * @param champs
     *            Champs � ajouter
     * @return �l�ve correspondant � l'INE demand�
     */
    public Eleve chargerParIneAvecChamps(String ine, ChampsEnum champs) {
        HashSet<String> fetchJoin = new HashSet<>();
        if (ChampsEnum.INFO_VOEUX.equals(champs) || ChampsEnum.ALL.equals(champs)) {
            fetchJoin.add("etablissement");
            fetchJoin.add("voeuxTeleservice");
            fetchJoin.add("candidatures.voeux.voeu.etablissement");
            fetchJoin.add("candidatures.voeux.voeu.etablissement.typeEtablissement");
            fetchJoin.add("candidatures.voeux.voeu.formation");
            fetchJoin.add("candidatures.voeux.voeu.voie");
            fetchJoin.add("candidatures.voeux.avisConseilClasse");
            fetchJoin.add("candidatures.voeux.avisEntretien");
            fetchJoin.add("candidatures.voeux.lv1");
            fetchJoin.add("candidatures.voeux.lv2");
            fetchJoin.add("candidatures.voeux.derogationsVoeuEleve");
            fetchJoin.add("candidatures.voeux.flagVoeuDerogation");
            fetchJoin.add("zoneGeo");
            fetchJoin.add("zoneGeo.liensZoneGeo");
        }
        if (ChampsEnum.RESPONSABLES_ADRESSES.equals(champs) || ChampsEnum.ALL.equals(champs)) {
            fetchJoin.add("adresseResidence.codePostal.commune");
            fetchJoin.add("adresseResidence.pays");
            fetchJoin.add("responsables.adresseResidence.codePostal.commune");
            fetchJoin.add("responsables.adresseResidence.pays");
            fetchJoin.add("responsables.lienEleveResponsable");
            fetchJoin.add("responsables.niveauResponsabilite");
            fetchJoin.add("responsables.categorieSocioProfessionnelle");
        }

        return eleveDao.charger(ine, fetchJoin);
    }

    /**
     * Charge un �l�ve avec les informations associ�es � ses voeux.
     *
     * @param ine
     *            INE de l'�l�ve
     * @return �l�ve
     */
    public Eleve chargerEleveParIneAvecInfosVoeux(String ine) {
        return this.chargerParIneAvecChamps(ine, ChampsEnum.INFO_VOEUX);
    }

    /**
     * Charge un �l�ve avec les informations du palier.
     *
     * @param ine
     *            INE de l'�l�ve
     * @return �l�ve
     */
    public Eleve chargerEleveParInePourEval(String ine) {

        HashSet<String> fetchJoin = new HashSet<>();
        fetchJoin.add("etablissement");
        fetchJoin.add("palier");
        fetchJoin.add("evaluationsDiscipline");
        fetchJoin.add("evaluationsSocle");
        fetchJoin.add("evaluationsComplementaires");

        Eleve eleve = null;
        try {
            eleve = eleveDao.charger(ine, fetchJoin);
        } catch (DaoException e) {
            LOG.info("L'�l�ve " + ine + " n'est pas pr�sent dans la base de donn�es.");
        }

        return eleve;
    }

    /**
     * M�thode de r�cup�ration des eleves selon certains crit�res.
     *
     * @param filtres
     *            les crit�res de s�lection des Eleves
     * @return la liste des Eleves s�lectionn�s
     */
    public List<Eleve> listerEleves(List<Filtre> filtres) {
        return eleveDao.lister(filtres);
    }

    /**
     * Retourne la liste des �l�ves sans adresse ou dont l'adresse est invalide.
     *
     * @param filtres
     *            les filtres � appliquer sur la liste des �l�ves
     * @return la liste des �l�ves concern�s
     */
    public List<Eleve> getListeElevesAdresseInvalide(List<Filtre> filtres) {
        List<String> jointuresFetch = new ArrayList<>();
        jointuresFetch.add("adresseResidence");
        jointuresFetch.add("adresseResidence.codePostal");
        jointuresFetch.add("adresseResidence.pays");
        filtres.add(Filtre.hql("(select count(*) from VoeuEleve voeu where voeu.id.ine = o.ine "
                + "and voeu.voeu.flagRecensement='N' " + "and voeu.id.numeroTour = '"
                + campagneManager.getNumeroTourCourant() + "') > 0"));
        filtres.add(filtreElevesNonForces());
        ScrollableResults scroll = eleveDao.scroll(filtres, null, null, null, jointuresFetch, true);
        List<Eleve> result = new ArrayList<>();
        try {
            while (scroll.next()) {
                Eleve eleve = (Eleve) scroll.get(0);
                Adresse adresse = eleve.getAdresseResidence();
                if (null == adresse || !adresse.estValide()) {
                    result.add(eleve);
                }
            }
        } finally {
            scroll.close();
        }
        return result;
    }

    /**
     * Teste s'il existe un �l�ve partir de son INE.
     *
     * @param ine
     *            INE de l'�l�ve � tester
     * @return vrai s'il existe un �l�ve avec cet INE
     */
    public boolean existeParIne(String ine) {
        String upperCasedIne = ine.toUpperCase();
        return eleveDao.existe(upperCasedIne);
    }

    /**
     * Teste s'il existe un �l�ve avec des voeux � partir de son INE.
     *
     * @param ine
     *            INE de l'�l�ve � tester
     * @return vrai s'il existe un �l�ve avec cet INE et ayant des voeux
     */
    public boolean existeAvecVoeu(String ine) {
        String upperCasedIne = ine.toUpperCase();
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("ine", upperCasedIne));
        filtres.add(Filtre.superieur("nbVoeu", 0));
        return eleveDao.getNombreElements(filtres) > 0;
    }

    /**
     * @param entiteOrigine
     *            L'entit� d'origine pour la saisie de l'INE provisoire.
     * @return cette fonction g�n�re un identifiant provisoire d'immatriculation
     *         pour un �l�ve compos� comme suit : P + code acad�mie + incr�ment sur
     *         7 + lettre modulo 23
     */
    public String genereIdEleveProvisoire(String entiteOrigine) {
        // IssueId 3192 �largissement des INEs temporaires.
        if (StringUtils.isEmpty(entiteOrigine)) {
            throw new ValidationException("L'entit� d'origine n'a pas �t� pr�cis�e");
        }
        String ine;

        do {
            int increment = parametreDao.getIncrement();
            String num = parametreManager.getCodeAcademie() + new DecimalFormat("0000000").format(increment);
            char modulo = ChaineUtils.lettreAttenduePourModulo23(Integer.parseInt(num));
            ine = entiteOrigine + num + modulo;
            if (!EleveHelper.isIneProvisoire(ine)) {
                throw new ValidationException("L'entit� d'origine n'est pas connue");
            }
        } while (eleveDao.existe(ine));

        return ine;
    }

    /**
     * Calcul de l'�ge de l'�l�ve. Age = ann�e de la future rentr�e scolaire - ann�e
     * de naissance
     *
     * @param dateNaissance
     *            la <code>Date</code> de naissance de l'�l�ve
     * @return age de l'�l�ve
     */
    public Integer calculAge(Date dateNaissance) {
        int anneeNaissance = DateUtils.getYear(dateNaissance);
        int anneeFutureRentree = parametreManager.getAnnee20xx();
        int age = anneeFutureRentree - anneeNaissance;
        return Integer.valueOf(age);
    }

    /**
     * Cette m�thode permet de v�rifier si un INE existe ou non.
     *
     * @param erreurs
     *            Liste des erreurs � compl�ter le cas �ch�ant
     * @param ine
     *            INE � tester
     * @param propertyKey
     *            propri�t� � utiliser en cas d'erreur
     * @return vrai si l'INE est celui d'un �l�ve existant, sinon faux
     */
    public boolean valideIneExiste(List<String> erreurs, String ine, String propertyKey) {
        // existence de l'ine
        if (!eleveDao.existe(ine.toUpperCase())) {
            erreurs.add(ApplicationMessage.get("errors.invalid",
                    new Object[] { ApplicationMessage.get(propertyKey) }));
            return false;
        }
        return true;
    }

    /**
     * Donne la candidature de l'�l�ve pour le tour courant, si elle existe.
     *
     * @param eleve
     *            l'�l�ve
     * @return la candidature pour le tour courant, ou null si elle n'existe pas
     */
    public Candidature getCandidatureCourante(Eleve eleve) {
        short numeroTour = campagneManager.getNumeroTourCourant();
        return eleve.getCandidature(numeroTour);
    }

    /**
     * Cette m�thode supprime un �l�ve et ajoute une entr�e dans le journal des
     * op�rations.
     *
     * @param eleve
     *            l'�l�ve
     * @param uid
     *            le login de l'utilisateur effectuant l'action
     *
     * @see fr.edu.academie.affelnet.service.JournalManager#addForceEleveAbandon(String, String)
     */
    public void supprimer(Eleve eleve, String uid) {

        String ine = eleve.getIne();

        LOG.info("Suppression de l'�l�ve " + ine + " par l'utilisateur " + uid);

        Candidature candidature = eleve.getCandidature(campagneManager.getNumeroTourCourant());
        if (candidature != null) {
            opaManager.obligerRelanceOpa(candidature);
        }

        evalNoteManager.purgerEvalNotesEleveOpa(ine);
        // suppression de envois de mail aux responsables
        envoiModifResponsableManager.supprimerEnvoiModifsResponsable(ine);
        // suppression des voeux de l'�l�ve
        voeuTeleserviceManager.supprimerVoeuxEleve(ine);
        // suppression de l'�l�ve
        eleveDao.supprimer(eleve);

        // enregistrement dans le journal des op�rations
        journalManager.addSupprimerEleve(uid, eleve);
    }

    /**
     * Conserve un �leve marqu� comme doublon.
     *
     * @param eleve
     *            eleve � traiter
     * @param uid
     *            identifiant de l'utilisateur effectuant la modification
     */
    public void conserverEleveMarqueCommeDoublon(Eleve eleve, String uid) {
        // enregistrement dans le journal des op�rations
        journalManager.addConservationDoublon(uid, eleve);

        eleveDao.traiterDoublon(eleve);
    }

    /**
     * Actualise la situation d'un �l�ve.
     *
     * @param uid
     *            identifiant de l'utilisateur demandeur
     * @param eleve
     *            Eleve � traiter
     */
    public void actualiserSituationEleve(String uid, Eleve eleve) {

        if (LOG.isDebugEnabled()) {
            LOG.debug("*** AVANT ACTUALISATION ELEVE ***");
            LOG.debug(expliciteSituationEleve(eleve));
        }

        // Mise � jour des d�cisions d'affectation de l'�l�ve
        ajusterDecisionFinaleEleve(eleve);

        modifier(eleve, eleve.getIne());

        journalManager.addActualisationSituationEleve(uid, eleve);

        if (LOG.isDebugEnabled()) {
            LOG.debug("*** APRES ACTUALISATION ELEVE ***");
            LOG.debug(expliciteSituationEleve(eleve));
        }

    }

    /**
     * Explicite la situation de l'�l�ve fourni.
     *
     * @param eleve
     *            �l�ve concern�
     * @return cha�ne explicitant la situation
     */
    private String expliciteSituationEleve(Eleve eleve) {

        StringBuilder sbSituation = new StringBuilder();

        sbSituation.append("--- Contr�le de la situation de l'�l�ve ---\n");
        sbSituation.append("INE : " + eleve.getIne() + "\n");

        sbSituation.append("Zone g�ographique : ");
        sbSituation.append(ChaineUtils.indiquePresenceValeur(eleve.getZoneGeo()));
        sbSituation.append("\n");

        Double[] notes = eleve.batchGetNotes();
        boolean uneNoteTrouvee = false;
        for (int i = 0; (i < notes.length) && (!uneNoteTrouvee); i++) {
            uneNoteTrouvee = (notes[i] != null);
        }

        sbSituation.append("Notes : ");
        if (uneNoteTrouvee) {
            sbSituation.append("Pr�sent(es)");
        } else {
            sbSituation.append("Absent(es)");
        }
        sbSituation.append("\n");

        Set<DecisionOrientationEleve> decisionsOrientation = eleve.getDecisionsOrientationEleve();
        StringBuilder libelleDecisionOrientation = new StringBuilder();
        if (!decisionsOrientation.isEmpty()) {
            for (DecisionOrientationEleve decisionOrientation : decisionsOrientation) {
                if (libelleDecisionOrientation.length() > 0) {
                    libelleDecisionOrientation.append(", ");
                }
                libelleDecisionOrientation.append(decisionOrientation.getDecisionOrientation().getLibelleLong());
            }
        } else {
            libelleDecisionOrientation.append("Absente(s)");
        }
        sbSituation.append("D�cision(s) d'orientation : " + libelleDecisionOrientation + "\n");

        List<VoeuEleve> voeux = eleve.voeuxDuTour(campagneManager.getNumeroTourCourant());

        boolean isAvisObligatoire;
        String presenceAvis;

        TypeAvisManager typeAvisManager = SpringUtils.getBean(TypeAvisManager.class);

        for (VoeuEleve voeuEleve : voeux) {
            Voeu voeu = voeuEleve.getVoeu();

            sbSituation.append("-- Voeu de rang " + voeuEleve.getId().getRang() + "-- \n");

            String indicateurPam = voeu.getIndicateurPam();
            String libelleIndicateurPam = TypeOffre.mapTypeOffreParIndicateur().get(indicateurPam)
                    .getLibelleCourt();
            sbSituation.append("Indicateur de prise en compte du bar�me : " + libelleIndicateurPam + "\n");

            String voie = voeu.getVoie() != null ? voeu.getVoie().getLibelleLong() : "";
            sbSituation.append("Voie d'orientation : " + voie + "\n");

            // Avis Paramefa
            isAvisObligatoire = voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT,
                    VoeuEleveManager.SAISIE_OBLIGATOIRE, voeuEleve, true);

            presenceAvis = ChaineUtils.indiquePresenceValeur(voeuEleve.getAvisConseilClasse());

            sbSituation.append(
                    typeAvisManager.charger(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT).getLibelleAvecMajuscule()
                            + " - obligatoire : " + isAvisObligatoire + "\n");
            sbSituation.append(
                    typeAvisManager.charger(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT).getLibelleAvecMajuscule()
                            + " - valeur : " + presenceAvis + "\n");

            // Avis Voeu
            isAvisObligatoire = voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_DE_GESTION,
                    VoeuEleveManager.SAISIE_OBLIGATOIRE, voeuEleve, true);

            presenceAvis = ChaineUtils.indiquePresenceValeur(voeuEleve.getAvisEntretien());

            sbSituation.append("Avis " + typeAvisManager.charger(CodeTypeAvis.AVIS_DE_GESTION).getLibelle()
                    + " - obligatoire : " + isAvisObligatoire + "\n");
            sbSituation.append("Avis " + typeAvisManager.charger(CodeTypeAvis.AVIS_DE_GESTION).getLibelle()
                    + " - valeur : " + presenceAvis + "\n");

        }

        // R�cup�ration de la candidature courante
        Candidature candidatureCourante = getCandidatureCourante(eleve);

        sbSituation.append(
                "Validit� des avis en �tablissement : " + candidatureCourante.getFlagValidationAvis() + "\n");
        sbSituation.append(
                "Validit� de la saisie en �tablissement : " + candidatureCourante.getFlagSaisieValide() + "\n");

        return sbSituation.toString();
    }

    /**
     * Cr�e un filtre 'commence par' sur l'�tablissement d'origine ou le d�partement
     * de l'�l�ve.
     *
     * @param prefixeUaiOuDeptMen
     *            d�but du code UAI ou du d�partement MEN
     * @return filtre sur l'�tablissement d'origine
     */
    public Filtre getFiltreStartsWithEtabOuDeptMenOrigine(String prefixeUaiOuDeptMen) {

        Filtre filtreLikeEtabOrigine = getFiltreStartsWithEtabOrigine(prefixeUaiOuDeptMen);

        if (prefixeUaiOuDeptMen.length() <= Departement.CODE_MEN_LENGTH) {
            Filtre filtreLikeDeptOrigine = getFiltreStartsWithDepartementOrigine(prefixeUaiOuDeptMen);

            return Filtre.or(filtreLikeEtabOrigine, filtreLikeDeptOrigine);
        } else {
            return filtreLikeEtabOrigine;
        }
    }

    /**
     * Cr�e un filtre 'commence par' sur l'�tablissement d'origine de l'�l�ve.
     *
     * @param prefixeUaiOuDeptMen
     *            d�but du code UAI
     * @return filtre sur l'�tablissement d'origine
     */
    public Filtre getFiltreStartsWithEtabOrigine(String prefixeUaiOuDeptMen) {
        return Filtre.like("eleve.etablissementNational.id", prefixeUaiOuDeptMen + "%");
    }

    /**
     * Cr�e un filtre 'commence par' sur le d�partement MEN d'origine de l'�l�ve.
     *
     * @param prefixeUaiOuDeptMen
     *            d�but du code UAI
     * @return filtre sur l'�tablissement d'origine
     */
    public Filtre getFiltreStartsWithDepartementOrigine(String prefixeUaiOuDeptMen) {
        return Filtre.like("eleve.departement.codeMen", prefixeUaiOuDeptMen + "%");
    }

    /**
     * Donne un filtre sur les �l�ves concern�s par les propositions d'accueil.
     *
     * @param listeTotale
     *            indique s'il faut prendre tous les �l�ves ou seulement les �l�ves
     *            qui n'ont pas encore de proposition d'accueil
     * @param isTraitementAuto
     *            indique si l'appel provient du batch d'attribution auto. des
     *            propositions d'accueil. Dans ce cas, il faut filtrer sur les
     *            �l�ves n'ayant pas fait de voeu de secteur
     * @return la liste des filtres ciblant les �l�ves concern�s par les
     *         propositions d'accueil
     *
     */
    public List<Filtre> getFiltresPropositionAccueil(boolean listeTotale, boolean isTraitementAuto) {
        List<Filtre> filtres = new ArrayList<>();

        // �l�ves ayant une zone g�o.
        filtres.add(getFiltreExistenceZoneGeo());

        // �l�ves dont la voie d'orientation est 2GT
        filtres.add(getFiltreVoieOrientation(DecisionOrientation.TypeDecisionOrientation.DO_2GT));

        if (!listeTotale) {
            filtres.add(eleveDao.getFiltreElevesSansPropositionAccueil());
        }

        // qui n'ont pas fait QUE des voeux de recensement
        filtres.add(eleveDao.getFiltreExistenceVoeuAutreQueRecencement());

        // �l�ves ayant formul� des voeux
        filtres.add(eleveDao.getFiltreEleveAvecVoeux());

        // �l�ves non forc�s
        filtres.add(filtreElevesNonForces());

        filtres.add(getFiltreNonAffecteEtNonRecencement());

        // s'il s'agit du batch d'attribution auto. des propositions d'accueil
        // on ajoute le filtre sur les �l�ves n'ayant pas fait de voeu de secteur
        if (isTraitementAuto) {
            filtres.add(getFiltreElevesSansVoeuSecteur());
        }

        return filtres;
    }

    /**
     * Filtre les �l�ves n'ayant pas fait de voeu de secteur.
     *
     * @return le filtre sur les �l�ves n'ayant pas fait de voeu de secteur
     */
    public Filtre getFiltreElevesSansVoeuSecteur() {

        StringBuilder sbFiltreHQL = new StringBuilder();
        sbFiltreHQL.append(" not exists (select 1 from LienZoneGeo l");
        sbFiltreHQL.append(" where o.zoneGeo.code=l.codeZoneGeoOrigine");
        sbFiltreHQL.append(" and l.voeu.code in ");
        sbFiltreHQL.append("     (select ve.voeu.code");
        sbFiltreHQL.append("      from VoeuEleve ve");
        sbFiltreHQL.append("      where ve.id.ine=o.ine");
        sbFiltreHQL.append("      and ve.flagPropositionAccueil!='O')");
        sbFiltreHQL.append(" and l.bonus!=0)");

        return Filtre.hql(sbFiltreHQL.toString());
    }

    /**
     * Filtre les �l�ves pour une voie d'orientation.
     *
     * @param typeDO
     *            type de la d�cision d'orientation
     * @return filtre sur la voie
     */
    public Filtre getFiltreVoieOrientation(TypeDecisionOrientation typeDO) {
        return Filtre.hql("o.ine in (select do.id.ine from DecisionOrientationEleve do "
                + "where do.id.codeDecisionOrientation = '" + typeDO.getCode() + "')");
    }

    /**
     * Filtre donnant les �l�ves ayant une zone g�ographique renseign�e.
     *
     * @return filtre sur la zone g�o non nulle
     */
    public Filtre getFiltreExistenceZoneGeo() {
        return Filtre.isNotNull("zoneGeo");
    }

    /**
     * Cr�e un filtre s�lectionnant les �l�ves dont la candidature pour le tour
     * courant a une d�cision qui n'est ni affect�, ni recencement.
     *
     * @return filtre cr��
     */
    public Filtre getFiltreNonAffecteEtNonRecencement() {
        return Filtre.hql("not exists (select 1 from Candidature candidature where candidature.id.ine = o.ine"
                + " and candidature.id.numeroTour = '" + campagneManager.getNumeroTourCourant()
                + "' and (candidature.codeDecisionFinale = '" + DecisionFinale.AFFECTE.getCode()
                + "' or candidature.codeDecisionFinale = '" + DecisionFinale.RECENSEMENT.getCode() + "'))");
    }

    /**
     * Cr�e un filtre s�lectionnant les �l�ves en fonction de leur INE.
     *
     * @param ines
     *            les INEs des �l�ves � r�cup�rer
     * @return
     */
    public List<Filtre> getFiltreInes(Collection<String> ines) {
        // Gestion des filtres
        List<Filtre> filtresEleves = new ArrayList<>();
        if (!ines.isEmpty()) {
            filtresEleves.add(Filtre.in("ine", ines.toArray()));
        } else {
            filtresEleves.add(Filtre.isNull("ine"));
        }
        return filtresEleves;
    }

    /**
     * M�thode r�cup�rant le nombre d'�l�ves ayant une saisie valide.
     *
     * @param idEtab
     *            L'identifiant de l'�tablissement acad�mique.
     * @return Le nombre d'�l�ves ayant une saisie valide.
     */
    public Integer getNbElevesSaisieValide(String idEtab) {

        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("etablissement.id", idEtab));
        filtres.add(filtreElevesSaisieValide());
        filtres.add(filtresElevesDuTour());

        return eleveDao.lister(filtres).size();
    }

    /**
     * M�thode r�cup�rant le nombre d'�l�ves d'un �tablissement.
     *
     * @param idEtab
     *            L'identifiant de l'�tablissement acad�mique.
     * @return Le nombre d'�l�ves d'un �tablissement.
     */
    public Integer getNbElevesParEtab(String idEtab) {

        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("etablissement.id", idEtab));
        filtres.add(filtresElevesDuTour());

        return eleveDao.lister(filtres).size();
    }

    /**
     * Modifie un avis sur un voeu d'�l�ve et met � jour la situation de l'�l�ve.
     *
     * @param uid
     *            identifiant de l'utilisateur effectuant l'action
     * @param ine
     *            ine de l'�l�ve
     * @param rang
     *            rang du voeu de l'�l�ve dont on modifie l'avis
     * @param typeAvis
     *            type de l'avis � modifier
     * @param nouveauCodeAvis
     *            code de l'avis � attribuer (vide ou null pour le retirer)
     */
    public void modifierAvisSurVoeuEleve(String uid, String ine, Integer rang, String typeAvis,
            String nouveauCodeAvis) {

        // Charge l'�l�ve et le voeu � modifier
        Eleve eleveConcerne = chargerParIne(ine);
        VoeuEleve voeuEleveAModifier = eleveConcerne.voeuDuTourEtDeRang(campagneManager.getNumeroTourCourant(),
                rang);

        // D�termine la valeur � attribuer
        Avis nouvelleValeurAvis = null;
        String informationModification = null;

        if (StringUtils.isNotBlank(nouveauCodeAvis)) {
            nouvelleValeurAvis = avisManager.charger(typeAvis, nouveauCodeAvis);
            informationModification = "Attribution d'avis " + nouvelleValeurAvis.getLibelleCourt();
        } else {
            informationModification = "Retrait d'avis ";
        }

        // Charge la valeur actuelle et met � jour en cas de changement
        boolean changementEffectif = false;

        if (typeAvis.equals(CodeTypeAvis.AVIS_DE_GESTION.getCode())) {

            Avis valeurAvisActuelle = voeuEleveAModifier.getAvisEntretien();
            changementEffectif = !Objects.equals(valeurAvisActuelle, nouvelleValeurAvis);

            if (changementEffectif) {
                voeuEleveAModifier.setAvisEntretien(nouvelleValeurAvis);
            }
        } else if (typeAvis.equals(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT.getCode())) {

            Avis valeurAvisActuelle = voeuEleveAModifier.getAvisConseilClasse();
            changementEffectif = !Objects.equals(valeurAvisActuelle, nouvelleValeurAvis);

            if (changementEffectif) {
                voeuEleveAModifier.setAvisConseilClasse(nouvelleValeurAvis);
            }
        } else if (typeAvis.equals(CodeTypeAvis.AVIS_PASSERELLE.getCode())) {

            Avis valeurAvisActuelle = voeuEleveAModifier.getAvisPasserelle();
            changementEffectif = !Objects.equals(valeurAvisActuelle, nouvelleValeurAvis);

            if (changementEffectif) {
                voeuEleveAModifier.setAvisPasserelle(nouvelleValeurAvis);
            }
        } else {
            throw new BusinessException("Le type d'avis " + typeAvis + " n'est pas g�r�.");
        }

        if (changementEffectif) {

            // Journalisation
            journalManager.addSaisieAvis(uid, voeuEleveAModifier, informationModification);

            // Mise � jour de l'�l�ve
            modifier(eleveConcerne, ine);
        } else {
            informationModification = "Pas de modification";
        }

        if (LOG.isDebugEnabled()) {
            String descriptionCibleOperation = " pour l'avis de type " + typeAvis + " sur le voeu "
                    + rang.toString() + " de l'�l�ve " + ine;
            LOG.debug(informationModification + descriptionCibleOperation);
        }
    }

    /**
     * D�taille la validit� des voeux d'un �l�ve.
     *
     * @param eleve
     *            �l�ve concern�
     * @param motifsValidite
     *            liste de cha�nes explicative sur la validation
     * @param flagsVoeuValide
     *            Table des flags de validit� des voeux pour chaque rang
     * @param accesGestionnaire
     *            consid�re-t-on les aspects gestionnaire
     */
    public void detailleValiditeVoeux(Eleve eleve, List<String> motifsValidite,
            Map<Integer, Boolean> flagsVoeuValide, boolean accesGestionnaire) {
        // On r�cup�re la liste des voeux de l'�l�ve
        List<VoeuEleve> listeVoeux = eleve.voeuxDuTour(campagneManager.getNumeroTourCourant());

        // Au d�part on marque tous les voeux comme valides
        for (int indiceRang = 1; indiceRang < listeVoeux.size(); indiceRang++) {
            flagsVoeuValide.put(Integer.valueOf(indiceRang), Boolean.TRUE);
        }

        // Dans le cas d'un �l�ve forc�, la saisie des voeux est automatiquement
        // valid�e.
        if (estCandidatureCouranteForcee(eleve)) {
            if (accesGestionnaire) {
                motifsValidite.add("Les voeux de l'�l�ve sont forc�s.");
            }
        } else {

            Palier palierOrigine = eleve.getPalierOrigine();

            // y a t il des voeux ?
            if (listeVoeux.isEmpty()) {
                motifsValidite.add("L'�l�ve n'a pas de voeux.");
            } else if (palierOrigine == null) {
                motifsValidite.add("L'�l�ve n'a pas de palier d'origine.");
            } else {

                int rang = 0;

                // pour chacun des voeux, on v�rifie si les avis sont saisis et si la d�cision d'orientation
                // correspond
                for (VoeuEleve ve : listeVoeux) {

                    rang++;

                    if (ve == null) {
                        // Saut de rang dans les voeux
                        motifsValidite.add("Le voeu de rang " + rang + " n'existe pas.");

                    } else if (ve.getFlagRefusVoeu().equals(Flag.NON)) {

                        Voeu offreFormation = ve.getVoeu();
                        if (!offreFormation.saisissablePourPalier(palierOrigine.getCode())) {
                            motifsValidite.add("Le voeu de rang " + rang
                                    + " est invalide car l'offre de formation " + offreFormation.getCode()
                                    + " ne peut pas �tre utilis�e pour le palier "
                                    + palierOrigine.getLibelleCourt());
                            flagsVoeuValide.put(Integer.valueOf(rang), Boolean.FALSE);
                        }

                        if (!ve.estValideReservation3Segpa()) {
                            motifsValidite.add("Le voeu de rang " + rang
                                    + " est invalide car l'offre de formation " + offreFormation.getCode()
                                    + " est r�serv�e aux �l�ves venant de 3�me SEGPA.");
                            flagsVoeuValide.put(Integer.valueOf(rang), Boolean.FALSE);
                        }

                        TypeAvisManager typeAvisManager = SpringUtils.getBean(TypeAvisManager.class);

                        // le voeu n'a pas �t� refus� (#415)
                        // L'avis paramefa est-il obligatoire ?
                        // Avis du chef de l'�tablissement d'origine
                        if ((ve.getAvisConseilClasse() == null)
                                && voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT,
                                        VoeuEleveManager.SAISIE_OBLIGATOIRE, ve, accesGestionnaire)) {

                            String libelleAvisConseilClasse = typeAvisManager
                                    .charger(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT).getLibelle();
                            motifsValidite.add("Le voeu de rang " + rang + " est en attente de saisie de l'"
                                    + libelleAvisConseilClasse);
                            flagsVoeuValide.put(Integer.valueOf(rang), Boolean.FALSE);
                        }
                        // L'avis de gestion du voeu est-il obligatoire ?
                        if ((ve.getAvisEntretien() == null)
                                && voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_DE_GESTION,
                                        VoeuEleveManager.SAISIE_OBLIGATOIRE, ve, accesGestionnaire)) {

                            motifsValidite.add("Le voeu de rang " + rang + " est en attente de saisie de l'"
                                    + typeAvisManager.charger(CodeTypeAvis.AVIS_DE_GESTION).getLibelle());

                            flagsVoeuValide.put(Integer.valueOf(rang), Boolean.FALSE);
                        }

                        // On ne contr�le pas l'avis DSDEN car il n'est pas obligatoire.

                        // L'avis de passerelle est-il obligatoire ?
                        if ((ve.getAvisPasserelle() == null)
                                && voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_PASSERELLE,
                                        VoeuEleveManager.SAISIE_OBLIGATOIRE, ve, accesGestionnaire)) {

                            motifsValidite.add("Le voeu de rang " + rang + " est en attente de saisie de l'"
                                    + typeAvisManager.charger(CodeTypeAvis.AVIS_PASSERELLE).getLibelle());

                            flagsVoeuValide.put(Integer.valueOf(rang), Boolean.FALSE);
                        }

                    }
                } // for -- voeux de l'�l�ve
            } // if -- existence de voeux
        } // if -- for�age
    }

    /**
     * M�thode d'ajout d'un voeu � un �l�ve. Cette m�thode assure la construction
     * d'une candidature si n�cessaire.
     *
     * @param eleve
     *            l'�l�ve
     * @param voeuEleve
     *            le voeu � ajouter
     *
     *
     */
    public void ajouterVoeu(Eleve eleve, VoeuEleve voeuEleve) {
        Candidature candidatureCourante = getCandidatureCourante(eleve);

        // Si c'est le premier voeu de l'�l�ve, on construit sa candidature pour le tour
        if (candidatureCourante == null) {
            candidatureCourante = candidatureManager.creer(eleve);
        } else {
            candidatureManager.modifier(candidatureCourante);
        }

        // creation du voeu pour l'eleve
        voeuEleve.setCandidature(candidatureCourante);
        voeuEleve.setDateCreation(new Date());
        // Attribution d'une valeur de d�partage al�atoire
        voeuEleve.setValeurDepartage(VoeuEleve.genererValeurDepartage());

        opaManager.obligerRelanceOpa(voeuEleve.getVoeu());
        voeuEleveDao.creer(voeuEleve);

        modifier(eleve, eleve.getIne());
    }

    /**
     * Supprimer le voeu d'un �l�ve.
     *
     * @param eleve
     *            �l�ve concern�
     * @param rang
     *            du voeu � supprimer
     * @param codeVoeu
     *            Code du voeu modifi� (pour confirmer la r�f�rence du voeu �
     *            modifier)
     * @param uid
     *            identifiant de l'utilisateur
     * @param accesGestionnaire
     *            vrai s'il s'agit d'un acc�s gestionnaire
     * @return l'�l�ve apr�s la supression du voeu
     */
    public Eleve supprimerVoeuEleve(Eleve eleve, int rang, String codeVoeu, String uid,
            boolean accesGestionnaire) {
        String ine = eleve.getIne();

        short numeroTourCourant = campagneManager.getNumeroTourCourant();

        VoeuElevePK voeuElevePK = new VoeuElevePK(numeroTourCourant, ine, rang);

        if (voeuEleveDao.existe(voeuElevePK)) {
            LOG.debug("Suppression de voeu pour l'�l�ve " + ine + ", au rang " + rang + ", par l'utilisateur "
                    + uid);

            // On fait un "deep" clone pour pouvoir voir les modifications qui ont ete apportees
            EleveSuiviCandidatureDto eleveAnciennesDonnees = eleveSuiviCandidatureMapper
                    .eleveToEleveSuiviCandidatureDto(eleve);

            VoeuEleve voeuEleve = voeuEleveDao.charger(voeuElevePK);
            controleCodeVoeuIdentique(ine, rang, codeVoeu, voeuEleve);
            if (!estVoeuEleveModifiable(eleve, voeuEleve, accesGestionnaire)) {
                throw new BusinessException("Le voeu " + voeuElevePK.toPrettyString() + " n'est pas modifiable");
            }

            opaManager.obligerRelanceOpa(voeuEleve.getVoeu());

            // enregistrement dans le journal des op�rations
            journalManager.addDeleteVoeuEleve(uid, voeuEleve);

            Candidature candidature = voeuEleve.getCandidature();

            // Suppression du voeu
            int rangVoeuSupprime = voeuEleve.getId().getRang();

            // Nettoyage de la session Hibernate
            // On s'assure ainsi que le lien du voeuEleve vers la candidature est lazy
            // Ce qui permet la suppression du voeuEleve
            HibernateUtil.cleanupSession();
            HibernateUtil.currentSession().refresh(voeuEleve);
            voeuEleveDao.supprimer(voeuEleve);

            // R�cup�ration des voeux pour un d�calage �ventuel du rang
            List<VoeuEleve> voeuxEleve = new ArrayList<>();
            voeuxEleve.addAll(candidature.getVoeux());
            voeuxEleve.remove(voeuEleve);

            // R�initialisation de la liste des voeux
            candidature.getVoeux().clear();

            // Pour mettre � jour le rang du voeu, on doit changer sa cl� primaire
            // Pour ce faire, on doit supprimer l'�l�ment puis le recr�er
            for (VoeuEleve voeuE : voeuxEleve) {
                VoeuEleve voeu = HibernateUtil.currentSession().load(VoeuEleve.class, voeuE.getId());
                if (voeu.getId().getRang() > rangVoeuSupprime) {
                    ResultatsProvisoiresOpa resultatsProvisoires = voeu.getResultatsProvisoiresOpa();
                    VoeuElevePK ancienId = voeu.getId();
                    VoeuElevePK nouvelId = new VoeuElevePK(ancienId.getNumeroTour(), ancienId.getIne(),
                            ancienId.getRang() - 1);

                    // Suppression du voeuEleve
                    voeuEleveDao.supprimer(voeu);

                    // Mise � jour de l'ID
                    voeu.setId(nouvelId);
                    if (resultatsProvisoires != null) {
                        resultatsProvisoires.setId(nouvelId);
                    }
                }
                // On remet les voeux dans la candidature
                candidature.getVoeux().add(voeu);
            }

            HibernateUtil.currentSession().flush();

            // Mise � jour du rang du voeu d'admission de l'�l�ve
            if (candidature.getRangAdmission() != null && candidature.getRangAdmission() > rangVoeuSupprime) {
                candidature.setRangAdmission(candidature.getRangAdmission() - 1);
            }

            // Enregistrement des voeux via la mise � jour de la candidature
            candidatureManager.modifier(candidature);

            // Rafraichissement de la session hibernate
            HibernateUtil.currentSession().flush();
            HibernateUtil.cleanupSession();

            // Mise � jour de l'�l�ve
            Eleve eleveModifie = chargerEleveParIneAvecInfosVoeux(eleve.getIne());

            // R�cup�ration de la candidature de l'�l�ve modifi�
            Candidature candidatureModifiee = getCandidatureCourante(eleve);

            // si l'eleve n'a plus de voeu, on supprime sa candidature pour le tour courant
            if (candidatureModifiee.getVoeux().isEmpty()) {
                Candidature candidatureSupprimee = eleveModifie.getCandidatures().remove(numeroTourCourant);
                candidatureManager.supprimer(candidatureSupprimee);
            } else {
                // Sinon on met � jour sa d�cision d'orientation si besoin
                if (candidatureModifiee.getCodeDecisionFinale() != null) {
                    ajusterDecisionFinaleEleve(eleveModifie);
                }
            }
            // mise � jour �l�ve
            modifier(eleveModifie, eleveModifie.getIne());

            // On creer une ligne pour chaque repr�sentant l�gal de l'eleve (pour plus tart leur envoyer un mail
            // d'information) selon certaines conditions
            preparerEnvoiNotificationsResponsablesSiNecessaire(eleveAnciennesDonnees, eleveModifie);

            return eleveModifie;
        } else {
            throw new BusinessException("Le voeu " + voeuElevePK.toPrettyString() + " � supprimer n'existe pas.");
        }
    }

    /**
     * M�thode de mise � jour de la d�cision finale de la candidature de l'�l�ve,
     * dans le tour courant. Si une d�cision finale existe dans la candidature
     * courante, les voeux de l'�l�ve sont analys�s, et la d�cision finale et le
     * rang d'admission sont mis � jour s'ils sont incorrects.
     *
     * @param eleve
     *            l'�l�ve dont la d�cision finale doit �tre v�rifi�e
     * @return vrai si la d�cision finale a �t� modifi�e, faux sinon
     */
    public boolean ajusterDecisionFinaleEleve(Eleve eleve) {
        Candidature candidatureCourante = getCandidatureCourante(eleve);
        if (candidatureCourante != null && candidatureCourante.getCodeDecisionFinale() != null) {

            if (Flag.OUI.equals(candidatureCourante.getFlagEleveForce())
                    && DecisionFinale.ABANDON.getCode() != candidatureCourante.getCodeDecisionFinale()) {
                switch (DecisionFinale.getPourCode(candidatureCourante.getCodeDecisionFinale())) {
                    case AFFECTE:
                    case ADMIS_CONTRAT_SIGNE:
                    case EN_ATTENTE_SIGNATURE_CONTRAT:
                    case REFUSE:
                        return false;
                    // Dans les cas suivants, la d�cision finale n'est pas compatible avec la candidature forc�e
                    // ou la d�cision peut encore changer (abandon)
                    case ABANDON:
                    case LISTE_SUPP:
                    case RECENSEMENT:
                    case DOSSIER_ABSENT:
                    case NON_TRAITE:
                        break;
                    default: break;
                }
            }
            VoeuEleveDecisionFinaleComparateur comparateur = new VoeuEleveDecisionFinaleComparateur(
                    DecisionFinale.ABANDON.getCode() != candidatureCourante.getCodeDecisionFinale());

            // On trie les voeux en fonction de leur impact sur la d�cision de la canditure
            List<VoeuEleve> voeux = new ArrayList<>(candidatureCourante.getVoeux());

            Integer nouveauCodeDecisionFinale = null;
            Integer nouveauRangAdmission = null;
            // Si il y reste au moins un voeu (suite � une suppression par exemple), on v�rifie la nouvelle
            // d�cision finale et le nouveau rang d'admission potentiel
            if (!voeux.isEmpty()) {
                voeux.sort(comparateur);
                VoeuEleve voeuAffectation = voeux.get(0);
                nouveauCodeDecisionFinale = adaptationDecisionVoeuACandidature(candidatureCourante,
                        voeuAffectation);

                if (nouveauCodeDecisionFinale != null
                        && DecisionFinale.AFFECTE.getCode() == nouveauCodeDecisionFinale) {
                    nouveauRangAdmission = voeuAffectation.getId().getRang();
                }
            }

            // Modification des informations dans la candidature si n�cessaire
            boolean codeDecisionFinaleCorrect = candidatureCourante
                    .getCodeDecisionFinale() == nouveauCodeDecisionFinale;
            boolean rangAdmissionCorrect = candidatureCourante.getRangAdmission() == null
                    && nouveauRangAdmission == null
                    || candidatureCourante.getRangAdmission() == nouveauRangAdmission;

            if (!codeDecisionFinaleCorrect || !rangAdmissionCorrect) {
                candidatureCourante.setCodeDecisionFinale(nouveauCodeDecisionFinale);
                candidatureCourante.setRangAdmission(nouveauRangAdmission);
                return true;
            }
        }

        return false;
    }

    /**
     * Convcerti si n�cessaire la d�cision finale du voeu vers la d�cision finale de candidature.
     *
     * @param candidatureCourante
     *            la candidature de l'�l�ve
     * @param voeuAffectation
     *            le voeu impactant la d�cision finale
     * @return
     */
    private Integer adaptationDecisionVoeuACandidature(Candidature candidatureCourante,
            VoeuEleve voeuAffectation) {
        Integer nouveauCodeDecisionFinale = voeuAffectation.getCodeDecisionFinal();

        if (nouveauCodeDecisionFinale == null) {
            return null;
        }

        // En cas de for�age "abandon", on conserve la d�cision "abandon" pour les voeux class�s et les voeux de
        // recensement
        if (DecisionFinale.ABANDON.getCode() == candidatureCourante.getCodeDecisionFinale()
                && (voeuAffectation.getVoeu().estRecensement()
                        || !voeuAffectation.getVoeu().estReclassementApprentissage())) {
            return DecisionFinale.ABANDON.getCode();
        }

        // La d�cision de la candidature est � refus� lorsque les d�cision des voeux sont "non trait�" ou "dossier
        // absent"
        if (nouveauCodeDecisionFinale.equals(DecisionFinale.NON_TRAITE.getCode())
                || nouveauCodeDecisionFinale.equals(DecisionFinale.DOSSIER_ABSENT.getCode())) {
            return DecisionFinale.REFUSE.getCode();
        }

        return nouveauCodeDecisionFinale;
    }

    /**
     * Teste si un voeu �l�ve est modifiable.
     *
     * @param eleve
     *            �l�ve concern�
     * @param voeuEleve
     *            voeu de l'�l�ve � examiner
     * @param accesGestionnaire
     *            vrai s'il s'agit d'un acc�s gestionnaire
     * @return vrai si le voeu est modifiable
     */
    public boolean estVoeuEleveModifiable(Eleve eleve, VoeuEleve voeuEleve, boolean accesGestionnaire) {

        short numeroTourVoeu = voeuEleve.getId().getNumeroTour();
        short numeroTourCourant = campagneManager.getNumeroTourCourant();

        // Pour �tre modifiable, le voeu doit �tre du tour courant
        if (numeroTourVoeu != numeroTourCourant) {
            return false;
        }

        // Un voeu d'�l�ve est modifiable si la d�cision finale n'est pas pr�sente
        // ou si on est gestionnaire et que l'�l�ve n'est pas forc�
        boolean estSansDecisionFinale = (voeuEleve.getCodeDecisionFinal() == null);

        // S'il y a des d�cisions finales, on permet les modifications tant que le tour
        // du voeu
        // n'a pas �t� enregistr�
        short numeroDernierTourEnregistre = campagneManager.getNumeroDernierTourEnregistre();

        boolean tourVoeuNonEnregistre = numeroTourVoeu > numeroDernierTourEnregistre;

        // R�cup�ration de la candidature courante pour examen du for�age
        Candidature candidatureCourante = getCandidatureCourante(eleve);

        boolean estEleveNonForce = candidatureCourante != null
                && Flag.NON.equals(candidatureCourante.getFlagEleveForce());

        return estSansDecisionFinale || (estEleveNonForce && (accesGestionnaire || tourVoeuNonEnregistre));
    }

    /**
     * Contr�le que le voeu retrouv� par (ine et rang) correspond bien au code voeu
     * indiqu�.
     *
     * @param ine
     *            INE de l'�l�ve ayant formul� le voeu (pour affichage en cas de
     *            probl�me)
     * @param rang
     *            rang du voeu contr�l� (pour affichage en cas de probl�me)
     * @param codeVoeu
     *            code du voeu � contr�ler
     * @param voeuEleve
     *            Voeu retrouv�
     *
     *            Une exception m�tier est d�clench�e si le code voeu n'est pas le
     *            m�me (Cela peut se produire si le voeu change entre temps ...)
     */
    private void controleCodeVoeuIdentique(String ine, int rang, String codeVoeu, VoeuEleve voeuEleve) {
        String codeVoeuActuel = voeuEleve.getVoeu().getCode();
        if (!codeVoeuActuel.equals(codeVoeu)) {
            throw new BusinessException(ApplicationMessage.get("saisieVoeux.eleve.voeux.modif.echecControle",
                    new Object[] { ine, rang, codeVoeu, codeVoeuActuel }));
        }
    }

    /**
     * Monte un voeu d'un �l�ve d'un rang.
     *
     * @param eleve
     *            �l�ve concern�
     * @param rang
     *            du voeu � monter
     * @param codeVoeu
     *            code du voeu � contr�ler
     * @param uid
     *            identifiant de l'utilisateur r�alisant l'op�ration
     * @param accesGestionnaire
     *            vrai s'il s'agit d'un acc�s gestionnaire
     */
    public void monterVoeu(Eleve eleve, int rang, String codeVoeu, String uid, boolean accesGestionnaire) {

        String ine = eleve.getIne();
        VoeuElevePK voeuElevePK = new VoeuElevePK(campagneManager.getNumeroTourCourant(), ine, rang);

        if (rang < 2) {
            throw new BusinessException("Impossible de monter plus haut le voeu de rang " + rang);

        } else if (voeuEleveDao.existe(voeuElevePK)) {
            LOG.debug("Mont�e du voeu " + voeuElevePK.toPrettyString() + " par l'utilisateur " + uid);

            // On fait un "deep" clone pour pouvoir voir les modifications qui ont ete apportees
            EleveSuiviCandidatureDto eleveAnciennesDonnees = eleveSuiviCandidatureMapper
                    .eleveToEleveSuiviCandidatureDto(eleve);

            VoeuEleve voeuEleve = voeuEleveDao.charger(voeuElevePK);
            controleCodeVoeuIdentique(ine, rang, codeVoeu, voeuEleve);
            if (!estVoeuEleveModifiable(eleve, voeuEleve, accesGestionnaire)) {
                throw new BusinessException("Le voeu " + voeuElevePK.toPrettyString() + " n'est pas modifiable");
            }

            voeuEleveManager.monterVoeu(uid, voeuEleve);

            // On creer une ligne pour chaque repr�sentant l�gal de l'eleve (pour plus tart leur envoyer un mail
            // d'information) selon certaines conditions
            preparerEnvoiNotificationsResponsablesSiNecessaire(eleveAnciennesDonnees,
                    chargerEleveParIneAvecInfosVoeux(ine));
        } else {
            throw new BusinessException("Le voeu " + voeuElevePK.toPrettyString() + " � monter n'existe pas.");
        }
    }

    /**
     * Descend un voeu d'un �l�ve d'un rang.
     *
     * @param eleve
     *            �l�ve concern�
     * @param rang
     *            du voeu � monter
     * @param codeVoeu
     *            code du voeu � contr�ler
     * @param uid
     *            identifiant de l'utilisateur r�alisant l'op�ration
     * @param accesGestionnaire
     *            vrai s'il s'agit d'un acc�s gestionnaire
     */
    public void descendreVoeu(Eleve eleve, int rang, String codeVoeu, String uid, boolean accesGestionnaire) {

        String ine = eleve.getIne();
        int nbVoeuxEleve = eleve.voeuxDuTour(campagneManager.getNumeroTourCourant()).size();

        VoeuElevePK voeuElevePK = new VoeuElevePK(campagneManager.getNumeroTourCourant(), ine, rang);

        if (rang >= nbVoeuxEleve) {
            // On ne peut pas descendre un voeu plus bas que le rang maximal actuel
            // qui est �gal au nombre de voeux actuel de l'�l�ve
            throw new BusinessException("Impossible de descendre plus bas le voeu de rang " + rang + ". L'�l�ve "
                    + ine + " a actuellement " + nbVoeuxEleve + " voeu(x).");

        } else if (voeuEleveDao.existe(voeuElevePK)) {
            LOG.debug("Descente du voeu " + voeuElevePK.toPrettyString() + " par l'utilisateur " + uid);

            // On fait un "deep" clone pour pouvoir voir les modifications qui ont ete apportees
            EleveSuiviCandidatureDto eleveAnciennesDonnees = eleveSuiviCandidatureMapper
                    .eleveToEleveSuiviCandidatureDto(eleve);

            Set<String> listFetchJoin = new HashSet<>();
            listFetchJoin.add("voeu.etablissement");
            listFetchJoin.add("voeu.etablissement.typeEtablissement");
            listFetchJoin.add("voeu.formation");
            listFetchJoin.add("voeu.voie");
            listFetchJoin.add("avisConseilClasse");
            listFetchJoin.add("avisEntretien");
            listFetchJoin.add("lv1");
            listFetchJoin.add("lv2");
            listFetchJoin.add("derogationsVoeuEleve");
            VoeuEleve voeuEleve = voeuEleveDao.charger(voeuElevePK, listFetchJoin);
            controleCodeVoeuIdentique(ine, rang, codeVoeu, voeuEleve);
            if (!estVoeuEleveModifiable(eleve, voeuEleve, accesGestionnaire)) {
                throw new BusinessException("Le voeu " + voeuElevePK.toPrettyString() + " n'est pas modifiable");
            }

            voeuEleveManager.descendreVoeu(uid, voeuEleve);

            // On creer une ligne pour chaque repr�sentant l�gal de l'eleve (pour plus tart leur envoyer un mail
            // d'information) selon certaines conditions
            preparerEnvoiNotificationsResponsablesSiNecessaire(eleveAnciennesDonnees,
                    chargerEleveParIneAvecInfosVoeux(ine));
        } else {
            throw new BusinessException("Le voeu " + voeuElevePK.toPrettyString() + " � descendre n'existe pas.");
        }
    }

    /**
     * @return vrai si les groupes origine ont d�j� �t� attribu�s, sinon faux
     */
    public boolean groupesOrigineAttribues() {
        int nbElevesAvecGroupeOrigine = eleveDao.getNombreElements(Filtre.isNotNull("groupeOrigine"));
        LOG.debug("El�ves ayant un groupe origine :" + nbElevesAvecGroupeOrigine);

        return nbElevesAvecGroupeOrigine > 0;
    }

    /**
     * V�rifie que tous les �l�ves de l'�tablissement sont valides, du point de vue
     * des avis.
     *
     * @param etablissement
     *            l'�tablissement � tester
     * @return vrai si tous les �l�ves sont valides du point de vue des avis, sinon
     *         faux
     */
    public boolean avisValidesPourEtablissement(Etablissement etablissement) {
        // On liste les �l�ves de l'�tablissement qui ont des voeux et pour lesquels les
        // avis ne sont pas valides
        List<Filtre> listFiltres = new ArrayList<>();
        listFiltres.add(Filtre.equal("etablissement.id", etablissement.getId()));
        listFiltres.add(filtreEleveAyantDesVoeux());
        listFiltres.add(filtreElevesAvisManquants());

        int nombreElevesAvisInvalides = eleveDao.getNombreElements(listFiltres);
        return nombreElevesAvisInvalides == 0;
    }

    /**
     * Contr�le que tous les �l�ves de l'�tablissement du palier 3�me ont des voeux.
     *
     * @param etablissement
     *            l'�tablissement
     * @return vrai si certains �l�ves n'ont pas de voeu pour l'�tablissement
     */
    public boolean existeElevesPalier3SansVoeuxDansEtablissement(Etablissement etablissement) {
        // Liste les �l�ves de l'�tablissement qui n'ont pas de voeux
        List<Filtre> listFiltres = new ArrayList<>();
        listFiltres.add(Filtre.equal("etablissement.id", etablissement.getId()));
        listFiltres.add(Filtre.equal("palierOrigine.id", Palier.CODE_PALIER_3EME));
        listFiltres.add(filtreEleveSansVoeu());

        return eleveDao.getNombreElements(listFiltres) != 0;
    }

    /**
     * V�rifie zones g�ographiques obligatoires pour les �l�ves de l'�tablissement
     * donn�.
     *
     * @param etablissement
     *            l'�tablissement � contr�ler
     * @return vrai s'il manque des zones g�ographiques obligatoires pour les �l�ves
     *         de l'�tablisssement.
     */
    public boolean zonesGeographiquesObligatoiresOk(Etablissement etablissement) {
        // On liste les �l�ves de l'�tablissement qui n'ont pas de zone g�ographique
        List<Filtre> filtresEleves = new ArrayList<>();
        filtresEleves.add(Filtre.equal("etablissement.id", etablissement.getId()));
        filtresEleves.add(Filtre.isNull("zoneGeo"));
        // v�rification des �l�ves ayants des voeux mais pas de zone g�ographique
        filtresEleves.add(filtreEleveAyantDesVoeux());

        return eleveDao.getNombreElements(filtresEleves) == 0;
    }

    /**
     * Modifie un �l�ve en base de donn�es.
     *
     * @param eleve
     *            L'�l�ve � modifier.
     * @return L'�l�ve qui vient d'�tre modifi�.
     */
    public Eleve modifier(Eleve eleve) {
        return modifier(eleve, eleve.getIne());
    }

    /**
     * M�thode de modification d'un �l�ve.
     *
     * @param eleve
     *            l'�l�ve modifi�
     * @param ine
     *            l'ine de l'�l�ve � modifier
     * @return l'�l�ve modifi�
     */
    public Eleve modifier(Eleve eleve, String ine) {
        Integer nbRangs = notesManager.getNbRangs();

        Candidature candidature = getCandidatureCourante(eleve);
        setFlagsEleve(candidature);

        // Recalcul de la note moyenne de l'�l�ve
        eleve.setNoteMoyenne(eleve.calculerMoyenne(nbRangs));

        // Mise � jour de la date de derni�re modification de l'�l�ve
        eleve.setDateMAJ(new Date());

        if (candidature != null) {
            opaManager.obligerRelanceOpa(candidature);
        }

        return eleveDao.maj(eleve, ine);
    }

    /**
     * G�re les d�cisions d'orientation des �l�ves en provenance de Orientation.
     *
     * @param ine
     *            L'ine de l'�l�ve provenant de Orientation.
     * @param uai
     *            Le code UAI de l'�tablissement de l'�l�ve.
     * @param decisionOrientationEleves
     *            La liste des DO associ�es � l'�l�ves.
     * @param decisionOrientationEleveRecues
     *            la liste des DO re�ue de l'xml
     * @return vrai si on modifie les d�cisions d'orientation de l'�l�ve
     */
    public boolean decisionOrientationSdo(String ine, String uai,
            Set<DecisionOrientationEleve> decisionOrientationEleves,
            Set<DecisionOrientationRecueCouplePalierCode> decisionOrientationEleveRecues) {

        // Initialisation
        boolean flagDecisionOrientationSdo = true;
        Eleve eleve = null;

        // Si l'�l�ve est dans un �tablissement existant, que son �tablissement
        // d'origine est coh�rent entre
        // AFFELNET et Orientation
        // alors on prend on compte la d�cision d'orientation en provenance de
        // Orientation.
        if (existeParIne(ine)) {
            eleve = chargerParIne(ine);

            Etablissement etab = eleve.getEtablissement();
            Short palierOrigineEleve = eleve.getPalierOrigine().getCode();
            if (etab == null) {
                LOG.debug("DO - L'�tablissement associ� � l'�l�ve " + ine + " est null");
                receptionDOManager.creerOperation(uai, ine, decisionOrientationEleveRecues,
                        TypeReceptionDO.ETAB_INCOHERENT_HORS_ACA);
                flagDecisionOrientationSdo = false;
            } else {
                String idEtab = etab.getId();
                if (StringUtils.equals(idEtab, uai)) {
                    if (palierOrigineEleve == null) {
                        LOG.debug("DO - Le palier de l'�l�ve n'est pas d�fini.");
                        receptionDOManager.creerOperation(uai, ine, decisionOrientationEleveRecues,
                                TypeReceptionDO.PALIER_INCOHERENT);
                        flagDecisionOrientationSdo = false;
                    } else {
                        /**
                         * Si une d�cision d'orientation a d�j� �t� saisie manuellement,
                         * on ne rend plus possible l'int�gration de d�cisions d'orientation via le webservice
                         *
                         */
                        if (eleve.getFlagDecOriManuelle() != null
                                && Flag.OUI.equals(eleve.getFlagDecOriManuelle())) {
                            LOG.debug(
                                    "DO - Les d�cisions d'orientation n'ont pas �t� " +
                                            "int�gr�es car une saisie manuelle a eu lieu.");
                            receptionDOManager.creerOperation(uai, ine, decisionOrientationEleveRecues,
                                    TypeReceptionDO.ELEVE_DECISIONS_ORIENTATION_MANU_EXISTE);
                            flagDecisionOrientationSdo = false;
                        } else {
                            if (controleDO(palierOrigineEleve, decisionOrientationEleves)) {
                                LOG.debug("Changement de d�cision d'orientation");
                                eleve.getDecisionsOrientationEleve().clear();
                                eleve.getDecisionsOrientationEleve().addAll(decisionOrientationEleves);
                                eleve.setFlagDecOriManuelle(Flag.NON);
                                modifier(eleve);
                            } else {
                                LOG.debug("DO - Le palier associ� � l'�l�ve " + ine + " est diff�rent ("
                                        + palierOrigineEleve + ") de celui envoy� par Orientation ");
                                receptionDOManager.creerOperation(uai, ine, decisionOrientationEleveRecues,
                                        TypeReceptionDO.PALIER_INCOHERENT);
                                flagDecisionOrientationSdo = false;
                            }
                        }
                    }
                } else {
                    LOG.debug("DO - L'�tablissement de l'�l�ve " + ine + " est diff�rent (" + idEtab
                            + ") de celui envoy� par Orientation :" + uai);
                    receptionDOManager.creerOperation(uai, ine, decisionOrientationEleveRecues,
                            TypeReceptionDO.ETAB_INCOHERENT);
                    flagDecisionOrientationSdo = false;
                }
            }
        } else {
            LOG.debug("DO - L'�l�ve n'existe pas pour l'ine " + ine);
            receptionDOManager.creerOperation(uai, ine, decisionOrientationEleveRecues,
                    TypeReceptionDO.ERREUR_INE);
            flagDecisionOrientationSdo = false;
        }

        return flagDecisionOrientationSdo;

    }

    /**
     * M�thode v�rifiant que chaque DO est bien conforme au palier d'origine de
     * l'�l�ve.
     *
     * @param codePalierOrigine
     *            le code Palier d'origine de l'�l�ve.
     * @param decisionOrientationEleves
     *            Une collection de DO.
     * @return
     *         <ul>
     *         <li><strong>true</strong> si toutes les DO de la collection de
     *         l'�l�ve sont conformes � son palier d'origine.</li>
     *         <li><strong>false</strong> si au moins une DO n'est pas conforme au
     *         palier d'origine.</li>
     *         </ul>
     */
    public boolean controleDO(Short codePalierOrigine, Set<DecisionOrientationEleve> decisionOrientationEleves) {
        for (DecisionOrientationEleve doe : decisionOrientationEleves) {
            if (!doe.getId().getCodePalier().equals(codePalierOrigine)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Recherche les �l�ves correspondant aux informations (nom, pr�nom, date de
     * naissance) indiqu�es pour un �tablissement.
     *
     * @param nom
     *            le nom de l'�l�ve
     * @param prenom
     *            le pr�nom de l'�l�ve
     * @param dateNaissance
     *            la date de naissance de l'�l�ve
     * @param idEtablissement
     *            Le code uai de l'�tablissement de l'�l�ve (ou null pour une
     *            recherche globale).
     * @return la liste des �l�ves en probalit� de doublons.
     */
    public List<Eleve> rechercherEleves(String nom, String prenom, Date dateNaissance, String idEtablissement) {
        return rechercherElevesGenerique(nom, prenom, dateNaissance, idEtablissement,
                "sql.select.commun.rechercheEleveParNomPrenom", "GV_ELEVE");
    }

    /**
     * Rechercher les �l�ves correspondant aux informations (nom, pr�nom, date de
     * naissance) indiqu�es.
     *
     * @param nom
     *            le nom de l'�l�ve
     * @param prenom
     *            le pr�nom de l'�l�ve nom de l'�l�ve
     * @param dateNaissance
     *            la date de naissance de l'�l�ve
     * @return la liste des �l�ves en probalit� de doublons.
     */
    public List<Eleve> rechercherEleves(String nom, String prenom, Date dateNaissance) {
        return rechercherEleves(nom, prenom, dateNaissance, null);
    }

    /**
     * Recherche les �l�ves par (nom, pr�nom, date de naissance) pour un
     * �tablissement.
     *
     * @param nom
     *            le nom de l'�l�ve
     * @param prenom
     *            le pr�nom de l'�l�ve nom de l'�l�ve
     * @param dateNaissance
     *            la date de naissance de l'�l�ve
     * @param idEtablissement
     *            Le code uai de l'�tablissement de l'�l�ve (ou null pour une
     *            recherche globale).
     * @param requeteNommee
     *            Le nom de la requete nomm�e.
     * @param tableReference
     *            La table qui sert � mapper les donn�es.
     * @return La liste des �l�ves correspondant aux crit�res.
     */
    @SuppressWarnings("unchecked")
    private List<Eleve> rechercherElevesGenerique(String nom, String prenom, Date dateNaissance,
            String idEtablissement, String requeteNommee, String tableReference) {
        Session session = HibernateUtil.currentSession();

        if (StringUtils.isNotEmpty(idEtablissement)
                && !EtablissementHelper.isIdentifiantConforme(idEtablissement)) {
            throw new ValidationException("L'�tablissement de l'�l�ve n'est pas conforme");
        }

        String nomRecherche = ChaineUtils.formaterCode(nom);
        String prenomRecherche = ChaineUtils.formaterCode(prenom);
        String idEtablissementRecherche = StringUtils.defaultString(StringUtils.upperCase(idEtablissement));

        if (LOG.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder("Recherche d'�l�ve : nom = '");
            sb.append(nomRecherche);
            sb.append("', pr�nom = '");
            sb.append(prenomRecherche);
            sb.append("', n�(e) le '");
            sb.append(dateNaissance);
            sb.append("', �tablissement '");
            sb.append(idEtablissementRecherche);
            sb.append("'.");

            LOG.debug(sb.toString());
        }

        Query requeteDoublons = DaoUtils.getNamedQuery(requeteNommee);

        String requeteDoulonsStr = requeteDoublons.getQueryString();

        SQLQuery requeteDoublonsSQL = session.createSQLQuery(requeteDoulonsStr);

        requeteDoublonsSQL.setString("idEtablissement", idEtablissementRecherche);
        requeteDoublonsSQL.setString("nom", nomRecherche);
        requeteDoublonsSQL.setString("prenom", prenomRecherche);
        requeteDoublonsSQL.setDate("dateNaissance", dateNaissance);

        requeteDoublonsSQL.addEntity(tableReference, Eleve.class);

        return (List<Eleve>) DaoUtils.listHibernateQuery(requeteDoublonsSQL);
    }

    /**
     * Permet de savoir si un �l�ve poss�de au moins un voeu.
     *
     * @param ine
     *            L'INE de l'�l�ve.
     * @return
     *         <ul>
     *         <li>true si l'�l�ve � au moins fait un voeu</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean possedeVoeu(String ine) {
        return voeuEleveDao.getNbVoeux(ine) > 0;
    }

    /**
     * M�thode permettant de suprimer les avis pour les voeux de l'�l�ve qui ne
     * doivent plus les renseigner. L'op�ration n'est effectu�e que si la formation
     * d'origine de l'�l�ve a �t� modifi�e.
     *
     * @param eleve
     *            l'�l�ve concern�
     * @param formationOrigine
     *            la formation d'origine de l'�l�ve avant sa modification
     * @return la liste des types d'avis supprim�s
     */
    public Set<CodeTypeAvis> suppressionAvisInutiles(Eleve eleve, Formation formationOrigine) {
        Set<CodeTypeAvis> typesAvisSupprimes = new HashSet<>();

        // Si la formation d'origine est modifi�e, on examine chaque voeu pour voir si
        // les avis sont toujours
        // accept�s
        if (!formationOrigine.equals(eleve.getFormation())) {
            for (VoeuEleve voeu : eleve.voeuxDuTour(campagneManager.getNumeroTourCourant())) {
                // Cas de l'avis du chef d'�tablissement
                ParametresFormationAccueil paramFormationAccueil = parametresFormationAccueilManager
                        .trouverParamefaPourVoeu(voeu.getVoeu());

                if (paramFormationAccueil != null && voeu.getAvisConseilClasse() != null) {
                    // Un bool�en pour m�moriser si l'avis doit �tre conserv� ou non
                    boolean avisASaisir = true;

                    if (ParametresFormationAccueilManager.IND_SAISIE_AVIS_SELON_FORMATION_ORIGINE
                            .equals(paramFormationAccueil.getIndicateurSaisieAvis())) {
                        // Si l'avis est � "Selon les formations d'origine", on parcourt les formations
                        // d'origine du parametre par formation d'accueil
                        avisASaisir = false;
                        for (AvisParFormationOrigine avis : paramFormationAccueil.getAvisParFormationOrigines()) {
                            // Si on trouve la formation d'origine de l'�l�ve on supprime l'avis pour
                            // l'�l�ve
                            if (avis.getFormation() != null) {
                                avisASaisir = avis.getFormation().equals(eleve.getFormation());
                            } else {
                                avisASaisir = avis.getMnemonique()
                                        .equals(eleve.getFormation().getId().getMnemonique());
                            }
                        }
                    }

                    // Si l'avis ne doit plus �tre saisi, alors on le retire pour le voeu de l'�l�ve
                    if (!avisASaisir) {
                        voeu.setAvisConseilClasse(null);
                        typesAvisSupprimes.add(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT);
                    }
                }

                // Cas de l'avis passerelle
                if (voeu.getAvisPasserelle() != null
                        && !voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_PASSERELLE,
                                VoeuEleveManager.SAISIE_POSSIBLE, voeu, true)) {
                    voeu.setAvisPasserelle(null);
                    typesAvisSupprimes.add(CodeTypeAvis.AVIS_PASSERELLE);
                }
            }
        }

        return typesAvisSupprimes;
    }

    /**
     * Compte le nombre de candidats affect�s.
     *
     * @return nombre de candidats affect�s
     */
    public int compteCandidatsAffectes() {
        List<Filtre> listeFiltresEleves = new ArrayList<>();
        listeFiltresEleves
                .add(Filtre.equal("codeDecisionFinale", Integer.valueOf(DecisionFinale.AFFECTE.getCode())));

        return eleveDao.getNombreElements(listeFiltresEleves);
    }

    /**
     * @param eleve
     *            l'�l�ve
     * @return vrai si l'�l�ve a une candidature forc�e globalement pour le tour en
     *         cours
     */
    public boolean estCandidatureCouranteForcee(Eleve eleve) {
        Candidature candidatureCourante = getCandidatureCourante(eleve);
        return estCandidatureForcee(candidatureCourante);
    }

    /**
     * @param candidature
     *            la candidature de l'�l�ve
     * @return vrai si candidature forc�e globalement pour le tour en
     *         cours
     */
    public boolean estCandidatureForcee(Candidature candidature) {
        if (candidature == null) {
            return false;
        }

        return candidature.estForcee();
    }

    /**
     * @param eleve
     *            l'�l�ve
     * @return vrai si l'onglet "Identification" pour la saisie des voeux d'un �l�ve
     *         est valide La saisie d'un �l�ve est valide
     */
    public boolean isIdentOK(Eleve eleve) {
        // cas d'un �l�ve forc� : la saisie est valide
        if (estCandidatureCouranteForcee(eleve)) {
            return true;
        }
        // v�rification des donn�es obligatoires sur l'�l�ve (qu'il ait ou non une candidature)
        return verificationDonneesEleves(eleve);
    }

    /**
     * @param eleve
     *            l'�l�ve
     * @return vrai si l'onglet "Identification" pour la saisie des voeux d'un �l�ve
     *         est valide La saisie d'un �l�ve est valide
     */
    public boolean verificationDonneesEleves(Eleve eleve) {
        // On doit avoir au moins un responsable notifiable
        List<Responsable> responsablesNotifiables = eleve.getResponsablesNotifiables();
        if (responsablesNotifiables.isEmpty()) {
            return false;
        }

        // La zone g�o est-elle saisie ?
        if (eleve.getZoneGeo() == null) {
            return false;
        }

        // Le palier d'origine est obligatoire
        return eleve.getPalierOrigine() != null;
    }

    /**
     * @param candidature
     *            la candidature de l'�l�ve
     * @return vrai si l'onglet "Identification" pour la saisie des voeux d'un �l�ve
     *         est valide La saisie d'un �l�ve est valide
     */
    public boolean isIdentOK(Candidature candidature) {
        // cas d'un �l�ve forc� : la saisie est valide
        if (estCandidatureForcee(candidature)) {
            return true;
        }
        return verificationDonneesEleves(candidature.getEleve());
    }

    /**
     * @param eleve
     *            l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait pour l'IA/Rectorat/Admin
     * @return vrai si la saisie d'un �l�ve est valide La saisie d'un �l�ve est
     *         valide :
     *         - si au moins 1 voeu est saisi
     *         - si un voeu bar�me avec �valuations/notes est saisi, les notes doivent �tre saisies.
     *         - si palier 3�me, il doit avoir des d�cisions d'orientation et qu'elles soient conformes
     */
    public boolean isSaisieValide(Eleve eleve, boolean isIaOuRectorat) {
        return isIdentOK(eleve) && evalNoteManager.isEvalNoteOK(eleve) && isVoeuxOK(eleve, isIaOuRectorat)
                && estConformeVoeuxDo(eleve, getCandidatureCourante(eleve));
    }

    /**
     * @param candidature
     *            la candidature de l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait pour l'IA/Rectorat/Admin
     * @return vrai si la saisie d'un �l�ve est valide La saisie d'un �l�ve est
     *         valide : - si au moins 1 voeu est saisi - si un voeu bar�me avec
     *         �valuations/notes est saisi, les notes doivent �tre saisies.
     */
    public boolean isSaisieValide(Candidature candidature, boolean isIaOuRectorat) {
        return isIdentOK(candidature) && evalNoteManager.isEvalNoteOK(candidature)
                && isVoeuxOK(candidature, isIaOuRectorat);
    }

    /**
     * Mets � jour les flag de conformit� des DO.
     * 
     * @param eleve
     *            l'�l�ve
     * @param candidatureCourante
     *            la candidature courante de l'�l�ve
     *            sont conformes (par d�faut conforme)
     */
    public void conformiteVoeuxDo(Eleve eleve, Candidature candidatureCourante) {
        // mise � jour sur la candidature de la valeur du Flag FL_CONFORME_DO
        candidatureCourante.setFlagConformeDo(Flag.pourBooleen(estConformeVoeuxDo(eleve, candidatureCourante)));
    }

    /**
     * D�termine si les voeux et les DO sont conformes.
     * 
     * @param eleve
     *            l'�l�ve concern�e
     * @param candidatureCourante
     *            la candidature de l'�l�ve
     * @return true si les voeux et les DO sont conformes
     */
    public boolean estConformeVoeuxDo(Eleve eleve, Candidature candidatureCourante) {

        boolean isVoeuxConformes = true;
        // init du flag � N si Palier 3 (reste initialis� � sa valeur par d�faut � 'O' dans tous les autres cas)
        if (eleve.getPalierOrigine() != null
                && Palier.CODE_PALIER_3EME.equals(eleve.getPalierOrigine().getCode())) {
            isVoeuxConformes = false;

            if (candidatureCourante != null) {
                Set<DecisionOrientationEleve> decisionOrientationEleve = eleve.getDecisionsOrientationEleve();
                List<VoeuEleve> voeuxEleve = candidatureCourante.getVoeux();

                if (CollectionUtils.isNotEmpty(voeuxEleve)) {

                    // Si aucun des voeux ne correspond aux voies des DO alors pas besoin de contr�ler les DO
                    boolean aQueVoeuxVoieHorsDo = voeuxEleve.stream()
                            .allMatch(v -> !Voie.LISTE_VOIES_DO_3EME.contains(v.getVoeu().getVoie().getCode()));

                    if (aQueVoeuxVoieHorsDo) {
                        isVoeuxConformes = true;
                    } else
                    if (decisionOrientationEleve != null && !decisionOrientationEleve.isEmpty()) {
                    isVoeuxConformes = evalueConformiteVoeux(decisionOrientationEleve, voeuxEleve);
                }
                }
            }
        }

        return isVoeuxConformes;
    }

    /**
     * Evalue la conformit� des voeux de l'�l�ve vis � vis des d�cisions d'orientation.
     *
     * @param candidature
     *            la candidature de l'�l�ve
     */
    public void conformiteVoeuxDo(Candidature candidature) {
        if (candidature != null) {
            conformiteVoeuxDo(candidature.getEleve(), candidature);
        }
    }

    /**
     *
     * @param candidature
     *            La candidature de l'�l�ve
     * @return true si le flag de conformit� des voeux vis � vis des d�cisions d'orientation est � "O" (Oui)
     */
    public boolean isVoeuxDoConformes(Candidature candidature) {
        return (Flag.OUI).equals(candidature.getFlagConformeDo()) ? true : false;
    }

    /**
     * Calcul de la conformit� des voeux avec les d�cisions d'orientation de l'�l�ve.
     *
     * @param decisionsOrientationEleve
     *            Les d�cisions d'orientation de l'�l�ve
     * @param voeuxEleve
     *            les voeux de l'�l�ve
     * @return boolean
     *         conforme (true) ou non conforme (false)
     */
    public boolean evalueConformiteVoeux(Set<DecisionOrientationEleve> decisionsOrientationEleve,
            List<VoeuEleve> voeuxEleve) {
        boolean isVoeuxConformes = true;
        // Ne garder que les voeux 2GT,2PRO et CAP
        String covoiDoSup = "";
        List<String> listCovoiDo = triListeCodeVoieDo(decisionsOrientationEleve);
        // on garde le premier �l�ment, qui donne donc le covoi le plus �lev�e hi�rarchiquement pour cet �l�ve
        if (!listCovoiDo.isEmpty()) {
            covoiDoSup = listCovoiDo.get(0);
        }
        String covoiVoeuSup = "";
        List<String> listCovoiVoeux = triListeCodeVoieVoeux(voeuxEleve);
        // on garde le premier �l�ment, qui donne donc le covoi le plus �lev�e hi�rarchiquement pour cet �l�ve
        if (!listCovoiVoeux.isEmpty()) {
            covoiVoeuSup = listCovoiVoeux.get(0);
        }
        ComparatorCovoi cvoi = new ComparatorCovoi();
        int retourCompare = cvoi.compare(covoiDoSup, covoiVoeuSup);
        // pour que la saisie soit conforme, covoiDoSup (le covoi de la DO) doit �tre > ou =
        // � covoiVoeuSup (le covoi du voeu)
        // par d�faut, conforme donc si le covoiVoeuSup est > au covoiDoSup, alors non conforme
        if (retourCompare == 1) {
            // non conforme
            isVoeuxConformes = false;
        }
        return isVoeuxConformes;
    }

    /**
     * Calcul de la conformit� d'un voeu avec les d�cisions d'orientation de cet �l�ve.
     *
     * @param voeuEleve
     *            Le voeu de l'�l�ve
     * @return boolean
     *         conforme (true) non conforme (false)
     */
    public boolean evalueConformiteVoeu(VoeuEleve voeuEleve) {
        boolean isVoeuConforme = true;
        Set<DecisionOrientationEleve> decisionsOrientationEleve = voeuEleve.getEleve()
                .getDecisionsOrientationEleve();
        if (!decisionsOrientationEleve.isEmpty()) {
            String covoiDoSup = "";
            List<String> listCovoiDo = triListeCodeVoieDo(decisionsOrientationEleve);
            if (!listCovoiDo.isEmpty()) {
                // on garde le premier �l�ment, qui donne donc le covoi le plus �lev�e hi�rarchiquement pour cet
                // �l�ve
                covoiDoSup = listCovoiDo.get(0);
            }
            String covoiVoeuEleve = voeuEleve.getVoeu().getVoie().getCode();
            ComparatorCovoi cvoi = new ComparatorCovoi();
            int retourCompare = cvoi.compare(covoiDoSup, covoiVoeuEleve);
            // pour que la saisie soit conforme, covoiDoSup (le covoi de la DO) doit �tre > ou =
            // � covoiVoeuSup (le covoi du voeu)
            // par d�faut, conforme donc si le covoiVoeuSup est > au covoiDoSup, alors non conforme
            if (retourCompare == 1) {
                // non conforme
                isVoeuConforme = false;
            }
        }
        return isVoeuConforme;
    }

    /**
     * Tri de la liste des codes voies d'orientation de la liste des d�cisions d'orientation de l'�l�ve.
     *
     * @param decisionsOrientationEleve
     *            la liste des d�cisions d'orientation de l'�l�ve
     * @return la liste tri�e
     */
    private List<String> triListeCodeVoieDo(Set<DecisionOrientationEleve> decisionsOrientationEleve) {
        // On trie la liste des Do par CO_VOI (hi�rarchie, du plus haut au plus bas)
        List<String> listCovoi = new ArrayList<>();

        for (DecisionOrientationEleve doe : decisionsOrientationEleve) {
            String coVoiDo = doe.getDecisionOrientation().getCodeVoieOrientation();

            if (coVoiDo != null && Voie.LISTE_VOIES_DO_3EME.contains(coVoiDo)) {
                listCovoi.add(coVoiDo);
            }
        }

        // On trie la liste des CO_VOI des DO (hi�rarchie, du plus haut au plus bas)
        if (!listCovoi.isEmpty()) {
            Collections.sort(listCovoi, new ComparatorCovoi());
        }
        return listCovoi;
    }

    /**
     * Tri de la liste des codes voies orientation de la liste des voeux de l'�l�ve.
     *
     * @param voeuxEleve
     *            la liste des voeux de l'�l�ve
     * @return la liste tri�e
     */
    private List<String> triListeCodeVoieVoeux(List<VoeuEleve> voeuxEleve) {
        // On trie la liste des voeux par CO_VOI (hi�rarchie, du plus haut au plus bas)
        List<String> listCovoi = new ArrayList<>();
        for (VoeuEleve voeu : voeuxEleve) {
            String coVoiVoeu = voeu.getVoeu().getVoie().getCode();

            if (coVoiVoeu != null && Voie.LISTE_VOIES_DO_3EME.contains(coVoiVoeu)) {
                listCovoi.add(coVoiVoeu);
            }
        }

        // On trie la liste des CO_VOI des voeux (hi�rarchie, du plus haut au plus bas)
        if (!listCovoi.isEmpty()) {
            Collections.sort(listCovoi, new ComparatorCovoi());
        }
        return listCovoi;
    }

    /**
     * @param eleve
     *            l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait pour l'IA/Rectorat/Admin
     * @return vrai si l'onglet "Saisie des voeux" pour la saisie des voeux d'un
     *         �l�ve est valide La saisie d'un �l�ve est valide.
     */
    public boolean isVoeuxOK(Eleve eleve, boolean isIaOuRectorat) {
        Candidature candidatureCourante = getCandidatureCourante(eleve);

        return isVoeuxOK(candidatureCourante, isIaOuRectorat);
    }

    /**
     * @param candidature
     *            la candidature de l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait pour l'IA/Rectorat/Admin
     * @return vrai si l'onglet "Saisie des voeux" pour la saisie des voeux d'un
     *         �l�ve est valide La saisie d'un �l�ve est valide.
     */
    public boolean isVoeuxOK(Candidature candidature, boolean isIaOuRectorat) {
        // cas d'un �l�ve forc� : la saisie est valide
        if (estCandidatureForcee(candidature)) {
            return true;
        }

        // r�cup�ration de la liste des voeux �l�ve
        List<VoeuEleve> listeVoeux = (candidature != null) ? candidature.getVoeux() : new ArrayList<>();

        if (candidature != null && candidature.getEleve() != null) {
            Eleve eleve = candidature.getEleve();
            // On rajoute un test si l'�l�ve est du palier seconde, il n'y a pas obligation
            // d'avoir des voeux
            Palier palierOrigine = eleve.getPalierOrigine();
            Etablissement etab = eleve.getEtablissement();
            // Test NullSafe
            if (palierOrigine == null) {
                LOG.error("Le palier d'origine de l'�l�ve INE " + eleve.getIne() + " est vide.");
                return false;
            }
            // R�gle RG-AFF-LYC-VOEUX-VALSAI-04
            if (etab != null && Palier.CODE_PALIER_3EME.equals(palierOrigine.getCode()) && etab.estPublicNonEREA()
                    && (listeVoeux == null || listeVoeux.isEmpty())) {
                return false;
            }

            // On v�rifie pour chacun des voeux
            for (VoeuEleve voeuEleve : listeVoeux) {

                // Probl�me de rangs => les voeux ne sont pas Ok.
                if (voeuEleve == null) {
                    LOG.error("Erreur de rang sur les voeux de l'�l�ve " + eleve.getIne());
                    return false;
                }

                if (!isVoeuOK(voeuEleve, isIaOuRectorat)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Teste si un voeu d'�l�ve est valide.
     *
     * <p>
     * On v�rifie que
     * </p>
     * <ul>
     * <li>le voeu est conforme au palier d'origine</li>
     * <li>le avis sont saisis et qu'ils sont conformes � la DO</li>
     * </ul>
     *
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait du point de vue de l'IA/Rectorat/Admin ?
     *
     * @return vrai si le voeu de l'�l�ve est correct, sinon faux
     */
    public boolean isVoeuOK(VoeuEleve voeuEleve, boolean isIaOuRectorat) {

        return voeuEleve.estValideReservation3Segpa() && isVoeuConformePalierOrigine(voeuEleve)
                && isAvisValide(voeuEleve, isIaOuRectorat);
    }

    /**
     * V�rifie si le voeu est conforme au palier d'origine.
     *
     * Remarque : Le for�age n'est pas pris en compte � ce niveau.
     *
     * @param voeuEleve
     *            le voeu � v�rifier
     * @return vrai si le voeu est conform, sinon faux
     */
    public boolean isVoeuConformePalierOrigine(VoeuEleve voeuEleve) {

        // Le palier origine doit �tre renseign� pour poursuivre le contr�le
        Palier palierOrigine = voeuEleve.getEleve().getPalierOrigine();
        if (palierOrigine == null) {
            return false;
        }

        // L'offre de formation du voeu doit �tre saisissable pour le palier origine
        return voeuEleve.getVoeu().saisissablePourPalier(palierOrigine.getCode());
    }

    /**
     * @param candidature
     *            la candidature de l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait pour l'IA/Rectorat/Admin
     * @return si les avis d'un �l�ve sont saisis
     */
    public boolean isAvisValide(Candidature candidature, boolean isIaOuRectorat) {
        // r�cup�ration de la liste des voeux �l�ve
        List<VoeuEleve> listeVoeux = (candidature != null) ? candidature.getVoeux() : new ArrayList<>();
        // y a t il des voeux ?
        if (listeVoeux.isEmpty()) {
            return false;
        }

        // pour chacun des voeux, on v�rifie si les avis CC et ENT sont saisis
        for (VoeuEleve voeuEleve : listeVoeux) {
            if (!isAvisValide(voeuEleve, isIaOuRectorat)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Teste si les avis d'un voeu d'un �l�ve sont saisis.
     *
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @param isIaOuRectorat
     *            le test est-il fait pour l'IA/Rectorat/Admin
     * @return vrai si les avis sont saisis
     */
    public boolean isAvisValide(VoeuEleve voeuEleve, boolean isIaOuRectorat) {

        Candidature candidature = voeuEleve.getCandidature();

        // si le voeu n'est pas forc� � refus� (IssueId #415) ET l'�l�ve est non forc�
        if (voeuEleve.getFlagRefusVoeu().equals(Flag.NON) && !estCandidatureForcee(candidature)) {

            // Avis du chef d'�tablissement d'origine
            if (voeuEleve.getAvisConseilClasse() == null
                    && voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT,
                            VoeuEleveManager.SAISIE_OBLIGATOIRE, voeuEleve, isIaOuRectorat)) {
                return false;
            }

            // Avis de gestion
            if (voeuEleve.getAvisEntretien() == null && voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_DE_GESTION,
                    VoeuEleveManager.SAISIE_OBLIGATOIRE, voeuEleve, isIaOuRectorat)) {
                return false;
            }

            // L'avis DSDEN n'est jamais obligatoire

            // Avis passerelle
            if (voeuEleve.getAvisPasserelle() == null
                    && voeuEleveManager.modaliteAvis(CodeTypeAvis.AVIS_PASSERELLE,
                            VoeuEleveManager.SAISIE_OBLIGATOIRE, voeuEleve, isIaOuRectorat)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Met � jour, apr�s validation, les flags de l'�l�ve (sans enregistrement BDD).
     *
     * @param candidature
     *            la candidature de l'�l�ve
     */
    public void setFlagsEleve(Candidature candidature) {
        // on v�rifie la pr�sence d'une candidature
        if (candidature != null) {
            // D�finition du flag de validit� des avis
            if (isAvisValide(candidature, false)) {
                candidature.setFlagValidationAvis(Flag.OUI);
            } else {
                candidature.setFlagValidationAvis(Flag.NON);
            }

            conformiteVoeuxDo(candidature.getEleve(), candidature);

            // D�finition du flag de validit� de la saisie
            if (Flag.OUI.equals(candidature.getFlagEleveForce()) || isSaisieValide(candidature, false)) {
                candidature.setFlagSaisieValide(Flag.OUI);
            } else {
                candidature.setFlagSaisieValide(Flag.NON);
            }
        }
    }

    /**
     * M�thode de cr�ation d'un �l�ve.
     *
     * @param eleve
     *            l'�l�ve � ajouter en base de donn�es
     * @return l'�l�ve cr��
     */
    public Eleve creerEleve(Eleve eleve) {
        // Mise � jour de la date de derni�re modification de l'�l�ve
        eleve.setDateMAJ(new Date());

        return eleveDao.creer(eleve);
    }

    /**
     * M�thode de r�cup�ration du nombre de voeux d'un �l�ve pour le tour en cours.
     *
     * @param eleve
     *            un �l�ve
     * @return le nombre de voeux de l'�l�ve sp�cifi�
     */
    public int nombreVoeux(Eleve eleve) {
        Session session = HibernateUtil.currentSession();

        Query requete = DaoUtils.getNamedQuery("sql.select.commun.nombreVoeuxEleve");

        String chaineRequete = requete.getQueryString();

        SQLQuery requeteSQL = session.createSQLQuery(chaineRequete);

        requeteSQL.setString("numeroTour", String.valueOf(campagneManager.getNumeroTourCourant()));

        requeteSQL.setString("ine", eleve.getIne());

        return ((Number) requeteSQL.uniqueResult()).intValue();
    }

    /**
     * M�thode de r�cup�ration des nombres de voeux des �l�ves, pour le tour
     * courant.
     *
     * @return la table des nombre de voeu par INE d'�l�ve
     */

    public Map<String, Integer> nombreVoeuxParEleve() {
        return nombreVoeuxParEleve(campagneManager.getNumeroTourCourant());
    }

    /**
     * M�thode de r�cup�ration des nombres de voeux des �l�ves pour le tour demand�.
     *
     * @param numeroTour
     *            num�ro du tour demand�
     * @return la table des nombre de voeu par INE d'�l�ve
     */
    @SuppressWarnings("unchecked")
    public Map<String, Integer> nombreVoeuxParEleve(short numeroTour) {
        Session session = HibernateUtil.currentSession();

        Query requete = DaoUtils.getNamedQuery("sql.select.commun.nombreVoeuxParEleve");

        String chaineRequete = requete.getQueryString();

        SQLQuery requeteSQL = session.createSQLQuery(chaineRequete);

        requeteSQL.setShort("numeroTour", numeroTour);
        List<Object[]> resultats = requeteSQL.list();

        Map<String, Integer> nombreDeVoeuxParEleve = new HashMap<>();
        for (Object[] ligne : resultats) {
            String ine = (String) ligne[0];
            Integer nombreDeVoeux = ((Number) ligne[1]).intValue();
            nombreDeVoeuxParEleve.put(ine, nombreDeVoeux);
        }

        return nombreDeVoeuxParEleve;
    }

    /**
     * M�thode de r�cup�ration des nombres de voeux des �l�ves filtr�s.
     *
     * @param elevesListes
     *            liste des �l�ves qui ont r�pondu au filtre
     * @return la table des nombre de voeu par INE d'�l�ve
     */
    public Map<String, Long> nombreVoeuxParEleveListe(List<Eleve> elevesListes) {
        return nombreVoeuxParEleveListe(elevesListes, campagneManager.getNumeroTourCourant());
    }

    /**
     * M�thode de r�cup�ration des nombres de voeux des �l�ves filtr�s.
     *
     * @param elevesListes
     *            liste des �l�ves qui ont r�pondu au filtre
     * @param numeroTour
     *            le numero du tour
     * @return la table des nombre de voeu par INE d'�l�ve
     */
    @SuppressWarnings("unchecked")
    public Map<String, Long> nombreVoeuxParEleveListe(List<Eleve> elevesListes, short numeroTour) {
        Map<String, Long> nombreDeVoeuxParEleveListe = new HashMap<>();
        if (elevesListes != null && !elevesListes.isEmpty()) {
            List<String> listesINE = new ArrayList<>();

            // Parcours de la liste des �l�ves pour avoir uniquement l'identifiant de
            // l'�l�ve
            for (Eleve eleve : elevesListes) {
                listesINE.add(eleve.getIne());
            }

            Query requete = DaoUtils.getNamedQuery("hql.select.commun.nombreVoeuxParEleveListe");

            requete.setParameterList("listeEleve", listesINE);
            requete.setShort("numeroTour", numeroTour);

            List<Object[]> resultats = requete.list();

            for (Object[] ligne : resultats) {
                String ine = (String) ligne[0];
                Long nombreDeVoeux = ((Number) ligne[1]).longValue();
                nombreDeVoeuxParEleveListe.put(ine, nombreDeVoeux);
            }
        }
        return nombreDeVoeuxParEleveListe;
    }

    /**
     * R�cuperer les d�partements des voeux de rang pour une liste d'�l�ves
     *
     * @param eleves
     * @param tour
     * @param rang
     * @return Mapping<ine - d�partement> du voeu au rang et tour demand�
     */
    public Map<String, String> departementDuVoeuAuRang(List<Eleve> eleves, Short tour, Integer rang) {
        List<String> listesINE = new ArrayList<>();
        for (Eleve eleve : eleves) {
            listesINE.add(eleve.getIne());
        }
        Map<String, String> mapping = new HashMap<>();
        Query requete = DaoUtils.getNamedQuery("hql.select.departementDuVoeuParRang");
        requete.setParameterList("listeEleve", listesINE);
        requete.setShort("numeroTour", tour);
        requete.setInteger("rang", rang);
        List<Object[]> resultat = requete.list();
        for (Object[] ineDep : resultat) {
            mapping.put((String) ineDep[0], (String) ineDep[1]);
        }
        return mapping;
    }

    /**
     * M�thode de construction du filtre de s�lection des �l�ves du tour en cours ne fonction de la classe et de
     * l'�tablissement.
     *
     * @param classe
     *            Classe.
     * @param idEtablissement
     *            Etablissement.
     * @return Une liste de filtres.
     */
    public List<Filtre> filtresElevesDuTour(String classe, String idEtablissement) {
        List<Filtre> filtres = new ArrayList<>();

        filtres.add(Filtre.equal("classe", classe));
        filtres.add(Filtre.equal("etablissement.id", idEtablissement));

        if (campagneManager.isToursSuivants()) {
            filtres.add(filtresElevesDuTour());
        }

        return filtres;
    }

    /**
     * M�thode de construction du filtre de s�lection des �l�ves du tour en cours.
     *
     * @return le filtre de s�lection des �l�ves du tour en cours
     */
    public Filtre filtresElevesDuTour() {

        short numeroTourCourant = campagneManager.getNumeroTourCourant();

        if (numeroTourCourant == 0) {
            // Filtre neutre permettant d'obtenir la liste compl�te des �l�ves en tour
            // principal
            return Filtre.trueValue();
        }

        // Pour un tour suivant, on retient
        // d'une part, les �l�ves qui ont particip� au tour pr�c�dent et n'ont pas �t� (affect�s ou que recens�s)
        // d'autre part, les �l�ves ayant des voeux dans le tour courant
        short numeroTourPrecedent = (short) (numeroTourCourant - 1);

        Filtre nonAffectesTourPrecedent = Filtre.hql("o.ine in (select candidature.id.ine"
                + " from Candidature candidature where candidature.id.numeroTour = " + numeroTourPrecedent
                + " and candidature.codeDecisionFinale not in (" + DecisionFinale.AFFECTE.getCode() + ", "
                + DecisionFinale.RECENSEMENT.getCode() + "))");

        Filtre candidatsTourSuivant = Filtre
                .hql("exists (select ve.id.ine from VoeuEleve ve where ve.id.ine = o.ine and ve.id.numeroTour = "
                        + numeroTourCourant + ")");

        return Filtre.or(nonAffectesTourPrecedent, candidatsTourSuivant);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves
     * forc�s.
     *
     * @return un filtre s�lectionnant les �l�ves forc�s
     */
    public Filtre filtreElevesForces() {
        return filtreElevesSelonForcage(true);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves non
     * forc�s.
     *
     * @return un filtre s�lectionnant les �l�ves non forc�s
     */
    public Filtre filtreElevesNonForces() {
        return filtreElevesSelonForcage(false);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves selon leur �tat de for�age.
     *
     * @param conditionForcage
     *            vrai pour s�lectionner les �l�ves forc�s, faux pour les �l�ves non
     *            forc�s
     * @return un filtre s�lectionnant les �l�ves selon le crit�re de for�age
     *         sp�cifi�
     */
    private Filtre filtreElevesSelonForcage(Boolean conditionForcage) {
        String negation = conditionForcage ? "" : "not ";
        return Filtre
                .hql(negation + "exists (select 1 from Candidature candidature where candidature.id.ine = o.ine"
                        + " and candidature.id.numeroTour = '" + campagneManager.getNumeroTourCourant() + "'"
                        + " and candidature.flagEleveForce = '" + Flag.OUI + "')");
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * les avis sont valides.
     *
     * @return un filtre s�lectionnant les �l�ves dont les avis sont valides
     */
    public Filtre filtreElevesAvisValides() {
        return filtreElevesSelonValiditeAvis(true);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * les avis sont manquants.
     *
     * @return un filtre s�lectionnant les �l�ves dont les avis sont manquants
     */
    public Filtre filtreElevesAvisManquants() {
        return filtreElevesSelonValiditeAvis(false);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves selon la validit� de leurs
     * avis.
     *
     * @param conditionValiditeAvis
     *            vrai pour s�lectionner les �l�ves dont les avis sont valides, faux
     *            pour s�lectionner les �l�ves dont les avis ne sont pas valides
     * @return un filtre s�lectionnant les �l�ves selon la validit� des avis
     */
    private Filtre filtreElevesSelonValiditeAvis(Boolean conditionValiditeAvis) {
        String negation = conditionValiditeAvis ? "not " : "";
        return Filtre
                .hql(negation + "exists (select 1 from Candidature candidature where candidature.id.ine = o.ine"
                        + " and candidature.id.numeroTour = '" + campagneManager.getNumeroTourCourant() + "'"
                        + " and candidature.flagValidationAvis = '" + Flag.NON + "')");
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * la saisie est valide.
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie est valide
     */
    public Filtre filtreElevesSaisieValide() {
        return filtreElevesSelonValiditeSaisie(true);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * la saisie est valide en phase de pr�-int�gration finale des voeux saisis via le t�l�service affectation.
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie est valide en phase de pr�-int�gration finale des
     *         voeux saisis via le t�l�service affectation.
     */
    public Filtre filtreElevesSaisieValidePreIntegFinaleVoeuxTeleservice() {
        Filtre filtreElevesSelonValiditeSaisie = filtreElevesSelonValiditeSaisie(true);
        Filtre filtreEleveSansVoeuTeleservice = filtreEleveSansVoeuTeleservice();
        return Filtre.and(filtreElevesSelonValiditeSaisie, filtreEleveSansVoeuTeleservice);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * la saisie est incomplete. On nuance par rapport au �l�ve dont la candidature
     * est manquante
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie n'est pas valide
     */
    public Filtre filtreElevesSaisieIncomplete() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.and(filtreElevesSelonValiditeSaisie(false), filtreEleveAvecCandidature(true)));
        return Filtre.and(filtres);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves avec
     * le palier d'origine.
     * @param codePalier Le palie de l'�l�ve
     * @return un filtre s�lectionnant les �l�ves avec le palier d'origine
     */
    public Filtre filtreElevesPalierOrigine(Short codePalier) {
        return Filtre.equal("palierOrigine.code", codePalier);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * la saisie n'est pas valide et dont la candidature est n�cessaire.
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie n'est pas valide
     */
    public Filtre filtreElevesSaisieInvalide() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.and(filtreElevesSelonValiditeSaisie(false), filtreEleveAvecCandidature(true)));
        filtres.add(Filtre.and(filtreEleveAvecCandidature(false), filtreEleveCandidatureNecessaire()));
        return Filtre.or(filtres);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves sur l'existance de leur
     * candidature pour le tour courant.
     *
     * @param conditionAvecCandidature
     *            vrai pour s�lectionner les �l�ves avec une candidature
     * @return un filtre s�lectionnant les �l�ves avec une candidature pour le
     *         filtre courant
     */
    public Filtre filtreEleveAvecCandidature(Boolean conditionAvecCandidature) {
        String negation = conditionAvecCandidature ? "" : "not ";
        return Filtre
                .hql(negation + "exists (select 1 from Candidature candidature where candidature.id.ine = o.ine"
                        + " and candidature.id.numeroTour = '" + campagneManager.getNumeroTourCourant() + "')");
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves selon la validit� de leur
     * saisie.
     *
     * @param conditionValiditeSaisie
     *            vrai pour s�lectionner les �l�ves dont la saisie est valide, faux
     *            pour s�lectionner les �l�ves dont la saisie n'est pas valide
     * @return un filtre s�lectionnant les �l�ves selon la validit� de leur saisie
     */
    public Filtre filtreElevesSelonValiditeSaisie(Boolean conditionValiditeSaisie) {
        String negation = conditionValiditeSaisie ? "" : "not ";
        return Filtre
                .hql(negation + "exists (select 1 from Candidature candidature where candidature.id.ine = o.ine"
                        + " and candidature.id.numeroTour = '" + campagneManager.getNumeroTourCourant() + "'"
                        + " and candidature.flagSaisieValide = '" + Flag.OUI + "')");
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves selon leur d�cision finale.
     *
     * @param decisionFinale
     *            la d�cision finale recherch�e
     * @return un filtre s�lectionnant les �l�ves selon leur d�cision finale
     */
    public Filtre filtreElevesSelonDecisionFinale(DecisionFinale decisionFinale) {
        return Filtre.hql("exists (select 1 from Candidature candidature where candidature.id.ine = o.ine"
                + " and candidature.id.numeroTour = '" + campagneManager.getNumeroTourCourant() + "'"
                + " and candidature.codeDecisionFinale = '" + decisionFinale.getCode() + "')");
    }

    /**
     * M�thode de cr�ation d'un filtre s�lectionnant les �l�ves ayant formul� des
     * voeux dans le tour en cours.
     *
     * @return un filtre s�lectionnant les �l�ves ayant formul� des voeux dans le
     *         tour en cours
     */
    public Filtre filtreEleveAyantDesVoeux() {
        return filtreEleveAyantDesVoeux(campagneManager.getNumeroTourCourant());
    }

    /**
     * Cr�e un filtre s�lectionnant les �l�ves ayant formul� des voeux dans le tour
     * demand�.
     *
     * @param numeroTour
     *            le num�ro du tour demand�
     * @return un filtre s�lectionnant les �l�ves ayant formul� des voeux dans le
     *         tour demand�
     */
    public Filtre filtreEleveAyantDesVoeux(short numeroTour) {
        return Filtre.hql("exists (select 1 from VoeuEleve voeu where voeu.id.ine = o.ine"
                + " and voeu.id.numeroTour = '" + numeroTour + "')");
    }

    /**
     * Cr�e un filtre s�lectionnant les �l�ves ayant formul� des voeux au sein du t�l�service affectation.
     *
     * @return un filtre s�lectionnant les �l�ves ayant formul� des voeux dans le
     *         t�l�service affectation.
     */
    public Filtre filtreEleveAyantDesVoeuxTeleservice() {
        return Filtre.exists(VoeuTeleservice.class, Filtre.eqPropertiesPere("eleve.ine", "eleve.ine"));
    }

    /**
     * M�thode de cr�ation d'un filtre s�lectionnant les �l�ves sans voeu dans le
     * tour en cours.
     *
     * @return un filtre s�lectionnant les �l�ves sans voeu dans le tour en cours
     */
    public Filtre filtreEleveSansVoeu() {
        return filtreEleveSansVoeu(campagneManager.getNumeroTourCourant());
    }

    /**
     * M�thode de cr�ation d'un filtre s�lectionnant les �l�ves sans voeu (aucun voeu affelnet et aucun voeu
     * t�l�service).
     *
     * @return un filtre s�lectionnant les �l�ves sans voeu (affelnet et t�l�service).
     */
    public Filtre filtreEleveSansVoeuAffelnetEtTeleservice() {
        Filtre filtreEleveSansVoeu = filtreEleveSansVoeu((short) 0);
        Filtre filtreEleveSansVoeuTeleservice = filtreEleveSansVoeuTeleservice();
        return Filtre.and(filtreEleveSansVoeu, filtreEleveSansVoeuTeleservice);
    }

    /**
     * M�thode de cr�ation d'un filtre s�lectionnant les �l�ves sans voeu formul� dans le t�l�service affectation.
     *
     * @return un filtre s�lectionnant les �l�ves sans voeu formul� dans le t�l�service affectation.
     */
    public Filtre filtreEleveSansVoeuTeleservice() {
        return Filtre.not(filtreEleveAyantDesVoeuxTeleservice());
    }

    /**
     * Cr�e d'un filtre s�lectionnant les �l�ves sans voeu dans le tour demand�.
     *
     * @param numeroTour
     *            le num�ro du tour demand�
     * @return un filtre s�lectionnant les �l�ves sans voeu dans le tour demand�
     */
    public Filtre filtreEleveSansVoeu(short numeroTour) {
        return Filtre.hql("not exists (select 1 from VoeuEleve voeu where voeu.id.ine = o.ine"
                + " and voeu.id.numeroTour = '" + numeroTour + "')");
    }

    /**
     * @return filtre sur les �l�ves issus d'une saisie individuelle (administration
     *         ou saisie simplifi�e).
     */
    public static Filtre getFiltreSaisieIndividuelle() {
        return Filtre.equal("flagSaisieIndividuelle", Flag.OUI);
    }

    /**
     * M�thode de calcul de la moyenne des �l�ves disposant d'au moins une note
     * saisie.
     */
    private void calculDesMoyennesDesEleves() {
        Session session = HibernateUtil.currentSession();
        Query calculDesMoyennes = session.getNamedQuery("calculDesMoyennesEleves");
        calculDesMoyennes.executeUpdate();
    }

    /**
     * M�thode de r�initialisation de la moyenne des �l�ves ne disposant d'aucune
     * note saisie.
     */
    private void reinitilisationDesMoyennesDesEleves() {
        Session session = HibernateUtil.currentSession();
        Query calculDesMoyennes = session.getNamedQuery("reinitilisationDesMoyennesEleves");
        calculDesMoyennes.executeUpdate();
    }

    /**
     * M�thode de mise � jour des moyennes pour tous les �l�ves.
     */
    public void miseAJourMoyennesEleves() {
        // Calcul des moyennes pour les �l�ves ayant des notes
        calculDesMoyennesDesEleves();

        // Mise de la moyenne � null pour les �l�ves n'ayant aucune note
        reinitilisationDesMoyennesDesEleves();
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves s�lectionnant les �l�ves dont
     * la saisie est incompl�te mais pas candidat.
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie est compl�te mais
     *         pas candidat.
     */
    public Filtre filtreEleveNonCandidat() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreEleveSansVoeu());
        filtres.add(Filtre.not(filtreEleveCandidatureNecessaire()));
        return Filtre.and(filtres);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves n�cessitant une candidature.
     *
     * @return un filtre s�lectionnant les �l�ves n�cessitant une candidature.
     */
    public Filtre filtreEleveCandidatureNecessaire() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreEleveEtablissementPublic());
        filtres.add(Filtre.not(filtreEleveEtablissementEREA()));
        filtres.add(Filtre.equal("palierOrigine.code", Palier.CODE_PALIER_3EME));
        filtres.add(Filtre.isNotNull("palierOrigine.code"));
        return Filtre.and(filtres);
    }

    /**
     * Filtre ajoutant la condition public pour l'�tablissement de l'�l�ve.
     *
     * @return un filtre sur les �tablissements publics
     */
    public Filtre filtreEleveEtablissementPublic() {
        return Filtre.equal("etablissement.codeSecteur", Etablissement.SECTEUR_PUBLIC);
    }

    /**
     * Filtre ajoutant la condition de type EREA pour l'�tablissement de l'�l�ve.
     *
     * @return un filtre sur les �tablissements de type EREA
     */
    public Filtre filtreEleveEtablissementEREA() {
        return Filtre.equal("etablissement.typeEtablissement.code", TypeEtablissement.TYPE_ETABLISSEMENT_EREA);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves ayant besoin d'une
     * candidature.
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie n'est pas compl�te
     *         dans le sens o� il leur manque un voeu alors que dans leur cas il est
     *         obligatoire.
     *
     */
    public Filtre filtreEleveCandidatureManquante() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreEleveSansVoeu());
        filtres.add(filtreEleveCandidatureNecessaire());
        return Filtre.and(filtres);
    }

    /**
     * M�thode de cr�ation d'un filtre sur les �l�ves ayant besoin d'une
     * candidature aant la phase d'int�gration finale des voeux issus du t�l�service.
     *
     * @return un filtre s�lectionnant les �l�ves dont la saisie n'est pas compl�te
     *         dans le sens o� il leur manque un voeu (aussi bien c�t� affelnet que t�l�service) alors que dans
     *         leur cas il est obligatoire.
     *
     */
    public Filtre filtreEleveCandidatureManquantePreIntegFinaleVoeuxTeleservice() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreEleveSansVoeuAffelnetEtTeleservice());
        filtres.add(filtreEleveCandidatureNecessaire());
        return Filtre.and(filtres);
    }

    /**
     * M�thode permettant de d�terminer l'�tat des saisies des �l�ves du point de
     * vue de l'�tablissement.
     *
     * @param eleve
     *            l'objet Eleve
     * @param nbVoeux
     *            le nombre de voeux
     * @return l'�tat saisie des �l�ves
     */
    public EtatSaisieEleve determinationEtatSaisie(Eleve eleve, Long nbVoeux) {
        // �tat de la saisie
        EtatSaisieEleve etat = null;

        if (nbVoeux == null || nbVoeux == 0) {
            // on v�rifie si il est possible que la candidature soit manquante
            Etablissement etab = eleve.getEtablissement();
            Palier palierOrigine = eleve.getPalierOrigine();

            if (palierOrigine == null) {

                // Le palier �tant obligatoire en saisie, l'�l�ve provient d'une s�lection
                // dont l'attribution des paliers n'a pas trouv� de palier associ� au MefStat
                // c'est donc un cas non "standard" et on n'oblige pas la pr�sence d'une
                // candidature.
                etat = EtatSaisieEleve.NON_CONCERNE;
            } else {

                Short codePalierEleve = palierOrigine.getCode();
                if (etab != null && etab.estPublicNonEREA() && Palier.CODE_PALIER_3EME.equals(codePalierEleve)) {
                    etat = EtatSaisieEleve.CANDIDATURE_MANQUANTE;
                } else {
                    etat = EtatSaisieEleve.NON_CONCERNE;
                }

            }

        } else {
            // R�cup�ration de la candidature courante
            Candidature candidatureCourante = getCandidatureCourante(eleve);

            if (candidatureCourante != null && Flag.OUI.equals(candidatureCourante.getFlagSaisieValide())) {
                etat = EtatSaisieEleve.TERMINE;
            } else {
                etat = EtatSaisieEleve.A_COMPLETER;
            }
        }
        return etat;
    }

    /**
     * M�thode de r�cup�ration de l'ensemble des �l�ves de palier 3�me d'un
     * �tablissement.
     *
     * @param idEtablissement
     *            id de l'�tablissement
     * @return un r�sultat scrollable contenant tous les �l�ves de palier 3�me de
     *         l'�tablissement
     */
    public ScrollableResults scrollElevePalier3emePourEtab(String idEtablissement) {
        return eleveDao.scrollElevePalier3emePourEtab(idEtablissement);
    }

    /**
     * M�thode fournissant la liste des �l�ves respectant les conditions pour que leurs informations soient envoy�s
     * au portail national d'affectation.
     *
     * @param filtre
     *            filtre supplementaire
     * @return l'ensemble des �l�ves dont on envoie les informations au portail
     */
    public Collecteur<Eleve> collecteurElevesEnvoyesTsa(Filtre filtre) {
        return new CollecteurScrollableResultSet<>(eleveDao.scroll(filtresElevesEnvoyesTsa(filtre), null, null,
                null, fetchJoinsElevesEnvoyesTsa(null), true));
    }

    /**
     * Cr�er une liste de filtres pour la recherche des �l�ves envoy�s au TSA.
     *
     * @param filtre
     *            le filtre additionnel
     * @return une liste de filtres.
     */
    public List<Filtre> filtresElevesEnvoyesTsa(Filtre filtre) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreEleveAEnvoyerTsa());
        if (filtre != null) {
            filtres.add(filtre);
        }
        return filtres;
    }

    /**
     * Cr�er une liste de fetchJoin pour la recherche des �l�ves envoy�s au TSA.
     *
     * @return une liste de fetchJoin.
     */
    public List<String> fetchJoinsElevesEnvoyesTsa(List<String> fetchJoinAdditionnels) {
        List<String> fetchJoin = new ArrayList<>();
        fetchJoin.add("adresseResidence.codePostal.commune");
        fetchJoin.add("adresseResidence.pays");
        fetchJoin.add("responsables.adresseResidence.codePostal.commune");
        fetchJoin.add("responsables.adresseResidence.pays");
        fetchJoin.add("responsables.lienEleveResponsable");
        fetchJoin.add("responsables.niveauResponsabilite");
        fetchJoin.add("responsables.categorieSocioProfessionnelle");
        fetchJoin.add("voeuxTeleservice");
        if (fetchJoinAdditionnels != null) {
            fetchJoin.addAll(fetchJoinAdditionnels);
        }

        return fetchJoin;
    }

    /**
     * Cr�er une liste de fetchJoin additionnels sur l'�tablissement pour la recherche des �l�ves envoy�s au TSA.
     *
     * @return une liste de fetchJoin.
     */
    public List<String> fetchJoinsEtablissementElevesEnvoyesTsa() {
        List<String> fetchJoin = new ArrayList<>();
        fetchJoin.add("etablissement.typeEtablissement");
        fetchJoin.add("etablissement.denominationComplementaire");
        fetchJoin.add("etablissement.ville");
        fetchJoin.add("etablissementNational.id");
        fetchJoin.add("departement.codeMen");
        return fetchJoin;
    }

    /**
     * M�thode fournissant la liste des �l�ves respectant les conditions pour que leurs informations soient envoy�s
     * au portail national d'affectation.
     *
     * @param filtre
     *            filtre suppl�mentaire
     * @param fetchJoinAdditionnels
     *            une liste de fetchJoin additionnels
     * @return l'ensemble des �l�ves dont on envoie les informations au portail
     */
    public Collecteur<Eleve> collecteurElevesEnvoyesTsa(Filtre filtre, List<String> fetchJoinAdditionnels) {
        return new CollecteurScrollableResultSet<>(eleveDao.scroll(filtresElevesEnvoyesTsa(filtre), null, null,
                null, fetchJoinsElevesEnvoyesTsa(fetchJoinAdditionnels), true));

    }

    /**
     * Pr�pare l'envoi de notifications aux responsables l�gaux en cas de modification du dossier �l�ve (certains
     * champs seulement).
     *
     * @param ancienDossierEleve
     *            Donn�es du dossier �l�ve avant modifications.
     * @param eleveMaj
     *            Donn�es du dossier �l�ve apr�s modifications.
     */
    public void preparerEnvoiNotificationsResponsablesSiNecessaire(EleveSuiviCandidatureDto ancienDossierEleve,
            Eleve eleveMaj) {
        if (!voeuTeleserviceManager.isPostIntegrationFinaleVoeuxTeleservice()) {
            return;
        }
        if (eleveMaj.getVoeuxTeleservice().isEmpty()) {
            return;
        }

        // On envoie les mails uniquement pendant le tour principal et avant que le tour ne soit enregistr�
        boolean tourPrincipalNonEnregistre = TypeTour
                .typeTourPourValeur(CampagneAffectationManager.NUMERO_TOUR_PRINCIPAL)
                .equals(TypeTour.typeTourPourValeur(campagneManager.getNumeroTourCourant()))
                && !campagneManager.estTourCourantEnregistre();
        if (!tourPrincipalNonEnregistre) {
            return;
        }

        // R�cup�re les �ventuels envois de modification en attente
        List<EnvoiModifResponsable> envoisModifResp = envoiModifResponsableManager
                .listerEnvoisModifsRepresentantsLegauxNonEnvoyes(eleveMaj);

        // Prend en r�f�rence le dossier de l'�l�ve archiv� lors de la 1ere d�tection de modification (= premier
        // envoi de modification en attente) trouv�, ou � d�faut le dossier de l'�l�ve juste avant la modification
        EleveSuiviCandidatureDto dossierEleveRef = null;
        boolean hasEnvoiModifResp = !envoisModifResp.isEmpty();
        if (hasEnvoiModifResp) {
            dossierEleveRef = eleveSuiviCandidatureMapper
                    .envoiModifResponsableToEleveSuiviCandidatureDto(envoisModifResp.get(0));
        } else {
            dossierEleveRef = ancienDossierEleve;
        }

        // D�termine les crit�res notifiants existants pour cet �l�ve
        CriteresNotifiantsDetectesDto criteresNotifiantsDetectes = checkModificationSaisieVoeuxEleve(
                dossierEleveRef, eleveMaj);

        // Si des crit�res notifiants sont trouv�s, on s'assure d'avoir l'envoi de modif aux responsables l�gaux.
        // Le cas contraire, on supprime les envois de modif aux responsables l�gaux en attente car le mail n'est
        // plus n�cessaire
        if (criteresNotifiantsDetectes.isNotEmpty() && !hasEnvoiModifResp) {
            envoiModifResponsableManager.creerLigneEnvoiNotificationsRepresentantsLegaux(eleveMaj,
                    ancienDossierEleve);
        } else if (criteresNotifiantsDetectes.isEmpty() && hasEnvoiModifResp) {
            envoiModifResponsableManager.supprimerEnvoiModifsResponsableNonEnvoyes(eleveMaj.getIne());
        }
    }

    /**
     * Fournit les responsables l�gaux de l'�l�ve.
     *
     * @param eleve
     *            L'�l�ve.
     * @return La liste des responsables l�gaux de l'�l�ve.
     */
    public List<Responsable> getResponsablesLegaux(Eleve eleve) {
        return eleve.getResponsables().stream().filter(Responsable::estRepresentantLegal)
                .collect(Collectors.toList());
    }

    /**
     * On regarde si les voeux de l'�l�ve ont �t� modifi�s.
     *
     * @param ancienDossierEleve
     *            Anciennes valeurs du dossier �l�ve.
     * @param eleveMaj
     *            El�ve mis � jour.
     * @return Crit�res notifiants d�tect�s
     */
    protected CriteresNotifiantsDetectesDto checkModificationSaisieVoeuxEleve(
            EleveSuiviCandidatureDto ancienDossierEleve, Eleve eleveMaj) {

        EleveSuiviCandidatureDto nouveauDossierEleve = eleveSuiviCandidatureMapper
                .eleveToEleveSuiviCandidatureDto(eleveMaj);

        CriteresNotifiantsDetectesDto criteresNotifiantsDetectes = new CriteresNotifiantsDetectesDto();
        List<VoeuSuiviCandidatureDto> ancienDossierEleveVoeux = ancienDossierEleve.getVoeux();
        List<VoeuSuiviCandidatureDto> nouveauDossierEleveVoeux = nouveauDossierEleve.getVoeux();

        // On cr�� une liste pour stocker les voeux conserv�s entre l'ancienne et la nouvelle version
        // du dossier �l�ve. Ce sont sur ces voeux que l'on v�rifiera l'ordre d'apparition.
        // Cette pirouette est n�cessaire car ajouter ou supprimer un voeu modifie le rang des voeux
        // qui le suivent.
        //
        // Exemple de changements :
        // - Avant : A (rang #1), B (rang #2), C (rang #3), D (rang #4)
        // - Apr�s : A (rang #1), C (rang #2), D (rang #3)
        // On voit que C et D ont chang� de rang � cause de la suppression de B.
        // Pourtant l'ordre des voeux est inchang�
        List<String> voeuxConserves = new ArrayList<>();

        for (VoeuSuiviCandidatureDto ancienVoeu : ancienDossierEleveVoeux) {
            // Recherche le nouveau voeu correspondant par son code
            VoeuSuiviCandidatureDto nouveauVoeu = nouveauDossierEleveVoeux.stream()
                    .filter(curVoeu -> curVoeu.getCode().equalsIgnoreCase(ancienVoeu.getCode())).findFirst()
                    .orElse(null);

            // Non trouv� = voeu supprim�
            if (nouveauVoeu == null) {
                criteresNotifiantsDetectes.ajouterCritereNotifiant(ancienVoeu.getCode(),
                        CritereNotifiantEnum.VOEU_SUPPRIMER);
                continue;
            }

            // Le voeu est conserv� dans la nouvelle version, on le stocke
            voeuxConserves.add(ancienVoeu.getCode());

            // Changement de demande d�rogation
            if (!ancienVoeu.getFlagVoeuDerogation().equals(nouveauVoeu.getFlagVoeuDerogation())) {
                if (Flag.OUI.equalsIgnoreCase(nouveauVoeu.getFlagVoeuDerogation())) {
                    criteresNotifiantsDetectes.ajouterCritereNotifiant(ancienVoeu.getCode(),
                            CritereNotifiantEnum.DEROGATION_AJOUTER);
                } else {
                    criteresNotifiantsDetectes.ajouterCritereNotifiant(ancienVoeu.getCode(),
                            CritereNotifiantEnum.DEROGATION_SUPPRIMER);
                }
            }

            // Changement de demande d'internet
            if (!ancienVoeu.getFlagInternatDemande().equals(nouveauVoeu.getFlagInternatDemande())) {
                if (Flag.OUI.equalsIgnoreCase(nouveauVoeu.getFlagInternatDemande())) {
                    criteresNotifiantsDetectes.ajouterCritereNotifiant(ancienVoeu.getCode(),
                            CritereNotifiantEnum.INTERNAT_AJOUTER);
                } else {
                    criteresNotifiantsDetectes.ajouterCritereNotifiant(ancienVoeu.getCode(),
                            CritereNotifiantEnum.INTERNAT_SUPPRIMER);
                }
            }
        }

        // Recherche les voeux cr��s
        for (VoeuSuiviCandidatureDto nouveauVoeu : nouveauDossierEleveVoeux) {
            // Recherche l'ancien voeu correspondant par son code. Non trouv� = nouveau voeu
            VoeuSuiviCandidatureDto ancienVoeu = ancienDossierEleveVoeux.stream()
                    .filter(curVoeu -> curVoeu.getCode() != null
                            && curVoeu.getCode().equalsIgnoreCase(nouveauVoeu.getCode()))
                    .findFirst().orElse(null);
            if (ancienVoeu == null) {
                criteresNotifiantsDetectes.ajouterCritereNotifiant(nouveauVoeu.getCode(),
                        CritereNotifiantEnum.VOEU_AJOUTER);
            }
        }

        // D�tecte les diff�rences d'ordre des voeux conserv�s
        List<String> ordreAnciensVoeuxConserves = ancienDossierEleveVoeux.stream()
                .filter(curVoeu -> voeuxConserves.contains(curVoeu.getCode()))
                .map(VoeuSuiviCandidatureDto::getCode).collect(Collectors.toList());
        List<String> ordreNouveauxVoeuxConserves = nouveauDossierEleveVoeux.stream()
                .filter(curVoeu -> voeuxConserves.contains(curVoeu.getCode()))
                .map(VoeuSuiviCandidatureDto::getCode).collect(Collectors.toList());
        for (String curVoeu : ordreNouveauxVoeuxConserves) {
            if (ordreAnciensVoeuxConserves.indexOf(curVoeu) != ordreNouveauxVoeuxConserves.indexOf(curVoeu)) {
                criteresNotifiantsDetectes.ajouterCritereNotifiant(curVoeu, CritereNotifiantEnum.VOEU_DEPLACER);
            }
        }

        return criteresNotifiantsDetectes;
    }

    /**
     * Le filtre correspondant aux �l�ves dont on doit envoyer les informations au TSA.
     *
     * @return le filtre des �l�ves � envoyer au TSA.
     */
    public Filtre filtreEleveAEnvoyerTsa() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("palierOrigine.code", Palier.CODE_PALIER_3EME));
        filtres.add(Filtre.isNotNull("etablissement"));
        filtres.add(Filtre.isNotNull("eleveIdSiecle"));

        return Filtre.and(filtres);
    }

    /**
     * Indique si l'�l�ve est �ligible au T�l�service Affectation.
     * Pour cela, l'�l�ve doit :
     * - �tre de palier 3�me
     * - avoir un �tablissement acad�mique
     * - avoir un ID issus de Siecle
     *
     * @param eleve
     *            l'�l�ve dont on souhaite v�rifier si il est �ligible
     * @return true si l'�l�ve peut �tre envoy� au TSA
     */
    public boolean estEligibleTsa(Eleve eleve) {
        return eleve != null && eleve.getPalierOrigine() != null
                && Palier.CODE_PALIER_3EME == eleve.getPalierOrigine().getCode()
                && eleve.getEleveIdSiecle() != null && eleve.getEtablissement() != null;
    }

    /**
     * Renvoie le nombre d'�l�ves qui seront envoy�s au TSA.
     *
     * @param filtre
     *            Filtre compl�mentaires.
     *
     * @return le nombre d'�l�ves envoy�s au T�l�service Affectation en fonction des filtres utilis�s.
     */
    public int getNombreElevesEnvoiTSA(Filtre filtre) {
        List<Filtre> filtres = new ArrayList<>();

        if (filtre != null) {
            filtres.add(filtre);
        }

        filtres.add(filtreEleveAEnvoyerTsa());
        return eleveDao.getNombreElements(filtres);
    }

    /**
     * Initialisation du statut de la zone g�ographique � 'NON_TRAITE' pour l'ensemble des �l�ves du palier 3�me
     * pour lesquels le statut de la zone g�ographique est diff�rent de 'MANUEL'.
     */
    public void reinitialiserStatutZoneGeoPalier3eme() {
        eleveDao.reinitialiserStatutZoneGeoPalier3eme();
    }

    /**
     * Attribuer la zone g�ographique des �l�ves du palier 3�me faisant partie de la commune sp�cifi�e dans le
     * tron�on de la carte scolaire fournit.
     *
     * @param tronconCarteScolaire
     *            Tron�on de la carte scolaire.
     */
    public void attribuerZoneGeoElevesMemeCommuneTroncon(TronconCarteScolaire tronconCarteScolaire) {
        try {
            eleveDao.specifierZoneGeoElevesMemeCommuneTroncon(tronconCarteScolaire);
        } catch (Exception e) {
            LOG.error(
                    "Une erreur s'est produite lors de l'appel a la methode specifierZoneGeoElevesMemeCommuneTroncon",
                    e);
        }
    }

    /**
     * Retourne un collecteur listant les �l�ves de palier 3�me pour lesquels les adresses ont �t� redress�es.
     *
     * @return Un collecteur listant les �l�ves de palier 3�me pour lesquels les adresses ont �t� redress�es.
     */
    public Collecteur<Eleve> scrollEleves3emeAdresseRedressee() {
        try {
            return eleveDao.scrollEleves3emeAcaAdresseRedressee();
        } catch (Exception e) {
            LOG.error("Une erreur s'est produite lors de l'appel a la methode scrollEleves3emeAdresseRedressee",
                    e);
            return null;
        }
    }

    /**
     * Retourne le nombre d'�l�ve du ayant pour statut de d�termination automatique de la zone g�ographique celui
     * fournit en entr�e.
     *
     * @param statutZoneGeo
     *            Le statut de la zone g�ographique.
     * @return Le nombre d'�l�ve du ayant pour statut de d�termination automatique de la zone g�ographique celui
     *         fournit en entr�e.
     */
    public int nombreElevesPalier3emeParStatutZoneGeo(StatutZoneGeoEnum statutZoneGeo) {
        return eleveDao.nombreElevesPalier3emeParStatutZoneGeo(statutZoneGeo);
    }

    /**
     * Retourne le nombre d'�l�ve ayant une adresse pour laquelle aucune correspondance n'a �t� trouv�e dans la
     * carte scolaire.
     *
     * @return Le nombre d'�l�ve ayant une adresse pour laquelle aucune correspondance n'a �t� trouv�e dans la
     *         carte scolaire.
     */
    public int nombreElevesAdresseInvalide() {
        return eleveDao.nombreElevesAdresseInvalide();
    }

    /**
     * Retourne les �l�ves dont la zone g�ographique n'a pas �t� d�termin�e automatiquement.
     *
     * @return �l�ves dont la zone g�ographique n'a pas �t� d�termin�e automatiquement.
     */
    public List<Eleve> elevesSansZoneGeoDetermineeAuto() {
        return eleveDao.listerElevesSansZoneGeoDetermineeAuto();
    }

    /**
     * D�termine la zone g�ographique de l'�l�ve.
     *
     * @param eleve
     *            L'�l�ve pour lequel recalculer la zone g�ographique.
     */
    public void determinerZoneGeo(Eleve eleve) {
        // Il faut que l'eleve soit de l'academie et du palier 3eme
        if (eleve != null && eleve.getAdresseResidence() != null && Flag.NON.equals(eleve.getFlagHorsAcademie())
                && eleve.getPalierOrigine() != null
                && Palier.CODE_PALIER_3EME.equals(eleve.getPalierOrigine().getCode())) {

            // Il faut que la carte scolaire ait ete importee
            int nombreElementsCarteScolaire = carteScolaireManager.getNombreElements();
            if (nombreElementsCarteScolaire > 0) {
                Adresse adresseResidence = eleve.getAdresseResidence();
                // Il faut egalement que l'adresse soit validee, c'est-a-dire redressee (auto, manuel, forceee ou
                // validee sans redressement)
                if (adresseResidence.estValideRedressement()) {
                    TronconCarteScolaire tronconCarteScolaire = carteScolaireManager
                            .rechercherTronconParCommuneOuAdresse(adresseResidence);
                    if (tronconCarteScolaire != null) {
                        eleve.setZoneGeo(tronconCarteScolaire.getZoneGeo());
                        eleve.setStatutZoneGeo(StatutZoneGeoEnum.AUTO);
                    } else if (eleve.getZoneGeo() != null) {
                        eleve.setStatutZoneGeo(StatutZoneGeoEnum.MANUEL);
                    }
                }
            }
        }
    }

    /**
     * / * Mise � jour de la note moyenne et de la date de mise � jour de l'�l�ve.
     * Utilis� dans les traitements de masse (batch d'int�gration finale).
     *
     * @param eleve
     *            Eleve
     * @return nombre d'enregistrements mis � jour
     */
    public int majNoteMoyenne(Eleve eleve) {
        Integer nbRangs = notesManager.getNbRangs();
        // Recalcul de la note moyenne de l'�l�ve
        Double newNoteMoyenne = eleve.calculerMoyenne(nbRangs);
        eleve.setNoteMoyenne(newNoteMoyenne);
        // Mise � jour de la date de derni�re modification de l'�l�ve
        eleve.setDateMAJ(new Date());
        return eleveDao.majNoteMoyenne(eleve);
    }

    /**
     * Mise � jour de masse d'une liste d'�l�ves.
     *
     * @param eleves
     *            Liste des �l�ves � mettre � jour en base de donn�es.
     */
    public void maj(List<Eleve> eleves) {
        eleveDao.maj(eleves);
    }

    /**
     * Permet de savoir si on affiche ou non le lien "Offre(s) de formation(s) de secteur".
     *
     * @param eleve
     *            L'�l�ve.
     * @return True si on affiche le lie, false dans le cas contraire.
     */
    public boolean affichageLienOffresSecteurAutorise(Eleve eleve) {
        // SESAM 0308037 Cas ou le Palier d'origine n'est pas renseign�
        if (eleve == null) {
            return false;
        }
        Palier palier = eleve.getPalierOrigine();
        return Palier.CODE_PALIER_3EME.equals(palier == null ? null : palier.getCode())
                && eleve.getZoneGeo() != null && !eleve.getZoneGeo().getLiensZoneGeo().isEmpty();
    }

    /**
     * R�cup�ration des identifiants des responsables de l'�l�ve pour lesquels la suppression est interdite.
     *
     * @param eleve
     *            L'�l�ve.
     * @return Identifiants des responsables de l'�l�ve pour lesquels la suppression est interdite.
     */
    public Set<Long> listerResponsablesEleveSuppressionInterdite(Eleve eleve) {
        Set<Long> responsablesSuppressionInterdite = new HashSet<>();
        if (eleve != null && Palier.CODE_PALIER_3EME.equals(eleve.getPalierOrigine().getCode())) {
            // Post integration finale des voeux du TSA sur affelnet
            if (Flag.OUI.equalsIgnoreCase(parametreManager.getFlagIntegrationFinaleVoeuxEffectuee())) {
                responsablesSuppressionInterdite = eleve.getVoeuxTeleservice().stream()
                        .map(voeuTeleservice -> voeuTeleservice.getResponsable().getId())
                        .collect(Collectors.toSet());
            }
            // Pre integration finale des voeux du TSA sur affelnet
            else if (estEligibleTsa(eleve)) {
                responsablesSuppressionInterdite = eleve.getResponsables().stream()
                        .filter(responsable -> responsable.getNiveauResponsabilite().estRepresentantLegal())
                        .map(Responsable::getId).collect(Collectors.toSet());
            }
            // Les responsables qui ont re�u un mail de modification ne peuvent pas �tre supprim�s
            if(reponsablesOntRecuMailModif(eleve.getResponsables())){
                responsablesSuppressionInterdite.addAll(eleve.getResponsables()
                        .stream()
                        .filter(responsable -> envoiModifResponsableManager.existeEnvoiModifResponsable(responsable))
                        .map(Responsable::getId).collect(Collectors.toSet()));
            }
            // Les responsable dont les r�sultats ont �t� envoy�s au TSA ne peuvent pas �tre supprim�s
            if(responsablesNotifies(eleve.getResponsables())){
                responsablesSuppressionInterdite.addAll(eleve.getResponsables()
                        .stream().filter(responsable -> !responsable.isSuppressible())
                        .map(Responsable::getId).collect(Collectors.toSet()));
            }


        }

        return responsablesSuppressionInterdite;
    }

    private boolean responsablesNotifies(Set<Responsable> responsables) {
        return responsables !=null
                && responsables.stream().filter(responsable -> !responsable.isSuppressible()).count() > 0;
    }

    public boolean reponsablesOntRecuMailModif(Set<Responsable> responsables){
        return responsables !=null
                && responsables
                .stream()
                .filter(responsable ->
                        envoiModifResponsableManager.existeEnvoiModifResponsable(responsable)).count() > 0;
    }
}
