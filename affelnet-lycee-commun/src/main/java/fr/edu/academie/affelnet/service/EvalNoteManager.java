/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.affectation.EvaluationChampDisciplinaireOpaDao;
import fr.edu.academie.affelnet.dao.interfaces.affectation.EvaluationComplementaireOpaDao;
import fr.edu.academie.affelnet.dao.interfaces.affectation.NoteOpaDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.VoeuDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.affectation.EvaluationChampDisciplinaireOpa;
import fr.edu.academie.affelnet.domain.affectation.EvaluationComplementaireOpa;
import fr.edu.academie.affelnet.domain.affectation.NoteOpa;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationDiscipline;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationEleve;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.evaluation.lsu.AffichageEvaluationLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.AffichageEvaluationLSU.AffichageEvaluationDisciplineLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.AffichageEvaluationLSU.PeriodeLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CorrespondanceEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationEleveLSU;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.EtablissementNational;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.ModeEvalNote;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.service.evaluation.EvaluationComplementaireManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationDisciplineManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationSocleManager;
import fr.edu.academie.affelnet.service.evaluation.lsu.CorrespondanceEvaluationManager;
import fr.edu.academie.affelnet.service.evaluation.lsu.DemandeEvaluationManager;
import fr.edu.academie.affelnet.service.nomenclature.EtablissementNationalManager;
import fr.edu.academie.affelnet.utils.LsuUtils;
import fr.edu.academie.affelnet.web.actions.NavigationException;

/**
 * Manager regroupant des m�thodes traitant � la fois les notes et les �valuations des �l�ves.
 */
@Service
public class EvalNoteManager extends AbstractManager {
    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(EvalNoteManager.class);

    /** Valeur correspondant � l'absence de valeur d'une moyenne. */
    private static final double SANS_VALEUR = 0.0;

    /** Manager g�rant les notes. */
    @Autowired
    private NotesManager notesManager;

    /** Manager g�rant les �valuations du socle. */
    private EvaluationSocleManager evaluationSocleManager;

    /** Manager g�rant les �valuations des disciplines. */
    @Autowired
    private EvaluationDisciplineManager evaluationDisciplineManager;

    /** Manager g�rant les �valuations des disciplines compl�mentaires. */
    @Autowired
    private EvaluationComplementaireManager evaluationComplementaireManager;

    /** Le manager pour g�rer les �l�ves. */
    @Autowired
    private EleveManager eleveManager;

    /** Le Dao pour acc�der aux �l�ves. */
    @Autowired
    private EleveDao eleveDao;

    /** Le Dao pour acc�der aux offres de formation. */
    @Autowired
    private VoeuDao voeuDao;

    /** Le Dao pour acc�der aux moyennes des champs disciplinaire. */
    @Autowired
    private EvaluationChampDisciplinaireOpaDao evaluationChampDisciplinaireOpaDao;

    /** Le Dao pour acc�der aux oints harmonis�s des �valuations compl�mentaires. */
    @Autowired
    private EvaluationComplementaireOpaDao evaluationComplementaireOpaDao;

    /** Le Dao pour acc�der aux notes harmonis�s. */
    @Autowired
    private NoteOpaDao noteOpaDao;

    /** Le manager pour le tour courant. */
    @Autowired
    private CampagneAffectationManager campagneAffectationManager;

    /** Le manager pour les demandes d'�valuation LSU. */
    @Autowired
    private DemandeEvaluationManager demandeEvaluationManager;

    /** Le manager pour les correspondances LSU. */
    @Autowired
    private CorrespondanceEvaluationManager correspondanceEvaluationManager;

    /** Le manager pour les �tablissements nationnaux. */
    @Autowired
    private EtablissementNationalManager etablissementNationalManager;

    /**
     * @param evaluationSocleManager
     *            Gestionnaire d'�valuations du socle
     */
    @Autowired
    public void setEvaluationSocleManager(EvaluationSocleManager evaluationSocleManager) {
        this.evaluationSocleManager = evaluationSocleManager;
    }

    /**
     * Fournit le filtre pour les �l�ves sans note/�valuation valide.
     * 
     * @return le filtre pour les �l�ves sans note/�valuation valide
     */
    public Filtre filtreElevesSansEvalNote() {
        List<Filtre> filtres = new ArrayList<>();

        Filtre filtrePossedeVoeuAvecBareme = eleveDao
                .filtreElevesAvecVoeuAvecBareme(campagneAffectationManager.getNumeroTourCourant());
        Filtre filtre2nde = notesManager.filtresElevesPalier2ndeSansNote();
        Filtre filtreSansEvalSocle = evaluationSocleManager.filtreElevesSansEvalSocle();
        Filtre filtreSansEvalDiscipline = evaluationDisciplineManager.filtreElevesSansEvalDiscipline();
        Filtre filtreEleveNonForce = eleveManager.filtreElevesNonForces();

        Filtre filtrePalier3 = eleveDao.filtreElevesPalierOrigine(Palier.CODE_PALIER_3EME);

        Filtre filtre3eme = Filtre.and(Filtre.or(filtreSansEvalSocle, filtreSansEvalDiscipline), filtrePalier3);

        filtres.add(filtrePossedeVoeuAvecBareme);
        filtres.add(Filtre.or(filtre2nde, filtre3eme));
        filtres.add(filtreEleveNonForce);

        return Filtre.and(filtres);
    }

    /**
     * Renvoie la liste des �l�ves sans note (pour la palier 2nde)
     * ou sans �valuation du socle et de discipline (palier 3�me).
     * 
     * @param filtresSupplementaires
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des �l�ves sans note/�valuation
     */
    public List<Eleve> listerElevesSansEvalNote(List<Filtre> filtresSupplementaires) {
        List<Filtre> filtres = new ArrayList<>();

        filtres.add(filtreElevesSansEvalNote());

        if (filtresSupplementaires != null) {
            filtres.addAll(filtresSupplementaires);
        }

        // gestion des tris
        List<Tri> listeTris = new ArrayList<>();
        listeTris.add(Tri.asc("etablissementNational.id"));
        listeTris.add(Tri.asc("departement.codeMen"));
        listeTris.add(Tri.asc("nom"));
        listeTris.add(Tri.asc("prenom"));

        return eleveDao.lister(filtres, listeTris);
    }

    /**
     * Cette m�thode teste si on travaille sans �valuation/note.
     * 
     * @return Vrai si l'on travaille sans �valuation/note.
     */
    public boolean estSansEvalNote() {
        // On r�cup�re tous les voeux bar�me avec �valuations/notes
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("indicateurPam", TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur()));
        int nb = voeuDao.getNombreElements(filtres);

        // On ne doit avoir aucun voeu dans la liste
        return nb == 0;
    }

    /**
     * V�rifie qu'il y a pas de probl�me avec les notes et les �valuations pour l'ensemble des �l�ves.
     * 
     * @return true si aucun probl�me avec les notes ou les �valuations n'a �t� trouv�
     */
    public boolean isEvalNoteOK() {
        Filtre filtre = null;
        return isEvalNoteOK(filtre);
    }

    /**
     * V�rifie qu'il y a pas de probl�me avec les notes et les �valuations pour un �tablissement.
     * 
     * @param etablissement
     *            �tablissement v�rifi�
     * @return true si aucun probl�me avec les notes ou les �valuations n'a �t� trouv�
     */
    public boolean isEvalNoteOK(Etablissement etablissement) {
        Filtre filtre = Filtre.equal("etablissement.id", etablissement.getId());
        return isEvalNoteOK(filtre);
    }

    /**
     * V�rifie qu'il y a pas de probl�me avec les notes et les �valuations pour un �l�ve.
     * 
     * @param eleve
     *            �tablissement v�rifi�
     * @return true si aucun probl�me avec les notes ou les �valuations n'a �t� trouv�
     */
    public boolean isEvalNoteOK(Eleve eleve) {
        Candidature candidature = eleveManager.getCandidatureCourante(eleve);
        return isEvalNoteOK(candidature);
    }

    /**
     * V�rifie qu'il y a pas de probl�me avec les notes et les �valuations pour un �l�ve.
     * 
     * @param candidature
     *            la candidature de l'�l�ve
     * @return true si aucun probl�me avec les notes ou les �valuations n'a �t� trouv�
     */
    public boolean isEvalNoteOK(Candidature candidature) {
        boolean evalNoteNecessaire = false;

        // Si un �l�ve n'a pas de candidature pour le tour courant, il n'a pas de voeux donc n'a pas besoin de
        // note/�valuation
        if (candidature == null) {
            return true;
        }

        for (VoeuEleve voeux : candidature.getVoeux()) {
            if (voeux.getFlagRefusVoeu().equals(Flag.NON) && voeux.getVoeu().getIndicateurPam()
                    .equals(TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur())) {
                evalNoteNecessaire = true;
            }
        }

        if (!evalNoteNecessaire) {
            return true;
        }

        if (candidature.getEleve() != null) {
            Eleve eleve = candidature.getEleve();
            if (eleve.modeEvalNote().equals(ModeEvalNote.NOTE)) {
                return notesManager.isNoteObligatoireSaisie(eleve);
            } else if (eleve.modeEvalNote().equals(ModeEvalNote.EVALUATION)) {
                return isEvaluationSaisie(eleve);
            } else {
                // Si le mode est ind�termin�
                LOG.info("Mode de bar�me non d�termin� pour l'�l�ve " + eleve.getIne());
                return true;
            }
        } else {
            // Dans tous les autres cas, return true
            return true;
        }
    }

    /**
     * V�rifie qu'il y a pas de probl�me avec les notes et les �valuations pour les filtres donn�es.
     * 
     * @param filtreSupplementaires
     *            Filtres suppl�mentaires
     * @return true si aucun probl�me avec les notes ou les �valuations n'a �t� trouv�
     */
    public boolean isEvalNoteOK(Filtre filtreSupplementaires) {
        Filtre filtre = null;
        if (filtreSupplementaires != null) {
            filtre = Filtre.and(filtreSupplementaires, filtreElevesSansEvalNote());
        } else {
            filtre = filtreElevesSansEvalNote();
        }
        return eleveDao.getNombreElements(filtre) == 0;
    }

    /**
     * Indique si les �valuations du socle saisies sont suffisantes pour un calculs de bar�me pour l'�l�ve.
     * 
     * @param eleve
     *            �l�ve �valu�
     * @return si il y a suffisamment d'�valuation du socle saisies
     */
    public boolean isEvaluationSaisie(Eleve eleve) {
        return eleve.getEvaluationsSocle().size() > 0 && eleve.getEvaluationsDiscipline().size() > 0;
    }

    /**
     * Fournit les �valuations du socle de l'�l�ve.
     * 
     * @param eleve
     *            �l�ve auquel appartient les �valuations.
     * @return une map contenant les comp�tences du socle en leur associant les �valuations du socle de l'�l�ve
     */
    public Map<String, String> recupererEvalSocle(Eleve eleve) {
        return recupererEvalSocle(eleve, evaluationSocleManager.listerCompetencesSocle());
    }

    /**
     * Fournit les �valuations du socle de l'�l�ve.
     * 
     * @param eleve
     *            �l�ve auquel appartient les �valuations.
     * @param socle
     *            liste des comp�tences du socle.
     * @return une map contenant les comp�tences du socle en leur associant les �valuations du socle de l'�l�ve
     */
    public Map<String, String> recupererEvalSocle(Eleve eleve, List<CompetenceSocle> socle) {
        Map<String, String> mapEvaluationSocle = new LinkedHashMap<>();
        Map<CompetenceSocle, EvaluationSocle> mapEvaluationSocleEleve = eleve.getEvaluationsSocle();

        String affichageEval;
        for (CompetenceSocle competence : socle) {
            affichageEval = EvaluationEleve.LIBELLE_ABSENCE_DISPENSE_NON_EVAL;
            if (mapEvaluationSocleEleve.containsKey(competence)) {
                affichageEval = mapEvaluationSocleEleve.get(competence).affichagePositionnement();
            }
            mapEvaluationSocle.put(competence.getLibelleCourt(), affichageEval);
        }
        return mapEvaluationSocle;
    }

    /**
     * Fournit les �valuations de discipline de l'�l�ve.
     * 
     * @param eleve
     *            �l�ve auquel appartient les �valuations.
     * @return une map contenant les disciplines en leur associant les �valuations de l'�l�ve si elles existent.
     */
    public Map<String, String> recupererEvalDiscipline(Eleve eleve) {
        String codeMef = eleve.getFormation().getMefStat().getCode();
        List<Discipline> disciplines = evaluationDisciplineManager.listerDisciplinesMefStatCache(codeMef);
        Map<String, String> mapEvalDiscipline = new LinkedHashMap<>();
        Map<Discipline, EvaluationDiscipline> mapEvalDisciplineEleve = eleve.getEvaluationsDiscipline();

        String affichageEval;
        for (Discipline discipline : disciplines) {
            affichageEval = EvaluationEleve.LIBELLE_ABSENCE_DISPENSE_NON_EVAL;
            if (mapEvalDisciplineEleve.containsKey(discipline)) {
                affichageEval = mapEvalDisciplineEleve.get(discipline).affichagePositionnement();
            }
            mapEvalDiscipline.put(discipline.getLibelle(), affichageEval);
        }

        return mapEvalDiscipline;
    }

    /**
     * Fournit les �valuations compl�mentaires de l'�l�ve.
     * 
     * @param eleve
     *            �l�ve auquel appartient les �valuations.
     * @return une map contenant les �valuations compl�mentaires de l'�l�ve si elles existent.
     */
    public Map<EvaluationComplementaire, String> recupererEvalCompl(Eleve eleve) {
        List<EvaluationComplementaire> evaluationComplementaireList = evaluationComplementaireManager
                .listerEvalComplementaire();
        Map<EvaluationComplementaire, String> evalComp = new LinkedHashMap<>();
        Map<EvaluationComplementaire, EvaluationComplementaireEleve> evalCompEleve = eleve
                .getEvaluationsComplementaires();

        String affichageEval;
        for (EvaluationComplementaire evaluationComplementaire : evaluationComplementaireList) {
            affichageEval = EvaluationEleve.LIBELLE_ABSENCE_DISPENSE_NON_EVAL;
            if (evalCompEleve.containsKey(evaluationComplementaire)) {
                affichageEval = evalCompEleve.get(evaluationComplementaire).affichagePositionnement();
            }
            evalComp.put(evaluationComplementaire, affichageEval);
        }

        return evalComp;
    }

    /**
     * M�thode permettant de r�cup�rer les notes harmonis�es d'un �l�ve.
     * L'indice est de la map est l'id du rang et l'objet la noteOpa associ�e.
     * 
     * @param ine
     *            l'identifiant national de l'�l�ve
     * @param idOpa
     *            l'identiifant de l'opa
     * @return la map des notes harmonis�e de l'�l�ve avec comme cl� l'id du rang de la note
     */
    @SuppressWarnings("unchecked")
    public Map<Rang, NoteOpa> mapNoteHarmo(String ine, Long idOpa) {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(NoteOpa.class);
        criteria.add(Restrictions.eq("id.ine", ine));
        criteria.add(Restrictions.eq("id.idOpa", idOpa));

        Map<Rang, NoteOpa> noteHarmoMap = new HashMap<>();
        for (NoteOpa noteOpa : (List<NoteOpa>) criteria.list()) {
            noteHarmoMap.put(noteOpa.getRang(), noteOpa);
        }

        return noteHarmoMap;
    }

    /**
     * M�thode permettant de r�cup�rer la map des points harmonis�es des champ disciplinaire d'un �l�ve.
     * 
     * @param ine
     *            l'identifiant national de l'�l�ve
     * @param idOpa
     *            l'identiifant de l'opa
     * @return la map des evaluationChampDisciplinaireOpa d'un �l�ve avec comme cl� le code
     */
    @SuppressWarnings("unchecked")
    public Map<ChampDisciplinaire, EvaluationChampDisciplinaireOpa> mapPointHarmoChampDisp(String ine,
            Long idOpa) {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(EvaluationChampDisciplinaireOpa.class);
        criteria.add(Restrictions.eq("id.ine", ine));
        criteria.add(Restrictions.eq("id.idOpa", idOpa));

        Map<ChampDisciplinaire, EvaluationChampDisciplinaireOpa> mapChampOpa = new HashMap<>();
        for (EvaluationChampDisciplinaireOpa champOpa : (List<EvaluationChampDisciplinaireOpa>) criteria.list()) {
            mapChampOpa.put(champOpa.getChampDisciplinaire(), champOpa);
        }
        return mapChampOpa;
    }

    /**
     * M�thode permettant de r�cup�rer la map des points liss�e des �valuations compl�mentaires.
     * 
     * @param ine
     *            l'identifiant national de l'�l�ve
     * @param idOpa
     *            l'identiifant de l'opa
     * @return la map des �valuation compl�mentaire d'un �l�ve aevc pour cl� l'�valuation compl�mentaire
     */
    @SuppressWarnings("unchecked")
    public Map<EvaluationComplementaire, EvaluationComplementaireOpa> mapPointLisseEvalCompl(String ine,
            Long idOpa) {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(EvaluationComplementaireOpa.class);
        criteria.add(Restrictions.eq("id.ine", ine));
        criteria.add(Restrictions.eq("id.idOpa", idOpa));

        Map<EvaluationComplementaire, EvaluationComplementaireOpa> mapEvalOpa = new HashMap<>();

        for (EvaluationComplementaireOpa evalOpa : (List<EvaluationComplementaireOpa>) criteria.list()) {
            mapEvalOpa.put(evalOpa.getEvaluationComplementaire(), evalOpa);
        }
        return mapEvalOpa;
    }

    /**
     * Fournit les �valuations du socle de l'�l�ve avec ; pour les socles manquants ; la moyenne.
     * Cas particulier, s'il n'y a aucun socle qui est valu�, on renverra la liste vide.
     * 
     * @param eleve
     *            �l�ve auquel appartient les �valuations.
     * @param socle
     *            liste des comp�tence du socle.
     * @return une map contenant les comp�tences du socle en leur associant les �valuations du socle de l'�l�ve
     */
    public Map<String, Double> recupererEvalSocleMoyenne(Eleve eleve, List<CompetenceSocle> socle) {
        Map<String, Double> mapEvaluationSocle = new LinkedHashMap<>();
        Map<CompetenceSocle, EvaluationSocle> mapEvaluationSocleEleve = eleve.getEvaluationsSocle();
        double moyenne = SANS_VALEUR;
        double affichageEval;
        // On ne consid�re que les comp�tence qui ont une valeur � la base
        int nombreCompetence = 0;
        for (CompetenceSocle competence : socle) {
            if (mapEvaluationSocleEleve.containsKey(competence)) {
                affichageEval = mapEvaluationSocleEleve.get(competence).getPoints();
                moyenne += affichageEval;
            } else {
                affichageEval = SANS_VALEUR;
            }
            mapEvaluationSocle.put(competence.getLibelleCourt(), affichageEval);
            // On met � jour le nombre de comp�tence pour pouvoir calculer correctement la moyenne
            if (affichageEval != SANS_VALEUR && (Double) affichageEval != null) {
                nombreCompetence++;
            }
        }

        if (nombreCompetence == 0) {

            // Cas particulier, o� il n'y a pas d'�valuations de socle : on renvoi la table vide
            mapEvaluationSocle = Collections.emptyMap();
        } else {
            // On calcule la moyenne
            moyenne = moyenne / nombreCompetence;

            if (mapEvaluationSocle.containsValue(SANS_VALEUR) || mapEvaluationSocle.containsValue(null)) {
                Set<String> listeCle = mapEvaluationSocle.keySet();
                for (String key : listeCle) {
                    if (mapEvaluationSocle.get(key).equals(SANS_VALEUR) || mapEvaluationSocle.get(key) == null) {
                        mapEvaluationSocle.put(key, moyenne);
                    }
                }
            }
        }
        return mapEvaluationSocle;
    }

    /**
     * Enregistre en base de donn�es l'evaluationChampDisciplinaireOpa.
     * 
     * @param evalChamOpa
     *            l'objet � enregistrer en base
     * @return l'objet enregistr�
     */
    public EvaluationChampDisciplinaireOpa creerChampDisciplinaireOpa(
            EvaluationChampDisciplinaireOpa evalChamOpa) {
        return evaluationChampDisciplinaireOpaDao.creer(evalChamOpa);
    }

    /**
     * Met � jour en base de donn�es l'evaluationChampDisciplinaireOpa.
     * 
     * @param evalChamOpa
     *            l'objet � mettre � jour.
     * @return l'objet mis � jour
     */
    public EvaluationChampDisciplinaireOpa majChampDisciplinaireOpa(EvaluationChampDisciplinaireOpa evalChamOpa) {
        return evaluationChampDisciplinaireOpaDao.maj(evalChamOpa, evalChamOpa.getId());
    }

    /**
     * Enregistre en base l'�vavaluation compl�mentaire OPA.
     * 
     * @param evalCompOpa
     *            objet enregistr� en base
     * @return l'objet enregistr�
     */
    public EvaluationComplementaireOpa creerEvaluationComplementaireOpa(EvaluationComplementaireOpa evalCompOpa) {
        return evaluationComplementaireOpaDao.creer(evalCompOpa);
    }

    /**
     * Enregistre en base la note harmonis�e.
     * 
     * @param noteOpa
     *            objet enregistr� en base
     * @return l'objet enregistr�
     */
    public NoteOpa creerNoteOpa(NoteOpa noteOpa) {
        return noteOpaDao.creer(noteOpa);
    }

    /**
     * Purge l'ensemble des evaluationChampDisciplinaireOpa pour l'OPA donn�e.
     * 
     * @param idOpa
     *            Id de l'OPA
     */
    public void purgerChampDisciplinaireOpa(Long idOpa) {
        evaluationChampDisciplinaireOpaDao.purgerChampDisciplinaireOpa(idOpa);
    }

    /**
     * Purge l'ensemble des evaluationComplementaireOpa pour l'OPA donn�.
     * 
     * @param idOpa
     *            Id de l'OPA
     */
    public void purgerEvaluationCompementaireOpa(Long idOpa) {
        evaluationComplementaireOpaDao.purgerEvaluationCompementaireOpa(idOpa);
    }

    /**
     * Purge l'ensemble des noteOpa pour l'OPA donn�.
     * 
     * @param idOpa
     *            Id de l'OPA
     */
    public void purgerNoteOpa(Long idOpa) {
        noteOpaDao.purger(idOpa);
    }

    /**
     * Purge l'ensemble des donn�es d'�valuation associ�es � une OPA.
     * 
     * @param idOpa
     *            l'identifiant d'OPA
     */
    public void purgerEvalNoteOpa(Long idOpa) {
        LOG.info("Suppression de toutes les notes et �valuations harmonis�es calcul�es pour l'OPA " + idOpa);
        purgerChampDisciplinaireOpa(idOpa);
        purgerEvaluationCompementaireOpa(idOpa);
        purgerNoteOpa(idOpa);
    }

    /**
     * Purge l'ensemble des donn�es d'�valuation OPA associ�es � l'�l�ve.
     * 
     * @param ine
     *            l'identifiant d'�l�ve
     */
    public void purgerEvalNotesEleveOpa(String ine) {
        LOG.info("Suppression de toutes les notes et �valuations harmonis�es calcul�es pour l'�l�ve " + ine
                + " dans les OPA ");
        evaluationChampDisciplinaireOpaDao.purgerChampDisciplinaireOpaEleve(ine);
        evaluationComplementaireOpaDao.purgerEvaluationComplementaireOpaEleve(ine);
        noteOpaDao.purgerEleve(ine);
    }

    /**
     * Attribue les valeurs pour l'objet d'affichage � l'aide des managers.
     * 
     * @param ine
     *            INE de l'�l�ve
     * @return l'AffichageEvaluationLSU de l'�l�ve
     */
    public AffichageEvaluationLSU genererAffichageEvaluationLsu(String ine) {
        Eleve eleve = eleveManager.chargerParIne(ine);
        EvaluationEleveLSU eleveLsu = null;
        if (eleve.getEtablissement() != null) {
            eleveLsu = demandeEvaluationManager.chargerEleveLsu(ine, eleve.getEtablissement().getId());
        }

        if (eleveLsu == null) {
            throw new NavigationException(
                    "Aucune �valuation en provenance de LSU n'a �t� trouv�e pour cet �l�ve.");
        }

        // Initialisation du bean d'affichage
        AffichageEvaluationLSU evaluations = new AffichageEvaluationLSU();
        if (eleveLsu.getDemandeEvaluation().isIntegree()) {
            evaluations.setDateIntegration(eleveLsu.getDemandeEvaluation().getDateSucces());
        }
        evaluations.setNomEleve(eleveLsu.toString());

        // On g�n�re les �valuations du socle
        evaluations.genererMapSocle(eleveLsu.getSocle(), evaluationSocleManager.mapCompetencesSocle(),
                evaluationSocleManager.mapDegreMaitrise());

        // On r�cup�re les disciplines et on g�n�re les �valuations associ�s
        String codeMefstat = eleve.getFormation().getMefStat().getCode();
        List<AffichageEvaluationDisciplineLSU> evalSansPointList = evaluations.genererAffichageDiscipline(
                eleveLsu.getBilans(), evaluationDisciplineManager.listerDisciplinesMefStat(codeMefstat),
                correspondanceEvaluationManager.listerGroupesNiveau());

        // On r�cup�re les �tablissements
        for (PeriodeLSU periode : evaluations.getPeriodes()) {
            EtablissementNational etablissement = etablissementNationalManager
                    .charger(periode.getIdEtablissement());
            periode.setEtablissement(etablissement);
        }

        // On attribue les points des �valuations qui ont besoin d'un acc�s aux managers
        String valEval;
        double evalNum;
        Map<Integer, GroupeNiveau> groupeNiveauMap = evaluationDisciplineManager.mapGroupeNiveau();
        for (AffichageEvaluationDisciplineLSU evalSansPoint : evalSansPointList) {
            if (EvaluationDisciplineManager.MODALITE_ELECTION_ACCEPTEES.contains(evalSansPoint.getModalite())) {
                valEval = evalSansPoint.getValEval();
                if (StringUtils.isBlank(valEval)) {
                    evalSansPoint.setNonNote(true);
                } else if (LsuUtils.estNoteSur20(valEval)) {
                    evalNum = LsuUtils.conversionEvalEnNumerique(valEval);
                    evalSansPoint.setGroupeNiveau(
                            evaluationDisciplineManager.conversionEvalNumEnGroupeNiveau(evalNum, groupeNiveauMap));
                } else {
                    CorrespondanceEvaluation correspondance = correspondanceEvaluationManager
                            .charger(eleve.getEtablissement().getId(), valEval);
                    if (correspondance.getFlagIgnore().equals(Flag.NON)) {
                        evalSansPoint.setGroupeNiveau(correspondance.getGroupeNiveau());
                    } else {
                        evalSansPoint.setNonNote(true);
                    }
                }
            } else {
                evalSansPoint.setModaliteIgnoree(true);
            }
        }
        return evaluations;
    }
}
