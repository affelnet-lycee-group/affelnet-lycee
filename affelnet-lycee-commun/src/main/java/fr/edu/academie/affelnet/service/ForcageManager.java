/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.affectation.DecisionCommission;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;
import fr.edu.academie.affelnet.domain.nomenclature.Pile;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.utils.exceptions.BusinessException;

/** Cette classe g�re la fonctionnalit� de forcage des d�cisions sur les candidatures et les voeux des �l�ves. */
@Service
public class ForcageManager {

    /** Valeur de param�tre sp�cifique pour indiquer tous les rangs. */
    public static final int RANG_TOUS = -1;

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(ForcageManager.class);

    // Codes de d�cisions finales (pour rendre plus lisible)

    /** D�cision finale affect�. */
    private static final Integer DF_AFFECTE = Integer.valueOf(DecisionFinale.AFFECTE.getCode());

    /** D�cision finale liste sup. */
    private static final Integer DF_LISTESUP = Integer.valueOf(DecisionFinale.LISTE_SUPP.getCode());

    /** D�cision finale refus�. */
    private static final Integer DF_REFUSE = Integer.valueOf(DecisionFinale.REFUSE.getCode());

    /** D�cision finale admis. */
    private static final Integer DF_ADMIS = Integer.valueOf(DecisionFinale.ADMIS_CONTRAT_SIGNE.getCode());

    /** D�cision finale attente de signature. */
    private static final Integer DF_ATT_SIG = Integer
            .valueOf(DecisionFinale.EN_ATTENTE_SIGNATURE_CONTRAT.getCode());

    /** D�cision finale abandon. */
    private static final Integer DF_ABANDON = Integer.valueOf(DecisionFinale.ABANDON.getCode());

    /** D�cision finale recensement. */
    private static final Integer DF_RECENSEMENT = Integer.valueOf(DecisionFinale.RECENSEMENT.getCode());

    /** Le gestionnaire d'�l�ves. */
    private EleveManager eleveManager;

    /** Le gestionnaire de campagne d'affectation. */
    private CampagneAffectationManager campagneManager;

    /** Le gestionnaire de param�tres applicatifs. */
    private ParametreManager parametreManager;

    /** Le gestionnaire de journalisation. */
    private JournalManager journalManager;

    /**
     * @param eleveManager
     *            Le gestionnaire d'�l�ves.
     */
    @Autowired
    public void setEleveManager(EleveManager eleveManager) {
        this.eleveManager = eleveManager;
    }

    /**
     * @param campagneManager
     *            Le gestionnaire de campagne d'affectation.
     */
    @Autowired
    public void setCampagneManager(CampagneAffectationManager campagneManager) {
        this.campagneManager = campagneManager;
    }

    /**
     * @param parametreManager
     *            Le gestionnaire de param�tres applicatifs.
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param journalManager
     *            Le gestionnaire de journalisation.
     */
    @Autowired
    public void setJournalManager(JournalManager journalManager) {
        this.journalManager = journalManager;
    }

    /**
     * Permettre de forcer/deforcer les d�cisions d'un �l�ve (sauf annulation globale de for�age).
     *
     * <table border="plain" summary="codes">
     * <tr>
     * <td colspan="2"></td>
     * <td>abandon</td>
     * <td>affect�</td>
     * <td>refus�</td>
     * <td>refus� 1 voeu</td>
     * <td>d�for�age refus� 1 voeu</td>
     * </tr>
     * <tr>
     * <td rowspan="2">Candidature</td>
     * <td>flag forc�</td>
     * <td>O</td>
     * <td>O</td>
     * <td>O</td>
     * <td><b>N</b></td>
     * <td>N</td>
     * </tr>
     * <tr>
     * <td>code d�cision finale</td>
     * <td>4 : Abandon</td>
     * <td>1 : Admis</td>
     * <td>3 : Refus�</td>
     * <td>selon recalcul</td>
     * <td>?</td>
     * </tr>
     * <tr>
     * <td rowspan="3">Voeu concern�</td>
     * <td>code d�cision finale</td>
     * </tr>
     * <tr>
     * <td>code d�cision provisoire</td>
     * </tr>
     * <tr>
     * <td>flag voeu forc� refus�</td>
     * </tr>
     * </table>
     *
     * @param ine
     *            INE de l'�l�ve concern� par le for�age
     * @param codeDecisionSaisi
     *            code du forcage de la d�cision 0 : Annulation du for�age de refus
     *            sur un voeu, 1 : Affect�, 3 : Refus� globalement ou sur un voeu, 4
     *            : Abandon
     * @param rangConcerne
     *            rang concern� par le for�age (RANG_TOUS = -1 pour une d�cision globale)
     * @param uid
     *            UID de l'utilisateur ayant demand� l'op�ration
     */
    public void forcerDeforcerDecision(String ine, String uid, int codeDecisionSaisi, int rangConcerne) {

        LOG.debug("(D�-)For�age INE:" + ine + " uid:" + uid + " code:" + codeDecisionSaisi + " rang:"
                + rangConcerne);

        // Contr�le sur la validit� si on force sur un voeu pr�cis
        // Il ne doit �tre ni recensement, ni apprentissage en commission
        if (rangConcerne != RANG_TOUS) {
            Eleve eleve = eleveManager.chargerParIne(ine);
            Candidature candidatureCourante = eleveManager.getCandidatureCourante(eleve);
            VoeuEleve voeueleve = candidatureCourante.getVoeux().get(rangConcerne - 1);
            blocageForcageUnitaireRecensementApprentissage(voeueleve);
        }

        if (codeDecisionSaisi == 0) {
            // annule le forcage refus� sur un voeu pr�cis.
            annuleForcageRefusVoeu(ine, rangConcerne, uid);
            return;
        }

        DecisionFinale decisionFinaleSaisie = DecisionFinale.getPourCode(codeDecisionSaisi);
        if (decisionFinaleSaisie == null) {
            LOG.error("Le code de d�cision " + codeDecisionSaisi + " n'est pas valide");
            throw new ValidationException("La d�cision forc�e n'est pas invalide.");
        }

        switch (decisionFinaleSaisie) {

            case AFFECTE:

                // Force � prendre l'�l�ve sur le voeu et � le refuser sur les autres
                forceElevePris(ine, rangConcerne, uid);

                break;

            case REFUSE:

                if (rangConcerne == RANG_TOUS) {
                    // Refus global de l'�l�ve
                    forceEleveRefuse(ine, uid);
                } else {
                    forceEleveRefuseVoeu(ine, rangConcerne, uid);
                }
                break;

            case ABANDON:
                forceEleveAbandon(ine, uid);
                break;

            default:
                LOG.error(
                        "La d�cision finale " + decisionFinaleSaisie + " ne peut pas �tre attribu�e par for�age.");
                throw new ValidationException("La d�cision ne peut pas �tre attribu�e par for�age.");
        }
    }

    /**
     * Bloque les for�ages sur les voeux concernant les offres de recensement ou apprentissage trait�es en
     * commission.
     *
     * @param voeueleve
     *            le voeu � tester
     */
    private void blocageForcageUnitaireRecensementApprentissage(VoeuEleve voeueleve) {
        Voeu offreFormation = voeueleve.getVoeu();

        if (offreFormation.estRecensement()) {
            LOG.error("Le for�age d'un voeu sur une offre de recensement n'est pas autoris�");
            throw new ValidationException("Le for�age d'un voeu sur une offre de recensement n'est pas autoris�");
        }

        if (offreFormation.estTraiteeEnCommission() && offreFormation.getStatut().isApprentissage()) {
            LOG.error("Le for�age sur une offre de statut apprentissage trait�e en commission n'est pas autoris�");
            throw new ValidationException(
                    "Le for�age sur une offre de statut apprentissage trait�e en commission n'est pas autoris�");
        }
    }

    /**
     * Force l'�l�ve pris sur un voeu (Log l'op�ration dans le journal des
     * op�rations).
     *
     * @param ine
     *            l'ine de l'�l�ve
     * @param rang
     *            le rang du voeu
     * @param uid
     *            le login de l'utilisateur
     */
    public void forceElevePris(String ine, Integer rang, String uid) {
        Eleve eleve = eleveManager.chargerParIne(ine);
        Candidature candidatureCourante = eleveManager.getCandidatureCourante(eleve);

        // mise � jour de l'�l�ve
        candidatureCourante.setFlagEleveForce(Flag.OUI);
        candidatureCourante.setFlagSaisieValide(Flag.OUI);
        candidatureCourante.setRangAdmission(rang);
        candidatureCourante.setCodeDecisionFinale(Integer.valueOf(DecisionFinale.AFFECTE.getCode()));

        short numeroTour = campagneManager.getNumeroTourCourant();
        // mise � jour des voeux de l'�l�ve :
        // -admis sur le voeu forc� et
        // -refus� sur les autres voeux
        for (VoeuEleve voeuEleve : eleve.voeuxDuTour(numeroTour)) {
            if (!voeuEleve.getId().getRang().equals(rang)) {
                // refus� sur les autres voeux
                forceVoeu(DecisionFinale.REFUSE, voeuEleve);
            } else {
                // admis sur le voeu forc� si le voeu a le m�me rang
                forceVoeu(DecisionFinale.AFFECTE, voeuEleve);
            }
        }

        // mise � jour des voeux de l'�l�ve : d�cision finale � 7 ('Sans objet') pour
        // tous ses voeux de recensement
        // Remarque : comme on force 'affect�' il y a forc�ment un autre voeu que du recensement.
        // Le contr�le en amont fait que l'on n'a pas � se pr�occuper de la d�cision recensement pour la
        // candidature
        forceDecisionRecensement(eleve.getIne());

        // Idem inutile de se pr�occuper de la d�cision finale d'apprentissage commission
        // => report simple
        reporteDecisionsApprentissage(candidatureCourante);

        eleveManager.modifier(eleve, eleve.getIne());

        // enregistrement dans le journal
        VoeuEleve voeuElv = eleveManager.chargerParIne(ine).voeuDuTourEtDeRang(numeroTour, rang);
        journalManager.addForceElevePris(uid, rang, voeuElv);
    }

    /**
     * Cette m�thode force la d�cision finale � 'Refus�' (3) sur tous les voeux de
     * l'�l�ve et ajoute une op�ration au journal.
     *
     * @param ine
     *            l'ine de l'�l�ve
     * @param uid
     *            le login de l'utilisateur effectuant l'action
     */
    public void forceEleveRefuse(String ine, String uid) {
        Eleve eleve = eleveManager.chargerParIne(ine);
        Candidature candidatureCourante = eleveManager.getCandidatureCourante(eleve);

        // mise � jour de la candidature : d�cision g�n�rale � 3 ('Refus�') + flag �l�ve forc�
        // � O ('Oui')
        Integer nouvelleDecisionFinaleCandidature = Integer.valueOf(DecisionFinale.REFUSE.getCode());
        Integer nouveauRangAdmission = null;

        // mise � jour des voeux de l'�l�ve : d�cision finale � 3 ('Refus�') pour tous
        // ses voeux
        for (VoeuEleve voeuEleve : eleve.voeuxDuTour(campagneManager.getNumeroTourCourant())) {
            forceVoeu(DecisionFinale.REFUSE, voeuEleve);
        }

        // mise � jour des voeux de l'�l�ve : d�cision finale � 7 ('Sans objet') pour
        // tous ses voeux de recensement
        if (forceDecisionRecensement(eleve.getIne())) {
            // Il n'y a que du recensement, cela se propage � la candidature
            nouvelleDecisionFinaleCandidature = DecisionFinale.RECENSEMENT.getCode();
        }

        // Reporte les d�cisions apprentissage en commission
        Integer meilleureDecisionApprCommiUniqueCandidature = reporteDecisionsApprentissage(candidatureCourante);
        if (meilleureDecisionApprCommiUniqueCandidature != null) {

            // S'il n'y a que des voeux apprentissage commission on reporte la meilleure d�cision possible
            nouvelleDecisionFinaleCandidature = meilleureDecisionApprCommiUniqueCandidature;
        }

        candidatureCourante.setCodeDecisionFinale(nouvelleDecisionFinaleCandidature);
        candidatureCourante.setRangAdmission(nouveauRangAdmission);
        candidatureCourante.setFlagEleveForce(Flag.OUI);
        candidatureCourante.setFlagSaisieValide(Flag.OUI);
        eleveManager.modifier(eleve, eleve.getIne());

        // enregistrement dans le journal des op�rations
        journalManager.addForceEleveRefuse(uid, ine);
    }

    /**
     * Force le refus d'un �l�ve pour son voeu de rang donn�.
     *
     * @param ine
     *            Identifiant de l'�l�ve
     * @param rang
     *            Rang du voeu pour lequel on refuse l'�l�ve
     * @param uid
     *            l'identifiant utilisateur
     */
    public void forceEleveRefuseVoeu(String ine, Integer rang, String uid) {

        LOG.debug("Forcage du refus de l'�l�ve " + ine + " sur le voeu de rang " + rang + ".");

        // On doit propager
        Eleve elv = eleveManager.chargerParIne(ine);

        short numeroTour = campagneManager.getNumeroTourCourant();
        VoeuEleve voeuEleve = elv.voeuDuTourEtDeRang(numeroTour, rang);

        // On force le voeu � refus� (d�cision finale et flag)
        forceVoeu(DecisionFinale.REFUSE, voeuEleve);
        // ici exception pour le flag refus voeu
        voeuEleve.setFlagRefusVoeu(Flag.OUI);
        recalculEtatCandidature(elv);
        eleveManager.modifier(elv, ine);

        // #459 Log dans le journal des d�cisions force/d�force
        journalManager.addRefus1Voeu(uid, rang, voeuEleve);

    }

    /**
     * Cette m�thode force la d�cision finale � 'Abandon' (4) sur tous les voeux de
     * l'�l�ve.
     *
     * @param ine
     *            l'ine de l'�l�ve
     * @param uid
     *            le login de l'utilisateur effectuant l'action
     */

    public void forceEleveAbandon(String ine, String uid) {
        Eleve eleve = eleveManager.chargerParIne(ine);
        Candidature candidatureCourante = eleveManager.getCandidatureCourante(eleve);

        Integer nouvelleDecisionFinaleCandidature = DF_ABANDON;

        // mise � jour des voeux de l'�l�ve : d�cision finale � 4 ('Abandon') pour tous
        // ses voeux
        for (VoeuEleve voeuEleve : eleve.voeuxDuTour(campagneManager.getNumeroTourCourant())) {
            forceVoeu(DecisionFinale.ABANDON, voeuEleve);
        }

        // mise � jour des voeux de l'�l�ve : d�cision finale � 7 ('Sans objet') pour
        // tous ses voeux de recensement
        if (forceDecisionRecensement(eleve.getIne())) {
            // Il n'y a que du recensement, cela se propage � la candidature
            nouvelleDecisionFinaleCandidature = DecisionFinale.RECENSEMENT.getCode();
        }

        // Reporte les d�cisions apprentissage en commission
        Integer meilleureDecisionApprCommiUniqueCandidature = reporteDecisionsApprentissage(candidatureCourante);
        if (meilleureDecisionApprCommiUniqueCandidature != null) {

            // S'il n'y a que des voeux apprentissage commission on reporte la meilleure d�cision possible
            nouvelleDecisionFinaleCandidature = meilleureDecisionApprCommiUniqueCandidature;
        }

        candidatureCourante.setCodeDecisionFinale(nouvelleDecisionFinaleCandidature);
        candidatureCourante.setRangAdmission(null);
        candidatureCourante.setFlagEleveForce(Flag.OUI);
        candidatureCourante.setFlagSaisieValide(Flag.OUI);
        eleveManager.modifier(eleve, eleve.getIne());

        // enregistrement dans le journal des op�rations
        journalManager.addForceEleveAbandon(uid, ine);
    }

    /**
     * Force le voeu en fonction du code d�cision pass� en param�tre et mettre �
     * jour la pile (valeur accept� : refus�, affect� et abandon).
     *
     * @param decisionFinaleAForcer
     *            la d�cision pour ce voeu
     * @param voeuElv
     *            le voeu de l'�l�ve
     */
    private void forceVoeu(DecisionFinale decisionFinaleAForcer, VoeuEleve voeuElv) {

        if (decisionFinaleAForcer != DecisionFinale.REFUSE && decisionFinaleAForcer != DecisionFinale.AFFECTE
                && decisionFinaleAForcer != DecisionFinale.ABANDON) {
            throw new BusinessException(
                    "La d�cision finale " + decisionFinaleAForcer + " est invalide pour le forcage");
        }

        Voeu offreFormation = voeuElv.getVoeu();
        if (offreFormation.estTraiteeEnCommission() && offreFormation.getStatut().isApprentissage()) {
            // Le for�age sur un voeu portant sur une offre d'apprentissage trait�e en commission
            // est sans effet de fa�on � pr�server les saisies manuelles sp�cifiques

            return;
        }

        Pile p;

        // si ce voeu �tait affect� nombre admis (-1)
        if (voeuElv.getCodeDecisionFinal() != null
                && voeuElv.getCodeDecisionFinal().equals(DecisionFinale.AFFECTE.getCode())) {
            p = offreFormation.getPile();
            p.setNombreElevesAdmis(p.getNombreElevesAdmis() - 1);
        }

        // refus ou abandon ou affect�
        voeuElv.setCodeDecisionFinal(decisionFinaleAForcer.getCode());

        // Flag refus voeu (tjs NON except� pour refus un voeu
        voeuElv.setFlagRefusVoeu(Flag.NON);
        // RAZ des infos liste supp.
        voeuElv.setNumeroListeSuppFinal(null);

        // si voeu affect� nombre admis (+1)
        if (voeuElv.getCodeDecisionFinal() != null
                && voeuElv.getCodeDecisionFinal().equals(DecisionFinale.AFFECTE.getCode())) {
            p = offreFormation.getPile();
            p.setNombreElevesAdmis(p.getNombreElevesAdmis() + 1);
        }

        // Pour les voeux de type commission, on modifie la d�cision provisoire en
        // cons�quence
        if (TypeOffre.COMMISSION.getIndicateur().equals(offreFormation.getIndicateurPam())) {
            switch (decisionFinaleAForcer) {
                case AFFECTE:
                    voeuElv.setCodeDecisionProvisoire(DecisionCommission.PRIS.getCode());
                    break;
                case REFUSE:
                    voeuElv.setCodeDecisionProvisoire(DecisionCommission.REFUSE.getCode());
                    break;
                case ABANDON:
                    voeuElv.setCodeDecisionProvisoire(DecisionCommission.NON_TRAITE.getCode());
                    break;
                default:
                    break;
            }
        }

        voeuElv.setDateMAJ(new Date());
    }

    /**
     * Cette m�thode met � toutes les d�cisions � 7 pour les voeux de recensement.
     *
     * @param ine
     *            l'ine de l'�l�ve
     * @return vrai s'il n'y a que des voeux de recensement
     */
    private boolean forceDecisionRecensement(String ine) {
        Eleve eleve = eleveManager.chargerParIne(ine);

        boolean uniquementRecensement = true;

        Integer codeDecisionFinalrecensement = Integer.valueOf(DecisionFinale.RECENSEMENT.getCode());
        for (VoeuEleve voeuEleve : eleve.voeuxDuTour(campagneManager.getNumeroTourCourant())) {
            // si voeu de recensement
            if (voeuEleve != null && voeuEleve.getVoeu().getFlagRecensement().equals(Flag.OUI)) {
                voeuEleve.setCodeDecisionFinal(codeDecisionFinalrecensement);
                voeuEleve.setFlagRefusVoeu(Flag.NON);
                voeuEleve.setNumeroListeSuppFinal(null);
                voeuEleve.setDateMAJ(new Date());
            } else {
                uniquementRecensement = false;
            }
        }

        return uniquementRecensement;
    }

    /**
     * Report des d�cisions prises en commission apprentissage sur les d�cisions finales pour tous les voeux.
     *
     * Remarque, cette m�thode ne doit �tre appel�e que dans le contexte d'un for�age global.
     * Le for�age d'un voeu unitaire apprentissage trait� en commission est interdit.
     *
     * @param candidature
     *            la candidature � traiter
     * @return la meilleure d�cision � placer sur la candidature s'il n'y a que des voeux apprentissage en
     *         commission
     */
    private Integer reporteDecisionsApprentissage(Candidature candidature) {

        Voeu offreFormation;
        Integer codeDecisionFinaleVoeu;
        Integer decisionApprentissageCandidature = null;

        for (VoeuEleve voeuEleve : candidature.getVoeux()) {

            offreFormation = voeuEleve.getVoeu();
            if (offreFormation.estTraiteeEnCommission() && offreFormation.getStatut().isApprentissage()) {

                String codeDecisionCommission = voeuEleve.getCodeDecisionProvisoire();
                if (codeDecisionCommission != null) {

                    boolean dAdmis = decisionApprentissageCandidature != null
                            && DF_ADMIS.equals(decisionApprentissageCandidature);
                    boolean dAttente = decisionApprentissageCandidature != null
                            && DF_ATT_SIG.equals(decisionApprentissageCandidature);
                    boolean dRefuse = decisionApprentissageCandidature != null
                            && DF_REFUSE.equals(decisionApprentissageCandidature);

                    codeDecisionFinaleVoeu = DecisionCommission.getPourCode(codeDecisionCommission)
                            .getDecisionFinaleCorrespondante().getCode();

                    if (DF_ADMIS.equals(codeDecisionFinaleVoeu) && !dAdmis) {
                        decisionApprentissageCandidature = DF_ADMIS;
                    }

                    if (DF_ATT_SIG.equals(codeDecisionFinaleVoeu) && !dAdmis && !dAttente) {
                        decisionApprentissageCandidature = DF_ATT_SIG;
                    }

                    if (DF_REFUSE.equals(codeDecisionFinaleVoeu) && !dAdmis && !dAttente && !dRefuse) {
                        decisionApprentissageCandidature = DF_REFUSE;
                    }

                } else {
                    // Par s�curit�, si rien n'est saisi, il n'y a rien � pr�server, ne devrait pas se produire
                    // si la d�cision commission est bien initialis�e avec la valeur par d�faut.
                    codeDecisionFinaleVoeu = null;
                }

                voeuEleve.setCodeDecisionFinal(codeDecisionFinaleVoeu);
                voeuEleve.setFlagRefusVoeu(Flag.NON);
                voeuEleve.setNumeroListeSuppFinal(null);
                voeuEleve.setDateMAJ(new Date());
            } else {

                Integer codeDecisionFinalVoeuAutre = voeuEleve.getCodeDecisionFinal();
                if (codeDecisionFinalVoeuAutre == null || !DF_ABANDON.equals(codeDecisionFinalVoeuAutre)) {

                    // Il y a d'autres voeux que de l'apprentissage commission, autres qu'abandon
                    // => annule la d�cision
                    decisionApprentissageCandidature = null;
                }

            }
        }

        return decisionApprentissageCandidature;
    }

    /**
     * Annule le for�age du refus d'un �l�ve pour son voeu de rang donn�.
     *
     * @param ine
     *            Identifiant de l'�l�ve
     * @param rang
     *            Rang du voeu pour lequel on ne refuse plus l'�l�ve
     * @param uid
     *            l'identifiant utilisateur
     */
    public void annuleForcageRefusVoeu(String ine, Integer rang, String uid) {
        Eleve eleve = eleveManager.chargerParIne(ine);

        LOG.debug("Annulation du forcage du refus de l'�l�ve " + ine + " sur le voeu de rang " + rang + ".");
        // d�forcer l'�l�ve sur ce voeu
        short numeroTour = campagneManager.getNumeroTourCourant();
        VoeuEleve voeuEleve = eleve.voeuDuTourEtDeRang(numeroTour, rang);

        if (voeuEleve == null) {
            throw new BusinessException(EleveManager.class + " annuleForcageRefusVoeu(Integer rang) :"
                    + " le voeu de rang " + rang + " n'existe pas.");
        }

        annulerForcageVoeu(voeuEleve);
        recalculEtatCandidature(eleve);
        eleveManager.modifier(eleve, eleve.getIne());

        // #459 logger l'op�ration dans le Journal des op�rations
        journalManager.addAnnuleForcageRefusVoeu(uid, rang, voeuEleve);
    }

    /**
     * Annule un forcage global concernant tous les voeux d'une candidature.
     *
     * On r�tablit les d�cisions ant�rieures si elles sont connues,
     * sinon les d�cisions par d�faut.
     *
     * @param eleve
     *            l'�l�ve dont on annule le for�age
     * @param uid
     *            identifiant de l'utilisateur effectuant l'action
     */
    public void annulerForcage(Eleve eleve, String uid) {

        String ine = eleve.getIne();
        Candidature candidatureCourante = eleveManager.getCandidatureCourante(eleve);

        if (candidatureCourante == null || Flag.NON.equals(candidatureCourante.getFlagEleveForce())) {
            throw new BusinessException("Annulation du for�age impossible car l'�l�ve " + ine
                    + " n'est pas (ou plus) forc� globalement.");
        }

        // Enregistrement dans le journal des op�rations
        LOG.debug("Annulation du for�age de l'�l�ve " + ine + " par l'utilisateur " + uid);
        journalManager.addDeforceEleve(uid, eleve);

        // Indique que l'�l�ve n'est plus forc� globalement pour ce tour
        candidatureCourante.setFlagEleveForce(Flag.NON);
        //recalcul de la validit� de la saisie
        candidatureCourante.setFlagSaisieValide(Flag.pourBooleen(eleveManager.isSaisieValide(candidatureCourante, true)));
        // R�initialise la d�cision finale et le rang d'admission (avant la MAJ)
        candidatureCourante.setRangAdmission(null);
        candidatureCourante.setCodeDecisionFinale(null);

        // Annulation des forcages sur chacun des voeux.
        for (VoeuEleve voeuElv : eleve.voeuxDuTour(campagneManager.getNumeroTourCourant())) {
            annulerForcageVoeu(voeuElv);
        }

        // R�tablit les d�cisions issues du reclassement s'il a d�ja �t� effectu�, sinon les d�cisions par d�faut)
        recalculEtatCandidature(eleve);

        eleveManager.modifier(eleve, ine);
    }

    /**
     * Annule le for�age sur un voeu.<br>
     * Attention, il faut appeler majdecisionFinale si cette m�thode est utilis�e.
     *
     * @see #recalculEtatCandidature(Eleve)
     * @param voeuElv
     *            le voeu de l'�l�ve
     */
    private void annulerForcageVoeu(VoeuEleve voeuElv) {

        Voeu offreFormation = voeuElv.getVoeu();

        // Si l'�l�ve �tait affect� sur ce voeu, on d�cr�mente d'une unit� le nombre
        // d'�l�ve admis dans la pile
        Integer codeDecisionFinaleAvantDeforcage = voeuElv.getCodeDecisionFinal();
        if (codeDecisionFinaleAvantDeforcage != null
                && codeDecisionFinaleAvantDeforcage.equals(DecisionFinale.AFFECTE.getCode())) {
            // nombre admis (-1)
            Pile p = offreFormation.getPile();
            p.setNombreElevesAdmis(p.getNombreElevesAdmis() - 1);
        }

        // code_decisionFinal<--CodeDecisionPostReclassement

        Integer codeDecisionPostReclassement = voeuElv.getCodeDecisionPostReclassement();
        voeuElv.setCodeDecisionFinal(codeDecisionPostReclassement);
        // num�roListeSuppl�mentaire <-- num�roListeSuppl�mentairePostReclassement
        voeuElv.setNumeroListeSuppFinal(voeuElv.getNumeroListeSuppPostReclassement());
        // le voeu n'est pas refus�
        voeuElv.setFlagRefusVoeu(Flag.NON);

        voeuElv.setDateMAJ(new Date());

        // MAJ de la pile
        // si d�cision POST-reclassement �tait affect� (ATTENTION, on peut se retrouver
        // avec plus d'�l�ve!)
        if (codeDecisionPostReclassement != null
                && codeDecisionPostReclassement.equals(DecisionFinale.AFFECTE.getCode())) {
            // nombre admis (-1)
            Pile p = offreFormation.getPile();
            p.setNombreElevesAdmis(p.getNombreElevesAdmis() + 1);
        }

        // On remet la d�cision par d�faut des voeux sur les offres de formation trait�es en commission, hors
        // apprentissage
        if (offreFormation.estTraiteeEnCommission() && !offreFormation.getStatut().isApprentissage()) {
            voeuElv.setCodeDecisionProvisoire(parametreManager.getDecisionParDefaut());
        }
    }

    /**
     * Mise � jour le candidature courante non forc�e de l'�l�ve en fonction ses voeux.<br>
     *
     * Cas possibles :
     * <ol>
     * <li>l'�l�ve est affect� sur un voeu</li>
     * <li>l'�l�ve n'est pas "affect�" et est sur liste compl�mentaire sur un voeu</li>
     * <li>l'�l�ve n'est pas "affect�", pas "liste supp" et est admis sur un voeu
     * <li>l'�l�ve n'est pas "affect�", pas "liste supp", pas "admis" et est en attente de signature sur un voeu
     * <li>l'�l�ve n'est pas "affect�", pas "liste supp", pas "admis", pas "en attente sig" et est refus� sur un
     * voeu
     * l'�l�ve est mis � refus� (uniquement si refus� global)</li>
     * </ol>
     *
     * Si on trouve un voeu avec une d�cision abandon, cela prime sur toutes les d�cisions 'scolaire' (hors "admis"
     * et "en attente sig") => abandon.
     *
     * @param eleve
     *            l'�l�ve
     */
    private void recalculEtatCandidature(Eleve eleve) {

        // On initialise la d�cision finale avec la valeur par d�faut avant la mise � jour
        Candidature candidature = eleveManager.getCandidatureCourante(eleve);

        Integer nvCodeDecisionFinal = null;
        Integer nvRangAdmission = null;

        boolean uniquementRecensement = true;

        // On parcourt les voeux pour d�terminer progressivement la d�cision finale � attribuer � la candidature
        for (VoeuEleve voeuEleve : candidature.getVoeux()) {

            boolean decisionVoeuRecensement = false;

            // On ne consid�re que les voeux ayant une d�cision finale pour cette recherche
            if (voeuEleve != null && voeuEleve.getCodeDecisionFinal() != null) {

                // Etat de la d�cision finale actuelle de la candidature
                boolean cAffecte = nvCodeDecisionFinal != null && DF_AFFECTE.equals(nvCodeDecisionFinal);
                boolean cListeSup = nvCodeDecisionFinal != null && DF_LISTESUP.equals(nvCodeDecisionFinal);
                boolean cRefuse = nvCodeDecisionFinal != null && DF_REFUSE.equals(nvCodeDecisionFinal);
                boolean cAbandon = nvCodeDecisionFinal != null && DF_ABANDON.equals(nvCodeDecisionFinal);
                boolean cAdmis = nvCodeDecisionFinal != null && DF_ADMIS.equals(nvCodeDecisionFinal);
                boolean cAttSig = nvCodeDecisionFinal != null && DF_ATT_SIG.equals(nvCodeDecisionFinal);

                // Met � jour la d�cision finale de la candidature en examinant le voeu indiqu�.
                int codeDecisionFinaleVoeu = voeuEleve.getCodeDecisionFinal();

                // On recherche la d�cision "prioritaire"

                // Voeu affect� non candidature non abandon => candidature affect�e sur le voeu
                if (codeDecisionFinaleVoeu == DF_AFFECTE && !cAbandon) {
                    nvCodeDecisionFinal = DF_AFFECTE;
                    nvRangAdmission = voeuEleve.getId().getRang();
                }

                // Voeu en liste compl�mentaire et candidature non affect�/abandon => candidature en liste supp.
                if (codeDecisionFinaleVoeu == DF_LISTESUP && !cAbandon && !cAffecte) {
                    nvCodeDecisionFinal = DF_LISTESUP;
                }

                // Voeu refus� et candidature non affect�/liste supp =>
                // Candidature refus�e
                if (codeDecisionFinaleVoeu == DF_REFUSE && !cAbandon && !cAffecte && !cListeSup) {
                    nvCodeDecisionFinal = DF_REFUSE;
                }

                // Voeu admis et candidature non affect�/liste sup/refus� => candidature admis (apprentissage) sur
                // le voeu.
                if (codeDecisionFinaleVoeu == DF_ADMIS && !cAffecte && !cListeSup && !cRefuse) {
                    nvCodeDecisionFinal = DF_ADMIS;

                    // Attention ici au nom "trompeur" du rang d'admission qui ne concerne que les �lves "affect�s"
                    // => on le laisse � null
                    nvRangAdmission = null;
                }

                // Voeu en attente signature et candidature non affect�/liste sup/refuse/admis =>
                // candidature en attente signature de contrat (apprentissage)
                if (codeDecisionFinaleVoeu == DF_ATT_SIG && !cAffecte && !cListeSup && !cRefuse && !cAdmis) {
                    nvCodeDecisionFinal = DF_ATT_SIG;
                }

                // Voeu abandon et candidature non abandon/admis/attente signature
                if (codeDecisionFinaleVoeu == DF_ABANDON && !cAbandon && !cAdmis && !cAttSig) {
                    // si le code d�cision de l'�l�ve n'est pas nul et est diff�rent d'abandon il y
                    // a un probl�me
                    if (nvCodeDecisionFinal != null && !nvCodeDecisionFinal.equals(DF_ABANDON)) {
                        throw new BusinessException(EleveManager.class + " majDecisionFinale(Eleve elv) : "
                                + "l'�l�ve " + candidature.getId().getIne()
                                + " a un voeu abandon et une d�cision finale diff�rente");
                    }
                    nvCodeDecisionFinal = DF_ABANDON;
                }

                if (codeDecisionFinaleVoeu == DF_RECENSEMENT) {
                    decisionVoeuRecensement = true;
                }

            } // if -- d�cision pr�sente sur le voeu

            uniquementRecensement &= decisionVoeuRecensement;

        } // for -- voeux � examiner

        if (uniquementRecensement) {
            nvCodeDecisionFinal = DF_RECENSEMENT;
            nvRangAdmission = null;
        }

        candidature.setCodeDecisionFinale(nvCodeDecisionFinal);
        candidature.setRangAdmission(nvRangAdmission);
    }

}
