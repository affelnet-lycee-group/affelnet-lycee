/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.RangDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.nomenclature.RangManager;
import fr.edu.academie.affelnet.utils.CacheUtils;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/** Cette classe regroupe les traitements sur les notes des �l�ves. */
@Service
public class NotesManager extends AbstractManager {

    /** Nombre de rangs en cache. */
    private static final CacheUtils.Init CACHE_NB_RANGS = new CacheUtils.Init() {
        public Object getValue() {
            RangDao dao = SpringUtils.getBean(RangDao.class);
            return dao.getNombreElements();
        }
    };

    /**
     * Le manager utilis� pour les rangs.
     */
    private RangManager rangManager;
    /**
     * Le dao utilis� pour les rangs.
     */
    private RangDao rangDao;

    /**
     * Le dao utilis� pour les �l�ves.
     */
    private EleveDao eleveDao;

    /**
     * @param rangDao
     *            the rangDao to set
     */
    @Autowired
    public void setRangDao(RangDao rangDao) {
        this.rangDao = rangDao;
    }

    /**
     * @param rangManager
     *            the rangManager to set
     */
    @Autowired
    public void setRangManager(RangManager rangManager) {
        this.rangManager = rangManager;
    }

    /**
     * @param eleveDao
     *            the eleveDao to set
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * Cette m�thode fournit le filtre pour les �l�ves de palier 2nde sans note valide.
     * 
     * @return le filtre pour les �l�ves sans note
     */
    public Filtre filtresElevesPalier2ndeSansNote() {
        List<Filtre> filtres = new ArrayList<Filtre>();
        filtres.add(eleveDao.filtreElevesPalierOrigine(Palier.CODE_PALIER_2NDE));
        filtres.add(eleveDao.filtreElevesSansNote(rangManager.rangObligatoire()));
        return Filtre.and(filtres);
    }

    /**
     * V�rifie la pr�sence de notes.
     * 
     * @return true si des notes ont �t� saisies.
     */
    public boolean isNotesSaisies() {
        return eleveDao.isNotesSaisies();
    }

    /**
     * Cette m�thode v�rifie si les notes obligatoires sont compl�t�es pour l'ensemble des �l�ves.
     * 
     * @return vrai si les notes obligatoires sont pr�sentes pour l'ensemble des �l�ves.
     */
    public boolean isNotesOK() {
        Filtre filtre = null;
        return isNotesOK(filtre);
    }

    /**
     * Cette m�thode v�rifie si les notes obligatoires sont compl�t�es pour un �tablissement donn�.
     * 
     * @param etablissement
     *            �tablissement � tester
     * @return vrai si les notes obligatoires sont pr�sentes pour l'�tablissement donn�.
     */
    public boolean isNotesOK(Etablissement etablissement) {
        return isNotesOK(Filtre.equal("etablissementNational.id", etablissement.getId()));
    }

    /**
     * V�rifie que les notes obligatoires de l'�l�ve ont �t� saisies.
     * 
     * @param eleve
     *            �tablissement v�rifi�
     * @return true si aucun probl�me avec les notes ou les �valuations n'a �t� trouv�
     */
    public boolean isNoteObligatoireSaisie(Eleve eleve) {
        for (Rang rang : rangManager.rangObligatoire()) {
            if (eleve.getNote(rang.getValeur()) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Cette m�thode v�rifie si les notes obligatoires sont compl�t�es pour les �l�ves concern�es par le filtre.
     * 
     * @param filtreSupplementaire
     *            Filtre suppl�mentaire
     * @return vrai si les notes obligatoires sont pr�sentes pour les �l�ves du filtre donn�.
     */
    public boolean isNotesOK(Filtre filtreSupplementaire) {
        // On r�cup�re le nombre de rangs
        int nbRangs = rangDao.getNombreElements();

        // Aucun rang => pas de notes => toujours OK
        if (nbRangs == 0) {
            return true;
        }

        // Cr�ation d'un filtre de s�lection de l'�tablissement
        List<Filtre> filtres = new ArrayList<Filtre>();
        if (filtreSupplementaire != null) {
            filtres.add(filtreSupplementaire);
        }

        // On r�cup�re le filtre pour les �l�ves avec bar�me avec �valuations/notes pour lesquels les notes sont
        // invalides
        filtres.add(filtresElevesPalier2ndeSansNote());

        // Les notes sont OK si la liste des �l�ves retourn�e est vide
        return eleveDao.getNombreElements(filtres) == 0;
    }

    /**
     * Retourne le nombre de rangs en cache.
     * 
     * @return le nombre de rangs.
     */
    public Integer getNbRangs() {
        return (Integer) CacheUtils.getValue(CACHE_NB_RANGS);
    }

    /**
     * R�cup�re le rang de la base de donn�es.
     * 
     * @param id
     *            L'identifiant technique du rang.
     * @return Le rang correspondant � l'identifiant.
     */
    public Rang chargerRang(Long id) {
        return rangDao.charger(id);
    }

    /**
     * @param rang
     *            Le rang.
     * @param id
     *            L'identifiant technique du rang.
     * @return
     *         <ul>
     *         <li>true si un autre rang existe avec cette cl� de gestion</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean existeDejaRang(Rang rang, Long id) {
        List<Filtre> listeFiltres = new ArrayList<Filtre>();
        if (id != null) {
            listeFiltres.add(Filtre.notEqual("id", id));
        }
        listeFiltres.add(Filtre.equal("cleGestion", rang.getCleGestion()));

        return rangDao.getNombreElements(listeFiltres) != 0;
    }

    /**
     * @param indice
     *            L'indice du rang.
     * @return
     *         <ul>
     *         <li>true si un rang existe avec cet indince</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean existeRangParIndice(Integer indice) {
        return rangDao.getNombreElements(Filtre.equal("valeur", indice)) != 0;
    }

    /**
     * @return La liste de tous les rangs.
     */
    public List<Rang> listerRangs() {
        return rangDao.lister(false);
    }

    /**
     * M�thode permettant de lister les rangs correspondant aux notes obligatoires.
     * 
     * @return La liste des rangs correspondant aux notes obligatoires
     */
    public List<Rang> listerRangsNotesObligatoires() {
        Filtre filtre = Filtre.equal("autorisationNonNote", Flag.NON);
        return rangDao.lister(filtre);
    }

    /**
     * M�thode permettant de lister les rangs correspondant aux notes facultatives.
     * 
     * @return La liste des rangs correspondant aux notes facultatives
     */
    public List<Rang> listerRangsNotesFacultatives() {
        Filtre filtre = Filtre.equal("autorisationNonNote", Flag.OUI);
        return rangDao.lister(filtre);
    }

    /**
     * @param indice
     *            L'indice du rang � charger.
     * @return Le rang correspondant � l'indice voulu.
     */
    public Rang chargerRangParIndice(Integer indice) {
        if (existeRangParIndice(indice)) {
            return (Rang) rangDao.lister(Filtre.equal("valeur", indice)).get(0);
        }

        return null;
    }

    /**
     * @param rang
     *            Le rang � modifier.
     * @return Le rang modifi�.
     */
    public Rang modifier(Rang rang) {
        return modifier(rang, rang.getId());
    }

    /**
     * @param rang
     *            Le rang � modifier.
     * @param id
     *            L'identifiant technique de ce rang.
     * @return Le rang modifi�.
     */
    public Rang modifier(Rang rang, Long id) {
        return rangDao.maj(rang, id);
    }

    /**
     * @param rang
     *            Le rang � ajouter en base.
     * @return Le rang qui vient d'�tre cr�er.
     */
    public Rang creerRang(Rang rang) {
        return rangDao.creer(rang);
    }
}
