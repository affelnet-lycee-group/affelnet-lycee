/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.VoeuDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.service.nomenclature.BonusFiliereManager;

/**
 * Ce module regroupe les contr�les n�cessaires � PAM, � utiliser :
 * 1- avant l'attribution des groupes (cas PAM normal)
 * 2- avant le reclassement (au cas o� le reclassement est utilis� sans attribution des groupes)
 * 3- avant le calcul des piles en tour suivant.
 */
@Service
public class PAMManager extends AbstractManager {
    /**
     * Le dao utilis� pour les �l�ves.
     */
    private EleveDao eleveDao;

    /**
     * Le dao utilis� pour les voeux.
     */
    private VoeuDao voeuDao;

    /**
     * Le dao utilis� pour les voeux des �l�ves.
     */
    private VoeuEleveDao voeuEleveDao;

    /**
     * Le manager utilis� pour les voeux des �l�ves.
     */
    private VoeuEleveManager voeuEleveManager;

    /**
     * Le manager utilis� pour les bonus fili�re.
     */
    private BonusFiliereManager bonusFiliereManager;

    /**
     * @param eleveDao
     *            the eleveDao to set
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * @param voeuDao
     *            the voeuDao to set
     */
    @Autowired
    public void setVoeuDao(VoeuDao voeuDao) {
        this.voeuDao = voeuDao;
    }

    /**
     * @param voeuEleveDao
     *            the voeuEleveDao to set
     */
    @Autowired
    public void setVoeuEleveDao(VoeuEleveDao voeuEleveDao) {
        this.voeuEleveDao = voeuEleveDao;
    }

    /**
     * @param voeuEleveManager
     *            the voeuEleveManager to set
     */
    @Autowired
    public void setVoeuEleveManager(VoeuEleveManager voeuEleveManager) {
        this.voeuEleveManager = voeuEleveManager;
    }

    /**
     * @param bonusFiliereManager
     *            the bonusFiliereManager to set
     */
    @Autowired
    public void setBonusFiliereManager(BonusFiliereManager bonusFiliereManager) {
        this.bonusFiliereManager = bonusFiliereManager;
    }

    /**
     * Obtenir la liste des �l�ves avec un avis manquant.
     * 
     * @param codeTypeAvis
     *            le type de l'avis concern�
     * @param filtresSupplementaires
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des �l�ves sans avis du type sp�cifi�
     */
    public List<VoeuEleve> listerElevesSansAvis(CodeTypeAvis codeTypeAvis, List<Filtre> filtresSupplementaires) {
        switch (codeTypeAvis) {
            case AVIS_DE_GESTION:
                return voeuEleveManager.listerSansAvisVoeu(filtresSupplementaires);
            case AVIS_DU_CHEF_D_ETABLISSEMENT:
                return voeuEleveManager.listerSansAvisParaMefA(filtresSupplementaires);
            case AVIS_PASSERELLE:
                return voeuEleveManager.listerSansAvisPasserelle(filtresSupplementaires);
            default:
                return null;
        }
    }

    /**
     * M�thode permettant d'avoir la liste des offres de formation de type bar�me avec �valuations/notes
     * qui n'ont pas de param�tres par formation d'accueil.
     * 
     * @param idOpa
     *            l'identifiant de l'OPA � contr�ler
     * @return la liste des offres de formation de type bar�me avec �valuations/notes qui n'ont pas de param�tres
     *         par formation d'accueil
     */
    public List<Voeu> listerVoeuPamNotesSansCoefs(Long idOpa) {
        return voeuDao.listerOffresAvecPriseEnCompteNoteEvalSansPFA(idOpa);
    }

    /**
     * @return la liste des �tablissements des �l�ves pr�sents n'ayant pas de
     *         district.
     * 
     * @see fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao
     */
    public List<Etablissement> listerEtablissementSansDistrictDesElevesPresentsEntite() {
        return eleveDao.listerEtablissementSansDistrictDesElevesPresentsEntite();
    }

    /**
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux ayant une capacit� nulle
     */
    public List<Voeu> listerVoeuxCapaciteNulle(List<Filtre> filtresSupplementaire) {
        // on ne controle pas la capacit� pour les voeux de recensement
        return voeuEleveDao.listerVoeuxCapaciteNulle(filtresSupplementaire);
    }

    /**
     * @return la liste des bonus fili�res de type 2GT &gt; 1GT
     */
    public List<BonusFiliere> listerBonusFilieresInterdits() {
        List<BonusFiliere> listeBonusFilieresDansFiliereExploratoire2Gt1Gt = new ArrayList<BonusFiliere>();
        List<BonusFiliere> listeBonusFilieres2GT = bonusFiliereManager.listeBonusFilieres2GT();

        if (CollectionUtils.isNotEmpty(listeBonusFilieres2GT)) {
            List<String> listeErreursValidation = new ArrayList<String>();
            for (BonusFiliere bonusFiliere2GT : listeBonusFilieres2GT) {
                bonusFiliereManager.validationHorsFiliereExploratoire2Gt1Gt(bonusFiliere2GT,
                        listeErreursValidation);
                if (!listeErreursValidation.isEmpty()) {
                    listeBonusFilieresDansFiliereExploratoire2Gt1Gt.add(bonusFiliere2GT);
                }
            }
        }
        return listeBonusFilieresDansFiliereExploratoire2Gt1Gt;
    }
}
