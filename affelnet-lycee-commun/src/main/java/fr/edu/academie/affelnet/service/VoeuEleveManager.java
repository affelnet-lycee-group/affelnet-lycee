/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.PropositionAccueilDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.affectation.DecisionCommission;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.AvisParFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.resultats.OffreRefuseCritereAleatoire;
import fr.edu.academie.affelnet.domain.voeu.Candidature;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.Passerelles;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuElevePK;
import fr.edu.academie.affelnet.service.nomenclature.AvisParFormationOrigineManager;
import fr.edu.academie.affelnet.service.nomenclature.ParametresFormationAccueilManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.utils.Collecteur;
import fr.edu.academie.affelnet.web.utils.FiltreUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/** Cette classe regroupe les traitements sur les voeux des �l�ves. */
@Service
public class VoeuEleveManager extends AbstractManager {

    /** Modalit� de saisie : la saisie est-elle possible ? */
    public static final int SAISIE_POSSIBLE = 0;

    /** Modalit� de saisie : la saisie est-elle obligatoire ? */
    public static final int SAISIE_OBLIGATOIRE = 1;

    /** Modalit� d'affichage dans le r�capitulatif des �l�ves. */
    public static final int AFFICHAGE_RECAPITULATIF = 3;

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(VoeuEleveManager.class);

    /**
     * Le manager utilis� pour les �l�ves.
     */
    private EleveManager eleveManager;

    /**
     * Le manager utilis� pour les candidatures.
     */
    private CandidatureManager candidatureManager;

    /**
     * Le manager utilis� pour les avis par formation d'origine.
     */
    private AvisParFormationOrigineManager avisParFormationOrigineManager;

    /**
     * Le manager utilis� pour le journal.
     */
    private JournalManager journalManager;

    /**
     * Le manager utilis� pour les campagnes.
     */
    private CampagneAffectationManager campagneManager;

    /** Le gestionnaire de param�tres applicatifs. */
    private ParametreManager parametreManager;

    /**
     * Le manager utilis� pour les OPA.
     */
    private OperationProgrammeeAffectationManager opaManager;

    /**
     * Le manager utilis� pour les propositions d'accueil.
     */
    private PropositionAccueilDao propositionAccueilDao;

    /**
     * Le dao utilis� pour les voeux des �l�ves.
     */
    private VoeuEleveDao voeuEleveDao;

    /**
     * Le dao utilis� pour les �l�ves.
     */
    private EleveDao eleveDao;

    /**
     * @param eleveManager
     *            the eleveManager to set
     */
    @Autowired
    public void setEleveManager(EleveManager eleveManager) {
        this.eleveManager = eleveManager;
    }

    /**
     * @param candidatureManager
     *            the candidatureManager to set
     */
    @Autowired
    public void setCandidatureManager(CandidatureManager candidatureManager) {
        this.candidatureManager = candidatureManager;
    }

    /**
     * @param avisParFormationOrigineManager
     *            the avisParFormationOrigineManager to set
     */
    @Autowired
    public void setAvisParFormationOrigineManager(AvisParFormationOrigineManager avisParFormationOrigineManager) {
        this.avisParFormationOrigineManager = avisParFormationOrigineManager;
    }

    /**
     * @param journalManager
     *            the journalManager to set
     */
    @Autowired
    public void setJournalManager(JournalManager journalManager) {
        this.journalManager = journalManager;
    }

    /**
     * @param campagneAffectationManager
     *            the campagneAffectationManager to set
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneManager = campagneAffectationManager;
    }

    /**
     * @param parametreManager
     *            le gestionnaire de param�tres
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param opaManager
     *            the opaManager to set
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * @param propositionAccueilDao
     *            the propositionAccueilDao to set
     */
    @Autowired
    public void setPropositionAccueilDao(PropositionAccueilDao propositionAccueilDao) {
        this.propositionAccueilDao = propositionAccueilDao;
    }

    /**
     * @param voeuEleveDao
     *            the voeuEleveDao to set
     */
    @Autowired
    public void setVoeuEleveDao(VoeuEleveDao voeuEleveDao) {
        this.voeuEleveDao = voeuEleveDao;
    }

    /**
     * @param eleveDao
     *            the eleveDao to set
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /** @return filtre sur les voeux �l�ve pour lesquels il manque l'avis necessaire li� au voeu. */
    public Filtre getFiltreAvisVoeuNecessaireEtManquant() {
        Filtre avisVoeuNecessaire = getFiltreNecessiteAvisVoeu(Flag.OUI);
        Filtre avisVoeuNonValue = getFiltreValuationAvis(CodeTypeAvis.AVIS_DE_GESTION, Flag.NON);
        return Filtre.and(avisVoeuNecessaire, avisVoeuNonValue);
    }

    /**
     * M�thode de r�cup�ration des VoeuEleves selon certains crit�res.
     *
     * @param filtres
     *            les crit�res de s�lection des VoeuEleve
     * @return la liste des VoeuEleves s�lectionn�s
     */
    public List<VoeuEleve> listerVoeuxEleve(List<Filtre> filtres) {
        return voeuEleveDao.lister(filtres);
    }

    /**
     * M�thode de chargement d'un voeu d'un �l�ve.
     *
     * @param clef
     *            la clef du voeu
     * @return le voeu d'�l�ve charg�
     */
    public VoeuEleve charger(VoeuElevePK clef) {
        return voeuEleveDao.charger(clef);
    }

    /**
     * M�thode de modification d'un voeu d'un �l�ve.
     *
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @return le voeu de l'�l�ve modifi�
     */
    public VoeuEleve modifier(VoeuEleve voeuEleve) {
        opaManager.obligerRelanceOpa(voeuEleve.getVoeu());
        Eleve eleve = voeuEleve.getEleve();
        Candidature candidatureCourante = voeuEleve.getCandidature();
        if (candidatureCourante != null) {
            if (eleveManager.isSaisieValide(candidatureCourante.getEleve(), false)) {
                candidatureCourante.setFlagSaisieValide(Flag.OUI);
            } else {
                candidatureCourante.setFlagSaisieValide(Flag.NON);
            }

            candidatureManager.modifier(candidatureCourante);
        }

        return voeuEleve;
    }

    /**
     * @param flagNecessite
     *            flag de n�cessite de la saisie Flag.OUI / Flag.NON
     * @return filtre sur les voeux �l�ve concernant la n�cessit� d'un avis li� au voeu.
     */
    public Filtre getFiltreNecessiteAvisVoeu(String flagNecessite) {

        Filtre avisVoeuNecessaire = null;

        // L'avis est requis selon le param�trage de la nomenclature voeu
        avisVoeuNecessaire = Filtre.equal("voeu.flagAvisEnt", Flag.OUI);

        if (flagNecessite.equals(Flag.OUI)) {
            return avisVoeuNecessaire;

        } else if (flagNecessite.equals(Flag.NON)) {
            return Filtre.not(avisVoeuNecessaire);

        } else {
            throw new IllegalArgumentException("Param�tre flag de n�cessit� invalide " + flagNecessite);
        }
    }

    /**
     * M�thode de cr�ation du filtre des �l�ves n�cessitant un avis passerelle.
     *
     * @return le filtre de s�lection des �l�ves n�cessitant un avis passerelle.
     */
    public Filtre getFiltreNecessiteAvisPasserelle() {
        String codeMefstat4Origine = "eleve.formation.codeMefStat4";
        String codeMefstat4Accueil = "voeu.formationAccueil.codeMefStat4";

        Map<String, List<String>> passerelles = Passerelles.getPasserelles();
        List<Filtre> filtresPasserelles = new ArrayList<>();

        for (Entry<String, List<String>> passerelle : passerelles.entrySet()) {
            Filtre filtreOrigine = Filtre.equal(codeMefstat4Origine, passerelle.getKey());
            Filtre filtreAccueil = Filtre.in(codeMefstat4Accueil, passerelle.getValue());
            filtresPasserelles.add(Filtre.and(filtreOrigine, filtreAccueil));
        }

        return Filtre.or(filtresPasserelles);
    }

    /**
     * @param typeAvis
     *            le type d'avis concern�
     * @param flagValuation
     *            �tat de valuation Flag.OUI / Flag.NON
     * @return filtre sur les voeux �l�ve concernant la valuation d'un avis.
     */
    public Filtre getFiltreValuationAvis(CodeTypeAvis typeAvis, String flagValuation) {

        String champAvis = null;

        switch (typeAvis) {
            case AVIS_DSDEN:
                champAvis = "avisDSDEN";
                break;
            case AVIS_DE_GESTION:
                champAvis = "avisEntretien";
                break;
            case AVIS_DU_CHEF_D_ETABLISSEMENT:
                champAvis = "avisConseilClasse";
                break;
            case AVIS_PASSERELLE:
                champAvis = "avisPasserelle";
                break;
            default:
                throw new IllegalArgumentException("Type d'avis incorrect : " + typeAvis);
        }

        if (flagValuation.equals(Flag.OUI)) {
            return Filtre.isNotNull(champAvis);
        } else if (flagValuation.equals(Flag.NON)) {
            return Filtre.isNull(champAvis);
        } else {
            throw new IllegalArgumentException("Etat de valuation invalide " + flagValuation);
        }
    }

    /**
     * Cr�e un filtre s�lectionnant les voeux de palier 3�me
     * dans le tour courant faits par des �l�ves
     * pour lesquels le palier troisi�me ne devrait pas �tre accessible.
     *
     * @return filtre cr��
     */
    public Filtre getFiltreIncompatFormationOrigine() {
        List<Filtre> filtres = new ArrayList<>();

        filtres.add(Filtre.equal("voeu.saisieAutorisee3EME", Flag.OUI));
        filtres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        filtres.add(Filtre.equal("eleve.formation.mefStat.mefstat4.flagAccesOffresPalier3eme", Flag.NON));

        return Filtre.and(filtres);
    }

    /**
     * R�cup�re l'ensemble des voeux incompatibles formation d'origine pour un �tablissement d'origine donn�.
     *
     * @param idEtablissement
     *            l'�tablissement o� chercher les voeux
     * @return une map de listes de voeux class�es par INE de l'�l�ve
     */
    public Map<String, List<VoeuEleve>> getVoeuxIncompatFormationOrigine(String idEtablissement) {
        // R�cup�ration des voeux
        List<Filtre> filtresVoeux = new ArrayList<>();
        filtresVoeux.add(this.getFiltreIncompatFormationOrigine());
        filtresVoeux.add(Filtre.equal("eleve.etablissementNational.id", idEtablissement));
        List<VoeuEleve> voeuxEleve = voeuEleveDao.lister(filtresVoeux);

        // R�cup�ration des INE concern�s
        Map<String, List<VoeuEleve>> voeux = new HashMap<>();
        for (VoeuEleve voeu : voeuxEleve) {
            String ine = voeu.getId().getIne();
            if (!voeux.containsKey(ine)) {
                List<VoeuEleve> newVoeux = new ArrayList<>();
                voeux.put(ine, newVoeux);
            }
            voeux.get(ine).add(voeu);
        }
        return voeux;
    }

    /**
     * R�cup�re l'ensemble des voeux incompatibles formation d'origine pour l'ensemble des �tablissements.
     * 
     * @return La liste des voeux incompatibles tri�e par �tablissement + tri par d�faut.
     */
    public List<VoeuEleve> getVoeuxIncompatFormationOrigineSuivi() {
        // R�cup�ration des voeux
        List<Filtre> filtresVoeux = new ArrayList<>();
        filtresVoeux.add(this.getFiltreIncompatFormationOrigine());

        // Tri par etablissement
        List<Tri> tris = new ArrayList<>();
        tris.add(Tri.asc("eleve.etablissementNational.id"));
        voeuEleveDao.completeAvecTrisParDefaut(tris);

        return voeuEleveDao.lister(filtresVoeux, tris);
    }

    /**
     * Indique si une modalite s'applique pour un avis.
     *
     * @param typeAvis
     *            le type d'avis concern�
     * @param codeModalite
     *            modalit� � tester (VoeuEleveManager.SAISIE_POSSIBLE ou SAISIE_OBLIGATOIRE)
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @param accesGestionnaire
     *            vrai si l'acc�s est administratif (IA, rectorat, admin), sinon faux
     * @return vrai si la saisie de l'avis est requise, faux sinon
     */
    public boolean modaliteAvis(CodeTypeAvis typeAvis, int codeModalite, VoeuEleve voeuEleve,
            boolean accesGestionnaire) {
        switch (typeAvis) {
            case AVIS_DSDEN:
                return modaliteAvisDSDEN(codeModalite, accesGestionnaire);
            case AVIS_DE_GESTION:
                return modaliteAvisGestion(codeModalite, voeuEleve, accesGestionnaire);
            case AVIS_DU_CHEF_D_ETABLISSEMENT:
                return modaliteAvisChefEtab(codeModalite, voeuEleve, accesGestionnaire);
            case AVIS_PASSERELLE:
                // Pas de saisie optionnelle : obligatoire ou non
                return necessiteAvisPasserelle(voeuEleve, accesGestionnaire);
            default:
                return false;
        }
    }

    /**
     * M�thode d�finissant si une modalite s'applique pour l'avis DSDEN.
     *
     * @param codeModalite
     *            la modalit� � tester
     * @param accesGestionnaire
     *            vrai si l'acc�s est administratif (IA, rectorat, admin), sinon faux
     * @return vrai si la modalit� s'applique sur l'avis, faux sinon
     */
    private boolean modaliteAvisDSDEN(int codeModalite, boolean accesGestionnaire) {
        return accesGestionnaire && codeModalite != SAISIE_OBLIGATOIRE;
    }

    /**
     * Indique si l'avis passerelle doit �tre saisi selon l'acc�s.
     *
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @param accesGestionnaire
     *            vrai si l'acc�s est administratif (IA, rectorat, admin), sinon faux
     * @return vrai si la saisie de l'avis est possible et obligatoire, faux sinon
     */
    protected boolean necessiteAvisPasserelle(VoeuEleve voeuEleve, boolean accesGestionnaire) {

        if (!accesGestionnaire) {
            return false;
        }

        String codeMefstat4Origine = voeuEleve.getEleve().getFormation().getCodeMefStat4();
        String codeMefstat4Accueil = voeuEleve.getVoeu().getFormationAccueil().getCodeMefStat4();

        return Passerelles.estPasserelle(codeMefstat4Origine, codeMefstat4Accueil);
    }

    /**
     * Indique si une modalite s'applique pour l'avis du chef d'�tablissement d'origine.
     *
     * @param codeModalite
     *            modalit� � tester (VoeuEleveManager.SAISIE_POSSIBLE ou SAISIE_OBLIGATOIRE)
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @param accesGestionnaire
     *            vrai si l'acc�s est administratif (IA, rectorat, admin), sinon faux
     * @return vrai si la saisie de l'avis du chef d'�tablissement d'origine
     */
    private boolean modaliteAvisChefEtab(int codeModalite, VoeuEleve voeuEleve, boolean accesGestionnaire) {

        Eleve eleve = voeuEleve.getEleve();
        Voeu voeu = voeuEleve.getVoeu();

        // Le gestionnaire peut toujours saisir l'avis
        if (accesGestionnaire && ((codeModalite == SAISIE_POSSIBLE) || codeModalite == AFFICHAGE_RECAPITULATIF)) {
            return true;
        }

        // Pour savoir si l'avis doit �tre saisi, il faut d�terminer le paramefa qui s'applique
        // on dispose pour cela d'indicateurs dans la pile qui mettent en cache ces informations pour chaque voeu.

        String indicateurSaisieAvis = voeu.getPile().getIndicateurSaisieAvis();

        // La saisie de l'avis est toujours requise pour cette formation d'accueil
        if (ParametresFormationAccueilManager.IND_SAISIE_AVIS_TOUJOURS.equals(indicateurSaisieAvis)) {
            return true;
        }

        // La saisie de l'avis n'est jamais requise pour cette formation d'accueil
        if (ParametresFormationAccueilManager.IND_SAISIE_AVIS_JAMAIS.equals(indicateurSaisieAvis)) {
            return false;
        }

        // La saisie de l'avis n'est requise pour cette formation d'accueil pour certaines formations d'origine de
        // l'�l�ve
        if (ParametresFormationAccueilManager.IND_SAISIE_AVIS_SELON_FORMATION_ORIGINE
                .equals(indicateurSaisieAvis)) {

            // Chargement des informations sur l'origine de l'�l�ve pour comparaison
            Formation mefEleve = eleve.getFormation();

            // Chargement du param�tre par formation d'accueil qui s'applique
            // pour r�cup�rer la liste des formations d'origine
            ParametresFormationAccueil pfa = voeu.getPile().getParametreFormationAccueil();
            Set<AvisParFormationOrigine> formationsOrigine = pfa.getAvisParFormationOrigines();

            boolean correspondanceOptionsOrigine;

            // Comparaison entre les formations d'origine et les infos de l'�l�ve
            for (AvisParFormationOrigine avisParMefO : formationsOrigine) {
                Formation mefO = avisParMefO.getFormation();

                // NOTE : Si le code sp�cialit� est null -> on a utilis� le $
                if (mefEleve.equals(mefO) || (avisParMefO.getCodeSpecialite() == null
                        && StringUtils.equals(avisParMefO.getMnemonique(), mefEleve.getId().getMnemonique()))) {

                    correspondanceOptionsOrigine = avisParFormationOrigineManager
                            .verifieOptionsAvisParFormationOrigine(avisParMefO, eleve);

                    // Si on a trouv� une correspondance (MEF origine + options),
                    // il n'est pas n�cessaire de poursuivre la recherche.
                    if (correspondanceOptionsOrigine) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Indique si une modalite s'applique pour l'avis sur le voeu (gestion IA ou �tablissement d'accueil).
     *
     * @param codeModalite
     *            modalit� � tester (VoeuEleveManager.SAISIE_POSSIBLE ou SAISIE_OBLIGATOIRE)
     * @param voeuEleve
     *            voeu de l'�l�ve
     * @param accesGestionnaire
     *            vrai si l'acc�s est administratif (IA, rectorat, admin), sinon faux
     * @return vrai si la modalit� demand�e s'applique, sinon faux
     */
    private boolean modaliteAvisGestion(int codeModalite, VoeuEleve voeuEleve, boolean accesGestionnaire) {

        Voeu voeuChoisi = voeuEleve.getVoeu();

        // Si l'on en rectorat ou en IA ou en admin, on *peut* toujours saisir l'avis
        if (accesGestionnaire && ((codeModalite == SAISIE_POSSIBLE) || codeModalite == AFFICHAGE_RECAPITULATIF)) {
            return true;
        }

        String flagAvisVoeu = voeuChoisi.getFlagAvisEnt();
        return Flag.OUI.equals(flagAvisVoeu);
    }

    /**
     * V�rifie si une valeur d'avis est surchargeable et affichable.
     * Attention, ici on se concentre sur la valeur et on suppose que l'on d�j� a le droit
     * d'afficher / saisir la valeur de l'avis.
     *
     * @param typeAvis
     *            le type de l'avis concern�
     * @param voeuEleve
     *            le voeu de l'�l�ve
     * @param accesGestionnaire
     *            vrai si l'acc�s est administratif (IA, rectorat, admin), sinon faux
     * @return vrai si on peut afficher la valeur de l'avis ou en saisir une nouvelle, faux sinon
     */
    public boolean avisAffichableEtSurchargeable(CodeTypeAvis typeAvis, VoeuEleve voeuEleve,
            boolean accesGestionnaire) {

        Avis valeurAvisCourante = voeuEleve.getAvisEntretien();

        // L'avis saisi par l'IA prime, il ne peut �tre �cras� par l'etablissement.
        // Par contre si l'avis a �t� saisi par l'�tablissment, il doit pouvoir le resaisir.

        // On peut �craser si on est gestionnaire ou pour les avis de gestion et du chef d'�tablissement
        // d'origine si la valeur est nulle ou saisissable en �tablissement.
        // Les avis DSDEN et passerelle ne sont pas surchargeables en �tablissement.
        return accesGestionnaire || CodeTypeAvis.AVIS_DSDEN != typeAvis && CodeTypeAvis.AVIS_PASSERELLE != typeAvis
                && ((valeurAvisCourante == null)
                        || (valeurAvisCourante.getAffichageEtablissement().equals(Flag.OUI)));
    }

    /**
     * M�thode mettant � jour le flag de validation par l'administration d'une d�rogation pour parcours scolaire
     * particulier.
     *
     * @param voeuElevePK
     *            La cl� composite du pojo VoeuEleve.
     * @param flagValidationParcoursScolaireParticulier
     *            La nouvelle valeur du flag.
     */
    public void miseAjourFlagParcoursScolaire(VoeuElevePK voeuElevePK,
            String flagValidationParcoursScolaireParticulier) {
        VoeuEleve voeuEleve = voeuEleveDao.charger(voeuElevePK);

        // La modification n'est effectu�e que si la valeur du flag a chang�
        if (!StringUtils.equals(voeuEleve.getFlagValidationParcoursScolaireParticulier(),
                flagValidationParcoursScolaireParticulier)) {

            voeuEleve.setFlagValidationParcoursScolaireParticulier(flagValidationParcoursScolaireParticulier);

            modifier(voeuEleve);
        }
    }

    /**
     * Teste si un �l�ve a un voeu donn�.
     *
     * @param ine
     *            INE de l'�l�ve
     * @param codeVoeu
     *            code du voeu
     * @return vrai si l'�l�ve a ce voeu, sinon faux
     */
    public boolean testeExistenceVoeuEleve(String ine, String codeVoeu) {

        List<Filtre> listeFiltres = new ArrayList<>();
        listeFiltres.add(Filtre.equal("id.ine", ine));
        listeFiltres.add(Filtre.equal("voeu.code", codeVoeu));
        List<VoeuEleve> listeVoeux = voeuEleveDao.lister(listeFiltres);

        return !listeVoeux.isEmpty();
    }

    /**
     * @param filtresSupplementaires
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux des �l�ves sans avis de gestion
     */
    public List<VoeuEleve> listerSansAvisVoeu(List<Filtre> filtresSupplementaires) {
        // Filtres :
        List<Filtre> listeFiltres = new ArrayList<>();
        listeFiltres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        // - avis n�cessaire et manquant
        listeFiltres.add(getFiltreAvisVoeuNecessaireEtManquant());
        // - �l�ves non forc�s
        listeFiltres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));

        if (filtresSupplementaires != null) {
            listeFiltres.addAll(filtresSupplementaires);
        }

        // tris de la liste
        List<Tri> listeTris = new ArrayList<>();
        listeTris.add(Tri.asc("eleve.nom"));
        listeTris.add(Tri.asc("eleve.prenom"));
        listeTris.add(Tri.asc("id.ine"));
        listeTris.add(Tri.asc("id.rang"));

        return voeuEleveDao.lister(listeFiltres, listeTris);
    }

    /**
     * @param filtresSupplementaires
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux des �l�ves sans avis de chef d'�tablissement
     */
    public List<VoeuEleve> listerSansAvisParaMefA(List<Filtre> filtresSupplementaires) {
        // Filtres :
        List<Filtre> listeFiltres = new ArrayList<>();
        listeFiltres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        listeFiltres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));
        listeFiltres.add(Filtre.equal("candidature.flagValidationAvis", Flag.NON));
        listeFiltres.add(Filtre.isNull("avisConseilClasse"));

        if (filtresSupplementaires != null) {
            listeFiltres.addAll(filtresSupplementaires);
        }

        List<Tri> listeTris = new ArrayList<>();
        listeTris.add(Tri.asc("eleve.etablissementNational.id"));
        listeTris.add(Tri.asc("eleve.nom"));
        listeTris.add(Tri.asc("eleve.prenom"));
        listeTris.add(Tri.asc("id.ine"));
        listeTris.add(Tri.asc("id.rang"));

        List<VoeuEleve> listeSansAvis = voeuEleveDao.lister(listeFiltres, listeTris);
        List<VoeuEleve> result = new ArrayList<>();

        // parcours de tous les voeux �l�ves sans avis, pour chacun : v�rification de la saisie requise
        for (VoeuEleve voeuEleve : listeSansAvis) {
            if (modaliteAvis(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT, VoeuEleveManager.SAISIE_OBLIGATOIRE,
                    voeuEleve, true)) {
                result.add(voeuEleve);
            }
        }
        return result;
    }

    /**
     * @param filtresSupplementaires
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux des �l�ves sans avis passerelle
     */
    public List<VoeuEleve> listerSansAvisPasserelle(List<Filtre> filtresSupplementaires) {

        // Filtres des �l�ves du tour non forc�s
        List<Filtre> listeFiltres = new ArrayList<>();
        listeFiltres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        listeFiltres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));

        // Les voeux sans avis passerelles alors qu'ils sont requis
        listeFiltres.add(getFiltreNecessiteAvisPasserelle());
        listeFiltres.add(Filtre.isNull("avisPasserelle"));

        if (filtresSupplementaires != null) {
            listeFiltres.addAll(filtresSupplementaires);
        }

        List<Tri> listeTris = new ArrayList<>();
        listeTris.add(Tri.asc("eleve.etablissementNational.id"));
        listeTris.add(Tri.asc("eleve.nom"));
        listeTris.add(Tri.asc("eleve.prenom"));
        listeTris.add(Tri.asc("id.ine"));
        listeTris.add(Tri.asc("id.rang"));

        return voeuEleveDao.lister(listeFiltres, listeTris);
    }

    /**
     * Liste les identifiants des �tablissement AFFECTATION utilis�s dans les voeux d'�l�ves.
     *
     * @return liste des identifiants UAIs concern�s
     */
    public List<String> listerIdEtablissementsAffectationUtilises() {
        return voeuEleveDao.listerIdEtablissementsAffectationUtilises();
    }

    /**
     * M�thode permettant de savoir si un �l�ve a d�j� saisi un voeu.
     *
     * @return vrai si au moins un �l�ve a saisi un voeu, faux sinon
     */
    public boolean existeVoeuEleve() {
        List<VoeuEleve> listeVoeuxEleves = voeuEleveDao.lister(null, 1, 2);
        return listeVoeuxEleves != null && !listeVoeuxEleves.isEmpty();
    }

    /**
     * Modifie la proposition d'accueil d'un �l�ve.
     *
     * @param uid
     *            Identifiant de l'utilisateur effectuant l'op�ration
     * @param ine
     *            INE de l'�l�ve concern�
     * @param codeVoeuPropositionAjoutee
     *            code voeu � ajouter (�ventuellement null)
     * @param codeVoeuPropositionSupprimee
     *            code voeu � supprimer (�ventuellement null)
     */
    public void modifierPropositionAccueil(String uid, String ine, String codeVoeuPropositionAjoutee,
            String codeVoeuPropositionSupprimee) {

        Eleve eleve = eleveManager.chargerParIne(ine);

        if (StringUtils.isNotEmpty(codeVoeuPropositionSupprimee)) {

            // Suppression de la proposition d'accueil
            LOG.debug("Suppression de la proposition d'accueil " + codeVoeuPropositionSupprimee);
            journalManager.addSuppressionPropositionAccueil(uid, eleve, codeVoeuPropositionSupprimee);
        }

        if (StringUtils.isNotEmpty(codeVoeuPropositionAjoutee)) {

            // Ajout de la proposition d'accueil
            LOG.debug("Ajout de la proposition d'accueil " + codeVoeuPropositionAjoutee);
            journalManager.addAjoutPropositionAccueil(uid, eleve, codeVoeuPropositionAjoutee);
        }

        // Modification de la proposition d'accueil
        propositionAccueilDao.modifPropositionAccueil(eleve, codeVoeuPropositionAjoutee,
                codeVoeuPropositionSupprimee);
    }

    /**
     * Donne le nombre d'�l�ves concern�s par les voeux correspondant aux filtres fournis.
     *
     * @param listFiltresVoeu
     *            filtres sur les voeux des �l�ve
     * @return nombre d'�l�ves concern�s
     */
    public int getNbElevesConcernesPourFiltreVoeu(List<Filtre> listFiltresVoeu) {
        List<Filtre> filtresVoeu = new ArrayList<>();
        filtresVoeu.addAll(listFiltresVoeu);
        filtresVoeu.add(Filtre.eqPropertiesPere("id.ine", "eleve.ine"));

        Filtre filtreEleve = Filtre.exists(VoeuEleve.class, Filtre.and(filtresVoeu));
        return eleveDao.getNombreElements(filtreEleve);
    }

    /**
     * M�thode permettant de r�cup�rer la liste des voeux �l�ves utilisant une offre de formation
     * r�serv�e aux �l�ves de 3�me SEGPA, formul�s par des �l�ves qui ne sont pas en 3�me SEGPA.
     *
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux �l�ves utilisant un voeu r�serv� aux �l�ves de 3�me SEGPA
     */
    public List<VoeuEleve> voeuxElevesReserves3SEGPAIncorrects(List<Filtre> filtresSupplementaire) {
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }

        filtres.add(Filtre.equal("voeu.flagReserve3SEGPA", Flag.OUI));
        filtres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));
        filtres.add(Filtre.equal("flagRefusVoeu", Flag.NON));
        filtres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        filtres.add(Filtre.notEqual("eleve.formation.mefStat.codeMefStat4", MefStat.CODE_MEFSTAT4_3SEGPA));

        // Ajout des fetchs join pour r�cup�rer les donn�es � afficher
        List<String> elementsFetchJoin = new ArrayList<>();
        elementsFetchJoin.add("eleve");
        elementsFetchJoin.add("eleve.formation");
        elementsFetchJoin.add("eleve.optionOrigine2");
        elementsFetchJoin.add("eleve.optionOrigine3");
        elementsFetchJoin.add("eleve.etablissementNational");
        elementsFetchJoin.add("eleve.departement");
        elementsFetchJoin.add("voeu");
        elementsFetchJoin.add("voeu.formationAccueil");
        elementsFetchJoin.add("voeu.statut");
        return voeuEleveDao.lister(filtres, null, null, null, elementsFetchJoin, true);
    }

    /**
     * M�thode permettant de r�cup�rer la liste des voeux d�rogatoires sans motif.
     *
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux �l�ves
     */
    public List<VoeuEleve> voeuxDerogatoiresSansMotif(List<Filtre> filtresSupplementaire) {
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }

        filtres.add(Filtre.equal("flagVoeuDerogation", Flag.OUI));
        filtres.add(Filtre.sizeEgale("derogationsVoeuEleve", 0));
        filtres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        filtres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));
        filtres.add(Filtre.equal("flagRefusVoeu", Flag.NON));

        // Ajout des fetchs join pour r�cup�rer les donn�es � afficher
        List<String> elementsFetchJoin = new ArrayList<>();
        elementsFetchJoin.add("eleve");
        elementsFetchJoin.add("eleve.formation");
        elementsFetchJoin.add("eleve.optionOrigine2");
        elementsFetchJoin.add("eleve.optionOrigine3");
        elementsFetchJoin.add("eleve.etablissementNational");
        elementsFetchJoin.add("eleve.departement");
        elementsFetchJoin.add("voeu");
        elementsFetchJoin.add("voeu.formationAccueil");

        return voeuEleveDao.lister(filtres, null, null, null, elementsFetchJoin, true);
    }

    /**
     * @return le nombre de voeux sur des offres trait�es en commission pour le tour courant
     */
    public int getNbVoeuxElevesCommissionPourTour() {
        List<Filtre> filtres = new ArrayList<>();
        FiltreUtils.ajouterCritereEgal(filtres, "id.numeroTour", campagneManager.getNumeroTourCourant());
        FiltreUtils.ajouterCritereEgal(filtres, "voeu.indicateurPam", TypeOffre.COMMISSION.getIndicateur());
        return voeuEleveDao.getNombreElements(filtres);
    }

    /**
     * Collecte les voeux des �l�ves concern�s par le filtre.
     *
     * @param filtre
     *            le filtre concern�
     * @param listeFectJoin
     *            Liste des �l�ments n�cessitant une jointure fecth
     * @return collecteur des voeux �l�ve concern�s
     */
    public Collecteur<VoeuEleve> collecterVoeuxEleves(Filtre filtre, List<String> listeFectJoin) {
        return collecterVoeuxEleves(Arrays.asList(filtre), null, listeFectJoin);
    }

    /**
     * Collecte les voeux concern�s par les filtres.
     * 
     * @param filtres
     *            la liste des filtres
     * @param tris
     *            la liste des tris
     * @param fetchList
     *            la liste des sous-�l�ment charg�s
     * @return un collecteur des voeux
     */
    public Collecteur<VoeuEleve> collecterVoeuxEleves(List<Filtre> filtres, List<Tri> tris,
            List<String> fetchList) {
        return voeuEleveDao.collecteur(filtres, tris, null, null, fetchList, false);
    }

    /**
     * Teste s'il existe des voeux avec une d�cision finale, hors for�age (pour le tour actuel).
     *
     * @return vrai s'il existe des r�sultats finaux non forc�s, sinon faux
     */
    public boolean existeResultatsFinauxHorsForcage() {

        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        filtres.add(Filtre.equal("candidature.flagEleveForce", Flag.NON));
        filtres.add(Filtre.equal("flagRefusVoeu", Flag.NON));
        filtres.add(Filtre.isNotNull("codeDecisionFinal"));

        return voeuEleveDao.getNombreElements(filtres) > 0;

    }

    /**
     * Donne le filtre sur les voeux d'�l�ves dont les d�cisions finales sont "non affect�".
     *
     * @return filtre sur les voeux d'�l�ves non affect�s
     *         Attention, ce filtre inclut les voeux d'�l�ves "non trait�s".
     */
    public Filtre filtreVoeuxElevesNonAffectesNonTraites() {

        // Ensemble des d�cisions finales pour les �l�ves non affect�s.
        int[] codeDecisionNonAffectesComplets = { DecisionFinale.LISTE_SUPP.getCode(),
                DecisionFinale.REFUSE.getCode(), DecisionFinale.NON_TRAITE.getCode() };

        return Filtre.in("candidature.codeDecisionFinale", codeDecisionNonAffectesComplets);
    }

    /**
     * Fournit un filtre pour r�cup�rer l'ensemble des voeux �l�ves pour une OPA.
     *
     * @param idOpa
     *            l'ID de l'OPA
     * @return un filtre sur les voeux �l�ves de l'OPA
     */
    public Filtre filtreVoeuxElevesPourOPA(long idOpa) {
        List<Filtre> filtres = new ArrayList<>();

        OperationProgrammeeAffectation opa = opaManager.charger(idOpa);

        if (opa == null) {
            LOG.error("Op�ration programm�e d'affectation introuvable  : idOpa = " + idOpa);
            throw new DaoException("L'op�ration programm�e d'affectation demand�e n'existe pas.");
        }

        // Pour le tour principal, on effectue le lien avec entre l'OPA et les offres de formation
        if (opa.getNumeroTourSuivant() == CampagneAffectationManager.NUMERO_TOUR_PRINCIPAL) {
            filtres.add(Filtre.equal("voeu.opaTourPrincipal.id", idOpa));
        }
        // Pour le tour suivant, toutes les offres de formations sont inclues dans l'OPA
        // Par contre, on r�cup�re celles dont la capacit� d'affectation est strictement positive
        else {
            filtres.add(Filtre.superieur("voeu.pile.capaciteAffectation", 0));
        }

        filtres.add(Filtre.equal("id.numeroTour", opa.getNumeroTourSuivant().shortValue()));

        return Filtre.and(filtres);
    }

    /**
     * Fournit un filtre pour r�cup�rer l'ensemble des voeux des �l�ves hors offres de recensement pour une OPA.
     * On ignore
     * - les voeux de recensement
     * - les voeux li�s � une autre OPA
     *
     * @param idOpa
     *            l'ID de l'OPA
     * @return un filtre sur les voeux �l�ves n�cessitant un calcul du bar�me de l'OPA
     */
    public Filtre filtreVoeuxElevesPourOPASansRecensement(long idOpa) {
        List<Filtre> filtres = new ArrayList<>();

        // Filtre pour les voeux el�ve de l'OPA
        filtres.add(filtreVoeuxElevesPourOPA(idOpa));

        filtres.add(Filtre.equal("voeu.flagRecensement", Flag.NON));
        return Filtre.and(filtres);
    }

    /**
     * Donne le nombre de voeux �l�ve pour le filtre fourni.
     *
     * @param filtre
     *            le filtre HQL
     * @return Le nombre de voeu correspondant
     */
    public int compterVoeuEleve(Filtre filtre) {
        return voeuEleveDao.getNombreElements(filtre);
    }

    /**
     * Donne le nombre de voeux pour lesquels il manque une d�cision finale.
     *
     * @param numeroTour
     *            le num�ro du tour
     * @return nombre de voeux concern�s
     */
    public Integer getNombreDecisionsFinalesManquantes(Short numeroTour) {
        return voeuEleveDao.getNombreDecisionsFinalesManquantes(numeroTour);
    }

    /**
     * D�termine si le voeu n�cessite un calcul du bar�me.
     * Ne n�cessite pas de calcul du bar�me si :
     * - le voeu n'est pas forc�
     * - la candidature n'est pas forc�e
     * - l'offre de formation est une offre de recensement
     * - l'offre de formation est trait�e en comission
     *
     * @param voeuEleve
     *            le voeu de l'�l�ve test�
     * @return true si n�cessite un calcul du bar�me
     */
    public boolean isAvecCalculBareme(VoeuEleve voeuEleve) {
        Voeu voeu = voeuEleve.getVoeu();
        return voeuEleve.getFlagRefusVoeu().equals(Flag.NON) && !voeu.estRecensement()
                && !voeu.getIndicateurPam().equals(TypeOffre.COMMISSION.getIndicateur())
                && voeuEleve.getCandidature().getFlagEleveForce().equals(Flag.NON);
    }

    /**
     * Cette m�thode monte d'un rang le voeu d'un �l�ve.
     *
     * @param uid
     *            l'UID de l'utilisateur connect� ou le nom du service (saisie simplifi�e / saisie individuelle)
     * @param voeuMonte
     *            le <code>VoeuEleve</code> � monter
     */
    public void monterVoeu(String uid, VoeuEleve voeuMonte) {

        VoeuElevePK id = voeuMonte.getId();
        int rang = id.getRang();
        Eleve eleve = voeuMonte.getEleve();

        LOG.debug("Mont�e du voeu de rang " + rang + " pour l'�l�ve " + id.getIne());

        List<VoeuEleve> voeux = eleve.voeuxDuTour(campagneManager.getNumeroTourCourant());

        // Recherche du voeu pr�c�dent
        for (VoeuEleve voeu : voeux) {
            if (voeu.getId().getRang().equals(voeuMonte.getId().getRang() - 1)) {
                permutterVoeux(voeuMonte, voeu);

                // Enregistrement de l'op�ration dans le journal
                journalManager.addMonterVoeuEleve(uid, voeuMonte);
                // Relance des OPA
                opaManager.obligerRelanceOpa(voeu.getVoeu());
                opaManager.obligerRelanceOpa(voeuMonte.getVoeu());
                break;
            }
        }
    }

    /**
     * Cette m�thode descend d'un rang le voeu d'un �l�ve.
     *
     * @param uid
     *            l'UID de l'utilisateur connect� ou le nom du service (saisie
     *            simplifi�e / saisie individuelle)
     * @param voeuDescendu
     *            le <code>VoeuEleve</code> � descendre
     */
    public void descendreVoeu(String uid, VoeuEleve voeuDescendu) {

        VoeuElevePK id = voeuDescendu.getId();
        int rang = id.getRang();
        Eleve eleve = voeuDescendu.getEleve();

        LOG.debug("Descente du voeu de rang " + rang + " pour l'�l�ve " + id.getIne());

        List<VoeuEleve> voeux = eleve.voeuxDuTour(campagneManager.getNumeroTourCourant());

        // Mise � jour du rang du voeu suivant
        for (VoeuEleve voeu : voeux) {
            if (voeu.getId().getRang().equals(voeuDescendu.getId().getRang() + 1)) {
                permutterVoeux(voeu, voeuDescendu);

                // Enregistrement de l'op�ration dans le journal
                journalManager.addDescendreVoeuEleve(uid, voeuDescendu);
                break;
            }
        }
    }

    /**
     * M�thode permettant de permutter deux voeux d'un �l�ve.
     *
     * @param voeuMontant
     *            le voeu montant
     * @param voeuDescendant
     *            le voeu descendant
     */
    private void permutterVoeux(VoeuEleve voeuMontant, VoeuEleve voeuDescendant) {

        // On met � jour le rang d'admission de la candidature
        Candidature candidature = voeuMontant.getCandidature();
        Integer rangAdmission = candidature.getRangAdmission();
        if (voeuMontant.getId().getRang().equals(candidature.getRangAdmission())) {
            candidature.setRangAdmission(rangAdmission - 1);
        } else if (voeuDescendant.getId().getRang().equals(candidature.getRangAdmission())) {
            candidature.setRangAdmission(rangAdmission + 1);
        }

        ResultatsProvisoiresOpa resultatsOpaVoeuMontant = voeuMontant.getResultatsProvisoiresOpa();
        VoeuElevePK idVoeuMontant = voeuMontant.getId();

        ResultatsProvisoiresOpa resultatsOpaVoeuDescendant = voeuDescendant.getResultatsProvisoiresOpa();
        VoeuElevePK idVoeuDescendant = voeuDescendant.getId();

        HibernateUtil.cleanupSession();
        HibernateUtil.currentSession().refresh(voeuMontant);
        HibernateUtil.currentSession().refresh(voeuDescendant);

        voeuEleveDao.supprimer(voeuMontant);
        voeuEleveDao.supprimer(voeuDescendant);

        candidature.getVoeux().remove(voeuMontant);
        candidature.getVoeux().remove(voeuDescendant);

        // Actualisation de l'�l�ve

        HibernateUtil.currentSession().flush();

        voeuMontant.setId(idVoeuDescendant);
        if (resultatsOpaVoeuMontant != null) {
            resultatsOpaVoeuMontant.setId(idVoeuDescendant);
        }
        voeuDescendant.setId(idVoeuMontant);
        if (resultatsOpaVoeuDescendant != null) {
            resultatsOpaVoeuDescendant.setId(idVoeuMontant);
        }

        candidature.getVoeux().add(idVoeuDescendant.getRang() - 1, voeuMontant);
        candidature.getVoeux().add(idVoeuMontant.getRang() - 1, voeuDescendant);

        candidatureManager.modifier(candidature);
        HibernateUtil.currentSession().flush();
    }

    /**
     * Initialise la d�cision prise en commission.
     * 
     * <strong>Attention, cette m�thode est aussi appel�e en saisie des voeux (g�n�rale et simplifi�e)
     * lorsque l'on revient modifier un voeu existant.</strong>
     * 
     * @param voeuEleve
     *            le voeu � initialiser
     */
    public void initialisationDecisionCommission(VoeuEleve voeuEleve) {

        String nouvelleDecisionCommission = null;

        Voeu offreFormation = voeuEleve.getVoeu();
        if (offreFormation.estTraiteeEnCommission()) {
            // Dans le cas d'une offre de formation trait�e en commission

            if (offreFormation.estRecensement()) {

                // Pour les offres de recensement
                nouvelleDecisionCommission = DecisionCommission.RECENSEMENT.getCode();

            } else if (offreFormation.getStatut().isApprentissage()) {
                // Pour les offres d'apprentissage en trait�es en commission

                if (DecisionCommission.ADMIS_CONTRAT_SIGNE.getCode()
                        .equals(voeuEleve.getCodeDecisionProvisoire())) {

                    // Dans le cas particulier d'une mise � jour, il faut pr�server une �ventuelle connaissance
                    // de la signature du contrat
                    nouvelleDecisionCommission = DecisionCommission.ADMIS_CONTRAT_SIGNE.getCode();

                } else {

                    // La valeur par d�faut est "En attente de signature du contrat"
                    // (m�me si le voeu est en modification)
                    nouvelleDecisionCommission = DecisionCommission.EN_ATTENTE_SIGNATURE_CONTRAT.getCode();
                }

            } else {

                // L'offre est sous statut scolaire
                // On positionne la d�cision par d�faut (m�me si le voeu est en modification)
                nouvelleDecisionCommission = parametreManager.getDecisionParDefaut();

            } // if -- statut de l'offre

        } // if -- traitement en commission

        voeuEleve.setCodeDecisionProvisoire(nouvelleDecisionCommission);

        // On remet toujours � null le num�ro de liste compl�mentaire
        voeuEleve.setNumeroListeSuppProvisoire(null);
    }

    /**
     * Fournit les filtres pour obtenir tous les �l�ves dont le bar�me est �gal au dernier admis pour l'offre et le
     * tour donn�.
     * 
     * @param numTour
     *            le num�ro du tour
     * @param codeVoeu
     *            le code de l'offre de formation
     * @return la liste des filtres sur les voeux des �l�ves
     */
    public List<Filtre> filtreVoeuxEleveEgaliteDernierAdmis(short numTour, String codeVoeu) {
        List<Filtre> filtresAdmission = new ArrayList<>();
        filtresAdmission.add(Filtre.isNull("candidature.rangAdmission"));
        filtresAdmission.add(Filtre.inferieurOuEgalProperties("id.rang", "candidature.rangAdmission"));

        List<Filtre> filtresOffreEleveRef = new ArrayList<>();
        filtresOffreEleveRef.add(Filtre.equal("id.numeroTour", numTour));
        filtresOffreEleveRef.add(Filtre.eqPropertiesPere("id.codeOffreFormation", "voeuEleve.voeu.code"));

        List<Filtre> filtres = new ArrayList<>();
        if (StringUtils.isNotBlank(codeVoeu)) {
            filtres.add(Filtre.equal("voeu.code", codeVoeu));
        }
        filtres.add(Filtre.equal("id.numeroTour", numTour));
        filtres.add(Filtre.equal("voeu.pile.id.numeroTour", numTour));
        filtres.add(Filtre.epProperties("resultatsProvisoiresOpa.bareme", "voeu.pile.baremeDernierAdmis"));
        filtres.add(Filtre.or(filtresAdmission));
        filtres.add(Filtre.exists(OffreRefuseCritereAleatoire.class, Filtre.and(filtresOffreEleveRef)));
        return filtres;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des voeux ayant une candidature non conforme.
     *
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des voeux �l�ves ayant une candidature non conforme
     */
    public List<VoeuEleve> voeuxEleveCandidatureNonConforme(List<Filtre> filtresSupplementaire) {
        List<Filtre> filtres = new ArrayList<>();
        if (filtresSupplementaire != null) {
            filtres.addAll(filtresSupplementaire);
        }
        filtres.add(Filtre.equal("id.numeroTour", campagneManager.getNumeroTourCourant()));
        filtres.add(Filtre.equal("candidature.flagConformeDo", Flag.NON));
        filtres.add(Filtre.equal("eleve.palierOrigine.code", Palier.CODE_PALIER_3EME));
        filtres.add(
                Filtre.hql("exists (select 1 from DecisionOrientationEleve doe where doe.id.ine = o.eleve.ine)"));

        // gestion des tris
        List<Tri> tris = new ArrayList<>();
        tris.add(Tri.asc("eleve.departement.codeMen"));
        tris.add(Tri.asc("eleve.etablissementNational.id"));
        tris.add(Tri.asc("eleve.nom"));
        tris.add(Tri.asc("eleve.prenom"));
        tris.add(Tri.asc("eleve.prenom2"));
        tris.add(Tri.asc("eleve.prenom3"));
        tris.add(Tri.asc("id.rang"));

        List<VoeuEleve> voeuxEleve = voeuEleveDao.lister(filtres, tris);
        return removeVoeuxEleveConformeFromListe(voeuxEleve);
    }

    /**
     * M�thode permettant de filtrer la liste des voeux ayant une candidature non conforme
     * pour ne garder que ceux en non conformit� avec les d�cisions d'orientation de l'�l�ve.
     *
     * @param voeuxEleve
     *            la liste des voeux �l�ves ayant une candidature non conforme
     * @return la liste des voeux �l�ves en non conformit� vis � vis des d�cisions d'orientation de l'�l�ve.
     */
    public List<VoeuEleve> removeVoeuxEleveConformeFromListe(List<VoeuEleve> voeuxEleve) {
        List<VoeuEleve> voeuxEleveNonConformes = new ArrayList<>();
        for (VoeuEleve voeuEleve : voeuxEleve) {
            if (!eleveManager.evalueConformiteVoeu(voeuEleve)) {
                // si non conforme, on le garde
                voeuxEleveNonConformes.add(voeuEleve);
            }
        }

        return voeuxEleveNonConformes;
    }

    /**
     * Supprime tous les voeux Affelnet des �l�ves ayant des voeux T�l�service.
     * 
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    public void supprimerVoeuxEleves(int tour, int palier) {
        voeuEleveDao.supprimerVoeuxEleves(tour, palier);
    }

    /**
     * Supprime toutes les candidatures des �l�ves ayant des voeux T�l�service.
     * 
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    public void supprimerCandidaturesEleves(int tour, int palier) {
        voeuEleveDao.supprimerCandidaturesEleves(tour, palier);
    }

    /**
     * Supprime toutes les d�rogations des �l�ves ayant des voeux T�l�service.
     * 
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    public void supprimerDerogationsEleves(int tour, int palier) {
        voeuEleveDao.supprimerDerogationsEleves(tour, palier);
    }

    /**
     * Supprime tous les r�sultats provisoires des Opa des �l�ves ayant des voeux T�l�service.
     * 
     * @param tour
     *            le num�ro du tour
     * @param palier
     *            le palier de l'�l�ve
     */
    public void supprimerResultatsProvisoiresOpaEleves(int tour, int palier) {
        voeuEleveDao.supprimerResultatsProvisoiresOpaEleves(tour, palier);
    }

    public boolean isSaisieVoeuApprentissageDemarre() {
        return this.voeuEleveDao.isSaisieVoeuApprentissageDemarre();
    }

    /**
     * V�rifie que le voeu n'a pas un doublon pour la m�me offre de formation � un autre rang.
     * 
     * @param voeuEleve
     *            le voeu dont on recherche un doublon dans les voeux existants
     */
    public void valideNonDoublon(VoeuEleve voeuEleve) {

        String codeOffreFormationTestee = voeuEleve.getVoeu().getCode();
        Integer rangVoeuTeste = voeuEleve.getId().getRang();

        // On parcourt les voeux de l'�l�ve pour le tour courant.
        List<VoeuEleve> voeuxDuTour = voeuEleve.getEleve().voeuxDuTour(campagneManager.getNumeroTourCourant());
        for (VoeuEleve voeuExistant : voeuxDuTour) {

            Integer rangVoeuExistant = voeuExistant.getId().getRang();
            String codeOffreVoeuExistant = voeuExistant.getVoeu().getCode();

            // On ne doit pas avoir le code d'offre utilis� � un rang diff�rent
            if (!rangVoeuExistant.equals(rangVoeuTeste)
                    && codeOffreVoeuExistant.equals(codeOffreFormationTestee)) {
                throw new ValidationException("L'offre de formation " + codeOffreFormationTestee
                        + " a d�j� �t� saisie au rang " + rangVoeuExistant);
            }
        }
    }

}
