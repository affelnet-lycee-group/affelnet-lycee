/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.echange;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.domain.evaluation.lsu.DemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.RetourLsu;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.LsunAffelnet;
import fr.edu.academie.affelnet.utils.exceptions.TechnicalException;

/**
 * Service g�rant les �changes avec le livret scolaire unique.
 */
@Service
public interface ImportLsuService {

    /**
     * Envoi de la demande asynchrone pour la g�n�ration des �valuations.
     * 
     * @param demande
     *            classe repr�sentant l'�tat de la demande d'�valuation
     * @param annee
     *            ann�e correspondante
     * @return le retour de l'appel � LSU
     * @throws TechnicalException
     *             Si le service n'arrive pas � envoyer la requ�te
     */
    public RetourLsu demandeGenerationEvaluationAsync(DemandeEvaluation demande, Integer annee)
            throws TechnicalException;

    /**
     * Demande de t�l�chargement de l'�valuation g�n�r�e.
     * 
     * @param codeUai
     *            id de l'�tablissmement
     * @param uuid
     *            id du document
     * @return l'InputSteam contenant le document
     * @throws TechnicalException
     *             Exception g�n�r�e si le service n'a pas pu �tre contact�
     */
    public InputStream downloadEvaluation(String codeUai, String uuid) throws TechnicalException;


    /**
     * Alimente les classes correspondant au fichier XML des �valuations LSU.
     * 
     * @param bytes
     *            tableau de byte contenant le fichier XML
     * @return les classes g�n�r�es
     * @throws JAXBException
     *             si le fichier est mal form�
     * @throws IOException
     *             si le stream d'entr�e est null
     */
    public LsunAffelnet generateClass(byte[] bytes) throws JAXBException, IOException;
}
