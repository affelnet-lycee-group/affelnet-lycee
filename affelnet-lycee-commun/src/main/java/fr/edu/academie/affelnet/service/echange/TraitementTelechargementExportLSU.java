/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.echange;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ScrollableResults;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.DataAccessException;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationDiscipline;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSoclePK;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CompteRenduEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CorrespondanceEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.DemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EtatDemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationBilanPeriodeLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationDisciplineLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationEleveLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationSocleLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.Acquis;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.EleveXml;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.EleveXml.BilansPeriodiques.BilanPeriodique;
import fr.edu.academie.affelnet.domain.evaluation.lsu.xml.LsunAffelnet;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DegreMaitrise;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.ConfigurationManager;
import fr.edu.academie.affelnet.service.EleveManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationDisciplineManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationSocleManager;
import fr.edu.academie.affelnet.service.evaluation.lsu.CorrespondanceEvaluationManager;
import fr.edu.academie.affelnet.service.evaluation.lsu.DemandeEvaluationManager;
import fr.edu.academie.affelnet.utils.ChaineUtils;
import fr.edu.academie.affelnet.utils.LsuUtils;
import fr.edu.academie.affelnet.utils.exceptions.BusinessException;
import fr.edu.academie.affelnet.utils.exceptions.TechnicalException;

/**
 * Classe pour l'appel asynchrone des t�l�chargements LSU.
 */
public class TraitementTelechargementExportLSU implements Runnable {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(TraitementTelechargementExportLSU.class);

    /** Nombre d'it�rations avant un flush de session Hibernate. */
    private static final int NB_ITERATIONS_FLUSH = 100;

    /** id de l'�tablissement � l'origine de la demande. */
    private String numeroUai;

    /** Service g�rant les traitements avec LSU. */
    private ImportLsuService importLsuService;

    /** Service pour acc�der aux propri�t�s de l'application. */
    private ConfigurationManager configurationManager;

    /** Service pour la gestion des demandes d'�valuation. */
    private DemandeEvaluationManager demandeEvaluationManager;

    /** Service pour la gestion des correspondances �valuation/groupe niveau. */
    private CorrespondanceEvaluationManager correspondanceEvaluationManager;

    /** Service pour la gestion des �l�ves. */
    private EleveManager eleveManager;

    /** Service pour la gestion des �valuations du socle. */
    private EvaluationSocleManager evaluationSocleManager;

    /** Service pour la gestion des �valuations du socle. */
    private EvaluationDisciplineManager evaluationDisciplineManager;

    /** Constructeur par d�faut. */
    public TraitementTelechargementExportLSU() {
        super();
    }

    /**
     * Constructeur de la classe avec la notification.
     * 
     * @param numeroUai
     *            l'identifiant de l'�tablissement
     */
    public TraitementTelechargementExportLSU(String numeroUai) {
        this();
        this.numeroUai = numeroUai;
    }

    @Override
    public void run() {
        // Ouverture de la session Hibernate
        HibernateUtil.openSession();
        LOG.info("D�but du traitement asynchrone pour la r�ception des �valuations");

        DemandeEvaluation demandeEvaluation = demandeEvaluationManager.charger(numeroUai);

        // r�cup�ration du fichier XML
        // Conversion de l'input stream en tableau de byte pour r�utilisation
        try (InputStream stream = importLsuService.downloadEvaluation(numeroUai, demandeEvaluation.getUuid());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();) {

            org.apache.commons.io.IOUtils.copy(stream, baos);
            byte[] bytes = baos.toByteArray();

            enregistrerfichier(bytes);
            // Transformation du fichier sous un format connue de Affelnet
            LsunAffelnet lsunAffelnet = importLsuService.generateClass(bytes);

            initialierCompteRendu(demandeEvaluation, lsunAffelnet);
            // Traitement pour l"int�gration
            if (demandeEvaluation.getFlagIntegration().equals(Flag.OUI)) {
                LOG.info("Purge des tables d'accueil");
                demandeEvaluation.getEleves().clear();
                demandeEvaluation.alimenterEvaluation(lsunAffelnet);

                LOG.info("Alimentation des tables d'accueil");
                // Enregistrement de la demande et des �valuations
                demandeEvaluationManager.maj(demandeEvaluation);

                // On v�rifie qu'il n'y a pas de correspondance non aliment�e
                if (!ajoutNvCorrespondance(demandeEvaluation)
                        && correspondanceEvaluationManager.compteCorresNonResolue(numeroUai) == 0) {
                    integrationEvaluation(demandeEvaluation);
                    compteursEleveNonIntegres(demandeEvaluation);
                } else {
                    throw new BusinessException("De nouvelles �valuations sont pr�sentes dans le fichier."
                            + " Veuillez les compl�ter dans le menu 'Pr�paration des �valuations'");
                }
            }
            // Traitement pour la pr�paration
            else {
                // Alimentation de la table de correspondance
                ajoutNvCorrespondance(lsunAffelnet, demandeEvaluation);
            }

            demandeEvaluation.setDateSucces(new Date());
            demandeEvaluation.setEtat(EtatDemandeEvaluation.OK);
            demandeEvaluationManager.maj(demandeEvaluation);
            LOG.info("Succ�s du t�l�chargement pour l'�tablissement " + numeroUai);

        } catch (TechnicalException | BusinessException e) {
            String msgException = e.getMessage();
            gestionErreur(msgException, e);
        } catch (IOException e) {
            String msgException = "Le fichier � d�compresser n'a pas �t� trouv�.";
            gestionErreur(msgException, e);
        } catch (JAXBException e) {
            String msgException = "Le fichier fourni est mal format� et ne peut �tre interpr�t�.";
            gestionErreur(msgException, e);
        } catch (DataAccessException | DaoException e) {
            String msgException = "Un probl�me a �t� rencontr� lors de l'enregistrement"
                    + " des informations en base de donn�es.";
            gestionErreur(msgException, e);
        } catch (Exception e) {
            String msgException = "Erreur lors de la r�ception et du traitement des �valuations LSU";
            gestionErreur(msgException, e);
        } finally {
            // fermeture de la session
            HibernateUtil.closeSession();
        }
    }

    /**
     * Stocke sur le serveur le fichier re�u.
     * 
     * @param bytes
     *            le fichier re�u en tant que tableau de byte
     */
    private void enregistrerfichier(byte[] bytes) {
        String nomFichier = null;
        Date dateActuelle = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("YYYYMMdd_HHmmss");

        LOG.info("Enregistrement du fichier sur le serveur");
        try {
            nomFichier = configurationManager.getConfigurationLocale().getRepertoireLsu()
                    + System.getProperty("file.separator") + numeroUai + "_" + formater.format(dateActuelle)
                    + ".zip";

            FileUtils.writeByteArrayToFile(new File(nomFichier), bytes);
        } catch (TechnicalException | IOException e) {
            LOG.error("Erreur lors de l'enregistrement du fichier en provenance de LSU", e);
        }
    }

    /**
     * Initialise le compte-rendu et renseigne les premi�res informations.
     * 
     * @param demandeEvaluation
     *            La demande d'�valuation
     * @param lsunAffelnet
     *            les donn�es re�ues
     */
    private void initialierCompteRendu(DemandeEvaluation demandeEvaluation, LsunAffelnet lsunAffelnet) {
        if (lsunAffelnet != null && lsunAffelnet.getDonnees() != null
                && lsunAffelnet.getDonnees().getEleves() != null
                && lsunAffelnet.getDonnees().getEleves().getEleve() != null) {
            demandeEvaluation.getCompteRenduEvaluation()
                    .setNbEleve(lsunAffelnet.getDonnees().getEleves().getEleve().size());
        }
    }

    /**
     * Effectue le traitement final de l'int�gration des �valuations LSU.
     * 
     * @param demandeEvaluation
     *            demande � int�grer
     */
    private void integrationEvaluation(DemandeEvaluation demandeEvaluation) {
        Map<String, EvaluationEleveLSU> eleveEvalMap = eleveSetToMap(demandeEvaluation.getEleves());
        CompteRenduEvaluation compteRendu = demandeEvaluation.getCompteRenduEvaluation();
        if (eleveEvalMap.size() == 0) {
            throw new BusinessException(
                    "Aucun �l�ve n'a �t� r�cup�r� pour l'�tablissement " + demandeEvaluation.getIdEtablissement());
        }

        String ine = null;
        Eleve eleve = null;
        Map<String, CompetenceSocle> competencesMap = evaluationSocleManager.mapCompetencesSocle();
        Map<Integer, DegreMaitrise> degresMap = evaluationSocleManager.mapDegreMaitrise();
        Map<Discipline, EvaluationDiscipline> disciplines = null;
        int compteurSocle;

        ScrollableResults scrollEleve = eleveManager.scrollElevePalier3emePourEtab(numeroUai);
        while (scrollEleve.next()) {
            eleve = (Eleve) scrollEleve.get(0);
            ine = eleve.getIne();
            int nbTraite = 0;

            if (eleveEvalMap.containsKey(ine)) {
                EvaluationEleveLSU eleveEval = eleveEvalMap.get(ine);

                // Socle
                compteurSocle = genererEvaluationSocle(eleve, eleveEval.getSocle(), competencesMap, degresMap);

                // Mise � jour du compte-rendu
                if (compteurSocle == competencesMap.size()) {
                    compteRendu.incrementerNbSocleComplet();
                } else {
                    compteRendu.addEleveSocleIncompletDemande(eleve.toStringAvecDivision());
                }

                // Disciplines
                disciplines = evaluationDisciplineManager.genererEvaluationDiscipline(eleve, eleveEval.getBilans(),
                        numeroUai, compteRendu);

                // Mise � jour du compte-rendu
                if (compteurSocle > 0 && disciplines != null && !disciplines.isEmpty()) {
                    compteRendu.incrementerNbEleveIntegreAvecEvaluationEtSocle();
                }

                eleveEval.setFlagIntegre(Flag.OUI);

                eleveManager.modifier(eleve);

                if (++nbTraite % NB_ITERATIONS_FLUSH == 0) {
                    HibernateUtil.cleanupSession();
                }
            } else {
                compteRendu.addEleveAbsentLsu(eleve.toStringAvecDivision());
            }
        }
        scrollEleve.close();
    }

    /**
     * G�n�res les �valuations du socle de l'�l�ve.
     * 
     * @param eleve
     *            �l�ve �valu�
     * @param socleLsuSet
     *            L'ensemble des �valuations du socle re�us de LSU
     * @param competencesMap
     *            La map liant les comp�tences du socle avec leur code
     * @param degresMap
     *            La map liant les degr�s de ma�trise avec leur code
     * @return Le nombre de comp�tences du socle alliment�es
     */
    private int genererEvaluationSocle(Eleve eleve, Set<EvaluationSocleLSU> socleLsuSet,
            Map<String, CompetenceSocle> competencesMap, Map<Integer, DegreMaitrise> degresMap) {
        int compteurSocle = 0;
        Map<CompetenceSocle, EvaluationSocle> evalSocleMap = eleve.getEvaluationsSocle();
        for (EvaluationSocleLSU socleLsu : socleLsuSet) {
            if (socleLsu.getDegre() != null) {

                CompetenceSocle competence = competencesMap.get(socleLsu.getId().getCompetence());
                DegreMaitrise degre = degresMap.get(socleLsu.getDegre());

                if (degre != null && competence != null) {
                    EvaluationSocle evalSocle = evalSocleMap.get(competence);

                    if (evalSocle == null) {
                        evalSocle = new EvaluationSocle();
                        evalSocle.setId(new EvaluationSoclePK(eleve, competence));
                    }
                    evalSocle.setDegre(degre);
                    evalSocleMap.put(competence, evalSocle);
                }
                // On incr�mente le compteur m�me si le degr� de ma�trise n'est pas trouv� (et
                // donc dispens�)
                compteurSocle++;
            }
        }
        return compteurSocle;
    }

    /**
     * M�thode alimentant les compteurs du compte-rendu pour inclure les �l�ves
     * pr�sents dans le fichiers mais non int�gr�s dans Affelnet. On r�cup�re : -
     * les �l�ves absents d'Affelnet
     * 
     * @param demandeEvaluation
     *            la demande d'�valuation trait�e
     */
    private void compteursEleveNonIntegres(DemandeEvaluation demandeEvaluation) {
        CompteRenduEvaluation compteRendu = demandeEvaluation.getCompteRenduEvaluation();
        for (EvaluationEleveLSU eleve : demandeEvaluation.getEleves()) {
            if (Flag.NON.equals(eleve.getFlagIntegre())) {
                // Ajout aux �l�ves absents d'Affelnet
                compteRendu.addEleveAbsentAffelnet(eleve.toString());
            }
        }
    }

    /**
     * Transforme l'ensemble des �l�ves en un ensemble index� par l'INE.
     * 
     * @param eleves
     *            l'ensemble des �l�ves
     * @return une map liant les �l�ves et leur INE
     */
    private Map<String, EvaluationEleveLSU> eleveSetToMap(Set<EvaluationEleveLSU> eleves) {
        Map<String, EvaluationEleveLSU> eleveMap = new HashMap<>();
        for (EvaluationEleveLSU eleve : eleves) {
            eleveMap.put(eleve.getIne(), eleve);
        }
        return eleveMap;
    }

    /**
     * Log l'erreur rencontr�e et passe la demande en erreur.
     * 
     * @param msgException
     *            Message enregistr� avec la demande
     * @param e
     *            Exception rencontr�e
     */
    private void gestionErreur(String msgException, Exception e) {
        LOG.error(msgException, e);
        DemandeEvaluation demandeEvaluation = demandeEvaluationManager.charger(numeroUai);
        demandeEvaluation.setEtat(EtatDemandeEvaluation.ERREUR);
        demandeEvaluation.setMsgNotification(msgException);
        demandeEvaluationManager.maj(demandeEvaluation);
    }

    /**
     * Cr�er les correspondances pour les nouvelles �valuations.
     * 
     * @param demandeEvaluation
     *            Demande contenant les �valuations.
     * @return true si des correspondances ont �t� ajout�es
     */
    private boolean ajoutNvCorrespondance(DemandeEvaluation demandeEvaluation) {
        Set<String> valEvalDemandeSet = recupererEvalDemandeCourante(demandeEvaluation);
        return ajoutNvCorrespondance(valEvalDemandeSet);
    }

    /**
     * Cr�er les correspondances pour les nouvelles �valuations.
     * 
     * @param lsunAffelnet
     *            Classe contenant les informations du fichier XML.
     * @param demandeEvaluation
     *            Demande contenant les �valuations.
     * @return true si des correspondances ont �t� ajout�es
     */
    private boolean ajoutNvCorrespondance(LsunAffelnet lsunAffelnet, DemandeEvaluation demandeEvaluation) {
        Set<String> valEvalDemandeSet = recupererEvalDemandeCourante(lsunAffelnet, demandeEvaluation);
        return ajoutNvCorrespondance(valEvalDemandeSet);
    }

    /**
     * Cr�er les correspondances pour les nouvelles �valuations.
     * 
     * @param valEvalDemandeSet
     *            Liste des valeurs des �valuations de la demande courante.
     * @return true si des correspondances ont �t� ajout�es
     */
    private boolean ajoutNvCorrespondance(Set<String> valEvalDemandeSet) {
        int i = 0;
        List<String> valEvalAnterieures = correspondanceEvaluationManager.chargerEvalParEtab(numeroUai);
        CorrespondanceEvaluation correspondance;
        for (String valEvalDemande : valEvalDemandeSet) {
            if (!containsIgnoreCase(valEvalAnterieures, valEvalDemande)) {
                correspondance = new CorrespondanceEvaluation(numeroUai, valEvalDemande);
                correspondanceEvaluationManager.creer(correspondance);
                i++;
                LOG.debug("Ajout de la correspondance : " + valEvalDemande);
            }
        }
        LOG.info("Ajout de " + i + " correspondance(s)");
        return i > 0;
    }

    /**
     * M�thode permettant de savoir si une cha�ne de caract�re est contenue dans une
     * liste de String � la casse pr�s.
     * 
     * @param strList
     *            Liste de String
     * @param value
     *            valeur recherch�e
     * @return vrai si la valeur est contenue dans la liste � la casse pr�s
     */
    private boolean containsIgnoreCase(Collection<String> strList, String value) {
        for (String str : strList) {
            if (str.equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * R�cup�re la liste des valeurs des �valuations ne correspondant pas � une note
     * sur 20 pour une demande.
     * 
     * @param demandeEvaluation
     *            la demande d'�valuation
     * @return le set des valeurs des �valuations.
     */
    private Set<String> recupererEvalDemandeCourante(DemandeEvaluation demandeEvaluation) {
        Set<String> valEvalDemandeSet = new HashSet<>();
        String valeur;
        for (EvaluationEleveLSU eleve : demandeEvaluation.getEleves()) {
            for (EvaluationBilanPeriodeLSU bilan : eleve.getBilans()) {
                for (EvaluationDisciplineLSU discipline : bilan.getDisciplines()) {
                    valeur = discipline.getValeur();
                    if (StringUtils.isNotBlank(valeur) && !LsuUtils.estNoteSur20(valeur)
                            && !containsIgnoreCase(valEvalDemandeSet, valeur)) {
                        valEvalDemandeSet.add(valeur);

                        // Mise � jour du compte-rendu
                        demandeEvaluation.getCompteRenduEvaluation().addElevePourCorrespondance(valeur,
                                eleve.toString());
                    } else if (containsIgnoreCase(valEvalDemandeSet, valeur)) {
                        // Mise � jour du compte-rendu
                        demandeEvaluation.getCompteRenduEvaluation().addElevePourCorrespondance(valeur,
                                eleve.toString());
                    }
                }
            }
        }
        return valEvalDemandeSet;
    }

    /**
     * R�cup�re la liste des valeurs des �valuations ne correspondant pas � une note
     * sur 20 pour une demande.
     * 
     * @param lsunAffelnet
     *            les informations du fichier XML
     * @param demandeEvaluation
     *            Demande contenant les �valuations.
     * @return le set des valeurs des �valuations.
     */
    private Set<String> recupererEvalDemandeCourante(LsunAffelnet lsunAffelnet,
            DemandeEvaluation demandeEvaluation) {
        Set<String> valEvalDemandeSet = new HashSet<>();
        String valEval;
        for (EleveXml eleve : lsunAffelnet.getDonnees().getEleves().getEleve()) {
            for (BilanPeriodique bilan : eleve.getBilansPeriodiques().getBilanPeriodique()) {
                for (Acquis acquis : bilan.getAcquis()) {
                    valEval = ChaineUtils.conversionUtf8VersIso15(acquis.getNote());
                    if (StringUtils.isNotBlank(valEval) && !LsuUtils.estNoteSur20(valEval)
                            && !containsIgnoreCase(valEvalDemandeSet, valEval)) {
                        valEvalDemandeSet.add(valEval);

                        // Mise � jour du compte-rendu
                        demandeEvaluation.getCompteRenduEvaluation().addElevePourCorrespondance(valEval,
                                eleve.toString());
                    } else if (containsIgnoreCase(valEvalDemandeSet, valEval)) {
                        // Mise � jour du compte-rendu
                        demandeEvaluation.getCompteRenduEvaluation().addElevePourCorrespondance(valEval,
                                eleve.toString());
                    }
                }
            }
        }
        return valEvalDemandeSet;
    }

    /**
     * @param importLsuService
     *            the importLsuService to set
     */
    public void setImportLsuService(ImportLsuService importLsuService) {
        this.importLsuService = importLsuService;
    }

    /**
     * @return the importLsuService
     */
    public ImportLsuService getImportLsuService() {
        return importLsuService;
    }

    /**
     * @param demandeEvaluationManager
     *            the demandeEvaluationManager to set
     */
    public void setDemandeEvaluationManager(DemandeEvaluationManager demandeEvaluationManager) {
        this.demandeEvaluationManager = demandeEvaluationManager;
    }

    /**
     * @return the numeroUai
     */
    public String getNumeroUai() {
        return numeroUai;
    }

    /**
     * @param numeroUai
     *            the numeroUai to set
     */
    public void setNumeroUai(String numeroUai) {
        this.numeroUai = numeroUai;
    }

    /**
     * @param correspondanceEvaluationManager
     *            the correspondanceEvaluationManager to set
     */
    public void setCorrespondanceEvaluationManager(
            CorrespondanceEvaluationManager correspondanceEvaluationManager) {
        this.correspondanceEvaluationManager = correspondanceEvaluationManager;
    }

    /**
     * @param configurationManager
     *            the configurationManager to set
     */
    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    /**
     * @param eleveManager
     *            the eleveManager to set
     */
    public void setEleveManager(EleveManager eleveManager) {
        this.eleveManager = eleveManager;
    }

    /**
     * @param evaluationSocleManager
     *            the evaluationSocleManager to set
     */
    public void setEvaluationSocleManager(EvaluationSocleManager evaluationSocleManager) {
        this.evaluationSocleManager = evaluationSocleManager;
    }

    /**
     * @param evaluationDisciplineManager
     *            the evaluationDisciplineManager to set
     */
    public void setEvaluationDisciplineManager(EvaluationDisciplineManager evaluationDisciplineManager) {
        this.evaluationDisciplineManager = evaluationDisciplineManager;
    }
}
