/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ScrollableResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.EchelonEvaluationDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.EvaluationComplementaireDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.EvaluationComplementaireEleveDao;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireEleve;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationComplementaireElevePK;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementairePK;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EchelonEvaluation;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.nomenclature.ParametresFormationAccueilManager;
import fr.edu.academie.affelnet.web.actions.NavigationException;

/**
 * Cette classe regroupe les actions sur les �valuations Compl�mentaires.
 *
 */
@Service
public class EvaluationComplementaireManager extends AbstractManager {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(EvaluationComplementaireManager.class);

    /**
     * Le DAO des �valuations compl�mentaires.
     */
    private EvaluationComplementaireDao evaluationComplementaireDao;

    /**
     * Le DAO des �valuations compl�mentaires �l�ve.
     */
    private EvaluationComplementaireEleveDao evaluationComplementaireEleveDao;

    /** Le DAO des �chelons utilis�s pour les �valuation compl�mentaires. */
    private EchelonEvaluationDao echelonEvaluationDao;

    /**
     * Le manager des param�tres par formation d'accueil.
     */
    private ParametresFormationAccueilManager parametresFormationAccueilManager;

    /**
     * M�thode permettant de mettre � jour une �valuation compl�mentaire.
     * 
     * @param evaluationComplementaireDao
     *            valeur
     */
    @Autowired
    public void setEvaluationComplementaireDao(EvaluationComplementaireDao evaluationComplementaireDao) {
        this.evaluationComplementaireDao = evaluationComplementaireDao;
    }

    /**
     * M�thode permettant de mettre � jour une �valuation compl�mentaire d'un �l�ve.
     * 
     * @param evaluationComplementaireEleveDao
     *            valeur
     */
    @Autowired
    public void setEvaluationComplementaireEleveDao(
            EvaluationComplementaireEleveDao evaluationComplementaireEleveDao) {
        this.evaluationComplementaireEleveDao = evaluationComplementaireEleveDao;
    }

    /**
     * @param echelonEvaluationDao
     *            le DAO des �chelons d'�valuations compl�mentaires
     */
    @Autowired
    public void setEchelonEvaluationDao(EchelonEvaluationDao echelonEvaluationDao) {
        this.echelonEvaluationDao = echelonEvaluationDao;
    }

    /**
     * M�thode permettant de mettre � jour le param�tre par formation d'accueil.
     * 
     * @param parametresFormationAccueilManager
     *            valeur
     */
    @Autowired
    public void setParametresFormationAccueilManager(
            ParametresFormationAccueilManager parametresFormationAccueilManager) {
        this.parametresFormationAccueilManager = parametresFormationAccueilManager;
    }

    /**
     * R�cup�re l'�valuation compl�mentaire de la base de donn�es.
     * 
     * @param id
     *            L'identifiant technique de l'�valuation compl�mentaire.
     * @return l'�valuation compl�mentaire correspondant � l'identifiant.
     */
    public EvaluationComplementaire chargerEvaluationComplementaire(Long id) {
        return evaluationComplementaireDao.charger(id);
    }

    /**
     * R�cup�re la liste des �valuations compl�mentaires.
     * 
     * @return la liste des �valuations compl�mentaires
     */
    public List<EvaluationComplementaire> listerEvalComplementaire() {
        return evaluationComplementaireDao.lister();
    }

    /**
     * M�thode permettant de cr�er une �valuation compl�mentaire.
     * 
     * @param evaluationComplementaire
     *            l'�valuation compl�mentaire
     * @return l'objet �valuation compl�mentaire
     */
    public EvaluationComplementaire creerEvaluationComplementaire(
            EvaluationComplementaire evaluationComplementaire) {
        return evaluationComplementaireDao.creer(evaluationComplementaire);
    }

    /**
     * M�thode permettant de mettre � jour une �valuation compl�mentaires.
     * 
     * @param evaluationComplementaire
     *            l'�valuation compl�mentaire
     * @return l'objet �valuation compl�mentaire
     */
    public EvaluationComplementaire modifier(EvaluationComplementaire evaluationComplementaire) {
        evaluationComplementaire.setDateMAJ(new Date());
        return evaluationComplementaireDao.maj(evaluationComplementaire, evaluationComplementaire.getId());
    }

    /**
     * M�thode permettant de supprimer une �valuation compl�mentaires.
     * 
     * @param evaluationComplementaire
     *            l'�valuation compl�mentaire
     */
    public void supprimer(EvaluationComplementaire evaluationComplementaire) {
        evaluationComplementaireDao.supprimer(evaluationComplementaire);
    }

    /**
     * Donne la liste des diff�rents �chelons utilisables pour les �valuations compl�mentaires.
     * 
     * @return liste des �chelons d'�valuation compl�mentaires
     */
    public List<EchelonEvaluation> listerEchelonsEvaluation() {
        return echelonEvaluationDao.lister();
    }

    /**
     * M�thode permettant de v�rifier si les �valuations n'ont pas encore �t� saisie.
     * 
     * @return vrai si des �valuations sont saisies
     */
    public boolean isEvaluationSaisies() {
        return evaluationComplementaireEleveDao.isEvaluationSaisies();
    }

    /**
     * R�cup�re l'�valuation compl�mentaire Eleve de la base de donn�es.
     * 
     * @param id
     *            L'identifiant technique de l'�valuation compl�mentaire.
     * @return l'�valuation compl�mentaire de l'�l�ve correspondant � l'identifiant.
     */
    public EvaluationComplementaireEleve chargerEvaluationComplementaireEleve(EvaluationComplementaireElevePK id) {
        return evaluationComplementaireEleveDao.charger(id);
    }

    /**
     * R�cup�re la liste des �valuations compl�mentaires de tous les �l�ves.
     * 
     * @return la liste des �valuations compl�mentaires de tous les �l�ves
     */
    public List<EvaluationComplementaireEleve> listerEvalComplementaireEleve() {
        return evaluationComplementaireEleveDao.lister();
    }

    /**
     * M�thode permettant de cr�er une �valuation compl�mentaire pour un �l�ve.
     * 
     * @param evaluationComplementaireEleve
     *            l'�valuation compl�mentaire de l'�l�ve
     * @return l'objet �valuation compl�mentaire de l'�l�ve
     */
    public EvaluationComplementaireEleve creerEvaluationComplementaireEleve(
            EvaluationComplementaireEleve evaluationComplementaireEleve) {
        return evaluationComplementaireEleveDao.creer(evaluationComplementaireEleve);
    }

    /**
     * M�thode permettant de mettre � jour une �valuation compl�mentaires pour un �l�ve.
     * 
     * @param evaluationComplementaireEleve
     *            l'�valuation compl�mentaire de l'�l�ve
     */
    public void modifier(EvaluationComplementaireEleve evaluationComplementaireEleve) {
        evaluationComplementaireEleveDao.maj(evaluationComplementaireEleve,
                evaluationComplementaireEleve.getEvaluationComplementaireElevePK());

    }

    /**
     * M�thode permettant de r�cup�rer le nombre d'�l�ment.
     * 
     * @return le nombre total d'�l�ment dans la liste
     */
    public int getNombreElements() {
        return evaluationComplementaireDao.getNombreElements();
    }

    /**
     * M�thode permettant de cr�er une �valuation compl�mentaire.
     * Cette m�thode a �t� d�placer dans le manager car c'est lui qui contr�le l'objet dao
     * 
     * @param evaluationComplementaire
     *            l'objet EvaluationComplementaire
     * @param modeEdition
     *            type d'action edition
     * @param message
     *            la liste des messages d'erreur
     * @return l'objet EvaluationComplementaire cr��
     */
    public EvaluationComplementaire creer(EvaluationComplementaire evaluationComplementaire, boolean modeEdition,
            List<String> message) {
        evaluationComplementaire.setDateCreation(new Date());
        // r�cup�ration de l'ordre souhait�
        int ordreSouhaite = evaluationComplementaire.getOrdreAffichage();

        // On v�rifie que l'on ne d�passe pas la taille maximum de la liste en ajoutant le nouvel �l�ment
        // recup�ration de la liste des �valuations compl�mentaires
        int nbEvalComp = getNombreElements();

        if (nbEvalComp >= EvaluationComplementaire.NOMBRE_MAXIMUM) {

            if (modeEdition) {
                message.add("Vous avez d�j� saisi " + EvaluationComplementaire.NOMBRE_MAXIMUM
                        + " �valuations compl�mentaires. Vous ne pouvez plus en ajouter.");
            } else {
                throw new ValidationException(
                        "Impossible d'ajouter des �valuations compl�mentaires suppl�mentaires");
            }
        }

        // On v�rifie que le num�ro d'ordre n'est pas utilis� par un autre
        if (verificationNumeroOrdreUtilise(ordreSouhaite)) {
            // si c'est le cas on charge l'�valuation compl�mentaire qui avait ce num�ro d'ordre
            // et on change son ordre en prenant la premi�re place libre
            LOG.debug("Il y a un doublon lors de la cr�ation de l'�valuation compl�mentaire "
                    + evaluationComplementaire.getId());
            modifierNumeroOrdre(ordreSouhaite);
            // CFN - M�thode en utilisant la modification au niveau de l'objet ne fonctionnant pas
            // EvaluationComplementaire evaluationComplementaireAModifier = modifierNumeroOrdre(ordreSouhaite);
            // evaluationComplementaireAModifier = modifier(evaluationComplementaireAModifier);
        }

        // Cr�ation avant pour �viter l'erreur :
        // org.hibernate.exception.ConstraintViolationException: could not insert:
        // [fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementaire]
        evaluationComplementaireDao.creer(evaluationComplementaire);

        // Mise � jour des coefficients par �valuation compl�mentaire par formation d'accueil
        // Mise � jour de la recherche pour n'avoir que les param�tres dont le mode bar�me = evaluation
        List<Long> listeParamefaId = parametresFormationAccueilManager
                .getListeIdParametresFormationAccueil(ParametresFormationAccueil.MODE_BAREME_EVALUATION);
        CoefficientEvaluationComplementaire coefficientEvaluationComplementaire = null;
        CoefficientEvaluationComplementairePK id = null;
        for (Long paramefaId : listeParamefaId) {
            LOG.debug("paramefaId:" + paramefaId);
            id = new CoefficientEvaluationComplementairePK(paramefaId, evaluationComplementaire.getId());
            LOG.debug("CoefficientEvaluationComplementairePK:" + id);
            coefficientEvaluationComplementaire = new CoefficientEvaluationComplementaire(id);
            LOG.debug("CoefficientEvaluationComplementaire:" + coefficientEvaluationComplementaire.toString());
            evaluationComplementaire.ajouteCoefficient(coefficientEvaluationComplementaire);
        }
        return evaluationComplementaireDao.maj(evaluationComplementaire, evaluationComplementaire.getId());

    }

    /**
     * M�thode permettant de savoir si il y a une coh�rence sur le num�ro d'ordre
     * On r�cup�re les valeurs de VAL_ORD (Num�ro d'ordre) de la table GN_EVAL_COMPLEMENTAIRE.
     * 
     * @return vrai si il suive un pas de 1 et commence par 1
     */
    public boolean verificationCoherenceNumeroOrdre() {
        // R�cup�ration de la liste des num�ro de l'ordre d'affichage utilis�
        List<Integer> listValOrdre = extractionListeOrdreAffichage();

        // V�rification de l'incr�mentation
        int j = 1;
        for (int i = 0; i < listValOrdre.size(); i++, j++) {
            if (listValOrdre.get(i) != j) {
                return false;
            }
        }
        return true;
    }

    /**
     * M�thode permettant de lister les num�ro d'ordre utilis�.
     * 
     * @return la liste des num�ro d'ordre d'affichage de la liste des �valuations compl�mentaires
     */
    public List<Integer> extractionListeOrdreAffichage() {
        List<EvaluationComplementaire> listeEvalCompl = evaluationComplementaireDao.lister();
        // On r�cup�rer tout les num�ros d'ordre de la liste
        ArrayList<Integer> listValOrdre = new ArrayList<>();
        for (EvaluationComplementaire evaluationComplementaire : listeEvalCompl) {
            int ordreAffichage = evaluationComplementaire.getOrdreAffichage();
            listValOrdre.add(ordreAffichage);
        }
        // R�cup�re une collection trier
        Collections.sort(listValOrdre);
        return listValOrdre;
    }

    /**
     * M�thode permettant de d�finir si un num�ro d'ordre est d�j� utilis�.
     * 
     * @param numero
     *            que l'on souhaite utilis� pour l'ordre
     * @return vrai si le num�ro en param�tre est d�j� utilis�.
     */
    public boolean verificationNumeroOrdreUtilise(int numero) {
        // R�cup�ration de la liste des num�ro de l'ordre d'affichage utilis�
        List<Integer> listValOrdre = extractionListeOrdreAffichage();
        return listValOrdre.contains(numero);
    }

    /**
     * M�thode permettant de r�cup�rer la liste des num�ro d'ordre possible
     * On r�cup�re les valeurs de VAL_ORD (Num�ro d'ordre) de la table GN_EVAL_COMPLEMENTAIRE.
     * 
     * @return la liste des num�ro d'ordre possible
     */
    public List<Integer> listeNumeroOrdrePossible() {
        // R�cup�ration de la liste des num�ro de l'ordre d'affichage utilis�
        List<Integer> listValOrdre = extractionListeOrdreAffichage();
        // Cr�ation de la liste des num�ro d'ordre
        List<Integer> listValOrdrePossible = new ArrayList<>();
        // On parcours les valeurs possibles et on les ajoute si elles ne sont pas contenues
        for (int i = 1; i <= EvaluationComplementaire.NOMBRE_MAXIMUM; i++) {
            if (!listValOrdre.contains(i)) {
                listValOrdrePossible.add(i);
            }
        }
        LOG.debug("Il y a " + listValOrdrePossible.size()
                + " num�ro d'ordre possible. Les num�ros d'ordre possible sont "
                + listValOrdrePossible.toString());
        return listValOrdrePossible;
    }

    /**
     * M�thode permettant de mettre � jour une �valuation compl�mentaire.
     * 
     * @param numeroOrdreAModifier
     *            num�ro d'ordre � modifier
     */
    public void modifierNumeroOrdre(int numeroOrdreAModifier) {
        LOG.debug("Modification du numero d'ordre pour l'�valuation compl�mentaire contenant l'ordre "
                + numeroOrdreAModifier);
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("ordreAffichage", numeroOrdreAModifier));
        List<EvaluationComplementaire> listeEvalCompl = evaluationComplementaireDao.lister(filtres, false);

        int tailleListEC = listeEvalCompl.size();
        if (tailleListEC != 1) {
            throw new NavigationException(
                    "Il y une erreur pour faire le tassement des �valuations compl�mentaires existants.");
        } else {
            EvaluationComplementaire evaluationComplementaireAModifier = listeEvalCompl.get(0);
            LOG.debug("L'�valuation compl�mentaire � modifier est " + evaluationComplementaireAModifier.getId());
            // Trouver le premier num�ro de libre
            int ordreAffichage = listeNumeroOrdrePossible().get(0);

            evaluationComplementaireDao.updateOrdre(numeroOrdreAModifier, ordreAffichage);

        }

    }

    /**
     * M�thode permettant de mettre � jour une �valuation compl�mentaire.
     * 
     * @param numeroOrdreAModifier
     *            num�ro d'ordre � modifer
     * @param ec
     *            �valuation compl�mentaire en cours de modification
     */
    public void modifierTousNumeroOrdre(int numeroOrdreAModifier, EvaluationComplementaire ec) {
        LOG.debug("Modification de tous les numeros d'ordre " + numeroOrdreAModifier);
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("ordreAffichage", numeroOrdreAModifier));
        Long idCourantEC = ec.getId();
        filtres.add(Filtre.notEqual("id", idCourantEC));
        List<EvaluationComplementaire> listeEvalComplAModifier = evaluationComplementaireDao.lister(filtres,
                false);
        List<Integer> listeNumeroOrdrePossible = listeNumeroOrdrePossible();
        // Sinon on prend l'ancien ordre d'affichage de l'�valuationcompl�mentaire qui nous force � changer
        int ordreAffichage = ec.getOrdreAffichage();
        int compteur = 0;
        for (EvaluationComplementaire evaluationComplementaire : listeEvalComplAModifier) {
            LOG.debug("L'�valuation compl�mentaire � modifier est " + evaluationComplementaire.getId());
            // Trouver le premier num�ro de libre
            if (!listeNumeroOrdrePossible.isEmpty()) {
                ordreAffichage = listeNumeroOrdrePossible.get(compteur);
            }

            LOG.debug("On change son ordre � " + ordreAffichage);

            evaluationComplementaireDao.updateOrdreAutreEC(numeroOrdreAModifier, ordreAffichage, idCourantEC);
            compteur++;

        }

    }

    /**
     * Compte le nombre d'�valuation compl�mentaire concernant des �l�ves avec groupe origine.
     * 
     * @return le nombre d'�valuation compl�mentaire concernant des �l�ves avec groupe origine
     */
    public int compteEvaluationComplementaireAvecGroupeOrigine() {
        Filtre filtre = evaluationComplementaireEleveDao.filtreEvalComAvecGroupeOrigine();
        return evaluationComplementaireEleveDao.getNombreElements(filtre);
    }

    /**
     * Fournit un r�sultat scrollable des �valuations compl�mentaires concernant des �l�ves avec groupe origine.
     * 
     * @return un r�sultat scrollable des �valuations compl�mentaires concernant des �l�ves avec groupe origine
     */
    public ScrollableResults scrollEvaluationComplementaireAvecGroupeOrigine() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(evaluationComplementaireEleveDao.filtreEvalComAvecGroupeOrigine());

        Set<String> fechtJoin = new HashSet<>();
        fechtJoin.add("eleve.palierOrigine");
        fechtJoin.add("echelon");

        return evaluationComplementaireEleveDao.scroll(filtres, null, null, fechtJoin, null, false);
    }

}
