/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.EvaluationDisciplineDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.ChampDisciplinaireDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.DisciplineDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.GroupeNiveauDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationDiscipline;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CompteRenduEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CorrespondanceEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationBilanPeriodeLSU;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationDisciplineLSU;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientDisciplineSpecialite;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.evaluation.lsu.CorrespondanceEvaluationManager;
import fr.edu.academie.affelnet.service.nomenclature.PalierManager;
import fr.edu.academie.affelnet.utils.CacheUtils;
import fr.edu.academie.affelnet.utils.LsuUtils;
import fr.edu.academie.affelnet.web.utils.FiltreUtils;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/** Manager pour les champs disciplinaires. */
@Service
public class EvaluationDisciplineManager extends AbstractManager {
    /** Ensemble des modalit�s d'�lection accept�es. */
    public static final Set<Character> MODALITE_ELECTION_ACCEPTEES = new HashSet<Character>();

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(EvaluationDisciplineManager.class);

    /**
     * Cache de la table des disciplines par code MefStat.
     */
    private static final CacheUtils.Init CACHE_DISCIPLINES_PAR_MEFSTAT = new CacheUtils.Init() {

        @Override
        public Map<String, List<Discipline>> getValue() {

            LOG.debug("Initialisation du cache de la table des disciplines par MefStat");

            // Liste les disciplines et collecte les libell�s de mati�re associ�s
            DisciplineDao dao = SpringUtils.getBean(DisciplineDao.class);
            List<Filtre> filtres = Collections.emptyList();
            List<Tri> tris = Collections.emptyList();
            List<String> elementsSelectionnes = Collections.emptyList();
            Set<String> listFetchJoin = new HashSet<>();
            List<String> elementsFetchJoin = new ArrayList<>();
            elementsFetchJoin.add("matiere");
            elementsFetchJoin.add("mefStat");
            elementsFetchJoin.add("champDisciplinaire");
            List<Discipline> toutesDisciplines = dao.lister(filtres, tris, elementsSelectionnes, listFetchJoin,
                    elementsFetchJoin, true);

            // Pr�pare la table associative des Discipline par code MefStat � mettre en
            // cache
            String codeMefStat;
            Map<String, List<Discipline>> mapDisciplinesParMefstat = new HashMap<>();

            for (Discipline discipline : toutesDisciplines) {

                codeMefStat = discipline.getMefStat().getCode();

                List<Discipline> disciplinesPourMefStat = mapDisciplinesParMefstat.get(codeMefStat);
                if (disciplinesPourMefStat == null) {

                    disciplinesPourMefStat = new ArrayList<Discipline>();
                    mapDisciplinesParMefstat.put(codeMefStat, disciplinesPourMefStat);
                }

                disciplinesPourMefStat.add(discipline);
            }

            return mapDisciplinesParMefstat;
        }
    };

    /**
     * Le dao utilis� pour les champs disciplinaire.
     */
    private ChampDisciplinaireDao champDisciplinaireDao;

    /** Le dao utilis� pour les disciplines. */
    private DisciplineDao disciplineDao;

    /** Le dao utilis� pour les disciplines. */
    private EvaluationDisciplineDao evaluationDisciplineDao;

    /** Le dao utilis� pour les correspondances. */
    private CorrespondanceEvaluationManager correspondanceEvaluationManager;

    /** Le gestionnaire de paliers. */
    private PalierManager palierManager;

    /** Le dao pour acc�der aux groupes niveau. */
    private GroupeNiveauDao groupeNiveauDao;

    static {
        MODALITE_ELECTION_ACCEPTEES.add('O');
        MODALITE_ELECTION_ACCEPTEES.add('S');
        MODALITE_ELECTION_ACCEPTEES.add('N');
    }

    /**
     * M�thode permettant de r�cup�rer le Dao li� � champDisciplinaire.
     * 
     * @param champDisciplinaireDao
     *            le champ disciplinaire
     */
    @Autowired
    public void setChampDisciplinaireDao(ChampDisciplinaireDao champDisciplinaireDao) {
        this.champDisciplinaireDao = champDisciplinaireDao;
    }

    /**
     * @param disciplineDao
     *            the disciplineDao to set
     */
    @Autowired
    public void setDisciplineDao(DisciplineDao disciplineDao) {
        this.disciplineDao = disciplineDao;
    }

    /**
     * @param evaluationDisciplineDao
     *            the evaluationDisciplineDao to set
     */
    @Autowired
    public void setEvaluationDisciplineDao(EvaluationDisciplineDao evaluationDisciplineDao) {
        this.evaluationDisciplineDao = evaluationDisciplineDao;
    }

    /**
     * @param correspondanceEvaluationManager
     *            the correspondanceEvaluationManager to set
     */
    @Autowired
    public void setCorrespondanceEvaluationManager(
            CorrespondanceEvaluationManager correspondanceEvaluationManager) {
        this.correspondanceEvaluationManager = correspondanceEvaluationManager;
    }

    /**
     * @param palierManager
     *            le gestionnaire de paliers
     */
    @Autowired
    public void setPalierManager(PalierManager palierManager) {
        this.palierManager = palierManager;
    }

    /**
     * @param groupeNiveauDao
     *            the groupeNiveauDao to set
     */
    @Autowired
    public void setGroupeNiveauDao(GroupeNiveauDao groupeNiveauDao) {
        this.groupeNiveauDao = groupeNiveauDao;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des champs disciplinaires.
     * 
     * @return une liste de champ disciplinaire
     */
    public List<ChampDisciplinaire> listerChampDisciplinaire() {
        // Lister toutes les champs disciplinaire
        List<Filtre> filtres = Collections.emptyList();
        List<Tri> tris = Collections.emptyList();
        List<String> elementsSelectionnes = Collections.emptyList();
        Set<String> listFetchJoin = new HashSet<>();
        List<String> elementsFetchJoin = new ArrayList<>();
        elementsFetchJoin.add("coefficientsDisciplineAca");
        List<ChampDisciplinaire> listChampDisciplinaire = champDisciplinaireDao.lister(filtres, tris,
                elementsSelectionnes, listFetchJoin, elementsFetchJoin, true);

        return listChampDisciplinaire;
    }

    /**
     * Liste les disciplines qui doivent �tre �valu�es pour le MefStat donn�.
     * 
     * @param codeMefStat
     *            le code du MefStat pour lequel obtenir les disciplines �valu�es
     * @return liste des disciplines concern�es
     */
    public List<Discipline> listerDisciplinesMefStat(String codeMefStat) {
        List<Filtre> listFiltres = new ArrayList<Filtre>(1);
        FiltreUtils.ajouterCritereEgal(listFiltres, "mefStat.code", codeMefStat);
        return disciplineDao.lister(listFiltres);
    }

    /**
     * Liste les disciplines qui doivent �tre �valu�es pour le MefStat donn�.
     * 
     * Cette version fait appel au cache. Attention, seules les donn�es
     * explicitement pr�vues en jointure sont disponibles !
     * 
     * @param codeMefStat
     *            le code du MefStat pour lequel obtenir les disciplines �valu�es
     * @return liste des disciplines concern�es (vide si le MefStat n'est pas pr�vu)
     */
    @SuppressWarnings("unchecked")
    public List<Discipline> listerDisciplinesMefStatCache(String codeMefStat) {

        List<Discipline> listeDisciplines = ((Map<String, List<Discipline>>) CacheUtils
                .getValue(CACHE_DISCIPLINES_PAR_MEFSTAT)).get(codeMefStat);

        if (listeDisciplines == null) {
            return Collections.emptyList();
        }
        return listeDisciplines;
    }

    /**
     * R�cup�re dans la map liant les groupes de niveau � la valeur de niveau.
     * 
     * @return une map liant les groupes de niveau avec comme cl� leur valeur
     */
    public Map<Integer, GroupeNiveau> mapGroupeNiveau() {
        List<GroupeNiveau> groupes = groupeNiveauDao.lister();

        Map<Integer, GroupeNiveau> groupeMap = new HashMap<Integer, GroupeNiveau>();
        for (GroupeNiveau groupe : groupes) {
            groupeMap.put(groupe.getNiveau(), groupe);
        }

        return groupeMap;
    }

    /**
     * Retourne un champ disciplinaire pour le codeGestion.
     * 
     * @param codeGestion
     *            la cl� de gestion du champ disciplinaire
     * @return Le champ disciplinaire
     * 
     *         Une DaoException est renvoy�e si le champ disciplinaire est
     *         introuvable.
     */
    public ChampDisciplinaire charger(String codeGestion) {
        return champDisciplinaireDao.charger(codeGestion);
    }

    /**
     * G�n�re les �valuations d'un �l�ve � partir des �valuations LSU.
     * 
     * @param eleve
     *            �l�ve �valu�
     * @param bilans
     *            bilans p�riodiques de l'�l�ves
     * @param idEtablissement
     *            id de l'�tablissement
     * @param compteRendu
     *            Le compte-rendu de la demande
     * @return les �valuations des disciplines g�n�r�es
     */
    public Map<Discipline, EvaluationDiscipline> genererEvaluationDiscipline(Eleve eleve,
            Set<EvaluationBilanPeriodeLSU> bilans, String idEtablissement, CompteRenduEvaluation compteRendu) {
        Map<Discipline, List<Integer>> evaluationsParDisc = new HashMap<Discipline, List<Integer>>();
        Discipline discipline = null;
        String codeMat = null;
        double moyenne;

        for (EvaluationBilanPeriodeLSU bilan : bilans) {
            // Mise � jour du compte-rendu
            if (CollectionUtils.isNotEmpty(bilan.getDisciplines())) {
                compteRendu.incrementerNbBilanPeriodique();
            }

            for (EvaluationDisciplineLSU evaluationDiscilineLsu : bilan.getDisciplines()) {
                if (MODALITE_ELECTION_ACCEPTEES.contains(evaluationDiscilineLsu.getModaliteElection())) {
                    discipline = null;
                    codeMat = evaluationDiscilineLsu.getCodeMat();
                    Integer points = conversionEnPoint(evaluationDiscilineLsu, idEtablissement);

                    for (Discipline disc : evaluationsParDisc.keySet()) {
                        if (disc.getCodeMatiere().equals(codeMat)) {
                            discipline = disc;
                        }
                    }
                    if (discipline == null) {
                        discipline = disciplineDao.charger(eleve.getFormation().getMefStat(), codeMat);
                        if (discipline != null) {
                            evaluationsParDisc.put(discipline, new ArrayList<Integer>());
                        } else {
                            LOG.info("La discipine " + codeMat + " n'a pas �t� trouv�e pour le Mefstat "
                                    + eleve.getFormation().getMefStat().getCode());
                            compteRendu.addFormationPourDiscipline(evaluationDiscilineLsu.disciplineToString(),
                                    eleve.getFormation().getMefStat().getLibelleCourt(),
                                    eleve.toStringAvecDivision());
                        }
                    }
                    if (points != null && discipline != null) {
                        evaluationsParDisc.get(discipline).add(points);
                    }
                }
            }
        }

        for (Discipline disc : evaluationsParDisc.keySet()) {
            if (evaluationsParDisc.get(disc).size() > 0) {
                moyenne = 0;
                for (Integer note : evaluationsParDisc.get(disc)) {
                    moyenne += note;
                }
                moyenne = moyenne / evaluationsParDisc.get(disc).size();
                if (eleve.getEvaluationsDiscipline().containsKey(disc)) {
                    eleve.getEvaluationsDiscipline().get(disc).setMoyenne(moyenne);
                } else {
                    eleve.getEvaluationsDiscipline().put(disc, new EvaluationDiscipline(eleve, disc, moyenne));
                }
            }
        }
        return eleve.getEvaluationsDiscipline();
    }

    /**
     * Convertit en nombre de point l'�valuation fournie.
     * 
     * @param evaluationDiscilineLsu
     *            L'�valuation LSU
     * @param idEtablissement
     *            id de l'�tablissement concern�
     * @return Le nombre de point correspondant
     */
    private Integer conversionEnPoint(EvaluationDisciplineLSU evaluationDiscilineLsu, String idEtablissement) {
        Integer points = null;
        if (evaluationDiscilineLsu.getNiveau() != null) {
            points = mapGroupeNiveau().get(evaluationDiscilineLsu.getNiveau()).getPoints();
        } else if (StringUtils.isNotBlank(evaluationDiscilineLsu.getValeur())) {
            String valEval = evaluationDiscilineLsu.getValeur();
            if (LsuUtils.estNoteSur20(valEval)) {
                points = conversionEvalNumEnPoint(LsuUtils.conversionEvalEnNumerique(valEval));
            } else {
                CorrespondanceEvaluation correspondance = correspondanceEvaluationManager.charger(idEtablissement,
                        valEval);
                if (correspondance.getFlagIgnore().equals(Flag.NON)) {
                    points = correspondance.getGroupeNiveau().getPoints();
                }
            }
        }
        return points;
    }

    /**
     * Associe une note num�rique � son groupe de niveau.
     * 
     * @param note
     *            l'�valuation num�rique.
     * @param groupeNiveauMap
     *            la map liant les groupes de niveau � leur indice
     * @return le groupe de niveau.
     */
    public GroupeNiveau conversionEvalNumEnGroupeNiveau(Double note, Map<Integer, GroupeNiveau> groupeNiveauMap) {
        if (note == null || note < 0 || note > 20) {
            return null;
        }
        NavigableMap<Double, Integer> corresNumPoint = new TreeMap<Double, Integer>();
        // On cr��e une Map qui regroupe les notes et les points associ�s.
        // Sup�rieur ou �gal � 0 : niveau 1
        corresNumPoint.put(0d, 1);
        // Sup�rieur ou �gal � 5 : niveau 2
        corresNumPoint.put(5d, 2);
        // Sup�rieur ou �gal � 10 : niveau 3
        corresNumPoint.put(10d, 3);
        // Sup�rieur ou �gal � 15 : niveau 4
        corresNumPoint.put(15d, 4);

        // On renvoie le nombre de point associ� au palier inf�rieur
        int valNiveau = corresNumPoint.floorEntry(note).getValue();

        if (groupeNiveauMap != null) {
            return groupeNiveauMap.get(valNiveau);
        } else {
            return mapGroupeNiveau().get(valNiveau);
        }
    }

    /**
     * Convertit une note num�rique sur 20 en points pour le bar�me.
     * 
     * @param note
     *            l'�valuation num�rique.
     * @param groupeNiveauMap
     *            la map liant les groupes de niveau � leur indice
     * @return le nombre de point.
     */
    public Integer conversionEvalNumEnPoint(Double note, Map<Integer, GroupeNiveau> groupeNiveauMap) {
        GroupeNiveau groupeNiveau = conversionEvalNumEnGroupeNiveau(note, groupeNiveauMap);
        if (groupeNiveau != null) {
            return groupeNiveau.getPoints();
        } else {
            return null;
        }
    }

    /**
     * Convertit une note num�rique sur 20 en points pour le bar�me.
     * 
     * @param note
     *            l'�valuation num�rique.
     * @return le nombre de point.
     */
    public Integer conversionEvalNumEnPoint(Double note) {
        return conversionEvalNumEnPoint(note, null);
    }

    /**
     * Fournit le filtre pour r�cup�rer les �l�ves de palier 3�me qui ne poss�dent
     * pas d'�valuation de discipline. Note : inclus les �l�ves forc�s.
     * 
     * @return le filtre pour les �l�ves sans �valuation de discipline
     */
    public Filtre filtreElevesSansEvalDiscipline() {
        return evaluationDisciplineDao.filtreElevesSansEvalDiscipline();
    }

    /**
     * Liste les disciplines � utiliser en fonction de l'�l�ve.
     * 
     * On prend en compte le cas o� l'�l�ve a �t� forc� sur le palier d'origine 3�me
     * (valeur diff�rente de ce que donne l'attribution automatique des paliers).
     * Dans ce cas, on lui retourne les disciplines du MefStat 3�me.
     * 
     * @param eleve
     *            l'�l�ve concern�
     * @param useCache
     *            utiliser le cache (v�rifier que les �l�ments charg�s en jointure
     *            sont suffisants)
     * @return liste des disciplines � utiliser pour l'�valuation de l'�l�ve en
     *         palier 3eme
     */
    public List<Discipline> listerDisciplinesSaisieEleve(Eleve eleve, boolean useCache) {
        MefStat mefStat = palierManager.getMefStatCompatiblePalier3eme(eleve);
        String codeMefStat = mefStat.getCode();
        LOG.debug("R�cup�ration des disciplines du MefStat " + codeMefStat);

        if (useCache) {
            return listerDisciplinesMefStatCache(codeMefStat);
        }

        return listerDisciplinesMefStat(codeMefStat);
    }

    /**
     * M�thode permettant de r�cup�rer les coefficients des disciplines de
     * sp�cialit�.
     * 
     * @param domaineSpecialite
     *            le domaine de sp�cialit�
     * 
     * @return une liste de coefficients par domaine de sp�cialit� et discipline de
     *         sp�cialit�
     */
    @SuppressWarnings("unchecked")
    public List<CoefficientDisciplineSpecialite> listerCoefficientDisciplineSpecialite(String domaineSpecialite) {

        // r�cup�ration de la session
        Session session = HibernateUtil.currentSession();

        // Lister tous les coefficients des discipline de sp�cialit�s
        Criteria critCoeffDispSpe = session.createCriteria(CoefficientDisciplineSpecialite.class,
                "coefficientDisciplineSpecialite");
        critCoeffDispSpe.add(
                Restrictions.eq("coefficientDisciplineSpecialitePK.codeDomaineSpecialite", domaineSpecialite));
        critCoeffDispSpe.createAlias("coefficientDisciplineSpecialite.champDiscipline", "chDisp");
        critCoeffDispSpe.addOrder(Order.asc("chDisp.valeurOrdre"));

        List<CoefficientDisciplineSpecialite> listCoefficient = critCoeffDispSpe.list();

        return listCoefficient;
    }

    /**
     * M�thode permettant de r�cup�rer les coefficients des disciplines de
     * sp�cialit� avec en cl� le champ disciplinaire et en valeur le coefficient
     * associ�.
     * 
     * @param domaineSpecialite
     *            le domaine de sp�cialit�
     * 
     * @return une map de coefficients par discipline de sp�cialit� pour un domaine
     *         de sp�cialit� pr�cis
     */
    @SuppressWarnings("unchecked")
    public Map<ChampDisciplinaire, Integer> recuperationMapCoefficientDisciplineSpecialite(
            String domaineSpecialite) {

        // r�cup�ration de la session
        Session session = HibernateUtil.currentSession();

        Map<ChampDisciplinaire, Integer> mapCoefficientDisciplineSpecialite = new TreeMap<ChampDisciplinaire, Integer>();

        // Lister tous les coefficients des discipline de sp�cialit�s
        Criteria critCoeffDispSpe = session.createCriteria(CoefficientDisciplineSpecialite.class,
                "coefficientDisciplineSpecialite");
        critCoeffDispSpe.add(
                Restrictions.eq("coefficientDisciplineSpecialitePK.codeDomaineSpecialite", domaineSpecialite));
        critCoeffDispSpe.createAlias("coefficientDisciplineSpecialite.champDiscipline", "champDiscipline");
        critCoeffDispSpe.addOrder(Order.asc("champDiscipline.valeurOrdre"));
        ProjectionList projList = Projections.projectionList();
        projList.add(Projections.property("champDiscipline"));
        projList.add(Projections.property("valCoef"));
        critCoeffDispSpe.setProjection(projList);

        List<Object[]> listDisciplineCoefficient = critCoeffDispSpe.list();

        // Parcours de la liste pour construire une map
        for (Object[] chpDispCoeff : listDisciplineCoefficient) {
            mapCoefficientDisciplineSpecialite.put((ChampDisciplinaire) chpDispCoeff[0],
                    ((Number) chpDispCoeff[1]).intValue());
        }

        return mapCoefficientDisciplineSpecialite;
    }
}
