/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.evaluation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.EvaluationSocleDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.CompetenceSocleDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.DegreMaitriseDao;
import fr.edu.academie.affelnet.domain.evaluation.EvaluationSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CompetenceSocle;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DegreMaitrise;
import fr.edu.academie.affelnet.service.AbstractManager;

/**
 * Manager pour g�rer les �valuations du socles et les beans associ�s.
 */
@Service
public class EvaluationSocleManager extends AbstractManager {
    /** Dao pour les �valuations du socle. */
    @Autowired
    private EvaluationSocleDao evaluationSocleDao;

    /** Dao pour les degr�s de ma�trise du socle. */
    @Autowired
    private DegreMaitriseDao degreMaitriseDao;

    /** Dao pour les comp�tences du socle. */
    @Autowired
    private CompetenceSocleDao competenceSocleDao;

    /**
     * Liste l'ensemble des comp�tences du socle.
     * 
     * @return liste des comp�tences du socle.
     */
    public List<CompetenceSocle> listerCompetencesSocle() {
        return competenceSocleDao.lister();
    }

    /**
     * Fournit une map des comp�tences du socle avec comme cl� le code.
     * 
     * @return map des comp�tences du socle.
     */
    public Map<String, CompetenceSocle> mapCompetencesSocle() {
        List<CompetenceSocle> competences = competenceSocleDao.lister();
        Map<String, CompetenceSocle> competencesMap = new HashMap<>();
        for (CompetenceSocle competence : competences) {
            competencesMap.put(competence.getCode(), competence);
        }
        return competencesMap;
    }

    /**
     * Fournit une map des degr�s de ma�trise avec comme cl� la valeur du degr�.
     * 
     * @return map des degr�s de ma�trise.
     */
    public Map<Integer, DegreMaitrise> mapDegreMaitrise() {
        List<DegreMaitrise> degres = degreMaitriseDao.lister();
        Map<Integer, DegreMaitrise> degreMap = new HashMap<>();
        for (DegreMaitrise degre : degres) {
            degreMap.put(degre.getValeur(), degre);
        }
        return degreMap;
    }

    /**
     * Renvoie la comp�tence socle associ�e au code.
     * 
     * @param code
     *            Code de la comp�tence
     * @return la comp�tence du socle
     */
    public CompetenceSocle chargerCompetenceSocle(String code) {
        return competenceSocleDao.charger(code);
    }

    /**
     * Sauvegarde en base de donn�e une �valuation du socle.
     * 
     * @param evaluationSocle
     *            �valuation du socle � enregistrer
     */
    public void creerEvaluationSocle(EvaluationSocle evaluationSocle) {
        evaluationSocleDao.creer(evaluationSocle);
    }

    /**
     * Fournit la liste des �valuations du socle d'un �l�ve.
     * 
     * @param ine
     *            id de l'�l�ve
     * @return la liste des �valuations du socle de l'�l�ve
     */
    public List<EvaluationSocle> listerEvaluationSocle(String ine) {
        return evaluationSocleDao.listerParEleve(ine);
    }

    /**
     * Supprime l'ensemble des �valuations du socle de l'�l�ve.
     * 
     * @param ine
     *            id de l'�l�ve
     */
    public void purger(String ine) {
        evaluationSocleDao.purger(ine);
    }

    /**
     * Renvoie la liste de l'ensemble des degr�s de ma�trise.
     * 
     * @return la liste de l'ensemble des degr�s de ma�trise
     */
    public List<DegreMaitrise> listerDegreMaitrise() {
        return degreMaitriseDao.lister();
    }

    /**
     * Fournit le filtre permettant de r�cup�rer les �l�ves sans �valuation du socle.
     * Note : inclus les �l�ves forc�s.
     * 
     * @return le filtre correspondant aux �l�ves sans �valuation du socle.
     */
    public Filtre filtreElevesSansEvalSocle() {
        return evaluationSocleDao.filtreElevesSansEvalSocle();
    }
}
