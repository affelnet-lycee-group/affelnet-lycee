/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.evaluation.lsu;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.evaluation.lsu.CorrespondanceEvaluationDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.GroupeNiveauDao;
import fr.edu.academie.affelnet.domain.evaluation.lsu.CorrespondanceEvaluation;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.GroupeNiveau;
import fr.edu.academie.affelnet.service.AbstractManager;

/**
 * Service pour la gestion des correspondances entre les �valuations des champs disciplinaires
 * et les groupes de niveau.
 */
@Service
public class CorrespondanceEvaluationManager extends AbstractManager {
    /** Dao pour les correspondances. */
    @Autowired
    private CorrespondanceEvaluationDao correspondanceEvaluationDao;

    /** Dao pour les groupes de niveau. */
    @Autowired
    private GroupeNiveauDao groupeNiveauDao;

    /**
     * Insert dans la base de donn�e une nouvelle correspondance.
     * 
     * @param correspondanceEvaluation
     *            La nouvelle correspondance.
     */
    public void creer(CorrespondanceEvaluation correspondanceEvaluation) {
        correspondanceEvaluationDao.creer(correspondanceEvaluation);
    }

    /**
     * Met � jour dans la base de donn�e la correspondance.
     * 
     * @param correspondanceEvaluation
     *            La correspondance � mettre � jour.
     */
    public void maj(CorrespondanceEvaluation correspondanceEvaluation) {
        correspondanceEvaluation.setDateModification(new Date());
        correspondanceEvaluationDao.maj(correspondanceEvaluation, correspondanceEvaluation.getId());
    }

    /**
     * Charge depuis la base de donn�e la correspondance li�e � cet id.
     * 
     * @param id
     *            l'id de la correspondance � r�cup�rer.
     * @return la correspondance demand�e.
     */
    public CorrespondanceEvaluation charger(Long id) {
        return correspondanceEvaluationDao.charger(id);
    }

    /**
     * Charge depuis la base de donn�e la correspondance.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @param valEval
     *            evaluation
     * @return la correspondance
     */
    public CorrespondanceEvaluation charger(String idEtablissement, String valEval) {
        return correspondanceEvaluationDao.charger(idEtablissement, valEval);
    }

    /**
     * R�cup�re l'ensemble des �valuations ne correspondant pas � une note sur 20 d'un �tablissement.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return une liste contenant toutes les �valuations des correspondances de l'�tablissement
     */
    public List<String> chargerEvalParEtab(String idEtablissement) {
        return correspondanceEvaluationDao.chargerEvalParEtab(idEtablissement);
    }

    /**
     * R�cup�re l'ensemble des correspondances d'un �tablissement.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return la liste des correspondances de l'�tablissement
     */
    public List<CorrespondanceEvaluation> chargerCorrespondanceParEtab(String idEtablissement) {
        return correspondanceEvaluationDao.chargerCorrespondanceParEtab(idEtablissement);
    }

    /**
     * Charge depuis la base de donn�e les correspondance dont l'id appartient � la liste.
     * 
     * @param idList
     *            liste des id des correspondances � r�cup�rer.
     * @return les correspondances demand�es.
     */
    public List<CorrespondanceEvaluation> lister(List<Long> idList) {
        Filtre filtre = Filtre.in("id", idList);
        return correspondanceEvaluationDao.lister(filtre, false);
    }

    /**
     * R�cup�re la liste des groupes de niveau.
     * 
     * @return les groupes de niveau
     */
    public List<GroupeNiveau> listerGroupesNiveau() {
        return (List<GroupeNiveau>) groupeNiveauDao.lister();
    }

    /**
     * R�cup�re le groupes associ� au niveau.
     *
     * @param niveau
     *            niveau du groupe
     * @return le groupe de niveau
     */
    public GroupeNiveau chargerGroupeNiveau(Integer niveau) {
        return groupeNiveauDao.chargerNullable(niveau);
    }

    /**
     * Enregistre une nouvelle correspondance dans la base de donn�es.
     * 
     * @param correspondance
     *            La nouvelle correspondance
     */
    public void ajouter(CorrespondanceEvaluation correspondance) {
        correspondanceEvaluationDao.creer(correspondance);
    }

    /**
     * Supprime la correspondance � partir de son id.
     * 
     * @param id
     *            id de la correspondance
     * @return La correspondance supprim�e
     */
    public CorrespondanceEvaluation supprimer(long id) {
        CorrespondanceEvaluation correspondance = correspondanceEvaluationDao.charger(id);
        correspondanceEvaluationDao.supprimer(correspondance);
        return correspondance;
    }

    /**
     * D�termine si une �valuation similaire � la casse pr�s existe d�j� en base de donn�es.
     * 
     * @param valEval
     *            valeur de l'�valuation
     * @param idEtablissement
     *            id de l'�tablissement
     * @return vrai si une telle �valuation existe
     */
    public boolean existe(String idEtablissement, String valEval) {
        return correspondanceEvaluationDao.existe(idEtablissement, valEval);
    }

    /**
     * Renvoie le nombre de correspondance non r�solue pour un �tablissement.
     * C'est � dire les correspondances sans groupe de niveau et dont le flag ignore est � non.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return le nombre de correspondance non r�solu�
     */
    public int compteCorresNonResolue(String idEtablissement) {
        return correspondanceEvaluationDao.compteCorresNonResolue(idEtablissement);
    }
}
