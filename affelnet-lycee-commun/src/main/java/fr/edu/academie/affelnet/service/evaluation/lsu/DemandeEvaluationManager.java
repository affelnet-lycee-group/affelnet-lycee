/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.evaluation.lsu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.evaluation.lsu.DemandeEvaluationDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.evaluation.lsu.DemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EtatDemandeEvaluation;
import fr.edu.academie.affelnet.domain.evaluation.lsu.EvaluationEleveLSU;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.ConfigurationManager;
import fr.edu.academie.affelnet.service.EtablissementManager;

/**
 * Service pour les demandes d'�valuation LSU.
 */
@Service
public class DemandeEvaluationManager extends AbstractManager {

    /** DAO pour la manipulation des demandes d'�valuations. */
    @Autowired
    private DemandeEvaluationDao evaluationDao;

    /** Service pour la r�cup�ration de l'�tablissement. */
    @Autowired
    private EtablissementManager etablissementManager;

    /** Service pour la r�cup�ration de l'�tablissement. */
    @Autowired
    private ConfigurationManager configManager;

    /**
     * R�cup�re la demande d'�valuation ou la cr��e si elle n'existe pas.
     * 
     * @param etablissement
     *            �tablissement associ� � la demande
     * @return une demande d'�valuation
     */
    public DemandeEvaluation charger(Etablissement etablissement) {
        if (evaluationDao.existe(etablissement.getId())) {
            DemandeEvaluation demande = evaluationDao.charger(etablissement.getId());
            demande.genererCompteRendu();
            return demande;
        } else {
            return new DemandeEvaluation(etablissement);
        }
    }

    /**
     * R�cup�re la demande d'�valuation ou la cr��e si elle n'existe pas.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return une demande d'�valuation
     */
    public DemandeEvaluation charger(String idEtablissement) {
        Etablissement etablissement = etablissementManager.charger(idEtablissement);
        return charger(etablissement);
    }

    /**
     * Met � jour ou cr��e la demande d'�valuation si elle n'existe pas.
     * 
     * @param demandeEvaluation
     *            la demande d'�valuation � enregistrer
     */
    public void maj(DemandeEvaluation demandeEvaluation) {
        demandeEvaluation.adapterBD();
        if (evaluationDao.existe(demandeEvaluation.getEtablissement().getId())) {
            evaluationDao.maj(demandeEvaluation, demandeEvaluation.getEtablissement().getId());
        } else {
            evaluationDao.creer(demandeEvaluation);
        }
    }

    /**
     * Cr��e la demande d'�valuation.
     * 
     * @param demandeEvaluation
     *            la demande d'�valuation � enregistrer
     */
    public void creer(DemandeEvaluation demandeEvaluation) {
        evaluationDao.creer(demandeEvaluation);
    }

    /**
     * Charge la demande li�e � l'�tablissement et v�rifie si elle a expir�e.
     * 
     * @param idEtablissement
     *            id de l'�tablissement
     * @return la demande li�e � l'�tablissement
     */
    public DemandeEvaluation verificationTimeout(String idEtablissement) {
        DemandeEvaluation demande = charger(idEtablissement);
        if (demande.getEtat().equals(EtatDemandeEvaluation.EN_COURS)) {
            Long timeout = configManager.getConfigurationLocale().getTimeoutDemandeEvalLsu();
            long tempsEcoule = new Date().getTime() - demande.getDateDerDemande().getTime();
            if (timeout != null && (tempsEcoule > timeout * (1000 * 60))) {
                demande.setEtat(EtatDemandeEvaluation.ERREUR);
                demande.reinitialiserCompteRendu();
                demande.setMsgNotification("Temps de traitement de la demande trop important : temps �coul�.");
                maj(demande);
            }
        }
        return demande;
    }

    /**
     * Fournit la liste des �l�ves non int�gr�s pour l'�tablissement donn� sous forme d'une liste de String.
     * 
     * @param idEtablissement
     *            id de l'�tablissement concern�
     * @return les �l�ves non int�gr�s en liste de String (vide si int�gration n'a pas eu lieu)
     */
    public List<String> listerEleveNonIntegreToString(String idEtablissement) {
        List<EvaluationEleveLSU> eleves = evaluationDao.listerEleveNonIntegre(idEtablissement);
        List<String> stringList = new ArrayList<String>();
        for (EvaluationEleveLSU eleve : eleves) {
            stringList.add(eleve.toString());
        }
        return stringList;
    }

    /**
     * Fournit le nombre d'�l�ve dont les �valuations ont �t� int�gr�s via la demande aupr�s de LSU.
     * 
     * @param idEtablissement
     *            id de l'�tablissement concern�
     * @return le nombre d'�l�ve dont les �valuations ont �t� int�gr�es.
     */
    public int compterEleveIntegre(String idEtablissement) {
        return evaluationDao.compterEleveIntegre(idEtablissement);
    }

    /**
     * R�cup�re depuis la table d'accueil l'�l�ve LSU correspondant � l'INE.
     * 
     * @param ine
     *            INE de l'�l�ve
     * @param idEtablissement
     *            Id de l'�tablissement de l'�l�ve
     * @return l'�l�ve LSU pr�sent dans la table d'accueil
     */
    public EvaluationEleveLSU chargerEleveLsu(String ine, String idEtablissement) {
        List<EvaluationEleveLSU> eleves = evaluationDao.chargerEleveLsu(ine, idEtablissement);
        if (eleves != null && eleves.size() > 0) {
            return eleves.get(0);
        }
        return null;
    }

    /**
     * Fournit la liste des demandes d'�valuation LSU.
     * 
     * @return la liste des demandes d'�vauation LSU
     */
    public List<DemandeEvaluation> lister() {
        return evaluationDao.lister();
    }

    /**
     * D�bloque l'int�gration pour un �tablissement.
     * 
     * @param idEtablissement
     *            Id de l'�tablissement
     */
    public void debloquerIntegration(String idEtablissement) {
        DemandeEvaluation demande = evaluationDao.chargerNullable(idEtablissement);
        if (demande != null) {
            demande.setFlagIntegration(Flag.NON);
            evaluationDao.maj(demande, idEtablissement);
        }
    }
}
