/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.AvisDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.TypeAvisDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.Avis;
import fr.edu.academie.affelnet.domain.nomenclature.AvisPK;
import fr.edu.academie.affelnet.domain.nomenclature.TypeAvis.CodeTypeAvis;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.VoeuEleveManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.utils.exceptions.BusinessException;

/**
 * Gestionnaire pour les avis.
 * 
 * <p>
 * Ce manager �tait initialement d�di� aux nomenclatures ce qui le laissait plut�t creux.
 * Il devrait r�cup�rer la responsabilit� de ce qui concerne la saisie des avis sur les voeux de fa�on � all�ger
 * VoeuEleveManager.
 * </p>
 */
@Service
public class AvisManager extends AbstractManager {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(AvisManager.class);

    /**
     * Le dao utilis� pour les avis.
     */
    private AvisDao avisDao;

    /**
     * Le dao utilis� pour les avis.
     */
    private TypeAvisDao typeAvisDao;

    /**
     * Le dao utilis� pour les voeux �l�ve.
     */
    private VoeuEleveDao voeuEleveDao;

    /** Le gestionnaire d'operations programmees d'affectation. */
    private OperationProgrammeeAffectationManager opaManager;

    /** Le gestionnaire du type d'avis. */
    private TypeAvisManager typeAvisManager;

    /** Le gestionnaire de voeux d'�l�ves. */
    private VoeuEleveManager voeuEleveManager;

    /**
     * @param avisDao
     *            the avisDao to set
     */
    @Autowired
    public void setAvisDao(AvisDao avisDao) {
        this.avisDao = avisDao;
    }

    /**
     * @param typeAvisDao
     *            the typeAvisDao to set
     */
    @Autowired
    public void setTypeAvisDao(TypeAvisDao typeAvisDao) {
        this.typeAvisDao = typeAvisDao;
    }

    /**
     * @param voeuEleveDao
     *            the voeuEleveDao to set
     */
    @Autowired
    public void setVoeuEleveDao(VoeuEleveDao voeuEleveDao) {
        this.voeuEleveDao = voeuEleveDao;
    }

    /**
     * @param opaManager
     *            le gestionnaire d'operations programmees d'affectation.
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * @param typeAvisManager
     *            le gestionnaire de types d'avis
     */
    @Autowired
    public void setTypeAvisManager(TypeAvisManager typeAvisManager) {
        this.typeAvisManager = typeAvisManager;
    }

    /**
     * @param voeuEleveManager
     *            le gestionnaire de voeux d'�l�ve
     */
    @Autowired
    public void setVoeuEleveManager(VoeuEleveManager voeuEleveManager) {
        this.voeuEleveManager = voeuEleveManager;
    }

    /**
     * Liste les valeurs pour un type d'avis.
     * 
     * @param codeTypeAvis
     *            type d'avis concern�
     * @param accesGestionnaire
     *            vrai pour les voeux gestionnaire, sinon faux pour les avis affichables en �tablissement
     * @return la liste des avis pour le type donn�
     */
    public List<Avis> listerAvisPourType(CodeTypeAvis codeTypeAvis, boolean accesGestionnaire) {

        List<Filtre> filtresAvis = new ArrayList<>();

        Filtre filtreTypeAvis = Filtre.equal("id.type.code", codeTypeAvis.getCode());
        filtresAvis.add(filtreTypeAvis);

        if (!accesGestionnaire) {
            Filtre filtreAffichableEtab = Filtre.equal("affichageEtablissement", Flag.OUI);
            filtresAvis.add(filtreAffichableEtab);
        }

        return avisDao.lister(filtresAvis);
    }

    /**
     * V�rifie l'existence d'un avis affichable pour le type donn�.
     * 
     * @param typeAvis
     *            le code du type d'avis � contr�ler
     * @return vrai s'il y a au moins un avis affich� en �tablissement pour le type de l'avis
     */
    public boolean verifExistenceAvisAffichable(String typeAvis) {
        int nbAvisAffichables = nombreAvisAffichablesEtablissementPourType(typeAvis);
        return (nbAvisAffichables > 0);
    }

    /**
     * Compte les avis affichables pour un type donn�.
     * 
     * @param codeTypeAvis
     *            Type d'avis
     * @return nombre d'avis affichables en �tablissement pour le type donn�
     */
    private int nombreAvisAffichablesEtablissementPourType(String codeTypeAvis) {
        LOG.debug("V�rification des affichage en �tablissement pour les avis de type " + codeTypeAvis);

        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("id.type.code", codeTypeAvis));
        filtres.add(Filtre.equal("affichageEtablissement", Flag.OUI));

        return avisDao.getNombreElements(filtres);
    }

    /**
     * Donne l'avis de type et de code demand�.
     * 
     * @param typeAvis
     *            type de l'avis
     * @param codeAvis
     *            code de l'avis
     * @return avis retrouv�
     */
    public Avis charger(String typeAvis, String codeAvis) {
        AvisPK idAvis = new AvisPK(codeAvis, typeAvisDao.charger(typeAvis));
        return charger(idAvis);
    }

    /**
     * Donne l'avis correspondant � la cl� fournie.
     * 
     * @param cleAvis
     *            cl� de l'avis (code et type)
     * @return avis retrouv�
     */
    public Avis charger(AvisPK cleAvis) {
        return avisDao.charger(cleAvis);
    }

    /**
     * Cr�er un nouvel avis � partir de celui fourni.
     * 
     * @param avis
     *            avis � cr�er pr�-renseign�
     * @return avis cr��
     */
    public Avis creer(Avis avis) {

        avis.setDateCreation(new Date());
        return avisDao.creer(avis);
    }

    /**
     * Met � jour un avis.
     * 
     * @param avis
     *            l'avis � mettre � jour
     * @param oldKey
     *            l'ancienne cl� de l'avis
     * @return avis cr��
     */
    public Avis maj(Avis avis, AvisPK oldKey) {

        opaManager.obligerRelanceOpa(avis);
        avis.setDateMAJ(new Date());
        return avisDao.maj(avis, oldKey);
    }

    /**
     * Supprime l'avis.
     * 
     * @param avis
     *            avis � supprimer
     */
    public void supprimer(Avis avis) {
        // SPEC RG-AFF3-NOM-AVI-01
        if (avis.getId().getCode().equals(Avis.CODE_AVIS_DEFAUT)) {
            throw new BusinessException("Les avis '" + Avis.CODE_AVIS_DEFAUT
                    + "' sont des valeurs par d�faut qui ne peuvent pas �tre supprim�es");
        }

        // v�rification de l'existence d'un voeu �l�ve utilisant cet avis
        if (estAvisUtiliseParVoeuEleve(avis)) {
            throw new BusinessException(
                    "La suppression de avis est impossible car un ou plusieurs voeux d'�l�ves l'utilisent.");
        }
        avisDao.supprimer(avis);
    }

    /**
     * Teste si l'avis fourni est utilis� par au moins un voeu d'�l�ve.
     * 
     * @param avis
     *            avis � tester
     * @return vrai si l'avis est
     */
    public boolean estAvisUtiliseParVoeuEleve(Avis avis) {
        return voeuEleveDao.isAvisSaisi(avis);
    }

    /**
     * V�rifie la pr�sence des avis obligatoires selon le type de saisie.
     * 
     * @param voeuEleve
     *            le voeu concern�
     * @param isIaOuRectorat
     *            vrai si le contr�le est effectu� du point de vue de l'administration
     */
    public void validerAvisObligatoires(VoeuEleve voeuEleve, boolean isIaOuRectorat) {
        validerAvisObligatoire(CodeTypeAvis.AVIS_DU_CHEF_D_ETABLISSEMENT, voeuEleve.getAvisConseilClasse(), voeuEleve,
                isIaOuRectorat);
        validerAvisObligatoire(CodeTypeAvis.AVIS_DE_GESTION, voeuEleve.getAvisEntretien(), voeuEleve, isIaOuRectorat);
        validerAvisObligatoire(CodeTypeAvis.AVIS_PASSERELLE, voeuEleve.getAvisPasserelle(), voeuEleve, isIaOuRectorat);
    }

    /**
     * V�rifie la pr�sence d'un avis obligatoire selon le type de saisie.
     * 
     * @param codeTypeAvisControle
     *            code du type d'avis � contr�ler
     * @param valeurAvis
     *            valeur de l'avis
     * @param voeuEleve
     *            le voeu concern�
     * @param isIaOuRectorat
     *            vrai si le contr�le est effectu� du point de vue de l'administration
     */
    private void validerAvisObligatoire(CodeTypeAvis codeTypeAvisControle, Avis valeurAvis, VoeuEleve voeuEleve,
            boolean isIaOuRectorat) {
        if (voeuEleveManager.modaliteAvis(codeTypeAvisControle, VoeuEleveManager.SAISIE_OBLIGATOIRE, voeuEleve,
                isIaOuRectorat) && valeurAvis == null) {
            throw new ValidationException(
                    "Veuillez saisir l'" + typeAvisManager.charger(codeTypeAvisControle).getLibelle());
        }
    }
}
