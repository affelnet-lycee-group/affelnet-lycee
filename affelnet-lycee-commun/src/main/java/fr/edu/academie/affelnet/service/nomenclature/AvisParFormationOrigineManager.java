/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.AvisParFormationOrigineDao;
import fr.edu.academie.affelnet.domain.nomenclature.AvisParFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.AbstractManager;

/**
 * Manager pour la gestion des formations d'origine pour les param�tres par
 * formation d'accueil (saisie de l'avis requise pour certaines formations d'origine).
 */
@Service
public class AvisParFormationOrigineManager extends AbstractManager {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(AvisParFormationOrigineManager.class);

    /**
     * Le dao utilis� pour les avis par formation d'origine.
     */
    private AvisParFormationOrigineDao avisParMefODao;

    /**
     * @param avisParMefODao
     *            the avisParMefODao to set
     */
    @Autowired
    public void setAvisParMefODao(AvisParFormationOrigineDao avisParMefODao) {
        this.avisParMefODao = avisParMefODao;
    }

    /**
     * Ajoute un avis entre une formation d'origine et un param�tre par formation d'accueil.
     * 
     * @param avisParFormationOrigine
     *            Le lien � ajouter.
     * @return Le lien venant d'�tre cr�er
     */
    public AvisParFormationOrigine ajouter(AvisParFormationOrigine avisParFormationOrigine) {

        if (avisParMefODao.isUnique(avisParFormationOrigine.getParametresFormationAccueil().getId(),
                avisParFormationOrigine.getMnemonique(), avisParFormationOrigine.getCodeSpecialite(),
                avisParFormationOrigine.getOption1(), avisParFormationOrigine.getOption2())) {

            LOG.debug("Ajout d'un avis pour entre " + avisParFormationOrigine.toString());
            return avisParMefODao.creer(avisParFormationOrigine);
        }
        LOG.error("L'avis par formation origine existe d�j� : " + avisParFormationOrigine.toString());

        throw new DaoException(
                "Cette formation d'origine a d�j� besoin d'un avis pour cette formation d'accueil.");
    }

    /**
     * Retourne la liste des formations d'origine dont l'avis est demand� pour cette formation d'accueil.
     * 
     * @param idPfa
     *            L'id du param�tre par formartion d'accueil
     * @return La liste des formations d'origine dont l'avis est demand� pour
     *         cette formation d'accueil ou une liste vide.
     */
    public List<AvisParFormationOrigine> recupereFormationsOrigines(String idPfa) {
        List<Filtre> listFilters = new ArrayList<Filtre>(1);
        listFilters.add(Filtre.equal("parametresFormationAccueil.id", Long.valueOf(idPfa)));

        return avisParMefODao.lister(listFilters, false);
    }

    /**
     * Ajoute un avis par formation d'origine dans la base de donn�es.
     * 
     * @param avisParFormationOrigine
     *            L'avis par formation d'origine a ajout�.
     * @return L'avis par formation d'origine cr��.
     */
    public AvisParFormationOrigine sauve(AvisParFormationOrigine avisParFormationOrigine) {
        return avisParMefODao.creer(avisParFormationOrigine);
    }

    /**
     * Supprime un avis par formation d'origine de la base de donn�es.
     * 
     * @param avisParFormationOrigine
     *            L'avis par formation d'origine a supprim�.
     */
    public void supprime(AvisParFormationOrigine avisParFormationOrigine) {
        avisParMefODao.supprimer(avisParFormationOrigine);
    }

    /**
     * V�rifie si les options d'un param�trage d'avis par formation origine
     * fourni correspondent aux options d'un �l�ve.
     * 
     * @param avisMefO
     *            Le param�trage d'avis par formation d'origine dont il faut v�rifier les options.
     * @param eleve
     *            L'eleve dont on v�rifie les options
     * @return
     *         <ul>
     *         <li>true si les options correspondent</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean verifieOptionsAvisParFormationOrigine(AvisParFormationOrigine avisMefO, Eleve eleve) {
        Matiere opt1 = avisMefO.getOption1();
        Matiere opt2 = avisMefO.getOption2();

        // pas d'options pr�cis�es (le param�trage est g�n�rique $ $)
        if (opt1 == null && opt2 == null) {
            return true;
        }

        Matiere opt1Eleve = eleve.getOptionOrigine2();
        Matiere opt2Eleve = eleve.getOptionOrigine3();

        // 1 seule option pr�cis�e
        if (opt1 != null && opt2 == null) {
            if (opt1.equals(opt1Eleve) || opt1.equals(opt2Eleve)) {
                return true;
            }
        }
        // 2 options pr�cis�es
        else if (opt1 != null && opt2 != null && ((opt1.equals(opt1Eleve) && opt2.equals(opt2Eleve))
                || (opt1.equals(opt2Eleve) && opt2.equals(opt1Eleve)))) {
            return true;
        }
        return false;
    }
}
