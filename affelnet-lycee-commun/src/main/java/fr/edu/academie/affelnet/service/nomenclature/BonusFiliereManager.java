/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.BonusFiliereComparator;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.BonusFiliereDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/**
 * Manager pour la gestion des <code>BonusFilere</code>.
 */
@Service
public class BonusFiliereManager extends AbstractManager {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(BonusFiliereManager.class);

    /**
     * Le dao utilis� pour les bonus fili�re.
     */
    private BonusFiliereDao bonusFiliereDao;

    /**
     * Le manager utilis� pour les formations.
     */
    private FormationManager formationManager;

    /**
     * Le manager utilis� pour les mati�res.
     */
    private MatiereManager matiereManager;

    /**
     * Le manager utilis� pour la notion d'ouverture.
     */
    private OuvrableManager ouvrableManager;

    /**
     * Le manager utilis� pour les formations-mati�res.
     */
    private FormationMatiereManager formationMatiereManager;

    /** Le DAO des �l�ves. */
    private EleveDao eleveDao;

    /** Le gestionnaire d'op�rations programm�es. */
    private OperationProgrammeeAffectationManager operationProgrammeeAffectationManager;

    /**
     * @param bonusFiliereDao
     *            the bonusFiliereDao to set
     */
    @Autowired
    public void setBonusFiliereDao(BonusFiliereDao bonusFiliereDao) {
        this.bonusFiliereDao = bonusFiliereDao;
    }

    /**
     * @param formationManager
     *            the formationManager to set
     */
    @Autowired
    public void setFormationManager(FormationManager formationManager) {
        this.formationManager = formationManager;
    }

    /**
     * @param ouvrableManager
     *            the ouvrableManager to set
     */
    @Autowired
    public void setOuvrableManager(OuvrableManager ouvrableManager) {
        this.ouvrableManager = ouvrableManager;
    }

    /**
     * @param formationMatiereManager
     *            the formationMatiereManager to set
     */
    @Autowired
    public void setFormationMatiereManager(FormationMatiereManager formationMatiereManager) {
        this.formationMatiereManager = formationMatiereManager;
    }

    /**
     * @param matiereManager
     *            the matiereManager to set
     */
    @Autowired
    public void setMatiereManager(MatiereManager matiereManager) {
        this.matiereManager = matiereManager;
    }

    /**
     * @param eleveDao
     *            le DAO �l�ves
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * @param operationProgrammeeAffectationManager
     *            le gestionnaire d'OPA.
     */
    @Autowired
    public void setOperationProgrammeeAffectationManager(
            OperationProgrammeeAffectationManager operationProgrammeeAffectationManager) {
        this.operationProgrammeeAffectationManager = operationProgrammeeAffectationManager;
    }

    /**
     * Cr�ation d'un bonus fili�re.
     * 
     * @param bonusFiliere
     *            le bonus fili�re � cr�er
     * @param listeErreursValidation
     *            la liste des erreurs de validation �ventuelles
     * @return le bonus fili�re cr��
     */
    public Object creer(BonusFiliere bonusFiliere, List<String> listeErreursValidation) {

        LOG.debug("Cr�ation du bonus fili�re : " + bonusFiliere.toString());

        validationMetier(bonusFiliere, listeErreursValidation);
        bonusFiliere.setDateCreation(new Date());
        BonusFiliere bonusFiliereCree = bonusFiliereDao.creer(bonusFiliere);
        obligerRelanceOpa(bonusFiliereCree);
        return bonusFiliereCree;
    }

    /**
     * Charge le bonus fili�re dont l'identifiant est indiqu�.
     * 
     * @param key
     *            identifiant du bonus fili�re
     * @return le bonus fili�re
     */
    public BonusFiliere charger(Long key) {
        return bonusFiliereDao.charger(key);
    }

    /**
     * Met � jour un bonus fili�re.
     * 
     * @param bonusFiliere
     *            le bonus fili�re � mettre � jour
     * @param ancienId
     *            l'ancien identifiant du bonus fili�re
     * @param listeErreursValidation
     *            liste des erreurs de validation
     * @return le bonus fili�re mis � jour
     */
    public BonusFiliere mettreAJour(BonusFiliere bonusFiliere, Long ancienId,
            List<String> listeErreursValidation) {

        LOG.debug("Mise � jour d'un bonus fili�re");
        bonusFiliere.setDateMAJ(new Date());

        validationMetier(bonusFiliere, listeErreursValidation);

        BonusFiliere ancienBonusFiliere = bonusFiliereDao.charger(ancienId);
        LOG.debug("Ancien bonus fili�re : " + ancienBonusFiliere);
        obligerRelanceOpa(ancienBonusFiliere);

        LOG.debug("Nouveau bonus fili�re : " + bonusFiliere);
        BonusFiliere bonusFiliereModifie = bonusFiliereDao.maj(bonusFiliere, ancienId);
        obligerRelanceOpa(bonusFiliereModifie);

        return bonusFiliereModifie;
    }

    /**
     * Supprime le bonus fili�re fourni.
     * 
     * @param bonusFiliere
     *            le bonus fili�re � supprimer
     */
    public void supprimer(BonusFiliere bonusFiliere) {

        LOG.debug("Supprime le bonus fili�re " + bonusFiliere);
        obligerRelanceOpa(bonusFiliere);
        bonusFiliereDao.supprimer(bonusFiliere);
    }

    /**
     * Applique les r�gles m�tiers sur l'objet et rempli la liste des erreurs de
     * validation si l'objet n'est pas valide.
     * 
     * @param bonusFiliere
     *            l'objet � valider
     * @param listeErreursValidation
     *            la liste des �ventuelles erreurs de validation
     */
    public void validationMetier(BonusFiliere bonusFiliere, List<String> listeErreursValidation) {
        Formation formationOrigine = bonusFiliere.getFormationOrigine();
        Matiere option1Origine = bonusFiliere.getOptionOrigine1();
        Matiere option2Origine = bonusFiliere.getOptionOrigine2();

        // le MEF d'origine doit r�pondre � la notion d'origine
        if (formationOrigine != null && !ouvrableManager.isOrigine(formationOrigine)) {
            listeErreursValidation.add("La formation en cours " + formationOrigine.getId().getMnemonique() + " "
                    + formationOrigine.getId().getCodeSpecialite() + " n'est pas ouverte");
        }

        // l'option d'origine 1 doit r�pondre � la notion d'origine
        if (option1Origine != null && !ouvrableManager.isOrigine(option1Origine)) {
            listeErreursValidation
                    .add("L'option d'origine 1 " + option1Origine.getCleGestion() + " n'est pas ouverte");
        }

        // l'option d'origine 2 doit r�pondre � la notion d'origine
        if (option2Origine != null && !ouvrableManager.isOrigine(option2Origine)) {
            listeErreursValidation
                    .add("L'option d'origine 2 " + option2Origine.getCleGestion() + " n'est pas ouverte");
        }

        bonusFiliere.setEOGeneriqusAutorise(true);
        // validation de la notion d'accueil pour la formation d'accueil et son �ventuel enseignement optionnel
        formationMatiereManager.validationOuvertureEO(bonusFiliere, listeErreursValidation);

        // validation de la formation d'accueil avec l'enseignement optionnel
        formationMatiereManager.validationEOPourFormation(bonusFiliere, listeErreursValidation);

        validationHorsFiliereExploratoire2Gt1Gt(bonusFiliere, listeErreursValidation);

        // controle d'unicit� : formation origine / options origine 1 & 2 /
        // formation accueil / ens. optionnel
        if (!bonusFiliereDao.isUnique(bonusFiliere)) {
            listeErreursValidation
                    .add("Il existe d�j� un enregistrement avec ces formations en cours et d'accueil");
        }

        if (!listeErreursValidation.isEmpty()) {
            throw new ValidationException(listeErreursValidation);
        }
    }

    /**
     * Valide que l'utilisation du bonus fili�re s'effectue hors d'une fili�re
     * "exploratoire".
     * 
     * @param bonusFiliere
     *            Bonus fili�re � contr�ler
     * @param listeErreursValidation
     *            liste des erreurs de validation � compl�ter en cas de probl�me
     * 
     *            Cette m�thode v�rifie que le bonus fili�re ne puisse pas �tre
     *            attribu� dans le cas de passage d'une MefStat4 de 2nde GT � 1�re
     *            GT. Note : Si le mefstat4 de 2nde GT est d�tect�, on interdit la
     *            saisie de $ en mn�monique de formation d'accueil.
     */
    public void validationHorsFiliereExploratoire2Gt1Gt(BonusFiliere bonusFiliere,
            List<String> listeErreursValidation) {

        List<String> listeMefStat4Origine = formationManager.getListeMefStat4PourFormationOuMnemonique(
                bonusFiliere.getMnemoniqueOrigine(), bonusFiliere.getFormationOrigine());

        if (listeMefStat4Origine.contains(MefStat.CODE_MEFSTAT4_2GT)) {
            String mnemoniqueAccueil = bonusFiliere.getMnemoniqueAccueil();

            if (StringUtils.isEmpty(mnemoniqueAccueil)) {
                LOG.error("Bonus fili�re non autoris� : " + " Mefstat4 Origine " + MefStat.CODE_MEFSTAT4_2GT
                        + " et Mn�monique de formation d'Accueil g�n�rique ($).");
                listeErreursValidation.add("Attribution du bonus fili�re non autoris�e");
            } else {
                Formation formationAccueil = bonusFiliere.getFormationAccueil();

                List<String> listeMefStat4Accueil = formationManager
                        .getListeMefStat4PourFormationOuMnemonique(mnemoniqueAccueil, formationAccueil);

                if (listeMefStat4Accueil.contains(MefStat.CODE_MEFSTAT4_1GT)) {

                    LOG.error("Bonus fili�re non autoris� : " + " Mefstat4 Origine " + MefStat.CODE_MEFSTAT4_2GT
                            + " et Mefstat4 Accueil " + MefStat.CODE_MEFSTAT4_1GT + ".");
                    listeErreursValidation.add("Attribution du bonus fili�re non autoris�e");

                } // if -- MefStat Accueil 1GT

            } // if -- mn�monique d'accueil g�n�rique

        } // if -- MefStat Origine 2GT
    }

    /**
     * Liste les <code>BonusFiliere</code> ayant une formation d'origine 2GT.
     * 
     * @return la liste des <code>BonusFiliere</code> correspondant.
     */
    public List<BonusFiliere> listeBonusFilieres2GT() {
        return bonusFiliereDao.listerBonusFilieresPourMefStat4(MefStat.CODE_MEFSTAT4_2GT);
    }

    /**
     * M�thode permettant de r�cup�rer tous les bonus de fili�re utilisant des
     * formations sp�cifiques.
     * 
     * @param formations
     *            la liste des formations utilis�es par les bonus de fili�re
     * @return les bonus de fili�re utilisant les formations en param�tre.
     */
    public List<BonusFiliere> bonusFiliereAvecFormation(List<Formation> formations) {
        if (formations.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<Filtre> filtresFormations = new ArrayList<>();

            for (Formation formation : formations) {
                filtresFormations
                        .add(Filtre.and(Filtre.equal("mnemoniqueAccueil", formation.getId().getMnemonique()),
                                Filtre.equal("codeSpecialiteAccueil", formation.getId().getCodeSpecialite())));
            }

            return bonusFiliereDao.lister(Filtre.or(filtresFormations));
        }
    }

    /**
     * Oblige la relance des OPA concern�es par le bonus fili�re, si on estime que
     * c'est n�cessaire.
     * 
     * @param bonusFiliere
     *            le bonus fili�re
     */
    private void obligerRelanceOpa(BonusFiliere bonusFiliere) {
        // Le recherche des OPA peut �tre assez longue, � r�server si le bonus fili�re
        // est utilis�
        // => test pr�alable de l'utilit�
        if (estFormationOrigineUtilisee(bonusFiliere)) {
            LOG.debug("Des �l�ves sont potentiellement concern�s par ce parametre => impact sur les OPA.");
            operationProgrammeeAffectationManager.obligerRelanceOpa(bonusFiliere);
        } else {
            LOG.debug("Aucun �l�ve n'est actuellement concern� par ce parametre => pas d'impact sur les OPA.");
        }
    }

    /**
     * V�rifie de l'existence d'�l�ves issus d'une formation concern�e par le bonus
     * fili�re (par mn�monique seulement).
     * 
     * @param bonusFiliere
     *            le bonus fili�re
     * 
     * @return vrai s'il y a des �l�ves qui viennent d'une formation concern�e,
     *         sinon faux
     */
    public boolean estFormationOrigineUtilisee(BonusFiliere bonusFiliere) {
        return eleveDao.estMnemoniqueFormationOrigineEleve(bonusFiliere.getMnemoniqueOrigine());
    }

    /**
     * R�cup�re le bonus fili�re le plus proche correspondant aux informations.
     * 
     * @param formationO
     *            la <code>Formation</code> d'origine
     * @param lOpt
     *            la <code>List</code> des options d'origine
     * @param formationA
     *            la <code>Formation</code> d'accueil
     * @param ensOpt
     *            ll'enseignement optionnel
     * @return le <code>BonusFiliere</code> correspondant ou <code>null</code> sinon
     */
    public BonusFiliere trouverBonusFiliereLePlusProche(Formation formationO, List<Matiere> lOpt,
            Formation formationA, Matiere ensOpt) {
        return trouverBonusFiliereLePlusProche(formationO, lOpt, formationA, ensOpt, null);
    }

    /**
     * R�cup�re le bonus filli�re le plus proche correspondant aux informations et alimente le cache si il est
     * fourni.
     * 
     * @param formationOrigine
     *            la formation d'origine
     * @param lOpt
     *            la liste des options d'origine
     * @param formationAccueil
     *            la formation d'accueil
     * @param ensOpt
     *            l'enseignement optionnel
     * @param cacheMap
     *            le cache
     * @return le bonus fili�re � appliquer
     */
    @SuppressWarnings("unchecked")
    public BonusFiliere trouverBonusFiliereLePlusProche(Formation formationOrigine, List<Matiere> lOpt,
            Formation formationAccueil, Matiere ensOpt, Map<String, Object> cacheMap) {

        // r�cup�ration de la liste des bonus filiere
        final String cacheName = "listeBonusFiliere";
        List<BonusFiliere> lBonFil;
        if (cacheMap == null) {
            // si pas de cache, on r�duit les possibilit�s en mettant les
            // crit�res obligatoires
            lBonFil = bonusFiliereDao.listerBonusFiliere(formationOrigine);
        } else if (!cacheMap.containsKey(cacheName)) {
            lBonFil = bonusFiliereDao.listerBonusFiliereFetch();
            cacheMap.put(cacheName, new ArrayList<>(lBonFil));
        } else {
            // new pour ne pas alterer la liste en cache
            lBonFil = new ArrayList<>((List<BonusFiliere>) cacheMap.get(cacheName));
        }

        List<Predicate<BonusFiliere>> predicats = listePredicat(formationOrigine, formationAccueil, lOpt, ensOpt,
                cacheMap);
        for (Predicate<BonusFiliere> predicat : predicats) {
            lBonFil.removeIf(predicat);
        }

        // pas de bonus corrects
        if (lBonFil.isEmpty()) {
            return null;
        }

        // il reste les �l�ments qui sont corrects, on les trie et on restitue
        // le premier
        TreeSet<BonusFiliere> listeTriee = new TreeSet<>(new BonusFiliereComparator());
        listeTriee.addAll(lBonFil);
        return listeTriee.first();
    }

    /**
     * Fournis la liste des pr�dicats pour �liminer tous les bonus fili�res qui ne correspondent pas aux param�tres
     * fournis
     * 
     * @param formationOrigine
     *            la formation d'origine
     * @param formationAccueil
     *            la formation d'accueil
     * @param options
     *            les options
     * @param enseignementOptionnel
     *            l'enseignement optionnel
     * @param cacheMap
     *            le cache
     * @return la liste des pr�dicats pour filter les bonus fili�re
     */
    private List<Predicate<BonusFiliere>> listePredicat(Formation formationOrigine, Formation formationAccueil,
            List<Matiere> options, Matiere enseignementOptionnel, Map<String, Object> cacheMap) {
        List<Predicate<BonusFiliere>> predicats = new ArrayList<>();

        // M�me formation origine
        predicats.add(bonus -> !bonus.getMnemoniqueOrigine().equals(formationOrigine.getId().getMnemonique()));
        predicats.add(bonus -> bonus.getCodeSpecialiteOrigine() != null
                && !bonus.getCodeSpecialiteOrigine().equals(formationOrigine.getId().getCodeSpecialite()));

        // M�me formation accueil
        predicats.add(bonus -> bonus.getMnemoniqueAccueil() != null
                && !bonus.getMnemoniqueAccueil().equals(formationAccueil.getId().getMnemonique()));
        predicats.add(bonus -> bonus.getCodeSpecialiteAccueil() != null
                && !bonus.getCodeSpecialiteAccueil().equals(formationAccueil.getId().getCodeSpecialite()));

        // Correspondance des options et de l'enseignement optionnel
        predicats.add(bonus -> bonus.getOptionOrigine1() != null
                && !matiereManager.containsMatOrLV(bonus.getOptionOrigine1(), options, cacheMap));
        predicats.add(bonus -> bonus.getOptionOrigine2() != null
                && !matiereManager.containsMatOrLV(bonus.getOptionOrigine2(), options, cacheMap));
        predicats.add(bonus -> bonus.getMatiereEnseigOptionnel() != null
                && !bonus.getMatiereEnseigOptionnel().equals(enseignementOptionnel));

        return predicats;
    }
}
