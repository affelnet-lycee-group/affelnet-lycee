/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.nomenclature.BonusDao;
import fr.edu.academie.affelnet.domain.nomenclature.Bonus;
import fr.edu.academie.affelnet.domain.nomenclature.TypeBonus;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/**
 * le Manager de gestion des Bonus.
 */
@Service
public class BonusManager extends AbstractManager {
    /** Le loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(BonusManager.class);

    /** Le Dao pour les bonus. */
    private BonusDao bonusDao;

    /** Le Dao pour les bonus. */
    private OperationProgrammeeAffectationManager opaManager;

    /**
     * @param bonusDao
     *            the bonusDao to set
     */
    @Autowired
    public void setBonusDao(BonusDao bonusDao) {
        this.bonusDao = bonusDao;
    }

    /**
     * @param opaManager
     *            the opaManager to set
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * Fournis la map des bonus avec le type du bonus en cl� et le bonus comme valeur.
     * 
     * @return une map de corresponbdance TypeBonus/Bonus
     */
    public Map<TypeBonus, Bonus> bonusMap() {
        return bonusDao.mapBonus();
    }

    /**
     * Met � jour le bonus fourni en effectuant les actions n�cessaire (relance des OPA, mise � jour de la date de
     * modification ...).
     * 
     * @param bonus
     *            le bonus modifi�
     * @return le bonus enregistr�
     */
    public Bonus maj(Bonus bonus) {
        LOG.debug("Mise � jour d'un bonus et relance des OPA");
        opaManager.obligerRelanceOpaTour();
        bonus.setDateMAJ(new Date());
        return bonusDao.maj(bonus, bonus.getType());
    }
}
