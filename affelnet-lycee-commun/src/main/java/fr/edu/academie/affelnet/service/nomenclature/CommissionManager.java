/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.nomenclature.CommissionDao;
import fr.edu.academie.affelnet.domain.affectation.DecisionCommission;
import fr.edu.academie.affelnet.domain.nomenclature.Commission;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.ParametreManager;
import fr.edu.academie.affelnet.service.interfaces.nomenclature.OffreFormationManager;

/**
 * La classe g�rant les commissions.
 */
@Service
public class CommissionManager extends AbstractManager {

    /**
     * Le dao utilis� pour les commissions.
     */
    private CommissionDao commissionDao;

    /** Le gestionnaire des param�tres. */
    private ParametreManager parametreManager;

    /** Le gestionnaire des offres de formation. */
    private OffreFormationManager offreFormationManager;

    /**
     * @param commissionDao
     *            the commissionDao to set
     */
    @Autowired
    public void setCommissionDao(CommissionDao commissionDao) {
        this.commissionDao = commissionDao;
    }

    /**
     * @param parametreManager
     *            the parametreManager to set
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param offreFormationManager
     *            le gestionnaire des offres de formation
     */
    @Autowired
    public void setOffreFormationManager(OffreFormationManager offreFormationManager) {
        this.offreFormationManager = offreFormationManager;
    }

    /**
     * Charge une commission depuis la base de donn�es.
     * 
     * @param codeCommission
     *            Le code de la commission d�sir�e.
     * @return La commission correspondant au code.
     */
    public Commission charger(String codeCommission) {
        return commissionDao.charger(codeCommission);
    }

    /**
     * Charge une commission � partir de son code si elle existe.
     * 
     * @param codeCommission
     *            Le code de la commission d�sir�e.
     * @return La commission correspondant au code ou null si elle est introuvable
     */
    public Commission chargerNullable(String codeCommission) {
        return commissionDao.chargerNullable(codeCommission);
    }

    /**
     * Indique si une commission existe d�j� en base de donn�es.
     * 
     * @param codeCommission
     *            Le code de la commission dont on v�rifie l'existence.
     * @return
     *         <ul>
     *         <li>true si la commission est d�j� pr�sente en base de donn�es</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean existe(String codeCommission) {
        return commissionDao.existe(codeCommission);
    }

    /**
     * Permet de construire une map des libell�s de d�cisions par commision par code de commission.
     * 
     * @return La map liant les codes de d�cision avec le libell� correspondant.
     */
    public Map<String, String> getLibelleDecisionCommissionParCode() {
        Map<String, String> libelleParCode = new HashMap<>();

        for (DecisionCommission decision : DecisionCommission.values()) {
            libelleParCode.put(decision.getCode(), decision.getLibelleLong());
        }

        String libelleDecisionRecensement = parametreManager.getLibelleDecisionRecensement();

        // AJout manuel de l'entr�e pour une commission de recensement.
        libelleParCode.put(DecisionCommission.RECENSEMENT.getCode(), libelleDecisionRecensement);

        return libelleParCode;
    }

    /**
     * Cr�ation d'une commission.
     * 
     * @param commission
     *            la commission
     * 
     */
    public void creer(Commission commission) {
        this.commissionDao.creer(commission);
    }

    /**
     * Teste si une commission est verrouill�e (sans offre de formation reli�e).
     * 
     * @param codeCommission
     *            code de la commission
     * @return vrai si la commission n'a pas d'offres reli�es, sinon faux
     */
    public boolean estCommissionVerrouillee(String codeCommission) {
        return offreFormationManager.listerOffresCommission(codeCommission).isEmpty();
    }

}
