/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.DerogationDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.LienZoneGeoDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.DerogationVoeuEleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Bonus;
import fr.edu.academie.affelnet.domain.nomenclature.Derogation;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.voeu.DerogationVoeuEleve;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.ParametreManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.web.utils.FiltreUtils;

/** Cette classe regroupe les traitements sur les crit�res de d�rogation. */
@Service
public class DerogationManager extends AbstractManager {

    /**
     * Le dao utilis� pour les d�rogations.
     */
    private DerogationDao derogationDao;

    /**
     * Le dao utilis� pour les demandes de d�rogation pour les voeux des �l�ves.
     */
    private DerogationVoeuEleveDao derogationVoeuEleveDao;

    /**
     * Le dao utilis� pour les liens zones g�ographiques.
     */
    private LienZoneGeoDao lienZoneGeoDao;

    /**
     * Le manager utilis� pour les param�tres.
     */
    private ParametreManager parametreManager;

    /**
     * Le manager utilis� pour les campagnes.
     */
    private CampagneAffectationManager campagneAffectationManager;

    /**
     * Le manager utilis� pour les OPA.
     */
    private OperationProgrammeeAffectationManager opaManager;

    /**
     * @param derogationDao
     *            the derogationDao to set
     */
    @Autowired
    public void setDerogationDao(DerogationDao derogationDao) {
        this.derogationDao = derogationDao;
    }

    /**
     * @param derogationVoeuEleveDao
     *            the derogationVoeuEleveDao to set
     */
    @Autowired
    public void setDerogationVoeuEleveDao(DerogationVoeuEleveDao derogationVoeuEleveDao) {
        this.derogationVoeuEleveDao = derogationVoeuEleveDao;
    }

    /**
     * @param lienZoneGeoDao
     *            the lienZoneGeoDao to set
     */
    @Autowired
    public void setLienZoneGeoDao(LienZoneGeoDao lienZoneGeoDao) {
        this.lienZoneGeoDao = lienZoneGeoDao;
    }

    /**
     * @param parametreManager
     *            the parametreManager to set
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param campagneAffectationManager
     *            the campagneAffectationManager to set
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneAffectationManager = campagneAffectationManager;
    }

    /**
     * @param opaManager
     *            the opaManager to set
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * V�rifie le bonus de d�rogation (v�rifie uniquement la somme des bonus par
     * rapport au plus petit bonus lien zone g�ographique et ignorer le caract�re d�gressif des bonus).<br>
     * Si la m�thode ne retourne pas d'exception, on consid�re que la v�rification s'est bien pass�e
     * 
     * @param erreurs
     *            la liste des erreurs de validation
     * @return true si la v�rification s'est termin�e sans rencontrer d'incoh�rence
     */
    public boolean verifBonusDerogation(List<String> erreurs) {
        List<Derogation> listeDerogations = getListeDerogationsAvecValeurCritereSupplementaire();
        return verifBonusDerogation(listeDerogations, erreurs);
    }

    /**
     * M�thode de v�rification des bonus de d�rogation, v�rifiant si la somme des bonus est inf�rieure
     * au plus petit bonus g�ographique et si les bonus sont bien d�gressifs.
     * 
     * @param listeDerogations
     *            la liste des d�rogations
     * @param erreurs
     *            la liste des erreurs de validation
     * @return true si la v�rification s'est termin�e sans rencontrer d'incoh�rence
     */
    public boolean verifBonusDerogation(List<Derogation> listeDerogations, List<String> erreurs) {
        return verifBonusDerogationBornes(listeDerogations, erreurs)
                && verifBonusDerogationDecroissance(listeDerogations, erreurs)
                && verifBonusDerogationSommeInferieureZoneGeo(listeDerogations, erreurs);
    }

    /**
     * V�rifie que tous les bonus d�rogatoires sont compris dans les bornes d�finies.
     * 
     * @param listeDerogations
     *            la liste des d�rogations
     * @param erreurs
     *            la liste des messages d'erreur de validation
     * @return true si l'ensemble des bonus sont correctement born�s
     */
    private boolean verifBonusDerogationBornes(List<Derogation> listeDerogations, List<String> erreurs) {
        boolean verif = true;
        for (Derogation derogation : listeDerogations) {
            int bonus = derogation.getBonus();
            if (bonus < Bonus.BONUS_MINIMAL || bonus > Bonus.BONUS_MAXIMAL) {
                verif = false;
                erreurs.add("La valeur du bonus du crit&egrave;re \"" + derogation.getLibelle() + "\" (" + bonus
                        + ") doit &ecirc;tre comprise entre 0 et " + Bonus.BONUS_MAXIMAL + ".");
            }
        }
        return verif;
    }

    /**
     * V�rifie que la valeur des bonus d�rogatoires non m�dicaux (� partir du 3�me) sont d�croissantes.
     * 
     * @param listeDerogations
     *            la liste des d�rogations
     * @param erreurs
     *            la liste des messages d'erreur de validation
     * @return true si les bonus non m�dicaux sont d�croissants
     */
    private boolean verifBonusDerogationDecroissance(List<Derogation> listeDerogations, List<String> erreurs) {
        int bonusPrecedent = -1;
        String libellePrecedent = "";
        boolean verif = true;

        // il faut au moins 2 �l�ments dans la liste (taille >= 2) on commence � 2 car on exclut les bonus
        // pour crit�res m�dicaux (les 2 premiers de la liste)
        if (listeDerogations != null && listeDerogations.size() >= 2) {
            ListIterator<Derogation> iterListeDerogations = listeDerogations.listIterator(2);
            while (iterListeDerogations.hasNext()) {
                Derogation derogation = iterListeDerogations.next();
                int bonus = derogation.getBonus();

                if (bonusPrecedent == -1 || bonus == 0 || bonus < bonusPrecedent) {
                    bonusPrecedent = bonus;
                    libellePrecedent = derogation.getLibelle();
                } else {
                    verif = false;
                    erreurs.add("La valeur du bonus du crit&egrave;re \"" + derogation.getLibelle() + "\" ("
                            + bonus + ") est sup&eacute;rieure ou &eacute;gale &agrave; celle du crit&egrave;re \""
                            + libellePrecedent + "\"(" + bonusPrecedent + ")");
                }
            }
        }
        return verif;
    }

    /**
     * V�rifie que la somme des valeurs des bonus d�rogatoires non m�dicaux est inf�rieure au plus faible faible
     * bonus de lien des zones g�ographiques.
     * 
     * @param listeDerogations
     *            la liste des d�rogations
     * @param erreurs
     *            la liste des messages d'erreur de validation
     * @return true si la somme des bonus d�rogatoire est inf�rieure aux bonus zones g�ographiques
     */
    private boolean verifBonusDerogationSommeInferieureZoneGeo(List<Derogation> listeDerogations,
            List<String> erreurs) {
        // Calcul de la somme des bonus
        int sommeBonus = 0;

        for (Derogation derogation : listeDerogations) {
            if (derogation.getOrdre() > 2) {
                sommeBonus += derogation.getBonus();
            }
        }

        // R�cup�ration du bonus minimal non null des zones g�ographiques
        Filtre filtre = Filtre.superieur("bonus", 0);

        List<Tri> tris = new ArrayList<Tri>();
        tris.add(Tri.asc("bonus"));

        List<LienZoneGeo> listLienZoneGeo = lienZoneGeoDao.lister(filtre, tris);

        int bonusLienZoneGeographique;
        // si la liste n'est pas vide
        if (listLienZoneGeo != null && !listLienZoneGeo.isEmpty()) {
            LienZoneGeo plusPetitBonusLienZoneGeo = listLienZoneGeo.get(0);
            bonusLienZoneGeographique = plusPetitBonusLienZoneGeo.getBonus();
        } else {
            // on met le plus petit bonus lienzone geo � l'infini (temporairement)
            // pour passer le test si il n'y a pas de bonus lien zone g�o
            bonusLienZoneGeographique = Integer.MAX_VALUE;
        }

        if (sommeBonus >= bonusLienZoneGeographique) {
            erreurs.add("La somme de tous les bonus \"crit&egrave;res de "
                    + "d&eacute;rogation\" except&eacute;s les " + "crit&egrave;res m&eacute;dicaux (" + sommeBonus
                    + ") " + "doit &ecirc;tre strictement inf&eacute;rieure au plus "
                    + "petit bonus \"lien zone g&eacute;ographique\" " + "(" + bonusLienZoneGeographique + ")");

            return false;
        } else {
            return true;
        }
    }

    /**
     * @return la liste des crit�res de derogations en fonctions de la valeur du flag crit�re acad�mique
     */
    public List<Derogation> getListeDerogationsAvecValeurCritereSupplementaire() {
        List<Derogation> listeDerogations = null;

        String flagCritereSupDerogation = parametreManager.getFlagCritereAcademique();
        if (flagCritereSupDerogation.equals(Flag.NON)) {
            List<Filtre> filtres = new ArrayList<Filtre>();
            filtres.add(Filtre.notEqual("id", Derogation.CODE_DEROGATION_AUTRE_MOTIF));
            listeDerogations = derogationDao.lister(filtres, false);
        } else {
            listeDerogations = derogationDao.lister(false);
            // retourne l'indice du code d�rogation autre motif
            int index = derogationDao.getIndexOf(null, null, Derogation.CODE_DEROGATION_AUTRE_MOTIF) - 1;
            if (listeDerogations != null && !listeDerogations.isEmpty() && index >= 0
                    && index < listeDerogations.size()) {
                Derogation derogation = listeDerogations.get(index);
                // et remplace le libelle "autre motif" par crit�re acad�mique
                derogation.setLibelle(parametreManager.getLibelleCritereAcademique());
                // � cause d'hibernate
                listeDerogations.set(index, derogation);
            }
        }

        return listeDerogations;
    }

    /**
     * M�thode permettant d'abtenir la liste des demandes de d�rogations pour les PSP non valid�es.
     * 
     * @param filtresSupplementaire
     *            les filtres suppl�mentaires � appliquer
     * @return la liste des demandes de d�rogations pour les PSP.
     */
    public List<DerogationVoeuEleve> listerDerogationsPSPNonValidees(List<Filtre> filtresSupplementaire) {
        // Ajout du filtre parcours scolaire particuliers.
        List<Filtre> listFiltres = new ArrayList<Filtre>();
        if (filtresSupplementaire != null) {
            listFiltres.addAll(filtresSupplementaire);
        }

        listFiltres.add(
                Filtre.equal("id.voeuEleve.id.numeroTour", campagneAffectationManager.getNumeroTourCourant()));
        FiltreUtils.ajouterCritereEgal(listFiltres, "id.derogation.id", 4);
        FiltreUtils.ajouterCritereIsNull(listFiltres, "id.voeuEleve.flagValidationParcoursScolaireParticulier");
        listFiltres.add(Filtre.equal("id.voeuEleve.flagRefusVoeu", Flag.NON));
        listFiltres.add(Filtre.equal("id.voeuEleve.candidature.flagEleveForce", Flag.NON));

        return derogationVoeuEleveDao.lister(listFiltres);
    }

    /**
     * Enregistre en base de donn�es la d�rogation.
     * 
     * @param derogation
     *            La d�rogation � enregistrer en base de donn�es.
     * @return la d�rogation
     */
    public Derogation maj(Derogation derogation) {
        opaManager.obligerRelanceOpaTour();
        derogation.setDateMAJ(new Date());
        return derogationDao.maj(derogation, derogation.getId());
    }
}
