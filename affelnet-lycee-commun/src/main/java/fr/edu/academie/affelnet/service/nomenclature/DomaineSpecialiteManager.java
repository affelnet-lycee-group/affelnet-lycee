/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.evaluation.DomaineSpecialiteDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DomaineSpecialite;
import fr.edu.academie.affelnet.service.AbstractManager;

/** Gestionnaire pour les pojo domaines de sp�cialit�s. */
@Service
public class DomaineSpecialiteManager extends AbstractManager {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(DomaineSpecialiteManager.class);

    /**
     * Le DAO des domaines de sp�cialit�.
     */
    private DomaineSpecialiteDao domaineSpecialiteDao;

    /**
     * Valorise le DAO des domaines de sp�cialit�.
     * 
     * @param domaineSpecialiteDao
     *            le DAO des domaines de sp�cialit�
     */
    @Autowired
    public void setDomaineSpecialiteDao(DomaineSpecialiteDao domaineSpecialiteDao) {
        this.domaineSpecialiteDao = domaineSpecialiteDao;
    }

    /**
     * Cr�er une m�thode qui permet d'apr�s une formation de r�cup�rer le domaine de sp�cialit�
     * ou v�rifier si il en existe une au niveau du formation manager :
     * Extraction des 2 caract�res 7 et 8 du mefstat11 et select d'apr�s le domaine de sp�cialit�.
     * 
     * @param formation
     *            la formation
     * @return Si on retourne un r�sultat c'est que le domaine de sp�cialit� pour la formation en param�tre existe,
     *         sinon on retourne null
     */
    public DomaineSpecialite recuperationDomaineSpecialite(Formation formation) {
        LOG.debug("V�rification si la formation " + formation.getLibelleLong()
                + " est une formation dans le domaine sp�cialit� ");

        // R�cup�ration des caract�res 7 et 8 du mefstat11 pour savoir si la formation fait partie d'un domaine
        // de sp�cialit�
        MefStat mefStat = formation.getMefStat();
        String codeMefStat = mefStat.getCode();
        LOG.debug("Le code mefstat de la formation est:" + codeMefStat);
        // r�cup�ration du 7i�me et 8i�me caract�res du MEFSTAT11
        String domSpeCodeMefStat = codeMefStat.substring(6, 8);
        LOG.debug("Les 7i�me et 8i�me caract�re du code mefstat11 donne:" + domSpeCodeMefStat);

        Criteria criteria = HibernateUtil.currentSession().createCriteria(DomaineSpecialite.class);
        criteria.add(Restrictions.eq("code", domSpeCodeMefStat));
        // Si la liste ne redonne rien c'est qu'il n'existe pas de domaine de sp�cialit� associ�
        return (DomaineSpecialite) criteria.uniqueResult();

    }

    /**
     * Cr�er une m�thode qui permet d'apr�s une formation de r�cup�rer le domaine de sp�cialit�
     * ou v�rifier si il en existe une au niveau du formation manager :
     * Extraction des 2 caract�res 7 et 8 du mefstat11 et select d'apr�s le domaine de sp�cialit�.
     * 
     * @param formation
     *            la formation
     * @return Si on retourne un r�sultat c'est que le domaine de sp�cialit� pour la formation en param�tre existe,
     *         sinon on retourne null
     */
    public DomaineSpecialite recuperationDomaineSpecialiteRestreint2ndPro1Cap(Formation formation) {
        LOG.debug("V�rification si la formation " + formation.getLibelleLong()
                + " est une formation dans le domaine sp�cialit� restreint aux formations accueil 1CAP2, 1CAP3 ou 2NDPROP");

        // R�cup�ration des caract�res 7 et 8 du mefstat11 pour savoir si la formation fait partie d'un domaine
        // de sp�cialit�
        MefStat mefStat = formation.getMefStat();
        String codeMefStat = mefStat.getCode();
        String codeMefStat4 = mefStat.getCodeMefStat4();
        // On effectue cette v�rification que pour les formations restreintes suivants
        if (MefStat.CODE_MEFSTAT4_2NDPRO.equals(codeMefStat4) || MefStat.CODE_MEFSTAT4_1CAP2.equals(codeMefStat4)
                || MefStat.CODE_MEFSTAT4_1CAP3.equals(codeMefStat4)) {
            LOG.debug("Le code mefstat de la formation est:" + codeMefStat);
            // r�cup�ration du 7i�me et 8i�me caract�res du MEFSTAT11
            String domSpeCodeMefStat = codeMefStat.substring(6, 8);
            LOG.debug("Les 7i�me et 8i�me caract�re du code mefstat11 donne:" + domSpeCodeMefStat);

            Criteria criteria = HibernateUtil.currentSession().createCriteria(DomaineSpecialite.class);
            criteria.add(Restrictions.eq("code", domSpeCodeMefStat));
            // Si la liste ne redonne rien c'est qu'il n'existe pas de domaine de sp�cialit� associ�
            return (DomaineSpecialite) criteria.uniqueResult();
        } else {
            // sinon le domaine de sp�cialit� n'est pas � prendre en compte car on est en dehors des formations
            // accueils d�finies
            return null;
        }

    }

    /**
     * M�thode permettant de v�rifier s'il existe un domaine de sp�cialit�.
     * 
     * @param formation
     *            la formation
     * @return vrai si le domaine de sp�cialit� existe
     */
    public boolean existeDomaineSpecialite(Formation formation) {
        DomaineSpecialite ds = recuperationDomaineSpecialiteRestreint2ndPro1Cap(formation);
        return ds != null;
    }

    /**
     * M�thode permettant de chercher si le code existe dans la table des domaines de sp�cialit�.
     * 
     * @param domSpeCodeMefStat
     *            les deux caract�re r�cup�r� correspondant au code
     * @return vrai si le code existe
     */
    public boolean existeCodeFormation(String domSpeCodeMefStat) {
        return domaineSpecialiteDao.existeCodeFormation(domSpeCodeMefStat);
    }

    /**
     * M�thode permettant de charger le domaine de sp�cialit� associ� � la formation.
     * 
     * @param domSpeCodeMefStat
     *            le code sur 2 caract�re
     * @return le domaine de sp�cialit�
     */
    public DomaineSpecialite charger(String domSpeCodeMefStat) {
        return domaineSpecialiteDao.chargerNullable(domSpeCodeMefStat);
    }

    /**
     * M�thode permettant de charger le domaine de sp�cialit� associ� � la formation.
     * 
     * @param f
     *            la formation
     * @return le domaine de sp�cialit�
     */
    public DomaineSpecialite charger(Formation f) {
        // R�cup�ration des caract�res 7 et 8 du mefstat11 pour savoir si la formation fait partie d'un domaine
        // de sp�cialit�
        MefStat mefStat = f.getMefStat();
        String codeMefStat = mefStat.getCode();
        LOG.debug("Le code mefstat de la formation est:" + codeMefStat);
        // r�cup�ration du 7i�me et 8i�me caract�res du MEFSTAT11
        String domSpeCodeMefStat = codeMefStat.substring(6, 8);
        LOG.debug("Les 7i�me et 8i�me caract�re du code mefstat11 donne:" + domSpeCodeMefStat);
        // V�rifie que ces caract�res appartiennent au domaine de sp�cialit�
        return charger(domSpeCodeMefStat);

    }

}
