/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.utils.CollecteurScrollableResultSet;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.AvisParFormationOrigineDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.BonusFiliereDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.CorrespondanceFormationDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.FormationDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.FormationOrigineModifieeDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.GroupeOrigineFormationDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.ParametreFormationOrigineDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.ParametresFormationAccueilDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.RapprochEtabDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.VoeuDao;
import fr.edu.academie.affelnet.dao.interfaces.parametrage.InitialisationFormationDao;
import fr.edu.academie.affelnet.dao.interfaces.parametrage.InitialisationFormationEleveDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.AvisParFormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.BonusFiliere;
import fr.edu.academie.affelnet.domain.nomenclature.CorrespondanceFormation;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigineModifiee;
import fr.edu.academie.affelnet.domain.nomenclature.FormationPK;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigineFormation;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.RapprochEtab;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.parametrage.InitialisationFormation;
import fr.edu.academie.affelnet.domain.parametrage.InitialisationFormationEleve;
import fr.edu.academie.affelnet.dto.nomenclature.InformationsPubliquesFormationDto;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.EleveManager;
import fr.edu.academie.affelnet.service.ParametreManager;
import fr.edu.academie.affelnet.utils.Collecteur;
import fr.edu.academie.affelnet.utils.DateUtils;
import fr.edu.academie.affelnet.utils.UrlUtils;
import fr.edu.academie.affelnet.utils.exceptions.BusinessException;
import fr.edu.academie.affelnet.web.utils.FiltreUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Gestionnaire pour les nomenclatures Formation et MefStat. */
@Service
public class FormationManager extends AbstractManager {

    /** Le loggueur de la classe. */
    private final Log log = LogFactory.getLog(FormationManager.class);

    /**
     * Le dao utilis� pour les �l�ves.
     */
    private EleveDao eleveDao;

    /**
     * Le dao utilis� pour les formations origine modifi�es.
     */
    private FormationOrigineModifieeDao formationOrigineModifieeDao;

    /**
     * Le dao utilis� pour les param�tres par formation d'accueil.
     */
    private ParametresFormationAccueilDao parametresFormationAccueilDao;

    /**
     * Le dao utilis� pour les formations n�cessitant la saisie de l'avis du chef d'�tablissement d'origine.
     */
    private AvisParFormationOrigineDao avisParFormationOrigineDao;

    /**
     * Le dao utilis� pour les param�tres par formation d'origine.
     */
    private ParametreFormationOrigineDao parametreFormationOrigineDao;

    /**
     * Le dao utilis� pour les voeux.
     */
    private VoeuDao voeuDao;

    /**
     * Le dao utilis� pour les rapprochements entre �tablissements.
     */
    private RapprochEtabDao rapprochEtabDao;

    /**
     * Le dao utilis� pour les formations des groupes origines.
     */
    private GroupeOrigineFormationDao groupeOrigineFormationDao;

    /**
     * Le dao utilis� pour les bonus fili�re.
     */
    private BonusFiliereDao bonusFiliereDao;

    /**
     * Le dao utilis� pour les correspondances entre formations.
     */
    private CorrespondanceFormationDao correspondanceFormationDao;

    /**
     * Le dao utilis� pour les formations.
     */
    private FormationDao formationDao;

    /**
     * Le dao utilis� pour les initialisations des formations.
     */
    private InitialisationFormationDao initMefDao;

    /**
     * Le dao utilis� pour les initialisations des �l�ves par formation.
     */
    private InitialisationFormationEleveDao initMefEleveDao;

    /**
     * Le manager utilis� pour les voeux de fili�re.
     */
    private VoeuDeFiliereManager voeuDeFiliereManager;

    /**
     * Le manager utilis� pour les param�tres.
     */
    private ParametreManager parametreManager;

    /** Le gestionnaire de paliers. */
    private PalierManager palierManager;

    /**
     * Le manager utilis� pour les �l�ves.
     */
    private EleveManager eleveManager;

    /**
     * le gestionnaire des offres de formation.
     */
    private VoeuManager offreFormationManager;

    /**
     * @param eleveDao
     *            the eleveDao to set
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * @param formationOrigineModifieeDao
     *            the formationOrigineModifieeDao to set
     */
    @Autowired
    public void setFormationOrigineModifieeDao(FormationOrigineModifieeDao formationOrigineModifieeDao) {
        this.formationOrigineModifieeDao = formationOrigineModifieeDao;
    }

    /**
     * @param parametresFormationAccueilDao
     *            the parametresFormationAccueilDao to set
     */
    @Autowired
    public void setParametresFormationAccueilDao(ParametresFormationAccueilDao parametresFormationAccueilDao) {
        this.parametresFormationAccueilDao = parametresFormationAccueilDao;
    }

    /**
     * @param avisParFormationOrigineDao
     *            le DAO d'avis par formation d'origine
     */
    @Autowired
    public void setAvisParFormationOrigineDao(AvisParFormationOrigineDao avisParFormationOrigineDao) {
        this.avisParFormationOrigineDao = avisParFormationOrigineDao;
    }

    /**
     * @param parametreFormationOrigineDao
     *            the parametreFormationOrigineDao to set
     */
    @Autowired
    public void setParametreFormationOrigineDao(ParametreFormationOrigineDao parametreFormationOrigineDao) {
        this.parametreFormationOrigineDao = parametreFormationOrigineDao;
    }

    /**
     * @param voeuDao
     *            the voeuDao to set
     */
    @Autowired
    public void setVoeuDao(VoeuDao voeuDao) {
        this.voeuDao = voeuDao;
    }

    /**
     * @param rapprochEtabDao
     *            the rapprochEtabDao to set
     */
    @Autowired
    public void setRapprochEtabDao(RapprochEtabDao rapprochEtabDao) {
        this.rapprochEtabDao = rapprochEtabDao;
    }

    /**
     * @param groupeOrigineFormationDao
     *            the groupeOrigineFormationDao to set
     */
    @Autowired
    public void setGroupeOrigineFormationDao(GroupeOrigineFormationDao groupeOrigineFormationDao) {
        this.groupeOrigineFormationDao = groupeOrigineFormationDao;
    }

    /**
     * @param bonusFiliereDao
     *            the bonusFiliereDao to set
     */
    @Autowired
    public void setBonusFiliereDao(BonusFiliereDao bonusFiliereDao) {
        this.bonusFiliereDao = bonusFiliereDao;
    }

    /**
     * @param correspondanceFormationDao
     *            the correspondanceFormationDao to set
     */
    @Autowired
    public void setCorrespondanceFormationDao(CorrespondanceFormationDao correspondanceFormationDao) {
        this.correspondanceFormationDao = correspondanceFormationDao;
    }

    /**
     * @param formationDao
     *            the formationDao to set
     */
    @Autowired
    public void setFormationDao(FormationDao formationDao) {
        this.formationDao = formationDao;
    }

    /**
     * @param initMefDao
     *            the initMefDao to set
     */
    @Autowired
    public void setInitMefDao(InitialisationFormationDao initMefDao) {
        this.initMefDao = initMefDao;
    }

    /**
     * @param initMefEleveDao
     *            the initMefEleveDao to set
     */
    @Autowired
    public void setInitMefEleveDao(InitialisationFormationEleveDao initMefEleveDao) {
        this.initMefEleveDao = initMefEleveDao;
    }

    /**
     * @param voeuDeFiliereManager
     *            the voeuDeFiliereManager to set
     */
    @Autowired
    public void setVoeuDeFiliereManager(VoeuDeFiliereManager voeuDeFiliereManager) {
        this.voeuDeFiliereManager = voeuDeFiliereManager;
    }

    /**
     * @param parametreManager
     *            the parametreManager to set
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param eleveManager
     *            the eleveManager to set
     */
    @Autowired
    public void setEleveManager(EleveManager eleveManager) {
        this.eleveManager = eleveManager;
    }

    /**
     * @param palierManager
     *            le gestionnaire de paliers
     */
    @Autowired
    public void setPalierManager(PalierManager palierManager) {
        this.palierManager = palierManager;
    }

    /**
     * @param offreFormationManager
     *            the offreFormationManager to set
     */
    @Autowired
    public void setOffreFormationManager(VoeuManager offreFormationManager) {
        this.offreFormationManager = offreFormationManager;
    }

    /**
     * D�coupe une cha�ne ce codes MefStat4 s�par�s par des ';'.
     * 
     * @param chaineCodeMefstat4
     *            cha�ne source
     * @return liste des codes MefStat4
     */
    public List<String> recupCodeMefstat4(String chaineCodeMefstat4) {
        if (StringUtils.isEmpty(chaineCodeMefstat4)) {
            return new ArrayList<>(0);
        }
        return Arrays.asList(chaineCodeMefstat4.split(";"));
    }

    /**
     * R�cup�re la liste des formations correspondantes � une cha�ne pass�e en
     * param�tre. Les s�parateurs principaux entre mefs sont des '|' et les
     * s�parateurs internes des ';' ex : 'LIC1;COSP1|LIC2;COSP2|...|LICn;COSPn'
     * 
     * @param chaineFormations
     *            Cha�ne contenant les codes MEF des formations
     * @return liste des identifiants (FormationPK) des MEF correspondant � la
     *         cha�ne
     */
    public List<FormationPK> recupFormations(String chaineFormations) {

        if (StringUtils.isEmpty(chaineFormations)) {
            return new ArrayList<>(0);
        }

        // D�coupe la cha�ne pour chaque Mef
        String[] codesMEFComposes = chaineFormations.split("\\|");

        // Cr�e la liste des FormationPK
        List<FormationPK> listFormationPKs = new ArrayList<>();
        for (int debut = 0; debut < codesMEFComposes.length; debut++) {
            String[] elementsPkMef = codesMEFComposes[debut].split(";");
            if (elementsPkMef.length == 1) {
                FormationPK fPK = new FormationPK("", elementsPkMef[0]);
                listFormationPKs.add(fPK);
            } else {
                FormationPK fPK = new FormationPK(elementsPkMef[1], elementsPkMef[0]);
                listFormationPKs.add(fPK);
            }
        }
        return listFormationPKs;
    }

    /**
     * Compose une cha�ne � partir d'une liste de codes de formations.
     * 
     * @param listeFormation
     *            liste de codes formations de la forme MEF[_SPMEF]
     * @return cha�ne de caract�res MEF1;SPMEF1| ... |MEFn;SPMEFn
     */
    public String getChaineCodesFormation(List<String> listeFormation) {

        // On concat�ne les codes formations composites
        String jointure = StringUtils.join(listeFormation, "|");

        return StringUtils.replaceChars(jointure, "_", ";");
    }

    /**
     * Convertit une liste de InitialisationFormation en FormationPK.
     * 
     * @param listeInitialisationFormation
     *            liste de <code>InitialisationFormation</code>
     * @return la liste convertie en <code>FormationPK</code>
     */
    public List<FormationPK> transformeFormationPK(List<InitialisationFormation> listeInitialisationFormation) {
        List<FormationPK> listeFormationPK = new ArrayList<>();
        for (InitialisationFormation initialisationFormation : listeInitialisationFormation) {
            listeFormationPK.add(initialisationFormation.getId());
        }
        return listeFormationPK;
    }

    /**
     * Teste s'il y a des �l�ves issus de cette la formation origine avec des voeux.
     * 
     * @param pk
     *            Formation d'origine test�e
     * @return vrai s'il y a des �l�ves avec des voeux pour cette formation en
     *         origine
     */
    public boolean isFormationOriAvecVoeux(FormationPK pk) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("formation.id.mnemonique", pk.getMnemonique()));
        filtres.add(Filtre.equal("formation.id.codeSpecialite", pk.getCodeSpecialite()));
        filtres.add(eleveManager.filtreEleveAyantDesVoeux());

        return !eleveDao.lister(filtres).isEmpty();
    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les formations origine
     *         modifi�es
     */
    public boolean estUtiliseFormationOrigineModifiee(Formation formation) {
        if (formation == null) {
            return false;
        }

        return estUtiliseFormationOrigineModifiee(formation.getId());
    }

    /**
     * @param pk
     *            la cl� de la formation
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les formations origine
     *         modifi�es
     */
    public boolean estUtiliseFormationOrigineModifiee(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheFormationOrigineModifiee(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des formations origine modifi�es qui utilisent la
     *         formation pass�e en param�tre
     */
    public List<FormationOrigineModifiee> rechercheFormationOrigineModifiee(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }

        // liste des formations initiales
        Filtre filtreMnemoIni = Filtre.equal("formationInitiale.id.mnemonique", pk.getMnemonique());
        Filtre filtreCodeSpeIni = Filtre.equal("formationInitiale.id.codeSpecialite", pk.getCodeSpecialite());
        Filtre filtreMefIni = Filtre.and(filtreMnemoIni, filtreCodeSpeIni);

        // liste des formations finales
        Filtre filtreMnemoFin = Filtre.equal("formationFinale.id.mnemonique", pk.getMnemonique());
        Filtre filtreCodeSpeFin = Filtre.equal("formationFinale.id.codeSpecialite", pk.getCodeSpecialite());
        Filtre filtreMefFin = Filtre.and(filtreMnemoFin, filtreCodeSpeFin);

        // r�sultat
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.or(filtreMefIni, filtreMefFin));

        return formationOrigineModifieeDao.lister(filtres);
    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les d�pendances
     * @return vrai si la formation est utilis�e par les param�tres par
     *         formation d'accueil
     */
    public boolean estUtiliseParametresFormationAccueil(Formation formation) {
        if (formation == null) {
            return false;
        }

        return estUtiliseParametresFormationAccueil(formation.getId());
    }

    /**
     * @param pk
     *            la cl� de la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e par les param�tres par
     *         formation d'accueil.
     */
    public boolean estUtiliseParametresFormationAccueil(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheParametresFormationAccueil(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des param�tres par formation d'accueil qui utilisent la
     *         formation pass�e en param�tre
     */
    public List<ParametresFormationAccueil> rechercheParametresFormationAccueil(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("formationAccueil.id.mnemonique", pk.getMnemonique()));
        filtres.add(Filtre.equal("formationAccueil.id.codeSpecialite", pk.getCodeSpecialite()));

        return parametresFormationAccueilDao.lister(filtres);
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des formations origine n�cessitant la saisie de l'avis du chef d'�tablissement d'origine
     *         dans les param�tres par formation d'accueil qui utilisent la formation pass�e en param�tre
     */
    public List<AvisParFormationOrigine> rechercheAvisFormationOrigine(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("formation.id.mnemonique", pk.getMnemonique()));
        filtres.add(Filtre.equal("formation.id.codeSpecialite", pk.getCodeSpecialite()));

        return avisParFormationOrigineDao.lister(filtres);
    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les d�pendances
     * @return vrai si la formation est utilis�e par les param�tres par
     *         formation d'origine
     */
    public boolean estUtiliseParametresFormationOrigine(Formation formation) {
        List<Filtre> l = new ArrayList<>();
        l.add(Filtre.equal("formationOrigine.id.mnemonique", formation.getId().getMnemonique()));
        l.add(Filtre.equal("formationOrigine.id.codeSpecialite", formation.getId().getCodeSpecialite()));

        return parametreFormationOrigineDao.getNombreElements(l) > 0;
    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les voeux
     */
    public boolean estUtiliseVoeu(Formation formation) {
        if (formation == null) {
            return false;
        }

        return estUtiliseVoeu(formation.getId());
    }

    /**
     * @param pk
     *            la cl� de la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances.
     * @return vrai si la formation est utilis�e pour les voeux.
     */
    public boolean estUtiliseVoeu(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheVoeu(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des voeux qui utilisent la formation pass�e en param�tre
     */
    public List<Voeu> rechercheVoeu(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("formationAccueil.id.mnemonique", pk.getMnemonique()));
        filtres.add(Filtre.equal("formationAccueil.id.codeSpecialite", pk.getCodeSpecialite()));
        return voeuDao.lister(filtres);
    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les rapprochements �tab.
     */
    public boolean estUtiliseRapprochementEtab(Formation formation) {
        if (formation == null) {
            return false;
        }

        return estUtiliseRapprochementEtab(formation.getId());
    }

    /**
     * @param pk
     *            La cl� de la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les rapprochements �tab.
     */
    public boolean estUtiliseRapprochementEtab(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheRapprochementEtab(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des rapprochements entre �tablissements qui utilisent la
     *         formation pass�e en param�tre
     */
    public List<RapprochEtab> rechercheRapprochementEtab(FormationPK pk) {
       if (pk == null) {
            return Collections.emptyList();
        }

        // filtres utiles
        Filtre filtreMnemoO = Filtre.equal("mnemoniqueOrigine", pk.getMnemonique());
        Filtre filtreCodeSpeO = Filtre.equal("codeSpecialiteOrigine", pk.getCodeSpecialite());
        Filtre filtreCodeSpeONull = Filtre.isNull("codeSpecialiteOrigine");
        Filtre filtreMnemoA = Filtre.equal("mnemoniqueAccueil", pk.getMnemonique());
        Filtre filtreCodeSpeA = Filtre.equal("codeSpecialiteAccueil", pk.getCodeSpecialite());
        Filtre filtreCodeSpeANull = Filtre.isNull("codeSpecialiteAccueil");

        // liste des formations origine exactes
        Filtre filtreMefOExact = Filtre.and(filtreMnemoO, filtreCodeSpeO);

        // liste des mn�mo origine exacts + m�tier origine � $
        Filtre filtreMnemoOExact = Filtre.and(filtreMnemoO, filtreCodeSpeONull);

        // liste des formations accueil exactes
        Filtre filtreMefAExact = Filtre.and(filtreMnemoA, filtreCodeSpeA);

        // liste des mn�mo accueil exacts + m�tier accueil � $
        Filtre filtreMnemoAExact = Filtre.and(filtreMnemoA, filtreCodeSpeANull);

        // r�sultat
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.or(Arrays
                .asList(new Filtre[] { filtreMefOExact, filtreMnemoOExact, filtreMefAExact, filtreMnemoAExact })));
        return rapprochEtabDao.lister(filtres);


    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les groupes origine
     *         formation
     */
    public boolean estUtiliseGof(Formation formation) {
        if (formation == null) {
            return false;
        }
        return estUtiliseGof(formation.getId());
    }

    /**
     * @param pk
     *            La cl� de la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e pour les groupes origine
     *         formation
     */
    public boolean estUtiliseGof(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheGof(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des formations pour les groupes origine qui utilisent la
     *         formation pass�e en param�tre
     */
    public List<GroupeOrigineFormation> rechercheGof(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }

        // filtres utiles
        Filtre filtreMnemo = Filtre.equal("mnemonique", pk.getMnemonique());
        Filtre filtreCodeSpe = Filtre.equal("codeSpecialite", pk.getCodeSpecialite());

        // liste des formations exactes
        Filtre filtreMefExact = Filtre.and(filtreMnemo, filtreCodeSpe);

        // r�sultat
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreMefExact);
        if (groupeOrigineFormationDao.getNombreElements(filtres) == 0) {
            // je v�rifie que le cospemef de la formation dans le groupe est diff�rent de null($)
            filtres.clear();
            filtres.add(filtreMnemo);
            filtres.add(Filtre.isNull("codeSpecialite"));
        }
        return groupeOrigineFormationDao.lister(filtres);
    }

    /**
     * @param formation
     *            la <code>Formation</code> dont il faut v�rifier les
     *            d�pendances
     * @return vrai si la formation est utilis�e dans les bonus fili�re
     */
    public boolean estUtiliseBonusFiliere(Formation formation) {
        if (formation == null) {
            return false;
        }

        return estUtiliseBonusFiliere(formation.getId());
    }

    /**
     * @param pk
     *            la cl� de la formation.
     * @return vrai si la formation est utilis�e dans les bonus fili�re.
     */
    public boolean estUtiliseBonusFiliere(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheBonusFiliere(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des bonus fili�res qui utilisent la formation pass�e en
     *         param�tre
     */
    public List<BonusFiliere> rechercheBonusFiliere(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }

        // filtres utiles
        Filtre filtreMnemoO = Filtre.equal("mnemoniqueOrigine", pk.getMnemonique());
        Filtre filtreCodeSpeO = Filtre.equal("codeSpecialiteOrigine", pk.getCodeSpecialite());
        Filtre filtreCodeSpeONull = Filtre.isNull("codeSpecialiteOrigine");
        Filtre filtreMnemoA = Filtre.equal("mnemoniqueAccueil", pk.getMnemonique());
        Filtre filtreCodeSpeA = Filtre.equal("codeSpecialiteAccueil", pk.getCodeSpecialite());
        Filtre filtreCodeSpeANull = Filtre.isNull("codeSpecialiteAccueil");

        // liste des formations origine exactes
        Filtre filtreMefOExact = Filtre.and(filtreMnemoO, filtreCodeSpeO);

        // liste des mn�mo origine exacts + m�tier origine � $
        Filtre filtreMnemoOExact = Filtre.and(filtreMnemoO, filtreCodeSpeONull);

        // liste des formations accueil exactes
        Filtre filtreMefAExact = Filtre.and(filtreMnemoA, filtreCodeSpeA);

        // liste des mn�mo accueil exacts + m�tier accueil � $
        Filtre filtreMnemoAExact = Filtre.and(filtreMnemoA, filtreCodeSpeANull);

        // r�sultat
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.or(Arrays
                .asList(new Filtre[] { filtreMefOExact, filtreMnemoOExact, filtreMefAExact, filtreMnemoAExact })));
        return bonusFiliereDao.lister(filtres);
    }

    /**
     * @param formation
     *            la <code>Formation</code>
     * @return vrai si la formation poss�de un rattachement
     */
    public boolean estUtiliseCorrespondance(Formation formation) {
        if (formation == null) {
            return false;
        }
        return estUtiliseCorrespondance(formation.getId());
    }

    /**
     * @param pk
     *            la cl� de la formation.
     * @return vrai si la formation poss�de un rattachement
     */
    public boolean estUtiliseCorrespondance(FormationPK pk) {
        return CollectionUtils.isNotEmpty(rechercheCorrespondance(pk));
    }

    /**
     * @param pk
     *            la cl� de la formation
     * @return la liste des rattachements formations qui utilisent la formation
     *         pass�e en param�tre
     */
    public List<CorrespondanceFormation> rechercheCorrespondance(FormationPK pk) {
        if (pk == null) {
            return Collections.emptyList();
        }
        Filtre fMefIni = Filtre.and(Filtre.equal("id.mnemonique", pk.getMnemonique()),
                Filtre.equal("id.codeSpecialite", pk.getCodeSpecialite()));
        Filtre fMefFin = Filtre.and(Filtre.equal("mnemoniqueFinal", pk.getMnemonique()),
                Filtre.equal("codeSpecialiteFinal", pk.getCodeSpecialite()));

        // r�sultat
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.or(fMefIni, fMefFin));
        return correspondanceFormationDao.lister(filtres);
    }

    /**
     * @param formation
     *            la formation
     * @return vrai si un �l�ve suit la formation en tant qu'origine
     */
    public boolean estUtiliseeEnOrigine(Formation formation) {
        List<Filtre> l = new ArrayList<>();
        l.add(Filtre.equal("formation.id.mnemonique", formation.getId().getMnemonique()));
        l.add(Filtre.equal("formation.id.codeSpecialite", formation.getId().getCodeSpecialite()));

        return eleveDao.getNombreElements(l) > 0;
    }

    /**
     * @param formation
     *            la formation.
     * @return vrai si la formation est associ�e � un voeu de fili�re, faux sinon
     */
    public boolean estUtiliseVoeuxDeFiliere(Formation formation) {
        return CollectionUtils.isNotEmpty(voeuDeFiliereManager.rechercherParFormation(formation.getId()));
    }

    /**
     * Indique si une formation est utilis�e par une table dans Affelnet.
     * 
     * @param formation
     *            la formation
     * @return vrai si la formation est utilis�e par une table dans Affelnet
     */
    public boolean estUtilise(Formation formation) {
        return estUtiliseHorsCorresp(formation) || estUtiliseCorrespondance(formation);
    }

    /**
     * Indique si une formation est utilis�e par une table dans Affelnet (hors
     * table GN_CORMEF).
     * 
     * @param formation
     *            la formation
     * @return vrai si la formation est utilis�e par une table dans Affelnet
     *         hors rattachements
     */
    public boolean estUtiliseHorsCorresp(Formation formation) {

        return estUtiliseVoeu(formation) || estUtiliseGof(formation) || estUtiliseRapprochementEtab(formation)
                || estUtiliseBonusFiliere(formation) || estUtiliseFormationOrigineModifiee(formation)
                || estUtiliseParametresFormationAccueil(formation)
                || estUtiliseParametresFormationOrigine(formation) || estUtiliseeEnOrigine(formation)
                || estUtiliseVoeuxDeFiliere(formation);
    }

    /**
     * Indique si une formation est utilis�e par une table dans Affelnet (hors
     * origine des �l�ves).
     * 
     * @param formation
     *            la formation
     * @return vrai si la formation est utilis�e par une table dans Affelnet
     *         hors �l�ves
     */
    public boolean estUtiliseHorsEleves(Formation formation) {

        return estUtiliseVoeu(formation) || estUtiliseGof(formation) || estUtiliseRapprochementEtab(formation)
                || estUtiliseBonusFiliere(formation) || estUtiliseFormationOrigineModifiee(formation)
                || estUtiliseParametresFormationAccueil(formation)
                || estUtiliseParametresFormationOrigine(formation) || estUtiliseCorrespondance(formation)
                || estUtiliseVoeuxDeFiliere(formation);
    }

    /**
     * M�thode permettant de calculer le nombre de formations pour une mn�monique.
     * 
     * @param mnemonique
     *            une mn�monique
     * @return le nombre de formations associ�s au mn�monique
     */
    public long nombreDeFormations(String mnemonique) {
        return formationDao.nombreFormations(mnemonique);
    }

    /**
     * M�thode permettant de calculer le nombre de param�tres par formation d'origine associ�s � une formation.
     * 
     * @param formation
     *            une formation
     * @return le nombre de param�tres par formation d'origine associ�s � la formation
     */
    public long nombreDeParamefoGeneriquesAssociees(Formation formation) {
        return parametreFormationOrigineDao.nombreParamefoGenerique(formation.getId().getMnemonique());
    }

    /**
     * Charge la formation.
     * 
     * @param formationPK
     *            la cl� mnemonique/codeSpecialite d'une formation
     * @return La formation demand�e
     */
    public Formation charger(FormationPK formationPK) {
        return formationDao.charger(formationPK);
    }

    /**
     * Charge la formation si elle existe.
     * 
     * @param formationPK
     *            la cl� mnemonique/codeSpecialite d'une formation
     * @return La formation si elle existe, null sinon
     */
    public Formation chargerNullable(FormationPK formationPK) {
        if (formationDao.existe(formationPK)) {
            return formationDao.charger(formationPK);
        } else {
            return null;
        }
    }

    /**
     * Teste l'existence d'un mn�monique de formation.
     * 
     * @param mnemonique
     *            code mn�monique
     * @return vrai si le mn�monique existe
     */
    public boolean existeMnemo(String mnemonique) {
        return formationDao.existeMnemo(mnemonique);
    }

    /**
     * Donne la liste des codes MefStat4 pour une formation avec dollar �ventuel.
     * 
     * @param mnemonique
     *            Mn�monique de la formation
     * @param formation
     *            Formation concern�e
     * @return la liste des codes MefStat4 correspondants
     */
    public List<String> getListeMefStat4PourFormationOuMnemonique(String mnemonique, Formation formation) {

        List<String> listeCodesMefStat4 = null;

        if (formation != null) {
            // On connait la formation, un mefstat4 lui est associ� de fa�on unique

            MefStat mefStat = formation.getMefStat();
            if (mefStat != null) {
                listeCodesMefStat4 = new ArrayList<>(1);
                listeCodesMefStat4.add(mefStat.getCodeMefStat4());
            } else {
                throw new BusinessException(
                        "La formation " + formation.getId().toPrettyString() + " n'a pas de MefStat associ�.");
            }
        } else {
            listeCodesMefStat4 = formationDao.getListeMefStat4PourMnemonique(mnemonique);
        }

        return listeCodesMefStat4;
    }

    /**
     * V�rifie l'existence d'une formation.
     * 
     * @param formationPK
     *            La cl� primaire de la formation.
     * @return
     *         <ul>
     *         <li>true si la formation existe</li>
     *         <li>false sinon</li>
     *         </ul>
     */
    public boolean existe(FormationPK formationPK) {
        return formationDao.existe(formationPK);
    }

    /**
     * Supprime les formations des tables GN_MEF et CN_INITNOMMEF.
     * 
     * @param listeFormationToDel
     *            la liste des <code>FormationPK</code> � supprimer
     */
    public void supprimerFormations(List<FormationPK> listeFormationToDel) {
        // Initialisation des variables de travail
        Formation formation = null;
        InitialisationFormation initFormation = null;

        for (FormationPK pk : listeFormationToDel) {

            // Suppression Formation (GN_MEF) si elle existe
            if (formationDao.existe(pk)) {
                formation = charger(pk);
                formationDao.supprimer(formation);
            }

            // Suppression InitialisationFormation (CN_INITNOMMEF) si elle existe
            if (initMefDao.existe(pk)) {
                initFormation = initMefDao.charger(pk);
                initMefDao.supprimer(initFormation);
            }

            log.debug("Suppression de : " + pk.toPrettyString());
        }
    }

    /**
     * Permet la r�cup�ration des formations depuis les donn�ees
     * d'initialisation.
     */
    public void initialisationFormations() {
        formationDao.initialisationFormations();
    }

    /**
     * Permet la r�cup�ration des formations n�cessaires pour les voeux.
     */
    public void recuperationFormationsNecessaires() {
        formationDao.recuperationFormationsNecessaires();
    }

    /**
     * Renvoi la liste de tous les InitialisationFormation.
     * 
     * @return La liste de tous les InitialisationFormation.
     */
    public List<InitialisationFormation> recupereInitialisationFormation() {
        return initMefDao.lister();
    }

    /**
     * La liste des formations par le dao d'InitialisationFormation.
     * 
     * @return La liste des formations.
     */
    public List<Formation> listerInitialisationFormation() {
        return initMefDao.listerInitialisationFormation();
    }

    /**
     * Insert les formations dans la table CN_INITNOMMEF.
     * 
     * @param formations
     *            la liste des formations
     */
    public void insertMultiple(List<FormationPK> formations) {
        initMefDao.insertionMultiple(formations);
    }

    /**
     * Renvoi l'initialisationFormation correspondant.
     * 
     * @param formationPK
     *            La cl� primaire de la formation.
     * @return L'initialisationFormation correspondant.
     */
    public InitialisationFormation chargerInitialisationFormation(FormationPK formationPK) {
        return initMefDao.charger(formationPK);
    }

    /**
     * Renvoi la liste des formations d'initialisation de la table CN_INITELVMEF.
     * 
     * @return La liste des formations d'initialisation de la table CN_INITELVMEF.
     */
    public List<InitialisationFormationEleve> listerFormationEleve() {
        return initMefEleveDao.lister();
    }

    /**
     * Renvoi la liste des formations pr�sentes dans la table d'initialisation CN_INITELVMEF.
     * 
     * @return La liste des formations pr�sentes dans la table d'initialisation CN_INITELVMEF.
     */
    public List<Formation> listerInitialisationFormationEleve() {
        return initMefEleveDao.listerInitialisationFormationEleve();
    }

    /**
     * @return La listes des formations affections non rattach�es.
     */
    public List<Formation> listerFormationsNonLiees() {
        return formationDao.listerFormationsNonLiees();
    }

    /**
     * M�thode de chargement des formations ouvertes correspondant � un code MefStat 4.
     * 
     * @param codeMefStat4
     *            le code MeStat 4 pour la s�lection
     * @return la liste des formations correspondant au MefStat 4
     */
    public List<Formation> listerFormationsOuvertesPourMefStat4(String codeMefStat4) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("mefStat.codeMefStat4", codeMefStat4));
        FiltreUtils.ajouterFiltreOrigine(filtres);

        return formationDao.lister(filtres);
    }

    /**
     * Cr�� la formation.
     * 
     * @param formation
     *            la formation � ajouter � la base de donn�es
     * @return La formation cr��e
     */
    public Formation creer(Formation formation) {

        if (StringUtils.isBlank(formation.getFlagLibelleOnisep())) {
            formation.setFlagLibelleOnisep(Flag.OUI);
        }

        if (StringUtils.isBlank(formation.getFlagUrlOnisep())) {
            formation.setFlagUrlOnisep(Flag.OUI);
        }

        return formationDao.creer(formation);
    }

    /**
     * Met � jour une formation.
     * 
     * @param formation
     *            la formation � modifier
     * @param oldKey
     *            la cl� de la formation � modifier
     * @return La formation modifi�e
     */
    public Formation miseAJour(Formation formation, FormationPK oldKey) {
        formation.setUrlDescription(UrlUtils.completeUrlOuNull(formation.getUrlDescription()));
        formation.setDateMAJ(new Date());

        return formationDao.maj(formation, oldKey);
    }

    /**
     * Supprime une formation.
     * 
     * @param formation
     *            la formation � supprimer
     */
    public void supprimer(Formation formation) {
        formationDao.supprimer(formation);
    }

    /**
     * @param codeMefstat4
     *            Le code mefstat4 pour lequel on souhaite r�cup�rer les mn�moniques distincts.
     * @return La liste des diff�rents mn�monique pour un mefstat4 donn�.
     */
    @SuppressWarnings("unchecked")
    public List<String> listerMenomiqueParMefstat4(String codeMefstat4) {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(Formation.class);
        criteria.createAlias("mefStat", "mefStat");
        criteria.setProjection(Projections.distinct(Projections.property("id.mnemonique")));
        criteria.add(Restrictions.eq("mefStat.codeMefStat4", codeMefstat4));

        return criteria.list();
    }

    /**
     * M�thode de r�cup�ration des mn�moniques des formations pour un mefstat4 donn�, pour la s�lection des �l�ves.
     * 
     * @param codeMefstat4
     *            Le code mefstat4 pour lequel on souhaite r�cup�rer les mn�moniques distincts.
     * @return La liste des diff�rents mn�monique pour un mefstat4 donn�.
     */
    @SuppressWarnings("unchecked")
    public List<String> listerMenomiqueParMefstat4PourSelection(String codeMefstat4) {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(Formation.class);
        criteria.createAlias("mefStat", "mefStat");
        criteria.setProjection(Projections.distinct(Projections.property("id.mnemonique")));
        criteria.add(Restrictions.eq("mefStat.codeMefStat4", codeMefstat4));

        // On ne r�cup�re par les formations dont le code est AFFECTATION
        criteria.add(Restrictions.ne("codeInterne", Formation.CODE_INTERNE_AFFECTATION));

        int anneeEnCours = parametreManager.getAnnee20xx();
        // 01/01 de l'ann�e consid�r�e
        Date dateOuverture = DateUtils.get01Janvier(anneeEnCours);
        // 01/01 de l'ann�e consid�r�e - 1 (on retire l'ann�e de battement)
        Date dateFermeture = DateUtils.get01Janvier(anneeEnCours - 1);

        criteria.add(Restrictions.lt("dateOuverture", dateOuverture));
        criteria.add(Restrictions.ge("dateFermeture", dateFermeture));

        return criteria.list();
    }

    /**
     * M�thode de r�cup�ration de la liste des Mnemonique en origine pour les �l�ves ayant d�j� des voeux.
     * 
     * @param mefstat4
     *            le code MefStat4 � rechercher
     * @return Liste des mnemoniques utilis�s pour le code MefStat4 sp�cifi�
     */
    public List<String> listerMnemoniquesUtilises(String mefstat4) {
        return formationDao.listerMnemoniquesUtilises(mefstat4);
    }

    /**
     * M�thode permettant de lister les formations qui sont ferm�es et utilis�es en tant qu'accueil par
     * l'application.
     * 
     * @return la liste des formations ferm�es et utilis�es en tant qu'accueil par l'application
     */
    public List<Formation> formationsFermeesEtUtiliseesEnAccueil() {
        log.debug("V�rification des formations ferm�es en tant qu'accueil");

        // liste des filtres
        List<Filtre> listFiltres = new ArrayList<>();

        // Construction des filtres : formations utilis�es
        ArrayList<Filtre> filtresMef = new ArrayList<>();
        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select v.code from Voeu v where o.id=v.formationAccueil.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select b.id from BonusFiliere b where o.id=b.formationAccueil.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select r.id from RapprochEtab r where o.id=r.formationAccueil.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select f.id from FormationOrigineModifiee f where o.id=f.formationFinale.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select p.id from ParametresFormationAccueil p where o.id=p.formationAccueil.id)");

        listFiltres.add(Filtre.or(filtresMef));

        // Construction des filtres : ajout notion fermeture
        FiltreUtils.ajouterFiltreNotAccueil(listFiltres);

        // Liste formations ferm�es utilis�es
        return formationDao.lister(listFiltres);
    }

    /**
     * @return la liste des formations qui sont ferm�es et utilis�es en tant
     *         qu'origine par l'application
     */
    public List<Formation> formationsFermeesEtUtiliseesEnOrigine() {
        log.debug("V�rification des formations ferm�es en tant qu'origine");

        // liste des filtres
        List<Filtre> listFiltres = new ArrayList<>();

        // Construction des filtres : formations utilis�es
        ArrayList<Filtre> filtresMef = new ArrayList<>();

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select b.id from BonusFiliere b where o.id=b.formationOrigine.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select g.formation.id from GroupeOrigineFormation g where o.id=g.formation.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select a.formation.id from AvisParFormationOrigine a where o.id=a.formation.id)");

        FiltreUtils.ajouterCritereHQL(filtresMef,
                "exists(select r.id from RapprochEtab r where o.id=r.formationOrigine.id)");
        listFiltres.add(Filtre.or(filtresMef));

        // Construction des filtres : ajout notion fermeture
        FiltreUtils.ajouterFiltreNotOrigine(listFiltres);

        // Liste formations ferm�es utilis�es
        return formationDao.lister(listFiltres);
    }

    /**
     * @return la liste des formations de type AFFECTATION qui datent de plus d'un an
     */
    public List<Formation> formationsAffectationDatantDePlusDUnAn() {
        log.debug("V�rification des formations de type AFFECTATION datant de plus d'un an");

        int anneeEnCours = parametreManager.getAnnee20xx();
        Date anneePrecedente = DateUtils.get01Janvier(anneeEnCours);

        // Construction des filtres : formations utilis�es
        ArrayList<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("codeInterne", Formation.CODE_INTERNE_AFFECTATION));
        filtres.add(Filtre.inferieur("dateCreation", anneePrecedente));
        filtres.add(Filtre.not(Filtre.like("id.mnemonique", "HAC%")));

        // Liste formations ferm�es utilis�es
        return formationDao.lister(filtres);
    }

    /**
     * @return la liste des formations utilis�es pour la saisie de l'avis du chef d'�tablissement d'origine (cf.
     *         nomenclature "Param�tres par formation d'accueil") inexistantes
     */
    public List<AvisParFormationOrigine> formationsInexistantesPourAvisParFormationOrigine() {

        // liste des filtres
        List<Filtre> filtres = new ArrayList<>();

        // Construction du filtre : formations origine utilis�es pour la saisie de l'avis du CE n'ayant pas de
        // correspondance dans la table des formations et n'utilisant pas la convention "$"
        FiltreUtils.ajouterCritereHQL(filtres,
                "not exists (select f.id from Formation f where o.formation.id=f.id)"
                        + " and o.codeSpecialite is not null");

        // Liste des formations inexistantes dans la nomenclature des formations
        return avisParFormationOrigineDao.lister(filtres);
    }

    /**
     * Constitue une table associative donnant le palier d'origine par code MefStat4.
     * 
     * @return table de code palier d'origine par MefStat4.
     */
    public Map<String, Short> getCodePalierParMefStat4() {

        // D�l�gue au manager sp�cialis�
        return palierManager.getCodePalierParMefStat4();
    }

    /**
     * Retourne la table des correspondances entre formations.
     * La table est compl�te, les rattachements �tant r�put�s ponctuels et � faible volum�trie.
     * 
     * @return Table compl�te des correspondances entre formations.
     */
    public Map<FormationPK, CorrespondanceFormation> chargeToutesCorrespondancesFormations() {
        Map<FormationPK, CorrespondanceFormation> correspondancesFormation = new HashMap<>();
        for (CorrespondanceFormation correspondanceFormation : correspondanceFormationDao.lister()) {
            correspondancesFormation.put(correspondanceFormation.getId(), correspondanceFormation);
        }
        return correspondancesFormation;
    }

    /**
     * M�thode permettant de lister les formations affectations qui sont hors domaine.
     * 
     * @return la liste des formations affectation de seconde Pro et premi�re CAP 2 et 3ans hors domaine
     */
    public List<Formation> formationsAffectation2ndPro1CapHorsDomaine() {
        log.debug("V�rification des formations affectation de type 2ndPro et 1CAP hors domaine");

        // liste des filtres
        List<Filtre> listFiltres = new ArrayList<>();
        listFiltres.add(Filtre.equal("codeInterne", Formation.CODE_INTERNE_AFFECTATION));
        // Liste des mefstat4 correspondant � 2ndPro et 1CAP
        String[] list2ndPro1Cap = { MefStat.CODE_MEFSTAT4_2NDPRO, MefStat.CODE_MEFSTAT4_1CAP2,
                MefStat.CODE_MEFSTAT4_1CAP3 };
        listFiltres.add(Filtre.in("codeMefStat4", list2ndPro1Cap));
        // Ajout du filtre pour r�cup�rer uniquement la liste des formations hors domaine
        FiltreUtils.ajouterCritereHQL(listFiltres,
                "substr(mefStat,'7','2') not in (select ds.code from DomaineSpecialite ds)");

        // Liste formations
        return formationDao.lister(listFiltres);
    }

    /**
     * M�thode permettant de lister les formations qui sont dans la liste des domaines de sp�cialit�.
     * On r�cup�re les formations dont sur le MEFStat11 on a les caract�res 7 et 8 qui d�termine un domaine
     * professionnel.
     * Cette m�thode va �tre utilis� pour que les coefficients nationaux par domaine professionnel (fixe)
     * auront la primeur sur les coefficients acad�miques.
     * 
     * @return la liste des formations qui correspondent au domaine sp�cialit�
     */
    public List<Formation> formationsDomaineSpecialite() {
        log.debug("V�rification des formations dans le domaine sp�cialit� ");

        // liste des filtres
        List<Filtre> listFiltres = new ArrayList<>();
        // Ajout du filtre pour r�cup�rer uniquement la liste des formations hors domaine
        FiltreUtils.ajouterCritereHQL(listFiltres,
                "substr(mefStat,'7','2') in (select ds.code from DomaineSpecialite ds)");

        // Liste formations
        return formationDao.lister(listFiltres);
    }

    /**
     * M�thode permettant de savoir si une formation d�termine un domaine professionnel.
     * Cette m�thode va �tre utilis� pour que les coefficients nationaux par domaine professionnel (fixe)
     * auront la primeur sur les coefficients acad�miques.
     * 
     * @param formation
     *            la formation associ�
     * @return vrai la formation associ� fait partie du domaine de sp�cialit�, sinon faux
     */
    public boolean formationsDomaineSpecialite(Formation formation) {
        log.debug("V�rification si la formation " + formation.getLibelleLong()
                + " est une formation dans le domaine sp�cialit� ");

        // liste des filtres
        List<Filtre> listFiltres = new ArrayList<>();
        listFiltres.add(Filtre.equal("id", formation.getId()));
        // Ajout du filtre pour r�cup�rer uniquement la liste des formations hors domaine
        FiltreUtils.ajouterCritereHQL(listFiltres,
                "substr(mefStat,'7','2') in (select ds.code from DomaineSpecialite ds)");

        // Liste formations
        return !formationDao.lister(listFiltres).isEmpty();
    }

    /**
     * Cr�� un filtre pour ne prendre en compte que les formations li�es � au moins une offre de formation
     * visible par le grand public.
     * 
     * @return un filtre pour les formations pour ne r�cup�rer que ceux li�s � une offre de formation visible
     *         par le grand public
     */
    public Filtre filtreFormationAppartientOffreFormationVisiblePublic() {
        List<Filtre> filtresOffre = new ArrayList<>();
        filtresOffre.add(Filtre.eqPropertiesPere("formationAccueil.id", "formation.id"));
        filtresOffre.add(Filtre.equal("visiblePortail", Flag.OUI));
        return Filtre.exists(Voeu.class, Filtre.and(filtresOffre));
    }

    /**
     * Cr�� un filtre pour ne prendre en compte que les formations qui peuvent �tre envoy�es vers le portail.
     * Sont concern�es les formations :
     * - dont le libell� public et l'URL de la fiche sont renseign�s
     * - utilis�es une offre de formation visible et dont le libell� public de l'�tablissement d'accueil est
     * renseign�
     * 
     * @return un filtre pour les formations qui sont envoy�es au portail
     */
    public Filtre filtreFormationAnvoyerPortail() {
        List<Filtre> filtresOffre = new ArrayList<>();

        filtresOffre.add(offreFormationManager.filtreOffresAEnvoyerPortail());
        filtresOffre.add(Filtre.eqPropertiesPere("formationAccueil.id", "formation.id"));

        return Filtre.exists(Voeu.class, Filtre.and(filtresOffre));
    }

    /**
     * D�termine si une formation est utilis�e dans au moins une offre de formation visible sur le portail.
     * 
     * @param formationPk
     *            l'id de la formation
     * @return true si la formation est utilis�e dans une offre de formation visible au public
     */
    public boolean estUtiliseOffreFormationVisiblePublic(FormationPK formationPk) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(filtreFormationAppartientOffreFormationVisiblePublic());
        filtres.add(Filtre.equal("id", formationPk));

        return formationDao.getNombreElements(filtres) > 0;
    }

    /**
     * Fournit la liste des formations utilis�es dans les offres de formations visibles par le grand public.
     * 
     * @return les formations visibles au grand public
     */
    public List<Formation> formationsUtiliseesOffreFormationVisiblePublic() {
        return formationDao.lister(filtreFormationAppartientOffreFormationVisiblePublic());
    }

    /**
     * Fournit la liste des formations qui peuvent �tre envoy�e au portail.
     * 
     * @return les formations visibles qui peuvent �tre envoy�es au portail
     */
    public List<Formation> formationsAEnvoyerPortail() {
        return formationDao.lister(filtreFormationAnvoyerPortail());
    }

    /**
     * Fournit la liste des formations li�es � une offre de formation visible mais sans libell� public.
     * 
     * @return les formations visibles qui ne peuvent pas �tre envoy�es au portail
     */
    public List<Formation> formationsUtiliseesOffreFormationVisibleSansLibellePublic() {
        List<Filtre> filtres = new ArrayList<>();

        // Les formations li�es � une offre visible
        filtres.add(filtreFormationAppartientOffreFormationVisiblePublic());

        // Le libelle public ou l'URL de la fiche non renseign�
        filtres.add(filtreFormationInformationsPubliquesManquantes());

        return formationDao.lister(filtres);
    }

    /**
     * Fournit un filtre les formations dont les informations publiques sont invalides.
     * 
     * @return le filtre sur les formations dont les informations publiques sont invalides
     */
    public Filtre filtreFormationInformationsPubliquesManquantes() {
        return filtreOffreOuFormationInformationsManquantes(false);
    }

    /**
     * Fournit un filtre les offres dont les informations publiques des formations sont invalides.
     * 
     * @return le filtre sur les offres dont les informations publiques des formations sont invalides
     */
    public Filtre filtreOffreFormationInformationsPubliquesManquantes() {
        return filtreOffreOuFormationInformationsManquantes(true);
    }

    /**
     * Fournit un filtre sur les offres ou les formations. Ne sont conserv�s que les formations dont les
     * informations publiques sont invalides.
     * 
     * @param estOffre
     *            si le filtre porte sur une offre
     * @return le filtre sur l'offre ou la formation
     */
    private Filtre filtreOffreOuFormationInformationsManquantes(boolean estOffre) {
        List<Filtre> filtres = new ArrayList<>();
        String prefixe = StringUtils.EMPTY;

        if (estOffre) {
            prefixe = "formationAccueil.";
        }

        filtres.add(Filtre.isNullOrVoid(prefixe + "libellePublic"));
        filtres.add(Filtre.isNullOrVoid(prefixe + "urlDescription"));

        return Filtre.or(filtres);
    }

    /**
     * Cr�� un collecteur pour le parcours de formations au sein d'un batch.
     * 
     * @param filtre
     *            Le filtre sur les formations
     * @return un collecteur de formations
     */
    public Collecteur<Formation> collecteur(Filtre filtre) {
        return new CollecteurScrollableResultSet<>(formationDao.scroll(filtre));
    }

    /**
     * Fournit une repr�sentation des donn�es publiques de la formation.
     * 
     * @param mnemonique
     *            le mn�monique de la formation
     * @param codeSpecialite
     *            le code sp�cialit�
     * @return un DTO contenant les donn�es issus de la base de donn�es
     */
    public InformationsPubliquesFormationDto informationsPubliques(String mnemonique, String codeSpecialite) {
        Formation formation = chargerNullable(new FormationPK(codeSpecialite, mnemonique));

        if (formation == null) {
            return null;
        }

        return new InformationsPubliquesFormationDto(formation);
    }

    /**
     * Fournit l'ensemble des formations d'origine li�es � un filtre.
     * 
     * @param codePalier
     *            le code du palier de ces formations
     * @return les formations d'origine li�es � un palier
     */
    public Collection<Formation> listerFormationPalier(Short codePalier) {
        return formationDao.lister(filtreFormationPalier(codePalier));
    }

    /**
     * Filtre pour les formations d'origine li�es � un palier.
     * 
     * @param codePalier
     *            le code du palier de ces formations
     * @return le filtre pour les formations d'origine li�es � un palier
     */
    public Filtre filtreFormationPalier(Short codePalier) {
        return Filtre.equal("mefStat.palier.code", codePalier);
    }
    
    /**
     * Fournit l'ensemble des formations.
     * 
     * @return les formations
     */
    public Collecteur<Formation> listeFormationScroll() {
    	return formationDao.collecteur(null, null, null, null,null, false);
    }
    
    /**
     * M�thode de mise � jour d'une liste de formation.
     * 
     * @param listeFormation
     *            Liste des formation � mettre � jour.
     */
    public void maj(List<Formation> listeFormation) {
    	formationDao.maj(listeFormation);
    }


    /**
     * Chargement de la formation en fonction du comef national donn�.
     * 
     * @param comef
     *            le comef national
     * @return la formation associ� au comef national
     */
    public Formation chargerParComef(String comef) {
        Filtre filtreComef = Filtre.equal("codeInterne", comef + "0");
        List<Formation> formations = this.formationDao.lister(filtreComef, false);
        return formations.stream().findFirst().orElse(null);

    }
    
}
