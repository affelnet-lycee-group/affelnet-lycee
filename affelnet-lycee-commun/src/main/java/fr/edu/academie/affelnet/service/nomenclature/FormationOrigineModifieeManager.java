/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.FormationOrigineModifieeComparator;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.FormationOrigineModifieeDao;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigineModifiee;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.service.AbstractManager;

/**
 * Gestionnaire pour les nomenclatures Formations origine modifi�es.
 */
@Service
public class FormationOrigineModifieeManager extends AbstractManager {
    /**
     * Le gestionnaire des mati�res.
     */
    private MatiereManager matiereManager;

    /**
     * Le dao utilis� pour les formation origine modifi�es.
     */
    private FormationOrigineModifieeDao formationOrigineModifieeDao;

    /**
     * @param matiereManager
     *            the matiereManager to set
     */
    @Autowired
    public void setMatiereManager(MatiereManager matiereManager) {
        this.matiereManager = matiereManager;
    }

    /**
     * @param formationOrigineModifieeDao
     *            the formationOrigineModifieeDao to set
     */
    @Autowired
    public void setFormationOrigineModifieeDao(FormationOrigineModifieeDao formationOrigineModifieeDao) {
        this.formationOrigineModifieeDao = formationOrigineModifieeDao;
    }

    /**
     * M�thode permettant de r�cup�rer toutes les formations origine modifi�es
     * utilisant des formations sp�cifiques.
     * 
     * @param formations
     *            la liste des formations utilis�es par les formations origine
     *            modifi�es
     * @return les formations origine modifi�es utilisant les formations en
     *         param�tre.
     */
    public List<FormationOrigineModifiee> formationOrigineModifieeAvecFormation(List<Formation> formations) {
        if (formations.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<Filtre> filtresFormations = new ArrayList<>();

            for (Formation formation : formations) {
                filtresFormations.add(Filtre.and(
                        Filtre.equal("formationFinale.id.mnemonique", formation.getId().getMnemonique()),
                        Filtre.equal("formationFinale.id.codeSpecialite", formation.getId().getCodeSpecialite())));
            }

            return formationOrigineModifieeDao.lister(Filtre.or(filtresFormations));
        }
    }

    /**
     * Fournis la formation origine modifi�e la plus proche des �l�ments fournis.
     * 
     * @param formation
     *            la <code>Formation</code> d'origine
     * @param lOpt
     *            la <code>List</code> des options initiales
     * @return le <code>FormationOrigineModifiee</code> correspondant ou
     *         <code>null</code> sinon
     */
    public FormationOrigineModifiee find(Formation formation, List<Matiere> lOpt) {
        return find(formation, lOpt, null);
    }

    /**
     * Fournis la formation origine modifi�e la plus proche des �l�ments fournis et alimente le cache si
     * n�cessaire.
     * 
     * @param formation
     *            la formation d'origine
     * @param lOpt
     *            la liste des options d'origine
     * @param cacheMap
     *            le cache de recherche
     * @return la formation d'origine modifi�e qui s'applique ou null si aucune ne correspond
     */
    @SuppressWarnings("unchecked")
    public FormationOrigineModifiee find(Formation formation, List<Matiere> lOpt, Map<String, Object> cacheMap) {
        // r�cup�ration de la liste des formations d'origine modifi�es
        final String cacheName = "listeFormationOrigineModifiee";
        List<FormationOrigineModifiee> lFom;

        if (cacheMap == null) {
            // si pas de cache, on r�duit les possibilit�s en mettant les
            // crit�res obligatoires

            lFom = formationOrigineModifieeDao.listerFormationOrigineModifieePourFormationInitiale(formation);
        } else if (!cacheMap.containsKey(cacheName)) {
            lFom = formationOrigineModifieeDao.listerFormationOrigineModifieeAvecJointuresFetch();
            cacheMap.put(cacheName, new ArrayList<FormationOrigineModifiee>(lFom));
        } else {

            // new pour ne pas alterer la liste en cache
            lFom = new ArrayList<>((List<FormationOrigineModifiee>) cacheMap.get(cacheName));
        }

        List<Predicate<FormationOrigineModifiee>> predicats = listePredicat(formation, lOpt, cacheMap);
        for (Predicate<FormationOrigineModifiee> predicat : predicats) {
            lFom.removeIf(predicat);
        }

        // pas d'�l�ment corrects
        if (lFom.isEmpty()) {
            return null;
        }

        // il reste les �l�ments qui sont corrects, on les trie et on restitue le premier
        TreeSet<FormationOrigineModifiee> listeTriee = new TreeSet<>(new FormationOrigineModifieeComparator());
        listeTriee.addAll(lFom);
        return listeTriee.first();
    }

    /**
     * Fournis la liste des pr�dicats pour �liminer toutes les formations origine modifi�es qui ne correspondent
     * pas aux param�tres fournis.
     * 
     * @param formation
     *            la formation initiale
     * @param options
     *            les options
     * @param cacheMap
     *            le cache
     * @return la liste des pr�dicats pour filter les formations origine modifi�es
     */
    private List<Predicate<FormationOrigineModifiee>> listePredicat(Formation formation, List<Matiere> options,
            Map<String, Object> cacheMap) {
        List<Predicate<FormationOrigineModifiee>> predicats = new ArrayList<>();

        predicats.add(fom -> !fom.getFormationInitiale().equals(formation));

        predicats.add(fom -> fom.getOptionFinale1() != null
                && !matiereManager.containsMatOrLV(fom.getOptionFinale1(), options, cacheMap));
        predicats.add(fom -> fom.getOptionFinale2() != null
                && !matiereManager.containsMatOrLV(fom.getOptionFinale2(), options, cacheMap));

        return predicats;
    }
}
