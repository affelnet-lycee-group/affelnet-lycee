/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.GroupeOrigineDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.GroupeOrigineFormation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.Nature;
import fr.edu.academie.affelnet.domain.nomenclature.TypeEtablissement;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.utils.TruncationList;

/**
 * Manager pour les groupes origine.
 */
@Service
public class GroupeOrigineManager extends AbstractManager {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(GroupeOrigineManager.class);

    /**
     * Le dao utilis� pour les groupes origine.
     */
    private GroupeOrigineDao groupeOrigineDao;

    /** Acc�s aux donn�es des �l�ves. */
    private EleveDao eleveDao;

    /**
     * Le manager des �l�ments ouvrables.
     */
    private OuvrableManager ouvrableManager;

    /** Le gestionnaires d'pperation programmees d'affectation. */
    private OperationProgrammeeAffectationManager opaManager;

    /**
     * @param groupeOrigineDao
     *            le DAO des groupes origine
     */
    @Autowired
    public void setGroupeDao(GroupeOrigineDao groupeOrigineDao) {
        this.groupeOrigineDao = groupeOrigineDao;
    }

    /**
     * @param eleveDao
     *            le DAO des �l�ves
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * @param ouvrableManager
     *            le gestionnaire d'ouverture/fermeture d'entit�s
     */
    @Autowired
    public void setOuvrableManager(OuvrableManager ouvrableManager) {
        this.ouvrableManager = ouvrableManager;
    }

    /**
     * @param opaManager
     *            Le gestionnaires d'pperation programmees d'affectation.
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * Charge un groupe origine depuis son code.
     * 
     * @param code
     *            le code du groupe
     * @return le groupe origine
     */
    public GroupeOrigine charger(String code) {
        return groupeOrigineDao.charger(code);
    }

    /**
     * Cr�e un groupe origine.
     * 
     * @param groupeOrigine
     *            le groupe origine a cr�er
     * @return le groupe origine cr��
     */
    public GroupeOrigine creer(GroupeOrigine groupeOrigine) {

        LOG.debug("Cr�ation d'un groupe origine " + groupeOrigine.toStringPretty());

        groupeOrigine.setDateCreation(new Date());
        valider(groupeOrigine);

        GroupeOrigine groupeOrigineCree = groupeOrigineDao.creer(groupeOrigine);
        opaManager.obligerRelanceOpaTour();
        return groupeOrigineCree;
    }

    /**
     * Met � jour un groupe origine.
     * 
     * @param ancienCode
     *            l'ancien code de groupe
     * @param groupeOrigine
     *            le groupe origine � mettre � jour
     * @return le groupe orgine mis � jour
     */
    public GroupeOrigine mettreAJour(String ancienCode, GroupeOrigine groupeOrigine) {

        LOG.debug("Mise � jour d'un groupe origine - avant : " + groupeOrigine.toStringPretty());

        groupeOrigine.setDateMAJ(new Date());
        valider(groupeOrigine);
        GroupeOrigine groupeOrigineResultant = groupeOrigineDao.maj(groupeOrigine, ancienCode);

        LOG.debug("Mise � jour d'un groupe origine - apr�s : " + groupeOrigineResultant.toStringPretty());

        opaManager.obligerRelanceOpaTour();
        return groupeOrigineResultant;
    }

    /**
     * Supprime un groupe origine.
     * 
     * @param groupeOrigine
     *            le groupe origine � supprimer
     */
    public void supprimer(GroupeOrigine groupeOrigine) {

        LOG.debug("Suppression du groupe origine " + groupeOrigine.toStringPretty());
        groupeOrigineDao.supprimer(groupeOrigine);
        opaManager.obligerRelanceOpaTour();
    }

    /**
     * Valide un groupe origine.
     * 
     * @param groupeOrigine
     *            le <code>GroupeOrigine</code> � valider
     * @throws ValidationException
     *             erreur de validation en cas de probl�me
     */
    public void valider(GroupeOrigine groupeOrigine) {
        // si le secteur est de type "tous" ou "public", il ne faut pas
        // sp�cifier de type contrat
        String codePublicPrive = groupeOrigine.getCodePublicPrive();
        if ((codePublicPrive.equals(GroupeOrigine.SECTEUR_TOUS)
                || codePublicPrive.equals(GroupeOrigine.SECTEUR_PUBLIC))
                && (!groupeOrigine.getCodeContrat().equals(GroupeOrigine.TYPE_CONTRAT_TOUS))) {
            throw new ValidationException("Ne pas sp�cifier de contrat lorsque le secteur n'est pas priv�");
        }

        // Les types d'�tablissements saisis doivent r�pondre � la notion d'origine
        validerTypeEtablissementOrigine(groupeOrigine.getTypeEtablissement1());
        validerTypeEtablissementOrigine(groupeOrigine.getTypeEtablissement2());
        validerTypeEtablissementOrigine(groupeOrigine.getTypeEtablissement3());

        // Les natures des �tablissements saisis doivent r�pondre � la notion d'origine
        validerNatureEtablissementOrigine(groupeOrigine.getNature1());
        validerNatureEtablissementOrigine(groupeOrigine.getNature2());
        validerNatureEtablissementOrigine(groupeOrigine.getNature3());

        // les formations et les options saisies doivent r�pondre � la notion d'origine si elles existent
        // et ne pas �tre de type "AFFECTATION"
        List<GroupeOrigineFormation> listFormations = groupeOrigine.getFormations();
        for (int i = 0; i < listFormations.size(); i++) {
            GroupeOrigineFormation gof = listFormations.get(i);
            // Formation ouverte ?
            Formation formation = gof.getFormation();
            if (formation != null && !ouvrableManager.isOrigine(formation)) {
                throw new ValidationException(
                        "La formation d'origine " + formation.getId().toPrettyString() + " n'est pas ouverte");
            }
            // Formation "AFFECTATION" ?
            if (formation != null && formation.estAffectation()) {
                throw new ValidationException("La formation d'origine " + formation.getId().toPrettyString()
                        + " est une formation de type 'AFFECTATION'. "
                        + "Elle ne peut pas figurer dans la liste des formations d'origine");
            }
            // Option 1
            Matiere option1 = gof.getOptionOrigine1();
            if (option1 != null && !ouvrableManager.isOrigine(option1)) {
                throw new ValidationException("L'option 1 " + option1.getCleGestion() + " n'est pas ouverte");

            }
            // Option 2
            Matiere option2 = gof.getOptionOrigine2();
            if (option2 != null && !ouvrableManager.isOrigine(option2)) {
                throw new ValidationException("L'option 2 " + option2.getCleGestion() + " n'est pas ouverte");
            }
            // les 2 options doivent �tre diff�rentes
            if (option1 != null && option2 != null && option1.getCleGestion().equals(option2.getCleGestion())) {
                throw new ValidationException(
                        "Les 2 options doivent �tre diff�rentes (" + option1.getCleGestion() + ")");
            }
        }
    }

    /**
     * Valide la nature �tablissement et son ouverture en origine.
     * 
     * @param nature
     *            la nature de l'�tablissement
     * @throws ValidationException
     *             en cas de probl�me sur la nature d'�tablissement
     */
    private void validerNatureEtablissementOrigine(Nature nature) {
        if (nature != null && !ouvrableManager.isOrigine(nature)) {
            throw new ValidationException("Le code nature " + nature.getCode() + " (" + nature.getLibelleLong()
                    + ") n'est pas ouvert en origine");
        }
    }

    /**
     * Valide le type d'�tablissement et son ouverture en origine.
     * 
     * @param type
     *            le type d'�tablissement
     * @throws ValidationException
     *             en cas de probl�me sur le type �tablissement
     */
    private void validerTypeEtablissementOrigine(TypeEtablissement type) {
        if (type != null && !ouvrableManager.isOrigine(type)) {
            throw new ValidationException("Le type �tablissement " + type.getCode() + " (" + type.getLibelleLong()
                    + ") n'est pas ouvert en origine");
        }
    }

    /**
     * @return vrai s'il y a au moins un groupe origine (diff�rent du groupe autres '99')
     */
    public boolean verifierExistenceGroupeOrigine() {
        LOG.debug("V�rification du nombre de groupes origine");
        Filtre filtreGroupe = Filtre.notEqual("code", GroupeOrigine.CODE_GROUPE_AUTRES);
        return groupeOrigineDao.getNombreElements(filtreGroupe) > 0;
    }

    /** @return liste de tous les groupes origine possibles pour les �l�ves */
    public List<GroupeOrigine> listerGroupesOrigine() {
        return groupeOrigineDao.lister();
    }

    /**
     * Liste les �l�ves du groupe 'Autres' avec une taille limite.
     * 
     * @param nombreMaximumElevesListes
     *            nombre maximal d'�l�ves � lister
     * @return liste, �ventuellement tronqu�e des �l�ves du groupe "autres"
     */
    public TruncationList<Eleve> listeElevesGroupeAutres(int nombreMaximumElevesListes) {

        TruncationList<Eleve> troncatureListeElevesGroupeAutres = new TruncationList<>(nombreMaximumElevesListes);

        // on prend les �l�ves dont le groupe origine est 'Autres' (99)
        List<Filtre> listFilters = new ArrayList<Filtre>();
        listFilters.add(Filtre.equal("groupeOrigine.code", GroupeOrigine.CODE_GROUPE_AUTRES));

        int nbElevesGroupeAutres = eleveDao.getNombreElements(listFilters);
        if (nbElevesGroupeAutres > nombreMaximumElevesListes) {

            troncatureListeElevesGroupeAutres.setList(eleveDao.lister(listFilters, 0, nombreMaximumElevesListes),
                    true);

        } else if (nbElevesGroupeAutres > 0) {

            troncatureListeElevesGroupeAutres.setList(eleveDao.lister(listFilters), false);
        }

        return troncatureListeElevesGroupeAutres;
    }
}
