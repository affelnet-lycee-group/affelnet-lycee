/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.LienZoneGeoDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/** Gestionnaire de liens de zones g�ographiques. */
@Service
public class LienZoneGeoManager extends AbstractManager {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(LienZoneGeoManager.class);

    /** Objet d'acc�s aux liens zones g�ographiques. */
    private LienZoneGeoDao lienZoneGeoDao;

    /** Gestionnaire d'op�rations programm�es d'affectation. */
    private OperationProgrammeeAffectationManager opaManager;

    /** Le gestionnaire des ofrres de formation. */
    private VoeuManager offreFormationManager;

    /**
     * @param lienZoneGeoDao
     *            Objet d'acc�s aux liens zones g�ographiques
     */
    @Autowired
    public void setLienZoneGeoDao(LienZoneGeoDao lienZoneGeoDao) {
        this.lienZoneGeoDao = lienZoneGeoDao;
    }

    /**
     * @param opaManager
     *            Gestionnaire d'op�rations programm�es d'affectation
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * @param offreFormationManager
     *            the offreFormationManager to set
     */
    @Autowired
    public void setOffreFormationManager(VoeuManager offreFormationManager) {
        this.offreFormationManager = offreFormationManager;
    }

    /**
     * @param id
     *            l'identifiant du lien zone g�ographique
     * @return lien zone g�ographique
     */
    public LienZoneGeo charger(Long id) {
        return lienZoneGeoDao.charger(id);
    }

    /**
     * Cr�e un nouveau lien zone g�ograpique.
     * 
     * @param lienZoneGeo
     *            le nouveau lien zone g�ographique
     * @return le lien cr��
     */
    public LienZoneGeo creer(LienZoneGeo lienZoneGeo) {

        LOG.debug("Cr�ation : " + lienZoneGeo.toStringPretty());

        lienZoneGeo.setDateCreation(new Date());
        valider(lienZoneGeo);

        opaManager.obligerRelanceOpa(lienZoneGeo);

        Voeu offreFormation = lienZoneGeo.getVoeu();
        if(lienZoneGeo.isLienVersOffreSecteur() && Flag.NON.equals(offreFormation.getVisiblePortail())) {
            offreFormation.setVisiblePortail(Flag.OUI);
            offreFormationManager.mettreAJour(offreFormation);
        }

        return lienZoneGeoDao.creer(lienZoneGeo);
    }

    /**
     * Met � jour le lien zone g�ographique.
     * 
     * @param lienZoneGeoModifie
     *            le lien mis � jour
     * @param ancienId
     *            l'ancien identifiant
     * @return la zone g�ographique mise � jour
     */
    public LienZoneGeo maj(LienZoneGeo lienZoneGeoModifie, Long ancienId) {

        LienZoneGeo lienZoneGeoActuel = charger(ancienId);
        LOG.debug("Mise � jour : lien actuel : " + lienZoneGeoActuel.toStringPretty());
        opaManager.obligerRelanceOpa(lienZoneGeoActuel);

        LOG.debug("Mise � jour : nouveau lien : " + lienZoneGeoModifie.toStringPretty());

        lienZoneGeoModifie.setDateMAJ(new Date());
        valider(lienZoneGeoModifie);
        opaManager.obligerRelanceOpa(lienZoneGeoModifie);

        Voeu offreFormation = lienZoneGeoModifie.getVoeu();
        if (lienZoneGeoModifie.isLienVersOffreSecteur() && Flag.NON.equals(offreFormation.getVisiblePortail())) {
            offreFormation.setVisiblePortail(Flag.OUI);
            offreFormationManager.mettreAJour(offreFormation);
        }

        return lienZoneGeoDao.maj(lienZoneGeoModifie, ancienId);
    }

    /**
     * M�thode de v�rification d'un lien entre zones g�ographiques.
     * 
     * @param lienZoneGeo
     *            le lien � v�rifier
     */
    protected void valider(LienZoneGeo lienZoneGeo) {
        if (existeParCriteres(lienZoneGeo)) {
            // il ne peut pas y avoir 2 liens zones g�o. avec les m�mes crit�res
            throw new ValidationException("Un autre lien zone g�ographique existe d�j� avec les m�me crit�res");
        }
    }

    /**
     * @param lienZoneGeo
     *            le lien zone g�ographique
     * @return vrai si le lien existe, selon les crit�res (code zone g�o origine + code voeu), sinon faux
     */
    protected boolean existeParCriteres(LienZoneGeo lienZoneGeo) {

        ZoneGeo zoneGeo = lienZoneGeo.getZoneGeoOrigine();
        Voeu voeu = lienZoneGeo.getVoeu();

        // filtres utiles
        List<Filtre> filtres = new ArrayList<Filtre>();
        if (zoneGeo == null) {
            filtres.add(Filtre.isNull("codeZoneGeoOrigine"));
        } else {
            filtres.add(Filtre.equal("codeZoneGeoOrigine", zoneGeo.getCode()));
        }
        filtres.add(Filtre.equal("voeu.code", voeu.getCode()));
        if (lienZoneGeo.getId() != null) {
            filtres.add(Filtre.notEqual("id", lienZoneGeo.getId()));
        }

        return lienZoneGeoDao.getNombreElements(filtres) > 0;
    }

    /**
     * Supprime un lien zone g�ographique.
     * 
     * @param lienZoneGeographique
     *            le lien zone g�ographique � supprimer
     */
    public void supprimer(LienZoneGeo lienZoneGeographique) {

        LOG.debug("Suppression : " + lienZoneGeographique.toStringPretty());

        opaManager.obligerRelanceOpa(lienZoneGeographique);

        lienZoneGeoDao.supprimer(lienZoneGeographique);
    }
}
