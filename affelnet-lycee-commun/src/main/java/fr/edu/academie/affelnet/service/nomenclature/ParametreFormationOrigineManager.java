/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.ParametreFormationOrigineComparator;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.ParametreFormationOrigineDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.EleveDao;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.ParametreFormationOrigine;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/** Gestionnaire de param�tres par formation d'origine. */
@Service
public class ParametreFormationOrigineManager {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(ParametreFormationOrigineManager.class);

    /**
     * Le DAO des param�tres par formation d'origine.
     */
    private ParametreFormationOrigineDao parametreFormationOrigineDao;

    /**
     * Le DAO des �l�ves.
     */
    private EleveDao eleveDao;

    /** Le gestionnaire d'operation programmee d'affectation. */
    private OperationProgrammeeAffectationManager opaManager;

    /** Le gestionnaire des mati�res. */
    private MatiereManager matiereManager;

    /**
     * @param parametreFormationOrigineDao
     *            parametreFormationOrigineDao
     */
    @Autowired
    public void setParametreFormationOrigineDao(ParametreFormationOrigineDao parametreFormationOrigineDao) {
        this.parametreFormationOrigineDao = parametreFormationOrigineDao;
    }

    /**
     * @param eleveDao
     *            Le DAO des �l�ves
     */
    @Autowired
    public void setEleveDao(EleveDao eleveDao) {
        this.eleveDao = eleveDao;
    }

    /**
     * @param opaManager
     *            Le gestionnaire d'operation programmee d'affectation.
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * @param matiereManager
     *            the matiereManager to set
     */
    @Autowired
    public void setMatiereManager(MatiereManager matiereManager) {
        this.matiereManager = matiereManager;
    }

    /**
     * Charge un param�tre par formation origine par son identifiant.
     * 
     * @param idParamefo
     *            identifiant du param�tre par formation origine.
     * @return le param�tre par formation origine
     */
    public ParametreFormationOrigine charger(Long idParamefo) {
        return parametreFormationOrigineDao.charger(idParamefo);
    }

    /**
     * Cr�e un nouveau param�tre par formation origine.
     * 
     * @param parametreFormationOrigine
     *            le nouveau param�tre par formation origine
     * @return le param�tre par formation origine cr��
     */
    public ParametreFormationOrigine creer(ParametreFormationOrigine parametreFormationOrigine) {

        parametreFormationOrigine.setDateCreation(new Date());

        LOG.debug("Cr�ation d'un paramefo : " + parametreFormationOrigine.toPrettyString());
        ParametreFormationOrigine parametreCree = parametreFormationOrigineDao.creer(parametreFormationOrigine);
        obligerRelanceOpa(parametreCree);
        return parametreCree;
    }

    /**
     * Oblige la relance des OPA concern�es par la modification du paramefo si la formation est utilis�e.
     * 
     * @param parametreFormationOrigine
     *            le param�tre par formation d'origine
     */
    private void obligerRelanceOpa(ParametreFormationOrigine parametreFormationOrigine) {

        // Le recherche des OPA peut �tre assez longue, � r�server si le paramefo est utilis�
        // => test pr�alable de l'utilit�
        if (estFormationOrigineUtilisee(parametreFormationOrigine)) {
            LOG.debug("Des �l�ves sont potentiellement concern� par ce parametre => impact sur les OPA.");
            opaManager.obligerRelanceOpa(parametreFormationOrigine);

        } else {
            LOG.debug("Aucun �l�ve n'est actuellement concern� par ce parametre => pas d'impact sur les OPA.");
        }

    }

    /**
     * Recherche du param�tre par formation d'origine qui s'applique pour les options indiqu�es.
     * 
     * @param formation
     *            Formation origine � tester
     * @param lOptions
     *            liste des options
     * @param cacheMap
     *            table associative servant de cache
     * @return le param�tre par formation origine qui s'applique
     */
    @SuppressWarnings("unchecked")
    public ParametreFormationOrigine find(Formation formation, List<Matiere> lOptions,
            Map<String, Object> cacheMap) {
        // r�cup�ration de la liste des ParametreFormationOrigine
        final String cacheName = "listeParametresFormationOrigine";
        List<ParametreFormationOrigine> lPfo;
        if (cacheMap == null) {
            // si pas de cache, on r�duit les possibilit�s en mettant les crit�res obligatoires
            lPfo = parametreFormationOrigineDao.listerParametreFormationOriginePourFormation(formation);
        } else if (!cacheMap.containsKey(cacheName)) {

            lPfo = parametreFormationOrigineDao.listerParametreFormationOrigineAvecJointureFetch();
            cacheMap.put(cacheName, new ArrayList<ParametreFormationOrigine>(lPfo));
        } else {

            // new pour ne pas alterer la liste en cache
            lPfo = new ArrayList<>((List<ParametreFormationOrigine>) cacheMap.get(cacheName));
        }
        List<Predicate<ParametreFormationOrigine>> predicats = listePredicat(formation, lOptions, cacheMap);
        for (Predicate<ParametreFormationOrigine> predicat : predicats) {
            lPfo.removeIf(predicat);
        }

        // pas d'�l�ment corrects
        if (lPfo.isEmpty()) {
            return null;
        }

        // Il reste les �l�ments qui correspondent au cas de l'�l�ve, donc on prend
        // le paramefo le plus sp�cifique (i.e. le min vis � vis du comparateur)
        return Collections.min(lPfo, new ParametreFormationOrigineComparator());
    }

    /**
     * Fournis la liste des pr�dicats pour �liminer tous les param�tres par formation d'origine qui ne
     * correspondent pas � la formation et les options en entr�e.
     * 
     * @param formation
     *            la formation
     * @param options
     *            les options
     * @param cacheMap
     *            le cache
     * @return la liste des pr�dicats pour filter les param�tres par formation d'origine
     */
    private List<Predicate<ParametreFormationOrigine>> listePredicat(Formation formation, List<Matiere> options,
            Map<String, Object> cacheMap) {
        List<Predicate<ParametreFormationOrigine>> predicats = new ArrayList<>();

        // M�me formation origine
        predicats.add(pfo -> !pfo.getMnemonique().equals(formation.getId().getMnemonique()));
        predicats.add(pfo -> pfo.getCodeSpecialite() != null
                && !pfo.getCodeSpecialite().equals(formation.getId().getCodeSpecialite()));

        predicats.add(pfo -> pfo.getOption1() != null
                && !matiereManager.containsMatOrLV(pfo.getOption1(), options, cacheMap));
        predicats.add(pfo -> pfo.getOption2() != null
                && !matiereManager.containsMatOrLV(pfo.getOption2(), options, cacheMap));

        return predicats;
    }

    /**
     * Met � jour un param�tre par formation origine.
     * 
     * @param parametreFormationOrigineModifie
     *            le param�tre par formation origine modifi�
     * @param idAncienParamefo
     *            l'identifiant de l'ancien param�tre par formation origine
     * 
     * @return le param�tre par formation origine modifi�
     */
    public ParametreFormationOrigine mettreAJour(ParametreFormationOrigine parametreFormationOrigineModifie,
            Long idAncienParamefo) {

        ParametreFormationOrigine ancienParamefo = charger(idAncienParamefo);

        parametreFormationOrigineModifie.setDateMAJ(new Date());

        LOG.debug("Mise � jour du paramefo : " + ancienParamefo.toPrettyString());

        obligerRelanceOpa(ancienParamefo);

        ParametreFormationOrigine parametreFormationOrigineResultant = parametreFormationOrigineDao
                .maj(parametreFormationOrigineModifie, idAncienParamefo);

        LOG.debug("Le paramefo mis � jour est " + parametreFormationOrigineModifie.toPrettyString());
        obligerRelanceOpa(parametreFormationOrigineResultant);

        return parametreFormationOrigineResultant;
    }

    /**
     * Supprime un param�tre par formation origine.
     * 
     * @param parametreFormationOrigine
     *            le param�tre par formation origine supprim�
     */
    public void supprimer(ParametreFormationOrigine parametreFormationOrigine) {

        LOG.debug("Suppression d'un paramefo : " + parametreFormationOrigine.toPrettyString());

        if (estFormationOrigineUtilisee(parametreFormationOrigine)) {
            throw new ValidationException("La suppression est impossible, un �l�ve suit cette formation");
        }
        parametreFormationOrigineDao.supprimer(parametreFormationOrigine);
    }

    /**
     * V�rifie de l'existence d'�l�ves issus d'une formation concern�e par le paremefo (par mn�monique seulement).
     * 
     * @param parametreFormationOrigine
     *            le parametre par formation origine
     * @return vrai s'il y a des �l�ves qui viennent d'une formation concern�e, sinon faux
     */
    public boolean estFormationOrigineUtilisee(ParametreFormationOrigine parametreFormationOrigine) {
        return eleveDao.estMnemoniqueFormationOrigineEleve(parametreFormationOrigine.getMnemonique());
    }

    /**
     * V�rifie que le param�tre par formation n'entrent pas en conflit avec un autre param�tre existant.
     * 
     * @param id
     *            l'identifiant du param�tre par formation origine
     * @param formation
     *            la formation test�e
     * @param mnemonique
     *            le mn�monique de la formation test�e
     * @param option1
     *            l'option 1 de la formation test�e
     * @param option2
     *            l'option 2 de la formation test�e
     * 
     * @return vrai si le paramefa n'existe pas ou d'il est unique avec le m�me identifiant, faux sinon
     */
    public boolean isUnique(Long id, Formation formation, String mnemonique, Matiere option1, Matiere option2) {
        return parametreFormationOrigineDao.isUnique(id, formation, mnemonique, option1, option2);
    }

    /**
     * Teste s'il existe des �l�ves avec dont la formation origine correspond � ce Paramefo.
     * 
     * @param pfo
     *            le <code>ParametreFormationOrigine</code> dont il faut
     *            v�rifier les d�pendances
     * @return vrai si le <code>ParametreFormationOrigine</code> est utilis�
     *         pour les voeux des �l�ves
     */
    public boolean rechercheEleves(ParametreFormationOrigine pfo) {
        List<Filtre> filtres = new ArrayList<Filtre>();

        if (pfo.getMnemonique() != null) {
            filtres.add(Filtre.equal("formation.id.mnemonique", pfo.getMnemonique()));
        }

        if (pfo.getCodeSpecialite() != null) {
            filtres.add(Filtre.equal("formation.id.codeSpecialite", pfo.getCodeSpecialite()));
        }

        if (pfo.getOption1() != null) {
            filtres.add(Filtre.equal("optionOrigine2", pfo.getOption1()));
        }
        if (pfo.getOption2() != null) {
            filtres.add(Filtre.equal("optionOrigine3", pfo.getOption2()));
        }

        return eleveDao.getNombreElements(filtres) > 0;
    }
}
