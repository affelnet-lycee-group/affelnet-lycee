/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.DaoException;
import fr.edu.academie.affelnet.dao.hibernate.HibernateUtil;
import fr.edu.academie.affelnet.dao.hibernate.nomenclature.comparators.CoefficientDisciplineAcademiqueComparator;
import fr.edu.academie.affelnet.dao.hibernate.nomenclature.comparators.CoefficientEvaluationComplementaireComparator;
import fr.edu.academie.affelnet.dao.hibernate.nomenclature.comparators.LienRangParametresFormationAccueilComparator;
import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.ParametresFormationAccueilComparator;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.ParametresFormationAccueilDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.PileDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.VoeuDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationPK;
import fr.edu.academie.affelnet.domain.nomenclature.LienRangParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.LienRangParametresFormationAccueilPK;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.Pile;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.ChampDisciplinaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientDisciplineAcademique;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientDisciplineAcademiquePK;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementaire;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.CoefficientEvaluationComplementairePK;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.Discipline;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.DomaineSpecialite;
import fr.edu.academie.affelnet.domain.nomenclature.evaluation.EvaluationComplementaire;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;
import fr.edu.academie.affelnet.service.NotesManager;
import fr.edu.academie.affelnet.service.ParametreManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationComplementaireManager;
import fr.edu.academie.affelnet.service.evaluation.EvaluationDisciplineManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;

/** Classe permettant de g�rer les param�tres par formation d'accueil. */
@Service
public class ParametresFormationAccueilManager extends AbstractManager {

    /** Valeur de l'indicateur de saisie de l'avis li� au ParameFA pour JAMAIS. */
    public static final String IND_SAISIE_AVIS_JAMAIS = "0";

    /**
     * Valeur de l'indicateur de saisie de l'avis lia au ParameFA pour SEULEMENT
     * POUR CERTAINES FORMATIONS.
     */
    public static final String IND_SAISIE_AVIS_SELON_FORMATION_ORIGINE = "1";

    /** Valeur de l'indicateur de saisie de l'avis li� au ParameFA pour TOUJOURS. */
    public static final String IND_SAISIE_AVIS_TOUJOURS = "2";

    /** Libell�s des indicateurs pour la saisie des avis ParameFA. */
    public static final String[] LIBELLES_INDICATEURS_SAISIE_AVIS = { "Jamais",
            "Seulement pour certaines formations d'origine", "Toujours" };

    /** Valeur de l'indicateur de saisie de l'avis li� au ParameFA par d�faut. */
    public static final String IND_SAISIE_AVIS_NONDEFINI = "9";

    /** Valeur de l'indicateur des modes de bareme non d�fini par d�faut. */
    public static final String MODE_BAREME_NONDEFINI = "INDEFINI";

    /** Le loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(ParametresFormationAccueilManager.class);

    /**
     * Le dao utilis� pour les param�tres par formation d'accueil.
     */
    private ParametresFormationAccueilDao parametresFormationAccueilDao;

    /**
     * Le dao utilis� pour les voeux.
     */
    private VoeuDao voeuDao;

    /**
     * Le dao utilis� pour les voeux des �l�ves.
     */
    private VoeuEleveDao voeuEleveDao;

    /** Le dao utilis� par les piles. */
    private PileDao pileDao;

    /**
     * Le manager utilis� pour les formations-mati�res .
     */
    private FormationMatiereManager formationMatiereManager;

    /**
     * Le manager utilis� pour les voeu.
     */
    private VoeuManager voeuManager;

    /** Le gestionnaire d'operation programmee d'affectation. */
    private OperationProgrammeeAffectationManager opaManager;

    /**
     * Le manager utilis� pour les notes.
     */
    private NotesManager notesManager;

    /**
     * Le manager utilis� pour les �valuations compl�mentaires.
     */
    private EvaluationComplementaireManager evaluationComplementaireManager;

    /**
     * Le manager du domaine de sp�cialit�.
     */
    private DomaineSpecialiteManager domaineSpecialiteManager;

    /**
     * Le manager des �valuations.
     */
    private EvaluationDisciplineManager evaluationDisciplineManager;

    /**
     * Le manager des param�tres.
     */
    private ParametreManager parametreManager;

    /**
     * Le manager pour le tour courant.
     */
    private CampagneAffectationManager campagneAffectationManager;

    /**
     * @param parametresFormationAccueilDao
     *            the parametresFormationAccueilDao to set
     */
    @Autowired
    public void setParametresFormationAccueilDao(ParametresFormationAccueilDao parametresFormationAccueilDao) {
        this.parametresFormationAccueilDao = parametresFormationAccueilDao;
    }

    /**
     * @param voeuDao
     *            the voeuDao to set
     */
    @Autowired
    public void setVoeuDao(VoeuDao voeuDao) {
        this.voeuDao = voeuDao;
    }

    /**
     * @param voeuEleveDao
     *            the voeuEleveDao to set
     */
    @Autowired
    public void setVoeuEleveDao(VoeuEleveDao voeuEleveDao) {
        this.voeuEleveDao = voeuEleveDao;
    }

    /**
     * @param formationMatiereManager
     *            the formationMatiereManager to set
     */
    @Autowired
    public void setFormationMatiereManager(FormationMatiereManager formationMatiereManager) {
        this.formationMatiereManager = formationMatiereManager;
    }

    /**
     * @param voeuManager
     *            the voeuManager to set
     */
    @Autowired
    public void setVoeuManager(VoeuManager voeuManager) {
        this.voeuManager = voeuManager;
    }

    /**
     * @param opaManager
     *            Le gestionnaire d'operation programmee d'affectation.
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * M�thode permettant de r�cup�rer le gestionnaire.
     * 
     * @param notesManager
     *            le gestionnaire des notes.
     */
    @Autowired
    public void setNotesManager(NotesManager notesManager) {
        this.notesManager = notesManager;
    }

    /**
     * M�thode permettant de r�cup�rer le gestionnaire.
     * 
     * @param evaluationComplementaireManager
     *            le gestionnaire des �valuations compl�mentaires
     */
    @Autowired
    public void setEvaluationComplementaireManager(
            EvaluationComplementaireManager evaluationComplementaireManager) {
        this.evaluationComplementaireManager = evaluationComplementaireManager;
    }

    /**
     * M�thode permettant de r�cup�rer le gestionnaire.
     * 
     * @param domaineSpecialiteManager
     *            le gestionnaire des domaines de sp�cialit�
     */
    @Autowired
    public void setDomaineSpecialiteManager(DomaineSpecialiteManager domaineSpecialiteManager) {
        this.domaineSpecialiteManager = domaineSpecialiteManager;
    }

    /**
     * M�thode permettant de r�cup�rer le gestionnaire.
     * 
     * @param evaluationDisciplineManager
     *            le gestionnaire des evaluations
     */
    @Autowired
    public void setEvaluationDisciplineManager(EvaluationDisciplineManager evaluationDisciplineManager) {
        this.evaluationDisciplineManager = evaluationDisciplineManager;
    }

    /**
     * M�thode permettant de r�cup�rer le gestionnaire.
     * 
     * @param parametreManager
     *            le gestionnaire des param�tres de l'application
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * M�thode permettant de r�cup�rer le dao.
     * 
     * @param pileDao
     *            le dao g�rant les piles
     */
    @Autowired
    public void setPileDao(PileDao pileDao) {
        this.pileDao = pileDao;
    }

    /**
     * @param campagneAffectationManager
     *            the campagneAffectationManager to set
     */
    @Autowired
    public void setCampagneAffectationManager(CampagneAffectationManager campagneAffectationManager) {
        this.campagneAffectationManager = campagneAffectationManager;
    }

    /**
     * Retire de la nomenclature des voeux tous les liens vers un ParameFA donn�.
     * 
     * @param paramefa
     *            Parametres par formation d'accueil concern�
     */
    public void retirerIndicationsVoeuxPourSaisieAvisParamefa(ParametresFormationAccueil paramefa) {

        LOG.debug("Nettoyage des indicateurs d'avis Paramefa pour le param�tre '" + paramefa.getId() + "'");

        // Cr�ation du filtre sur les voeux correspondants au Paramefa
        List<Filtre> listeFiltres = new ArrayList<>();
        listeFiltres.add(voeuManager.getFiltreUtilisantParamefa(paramefa));

        // Retire le lien paramefa pour tous les voeux correspondant au filtre
        majIndicateursAvisParamefaVoeu(listeFiltres, IND_SAISIE_AVIS_JAMAIS, null);
    }

    /**
     * Mise � jour des indications de saisie de l'avis li� au ParameFA dans les
     * voeux.
     * 
     * @param formation
     *            la <code>Formation</code> d'accueil
     * 
     *            La mise � jour concerne TOUS les voeux li�s � la formation
     *            d'accueil.
     */
    public void majIndicationsVoeuxPourSaisieAvisParamefa(Formation formation) {

        LOG.debug("Recalcul des indicateurs d'avis Paramefa pour tous les voeux sur la formation '"
                + formation.getId().toPrettyString() + "'");

        // r�-initialisation des indicateurs pour tous les voeux qui utilisent
        // la formation d'accueil
        reinitialiseIndicateursAvisParamefaVoeuPourFormation(formation);

        // r�cup�ration d'un tableau contenant tous les PARAMEFA qui utilisent
        // la formation d'accueil tri� dans l'ordre de leur application (du +
        // g�n�rique au - g�n�rique)
        // CAR on veut que le plus sp�cifique s'applique en dernier
        ParametresFormationAccueil[] tPfas = parametresFormationAccueilDao.listerPfaFormation(formation);

        // Pour chaque param�tre par formation d'accueil, mise � jour dans les voeux
        // - de l'indicateur pour la saisie de l'avis
        // - du Paramefa qui s'applique
        for (int i = 0; i < tPfas.length; i++) {
            ParametresFormationAccueil pfaEnCours = tPfas[i];
            majIndicateursAvisParamefaVoeu(pfaEnCours);
        }

    }

    /**
     * R�initialise les indicateurs de saisie de l'Avis ParameFA pour tous les voeux
     * sur la formation donn�e.
     * 
     * @param formation
     *            Formation d'accueil concern�e
     */
    private void reinitialiseIndicateursAvisParamefaVoeuPourFormation(Formation formation) {

        // Cr�ation du filtre sur les voeux � partir de la formation
        List<Filtre> listeFiltres = new ArrayList<>();

        // ajout du filtre formation
        listeFiltres.add(voeuManager.getFiltreSurOffreFormationFormationAccueil(formation));
        majIndicateursAvisParamefaVoeu(listeFiltres, IND_SAISIE_AVIS_JAMAIS, null);
    }

    /**
     * Positionne les indicateurs de saisie de l'Avis ParameFA pour tous les voeux
     * concern�s par le ParameFA.
     * 
     * @param paramefa
     *            Param�tre par formation d'accueil concern�
     */
    private void majIndicateursAvisParamefaVoeu(ParametresFormationAccueil paramefa) {

        // Cr�ation du filtre sur les voeux � partir du Paramefa
        List<Filtre> listeFiltres = new ArrayList<>();
        listeFiltres.add(voeuManager.getFiltreSurOffreFormationFormationAccueil(paramefa.getFormationAccueil()));
        if (paramefa.getMatiereEnseigOptionnel() != null) {
            listeFiltres.add(voeuManager
                    .ajouteFiltreSurOffreFormationEnseignementOptionnel(paramefa.getMatiereEnseigOptionnel()));
        }

        majIndicateursAvisParamefaVoeu(listeFiltres, paramefa.getIndicateurSaisieAvis(), paramefa);
    }

    /**
     * Cette m�thode met � jour les informations de saisie de l'avis li� aux
     * paramefa dans la nomenclature voeu pour tous les voeux correspondant au
     * filtre fourni.
     * 
     * Les champs de la nomenclature voeu suivants sont impact�s : - l'indicateur de
     * saisie de l'avis Paramefa (chef d'�tab / conseil de classe) - le lien vers le
     * paramefa concern�
     * 
     * @param filtresVoeuxConcernes
     *            filtre sur les voeux � mettre � jour
     * @param indicateur
     *            la valeur � donner pour l'indicateur
     * @param pfa
     *            le param�tre par formation d'accueil qui s'applique (utile dans le
     *            cas IND_CC="1") sert lors de la saisie des voeux en �tablissement
     *            pour optimiser les temps d'acc�s
     */
    private void majIndicateursAvisParamefaVoeu(List<Filtre> filtresVoeuxConcernes, String indicateur,
            ParametresFormationAccueil pfa) {

        // Mise � jour de l'indicateur de saisie de l'avis
        List<Voeu> listeVoeux = voeuDao.lister(filtresVoeuxConcernes, false);

        for (Voeu voeu : listeVoeux) {
            for (Pile pile : voeu.getPiles().values()) {
                pile.setIndicateurSaisieAvis(indicateur);
                pile.setParametreFormationAccueil(pfa);
            }
            voeuDao.maj(voeu, voeu.getCode());
        }
    }

    /**
     * Positionne les informations d'avis paramefa pour un voeu et sa pile (flags et
     * liens).
     * 
     * Ces indicateurs concernent le champ IND_CC pour activer, selon l'avis : du
     * chef �tablissement d'origine / du conseil de classe
     * 
     * @param voeu
     *            Voeu � modifier
     * @param pile
     *            La pile associ�e au voeu.
     */
    public void positionneIndicateursAvisParamefaVoeu(Voeu voeu, Pile pile) {
        ParametresFormationAccueil paramefaApplicable = trouverParamefaPourVoeu(voeu);
        String indicateurSaisieAvis;

        if (paramefaApplicable != null) {
            indicateurSaisieAvis = paramefaApplicable.getIndicateurSaisieAvis();
        } else {
            indicateurSaisieAvis = ParametresFormationAccueilManager.IND_SAISIE_AVIS_JAMAIS;
        }

        pile.setIndicateurSaisieAvis(indicateurSaisieAvis);
        pile.setParametreFormationAccueil(paramefaApplicable);
    }

    /**
     * Trouve le param�tre par formation d'accueil qui s'applique sur un voeu donn�.
     * 
     * @param voeu
     *            Voeu � examiner
     * @return Param�tre par formation d'accueil le plus sp�cifique qui s'applique
     *         sur le voeu
     */
    public ParametresFormationAccueil trouverParamefaPourVoeu(Voeu voeu) {
        return trouverParamefaPourVoeu(voeu, null);
    }

    /**
     * Trouve le param�tre par formation d'accueil qui s'applique sur un voeu donn�.
     * 
     * @param voeu
     *            Voeu � examiner
     * @param cacheMap
     *            Le cache des param�tres par formation d'accueil
     * @return Param�tre par formation d'accueil le plus sp�cifique qui s'applique
     *         sur le voeu
     */
    public ParametresFormationAccueil trouverParamefaPourVoeu(Voeu offre, Map<String, Object> cacheMap) {

        Formation formation = offre.getFormationAccueil();
        Matiere enseignOptionnel = offre.getMatiereEnseigOptionnel();
        List<ParametresFormationAccueil> lPfa = new ArrayList<>(
                parametresFormationAccueilDao.recuprererParamefa(formation, cacheMap));

        List<Predicate<ParametresFormationAccueil>> predicats = listePredicat(formation, enseignOptionnel);
        for (Predicate<ParametresFormationAccueil> predicat : predicats) {
            lPfa.removeIf(predicat);
        }

        // pas d'�l�ment corrects
        if (lPfa.isEmpty()) {
            return null;
        }

        // il reste les �l�ments qui sont corrects, on les trie et on restitue le premier
        TreeSet<ParametresFormationAccueil> listeTriee = new TreeSet<>(new ParametresFormationAccueilComparator());
        listeTriee.addAll(lPfa);
        return listeTriee.first();
    }

    /**
     * Fournis la liste des pr�dicats pour �liminer tous les param�tres par formation d'accueil qui ne
     * correspondent pas � la formation et les options en entr�e.
     * 
     * @param formation
     *            la formation
     * @param enseignOptionnel
     *            l'enseignement optionnel
     * @return la liste des pr�dicats pour filter les param�tres par formation d'accueil
     */
    private List<Predicate<ParametresFormationAccueil>> listePredicat(Formation formation,
            Matiere enseignOptionnel) {
        List<Predicate<ParametresFormationAccueil>> predicats = new ArrayList<>();

        // M�me formation origine
        predicats.add(pfa -> !pfa.getFormationAccueil().equals(formation));

        if (enseignOptionnel != null) {
            predicats.add(pfa -> pfa.getMatiereEnseigOptionnel() != null
                    && !pfa.getMatiereEnseigOptionnel().equals(enseignOptionnel));
        } else {
            predicats.add(pfa -> pfa.getMatiereEnseigOptionnel() != null);
        }

        return predicats;
    }

    /**
     * Applique les r�gles m�tiers sur l'objet et rempli la liste des erreurs de
     * validation si l'objet n'est pas valide.
     * 
     * @param parametresFormationAccueil
     *            l'objet � valider
     * @param listeErreursValidation
     *            la liste des �ventuelles erreurs de validation
     */
    public void validationMetier(ParametresFormationAccueil parametresFormationAccueil,
            List<String> listeErreursValidation) {
        parametresFormationAccueil.setEOGeneriqusAutorise(true);
        // la formation d'accueil et l'enseignement optionnel doivent r�pondre � la notion d'accueil
        formationMatiereManager.validationOuvertureEO(parametresFormationAccueil, listeErreursValidation);
        // validation de la formation d'accueil avec l'enseignement optionnel
        formationMatiereManager.validationEOPourFormation(parametresFormationAccueil, listeErreursValidation);

        // controle d'unicit� form/enseignement optionnel
        if (!parametresFormationAccueilDao.isUnique(parametresFormationAccueil)) {
            String msgErreur = "Il existe d�j� un enregistrement avec la m�me formation d'accueil et le m�me enseignement optionnel";
            listeErreursValidation.add(msgErreur);
        }

        if (!listeErreursValidation.isEmpty()) {
            throw new ValidationException(listeErreursValidation);
        }
    }

    /**
     * Applique les r�gles m�tiers sur l'objet et rempli la liste des erreurs de
     * validation si l'objet n'est pas valide. Duplication de la m�thode
     * validationMetier pour ne pas avoir l'erreur si le triplet existe d�j�
     * 
     * @param parametresFormationAccueil
     *            l'objet � valider
     * @param listeErreursValidation
     *            la liste des �ventuelles erreurs de validation
     */
    public void validationMetierEE(ParametresFormationAccueil parametresFormationAccueil,
            List<String> listeErreursValidation) {
        parametresFormationAccueil.setEOGeneriqusAutorise(true);
        // la formation d'accueil et l'enseignement optionnel doivent r�pondre � la notion d'accueil
        formationMatiereManager.validationOuvertureEO(parametresFormationAccueil, listeErreursValidation);
        // validation de la formation d'accueil avec l'enseignement optionnel
        formationMatiereManager.validationEOPourFormation(parametresFormationAccueil, listeErreursValidation);

        if (!listeErreursValidation.isEmpty()) {
            throw new ValidationException(listeErreursValidation);
        }
    }

    /**
     * Indique si il existe des voeux �l�ves sur la formation et l'enseignement optionnel du paramefa.
     * 
     * @param pfa
     *            le param�tre par formation d'accueil
     * @return vrai si au moins un �l�ve utilise cette formation + EE
     */
    public boolean isUtiliseParEleves(ParametresFormationAccueil pfa) {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("voeu.formationAccueil.id.mnemonique",
                pfa.getFormationAccueil().getId().getMnemonique()));
        filtres.add(Filtre.equal("voeu.formationAccueil.id.codeSpecialite",
                pfa.getFormationAccueil().getId().getCodeSpecialite()));
        if (pfa.getMatiereEnseigOptionnel() != null) {
            filtres.add(Filtre.equal("voeu.matiereEnseigOptionnel", pfa.getMatiereEnseigOptionnel()));
        }

        return voeuEleveDao.getNombreElements(filtres) > 0;
    }

    /**
     * @return La liste de tous les identifiants de paramefa.
     */
    @SuppressWarnings("unchecked")
    public List<Long> getListeIdParametresFormationAccueil() {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(ParametresFormationAccueil.class);
        criteria.setProjection(Projections.property("id"));

        return criteria.list();
    }

    /**
     * M�thode permettant de renvoyer la liste des param�tres par formations
     * d'accueil en fonction du mode de bar�me.
     * 
     * @param mode
     *            le mode de bar�me
     * 
     * @return La liste de tous les identifiants de paramefa.
     */
    @SuppressWarnings("unchecked")
    public List<Long> getListeIdParametresFormationAccueil(String mode) {
        Criteria criteria = HibernateUtil.currentSession().createCriteria(ParametresFormationAccueil.class);
        criteria.setProjection(Projections.property("id"));
        criteria.add(Restrictions.eq("modeBareme", mode));

        return criteria.list();
    }

    /**
     * @param id
     *            L'identifiant technique du param�tre par formation d'accueil.
     * @return Le param�tre par formation d'accueil correspondant � l'identifiant.
     */
    public ParametresFormationAccueil charger(Long id) {
        HashSet<String> fetchJoin = new HashSet<>();
        fetchJoin.add("coefficientsDisciplineAca");
        fetchJoin.add("coefficients");
        fetchJoin.add("coefficientsEvalCompl");
        return parametresFormationAccueilDao.charger(id, fetchJoin);
    }

    /**
     * Fournit le parametre par formation d'accueil avec le domaine sp�cialit� associ�.
     * 
     * @param id
     *            L'identifiant technique du param�tre par formation d'accueil.
     * @return Le param�tre par formation d'accueil correspondant � l'identifiant.
     */
    public ParametresFormationAccueil chargerPfaAvecDemaineSpecialite(Long id) {
        ParametresFormationAccueil pfa = charger(id);

        // V�rification si on a un domaine de sp�cialit�
        DomaineSpecialite dm = domaineSpecialiteManager
                .recuperationDomaineSpecialiteRestreint2ndPro1Cap(pfa.getFormationAccueil());

        // Mise � jour de la valeur pour faire affichage du formulaire
        pfa.setDomaineSpecialite(dm);
        return pfa;
    }

    /**
     * Cr�e un param�tre nouveau para��tre par formation d'accueil.
     * 
     * @param paramefaACreer
     *            l'objet � cr�er
     * @return Le param�tre par formation d'accueil r�sultant de la cr�ation
     */
    public ParametresFormationAccueil creerParDefaut(ParametresFormationAccueil paramefaACreer) {

        // Fixe la date de cr�ation
        paramefaACreer.setDateCreation(new Date());

        // Par d�faut - suite d�coupage en trois page - on intialise les deux valeurs
        // obligatoires
        paramefaACreer.setModeBareme(ParametresFormationAccueil.MODE_BAREME_NOTE);
        paramefaACreer.setIndicateurSaisieAvis(IND_SAISIE_AVIS_JAMAIS);

        LOG.debug("Cr�ation d'un nouveau paramefa : " + paramefaACreer.toPrettyString());

        // R�cup�re les coefficients de pond�ration positionn�s sur les notes pour le
        // paramefa
        Set<LienRangParametresFormationAccueil> coefficients = paramefaACreer.getCoefficients();
        // R�cup�re les coefficients de pond�ration positionn�s sur les disciplines
        // acad�miques

        Set<CoefficientDisciplineAcademique> coefficientsDisciplineAca = paramefaACreer
                .getCoefficientsDisciplineAca();

        // R�cup�re les coefficients de pond�ration positionn�s sur les disciplines
        // acad�miques
        Set<CoefficientEvaluationComplementaire> coefficientsEvalCompl = paramefaACreer.getCoefficientsEvalCompl();

        // Initialise un ensemble tri�
        paramefaACreer.setCoefficients(new TreeSet<LienRangParametresFormationAccueil>(
                new LienRangParametresFormationAccueilComparator()));
        paramefaACreer.setCoefficientsEvalCompl(new TreeSet<CoefficientEvaluationComplementaire>(
                new CoefficientEvaluationComplementaireComparator()));
        paramefaACreer.setCoefficientsDisciplineAca(
                new TreeSet<CoefficientDisciplineAcademique>(new CoefficientDisciplineAcademiqueComparator()));

        // Enegistre le nouveau paramefa
        ParametresFormationAccueil paramefaResultant = parametresFormationAccueilDao.creer(paramefaACreer);

        for (LienRangParametresFormationAccueil lienRangParametresFormationAccueil : coefficients) {
            LOG.debug("ajout des coeffcients du rang au paramefa");
            lienRangParametresFormationAccueil.getId().setIdParamefa(paramefaResultant.getId());
            paramefaResultant.ajouteCoefficient(lienRangParametresFormationAccueil);
        }

        for (CoefficientEvaluationComplementaire coefEvalComp : coefficientsEvalCompl) {
            LOG.debug("ajout des coeffcients des �valuations compl�mentaires au paramefa");
            coefEvalComp.getCoefficientEvaluationComplementairePK().setIdParamefa(paramefaResultant.getId());
            paramefaResultant.ajouteCoefficient(coefEvalComp);
        }

        for (CoefficientDisciplineAcademique coeffDispAca : coefficientsDisciplineAca) {
            LOG.debug("ajout des coeffcients des disciplines acad�mique au paramefa");
            coeffDispAca.getCoefficientDisciplineAcademiquePK().setIdParamefa(paramefaResultant.getId());
            paramefaResultant.ajouteCoefficient(coeffDispAca);
        }

        // Par d�faut, on est dans un bar�me de type "Note", on cr�� la liste des
        // coefficients pour les rangs
        creerListeCoefficientRang(paramefaResultant);

        return parametresFormationAccueilDao.maj(paramefaResultant, paramefaResultant.getId());
    }

    /**
     * @param pfa
     *            Le param�tre par formation d'accueil que l'on souhaite supprimer.
     */
    public void supprimer(ParametresFormationAccueil pfa) {

        LOG.debug("Suppression d'un paramefa : " + pfa.toPrettyString());

        // Oblige la relance des OPA pour le paramefa supprim� (au cas o� il s'�tait
        // appliqu�)
        obligerRelanceOpa(pfa);

        // Retire les liens sur ce paramefa pr�sents dans la nomenclature des voeux
        retirerIndicationsVoeuxPourSaisieAvisParamefa(pfa);

        parametresFormationAccueilDao.supprimer(pfa);

        // Maj des indicateurs pour l'avis ParameFA dans la nomenclature des voeux
        majIndicationsVoeuxPourSaisieAvisParamefa(pfa.getFormationAccueil());
    }

    /**
     * M�thode permettant de r�cup�rer tous les param�tres par formation d'accueil
     * utilisant des formations sp�cifiques.
     * 
     * @param formations
     *            la liste des formations utilis�es par les param�tres par formation
     *            d'accueil
     * @return les param�tres par formation d'accueil utilisant les formations en
     *         param�tre
     */
    public List<ParametresFormationAccueil> parametresFormationAccueilAvecFormation(List<Formation> formations) {
        if (formations.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<Filtre> filtresFormations = new ArrayList<>();

            for (Formation formation : formations) {
                filtresFormations.add(Filtre.and(
                        Filtre.equal("formation.id.mnemonique", formation.getId().getMnemonique()),
                        Filtre.equal("formation.id.codeSpecialite", formation.getId().getCodeSpecialite())));
            }

            return parametresFormationAccueilDao.lister(Filtre.or(filtresFormations));
        }
    }

    /**
     * Oblige la relance d'une OPA pour le param�tre par formation d'accueil donn�.
     * 
     * @param pfa
     *            le param�tre par formation d'accueil
     */
    public void obligerRelanceOpa(ParametresFormationAccueil pfa) {
        opaManager.obligerRelanceOpa(pfa);
    }

    /**
     * M�thode permettant de charger la formation.
     * 
     * @param formationPK
     *            identifiant de la formation
     * @return la formation associ�
     */
    public Formation chargerFormation(FormationPK formationPK) {

        Criteria criteria = HibernateUtil.currentSession().createCriteria(Formation.class);
        criteria.add(Restrictions.eq("id.mnemonique", formationPK.getMnemonique()));
        criteria.add(Restrictions.eq("id.codeSpecialite", formationPK.getCodeSpecialite()));
        return (Formation) criteria.uniqueResult();

    }

    /**
     * M�thode permettant de r�cup�rer un tableau listant les param�tres par
     * formations d'accueil.
     * 
     * @param formation
     *            la formation d'accueuil
     * @return un tableau de ParametresFormationAccueil filtrer sur la formation
     */
    public ParametresFormationAccueil[] recuperationFormationAccueil(Formation formation) {
        return parametresFormationAccueilDao.listerPfaFormation(formation);
    }

    /**
     * M�thode permettant d'indiquer si il existe des formation d'accueil en
     * fonction d'une formation.
     * 
     * @param formation
     *            la formation d'accueuil
     * @return vrai si le tableau renvoie au moins un �l�ment
     */
    public boolean existeFormationAccueilAvecFormation(Formation formation) {
        ParametresFormationAccueil[] pfaTab = parametresFormationAccueilDao.listerPfaFormation(formation);
        return pfaTab.length > 0;
    }

    /**
     * M�thode r�cup�rant le param�tre par formation accueil correspondant strictement � la formation et
     * l'enseignement optionnel.
     * 
     * @param formation
     *            la formation d'accueuil
     * @param cleEO
     *            la cl� de gestion pour enseignement optionnel
     * @return le param�tres par formation accueil
     */
    public ParametresFormationAccueil recuperationParamefaExactAvecFormationEtEO(Formation formation,
            String cleEO) {
        return parametresFormationAccueilDao.recuprererParamefaExactAvecFormationEtEO(formation, cleEO);
    }

    /**
     * M�thode r�cup�rant le param�tre par formation accueil correspondant strictement � la formation et
     * l'enseignement optionnel.
     * 
     * @param codeSpecialite
     *            le code sp�cialit� de la formation
     * @param mnemonique
     *            le code mnemonique de la formation
     * @param cleEO
     *            la cl� de gestion pour enseignement optionnel
     * @return le param�tres par formation accueil
     */
    public ParametresFormationAccueil recuperationParamefaExactAvecFormationEtEO(String codeSpecialite,
            String mnemonique, String cleEO) {
        return parametresFormationAccueilDao.recuprererParamefaExactAvecFormationEtEO(codeSpecialite, mnemonique,
                cleEO);
    }

    /**
     * Met � jour un parametre par formation d'accueil.
     * 
     * @param paramefaModifie
     *            le parametre par formation d'accueil modifi�
     */
    public ParametresFormationAccueil mettreAJour(ParametresFormationAccueil paramefaModifie) {

        paramefaModifie.setDateMAJ(new Date());

        // Retire les liens sur ce paramefa pr�sents dans la nomenclature des voeux
        retirerIndicationsVoeuxPourSaisieAvisParamefa(paramefaModifie);

        ParametresFormationAccueil pfa = parametresFormationAccueilDao.maj(paramefaModifie,
                paramefaModifie.getId());

        // Maj des indicateurs pour l'avis ParameFA dans la nomenclature des voeux
        majIndicationsVoeuxPourSaisieAvisParamefa(pfa.getFormationAccueil());

        // Oblige la relance des OPA pour le paramefa modifi� (au cas o� il s'applique)
        obligerRelanceOpa(pfa);

        return pfa;
    }

    /**
     * M�thode permettant de g�n�rer l'objet FormationPK.
     * 
     * @param mnemonique
     *            mnemonique de la formation
     * @param codeSpecialite
     *            codeSpecialite de la formation
     * @return un objet de type FormationPK
     */
    public FormationPK validePK(String codeSpecialite, String mnemonique) {
        if (StringUtils.isBlank(codeSpecialite) && StringUtils.isBlank(mnemonique)) {
            throw new ValidationException("L'identifiant de la formation est invalide");
        }
        return new FormationPK(codeSpecialite, mnemonique);
    }

    /**
     * M�thode permettant de creer la liste des coefficients par rang.
     * 
     * @param pfa
     *            le param�tres par formation
     */
    public void creerListeCoefficientRang(ParametresFormationAccueil pfa) {
        List<Rang> listeRangs = notesManager.listerRangs();
        LienRangParametresFormationAccueil lienRangParametresFormationAccueil = null;
        LienRangParametresFormationAccueilPK id = null;

        for (Rang r : listeRangs) {
            LOG.debug("initilisation du paramefa pour le rang : " + r.getCleGestion());
            id = new LienRangParametresFormationAccueilPK();
            id.setIdRang(r.getId());
            Long idParamefa = pfa.getId();
            id.setIdParamefa(idParamefa);
            lienRangParametresFormationAccueil = new LienRangParametresFormationAccueil(id);
            lienRangParametresFormationAccueil.setRang(r);
            pfa.getCoefficients().add(lienRangParametresFormationAccueil);
        }
    }

    /**
     * M�thode permettant de creer la liste des coefficients par �valuation
     * compl�mentaire.
     * 
     * @param pfa
     *            le param�tres par formation
     */
    public void creerListeCoefficientEvaluationComplementaire(ParametresFormationAccueil pfa) {
        List<EvaluationComplementaire> listeEvalCompl = evaluationComplementaireManager.listerEvalComplementaire();
        CoefficientEvaluationComplementaire coeffEvalCompl = null;
        CoefficientEvaluationComplementairePK coefficientEvaluationComplementairePK = null;

        for (EvaluationComplementaire ec : listeEvalCompl) {
            Long idEvalComplPK = ec.getId();
            LOG.debug("initilisation du paramefa pour l'�valuation compl�mentaire : " + idEvalComplPK);
            coefficientEvaluationComplementairePK = new CoefficientEvaluationComplementairePK();
            coefficientEvaluationComplementairePK.setIdEvalComp(idEvalComplPK);
            Long idParamefa = pfa.getId();
            coefficientEvaluationComplementairePK.setIdParamefa(idParamefa);
            coeffEvalCompl = new CoefficientEvaluationComplementaire(coefficientEvaluationComplementairePK);
            coeffEvalCompl.setEvalComp(ec);
            pfa.getCoefficientsEvalCompl().add(coeffEvalCompl);
        }
    }

    /**
     * M�thode permettant de creer la liste des coefficients par champ Disciplinaire
     * et de redonner la liste des champs disciplinaire.
     * 
     * @param pfa
     *            le param�tres par formation
     * @return la liste des champs disciplinaires
     */
    public List<ChampDisciplinaire> creerListeCoefficientChampDisciplinaire(ParametresFormationAccueil pfa) {
        // Affichage si on a un domaine de sp�cialit�
        DomaineSpecialite dm = domaineSpecialiteManager
                .recuperationDomaineSpecialiteRestreint2ndPro1Cap(pfa.getFormationAccueil());
        // On r�cup�re les champs disciplinaires
        List<ChampDisciplinaire> listeChampDisciplinaire = evaluationDisciplineManager.listerChampDisciplinaire();

        // Si on a un domaine de sp�cialit� diff�rent de null, on charge les disciplines
        // en utilisant les
        // coefficients nationaux fixes
        if (dm != null) {
            LOG.debug("Le chargement sera celui des coefficients nationaux fixes");
            pfa.setDomaineSpecialite(dm);

        } else {
            // sinon on charge les coefficients acad�miques � utiliser
            CoefficientDisciplineAcademique coeffDispAca = null;
            CoefficientDisciplineAcademiquePK coefficientDispAcaPK = null;

            for (ChampDisciplinaire cd : listeChampDisciplinaire) {
                String codeChampDisciplinairePK = cd.getCode();
                LOG.debug("initilisation du paramefa pour le champ disciplinaire : " + codeChampDisciplinairePK);
                coefficientDispAcaPK = new CoefficientDisciplineAcademiquePK();
                coefficientDispAcaPK.setCodeChampDiscipline(codeChampDisciplinairePK);
                Long idParamefa = pfa.getId();
                coefficientDispAcaPK.setIdParamefa(idParamefa);
                coeffDispAca = new CoefficientDisciplineAcademique(coefficientDispAcaPK);
                coeffDispAca.setChampDiscipline(cd);
                pfa.getCoefficientsDisciplineAca().add(coeffDispAca);
            }
        }
        return listeChampDisciplinaire;
    }

    /**
     * M�thode permettant de remettre � z�ro les coefficients li�es au �valuation.
     * 
     * @param pfa
     *            le param�tre par formation
     */
    public void resetListeCoefficientEvaluation(ParametresFormationAccueil pfa) {
        // Mise � jour des Champs disciplinaire
        Set<CoefficientDisciplineAcademique> listeCoeffAca = pfa.getCoefficientsDisciplineAca();
        listeCoeffAca.clear();

        // Mise � jour des �valuations copmpl�mentaires
        Set<CoefficientEvaluationComplementaire> listeCoeffEvalCompl = pfa.getCoefficientsEvalCompl();
        listeCoeffEvalCompl.clear();

        // On effectue un flush pour s'assurer que les donnees associees aux ?valuations
        // actuelles sont purgees
        // dans la session Hibernate suite au 'clear' sur les collections.
        HibernateUtil.currentSession().flush();

        // Mise � jour des objets
        pfa.setCoefficientsDisciplineAca(listeCoeffAca);
        pfa.setCoefficientsEvalCompl(listeCoeffEvalCompl);
    }

    /**
     * M�thode permettant de remettre � z�ro les coefficients li�es au note.
     * 
     * @param pfa
     *            le param�tre par formation
     */
    public void resetListeCoefficientNote(ParametresFormationAccueil pfa) {
        // Mise � jour des Champs disciplinaire
        Set<LienRangParametresFormationAccueil> listeCoeff = pfa.getCoefficients();
        listeCoeff.clear();

        // On effectue un flush pour s'assurer que les donnees associees aux ?valuations
        // actuelles sont purgees
        // dans la session Hibernate suite au 'clear' sur les collections.
        HibernateUtil.currentSession().flush();

        // Mise � jour des objets
        pfa.setCoefficients(listeCoeff);

    }

    /**
     * M�thode permettant de lister la liste des param�tres par formation d'accueil
     * du mode bareme Note : les offres de formation associ� doivent autoris� la
     * saisie des �l�ves de 2nd.
     * 
     * @return la liste des paramefa de type Note ne respectant pas la r�gle
     */
    public List<ParametresFormationAccueil> listeCoherenceModeBaremeNote() {
        LOG.debug("Mode de calcul Note");
        // Liste contenant les incoherences pour les offres de formations
        List<ParametresFormationAccueil> listeIncoherent = new ArrayList<>();

        // On parcourt la liste du tour courant de la GV_PILE
        List<Object[]> listePFAVoeuTourCourant = pileDao.listeVoeuxParametresFormationAccueilPourTour(
                Short.valueOf(parametreManager.getNumeroTourCourant()),
                ParametresFormationAccueil.MODE_BAREME_NOTE);
        LOG.debug("La requ�te renvoit " + listePFAVoeuTourCourant.size() + " r�sultats");

        // Si la liste ne renvoi rien, il n'y a pas de paramefa � v�rifier
        if (!listePFAVoeuTourCourant.isEmpty()) {

            // Cr�ation d'un dictionnaire ayant pour cl� le paramefa et pour valeur la liste
            // des codes d'offres de
            // formations
            HashMap<ParametresFormationAccueil, List<String>> dictParamefaOffreDeFormation = new HashMap<>();

            // Cr�ation de la liste contenant les offres de formations pour un paramefa
            List<String> listOffreFormation = null;

            // -- Parcours pour cr�er un dictionnaire ayant pour cl� un paramefa et en
            // valeur la liste des codes de
            // voeu si ils sont utilis� par un �l�ve et sont de type "Bar�me avec
            // note/�valuation"
            for (Object[] pfaVoeu : listePFAVoeuTourCourant) {

                // R�cup�ration du paramefa
                ParametresFormationAccueil pfa = (ParametresFormationAccueil) pfaVoeu[0];

                Long idpfa = pfa.getId();
                Voeu offreFormation = (Voeu) pfaVoeu[1];
                String codeOffreFormation = offreFormation.getCode();
                LOG.debug("ID PFA:" + idpfa + "---------- Code Offre de Formation :" + codeOffreFormation);

                // r�cup�ration de la liste des codes des offres de formations
                List<String> listCodeOffreFormation = dictParamefaOffreDeFormation.get(pfa);

                // Si l'identifiant n'est pas encore connu dans le dictionnaire on le cr��
                if (listCodeOffreFormation == null) {
                    listOffreFormation = new ArrayList<>();
                    dictParamefaOffreDeFormation.put(pfa, listOffreFormation);
                }

                // on ajoute l'offre de formation � la liste
                listOffreFormation.add(codeOffreFormation);
            }

            // Parcours de la liste pour v�rifier la r�gle
            Set<ParametresFormationAccueil> listPfa = dictParamefaOffreDeFormation.keySet();
            LOG.debug("La liste de paramefa poss�de " + listPfa.size());
            for (ParametresFormationAccueil pfa : listPfa) {
                LOG.debug("Ex�cution sur les offres de formations de l'id PARAMEFA:" + pfa.getId());
                List<String> listeCodeVoeu = dictParamefaOffreDeFormation.get(pfa);

                // correspond � la r�gle
                // "toutes ses offres de formation associ�es doivent autoriser la saisie pour
                // les �l�ves du palier
                // 2NDE"
                boolean resultat = voeuManager.voeuxCoherenceNote(listeCodeVoeu);
                if (!resultat) {
                    listeIncoherent.add(pfa);
                }

            }
            LOG.debug("Il y a " + listeIncoherent.size()
                    + " �l�ments incoh�rent entre les paramefa et les offres de formations");
        }
        return listeIncoherent;
    }

    /**
     * M�thode permettant de lister la liste des param�tres par formation d'accueil
     * du mode bareme Evaluation : au moins une offre de formation doit permettre la
     * saisie au troisi�me.
     * 
     * @return la liste des paramefa de type Evaluation ne respectant pas la r�gle
     */
    public List<ParametresFormationAccueil> listeCoherenceModeBaremeEvaluation() {
        LOG.debug("Mode de calcul Evaluation");

        // Liste contenant les incoherences pour les offres de formations
        List<ParametresFormationAccueil> listeIncoherent = new ArrayList<>();

        // On parcourt la liste du tour courant de la GV_PILE
        List<Object[]> listePFAVoeuTourCourant = pileDao.listeVoeuxParametresFormationAccueilPourTour(
                Short.valueOf(parametreManager.getNumeroTourCourant()),
                ParametresFormationAccueil.MODE_BAREME_EVALUATION);

        LOG.debug("La requ�te renvoit " + listePFAVoeuTourCourant.size() + " r�sultats");

        // Si la liste ne renvoi rien, il n'y a pas de paramefa � v�rifier
        if (!listePFAVoeuTourCourant.isEmpty()) {

            // Cr�ation d'un dictionnaire ayant pour cl� le paramefa et pour valeur la liste
            // des code d'offres de
            // formations
            HashMap<ParametresFormationAccueil, List<String>> dictParamefaOffreDeFormation = new HashMap<>();

            List<String> listOffreFormation = null;

            // -- Parcours pour cr�er un dictionnaire ayant pour cl� un paramefa et en
            // valeur la liste des codes de
            // voeu si ils sont utilis� par un �l�ve et sont de type "Bar�me avec
            // note/�valuation"
            for (Object[] pfaVoeu : listePFAVoeuTourCourant) {

                // R�cup�ration du paramefa
                ParametresFormationAccueil pfa = (ParametresFormationAccueil) pfaVoeu[0];

                Long idpfa = pfa.getId();
                Voeu offreFormation = (Voeu) pfaVoeu[1];
                String codeOffreFormation = offreFormation.getCode();
                LOG.debug("ID PFA:" + idpfa + "---------- Code Offre de Formation :" + codeOffreFormation);

                // r�cup�ration de la liste des codes des offres de formations
                List<String> listCodeOffreFormation = dictParamefaOffreDeFormation.get(pfa);

                // Si l'identifiant n'est pas encore connu dans le dictionnaire on le cr��
                if (listCodeOffreFormation == null) {
                    listOffreFormation = new ArrayList<>();
                    dictParamefaOffreDeFormation.put(pfa, listOffreFormation);
                }

                // on ajoute l'offre de formation � la liste
                listOffreFormation.add(codeOffreFormation);
            }

            // parcours de la liste pour voir si elle correspond � la r�gle d�finie
            Set<ParametresFormationAccueil> listPfa = dictParamefaOffreDeFormation.keySet();
            LOG.debug("La liste de paramefa poss�de " + listPfa.size());
            for (ParametresFormationAccueil pfa : listPfa) {
                LOG.debug("Ex�cution sur les offres de formations de l'id PARAMEFA:" + pfa.getId());

                List<String> listeCodeVoeu = dictParamefaOffreDeFormation.get(pfa);

                // correspond � la r�gle "au moins une de ses offres de formation associ�es
                // doivent autoriser la saisie pour les �l�ves du palier 3EME"
                boolean resultat = voeuManager.voeuxCoherenceEvaluation(listeCodeVoeu);

                if (!resultat) {
                    listeIncoherent.add(pfa);
                }

            }
            LOG.debug("Il y a " + listeIncoherent.size()
                    + " �l�ments incoh�rent entre les paramefa et les offres de formations");
        }

        return listeIncoherent;
    }

    /**
     * M�thode permettant de collecter une table des offres de formation
     * incompatibles avec le mode de calcul du bar�me pour chaque Paramefa.
     * 
     * @param listePFAVoeuTourCourant
     *            la liste des param�tres par formation accueil et des offres de
     *            formations
     * 
     * @return la liste des paramefa respectant la r�gle d�fini dans les
     *         nomenclatures : RG-AFF-LYC-NOM-AUDIT-22
     */
    public List<ParametresFormationAccueil> listeIncoherenceModeBareme(List<Object[]> listePFAVoeuTourCourant) {
        LOG.debug("Coherence mode de calcul du bareme");
        // Liste contenant les incoherences pour les offres de formations
        List<ParametresFormationAccueil> listeIncoherent = new ArrayList<>();

        // Si la liste ne renvoit rien, il n'y a pas de paramefa � v�rifier
        if (!listePFAVoeuTourCourant.isEmpty()) {

            // Cr�ation d'un dictionnaire ayant pour cl� le paramefa et pour valeur la liste
            // des codes d'offres de
            // formations
            HashMap<ParametresFormationAccueil, List<String>> dictParamefaOffreDeFormation = new HashMap<>();

            // Cr�ation de la liste contenant les offres de formations pour un paramefa
            List<String> listOffreFormation = null;

            // -- Parcours pour cr�er un dictionnaire ayant pour cl� un paramefa et en
            // valeur la liste des codes de
            // voeu si ils sont utilis� par un �l�ve et sont de type "Bar�me avec
            // note/�valuation"
            for (Object[] pfaVoeu : listePFAVoeuTourCourant) {

                // R�cup�ration du paramefa
                ParametresFormationAccueil pfa = (ParametresFormationAccueil) pfaVoeu[0];

                Long idpfa = pfa.getId();
                Voeu offreFormation = (Voeu) pfaVoeu[1];
                String codeOffreFormation = offreFormation.getCode();
                LOG.debug("ID PFA:" + idpfa + "---------- Code Offre de Formation :" + codeOffreFormation);

                // r�cup�ration de la liste des codes des offres de formations
                List<String> listCodeOffreFormation = dictParamefaOffreDeFormation.get(pfa);

                // Si l'identifiant n'est pas encore connu dans le dictionnaire on le cr��
                if (listCodeOffreFormation == null) {
                    listOffreFormation = new ArrayList<>();
                    dictParamefaOffreDeFormation.put(pfa, listOffreFormation);
                }

                // on ajoute l'offre de formation � la liste
                listOffreFormation.add(codeOffreFormation);
            }

            // Parcours de la liste pour v�rifier la r�gle
            Set<ParametresFormationAccueil> listPfa = dictParamefaOffreDeFormation.keySet();
            LOG.debug("La liste de paramefa poss�de " + listPfa.size());
            for (ParametresFormationAccueil pfa : listPfa) {
                LOG.debug("Ex�cution sur les offres de formations de l'id PARAMEFA:" + pfa.getId());
                List<String> listeCodeVoeu = dictParamefaOffreDeFormation.get(pfa);

                // R�cup�ration du mode de bar�me du param�tre par formation accueil
                String modeBareme = pfa.getModeBareme();

                if (ParametresFormationAccueil.MODE_BAREME_EVALUATION.equals(modeBareme)) {

                    // correspond � la r�gle "au moins une de ses offres de formation associ�es
                    // doivent autoriser la saisie pour les �l�ves du palier 3EME"
                    boolean resultat = voeuManager.voeuxCoherenceEvaluation(listeCodeVoeu);

                    if (!resultat) {
                        listeIncoherent.add(pfa);
                    }
                } else if (ParametresFormationAccueil.MODE_BAREME_NOTE.equals(modeBareme)) {

                    // correspond � la r�gle
                    // "toutes ses offres de formation associ�es doivent autoriser la saisie pour
                    // les �l�ves du
                    // palier 2NDE"
                    boolean resultat = voeuManager.voeuxCoherenceNote(listeCodeVoeu);

                    if (!resultat) {
                        listeIncoherent.add(pfa);
                    }
                } else {
                    throw new DaoException("Le mode de bar�me " + modeBareme + " est " + MODE_BAREME_NONDEFINI);
                }
            }
            LOG.debug("Il y a " + listeIncoherent.size()
                    + " �l�ments incoh�rent entre les paramefa et les offres de formations");
        }

        return listeIncoherent;
    }

    /**
     * M�thode permettant de r�cup�rer la valeur d'un coefficient d'apr�s ces
     * param�tres.
     * 
     * @param idParamefa
     *            l'identifant du param�tre par formation accueil
     * @param idEvalCompl
     *            l'identifiant de l'�valuation compl�mentaire
     * @return le coefficient associ� � l'�valuation compl�mentaire et la param�tre
     *         par formation accueuil
     */
    public int recuperationCoeffEvalCompl(Long idParamefa, Long idEvalCompl) {

        Criteria criteria = HibernateUtil.currentSession()
                .createCriteria(CoefficientEvaluationComplementaire.class);
        criteria.add(Restrictions.eq("coefficientEvaluationComplementairePK.idEvalComp", idEvalCompl));
        criteria.add(Restrictions.eq("coefficientEvaluationComplementairePK.idParamefa", idParamefa));
        return ((Number) criteria.uniqueResult()).intValue();

    }

    /**
     * M�thode permettant de r�cup�rer la liste des param�tres par formation accueil
     * n'ayant pas de coefficients ou ayant des coefficients hors de l'intervalle.
     * 
     * @param listePFAVoeuTourCourant
     *            la liste des param�tres par formation accueil et des offres de
     *            formations
     * 
     * @return la liste des param�tres par formation accueil n'ayant pas de
     *         coefficient ou des coefficients hors intervalle
     */
    public List<ParametresFormationAccueil> listePfaSansCoefficientHorsIntervalle(
            List<Object[]> listePFAVoeuTourCourant) {

        LOG.debug("Param�tres par formation accueil sans Coefficient");
        // Liste contenant les incoherences sur les coefficients pour les offres de
        // formations
        List<ParametresFormationAccueil> listeSsCoeff = new ArrayList<>();

        // Si la liste ne renvoit rien, il n'y a pas de paramefa � v�rifier
        if (!listePFAVoeuTourCourant.isEmpty()) {

            // Cr�ation d'une liste d'id du paramefa v�rifiant les conditions suivantes : de
            // type
            // "bar�me avec note/�valuation"
            List<ParametresFormationAccueil> listeParamefaBaremeAvNoteEval = new ArrayList<>();

            // -- Parcours pour cr�er une liste de paramefa
            // si ils sont de type "Bar�me avec note/�valuation"
            for (Object[] pfaVoeu : listePFAVoeuTourCourant) {

                // R�cup�ration du paramefa
                ParametresFormationAccueil pfa = (ParametresFormationAccueil) pfaVoeu[0];
                Voeu offreFormation = (Voeu) pfaVoeu[1];
                LOG.debug("ID PFA:" + pfa.getId() + "---------- Code Offre de Formation :"
                        + offreFormation.getCode());

                // On v�rifie que le voeu soit du mode bar�me avec note/�valuation
                // on ajoute le pfa � la liste s'il n'est pas d�j� ajout�
                if (voeuManager.testModeBaremeNoteEvaluation(offreFormation)
                        && !listeParamefaBaremeAvNoteEval.contains(pfa)) {
                    listeParamefaBaremeAvNoteEval.add(pfa);
                }

            }

            // parcours de la liste pour voir si elle correspond � la r�gle d�finie
            LOG.debug("La liste de paramefa poss�de " + listeParamefaBaremeAvNoteEval.size());
            for (ParametresFormationAccueil pfa : listeParamefaBaremeAvNoteEval) {
                Long idPfa = pfa.getId();
                LOG.debug("Ex�cution sur les offres de formations de l'id PARAMEFA:" + idPfa);

                // V�rification si le param�tre par formation ne poss�de pas de coefficient
                // utilisation de la m�thode qui v�rifie que pour un id de paramefa, on a un
                // coefficient
                if (!(existeCoefficientAssocieRespectantIntervalle(pfa))) {
                    listeSsCoeff.add(pfa);
                }

            }
            LOG.debug("Il y a " + listeSsCoeff.size() + " �l�ments incoh�rent au niveau des sur le paramefa");
        }
        return listeSsCoeff;
    }

    /**
     * M�thode permettant de savoir si un coefficient est associ�.
     * 
     * @param pfa
     *            le param�tre par formation accueil
     * @return vrai si un coefficient est associ� (existe dans la table de base de
     *         donn�e) et qu'il est dans l'intervalle d�fini par le coefficient
     */
    @SuppressWarnings("unchecked")
    private boolean existeCoefficientAssocieRespectantIntervalle(ParametresFormationAccueil pfa) {

        // Initialisation du param�tre
        boolean resultatExisteCoefficient = true;
        boolean resultatHorsIntervalle = false;

        // R�cup�ration de l'identifiant
        Long idPfa = pfa.getId();

        // R�cup�ration du mode de bar�me du param�tre par formation accueil
        String modeBareme = pfa.getModeBareme();

        if (ParametresFormationAccueil.MODE_BAREME_EVALUATION.equals(modeBareme)) {

            // On v�rifie s'il existe des Evaluations compl�mentaires
            if (evaluationComplementaireManager.getNombreElements() > 0) {
                Criteria criteria = HibernateUtil.currentSession()
                        .createCriteria(CoefficientEvaluationComplementaire.class);
                criteria.add(Restrictions.eq("coefficientEvaluationComplementairePK.idParamefa", idPfa));
                criteria.setProjection(Projections.property("valCoef"));
                List<Integer> listeValCoef = criteria.list();
                resultatExisteCoefficient = CollectionUtils.isNotEmpty(listeValCoef);
                // On v�rifie que la liste se situe dans l'intervalle si le coefficient existe
                if (resultatExisteCoefficient) {

                    for (Integer valCoef : listeValCoef) {
                        if (valCoef != 0 && valCoef != 1) {
                            resultatHorsIntervalle = true;
                        }
                    }
                }
            }

            // R�cup�ration du domaine de sp�cialit�
            DomaineSpecialite dm = domaineSpecialiteManager
                    .recuperationDomaineSpecialiteRestreint2ndPro1Cap(pfa.getFormationAccueil());
            // Si dm est null on doit utiliser les coefficients acad�miques, et non les
            // nationaux
            if (dm == null) {
                Criteria criteria = HibernateUtil.currentSession()
                        .createCriteria(CoefficientDisciplineAcademique.class);
                criteria.add(Restrictions.eq("coefficientDisciplineAcademiquePK.idParamefa", idPfa));
                criteria.setProjection(Projections.property("valCoef"));
                List<Integer> listeValCoef = criteria.list();
                boolean resultatExisteCoefficientDiscipline = CollectionUtils.isNotEmpty(listeValCoef);
                resultatExisteCoefficient &= resultatExisteCoefficientDiscipline;
                // On v�rifie que la liste se situe dans l'intervalle si le coefficient existe
                if (resultatExisteCoefficientDiscipline) {

                    for (Integer valCoef : listeValCoef) {
                        if (!(valCoef >= 0 && valCoef <= Discipline.VALEUR_MAXIMUM)) {
                            resultatHorsIntervalle |= true;
                        }
                    }
                }
            }

        } else if (ParametresFormationAccueil.MODE_BAREME_NOTE.equals(modeBareme)) {
            Criteria criteria = HibernateUtil.currentSession()
                    .createCriteria(LienRangParametresFormationAccueil.class);
            criteria.add(Restrictions.eq("id.idParamefa", idPfa));
            criteria.setProjection(Projections.property("valCoef"));
            List<Integer> listeValCoef = criteria.list();
            resultatExisteCoefficient = CollectionUtils.isNotEmpty(listeValCoef);

            // On v�rifie que la liste se situe dans l'intervalle si le coefficient existe
            if (resultatExisteCoefficient) {

                for (Integer valCoef : listeValCoef) {
                    if (!(valCoef >= 0 && valCoef <= Rang.VALEUR_COEFFICIENT_MAXIMUM)) {
                        resultatHorsIntervalle |= true;
                    }
                }
            }

        } else {
            throw new DaoException(MODE_BAREME_NONDEFINI);
        }
        return resultatExisteCoefficient && !resultatHorsIntervalle;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des param�tres par formation accueil
     * et des offres de formations associ� � l'op�ration programm�e.
     * 
     * @param idopa
     *            l'identifiant des opa
     * 
     * @return la liste des param�tres par formation accueil
     *         {(ParametresFormationAccueil) pfaVoeu[0];} et des offres de
     *         formations {(Voeu) pfaVoeu[1]} associ�
     */
    public List<Object[]> listePfaOffreFormation(Long idopa) {

        LOG.debug(
                "R�cup�ration de la liste du couple 'param�tre par formation accueuil' et 'offre de formation' ");

        // On parcourt la liste du tour courant de la GV_PILE
        List<Object[]> listePFAVoeuTourCourant = pileDao.listeVoeuxParametresFormationAccueilPourOpa(idopa);

        LOG.debug("La requ�te renvoit " + listePFAVoeuTourCourant.size() + " r�sultats");
        return listePFAVoeuTourCourant;
    }

    /**
     * M�thode permettant de r�cup�rer la liste des param�tres par formation accueil
     * et des offres de formations associ� au tour courant.
     * 
     * 
     * @return la liste des param�tres par formation accueil
     *         {(ParametresFormationAccueil) pfaVoeu[0];} et des offres de
     *         formations {(Voeu) pfaVoeu[1]} associ�
     */
    public List<Object[]> listePfaOffreFormationTourCourant() {

        LOG.debug(
                "R�cup�ration de la liste du couple 'param�tre par formation accueuil' et 'offre de formation' ");
        // On parcourt la liste du tour courant de la GV_PILE
        List<Object[]> listePFAVoeuTourCourant = pileDao.listeVoeuxParametresFormationAccueilPourTour(
                campagneAffectationManager.getNumeroTourCourant(), null);

        LOG.debug("La requ�te renvoit " + listePFAVoeuTourCourant.size() + " r�sultats");
        return listePFAVoeuTourCourant;
    }

    /**
     * Fournit les coefficients pour les champs disciplinaires.
     * 
     * @param pfa
     *            le param�tre par formation d'accueil dont on souhaite connaitre
     *            les coefficients
     * @return la map des coefficients par champ disciplinaire
     */
    public Map<ChampDisciplinaire, Integer> coefficientChampDisciplinaireMap(ParametresFormationAccueil pfa) {
        if (ParametresFormationAccueil.MODE_BAREME_EVALUATION.equals(pfa.getModeBareme())) {

            DomaineSpecialite domaineSpecialite = domaineSpecialiteManager
                    .recuperationDomaineSpecialiteRestreint2ndPro1Cap(pfa.getFormationAccueil());

            Map<ChampDisciplinaire, Integer> coefficientsDiscipline;

            if (domaineSpecialite != null) {
                coefficientsDiscipline = evaluationDisciplineManager
                        .recuperationMapCoefficientDisciplineSpecialite(domaineSpecialite.getCode());

            } else {
                coefficientsDiscipline = new TreeMap<>();

                for (CoefficientDisciplineAcademique coefDisAca : pfa.getCoefficientsDisciplineAca()) {
                    coefficientsDiscipline.put(coefDisAca.getChampDiscipline(), coefDisAca.getValCoef());
                }
            }
            return coefficientsDiscipline;
        } else {
            return null;
        }
    }

    /**
     * Cr�� un nouveau param�tre par formation d'accueil.
     * 
     * @param pfa
     *            le paramefa � cr�er
     * @return le paramefa g�n�r�
     */
    public ParametresFormationAccueil creer(ParametresFormationAccueil pfa) {
        validationMetierEE(pfa, new ArrayList<>());
        pfa = creerParDefaut(pfa);

        // Mise � jour des indicateurs pour l'avis ParameFA au niveau de la pile
        majIndicationsVoeuxPourSaisieAvisParamefa(pfa.getFormationAccueil());

        // Oblige la relance des OPA pour le paramefa supprim� (au cas o� il s'applique)
        obligerRelanceOpa(pfa);

        return pfa;
    }
}
