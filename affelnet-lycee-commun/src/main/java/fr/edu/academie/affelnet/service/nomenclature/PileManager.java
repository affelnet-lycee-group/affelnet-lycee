/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.nomenclature.PileDao;
import fr.edu.academie.affelnet.domain.nomenclature.Pile;
import fr.edu.academie.affelnet.domain.nomenclature.PilePK;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.CampagneAffectationManager;

/**
 * La classe g�rant les piles.
 */
@Service
public class PileManager extends AbstractManager {

    /**
     * Le dao utilis� pour les piles.
     */
    private PileDao pileDao;

    /**
     * Le manager pour les campagnes.
     */
    private CampagneAffectationManager campagneManager;

    /**
     * @param pileDao
     *            the pileDao to set
     */
    @Autowired
    public void setPileDao(PileDao pileDao) {
        this.pileDao = pileDao;
    }

    /**
     * @param campagneManager
     *            the campagneManager to set
     */
    @Autowired
    public void setCampagneManager(CampagneAffectationManager campagneManager) {
        this.campagneManager = campagneManager;
    }

    /**
     * Charge une pile depuis la base.
     * 
     * @param clef
     *            La clef de la pile
     * @return La pile du voeu, ou null si elle n'existe pas
     */
    public Pile charger(PilePK clef) {
        if (pileDao.existe(clef)) {
            return pileDao.charger(clef);
        } else {
            return null;
        }
    }

    /**
     * M�thode de chargement de la pile du tour courant.
     * 
     * @param codeVoeu
     *            le code d'une offre de formation
     * @return la pile correspondant au voeu pour le tour courant, ou null si elle n'existe pas
     */
    public Pile charger(String codeVoeu) {
        PilePK clef = new PilePK(campagneManager.getNumeroTourCourant(), codeVoeu);
        if (pileDao.existe(clef)) {
            return pileDao.charger(clef);
        } else {
            return null;
        }
    }

    /**
     * Met � jour la pile.
     * 
     * @param pile
     *            la pile modifi�e
     * @param clef
     *            la clef de la pile
     * @return La pile modifi�e.
     */
    public Pile mettreAJour(Pile pile, PilePK clef) {
        return pileDao.maj(pile, clef);
    }

    /**
     * Met � jour la pile.
     * 
     * @param pile
     *            Le pile modifi�e.
     * @return La pile modifi�e.
     */
    public Pile mettreAJour(Pile pile) {
        return mettreAJour(pile, pile.getId());
    }

    /**
     * Supprime une pile de la base de donn�es.
     * 
     * @param pile
     *            La pile � supprimer.
     */
    public void supprimer(Pile pile) {
        pileDao.supprimer(pile);
    }

    /**
     * Ajoute une pile en base de donn�es.
     * 
     * @param pile
     *            La pile � ajouter.
     * @return La pile qui vient d'�tre cr��e.
     */
    public Pile creer(Pile pile) {
        return pileDao.creer(pile);
    }

}
