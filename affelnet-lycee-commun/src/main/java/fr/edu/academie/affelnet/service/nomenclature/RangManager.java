/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.RangDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.nomenclature.Rang;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.utils.CacheUtils;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/**
 * Manager pour les rangs.
 */
@Service
public class RangManager extends AbstractManager {
    /** Liste des rangs obligatoires. */
    private static final CacheUtils.Init CACHE_RANGS_OBLIGATOIRES = new CacheUtils.Init() {
        public Object getValue() {
            LOG.debug("Initialisation du cache des rangs obligatoires");
            RangDao dao = SpringUtils.getBean(RangDao.class);
            // liste des filtres
            List<Filtre> listFiltres = new ArrayList<Filtre>();
            listFiltres.add(Filtre.equal("autorisationNonNote", Flag.NON));
            return dao.lister(listFiltres);
        }
    };

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(RangManager.class);

    /**
     * Le dao utilis� pour les rangs.
     */
    private RangDao rangDao;

    /**
     * @param rangDao
     *            the rangDao to set
     */
    @Autowired
    public void setRangDao(RangDao rangDao) {
        this.rangDao = rangDao;
    }

    /**
     * M�thode de r�cup�ration de la liste des rangs.
     * 
     * @return les rangs utilis�s
     */
    public List<Rang> lister() {
        return rangDao.lister();
    }

    /**
     * @return vrai si les rangs sont correctement incr�ment�s
     */
    public boolean verifierIncrementationDesRangs() {
        LOG.debug("V�rification des rangs");
        List<Rang> listRang = rangDao.lister();
        int j = 1;
        for (int i = 0; i < listRang.size(); i++, j++) {
            if (listRang.get(i).getValeur() != j) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return vrai s'il y a au moins un rang pour lequel la note est obligatoire, sinon faux
     */
    public boolean verifierPresenceRangAvecNoteObligatoire() {
        LOG.debug("V�rification des valeurs non not�es");
        Filtre filtreNonNotee = Filtre.equal("autorisationNonNote", Flag.NON);
        return (rangDao.getNombreElements(filtreNonNotee) > 0);
    }

    /**
     * M�thode permettant de savoir si il y a au moins une occurrence dans la table GN_RANG :
     * respectant ces contraintes FL_NN='N' et CO_CHAMP_DIS not null.
     * 
     * @return vrai si il y a au moins un r�sultat dans le tableau de la requ�te
     */
    public boolean verifierExistenceAuMoinsUneOccurenceRangObligatoireAcChampDisciplinaire() {
        LOG.debug("V�rification existence d'au moins un rangs obligatoire avec champ disciplinaire");
        // liste des filtres
        List<Filtre> listFiltres = new ArrayList<Filtre>();
        listFiltres.add(Filtre.equal("autorisationNonNote", Flag.NON));
        listFiltres.add(Filtre.isNotNull("champDisciplinaire"));
        return rangDao.getNombreElements(listFiltres) > 0;
    }

    /**
     * Fournit via le cache la liste des rangs obligatoires.
     * 
     * @return la liste des rangs obligatoires
     */
    @SuppressWarnings("unchecked")
    public List<Rang> rangObligatoire() {
        return (List<Rang>) CacheUtils.getValue(CACHE_RANGS_OBLIGATOIRES);
    }
}
