/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import fr.edu.academie.affelnet.dao.hibernate.pam.comparators.RapprochEtabComparator;
import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.RapprochEtabDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.FormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.FormationOrigine;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.RapprochEtab;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Predicate;

/**
 * Manager pour la gestion des <code>RapprochEtab</code>.
 */
@Service
public class RapprochEtabManager extends AbstractManager {

    /** Logger de la classe. */
    private static final Log LOG = LogFactory.getLog(RapprochEtabManager.class);

    /**
     * Le dao utilis� pour les rapprochements entre �tablissements.
     */
    private RapprochEtabDao rapprochEtabDao;

    /**
     * Le manager utilis� pour la notion d'ouverture des nomenclatures en fonction
     * du type origine ou accueil.
     */
    private OuvrableManager ouvrableManager;

    /**
     * Le manager utilis� pour les formations-mati�res.
     */
    private FormationMatiereManager formationMatiereManager;

    /** Le gestionnaire d'OPA. */
    private OperationProgrammeeAffectationManager operationProgrammeeAffectationManager;

    /**
     * @param rapprochEtabDao
     *            the rapprochEtabDao to set
     */
    @Autowired
    public void setRapprochEtabDao(RapprochEtabDao rapprochEtabDao) {
        this.rapprochEtabDao = rapprochEtabDao;
    }

    /**
     * @param ouvrableManager
     *            the ouvrableManager to set
     */
    @Autowired
    public void setOuvrableManager(OuvrableManager ouvrableManager) {
        this.ouvrableManager = ouvrableManager;
    }

    /**
     * @param formationMatiereManager
     *            the formationMatiereManager to set
     */
    @Autowired
    public void setFormationMatiereManager(FormationMatiereManager formationMatiereManager) {
        this.formationMatiereManager = formationMatiereManager;
    }

    /**
     * @param operationProgrammeeAffectationManager
     *            le gestionnaire d'OPA
     */
    @Autowired
    public void setOperationProgrammeeAffectationManager(
            OperationProgrammeeAffectationManager operationProgrammeeAffectationManager) {
        this.operationProgrammeeAffectationManager = operationProgrammeeAffectationManager;
    }

    /**
     * Cr�e un rapprochement entre �tablissement.
     * 
     * @param rapprochEtab
     *            le rapprochement entre �tablissement � cr�er
     * @param listeErreursValidation
     *            liste des erreurs de validation � compl�ter
     * @return le rapprochement entre �tablissement cr��
     */
    public RapprochEtab creer(RapprochEtab rapprochEtab, List<String> listeErreursValidation) {

        LOG.debug("Cr�ation d'un rapprochement entre �tablissement : " + rapprochEtab);

        validationMetier(rapprochEtab, listeErreursValidation);
        rapprochEtab.setDateCreation(new Date());

        final RapprochEtab rapprochementEtablissementCree = rapprochEtabDao.creer(rapprochEtab);
        obligerRelanceOpa(rapprochementEtablissementCree);
        return rapprochementEtablissementCree;
    }

    /**
     * Charge un rapprochement entre �tablissement.
     * 
     * @param id
     *            l'identifiant du rapprochement entre �tablissements � charger.
     * @return un rapprochement entre �tablissement
     */
    public RapprochEtab charger(Long id) {
        return rapprochEtabDao.charger(id);
    }

    /**
     * Met � jour un rapprochement entre �tablissement.
     * 
     * @param rapprochEtab
     *            un rapprochement entre �tablissement
     * @param ancienId
     *            ancien identifiant du rapprochement
     * @param listeErreursValidation
     *            liste des erreurs de validation � compl�ter
     * @return un rapprochement entre �tablissement
     */
    public RapprochEtab mettreAJour(RapprochEtab rapprochEtab, Long ancienId,
            List<String> listeErreursValidation) {

        RapprochEtab ancienRapprochEtab = rapprochEtabDao.charger(ancienId);
        LOG.debug("Mise � jour d'un rapprochement entre �tablissement - ancien : " + ancienRapprochEtab);

        obligerRelanceOpa(ancienRapprochEtab);

        validationMetier(rapprochEtab, listeErreursValidation);
        rapprochEtab.setDateMAJ(new Date());
        RapprochEtab rapprochEtabModifie = rapprochEtabDao.maj(rapprochEtab, ancienId);
        LOG.debug("Mise � jour d'un rapprochement entre �tablissement - nouveau : " + rapprochEtabModifie);

        obligerRelanceOpa(rapprochEtabModifie);

        return rapprochEtabModifie;
    }

    /**
     * Supprime un rapprochement entre �tablissement.
     * 
     * @param rapprochEtab
     *            le rapprochement entre �tablissement � supprimer
     */
    public void supprimer(RapprochEtab rapprochEtab) {

        LOG.debug("Suppression d'un rapprochement entre �tablissement : " + rapprochEtab);
        obligerRelanceOpa(rapprochEtab);
        rapprochEtabDao.supprimer(rapprochEtab);
    }

    /**
     * Applique les r�gles m�tiers sur l'objet et rempli la liste des erreurs de
     * validation si l'objet n'est pas valide.
     * 
     * @param rapprochEtab
     *            l'objet � valider
     * @param listeErreursValidation
     *            la liste des �ventuelles erreurs de validation
     */
    public void validationMetier(RapprochEtab rapprochEtab, List<String> listeErreursValidation) {
        // l'�tablissement d'origine saisi doit r�pondre � la notion d'origine
        Etablissement etabO = rapprochEtab.getEtablissementOrigine();
        if (etabO != null && !ouvrableManager.isOrigine(etabO)) {
            listeErreursValidation.add("L'�tablissement d'origine " + etabO.getId() + " n'est pas ouvert");
        }

        // l'�tablissement d'accueil saisi doit r�pondre � la notion d'accueil
        Etablissement etabA = rapprochEtab.getEtablissementAccueil();
        if(etabA == null) {
            listeErreursValidation.add("L'�tablissement d'accueil n'existe pas");
        } else if (!ouvrableManager.isAccueil(etabA)) {
            listeErreursValidation.add("L'�tablissement d'accueil " + etabA.getId() + " n'est pas ouvert");
        }

        rapprochEtab.setEOGeneriqusAutorise(true);
        // la formation d'accueil et l'enseignement optionnel doivent r�pondre � la notion d'accueil
        formationMatiereManager.validationOuvertureEO((FormationAccueil) rapprochEtab, listeErreursValidation);
        // la formation d'origine et l'enseignement optionnel doivent r�pondre � la notion d'origine
        formationMatiereManager.validationOuvertureEO((FormationOrigine) rapprochEtab, listeErreursValidation);

        // validation de la formation d'accueil avec l'enseignements optionnel
        formationMatiereManager.validationEOPourFormation((FormationAccueil)rapprochEtab, listeErreursValidation);
        // validation de la formation d'origine avec l'enseignements optionnel
        formationMatiereManager.validationEOPourFormation((FormationOrigine) rapprochEtab, listeErreursValidation);

        // controle d'unicit� : etab origine / etab accueil / formation accueil / ens.
        // det. 1 & 2
        if (!rapprochEtabDao.isUnique(rapprochEtab)) {
            listeErreursValidation
                    .add("Il existe d�j� un enregistrement avec cette formation d'origine et d'accueil");
        }

        if (!listeErreursValidation.isEmpty()) {
            throw new ValidationException(listeErreursValidation);
        }
    }

    /**
     * M�thode permettant de r�cup�rer tous les rapprochement entre �tablissements
     * utilisant des formations sp�cifiques.
     * 
     * @param formations
     *            la liste des formations utilis�es par les rapprochement entre
     *            �tablissements
     * @return les rapprochement entre �tablissements utilisant les formations en
     *         param�tre
     */
    public List<RapprochEtab> rapprochEtabAvecFormation(List<Formation> formations) {
        if (formations.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<Filtre> filtresFormations = new ArrayList<>();

            for (Formation formation : formations) {
                filtresFormations
                        .add(Filtre.and(Filtre.equal("mnemoniqueAccueil", formation.getId().getMnemonique()),
                                Filtre.equal("codeSpecialiteAccueil", formation.getId().getCodeSpecialite())));
            }

            return rapprochEtabDao.lister(Filtre.or(filtresFormations));
        }
    }

    /**
     * Oblige la relance des OPA potentiellement concern�es par le rapprochement
     * �tablissement fourni.
     * 
     * @param rapprochementEtablissement
     *            le rapprochement �tablissement � traiter
     */
    private void obligerRelanceOpa(RapprochEtab rapprochementEtablissement) {
        operationProgrammeeAffectationManager.obligerRelanceOpa(rapprochementEtablissement);
    }

    /**
     * Fournis le rapprochement �tablissement correspondant le mieux aux crit�res donn�es et alimente le cache si
     * n�cessaire.
     * 
     * @param idEtabOrgine
     *            l'�tablissemenent d'origine
     * @param etabAccueil
     *            l'�tablissemenent d'accueil
     * @param formationA
     *            la formation d'accueil
     * @param ensOpt
     *            l'enseignement optionnel
     * @param cacheMap
     *            le cache
     * @return le rapprochement d'�tablissement � appliquer ou null si aucun ne correspond
     */
    @SuppressWarnings("unchecked")
    public RapprochEtab trouverPlusProche(String idEtabOrgine, Etablissement etabAccueil, Formation formationA, Formation formationOrigine,
            Matiere ensOpt, Map<String, Object> cacheMap) {
        // r�cup�ration de la liste des RapprochEtab
        final String cacheName = "listeRapprochEtab";
        List<RapprochEtab> lRapEtab;
        if (cacheMap == null) {
            // si pas de cache, on r�duit les possibilit�s en mettant les crit�res obligatoires
            lRapEtab = rapprochEtabDao.listerRapprochEtabPourEtabAccueil(etabAccueil);
        } else if (!cacheMap.containsKey(cacheName)) {
            lRapEtab = rapprochEtabDao.listerRapprochEtabAvecJointureFetch();
            cacheMap.put(cacheName, new ArrayList<RapprochEtab>(lRapEtab));
        } else {

            // new pour ne pas alterer la liste en cache
            lRapEtab = new ArrayList<>((List<RapprochEtab>) cacheMap.get(cacheName));
        }

        List<Predicate<RapprochEtab>> predicats = listePredicat(idEtabOrgine, formationOrigine, etabAccueil, formationA,  ensOpt);
        for (Predicate<RapprochEtab> predicat : predicats) {
            LOG.debug("==================== Elimination par pr�dicat :"+ predicat.toString()+ "==================");
            lRapEtab.removeIf(predicat);
            LOG.debug("==========================================================================================");
        }

        // pas de bonus corrects
        if (lRapEtab.isEmpty()) {
            return null;
        }

        // il reste les �l�ments qui sont corrects, on les trie et on restitue le premier
        TreeSet<RapprochEtab> listeTriee = new TreeSet<>(new RapprochEtabComparator());
        listeTriee.addAll(lRapEtab);
        return listeTriee.first();
    }

    /**
     * Liste des pr�dicats pour �liminer les rapprochements �tablissement qui ne correspondent pas � celui
     * recherch�.
     * 
     * @param idEtabOrgine
     *            Id de l'�tablissement origine
     * @param etabAccueil
     *            etablissement de l'offre de formation
     * @param formationA
     *            formation sur laquelle porte l'offre de formation
     * @param formationOrigine la formation d'origine
     * @param enseignementOptionnel
     *            l'enseignement optionnel (nullable)
     * @return la liste des pr�dicats pour �liminer les rapprochements �tablissements incorrects
     */
    private List<Predicate<RapprochEtab>> listePredicat(String idEtabOrgine, Formation formationOrigine, Etablissement etabAccueil, Formation formationA
            , Matiere enseignementOptionnel) {
        List<Predicate<RapprochEtab>> predicats = new ArrayList<>();

        // V�rification de l'�tablissement d"origine
        predicats.add(
                rap -> {
                    boolean elimine=rap.getIdEtablissementOrigine() != null && rap.getIdEtablissementOrigine().length() == 3
                            && !idEtabOrgine.startsWith(rap.getIdEtablissementOrigine());
                    LOG.debug("Elimin� EO "+idEtabOrgine+" startsWith: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
                    return elimine;
                });
        predicats.add(
                rap -> {
                    boolean elimine=rap.getIdEtablissementOrigine() != null && rap.getIdEtablissementOrigine().length() != 3
                            && !idEtabOrgine.equals(rap.getIdEtablissementOrigine());
                    LOG.debug("Elimin� EO "+idEtabOrgine+" neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
                    return elimine;
                });

        // V�rification de l'�tablissement d'accueil
        predicats.add(rap -> {
            boolean elimine=!rap.getEtablissementAccueil().equals(etabAccueil);
            LOG.debug("Elimin� EA "+etabAccueil+" neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
            return elimine;
        });

        // V�rification de la formatiion d'accueil
        predicats.add(rap -> {
            boolean elimine=rap.getMnemoniqueAccueil() != null
                    && !rap.getMnemoniqueAccueil().equals(formationA.getId().getMnemonique());
            LOG.debug("Elimin� MNEA "+formationA.getId().getMnemonique()+" neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
            return elimine;
        });
        predicats.add(rap -> {
            boolean elimine=rap.getCodeSpecialiteAccueil() != null
                    && !rap.getCodeSpecialiteAccueil().equals(formationA.getId().getCodeSpecialite());
            LOG.debug("Elimin� CSA "+formationA.getId().getCodeSpecialite()+" neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
            return elimine;
        });

        // V�rification de la formatiion d'origine
        predicats.add(rap -> {
            boolean elimine=rap.getMnemoniqueOrigine() != null
                    && !rap.getMnemoniqueOrigine().equals(formationOrigine.getId().getMnemonique());
            LOG.debug("Elimin� MNEO "+formationOrigine.getId().getMnemonique()+"  neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
            return elimine;
        });
        predicats.add(rap -> {
            boolean elimine=rap.getCodeSpecialiteOrigine() != null
                    && !rap.getCodeSpecialiteOrigine().equals(formationOrigine.getId().getCodeSpecialite());
            LOG.debug("Elimin� CSO "+formationOrigine.getId().getCodeSpecialite()+"   neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
            return elimine;
        });

        // V�rification de l'enseignement optionnel
        predicats.add(rap -> {
            boolean elimine = rap.getMatiereEnseigOptionnel() != null
                    && !rap.getMatiereEnseigOptionnel().equals(enseignementOptionnel);
            LOG.debug("Elimin� MEnsOpt  "+enseignementOptionnel+"    neq: " + (elimine ? Flag.OUI : Flag.NON) + " "+rap.toString());
            return elimine;
        });

        return predicats;
    }
}
