/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.service.nomenclature;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.LienZoneGeoDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.PropositionAccueilDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.VoeuDao;
import fr.edu.academie.affelnet.dao.interfaces.nomenclature.accueil.StatutEleveDao;
import fr.edu.academie.affelnet.dao.interfaces.planificationCampagneAffectation.OffreFormationSansOPADao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuEleveDao;
import fr.edu.academie.affelnet.dao.interfaces.voeu.VoeuTeleserviceDao;
import fr.edu.academie.affelnet.domain.Flag;
import fr.edu.academie.affelnet.domain.ValidationException;
import fr.edu.academie.affelnet.domain.affectation.DecisionFinale;
import fr.edu.academie.affelnet.domain.nomenclature.Commission;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Formation;
import fr.edu.academie.affelnet.domain.nomenclature.LienZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.Matiere;
import fr.edu.academie.affelnet.domain.nomenclature.MefStat;
import fr.edu.academie.affelnet.domain.nomenclature.Palier;
import fr.edu.academie.affelnet.domain.nomenclature.ParametresFormationAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.PropositionAccueil;
import fr.edu.academie.affelnet.domain.nomenclature.TypeOffre;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.Voie;
import fr.edu.academie.affelnet.domain.nomenclature.ZoneGeo;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.StatutEleve;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OffreFormationSansOPA;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.Eleve;
import fr.edu.academie.affelnet.domain.voeu.VoeuEleve;
import fr.edu.academie.affelnet.service.AbstractManager;
import fr.edu.academie.affelnet.service.EtablissementManager;
import fr.edu.academie.affelnet.service.ParametreManager;
import fr.edu.academie.affelnet.service.interfaces.nomenclature.OffreFormationManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.utils.DateUtils;
import fr.edu.academie.affelnet.web.utils.FiltreUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Classe aidant � la gestion des offres de formation.
 * 
 * TODO : Le terme 'voeu' est historique. Il est pr�f�rable d'utiliser
 * le terme 'offre de formation' pour �viter la confusion avec les voeux formul�s par les
 * �l�ves. Au niveau des nouveaux usages, pr�f�rer passer par {@link OffreFormationManager}.
 */
@Service
public class VoeuManager extends AbstractManager implements OffreFormationManager {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(VoeuManager.class);

    /**
     * Format des codes des offres de formation, 3 caract�res alpha-num�riques de pr�fixe,
     * puis 5 chiffres d'incr�ment.
     */
    private static final String FORMAT_CODE_VOEU = "^[A-Za-z0-9]{3}[0-9]{5}$";

    /**
     * Le dao utilis� pour les offres de formation.
     */
    private VoeuDao voeuDao;

    /**
     * Le dao utilis� pour les liens entre zones g�ographiques.
     */
    private LienZoneGeoDao lienZoneGeoDao;

    /**
     * Le dao utilis� pour les propositions d'accueil.
     */
    private PropositionAccueilDao propositionAccueilDao;

    /**
     * Le dao utilis� pour les voeux �l�ves.
     */
    private VoeuEleveDao voeuEleveDao;

    /**
     * Le dao utilis� pour les offres de formations sans OPA de tour principal.
     */
    private OffreFormationSansOPADao offreFormationSansOPADao;

    /**
     * Le manager utilis� pour les formations-mati�res.
     */
    private FormationMatiereManager formationMatiereManager;

    /**
     * Le manager pour les �tablissements.
     */
    private EtablissementManager etablissementManager;

    /**
     * Le manager pour les formations.
     */
    private FormationManager formationManager;

    /**
     * Le manager utilis� pour les mati�res.
     */
    private OuvrableManager ouvrableManager;

    /**
     * Le manager utilis� pour les param�tres.
     */
    private ParametreManager parametreManager;

    /**
     * Le manager utilis� pour les OPA.
     */
    private OperationProgrammeeAffectationManager opaManager;

    /** Le DAO pour l'acc�s aux statuts. */
    private StatutEleveDao statutEleveDao;

    /** Le DAO pour l'acc�s aux offres de formation issues du t�l�service affectation. */
    private VoeuTeleserviceDao voeuTeleserviceDao;

    /**
     * @param statutEleveDao
     *            the statutEleveDao to set
     */
    @Autowired
    public void setStatutEleveDao(StatutEleveDao statutEleveDao) {
        this.statutEleveDao = statutEleveDao;
    }

    /**
     * @param voeuDao
     *            the voeuDao to set
     */
    @Autowired
    public void setVoeuDao(VoeuDao voeuDao) {
        this.voeuDao = voeuDao;
    }

    /**
     * @param voeuTeleserviceDao
     *            Le DAO pour l'acc�s aux offres de formation issues du t�l�service affectation
     */
    @Autowired
    public void setVoeuTeleserviceDao(VoeuTeleserviceDao voeuTeleserviceDao){
        this.voeuTeleserviceDao = voeuTeleserviceDao;
    }

    /**
     * @param lienZoneGeoDao
     *            the lienZoneGeoDao to set
     */
    @Autowired
    public void setLienZoneGeoDao(LienZoneGeoDao lienZoneGeoDao) {
        this.lienZoneGeoDao = lienZoneGeoDao;
    }

    /**
     * @param propositionAccueilDao
     *            the propositionAccueilDao to set
     */
    @Autowired
    public void setPropositionAccueilDao(PropositionAccueilDao propositionAccueilDao) {
        this.propositionAccueilDao = propositionAccueilDao;
    }

    /**
     * @param voeuEleveDao
     *            the voeuEleveDao to set
     */
    @Autowired
    public void setVoeuEleveDao(VoeuEleveDao voeuEleveDao) {
        this.voeuEleveDao = voeuEleveDao;
    }

    /**
     * @param offreFormationSansOPADao
     *            the offreFormationSansOPADao to set
     */
    @Autowired
    public void setOffreFormationSansOPADao(OffreFormationSansOPADao offreFormationSansOPADao) {
        this.offreFormationSansOPADao = offreFormationSansOPADao;
    }

    /**
     * @param formationMatiereManager
     *            the formationMatiereManager to set
     */
    @Autowired
    public void setFormationMatiereManager(FormationMatiereManager formationMatiereManager) {
        this.formationMatiereManager = formationMatiereManager;
    }

    /**
     * @param etablissementManager
     *            the etablissementManager to set
     */
    @Autowired
    public void setEtablissementManager(EtablissementManager etablissementManager) {
        this.etablissementManager = etablissementManager;
    }

    /**
     * @param formationManager
     *            the formationManager to set
     */
    @Autowired
    public void setFormationManager(FormationManager formationManager) {
        this.formationManager = formationManager;
    }

    /**
     * @param ouvrableManager
     *            the ouvrableManager to set
     */
    @Autowired
    public void setOuvrableManager(OuvrableManager ouvrableManager) {
        this.ouvrableManager = ouvrableManager;
    }

    /**
     * @param parametreManager
     *            the parametreManager to set
     */
    @Autowired
    public void setParametreManager(ParametreManager parametreManager) {
        this.parametreManager = parametreManager;
    }

    /**
     * @param opaManager
     *            the opaManager to set
     */
    @Autowired
    public void setOpaManager(OperationProgrammeeAffectationManager opaManager) {
        this.opaManager = opaManager;
    }

    /**
     * Charge l'offre de formation dont le code est fourni.
     * 
     * @param code
     *            le code de l'offre de formation � charger
     * @return le <code>Voeu</code>
     * 
     *         Si l'offre de formation n'existe pas, l'appel d�clenche une DaoExecption.
     */
    public Voeu charger(String code) {
        return voeuDao.charger(code);
    }

    /**
     * Charge l'offre de formation si elle existe.
     * 
     * @param code
     *            le code l'offre de formation � charger
     * @return le <code>Voeu</code> s'il existe, null sinon
     */
    public Voeu chargerNullable(String code) {
        if (voeuDao.existe(code)) {
            return voeuDao.charger(code);
        } else {
            return null;
        }
    }

    /**
     * Teste s'il existe une offre de formation (nomenclature voeu) avec le code indiqu�.
     * 
     * @param code
     *            code de l'offre de formation � tester
     * @return vrai s'il existe une offre de formation pour ce code, sinon faux
     */
    public boolean existeParCode(String code) {
        String upperCasedCode = code.toUpperCase();
        return voeuDao.existe(upperCasedCode);
    }

    /**
     * Met � jour l'offre de formation pass�e en param�tre.
     * 
     * @param offreFormation
     *            L'offre de formation � mettre � jour.
     * @param codeOffre
     *            Le code de l'offre de formation avant changement.
     * @return L'offre de formation mise � jour.
     */
    public Voeu mettreAJour(Voeu offreFormation, String codeOffre) {
        offreFormation.setDateMAJ(new Date());
        if (offreFormation.getFlagCatalogueApprentissage() == null) {
            offreFormation.setFlagCatalogueApprentissage(Flag.NON);
        }

        LOG.debug("Mise � jour de l'offre de formation " + codeOffre);
        opaManager.obligerRelanceOpa(offreFormation);
        return voeuDao.maj(offreFormation, codeOffre);
    }

    /**
     * Met � jour l'offre de formation pass�e en param�tre.
     * 
     * @param offreFormation
     *            L'offre de formation � mettre � jour.
     * @return L'offre de formation mise � jour.
     */
    public Voeu mettreAJour(Voeu offreFormation) {
        return mettreAJour(offreFormation, offreFormation.getCode());
    }

    /**
     * Contr�le de l'unicit� de l'offre de formation transmise.
     * On se base sur les �l�ments suivants :
     * <ul>
     * <li>la formation d'accueil (MEF : mn�monique + sp�cialit�)</li>
     * <li>les enseignements optionnels contingent�s �ventuels (cl� de gestion)</li>
     * <li>le statut de la scolarit� d'accueil (code statut)</li>
     * <li>l'�tablissement d'accueil (identifiant �tablissement UAI/RNE)</li>
     * </ul>
     * 
     * @param offreFormation
     *            l'offre de formation dont l'unicit� est � tester
     * @return vrai si l'offre de formation est unique, sinon faux.
     */
    public boolean isUnique(Voeu offreFormation) {
        return voeuDao.isUnique(offreFormation);
    }

    /**
     * Teste la compatibilit� de la formation d'accueil CAP avec l'autorisation palier 3�me d'une offre de
     * formation.
     * 
     * @param offreFormation
     *            offre de formation
     * @return vrai si la formation d'accueil CAP est compatible avec le palier 3�me autoris� pour la saisie, sinon
     *         faux
     */
    public boolean isFormationAccueilCAPCompatiblePourPalier3(Voeu offreFormation) {

        return isFormationAccueilCAPCompatiblePourPalier3(offreFormation.getFormationAccueil(),
                offreFormation.getSaisieAutorisee3EME());
    }

    /**
     * Teste la compatibilit� de la formation d'accueil CAP avec l'autorisation palier 3�me.
     * 
     * @param formationAccueil
     *            formation d'accueil
     * @param saisieAutorisee3EME
     *            flag de saisie autoris�e pour le palier 3�me
     * @return vrai si la formation d'accueil CAP est compatible avec le palier 3�me autoris� pour la saisie, sinon
     *         faux
     */
    public boolean isFormationAccueilCAPCompatiblePourPalier3(Formation formationAccueil,
            String saisieAutorisee3EME) {

        // On examine seulement les offres accessibles au palier 3�me
        // Pour le reste, on ne bloque rien (doublement)
        if (saisieAutorisee3EME.equals(Flag.NON)) {

            // Non concern�, inutile d'examiner le MefStat
            return true;
        }

        // L'offre de formation est autoris�e au palier 3�me
        // On r�cup�re le MefStat4 du Mef d'accueil pour tester s'il fait partie des blocages pr�vus
        String codeMefStat4 = formationAccueil.getCodeMefStat4();

        // Ok si le MefStat4 n'est pas inaccessible au palier 3�me
        return !(MefStat.ACCUEIL_CAP_INACCESSIBLE_PALIER_3EME.contains(codeMefStat4));

    }

    /**
     * @param voeu
     *            l'offre de formation
     * @return vrai si au moins un lien zone g�o. utilise cette offre de formation
     */
    public boolean isUtiliseParLienZoneGeo(Voeu voeu) {
        if (voeu == null) {
            return false;
        }
        return (lienZoneGeoDao.getNombreElements(Filtre.equal("voeu", voeu)) > 0);
    }

    /**
     * D�termine si l'offre est li� � un lien zone g�o avec bonus positif.
     * 
     * @param codeOffre
     *            le code de l'offre de formation
     * @return vrai si au moins un lien zone g�o. avec bonus positif utilise cette offre de formation
     */
    public boolean isUtiliseParLienZoneGeoAvecBonusPosisitf(String codeOffre) {
        if (StringUtils.isBlank(codeOffre)) {
            return false;
        }
        return (lienZoneGeoDao.getNombreElements(
                Filtre.and(Filtre.equal("voeu.code", codeOffre), Filtre.superieur("bonus", 0))) > 0);
    }

    /**
     * @param voeu
     *            l'offre de formation
     * @return vrai si au moins une proposition d'accueil (nomenclature) utilise cette offre de formation
     */
    public boolean isUtiliseParPropositionAccueil(Voeu voeu) {
        if (voeu == null) {
            return false;
        }
        return (propositionAccueilDao.getNombreElements(Filtre.equal("voeu", voeu)) > 0);
    }

    /**
     * @param voeu
     *            offre de formation
     * @return vrai si au moins un �l�ve a formul� cette offre de formation
     */
    public boolean isUtiliseParVoeuEleve(Voeu voeu) {
        if (voeu == null) {
            return false;
        }
        return (voeuEleveDao.getNombreElements(Filtre.equal("voeu", voeu)) > 0);
    }

    /**
     * @param voeu
     *            offre de formation
     * @return vrai si au moins un �l�ve a formul� cette offre de formation via le t�l�service
     */
    public boolean isUtiliseParVoeuTeleservice(Voeu voeu) {
        if (voeu == null) {
            return false;
        }
        return (voeuTeleserviceDao.getNombreElements(Filtre.equal("offreFormation", voeu)) > 0);
    }
    /**
     * M�thode permettant de v�rifier si le voeu est utilis� par un �l�ve du palier 3i�me.
     * 
     * @param voeu
     *            offre de formation
     * @return vrai si au moins un �l�ve du palier 3i�me a formul� cette offre de formation
     */
    public boolean isUtiliseParVoeuElevePalier3(Voeu voeu) {
        if (voeu == null) {
            return false;
        }
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.equal("voeu", voeu));
        filtres.add(Filtre.equal("eleve.palierOrigine.code", Palier.CODE_PALIER_3EME));
        return (voeuEleveDao.getNombreElements(filtres) > 0);
    }

    /**
     * @param id
     *            l'identifiant d'un etablissement
     * @return le nombre d'offres de formation dans l'�tablissement
     */
    public int nbOffresFormationParEtab(String id) {
        List<Filtre> listFilters = new ArrayList<>();
        listFilters.add(Filtre.equal("etablissement.id", id));

        return voeuDao.getNombreElements(listFilters);
    }

    /**
     * Donne le libell� court correspondant � la d�cision finale sur un voeu �l�ve.
     * 
     * @param ve
     *            voeu �l�ve
     * @return libell� court de la d�cision finale (si elle existe) sinon la chaine vide.
     */
    public String getLibelleCourtDecisionFinale(VoeuEleve ve) {

        Integer codeDecFin = ve.getCodeDecisionFinal();

        if (codeDecFin == null) {
            return "";
        }

        if (DecisionFinale.AFFECTE.getCode() == codeDecFin) {
            return "PRIS";
        } else if (DecisionFinale.LISTE_SUPP.getCode() == codeDecFin) {
            return "LS n�" + ve.getNumeroListeSuppFinal();
        } else if (DecisionFinale.REFUSE.getCode() == codeDecFin) {
            return "Refus�";
        } else if (DecisionFinale.ABANDON.getCode() == codeDecFin) {
            return "Abandon";
        } else if (DecisionFinale.NON_TRAITE.getCode() == codeDecFin) {
            return "Non trt.";
        } else if (DecisionFinale.RECENSEMENT.getCode() == codeDecFin) {
            return parametreManager.getLibelleDecisionRecensement();
        } else if (DecisionFinale.DOSSIER_ABSENT.getCode() == codeDecFin) {
            return "Doss. abs.";
        } else {
            return "";
        }
    }

    /**
     * Compose le descriptif correspondant � une offre de formation donn�e.
     * 
     * @param voeu
     *            l'offre de formation � traiter
     * @param nomsChamps
     *            vrai s'il faut ajouter les noms des champs dans le descriptif
     * @return libell� de descriptif
     */
    public String getLibelleFiltreVoeu(Voeu voeu, boolean nomsChamps) {

        String libelle = "";

        // Code Voeu
        if (nomsChamps) {
            libelle += "code voeu : ";
        }

        libelle += voeu.getCode();

        // Formation du voeu
        libelle += " / ";
        if (nomsChamps) {
            libelle += "formation : ";
        }

        Formation formation = voeu.getFormationAccueil();
        libelle += formation.getId().getMnemonique() + " " + formation.getId().getCodeSpecialite() + " "
                + formation.getLibelleLong();

        Matiere eo = voeu.getMatiereEnseigOptionnel();

        // Teste s'il y a un enseignement optionnel sur cette offre de formation
        if (eo != null) {
            libelle += " (ens opt 1 : ";
            libelle += eo.getCleGestion();
            libelle += ")";
        }

        // �tablissement du voeu
        libelle += " / ";
        if (nomsChamps) {
            libelle += "�tablissement : ";
        }

        Etablissement etab = voeu.getEtablissement();
        libelle += etab.getId() + " " + etab.getTypeEtablissement().getCode() + " " + " "
                + etab.getDenominationComplementaire() + " " + etab.getVille();

        return libelle;
    }

    /**
     * Teste si la chaine fournie peut correspondre � un code d'offre de formation.
     * 
     * @param code
     *            cha�ne � tester
     * @return vrai si la cha�ne peut �tre un code d'offre de formation, sinon faux
     */
    public boolean isCodeVoeuPossible(String code) {

        if (StringUtils.isEmpty(code)) {
            return false;
        }

        // V�rification du format du code voeu
        return code.matches(FORMAT_CODE_VOEU);
    }

    /**
     * M�thode qui renvoie la liste des offres de formation d'un �tablissement.
     * 
     * @param idEtab
     *            le code RNE de l'�tablissement.
     * @return la liste des offres de formation d'un �tablissement.
     */
    public List<Voeu> getVoeuxParEtab(String idEtab) {
        return voeuDao.lister(Filtre.equal("etablissement.id", idEtab));
    }

    /**
     * M�thode qui renvoie la liste des offres de formation d'un �tablissement.
     * 
     * @param idEtab
     *            le code RNE de l'�tablissement.
     * @param tris
     *            le tri � appliquer
     * @return la liste des offres de formation d'un �tablissement.
     */
    public List<Voeu> getVoeuxParEtab(String idEtab, List<Tri> tris) {
        return voeuDao.lister(Filtre.equal("etablissement.id", idEtab), tris);
    }

    /**
     * M�thode qui renvoie la liste de toutes les offres de formation.
     * 
     * @return La liste de toutes les offres de formation.
     */
    public List<Voeu> getVoeux() {
        return voeuDao.lister();
    }

    /**
     * M�thode d'obtention des offres de formation qui ne sont pas de recensement.
     * 
     * @return les offres de formation qui ne sont pas de recensement
     */
    public List<Voeu> getVoeuxHorsRecensement() {

        Filtre filtreRecensement = Filtre.equal("flagRecensement", Flag.NON);
        Filtre filtreCapaciteAffectation = Filtre.superieur("pile.capaciteAffectation", 0);
        return voeuDao.lister(Filtre.and(filtreRecensement, filtreCapaciteAffectation));
    }

    /**
     * Compose un filtre sur les offres de formations par formation d'accueil.
     * 
     * @param formationAccueil
     *            Formation d'accueil
     * @return filtre r�sultant
     */
    public Filtre getFiltreSurOffreFormationFormationAccueil(Formation formationAccueil) {
        return Filtre.and(Filtre.equal("formationAccueil.id.mnemonique", formationAccueil.getId().getMnemonique()),
                Filtre.equal("formationAccueil.id.codeSpecialite", formationAccueil.getId().getCodeSpecialite()));
    }

    /**
     * Compose un filtre sur les codes d'offre de formation.
     * 
     * @param codeVoeu
     *            Code de l'offre de formation
     * @return filtre r�sultant
     */
    public Filtre getFiltreCodeVoeu(String codeVoeu) {
        return Filtre.equal("voeu.code", codeVoeu);
    }

    /**
     * Compose un filtre sur l'�tablissement d'accueil de l'offre de formation.
     * 
     * @param codeUaiEtablissementAccueil
     *            code de l'�tablissement d'accueil
     * @return filtre r�sultant
     */
    public Filtre getEtablissementAccueil(String codeUaiEtablissementAccueil) {
        return Filtre.equal("voeu.etablissement.id", codeUaiEtablissementAccueil);
    }

    /**
     * Fournis un filtre pour la nomenclature des offres de formation correspondant � l'enseignement optionnel
     * demand�. A n'utiliser que pour un enseignement non null.
     * 
     * @param enseignementOptionnel
     *            enseignement optionnel
     * 
     * @return un filtre d'offre de formation sur l'enseignement optionnel.
     */
    public Filtre ajouteFiltreSurOffreFormationEnseignementOptionnel(Matiere enseignementOptionnel) {
        if (enseignementOptionnel != null) {
            return Filtre.equal("matiereEnseigOptionnel.cleGestion", enseignementOptionnel.getCleGestion());
        } else {
            return Filtre.trueValue();
        }
    }

    /**
     * Compose un filtre sur les offres de formation utilisant un paramefa donn�.
     * 
     * @param paramefa
     *            le param�tre par formation d'accueil
     * @return filtre cr��
     */
    public Filtre getFiltreUtilisantParamefa(ParametresFormationAccueil paramefa) {
        return Filtre.equal("piles.parametreFormationAccueil.id", paramefa.getId());
    }

    /**
     * Indique si la formation est valide pour la voie donn�e.
     * 
     * @param voie
     *            la voie � tester
     * @param formation
     *            la formation associ�e � la mati�re
     * @param statut
     *            le statut de l'offre de formation
     * 
     * @return la liste des erreurs rencontr�es
     */
    public List<String> validationFormationPourVoie(Voie voie, Formation formation, StatutEleve statut) {
        List<String> listeErreursValidation = new ArrayList<>();
        String codeVoie = null;
        String mnemonique = null;

        if (voie != null) {
            codeVoie = voie.getCode();
        }

        if (formation != null) {
            mnemonique = formation.getId().getMnemonique();
        }

        // Seule la voie 2nd GT permet la 2-GT
        if (!Voie.VOIE_ORIENTATION_2GT.equals(codeVoie) && Formation.SECONDE_2GT.equals(mnemonique)) {
            listeErreursValidation.add("Seul le parcours d'orientation \"2NDE GENERALE ET TECHNOLOGIQUE\" "
                    + "peut contenir la formation " + Formation.SECONDE_2GT);
        }

        // Un statut apprentissage n'est pas compatible avec la voie GT
        if (statut != null && statut.isApprentissage() && (Voie.LISTE_VOIES_GT.contains(codeVoie))) {
            listeErreursValidation.add("Le statut apprentissage n'est pas compatible avec la voie GT.");
        }

        return listeErreursValidation;
    }

    /**
     * Applique les r�gles m�tiers sur l'objet et rempli la liste des erreurs de validation si
     * l'objet n'est pas valide.
     * 
     * @param voeu
     *            le voeu
     * @param listeErreursValidation
     *            la liste des �ventuelles erreurs de validation
     */
    public void validationMetier(Voeu voeu, List<String> listeErreursValidation) {
        // un voeu de type "0" ne peut �tre associ� qu'� une commission de type non pam
        // un voeu de type "1" ou "2" ne peut �tre associ� qu'� une commission de type pam
        String indicateurPam = voeu.getIndicateurPam();
        Commission commission = voeu.getCommission();
        if (indicateurPam.equals(TypeOffre.COMMISSION.getIndicateur())
                && !commission.getFlPam().equals(Flag.NON)) {
            listeErreursValidation.add("Une offre de formation de type 'commission' (sans bar�me) "
                    + "ne peut �tre associ�e � une commission 'avec prise en compte de bar�me'");
        }
        if (!indicateurPam.equals(TypeOffre.COMMISSION.getIndicateur())
                && commission.getFlPam().equals(Flag.NON)) {
            listeErreursValidation.add("Une offre de formation 'avec prise en compte de bar�me' "
                    + "ne peut �tre associ�e � une commission 'sans prise en compte de bar�me'");
        }

        // l'�tablissement saisi doit r�pondre � la notion d'accueil
        Etablissement etablissement = voeu.getEtablissement();
        if (etablissement != null && !ouvrableManager.isAccueil(etablissement)) {
            listeErreursValidation.add("L'�tablissement n'est pas ouvert");
        }
        // la voie d'orientation saisie doit r�pondre � la notion d'accueil
        Voie voie = voeu.getVoie();
        if (voie != null && !ouvrableManager.isAccueil(voeu.getVoie())) {
            listeErreursValidation.add("Le parcours d'orientation n'est pas ouvert");
        }

        Formation formation = voeu.getFormationAccueil();

        // la formation d'accueil et l'enseignement optionnel doivent r�pondre � la notion d'accueil
        formationMatiereManager.validationOuvertureEO(voeu, listeErreursValidation);

        // validation de la formation d'accueil avec l'enseignement optionnel
        formationMatiereManager.validationEOPourFormation(voeu, listeErreursValidation);

        listeErreursValidation.addAll(validationFormationPourVoie(voie, formation, voeu.getStatut()));

        listeErreursValidation.addAll(validationFamilleMetier(formation, voeu.getStatut()));

        // Contr�le des formations CAP compatibles avec l'acc�s depuis le palier 3�me
        if (!isFormationAccueilCAPCompatiblePourPalier3(voeu)) {
            listeErreursValidation.add("La formation d'accueil " + formation.getLibelleLong()
                    + " n'est pas compatible avec le palier d'origine 3�me");
        }

        // controle d'unicit�
        if (!voeuDao.isUnique(voeu)) {
            listeErreursValidation.add("Il existe d�j� un enregistrement avec cette formation d'accueil");
        }

        if (!listeErreursValidation.isEmpty()) {
            throw new ValidationException(listeErreursValidation);
        }
    }

    /**
     * V�rifie qu'il n'y a pas d'incoh�rence entre le statut de l'offre et la famille de m�tier de la formation
     * d'accueil.
     * 
     * @param formation
     *            la formation d'accueil
     * @param statut
     *            le statut de l'offre
     * @return la liste des erreurs rencontr�es
     */
    public List<String> validationFamilleMetier(Formation formation, StatutEleve statut) {
        List<String> listeErreursValidation = new ArrayList<>();

        if (statut != null && statut.isApprentissage()
                && MefStat.MEFSTAT_INTERDIT_POUR_APPRENTISSAGE.contains(formation.getMefStat().getCode())) {
            listeErreursValidation
                    .add("Le statut apprentissage n'est pas compatible avec les familles de m�tier.");
        }

        return listeErreursValidation;
    }

    /**
     * Donne la liste des offres de formation utilisables en proposition d'accueil pour l'�l�ve.
     * 
     * @param eleve
     *            Eleve concern�
     * @return liste des offres de formation proposables
     */
    public List<Voeu> getListeVoeuxProposables(Eleve eleve) {

        // cr�ation de la liste des propositions d'accueil
        ZoneGeo zoneGeo = eleve.getZoneGeo();
        if (zoneGeo == null) {
            LOG.debug("La zone g�ographique de l'�l�ve " + eleve.getIne()
                    + " n'est pas connue, il n'y a pas de proposition d'accueil.");
            return Collections.emptyList();
        }
        String codeZoneGeo = zoneGeo.getCode();

        List<PropositionAccueil> listePropAcc = getListePropositionAccueilPourZoneGeo(codeZoneGeo);

        List<Voeu> voeuxProposables = new ArrayList<>();
        for (PropositionAccueil propAcc : listePropAcc) {
            Voeu voeuProposable = propAcc.getVoeu();
            if (voeuProposable.getPile().getCapaciteAffectation() > 0) {
                voeuxProposables.add(voeuProposable);
            }
        }

        return voeuxProposables;
    }

    /**
     * Donne la liste des propositions d'accueil pour la zone g�ographique indiqu�e.
     * 
     * @param codeZoneGeo
     *            Code de la ZoneGeo
     * @return liste des voeux proposables
     */
    public List<PropositionAccueil> getListePropositionAccueilPourZoneGeo(String codeZoneGeo) {
        return propositionAccueilDao.lister(Filtre.equal("codeZoneGeoOrigine", codeZoneGeo));
    }

    /**
     * G�n�re un nouveau code d'offre de formation.
     * 
     * @param prefixeVoeu
     *            Le prefixe du voeu (3 carat�res).
     * @return Un nouveau code d'offre de formation commen�ant par le prefixe de donn�e.
     */
    public String getCode(String prefixeVoeu) {
        return voeuDao.getCode(prefixeVoeu);
    }

    /**
     * Cr�er une nouvelle offre de formation en base de donn�es.
     * 
     * @param voeu
     *            l'offre de formation � ajouter.
     * @return l'offre de formation qui vient d'�tre cr��.
     */
    public Voeu creer(Voeu voeu) {
        return voeuDao.creer(voeu);
    }

    /**
     * Supprime l'offre de formation de la base de donn�es.
     * 
     * @param offreDeFormation
     *            l'offre de formation � supprimer.
     */
    public void supprimer(Voeu offreDeFormation) {

        LOG.debug("Suppression de l'offre de formation " + offreDeFormation.getCode());
        opaManager.obligerRelanceOpa(offreDeFormation);
        voeuDao.supprimer(offreDeFormation);
    }

    /**
     * M�thode permettant de lister les offres de formation.
     * 
     * @return la liste des offres de formation
     */
    public List<Voeu> lister() {
        return voeuDao.lister();
    }

    /**
     * M�thode de construction du filtre qui v�rifie si la formation de l'offre de formation
     * est ouverte en accueil.
     * 
     * @return le filtre v�rifiant si la formation de l'offre de formation est ouverte en accueil
     */
    public Filtre filtreFormationOuverteEnAccueil() {
        int anneeEnCours = parametreManager.getAnnee20xx();
        Date dateReference = DateUtils.get31Decembre(anneeEnCours);
        Filtre filtreDateOuverture = Filtre.inferieurOuEgal("formationAccueil.dateOuverture", dateReference);
        Filtre filtreDateFermeture = Filtre.superieur("formationAccueil.dateFermeture", dateReference);
        return Filtre.and(filtreDateOuverture, filtreDateFermeture);
    }

    /**
     * @return vrai s'il existe au moins une offre de formation dans la nomenclature
     */
    public boolean existeAuMoinsUnVoeu() {
        LOG.debug("V�rification du nombre d'offres de formation");
        return (voeuDao.getNombreElements() > 0);
    }

    /**
     * Liste des offres de formation "bar�me avec �valuations/notes" qui n'ont pas de param�tres
     * par formation d'accueil.
     * 
     * @return la liste des offres de formation "bar�me avec �valuations/notes" qui n'ont pas de param�tres
     *         par formation d'accueil
     */
    public List<Voeu> offresAvecPriseEnCompteNoteEvalSansPFA() {
        LOG.debug("V�rification des offres de formation \"bar�me avec �valuations/notes\" qui n'ont pas de "
                + "correspondance dans les PARAMEFA");
        return voeuDao.listerOffresAvecPriseEnCompteNoteEvalSansPFA(null);
    }

    /**
     * Liste les offres de formation dont les crit�res d'unicit� sont identiques.
     * 
     * Combinaison de crit�res : �tablissement d'accueil, formation d'accueil, enseignement optionnel �ventuel,
     * statut.
     * 
     * @return la liste les offres de formation identiques
     */
    public List<Voeu> offresFormationIdentiques() {
        LOG.debug("Constitution de la liste des offres de formation identiques (�tab, MEF, EO, statut)");
        return voeuDao.listerOffresFormationIdentiques();
    }

    /**
     * M�thode permettant de r�cup�rer les offres de formation correspondant � un enseignement
     * optionnel contingent�.
     * 
     * @return la liste des offres de formation corresponant � une enseignement optionnel contingent�
     */
    public List<Voeu> voeuxAvecEOContingente() {
        List<Filtre> filtresFormations = new ArrayList<>();
        // Seuls les offres de formation 2-GT peuvent posseder des enseignements optionnels (tous les enseignements
        // optionnels sont contingentes)
        filtresFormations.add(Filtre.isNotNull("matiereEnseigOptionnel"));

        return voeuDao.lister(filtresFormations);
    }

    /**
     * M�thode permettant de r�cup�rer les offres de formation correspondant � un enseignement
     * optionnel contingent� avec une capacit� d'affectation sup�rieure � 0.
     *
     * @return la liste des offres de formation corresponant � une enseignement optionnel contingent�
     */
    public List<Voeu> voeuxAvecEOContingenteAvecCapaciteAffectationPositive() {
        List<Filtre> filtresFormations = new ArrayList<>();
        // Seuls les offres de formation 2-GT peuvent posseder des enseignements optionnels (tous les enseignements
        // optionnels sont contingentes)
        filtresFormations.add(Filtre.isNotNull("matiereEnseigOptionnel"));
        filtresFormations.add(Filtre.superieur("pile.capaciteAffectation", 0));
        return voeuDao.lister(filtresFormations);
    }

    /**
     * @return la liste des offres de formation qui ne poss�dent pas de parcours d'orientation
     */
    public List<Voeu> offresFormationSansParcoursOrientation() {
        List<Filtre> filtres = new ArrayList<>();
        filtres.add(Filtre.isNull("voie"));

        return voeuDao.lister(filtres);
    }

    /**
     * M�thode de cr�ation du filtre de s�lection des offres de formation d'une OPA.
     * 
     * @param idOPA
     *            l'identifiant de l'OPA concern�e
     * @return le filtre de s�lection des offres de formation d'une OPA
     */
    public Filtre filtreOffresOPA(long idOPA) {
        Filtre filtre = null;
        OperationProgrammeeAffectation opa = opaManager.charger(idOPA);

        // Il faut contraindre sur les codes des offres de l'OPA
        if (!opa.getOffreFormations().isEmpty()) {

            List<String> codesOffres = new ArrayList<>();

            for (Voeu offre : opa.getOffreFormations()) {
                codesOffres.add(offre.getCode());
            }

            filtre = Filtre.in("code", codesOffres);
        }

        return filtre;
    }

    /**
     * M�thode de cr�ation des filtres de s�lection des offres de formation ayant �t� demand�es
     * par au moins un �l�ve pour une OPA.
     * 
     * @param idOPA
     *            l'identifiant de l'OPA concern�e
     * @return le filtre construit
     */
    public Filtre filtreOffresDemandeesPourOPA(long idOPA) {
        return Filtre.hql("o.code in (select res.voeuEleve.voeu.code " + "from ResultatsProvisoiresOpa res "
                + "where o.code = res.voeuEleve.voeu.code and res.opa.id = " + idOPA + ")");
    }

    /**
     * M�thode permettant d'apr�s un code Voeu de v�rifier que l'offre de formation associ�
     * autorise la saisie 2nd.
     * 
     * @param codeVoeu
     *            le code du voeu de l'offre de formation
     * 
     * @return vrai si la saisie 2nd est autorise
     */
    public boolean voeuxCoherenceNote(String codeVoeu) {
        LOG.debug("V�rification des offres de formation \"bar�me avec notes\" autorise la saisie 2nd pour "
                + codeVoeu);
        Voeu v = charger(codeVoeu);
        return Flag.OUI.equals(v.getSaisieAutorisee2NDE());
    }

    /**
     * M�thode permettant d'apr�s une liste de code Voeu de v�rifier que l'offre de formation associ�
     * autorise la saisie 2nd pour le mode de bareme avec note/eval.
     * 
     * @param listeCodeVoeu
     *            liste contenant des codes de voeu d'offre de formation
     * 
     * @return vrai si la saisie 2nd est autorise
     */
    public boolean voeuxCoherenceNote(List<String> listeCodeVoeu) {
        LOG.debug(
                "V�rification que toutes les offres de formation \"bar�me avec notes\" autorise la saisie 2nd pour "
                        + listeCodeVoeu.toString());
        boolean resultatCoherence = true;

        Iterator<String> itCodeVoeu = listeCodeVoeu.iterator();
        while (itCodeVoeu.hasNext() && resultatCoherence) {
            String codeVoeu = itCodeVoeu.next();
            Voeu v = charger(codeVoeu);
            // On effectue la v�rification uniquement sur les voeux qui sont de type bar�me avec �valuations/notes
            if (TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur().equals(v.getIndicateurPam())) {
                if (Flag.NON.equals(v.getSaisieAutorisee2NDE())) {
                    resultatCoherence = false;
                }
                // Ajout de la v�rification qu'il n'y a pas de saisie autoris� pour les troisi�mes
                if (Flag.OUI.equals(v.getSaisieAutorisee3EME())) {
                    resultatCoherence = false;
                }
            }
        }
        return resultatCoherence;
    }

    /**
     * M�thode permettant d'apr�s une liste de code Voeu de v�rifier qu'au moins une des offre de formation associ�
     * autorise la saisie 3�me.
     * 
     * @param listeCodeVoeu
     *            liste contenant des codes de voeu d'offre de formation
     * 
     * @return vrai si au moins une saisie de 3�me est autorise sur un type de bar�me avec note/Eval
     */
    public boolean voeuxCoherenceEvaluation(List<String> listeCodeVoeu) {
        LOG.debug("V�rification que au moins une des offres de formation"
                + " \"bar�me avec �valuation\" autorise la saisie 3eme pour " + listeCodeVoeu.toString());
        // On initialise le r�sultat de la coherence de la mani�re suivante :
        // On passe la v�rification, on retourne "passe"
        // On effectue la v�rification et elle correspond � la saisie 3i�me OUI, on retourne "saisie3ieme_Oui"
        // On effectue la v�rification et elle correspond � la saisie 3i�me NON, on retourne "saisie3ieme_Non"
        String resultatCoherence = "passe";
        // cr�ation d'un dictionnaire contenant en cl� l'offre de formation et en valeur sa coh�rence
        HashMap<Voeu, String> dictionnaireCoherenceVoeu = new HashMap<>();

        Iterator<String> itCodeVoeu = listeCodeVoeu.iterator();
        while (itCodeVoeu.hasNext()) {
            String codeVoeu = itCodeVoeu.next();
            Voeu v = charger(codeVoeu);
            // On effectue la v�rification uniquement sur les voeux qui sont de type bar�me avec �valuations/notes
            if (TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur().equals(v.getIndicateurPam())) {
                if (Flag.OUI.equals(v.getSaisieAutorisee3EME())) {
                    resultatCoherence = "saisie3ieme_Oui";
                } else {
                    resultatCoherence = "saisie3ieme_Non";
                }
            }
            // On remplit le dictionnaire
            dictionnaireCoherenceVoeu.put(v, resultatCoherence);
        }

        // ------- Partie pour traiter le r�sultat de la recherche -----------------
        // Initialisation du flag pour d�finir s'il y avait coh�rence
        boolean testCoherence = false;
        // Si il y en a un qui a la valeur "saisie3ieme_Oui" alors il y a coh�rence et on retourne vrai
        if (dictionnaireCoherenceVoeu.containsValue("saisie3ieme_Oui")) {
            testCoherence = true;
        } else {
            // si je n'ai aucune valeur saisie3ieme_Oui et aucune valeur saisie3ieme_Non,
            // alors je n'ai eu que des r�sultat pass� donc il y avait coh�rence car on les ignore
            if (dictionnaireCoherenceVoeu.containsValue("saisie3ieme_Non")) {
                testCoherence = false;
            } else {
                testCoherence = true;
            }
        }

        return testCoherence;
    }

    /**
     * M�thode permettant de tester si l'offre de formation est de type "bar�me avec note/�valuation".
     * 
     * @param offreFormation
     *            offre de formation
     * @return vrai si l'offre de formation est de type "bar�me avec note/�valuation"
     */
    public boolean testModeBaremeNoteEvaluation(Voeu offreFormation) {
        return TypeOffre.BAREME_AVEC_NOTE_EVAL.getIndicateur().equals(offreFormation.getIndicateurPam());
    }

    /**
     * M�thode permettant de tester si l'offre de formation est de type "bar�me avec note/�valuation"
     * et est utilis� par des �l�ves du palier troisi�me.
     * 
     * @param offreFormation
     *            offre de formation
     * @return vrai si l'offre de formation est de type "bar�me avec note/�valuation"
     */
    public boolean testUtilisePalier3iemeEtModeBaremeNoteEvaluation(Voeu offreFormation) {
        return (isUtiliseParVoeuElevePalier3(offreFormation) && testModeBaremeNoteEvaluation(offreFormation));
    }

    /** @return liste des offres de formation de CAP incompatibles avec le palier 3�me. */
    public List<Voeu> offresFormationCAPincompatiblesPalier3() {

        List<Filtre> listFiltres = new ArrayList<>();
        FiltreUtils.ajouterCritereEgal(listFiltres, "saisieAutorisee3EME", Flag.OUI);
        listFiltres.add(Filtre.in("formationAccueil.codeMefStat4", MefStat.ACCUEIL_CAP_INACCESSIBLE_PALIER_3EME));
        return voeuDao.lister(listFiltres);
    }

    /**
     * Fournis la liste des offres de formation hors recensement, � capacit� non nulle
     * et li�es � aucune OPA le nombre de voeux formul�s dessus au tour principal.
     * 
     * @return la liste des offres concern�es
     */
    public List<OffreFormationSansOPA> offresFormationSansOpa() {
        List<String> fetchList = new ArrayList<>();
        fetchList.add("offreFormation.etablissement.typeEtablissement");

        return offreFormationSansOPADao.lister(null, null, null, null, fetchList, true);
    }

    /**
     * Fournis la liste des offres de formation hors recensement, � capacit� non nulle,
     * li�es � aucune OPA et sur lesquelles un voeu a �t� fait pour le tour principal.
     * 
     * @return la liste les offres de formation sans OPA et avec voeux
     */
    public List<Voeu> offresFormationsSansOpaAvecVoeux() {
        Stream<OffreFormationSansOPA> offresFormationSansOpa = offresFormationSansOpa().stream();
        return offresFormationSansOpa
                .filter(offreFormationSansOpa -> (offreFormationSansOpa.getNombreVoeux() != 0))
                .map(OffreFormationSansOPA::getOffreFormation).collect(Collectors.toList());
    }

    /**
     * Fournis la listes des offres de formation de statut apprentissage incompatibles avec leurs familles de
     * m�tiers.
     *
     * @return La liste des offres de formation concern�es.
     */
    public List<Voeu> offresFormationApprentissageIncoherentesAvecFamilleMetier() {
        List<Filtre> listFiltres = new ArrayList<>();
        FiltreUtils.ajouterCritereEgal(listFiltres, "statut.code", StatutEleve.CODE_APPRENTISSAGE);
        listFiltres.add(Filtre.in("formationAccueil.mefStat.code", MefStat.MEFSTAT_INTERDIT_POUR_APPRENTISSAGE));

        return voeuDao.lister(listFiltres);
    }

    /**
     * Fournit La liste des offres dont la formation n'est pas de type affectation
     * qui ont un code statut de l'offre incompatible avec la formation nationale
     * d'un point de vue m�tier
     *
     * @param codeStatutOffre     statut de l'offre
     * @param codeMefIncompatible code statut de la formation nationale incompatible
     * @return
     */
    public List<Voeu> offresFormationIncompatiblesMef(String codeStatutOffre, Integer codeMefIncompatible) {
        List<Filtre> listFiltres = new ArrayList<>();
        FiltreUtils.ajouterCritereEgal(listFiltres, "statut.code", codeStatutOffre);
        FiltreUtils.ajouterCritereNotEqual(listFiltres, "formationAccueil.codeInterne", "AFFECTATION");
        FiltreUtils.ajouterCritereHQL(listFiltres, " exists (select fn.id from FormationNational fn " +
                "where o.formationAccueil.id.codeSpecialite = fn.id.codeSpecialite " +
                "and o.formationAccueil.id.mnemonique = fn.id.mnemonique " +
                "and fn.codeStatut =" + codeMefIncompatible + ')');
        return voeuDao.lister(listFiltres);
    }

    /**
     * R�cup�re un statut.
     *
     * @param code le code du statut
     * @return le statut correspondant
     */
    public StatutEleve chargerStatut(String code) {
        return statutEleveDao.chargerNullable(code);
    }

    /**
     * R�cup�re l'ensemble des statuts.
     * 
     * @return la liste des statuts
     */
    public List<StatutEleve> listerStatuts() {
        return statutEleveDao.lister();
    }

    /**
     * Liste les offres de formations visibles au grand public.
     * 
     * @return les offres de formation visibles au public
     */
    public List<Voeu> offresFormationVisiblePublic() {
        Filtre filtreVisible = Filtre.equal("visiblePortail", Flag.OUI);
        return voeuDao.lister(filtreVisible);
    }

    /**
     * Cr�� un filtre regroupant l'ensemble des offres que l'on peut envoyer sur le portail.
     * Sont concern�es :
     * - les offres de formation visibles
     * - le libell� public de l'�tablissement d'accueil est renseign�
     * - le libell� public de la formation d'accueil est renseign�
     * 
     * @return un filtre sur les offres de formations � envoyer au portail
     */
    public Filtre filtreOffresAEnvoyerPortail() {
        List<Filtre> filtres = new ArrayList<>();

        // Offre visible
        filtres.add(Filtre.equal("visiblePortail", Flag.OUI));

        // Informations publiques de l'�tablissement compl�tes
        filtres.add(Filtre.not(etablissementManager.filtreOffreEtabInformationsPubliquesManquantes()));

        // Informations publiques de la formation compl�tes
        filtres.add(Filtre.not(formationManager.filtreOffreFormationInformationsPubliquesManquantes()));

        return Filtre.and(filtres);
    }

    /**
     * Liste les offres de formations sivisbles au grand public mais dont au moins une nomenclature li�e n'a pas
     * son libell� renseign�.
     * 
     * @return les offres de formation qui ne peuvent �tre pas envoy�es au portail car les nomenclatures li�es sont
     *         incompl�tes
     */
    public List<Voeu> offresVisiblesAvecInfoPubliquesManquantes() {
        List<Filtre> filtresNom = new ArrayList<>();

        // L'�tablissement n'est pas valide
        filtresNom.add(etablissementManager.filtreOffreEtabInformationsPubliquesManquantes());

        // La formation n'est pas valide
        filtresNom.add(formationManager.filtreOffreFormationInformationsPubliquesManquantes());

        Filtre filtreNomIncomplete = Filtre.or(filtresNom);

        Filtre filtreVisible = Filtre.equal("visiblePortail", Flag.OUI);

        return voeuDao.lister(Filtre.and(filtreVisible, filtreNomIncomplete));
    }

    /**
     * Liste les offres de formations qui peuvent �tre envoy�es au portail.
     * 
     * @return les offres de formation qui peuvent �tre envoy�es au portail
     */
    public List<Voeu> offresAEnvoyerPortail() {
        return voeuDao.lister(filtreOffresAEnvoyerPortail());
    }

    /**
     * V�rifie si les conditions pour l'envoi des offres de formation vers le national sont respect�es.
     * Pour cela, il faut que l'audit des nomenclature ait �t� effectu� le jour m�me.
     * 
     * @return true si l'envoi des offres de formation est possible
     */
    public boolean estEnvoiOffresAutorise() {
        Date dateAuditNomenclature = parametreManager.getHorodatageAuditNomenclature();

        if (dateAuditNomenclature == null) {
            return false;
        }

        return DateUtils.estMemeJour(dateAuditNomenclature, new Date());
    }

    /**
     * R�cup�re les offres de formations de secteurs de l'�l�ve sp�cifi�.
     *
     * @param eleve
     *            �l�ve � partir duquel on recherche les offres de formations de secteurs.
     * @return La liste des offres de formations de secteurs de l'�l�ve sp�cifi�.
     */
    public List<Voeu> recupererOffresSecteursEleve(Eleve eleve) {
        List<Voeu> offresSecteursEleve = new ArrayList<>();
        Iterator<LienZoneGeo> iterator = eleve.getZoneGeo().getLiensZoneGeo().iterator();
        while (iterator.hasNext()) {
            LienZoneGeo lienZoneGeo = iterator.next();
            if (lienZoneGeo.isLienVersOffreSecteur()) {
                offresSecteursEleve.add(lienZoneGeo.getVoeu());
            }
        }
        return offresSecteursEleve;
    }

    /**
     * Fournis la listes des offres de formation hors acad�mie.
     *
     * @return La liste des offres de formation concern�es sous forme d'une Map avec cl� = co_voe et valeur = offre
     *         de formation.
     */
    public Map<String, Voeu> offresFormationHorsAcademie() {
        List<Filtre> listFiltres = new ArrayList<>();
        FiltreUtils.ajouterCritereCommencantPar(listFiltres, "code", Voeu.PREFIXE_OFFRE_FORMATION_INSTANCE);
        List<Voeu> voeux = voeuDao.lister(listFiltres);
        Map<String, Voeu> mapVoeux = new TreeMap<>();
        for (Voeu voeu : voeux) {
            mapVoeux.put(voeu.getCode(), voeu);
        }
        return mapVoeux;
    }

    /**
     * Fournis la liste des offres de formations en apprentissage.
     * 
     * @return la liste des offres de formations en apprentissage.
     * 
     */
    public List<Voeu> offresFormationApprentissage() {
        return voeuDao.lister(Filtre.equal("statut.code", StatutEleve.CODE_APPRENTISSAGE));
    }

    /**
     * Lister les offres de formation pour une commission donn�e.
     * 
     * @param codeCommission
     *            le code de la commission
     * @return la liste des offres de formation associ�es � la commission
     */
    public List<Voeu> listerOffresCommission(String codeCommission) {
        return voeuDao.lister(Filtre.equal("commission.code", codeCommission));
    }

}
