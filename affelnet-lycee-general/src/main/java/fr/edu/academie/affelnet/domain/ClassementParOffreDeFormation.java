/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.domain.droits.Module;
import fr.edu.academie.affelnet.web.forms.lancementOpa.ListeProvisoireForm;
import fr.edu.academie.affelnet.web.utils.GestionHabilitation;

/**
 * Le classement par offre de formation pour une OPA.
 */
public class ClassementParOffreDeFormation extends ListeProvisoire {

    /** Table des tris autoris�s. */
    public static final Map<String, String[]> MAP_TRIS_AUTORISES = new HashMap<>();

    static {
        MAP_TRIS_AUTORISES.put("ine", new String[] { "voeuEleve.eleve.ine" });
        MAP_TRIS_AUTORISES.put("nom", new String[] { "voeuEleve.eleve.nom", "voeuEleve.eleve.prenom" });
        MAP_TRIS_AUTORISES.put("etablissementOrigine", new String[] { "voeuEleve.eleve.etablissement.id" });
        MAP_TRIS_AUTORISES.put("rang", new String[] { "id.rang" });
        MAP_TRIS_AUTORISES.put("bareme", new String[] { "bareme" });
        MAP_TRIS_AUTORISES.put("decision", new String[] { "codeDecisionProvisoire", "numeroListeSupp" });
    }

    /** Tris par d�faut. */
    public static final List<Tri> DEFAULT_ORDERS = new ArrayList<>();

    static {
        DEFAULT_ORDERS.add(Tri.asc("codeDecisionProvisoire"));
        DEFAULT_ORDERS.add(Tri.asc("numeroListeSupp"));
        DEFAULT_ORDERS.add(Tri.asc("voeuEleve.eleve.nom"));
        DEFAULT_ORDERS.add(Tri.asc("voeuEleve.eleve.prenom"));
        DEFAULT_ORDERS.add(Tri.asc("voeuEleve.eleve.prenom2"));
        DEFAULT_ORDERS.add(Tri.asc("voeuEleve.eleve.prenom3"));
    }

    /**
     * Le constructeur de ListeProvisoireAffectes.
     * 
     * @param formulaire
     *            le formulaire des filtres
     */
    public ClassementParOffreDeFormation(ListeProvisoireForm formulaire) {
        super(formulaire);
    }

    /**
     * M�thode de v�rification des habilitations pour la liste.
     * 
     * @param request
     *            la requ�te courante
     */
    public static void verifierHabilitations(HttpServletRequest request) {
        GestionHabilitation.verifierAutorisations(request, Module.RACINE, Module.LANCEMENT_OPA);
    }

    @Override
    protected void compositionFiltres(ListeProvisoireForm formulaire) {
        ajouterFiltreVoeu(formulaire);
    }
}
