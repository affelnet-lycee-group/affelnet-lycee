/*
 * Affelnet-Lyc�e - Application nationale d'affectation des �l�ves au lyc�e
 * Copyright (C) 2021 Minist�re de l'�ducation nationale, de la Jeunesse et des Sports
 * Direction g�n�rale de l'enseignement scolaire
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package fr.edu.academie.affelnet.domain;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.edu.academie.affelnet.dao.interfaces.Filtre;
import fr.edu.academie.affelnet.dao.interfaces.Tri;
import fr.edu.academie.affelnet.dao.interfaces.voeu.ResultatsProvisoiresOpaDao;
import fr.edu.academie.affelnet.domain.nomenclature.Cio;
import fr.edu.academie.affelnet.domain.nomenclature.Etablissement;
import fr.edu.academie.affelnet.domain.nomenclature.Voeu;
import fr.edu.academie.affelnet.domain.nomenclature.accueil.Departement;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.OperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.planificationCampagneAffectation.TypeOperationProgrammeeAffectation;
import fr.edu.academie.affelnet.domain.voeu.ResultatsProvisoiresOpa;
import fr.edu.academie.affelnet.service.EtablissementManager;
import fr.edu.academie.affelnet.service.nomenclature.CioManager;
import fr.edu.academie.affelnet.service.nomenclature.DepartementManager;
import fr.edu.academie.affelnet.service.nomenclature.VoeuManager;
import fr.edu.academie.affelnet.service.planificationCampagneAffectation.OperationProgrammeeAffectationManager;
import fr.edu.academie.affelnet.web.forms.lancementOpa.ListeProvisoireForm;
import fr.edu.academie.affelnet.web.utils.SpringUtils;

/**
 * Une liste provisoire des r�sultats d'une OPA.
 * Cette classe permet de construire les filtres pour la liste,
 * puis de charger les r�sultats provisoires correspondants.
 * Seuls les filtres sont conserv�s par l'objet, ceux-ci sont construits � la cr�ation de l'objet.
 * Le contenu de la liste est r�cup�r� dans la base de donn�es � chaque lecture de la liste.
 */
public abstract class ListeProvisoire {

    /** Loggueur de la classe. */
    private static final Log LOG = LogFactory.getLog(ListeProvisoire.class);

    /**
     * La liste des filtres appliqu�s � la liste.
     */
    protected List<Filtre> filtres;

    /**
     * Les libell�s des filtres, organis� en une table contenant pour chaque crit�re la valeur associ�e.
     */
    private Map<String, String> libellesFiltres;

    /**
     * Le gestionnaire des �tablissements.
     */
    private final EtablissementManager etablissementManager = SpringUtils.getBean(EtablissementManager.class);

    /**
     * Le gestionnaire des d�partements.
     */
    private final DepartementManager departementManager = SpringUtils.getBean(DepartementManager.class);

    /**
     * Le gestionnaire des CIO.
     */
    private final CioManager cioManager = SpringUtils.getBean(CioManager.class);

    /**
     * Le gestionnaire des voeux.
     */
    private final VoeuManager voeuManager = SpringUtils.getBean(VoeuManager.class);

    /**
     * Le DAO des r�sultats provisoires des OPA.
     */
    private final ResultatsProvisoiresOpaDao resultatsProvisoiresOpaDao = SpringUtils
            .getBean(ResultatsProvisoiresOpaDao.class);

    /**
     * Le constructeur de ListeProvisoire.
     * 
     * @param formulaire
     *            le formulaire des filtres
     */
    public ListeProvisoire(ListeProvisoireForm formulaire) {
        libellesFiltres = new LinkedHashMap<String, String>();
        creationDesfiltres(formulaire);
    }

    /**
     * @return the filtres
     */
    public List<Filtre> getFiltres() {
        return filtres;
    }

    /**
     * @return the libellesFiltres
     */
    public Map<String, String> getLibellesFiltres() {
        return libellesFiltres;
    }

    /**
     * @return the libelleFiltres
     */
    public String getChaineLibellesFiltres() {
        StringBuilder chaineLibelles = new StringBuilder();
        for (Entry<String, String> libelleFiltre : libellesFiltres.entrySet()) {
            if (chaineLibelles.length() > 0) {
                chaineLibelles.append(" | ");
            }
            chaineLibelles.append(libelleFiltre.getKey());
            chaineLibelles.append(" : ");
            chaineLibelles.append(libelleFiltre.getValue());
        }
        return chaineLibelles.toString();
    }

    /**
     * M�thode d'ajout d'un libell� de filtre � la cha�ne des libell�s des filtres.
     * 
     * @param critere
     *            le crit�re ajout�
     * @param valeur
     *            la valeur pour le crit�re
     */
    public void ajouterLibelleFiltre(String critere, String valeur) {
        libellesFiltres.put(critere, valeur);
    }

    /**
     * M�thode de r�cup�ration des �l�ments de la liste.
     * 
     * @param tris
     *            le tri appliqu�
     * @return les �l�ments de la liste
     */
    public List<ResultatsProvisoiresOpa> elements(List<Tri> tris) {
        return resultatsProvisoiresOpaDao.lister(filtres, tris);
    }

    /**
     * M�thode de cr�ation des filtres correspondant au formulaire sp�cifi�.
     * 
     * @param formulaire
     *            le formulaire
     */
    private void creationDesfiltres(ListeProvisoireForm formulaire) {

        // Traitement du formulaire
        String idEtablissementOrigine = formulaire.getIdEtablissementOrigine();
        String idCioOrigine = formulaire.getIdCioOrigine();
        String secteur = formulaire.getSecteur();
        String idEtablissementAccueil = formulaire.getIdEtablissementAccueil();
        String idCioAccueil = formulaire.getIdCioAccueil();
        String codeVoeu = formulaire.getCodeVoeu();

        LOG.debug("* idEtablissementOrigine : " + idEtablissementOrigine);
        LOG.debug("* idCioOrigine : " + idCioOrigine);
        LOG.debug("* secteur : " + secteur);
        LOG.debug("* idEtablissementAccueil : " + idEtablissementAccueil);
        LOG.debug("* idCioAccueil : " + idCioAccueil);
        LOG.debug("* codeVoeu : " + codeVoeu);

        if (StringUtils.isEmpty(idEtablissementOrigine) && StringUtils.isEmpty(idCioOrigine)
                && StringUtils.isEmpty(secteur) && StringUtils.isEmpty(idEtablissementAccueil)
                && StringUtils.isEmpty(idCioAccueil) && StringUtils.isEmpty(codeVoeu)) {

            // Erreur de saisie
            formulaire.setSearchStatus("-1");
            throw new ValidationException(
                    "Vous devez pr�ciser au moins un crit�re de filtrage pour afficher la liste");
        } else {

            // Ajout des crit�res de s�lection fixes
            filtres = new ArrayList<Filtre>();
            long idOpa = Long.parseLong(formulaire.getIdOpa());
            ajouterFiltresFixes(idOpa);

            compositionFiltres(formulaire);
        }
    }

    /**
     * M�thode d'ajout des filtres sp�cifiques � la page.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected abstract void compositionFiltres(ListeProvisoireForm formulaire);

    /**
     * M�thode d'ajout du filtre de num�ro d'OPA.
     * 
     * @param idOpa
     *            l'identifiant de l'OPA
     */
    private void ajouterFiltresFixes(long idOpa) {
        filtres.add(Filtre.equal("opa.id", idOpa));
    }

    /**
     * M�thode d'ajout du filtre de l'�tablissement d'origine.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected void ajouterFiltreEtablissementOrigine(ListeProvisoireForm formulaire) {
        String idEtablissementOrigine = formulaire.getIdEtablissementOrigine();

        if (StringUtils.isNotEmpty(idEtablissementOrigine)) {

            idEtablissementOrigine = idEtablissementOrigine.toUpperCase();
            Etablissement etablissementOrigine = etablissementManager.chargerNullable(idEtablissementOrigine);

            if (etablissementOrigine != null) {
                // S�lection d'un �tablissement d'origine
                String libelleEtablissementOrigine = etablissementOrigine.libelleEtablissement();
                formulaire.setLibelleEtablissementOrigine(libelleEtablissementOrigine);

                // Filtre de s�lection de l'�tablissement d'origine
                filtres.add(Filtre.equal("voeuEleve.eleve.etablissement.id", idEtablissementOrigine));
                ajouterLibelleFiltre("�tablissement d'origine", libelleEtablissementOrigine);

            } else if (idEtablissementOrigine.equals("HACA")) {
                // S�lection des �l�ves hors acad�mie
                String libelleEtablissementOrigine = "HORS ACAD�MIE";
                formulaire.setLibelleEtablissementOrigine(libelleEtablissementOrigine);

                // Filtre de s�lection des �l�ves hors acad�mie
                filtres.add(Filtre.equal("voeuEleve.eleve.flagHorsAcademie", Flag.OUI));
                ajouterLibelleFiltre("�tablissement d'origine", "HACA - HORS ACAD�MIE");

            } else if (idEtablissementOrigine.length() <= 3
                    && departementManager.isCodeMen(idEtablissementOrigine)) {
                // S�lection d'un d�partement d'origine
                Departement departementOrigine = departementManager.getDepartement(idEtablissementOrigine);
                String libelleEtablissement = departementOrigine.getLibelleLong();
                formulaire.setLibelleEtablissementOrigine(libelleEtablissement);

                // Filtre de s�lection du d�partement d'origine
                filtres.add(Filtre.equal("voeuEleve.eleve.departement.codeMen", idEtablissementOrigine));
                ajouterLibelleFiltre("D�partement d'origine",
                        idEtablissementOrigine + " - " + departementOrigine.getLibelleLong());

            } else {
                formulaire.setSearchStatus("-1");
                throw new ValidationException("L'�tablissement d'origine n'existe pas.");
            }
        }
    }

    /**
     * M�thode d'ajout du filtre du CIO d'origine.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected void ajouterFiltreCioOrigine(ListeProvisoireForm formulaire) {
        String idCioOrigine = formulaire.getIdCioOrigine();

        if (StringUtils.isNotEmpty(idCioOrigine)) {
            // S�lection d'un CIO d'origine
            idCioOrigine = idCioOrigine.toUpperCase();
            Cio cioOrigine = cioManager.chargerNullable(idCioOrigine);

            if (cioOrigine != null) {

                formulaire.setLibelleCioOrigine(cioOrigine.getLibelleLong());

                // Filtre de s�lection du CIO d'origine
                filtres.add(Filtre.equal("voeuEleve.eleve.etablissement.cio.id", idCioOrigine));
                ajouterLibelleFiltre("C. I. O. d'origine", idCioOrigine + " - " + cioOrigine.getLibelleLong());

            } else {
                formulaire.setSearchStatus("-1");
                throw new ValidationException("Le C.I.O. d'origine n'existe pas.");
            }
        }
    }

    /**
     * M�thode d'ajout du filtre du secteur.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected void ajouterFiltreSecteur(ListeProvisoireForm formulaire) {
        String secteur = formulaire.getSecteur();

        if (StringUtils.isNotEmpty(secteur)) {
            filtres.add(Filtre.equal("voeuEleve.eleve.etablissement.codeSecteur", secteur));
            ajouterLibelleFiltre("Secteur", secteur);
        }
    }

    /**
     * M�thode d'ajout du filtre de l'�tablissement d'accueil.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected void ajouterFiltreEtablissementAccueil(ListeProvisoireForm formulaire) {
        String idEtablissementAccueil = formulaire.getIdEtablissementAccueil();

        if (StringUtils.isNotEmpty(idEtablissementAccueil)) {
            // Filtrage de l'�tablissement d'accueil

            idEtablissementAccueil = idEtablissementAccueil.toUpperCase();

            Etablissement etablissementAccueil = etablissementManager.chargerNullable(idEtablissementAccueil);

            if (etablissementAccueil != null) {
                // S�lection d'un �tablissement d'accueil
                String libelleEtablissementAccueil = etablissementAccueil.libelleEtablissement();
                formulaire.setLibelleEtablissementAccueil(libelleEtablissementAccueil);

                // Filtre de s�lection de l'�tablissement d'accueil
                filtres.add(Filtre.equal("voeuEleve.voeu.etablissement.id", idEtablissementAccueil));
                ajouterLibelleFiltre("�tablissement d'accueil", libelleEtablissementAccueil);

            } else if (idEtablissementAccueil.length() <= 3
                    && departementManager.isCodeMen(idEtablissementAccueil)) {
                // S�lection d'un d�partement d'accueil
                Departement departementAccueil = departementManager.getDepartement(idEtablissementAccueil);
                String libelleEtablissement = departementAccueil.getLibelleLong();
                formulaire.setLibelleEtablissementAccueil(libelleEtablissement);

                // Filtre de s�lection du d�partement d'accueil
                filtres.add(Filtre.like("voeuEleve.voeu.etablissement.id", idEtablissementAccueil + "%"));
                ajouterLibelleFiltre("D�partement d'accueil",
                        idEtablissementAccueil + " - " + departementAccueil.getLibelleLong());

            } else {
                formulaire.setSearchStatus("-1");
                throw new ValidationException("L'�tablissement d'accueil n'existe pas.");
            }
        }
    }

    /**
     * M�thode d'ajout du filtre du CIO d'accueil.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected void ajouterFiltreCioAccueil(ListeProvisoireForm formulaire) {
        String idCioAccueil = formulaire.getIdCioAccueil();

        if (StringUtils.isNotEmpty(idCioAccueil)) {
            // S�lection d'un CIO d'accueil
            idCioAccueil = idCioAccueil.toUpperCase();
            Cio cioAccueil = cioManager.chargerNullable(idCioAccueil);

            if (cioAccueil != null) {

                formulaire.setLibelleCioAccueil(cioAccueil.getLibelleLong());

                // Filtre de s�lection du CIO d'accueil
                filtres.add(Filtre.equal("voeuEleve.voeu.etablissement.cio.id", idCioAccueil));
                ajouterLibelleFiltre("C. I. O. d'accueil", idCioAccueil + " - " + cioAccueil.getLibelleLong());

            } else {
                formulaire.setSearchStatus("-1");
                throw new ValidationException("Le C.I.O. d'accueil n'existe pas.");
            }
        }
    }

    /**
     * M�thode d'ajout du filtre du voeu.
     * 
     * @param formulaire
     *            le formulaire
     */
    protected void ajouterFiltreVoeu(ListeProvisoireForm formulaire) {
        String codeVoeu = formulaire.getCodeVoeu();

        if (StringUtils.isNotEmpty(codeVoeu)) {

            // Chargement du voeu
            codeVoeu = codeVoeu.toUpperCase();
            Voeu voeu = voeuManager.chargerNullable(codeVoeu);

            if (voeu != null) {
                String libelleVoeu = voeu.getLibelleLong();
                formulaire.setLibelleVoeu(libelleVoeu);

                OperationProgrammeeAffectationManager opaManager = SpringUtils
                        .getBean(OperationProgrammeeAffectationManager.class);

                OperationProgrammeeAffectation opa = opaManager.charger(Long.valueOf(formulaire.getIdOpa()));

                if (!opa.getType().equals(TypeOperationProgrammeeAffectation.TOUR_SUIVANT)
                        && !opa.getOffreFormations().contains(voeu)) {
                    throw new ValidationException("L'offre de formation n'est pas trait�e par cette OPA.");
                }

                filtres.add(Filtre.equal("voeuEleve.voeu", voeu));
                ajouterLibelleFiltre("Voeu", voeu.getCode() + " - " + voeu.getLibelleLong());

            } else {
                formulaire.setSearchStatus("-1");
                throw new ValidationException("L'offre de formation n'existe pas.");
            }
        }
    }
}
