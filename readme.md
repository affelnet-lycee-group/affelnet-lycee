Affelnet-Lyc�e
==============

Version 22.3.1.0

# Description

Ce d�p�t contient les publications des algorithmes de l'application nationale *Affelnet-Lyc�e*
utilis�e dans les acad�mies pour g�rer l'affectation des �l�ves au lyc�e.

Acad�mies utilisatrices :

Aix-Marseille, Amiens, Besan�on, Bordeaux, Clermont-Ferrand, Corse, Cr�teil, Dijon, Grenoble, Guadeloupe, Guyane,
La Reunion, Lille, Limoges, Lyon, Martinique, Mayotte, Montpellier, Nancy-Metz, Nantes, Nice, Normandie,
Nouvelle Cal�donie, Orleans-Tours, Paris, Poitiers, Polynesie Fran�aise, Reims, Rennes, Strasbourg, Toulouse et Versailles.


# Extrait des sources

L'application est �crite en langage Java et construite avec l'outil Apache Maven.

Elle utilise principalement les frameworks Spring et Hibernate.

Les sources publi�es sont extraites de la base de code Affelnet-Lyc�e
* du service g�n�ral, application web de gestion Affelnet-Lyc�e utilis�e par les �tablissements et l'administration (rectorat, DSDEN)
* du module logiciel commun inclus dans l'application de gestion et les applications web compl�mentaires

*Remarque :*

Le projet *Affelnet-Lyc�e* entre dans une d�marche de publication du code source. 

Cette extraction fournie en l'�tat, __ne constitue pas � ce jour__, un ensemble compilable et ex�cutable.

# Algorithmes m�tiers

Les principaux algorithmes m�tier sont les suivants :
- int�gration des �valuations du livret scolaire unique (LSU)
- calcul des moyennes
- harmonisation
- calcul du bar�me
- classement des voeux des �l�ves

## Int�gration des �valuations LSU

Classe principale : `EvaluationDisciplineManager`

Cet algorithme prend en entr�e les �valuations du livret scolaire unique (LSU) des �l�ves du palier d'origine 3�me.
Il convertit les �valuations de chaque discipline, pour chaque bilan p�riodique, en moyennes de points par discipline, exploitables par la suite des algorithmes.

## Calcul des moyennes

Classe principale : `CalculMoyenneDaoImpl`

Cet algorithme calcule, 
* pour les *bar�mes �valuation*, les moyennes de points par champ disciplinaire pour les �l�ves du palier 3�me et doublants du palier 2nde
* pour les *bar�mes note*, les moyennes de notes des �l�ves du palier 2nde 

Ces moyennes sont utilis�es dans la suite des algorithmes.

## Harmonisation 

Classe principale : `HarmonisationDaoImpl`

Cet algorithme effectue des harmonisations pour 
* les moyennes de points par champ disciplinaire, selon les groupes origine des �l�ves
* les points des �valuations compl�mentaires, selon les groupes origine des �l�ves
* les notes par mati�re, selon les groupes origine des �l�ves

## Calcul du bar�me

Classe principale : `CalculBaremeDaoImpl`

Cet algorithme �tablit le bar�me de chaque voeu formul� sur une offre de formation qui le requiert.
 
Voir le document : AFFL-le bar�me
 
## Classement des voeux des �l�ves

Classe principale : `BaseClassementDaoImpl`

Cet algorithme �tablit le classement des voeux formul�s par les �l�ves pour chaque offre de formation.

L'algorithme attribue les d�cisions finales, selon le type d'offre de formation,
sur chaque voeu formul� et sur l'ensemble de la candidature des �l�ves,
en tenant compte des capacit�s de liste principale et de liste suppl�mentaire.

Voir le document : AFFL-le classement

# �diteur

Minist�re de l'�ducation nationale, de la Jeunesse et des Sports

Direction g�n�rale de l'enseignement scolaire

*107 rue de Grenelle,*
*75007 Paris*
